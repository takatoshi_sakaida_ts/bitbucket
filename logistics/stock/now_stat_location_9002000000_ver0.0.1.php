<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>現時点の仕訳前フリー滞留中の部門在庫状況</title>
<link type="text/css" rel="stylesheet" media="all" href="/css/bolweb-common.css">
<script language="JavaScript" src="/js/jquery-1.4.3.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/bolweb-common.js" type="text/javascript"></script>

</head>
<body bgcolor="#ffffff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\souko\parts\selectvalue_souko.php");
//ログイン情報の読み込み
require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\souko\parts\login_souko.php");

$keyti= date("Ymd",mktime(0, 0, 0, date("m"), date("d"),   date("Y")));

$sql = "select ".
"stock.bumon,".
"m_bumon.bumonname,".
"m_bumon.bumoncatcode,".
"stock.bumon_cost,".
"stock.bumon_count ".
"from".
"(".
"select substr(d_stock.instorecode,8,11) as bumon, count(d_stock.instorecode) as bumon_count, to_char(d_goods_cost.costprice, '9,999,999.999') as bumon_cost from d_stock,d_goods_cost where locationcode = '9002000000' ".
"and d_stock.instorecode like '9%' ".
"and d_stock.instorecode = d_goods_cost.instorecode ".
"and d_goods_cost.newused_kind = '0' ".
"group by d_stock.instorecode,d_goods_cost.costprice".
") stock,".
"m_bumon ".
"where ".
"stock.bumon = m_bumon.bumoncode ".
"order by ".
"stock.bumon";

// print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<div>";
print "<table>\n";
print "<tr>\n";
//項目名の表示
print "<th>部門コード</th><th>部門名</th><th>在庫単価</th><th>在庫点数</th></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
	print "<td class=\"code\">". $row[0]."</td>";
	print "<td>".$row[1] ."</td>";
	print "<td class=\"text\">".$row[3] ."</td>";
	print "<td class=\"number\">".$row[4] ."</td></tr>";
}
print "</table>";
print "</div>";
//データの開放
$res->free();
$db->disconnect();
?>
</body>
</html>
