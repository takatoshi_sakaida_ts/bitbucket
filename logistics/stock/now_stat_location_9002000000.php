<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>部門商材の計算/保有在庫状況</title>
<link type="text/css" rel="stylesheet" media="all" href="/css/bolweb-common.css">
<script language="JavaScript" src="/js/jquery-1.4.3.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/bolweb-common.js" type="text/javascript"></script>

</head>
<body bgcolor="#ffffff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\souko\parts\selectvalue_souko.php");
//ログイン情報の読み込み
require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\souko\parts\login_souko.php");

$keyti= date("Ymd",mktime(0, 0, 0, date("m"), date("d"),   date("Y")));

$lines = file("pre_free_bumon_stock.csv");

print "<div>";
print "データ取得日時 - " . date ("Y/m/d H:i", filemtime("pre_free_bumon_stock.csv"));
print "<table>";
print "<tr><th>部門名</th><th>部門コード</th><th>承認待ち点数</th><th>崩し待ち在庫点数</th><th>仕分け前フリー在庫点数</th><th>仕分け前フリー在庫単価</th></tr>";
foreach ($lines as $line_num => $line) {
  $columns = explode(",", $line);
  print "<tr>".
        "<td>".$columns[0]."</td>". // 部門名
        "<td class=\"code\">".$columns[1]."</td>". // 部門コード
        "<td class=\"number\">".$columns[2]."</td>". // 承認待ち点数
        "<td class=\"number\">".$columns[3]."</td>". // 崩し待ち在庫点数
        "<td class=\"number\">".$columns[4]."</td>". // 仕分け前フリー在庫点数
        "<td class=\"text\">".$columns[5]."</td>". // 仕分け前フリー在庫単価
        "</tr>";
}
print "</table>";
print "</div>";

?>
</body>
</html>
