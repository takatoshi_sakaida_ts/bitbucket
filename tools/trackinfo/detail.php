<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
		<title>トラック情報修正</title>
		<script type="text/javascript" src="trackinfo.js"></script>
		<link rel="stylesheet" type="text/css" href="trackinfo.css">
	</head>
	<body>
		<div style="margin-left:10px; margin-top:30px; font-size:15px;">
			<b>【トラック情報修正】</b><br /><br />
			<b><font color="red">※ECサイトへのトラック情報反映は翌日になります。</font></b><br /><br />
			<a href="search.php">検索へ戻る</a>　<a href="../../index.php">トップへ戻る</a>
		</div>
		<hr style="margin:20px 0px;">
		<?php
			$instoreCode = addslashes(@$_POST["instoreCode"]);
			if(!$instoreCode){
				$instoreCode = addslashes(@$_GET["instoreCode"]);
			}
			//ファイルの読み込み
			//PEARの利用     -------(1)
			require_once("DB.php");
			//ログイン情報の読み込み
			require_once("../common/dbc/login_souko_dev.php");
			//商品名取得
			$goodsSql = "select GOODSNAME from m_goods_display where INSTORECODE = '".$instoreCode."'";
			$goodsRes = $db->query($goodsSql);
			if(DB::isError($goodsRes)){
				$goodsRes->DB_Error($goodsRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
			}
			$trackSql = "select JMDKEY, MUSICORDER, DISPLAYORDER, CONTENTS from m_goods_jmd where JMDKEY in (";
			$trackSql = $trackSql."select JMDKEY from M_JMD_GOODS_KEY where INSTORECODE = '".$instoreCode."') order by MUSICORDER, DISPLAYORDER";
			$trackRes = $db->query($trackSql);
			if(DB::isError($trackRes)){
				$trackRes->DB_Error($trackRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
			}
		?>
		<div style="margin-left:10px; margin-top:15px;">
			<B>●検索結果</B><br /><br />
			<table class="inputTbl">
				<tr style="border:1px;">
					<th>インストアコード</th>
					<?php
						print "<td><B>".$instoreCode."</B></td>";
						print "<th>商品名</th>";
						while($goodsRow = $goodsRes->fetchRow()){
							print "<td><B>".$goodsRow[0]."</B></td>";
						}
						$goodsRes->free();
					?>
				</tr>
			</table>
		</div>
		<hr style="margin:20px 0px;">
		<div style="margin-left:10px; margin-top:15px;">
			<B>●トラック情報</B><br /><br />
			<table class="dataTbl">
				<tr>
					<th>No</th>
					<th>DickNo</th>
					<th>トラック名</th>
					<th>更新</th>
					<th>削除</th>
				</tr>
				<?php
					$count = 0;
					$dickNo = 0;
					$musicOrder = "";
					$dataExist = false;
					while($trackRow = $trackRes->fetchRow()){
						$count++;
						$dataExist = true;
						if($count % 2 == 0){
							print "<tr style=\"background-color:#96F2A1;\">\r\n";
						} else {
				    		print "<tr>\r\n";
				    	}
				    	print "<td>".$count."</td>";
				    	if($dickNo == 0){
				    		$dickNo++;
				    		$musicOrder = $trackRow[1];
				    		print "<td><b>Disk".$dickNo."</b></td>";
				    	} else {
				    		if($musicOrder == $trackRow[1]){
				    			print "<td><b>Disk".$dickNo."</b></td>";
				    		} else {
				    			$dickNo++;
				    			$musicOrder = $trackRow[1];
				    			print "<td><b>Disk".$dickNo."</b></td>";
				    		}
				    	}
							//print "<td>".$trackRow[0]."</td>\r\n";
							print "<td>".$trackRow[1]."</td>\r\n";
							print "<td>".$trackRow[2]."</td>\r\n";
						print "<form action=\"update.php\" method=\"post\" onSubmit=\"return updateCheck()\">\r\n";
						print "<td><input type=\"text\"  size=\"100\" maxlength=\"2000\" name=\"trackName\" id=\"trackName\" value=\"".$trackRow[3]."\"></td>\r\n";
						print "<input type=\"hidden\" name=\"jmdKey\" value=\"".$trackRow[0]."\">\r\n";
						print "<input type=\"hidden\" name=\"musicOrder\" value=\"".$trackRow[1]."\">\r\n";
						print "<input type=\"hidden\" name=\"displayOrder\" value=\"".$trackRow[2]."\">\r\n";
						print "<input type=\"hidden\" name=\"instoreCode\" id=\"instoreCode\" value=\"".$instoreCode."\">\r\n";
						print "<td><input type=\"submit\" name=\"button1\" value=\"更新\"></td>\r\n";
						print "</form>\r\n";
						print "<form action=\"delete.php\" method=\"post\" onSubmit=\"return deleteCheck(".$dickNo.",'".$trackRow[3]."')\">\r\n";
						print "<input type=\"hidden\" name=\"jmdKey\" value=\"".$trackRow[0]."\">\r\n";
						print "<input type=\"hidden\" name=\"musicOrder\" value=\"".$trackRow[1]."\">\r\n";
						print "<input type=\"hidden\" name=\"displayOrder\" value=\"".$trackRow[2]."\">\r\n";
						print "<input type=\"hidden\" name=\"instoreCode\" id=\"instoreCode\" value=\"".$instoreCode."\">\r\n";
						print "<td><input type=\"submit\" name=\"button1\" value=\"削除\"></td>\r\n";
						print "</form>\r\n";
						print "</tr>\r\n";
					}
					if(!$dataExist){
							print "<tr><td colspan=\"7\">";
						print "<font color=\"red\">\r\n";
						print "該当データが見つかりませんでした。　システムトラックを発行してください。\r\n";
						print "</font>\r\n";
						print "</td></tr>";
					}
					$trackRes->free();
					$db->disconnect();
				?>
			</table>
		</div>
	</body>
</html>