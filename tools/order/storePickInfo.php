<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
		<title>店舗引き当て確認画面</title>
		<link type="text/css" rel="stylesheet" media="all" href="/css/bolweb-common.css">
		<script language="JavaScript" src="/js/jquery-1.4.3.js" type="text/javascript"></script>
		<script language="JavaScript" src="/js/bolweb-common.js" type="text/javascript"></script>
		
		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	</head>
	<?php
		require_once("DB.php");
		require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\\tools\common\dbc\login_ec.php");
		
		$ord_no = @$_GET["ord_no"];
	?>
	<body>
		<!-- 検索フォーム -->
		<form id="search-form" class="form" method="get" action="#" method="get">
			<div class="container">
				<h2>店舗引き当て確認画面</h2>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<!-- 注文番号 -->
							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">注文番号 <span class="label label-info">完全一致</span></label>
									<?php
										print "<input type=\"txt\" class=\"form-control\" name=\"ord_no\" value=\"$ord_no\">";
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<div class="form-inline">
									<button type="submit" class="btn btn-primary">検索</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- 結果出力 -->
		<div class="container">
			<?php
				if(!empty($ord_no)) {
					// SQLインジェクション対策
					if(preg_match("/^\d{16}$/",$ord_no)){
					
						print "<div class=\"page-header\"><nav class=\"pull-right\"><ul class=\"pager\" style=\"margin-top:0;\"></ul></nav><h3>検索結果</h3></div>";
						print "<div class=\"row\"><div class=\"col-md-12\">";
						
						// 注文詳細抽出
						$sql_dtl = "select A.ord_no,A.ord_seq,A.instorecode,A.store_cd,B.store_nm "
						."from torderdtl A "
						."inner join tstore_tkh B "
						."on A.store_cd = B.store_cd "
						."where A.ord_no = '".$ord_no."' order by A.ord_seq";
						$result_dtl = $ecdb->query($sql_dtl);
						
						// 結果出力
						print "<table class=\"table table-striped table-bordered table-condensed\">";
						print "<thead style=\"background-color:#101010; color:#E0E0E0;\"><tr>"
						."<th>#</th><th>注文番号</th><th>インストアコード</th><th>店舗コード</th><th>店舗名</th><th>引当区分</th>"
						."</tr></thead>";
						
						print "<tbody>";
						
						while($row_dtl = $result_dtl->fetchRow()) {
							$ord_seq = $row_dtl[1];
							
							// 現在の引き当て情報出力
							print "<tr>"
							."<td>".$row_dtl[1]."</td><td>".$row_dtl[0]."</td><td>".$row_dtl[2]."</td><td>".$row_dtl[3]."</td><td>".$row_dtl[4]."</td><td>現在</td>"
							."</tr>";
							
							// 過去の引き当て情報抽出
							$sql_pick = "select A.ord_no,A.ord_seq,A.instorecode,A.store_cd,B.store_nm,A.reg_dm "
							."from tstore_pick_spf A "
							."inner join tstore_tkh B "
							."on A.store_cd = B.store_cd "
							."where A.ord_no = '".$ord_no."' and A.ord_seq='".$ord_seq."' order by A.reg_dm desc";
							$result_pick = $ecdb->query($sql_pick);
							
							while($row_pick = $result_pick->fetchRow()) {
								//店舗の引き当て情報出力
								if($row_dtl[3] !== $row_pick[3]){
									print "<tr class=\"danger\">"
									."<td>┗</td><td>".$row_pick[0]."</td><td>".$row_pick[2]."</td><td>".$row_pick[3]."</td><td>".$row_pick[4]."</td><td>過去</td>"
									."</tr>";
								}
							
							}
						}
						print "</tbody>";
						print "</table>";

						print "</div></div>";
						
					} else {
						print "<div class=\"panel panel-danger\"><div class=\"panel-body bg-danger text-center\" style=\"color:#FF0000\">注文番号は半角英数字16文字で入力してください。</div></div>";
					}
				}
			?>
		</div>
	</body>
</html>