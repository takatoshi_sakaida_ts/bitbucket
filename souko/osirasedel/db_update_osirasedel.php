<?PHP
function db_update(){
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
$db->autoCommit( false );
//ブックマーク削除
if ($_SESSION["HSMAP"]["bookresultsql"]!="nothing")
{
	$sql=$_SESSION["HSMAP"]["bookresultsql"];
//print "bookmark:".$sql."<BR>";
	$res2 = $db->query($sql);
	if(DB::isError($res2)){
		print $res2->getMessage();
		$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($sql,$_SESSION["user_id"],'osirasedel_sql');
	output_lognotime($_SESSION["HSMAP"]["bookresultlog"],'osirasedelresult');
}
//入荷お知らせ削除
if ($_SESSION["HSMAP"]["arrivalresultsql"]!="nothing")
{
	$sql=$_SESSION["HSMAP"]["arrivalresultsql"];
//print "arrival:".$sql."<BR>";
	$res2 = $db->query($sql);
	if(DB::isError($res2)){
		print $res2->getMessage();
		$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($sql,$_SESSION["user_id"],'osirasedel_sql');
	output_lognotime($_SESSION["HSMAP"]["arrivalresultlog"],'osirasedelresult');
}
//値下お知らせ削除
if ($_SESSION["HSMAP"]["discountresultsql"]!="nothing")
{
	$sql=$_SESSION["HSMAP"]["discountresultsql"];
//print "discount:".$sql."<BR>";
	$res2 = $db->query($sql);
	if(DB::isError($res2)){
		print $res2->getMessage();
		$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($sql,$_SESSION["user_id"],'osirasedel_sql');
	output_lognotime($_SESSION["HSMAP"]["discountresultlog"],'osirasedelresult');
}
$db->commit();
//データの開放
$db->disconnect();
}
?>
