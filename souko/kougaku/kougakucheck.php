<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>最近１週間高額（３万円）以上注文検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
$sql = "select ord_no,(TOT_INV_AMT-ORD_CNCL_AMT),pay_tp,ORDR_NM_FST||ORDR_NM_MID from torder ".
"where (TOT_INV_AMT-ORD_CNCL_AMT)>29999 ".
"and substr(ord_dm,1,8)>(select to_char(sysdate-8,'yyyymmdd') from dual) ".
"order by ord_dm";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>最近１週間高額（３万円）以上注文検索結果</strong> <br><br>\n<HR>";
$j=1;
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td></td><td nowrap width=120>注文番号</td><td nowrap width=120>注文金額</td><td nowrap width=120>支払方法</td><td nowrap width=120>注文者名</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//注文番号
	print "<td>".$row[0] ."</td>";
//金額
	print "<td>".$row[1] ."</td>";
//支払方法
	if ($row[2]=='2')
	{print "<td>カード</td>";}
	else
	{print "<td>代引</td>";}
//注文者名
	print "<td>".$row[3] ."</td>";
	print "</tr>";
	$j++;
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>