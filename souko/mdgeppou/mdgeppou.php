<?PHP
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../log/loger.inc");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//ファイル書き込み
$settoday = date("Ymd");
$today = date("YmdHis");
$sw=1;

//ジャンル設定
/*
$setgenre="GENRE1CODE";
$setgenretitle='ジャンル１';
$setgenre2code="4";
*/
$setgenre="GENRE2CODE";
$setgenre2code="6";
$setgenretitle='ジャンル２';
//カテゴリ設定
switch ($sw){
	case 0:
		$cat="11";
		$catname="comic";
		break;
	case 1:
		$cat="12";
		$catname="book";
		break;
	case 2:
		$cat="31";
		$catname="cd";
		break;
	case 3:
		$cat="71";
		$catname="dvd";
		break;
	case 4:
		$cat="51";
		$catname="game";
		break;
}
$filename= "./dlfile/mdgeppou_".$setgenre."_".$catname."_" . $settoday . ".tsv";
//データベースへの接続開始
/*
以下関数
*/
function outputfile($insql,$midasi,$fn){
	$fp = fopen( $fn, "a");
//タイトルの書き込み
	fputs($fp,$midasi);
	fputs($fp,"\r\n");
	$ncols ="0";
	@$ncols = $insql->numCols();
//ファイル出力処理
	while($row = $insql->fetchRow()){
		$str="";
		for($i=0;$i<$ncols;$i++){
			$str=$str . $row[$i] . '	';//行の列データを取得
		}
		fputs($fp,$str);
		fputs($fp,"\r\n");
	}
	fclose( $fp );
}

//DB接続
$db_souko = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db_souko)){
	echo "Fail\n" . DB::errorMessage($db_souko) . "\n";
}
$db_souko->autoCommit( false ); 
//売上数合計
$sql="select m.".$setgenre.", ".
"d.NEWUSED_KIND, ".
"count(g.stockno), ".
"sum(g.UNITPRICE) ".
"from ".
"d_slip_goods g,m_goods m,d_reception_goods d ".
"where g.instorecode=m.instorecode ".
"and g.stockno=d.stockno ".
"and GENRE0CODE='".$cat."' ".
"and substr(slipcode,1,8)='00'||(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"and d.cancel_kind='0' ".
"group by m.".$setgenre.",d.NEWUSED_KIND ".
"order by m.".$setgenre.",d.NEWUSED_KIND ";

$title="売上集計(0:中古 1:新古2:新品)\r\n".$setgenretitle."	在庫区分	販売数	販売金額";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//

//
$sql="select m.".$setgenre.", ".
"count(g.stockno), ".
"sum(g.UNITPRICE) ".
"from ".
"d_slip_goods g,m_goods m ".
"where g.instorecode=m.instorecode ".
"and substr(slipcode,1,8)='20'||(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"and GENRE0CODE='".$cat."' ".
"group by m.".$setgenre." ".
"order by m.".$setgenre ;

$title="買取集計\r\n".$setgenretitle."	買取数	買取金額";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//
/*棚入れ　ジャンル１*/
$sql="select  ".
"m.".$setgenre.", ".
"s.newused_kind, ".
"count(stockno), ".
"sum(COSTPRICE) ".
"from d_tana_goods s,m_goods m,d_goods_cost c ".
"where substr(s.locationcode,1,1)<'9'  and tana_kind in ('51','52','55') ".
"and s.instorecode=m.instorecode ".
"and s.instorecode=c.instorecode ".
"and s.newused_kind=c.newused_kind ".
"and substr(s.modifydate,1,6)=(select to_char(sysdate-1,'yyyymm') from dual) ".
"and GENRE0CODE='".$cat."' ".
"group by m.".$setgenre.",s.newused_kind ".
"order by m.".$setgenre.",s.newused_kind ";
$title="入庫集計(0:中古 1:新古)\r\n".$setgenretitle."	在庫区分	入庫数	原価";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//
/*在庫　ジャンル１*/
$sql="select m.".$setgenre.", ".
"c.NEWUSED_KIND, ".
"count(s.stockno), ".
"sum(c.costprice) ".
"from d_stock s,m_goods m,d_goods_cost c ".
"where s.instorecode=m.instorecode ".
"and s.instorecode=c.instorecode ".
"and s.NEWUSED_KIND=c.NEWUSED_KIND ".
"and GENRE0CODE='".$cat."' ".
"group by m.".$setgenre.",c.NEWUSED_KIND ".
"order by m.".$setgenre.",c.NEWUSED_KIND ";
$title="在庫集計(0:中古 1:新古)\r\n".$setgenretitle."	在庫区分	在庫数	原価";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//
/*発注金額*/
$sql="select m.".$setgenre.", ".
"count(g.detailqty), ".
"sum(g.DETAILPRICE) ".
"from ".
"d_order_goods g,m_goods m,d_reception_goods d ".
"where g.instorecode=m.instorecode ".
"and g.receptioncode=d.receptioncode ".
"and g.DETAILNO=d.DETAILNO ".
"and GENRE0CODE='".$cat."' ".
"and cancel_kind='0' ".
"and substr(FIXDATE,1,6)=(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"group by m.".$setgenre." ".
"order by m.".$setgenre;
$title="発注集計\r\n".$setgenretitle."	発注数	発注金額";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//
/*返品実績*/
$sql="select m.".$setgenre.", ".
"count(g.stockno), ".
"sum(g.costprice) ".
"from ".
"d_slip_goods g,m_goods m ".
"where g.instorecode=m.instorecode ".
"and substr(slipcode,1,8)='13'||(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"and GENRE0CODE='".$cat."' ".
"group by m.".$setgenre." ".
"order by m.".$setgenre ;
$title="返品集計\r\n".$setgenretitle."	返品数	原価";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//

/*中古在庫化*/
$sql="select m.".$setgenre.", ".
"count(g.stockno), ".
"sum(g.costprice) ".
"from ".
"d_slip_goods g,m_goods m ".
"where g.instorecode=m.instorecode ".
"and substr(slipcode,1,8)='10'||(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"and GENRE0CODE='".$cat."' ".
"group by m.".$setgenre." ".
"order by m.".$setgenre;
$title="中古在庫化集計\r\n".$setgenretitle."	中古化数	原価";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//

/*商品マスタ調査*/
/*マスタ数*/
$sql="select ".$setgenre.", ".
"count(instorecode) ".
"from m_goods ".
"where DELETE_FLG='0' ".
"and GENRE0CODE='".$cat."' ".
"group by ".$setgenre." ".
"order by ".$setgenre;
$title="商品マスタ集計\r\n".$setgenretitle."	マスタ数";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//

/*商品区分毎*/
$sql="select ".$setgenre.", ".
"GOODS_KIND, ".
"count(instorecode) ".
"from m_goods ".
"where GENRE0CODE='".$cat."' ".
"group by GOODS_KIND,".$setgenre." ".
"order by ".$setgenre.",GOODS_KIND ";
$title="商品区分集計\r\n".$setgenretitle."	商品区分（０：非表示　１：新品のみ　２：中古のみ　３：すべて）	マスタ数";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//

/*更新マスタ数*/
$sql="select ".$setgenre.", ".
"count(instorecode) ".
"from m_goods ".
"where DELETE_FLG='0' ".
"and GENRE0CODE='".$cat."' ".
"and substr(modifydate,1,6)=(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"group by ".$setgenre." ".
"order by ".$setgenre ;
$title="更新マスタ集計\r\n".$setgenretitle."	マスタ数";
//データ取得
	$res = $db_souko->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
outputfile($res,$title,$filename);
//
//倉庫接続切断
$db_souko->disconnect();
//EC接続
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
$db_ec = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db_ec)){
	echo "Fail\n" . DB::errorMessage($db_ec) . "\n";
}
$db_ec->autoCommit( false ); 

/*楽天・モバイル出荷集計*/
$sql="select ".
"ORD_CRT_TP, ".
"substr(genre3code,1,".$setgenre2code."), ".
"sum(suu), ".
"sum(goukei) ".
"from ".
"( ".
"select distinct ".
"d.ord_no||d.ORD_SEQ, ".
"d.instorecode, ".
"(GOODS_CNT-ORD_CNCL_CNT) as suu, ".
"((GOODS_CNT-ORD_CNCL_CNT)*real_sale_pr) as goukei, ".
"t.ORD_CRT_TP ".
"from  ".
"torderdtl d, ".
"torderdelv l, ".
"torder t ".
"where ".
"d.delv_no=l.delv_no ".
"and d.ord_no=t.ord_no ".
"and t.ord_stat_cl='03' ".
"and substr(l.delv_dm,1,6)=(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"and (GOODS_CNT-ORD_CNCL_CNT)>0 ".
") m, ".
"tgoods_bko g ".
"where m.instorecode=g.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by ORD_CRT_TP,substr(genre3code,1,".$setgenre2code.") ".
"order by ORD_CRT_TP,substr(genre3code,1,".$setgenre2code.") ";
$title="楽天・モバイル注文金額\r\n注文経路(1:PC 5:MB 6:楽天)	".$setgenretitle."	販売数	販売金額";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);
/*BtoB出荷集計*/
$sql="select ".
"substr(genre3code,1,".$setgenre2code."), ".
"sum(suu), ".
"sum(goukei) ".
"from ".
"( ".
"select distinct ".
"d.ord_no||d.ORD_SEQ, ".
"d.instorecode, ".
"(GOODS_CNT-ORD_CNCL_CNT) as suu, ".
"((GOODS_CNT-ORD_CNCL_CNT)*real_sale_pr) as goukei, ".
"t.ORD_CRT_TP ".
"from  ".
"torderdtl d, ".
"torderdelv l, ".
"torder t ".
"where ".
"d.delv_no=l.delv_no ".
"and d.ord_no=t.ord_no ".
"and t.ord_stat_cl='03' ".
"and substr(l.delv_dm,1,6)=(SELECT TO_CHAR(SYSDATE-15,'YYYYMM') FROM DUAL) ".
"and (GOODS_CNT-ORD_CNCL_CNT)>0 ".
"and mem_id in ".
"('toshimi@Bookoffonline.jp',".
"'Toshimi@Bookoffonline.jp',".
"'TOshimi@Bookoffonline.jp',".
"'TOShimi@Bookoffonline.jp',".
"'TOSHimi@Bookoffonline.jp',".
"'TOSHImi@Bookoffonline.jp',".
"'TOSHIMi@Bookoffonline.jp',".
"'TOSHIMI@Bookoffonline.jp',".
"'tOshimi@Bookoffonline.jp',".
"'toShimi@Bookoffonline.jp',".
"'toshimi@bookoffonline.jp',".
"'Toshimi@bookoffonline.jp',".
"'TOshimi@bookoffonline.jp',".
"'TOShimi@bookoffonline.jp',".
"'TOSHimi@bookoffonline.jp',".
"'TOSHImi@bookoffonline.jp',".
"'TOSHIMi@bookoffonline.jp',".
"'TOSHIMI@bookoffonline.jp',".
"'tOshimi@bookoffonline.jp',".
"'toShimi@bookoffonline.jp',".
"'toshimi@BOokoffonline.jp',".
"'Toshimi@BOokoffonline.jp',".
"'TOshimi@BOokoffonline.jp',".
"'TOShimi@BOokoffonline.jp',".
"'yanagi@bookoffonline.jp',".
"'Shimizu@bookoffonline.jp',".
"'yuji.hagiwara@Boc.bookoff.co.jp',".
"'Yuji.hagiwara@Boc.bookoff.co.jp',".
"'YUji.hagiwara@Boc.bookoff.co.jp',".
"'YUJji.hagiwara@Boc.bookoff.co.jp',".
"'YUJI.hagiwara@Boc.bookoff.co.jp',".
"'YUJI.Hagiwara@Boc.bookoff.co.jp',".
"'yuji.hagiwara@boc.bookoff.co.jp',".
"'Yuji.hagiwara@boc.bookoff.co.jp',".
"'YUji.hagiwara@boc.bookoff.co.jp',".
"'YUJi.hagiwara@boc.bookoff.co.jp',".
"'YUJI.hagiwara@boc.bookoff.co.jp',".
"'YUJI.Hagiwara@boc.bookoff.co.jp',".
"'yuji.hagiwara@BOc.bookoff.co.jp',".
"'Yuji.hagiwara@BOc.bookoff.co.jp',".
"'YUji.hagiwara@BOc.bookoff.co.jp',".
"'YUJi.hagiwara@BOc.bookoff.co.jp'".
") ".
") m, ".
"tgoods_bko g ".
"where m.instorecode=g.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code.") ".
"order by substr(genre3code,1,".$setgenre2code.") ";

$title="BtoB注文金額　\r\n".$setgenretitle."	販売数	販売金額";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*登録メール数*/
/*５０以上*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>50 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　５０以上\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);
//
/*４１から５０*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>40 ".
"and suu<51 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　４１以上５０\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*３１から４０*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>30 ".
"and suu<41 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　３１以上４０\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*２１から３０*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>20 ".
"and suu<31 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　２１以上３０\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*１１から２０*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>10 ".
"and suu<21 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　１１以上２０\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*６から１０*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>5 ".
"and suu<11 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　６以上１０\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*２から５*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu>1 ".
"and suu<6 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　２以上５\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*１*/
$sql="select ".
"genre2code, ".
"count(instorecode) ".
"from ".
"( ".
"select ".
"substr(genre3code,1,".$setgenre2code.") as genre2code, ".
"t.instorecode, ".
"count(mem_no||t.instorecode) as suu ".
"from TALERTMAIL_ARRIVAL t, ".
"tgoods_bko m ".
"where  ".
"t.instorecode=m.instorecode ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code."),t.instorecode ".
") ".
"where suu=1 ".
"group by genre2code ".
"order by genre2code ";
$title="お知らせメール登録　１\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*オトナ買い登録数*/
$sql="select ".
"substr(genre3code,1,".$setgenre2code."), ".
"count(grp_id) ".
"from TGOODS_GRP ".
"where ".
"substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code.") ".
"order by substr(genre3code,1,".$setgenre2code.") ";
$title="オトナ買い登録数\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);

/*欠品ありオトナマスタ数*/
$sql="select ".
"substr(genre3code,1,".$setgenre2code."), ".
"count(distinct d.grp_id) ".
"from TGOODS_GRP g,TGOODS_GRPDTL d,tstock_bko s ".
"where ".
"g.grp_id=d.grp_id ".
"and d.instorecode=s.instorecode ".
"and (STOCK_RSV+STOCK_NEW+STOCK_used)=0 ".
"and substr(genre3code,1,2)='".$cat."' ".
"group by substr(genre3code,1,".$setgenre2code.") ".
"order by substr(genre3code,1,".$setgenre2code.") ";
$title="欠品ありオトナ買い登録数\r\n".$setgenretitle."	登録数";
$res = $db_ec->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
outputfile($res,$title,$filename);
$db_ec->disconnect();
$sw++;
//output_log($logword,'batch','searchhonninn');

?>
