<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取検索結果</title>
</head>
<body bgcolor="#ccccff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti2 = addslashes(@$_POST["TI2"]);
$keyti3 = addslashes(@$_POST["TI3"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti2)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
if (empty($keyti)) {
	$sql = "select d.ASSCODE, d.BRANCHNUM, m.STAFFNAME, g.MODIFYTIME, r.CUSTOMERNAME, d.BOX_RECEPT_QTY, sum(DETAILQTY), q.qty, sum(TAX_PRICE) ";
	$sql = $sql . "from d_ass d, d_ass_request r, d_ass_goods g, m_staff m, ";
	$sql = $sql . "(select ASSCODE, sum(qty) as qty, BRANCHNUM from D_ASS_DISCARD group by ASSCODE, BRANCHNUM) q ";
	$sql = $sql . "where d.ASSCODE = r.ASSCODE ";
	$sql = $sql . "and d.ASSCODE = g.ASSCODE ";
	$sql = $sql . "and d.BRANCHNUM = g.BRANCHNUM ";
	$sql = $sql . "and d.BRANCHNUM = q.BRANCHNUM ";
	$sql = $sql . "and d.OPERATESTAFFCODE = m.STAFFCODE ";
	$sql = $sql . "and d.ASSCODE = q.ASSCODE ";
	$sql = $sql . "and d.OPERATEDATE = '".$keyti2."' ";
	$sql = $sql . "and m.STAFFNAME like '%".$keyti3."%' ";
	$sql = $sql . "group by d.ASSCODE, d.BRANCHNUM, d.BOX_RECEPT_QTY, m.STAFFNAME, g.MODIFYTIME, r.CUSTOMERNAME, q.qty ";
	$sql = $sql . "order by d.ASSCODE, d.BRANCHNUM ";
} else {
	$sql = "select d.ASSCODE, d.BRANCHNUM, m.STAFFNAME, g.MODIFYTIME, r.CUSTOMERNAME, d.BOX_RECEPT_QTY, sum(DETAILQTY), q.qty, sum(TAX_PRICE) ";
	$sql = $sql . "from d_ass d, d_ass_request r, d_ass_goods g, m_staff m, ";
	$sql = $sql . "(select ASSCODE, sum(qty) as qty, BRANCHNUM from D_ASS_DISCARD group by ASSCODE, BRANCHNUM) q ";
	$sql = $sql . "where d.ASSCODE = r.ASSCODE ";
	$sql = $sql . "and d.ASSCODE = g.ASSCODE ";
	$sql = $sql . "and d.BRANCHNUM = g.BRANCHNUM ";
	$sql = $sql . "and d.BRANCHNUM = q.BRANCHNUM ";
	$sql = $sql . "and d.OPERATESTAFFCODE = m.STAFFCODE ";
	$sql = $sql . "and d.ASSCODE = q.ASSCODE ";
	$sql = $sql . " and d.OPERATEDATE = '".$keyti2."'";
	$sql = $sql . " and d.OPERATESTAFFCODE ='".$keyti."'";
	$sql = $sql . "group by d.ASSCODE, d.BRANCHNUM, d.BOX_RECEPT_QTY, m.STAFFNAME, g.MODIFYTIME, r.CUSTOMERNAME, q.qty ";
	$sql = $sql . "order by d.ASSCODE, d.BRANCHNUM ";
}
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索結果　スタッフコード：". $keyti."　計算日付：". $keyti2."</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>項番</td>";
print "<td nowrap width=150>買取受付番号</td>";
print "<td nowrap width=40>枝番</td>";
print "<td nowrap width=120>計算者名</td>";
print "<td nowrap width=100>計算時間</td>";
print "<td nowrap width=100>氏名</td>";
print "<td nowrap width=100>箱数</td>";
print "<td nowrap width=100>点数</td>";
print "<td nowrap width=100>D点数</td>";
print "<td nowrap width=100>金額</td>";
print "</tr>";
while($row = $res->fetchRow()){
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//注文番号
	print "<td><a href='../skaitori/detailkaitori.php?receptioncode=" .$row[0] ."'>".$row[0] ."</td>";
	print "<td><a href='../skaitori/detailkaitori.php?receptioncode=" .$row[0] ."&branchnum=" .$row[1] ."'>".sprintf('%03d', $row[1]) ."</td>";
//	print "<td>".sprintf('%03d', $row[1]) ."</td>";
	print "<td>".$row[2] ."</td>";
	print "<td>".$row[3] ."</td>";
	print "<td>".$row[4] ."</td>";
	print "<td>".$row[5] ."</td>";
	print "<td>".$row[6] ."</td>";
	print "<td>".$row[7] ."</td>";
	print "<td>".$row[8] ."</td>";
	$j++;
}
	print "</tr></table>";
	print "<HR>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
