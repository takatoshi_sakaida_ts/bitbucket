<?PHP
require '../parts/pagechk.inc';
require '../parts/selectvalue_souko.php';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取ステータス確認対象検索結果</title>
</head>
<body bgcolor="#99ffcc">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
$sql = "select sell_no,PICKUP_NM_FST||PICKUP_NM_MID,RECEIPT_TP,SELL_STAT,NOTE_YN,AUTH_STAT from tsell t,tmember m";
$sql = $sql . " where sell_no='".$keyti."' and t.mem_no=m.mem_no(+)";
//print $sql;
//検索結果の表示
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
print "<strong><BR>検索条件：". $keyti."</strong> <br><br>\n<HR>";
$row = $res->fetchRow();
if (empty($row[0]))
{
print "対象の買取受付はありません";
}
else
{
	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td nowrap width=120>買取受付番号</td><td nowrap width=100>氏名</td><td width=100>受付経路</td><td width=100>ECステータス</td><td width=100>通知要否</td><td width=100>本人確認</td>";
	print "</tr>";
		print "<tr>";
	//買取番号
		print "<td width=100>".$row[0] ."</td>";
		print "<td>".$row[1] ."</td>";
		print "<td width=100>".Retkeiroec($row[2]) ."</td>";
		print "<td width=100>".Retsell_stat($row[3]) ."</td>";
		print "<td width=100>".Rettuutiec($row[4]) ."</td>";
		if ($row[2]=="1"){
			print "<td width=100>".Retsyouninec($row[5]) ."</td>";
		}else
		{
			print "<td width=100>　</td>";
		}
		print "</tr></table>";
		print "<HR>";
}
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
