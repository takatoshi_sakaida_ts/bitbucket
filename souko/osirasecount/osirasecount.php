<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>書籍・コミック　入荷お知らせメール登録データ抽出結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
$sql = 'select ';
$sql = $sql . 'g.instorecode, ';
$sql = $sql . 'g.JAN, ';
$sql = $sql . 'g.genre3code, ';
$sql = $sql . 'g.GOODS_NAME1, ';
$sql = $sql . 'g.GOODS_NAME3, ';
$sql = $sql . 'tourokusuu, ';
$sql = $sql . 'STOCK_NEW, ';
$sql = $sql . 'STOCK_USED, ';
$sql = $sql . 'MKT_PRICE, ';
$sql = $sql . 'SALE_PR_USED ';
$sql = $sql . 'from ';
$sql = $sql . '( ';
$sql = $sql . 'select ';
$sql = $sql . 'a.instorecode, ';
$sql = $sql . 'count(a.instorecode) as tourokusuu ';
$sql = $sql . 'from TALERTMAIL_ARRIVAL a,tgoods_bko g ';
$sql = $sql . 'where ';
$sql = $sql . 'a.instorecode=g.instorecode ';
$sql = $sql . "and a.stock_tp='1' ";
$sql = $sql . "and a.info_kind='1' ";
$sql = $sql . "and substr(g.genre3code,1,2) in ('12','11') ";
$sql = $sql . 'having count(a.instorecode)>9 ';
$sql = $sql . 'group by a.instorecode ';
$sql = $sql . ') c,tstock_bko s,tgoods_bko g ';
$sql = $sql . 'where ';
$sql = $sql . 'c.instorecode=s.instorecode ';
$sql = $sql . 'and c.instorecode=g.instorecode ';
$sql = $sql . 'order by g.instorecode';
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>入荷お知らせメール登録データ抽出結果(書籍・コミック)</strong> <br><br>\n<HR>";
$j=1;
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td></td><td nowrap>INSTORECODE</td><td nowrap width=120>JANコード</td>";
print "<td nowrap>GENRE3CODE</td>";
print "<td nowrap width=200>商品名</td><td nowrap width=150>シリーズ名</td>";
print "<td nowrap width=100>お知らせ<BR>メール登録数</td>";
print "<td nowrap >新古<BR>在庫数</td>";
print "<td nowrap >中古<BR>在庫数</td>";
print "<td nowrap >定価</td>";
print "<td nowrap >中古<BR>販売価格</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//インストアコード
	print "<td>" .$row[0] ."</td>";
//JANコード
	print "<td>".$row[1] ."</td>";
//ジャンル３コード
	print "<td>".$row[2] ."</td>";
//商品名
	print "<td>".$row[3] ."</td>";
//シリーズ名
	print "<td>".$row[4]."</td>";
//お知らせメール登録数
	print "<td align=right>".$row[5] ."</td>";
//新古在庫数
		print "<td>".$row[6]."</td>";
//中古在庫数
		print "<td>".$row[7]."</td>";
//定価
		print "<td>".$row[8]."</td>";
//中古販売価格
	print "<td>".$row[9]."</td>";
	print "</tr>";
	$j++;
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>