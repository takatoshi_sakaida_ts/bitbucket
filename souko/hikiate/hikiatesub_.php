<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<?PHP
require_once("../parts/pagechk.inc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>強制引当作業結果</title>
</head>
<body>
<?php
//**********************************************************************************************************
//ファイルの読み込み
//PEARの利用     -------(1)
//**********************************************************************************************************
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
// 前画面からの検索キーワードを取得する

//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//**********************************************************************************************************
/*
print $_SESSION["HSMAP"]["sql_d_reception_goods"];
print $_SESSION["HSMAP"]["sql_d_stock"];
print $_SESSION["HSMAP"]["sql_d_goods_stock"];
print $_SESSION["HSMAP"]["sql_d_order_goods"];
print $_SESSION["HSMAP"]["sql_d_reception_invoiceno"];
*/
//SQLの実行
$update_chk=0;
$db->autoCommit( false ); 
//d_reception_goods
$sql=$_SESSION["HSMAP"]["sql_d_reception_goods"];
$res2 = $db->query($sql);
if(DB::isError($res2)){
	print $res2->getMessage();
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
$update_chk=1;
}
output_log($sql,$_SESSION["user_id"],'hikiate_sql');
//d_stock
$sql=$_SESSION["HSMAP"]["sql_d_stock"];
$res2 = $db->query($sql);
if(DB::isError($res2)){
	print $res2->getMessage();
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
$update_chk=1;
}
output_log($sql,$_SESSION["user_id"],'hikiate_sql');
//d_goods_stock
$sql=$_SESSION["HSMAP"]["sql_d_goods_stock"];
$res2 = $db->query($sql);
if(DB::isError($res2)){
	print $res2->getMessage();
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
$update_chk=1;
}
output_log($sql,$_SESSION["user_id"],'hikiate_sql');
//d_order_goods
$sql=$_SESSION["HSMAP"]["sql_d_order_goods"];
$res2 = $db->query($sql);
if(DB::isError($res2)){
	print $res2->getMessage();
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
$update_chk=1;
}
output_log($sql,$_SESSION["user_id"],'hikiate_sql');
//d_reception_invoice
	if ($_SESSION["HSMAP"]["reception_chk"]==0)
	{
	$sql=$_SESSION["HSMAP"]["sql_d_reception_invoiceno"];
	$res2 = $db->query($sql);
	if(DB::isError($res2)){
		print $res2->getMessage();
		$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	$update_chk=1;
	}
	output_log($sql,$_SESSION["user_id"],'hikiate_sql');
}
if ($update_chk==0)
{
	$db->commit();
}else
{
	$db->rollback();
}
//EC側で他更新
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
$update_chk=0;
//データベースへの接続開始
$db2 = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db2)){
	echo "Fail\n" . DB::errorMessage($db2) . "\n";
}
$db2->autoCommit( false ); 
$res = $db2->query($_SESSION["HSMAP"]["sql_ec_upd"]);
if(DB::isError($res)){
	print $res->getMessage();
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		$update_chk=1;
}
output_log($_SESSION["HSMAP"]["sql_ec_upd"],$_SESSION["user_id"],'hikiate');
if ($update_chk==0)
{
	$db2->commit();
	$_SESSION["HSMAP"]["sql_ec"]="select ord_no,instorecode,stock_tp,GOODS_CNT,ORD_CNCL_CNT from torderdtl where ord_no='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."' and instorecode='". $_SESSION["HSMAP"]["INSTORECODE"]."'";
	$res = $db2->query($_SESSION["HSMAP"]["sql_ec"]);
	if(DB::isError($res)){
		print $res->getMessage();
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($_SESSION["HSMAP"]["sql_ec"],$_SESSION["user_id"],'hikiate');

	//ファイル出力準備
		$fp = fopen($_SESSION["HSMAP"]["file_name_torderdtl"], "a" );
		fputs($fp,"ord_no,instorecode,stock_tp,GOODS_CNT,ORD_CNCL_CNT");
		fputs($fp,"\n");

	while($row = $res->fetchRow()){
	$str="";
		print "<tr>";
		$ncols = $res->numCols();
		for($i=0;$i<$ncols;$i++){
				$str=$str . $row[$i] . ",";//行の列データを取得
		}
		print "</tr>";
	//ファイル出力
		$str = ereg_replace("\r|\n","",$str);
		fputs($fp,$str);
		fputs($fp,"\n");
	}
	fclose( $fp );
}else
{
	$db2->rollback();
}
$res->free();
$db2->disconnect();

//**********************************************************************************************************
$res = $db->query($_SESSION["HSMAP"]["sql_D_RECEPTION"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_RECEPTION"],$_SESSION["user_id"],'hikiate');
//検索結果の表示
print "<strong><BR>注文番号：". $_SESSION["HSMAP"]["RECEPTIONCODE"]."　　インストアコード：".$_SESSION["HSMAP"]["INSTORECODE"]."　stockno：".$_SESSION["HSMAP"]["setstockno"]."　ロケーション番号：".$_SESSION["HSMAP"]["setlocationcode"]."</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>注文詳細</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=120>receptioncode</td><td nowrap width=40>detailno</td><td nowrap width=100>stockno</td><td nowrap width=100>locationcode</td><td nowrap width=100>instorecode</td><td nowrap width=120>newused_kind</td>";
print "<td nowrap width=80>pick_kind</td><td nowrap width=80>cancel_kind</td><td nowrap width=80>adv_reserve_kind</td></tr>";
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_reception_goods"], "a" );
	fputs($fp,"RECEPTIONCODE,DETAILNO,STOCKNO,LOCATIONCODE,INSTORECODE,NEWUSED_KIND,PICKED_KIND,CANCEL_KIND,ADV_RESERVE_KIND");
	fputs($fp,"\n");
while($row = $res->fetchRow()){
	$str="";
	$warnmess="<br>";
	print "<tr>";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
}
print "</table>";
fclose( $fp );
$res->free();
//**********************************************************************************************************
//在庫テーブルチェック
//**********************************************************************************************************
$res = $db->query($_SESSION["HSMAP"]["sql_D_STOCK"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_STOCK"],$_SESSION["user_id"],'hikiate');
//検索結果の表示
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>在庫詳細</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=120>stockno</td><td nowrap width=40>stockdate</td><td nowrap width=100>locationcode</td><td nowrap width=100>instorecode</td><td nowrap width=120>newused_kind</td>";
print "<td nowrap width=80>STOCKRESERVE_KIND</td></tr>";
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_stock"], "a" );
	fputs($fp,"STOCKNO,STOCKDATE,LOCATIONCODE,INSTORECODE,NEWUSED_KIND,STOCKRESERVE_KIND");
	fputs($fp,"\n");
while($row = $res->fetchRow()){
	print "<tr>";
	$str="";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
}
fputs($fp,"select location:".$_SESSION["HSMAP"]["setlocationcode"]." select stockno:".$_SESSION["HSMAP"]["setstockno"]."\n");
print "</table>";
fclose( $fp );
//データの開放
$res->free();
//**********************************************************************************************************
//商品在庫テーブルチェック
//**********************************************************************************************************
$res = $db->query($_SESSION["HSMAP"]["sql_D_GOODS_STOCK"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_GOODS_STOCK"],$_SESSION["user_id"],'hikiate');
//検索結果の表示
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>商品在庫詳細</strong><HR>";
print "予定引当在庫　locationcode".$_SESSION["HSMAP"]["setlocationcode"]."　在庫番号".$_SESSION["HSMAP"]["setstockno"];
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=100>locationcode</td><td nowrap width=100>instorecode</td><td nowrap width=120>newused_kind</td>";
print "<td nowrap width=80>QTY</td><td nowrap width=80>RESERVE_QTY</td></tr>";
//項番
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_goods_stock"], "a" );
	fputs($fp,"LOCATIONCODE,INSTORECODE,NEWUSED_KIND,QTY,RESERVE_QTY");
	fputs($fp,"\n");
while($row = $res->fetchRow()){
	$str="";
	print "<tr>";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
}
print "</table>";
fclose( $fp );
//データの開放
$res->free();

//**********************************************************************************************************
//発注テーブルチェック
//**********************************************************************************************************
$res = $db->query($_SESSION["HSMAP"]["sql_D_ORDER_GOODS"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_ORDER_GOODS"],$_SESSION["user_id"],'hikiate');
//検索結果の表示
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_order_goods"], "a" );
	fputs($fp,$_SESSION["HSMAP"]["sql_D_ORDER_GOODS"]."\n");
while($row = $res->fetchRow()){
	$str="";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
}
fclose( $fp );
//データの開放
$res->free();
//**********************************************************************************************************
//納品書テーブルチェック
//**********************************************************************************************************
$res = $db->query($_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"],$_SESSION["user_id"],'hikiate');
//検索結果の表示
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	print "<strong><BR>納品書データ</strong><HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	//項目名の表示
	print "<td nowrap width=100>invoiceno</td><td nowrap width=100>ord_no</td><td nowrap width=100>zonecode</td><td nowrap width=100>modifydate</td><td nowrap width=100>modifytime</td></tr>";
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_reception_invoiceno"], "a" );
	fputs($fp,$_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"]."\n");
while($row = $res->fetchRow()){
	$str="";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
	print "</TR>";
}
print "</table>";
fclose( $fp );
//データの開放
$res->free();
$db->disconnect();
unset($_SESSION["HSMAP"]);
?>
<BR>
<FORM action="searchhikiate.php"><INPUT TYPE="submit" VALUE="戻る"></FORM>
</body>
</html>