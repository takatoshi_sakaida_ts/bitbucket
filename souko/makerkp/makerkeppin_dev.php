<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>メーカー欠品・入荷待ち検索結果_ECD</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = 'SELECT DISTINCT G.RECEPTIONCODE,G.ORDERCODE,G.ORDERDATE,DG.INSTORECODE,G.STATUS_KIND,';
$sql = $sql . 'D.RECEPTIONDATE,D.STATUS_KIND,DG.cancel_kind, r.answerdate_1 ';
$sql = $sql . 'FROM D_ORDER_GOODS G,D_RECEPTION D,D_RECEPTION_GOODS DG, D_ORDER_RESULT R WHERE ';
$sql = $sql . 'G.RECEPTIONCODE=DG.RECEPTIONCODE ';
$sql = $sql . 'AND G.DETAILNO=DG.DETAILNO ';
$sql = $sql . 'AND G.INSTORECODE=DG.INSTORECODE ';
$sql = $sql . 'AND DG.RECEPTIONCODE=D.RECEPTIONCODE ';
$sql = $sql . 'AND DG.INSTORECODE = R.INSTORECODE ';
$sql = $sql . 'AND G.ordercode = R.ordercode(+) ';
$sql = $sql . "AND G.STATUS_KIND IN ('04','05') ";
$sql = $sql . "AND D.STATUS_KIND IN ('00','01') ";
$sql = $sql . "AND substr(D.RECEPTIONCODE,1,6) >(select to_char(sysdate-120,'yyyymm') from dual)";
$sql = $sql . 'ORDER BY D.RECEPTIONDATE DESC';
//print $sql;


// 設定ファイル読込
$ini = parse_ini_file("../parts/db.ini",true);

// コネクション取得
$dsn_lg = "oci8://". $ini['logi_test']['usr'] . ":" . $ini['logi_test']['pwd'] . "@" . $ini['logi_test']['dbn'];
//データベースへの接続
$db = DB::connect($dsn_lg);
if(DB::isError($db)){
	die($db->getMessage());
}
$db->autoCommit(false);

$res = $db->query($sql);
if(DB::isError($res)){
    die($res->getMessage());
}
// $res = $db->query($sql);
// if(DB::isError($res)){
// 	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
// }

//検索結果の表示
print "<strong><BR>★★★ 検証用 ★★★ メーカー欠品・入荷待ち検索結果 ★★★ 検証用 ★★★</strong> <br><br>\n<HR>";
$j=1;
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td></td><td nowrap width=120>注文番号</td><td nowrap width=120>メーカ発注番号</td>";
print "<td nowrap width=120>メーカ発注日</td>";
print "<td nowrap width=120>インストアコード</td><td nowrap width=100>メーカー発注状態</td>";
print "<td nowrap width=100>注文受付日</td>";
print "<td nowrap width=100>回答日</td>";
print "<td nowrap width=100>注文状態</td>";
print "<td nowrap width=100>明細状態</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//注文番号
	print "<td><a href='../sorder/detailorder.php?receptioncode=" .$row[0] . "'>".$row[0] ."</td>";
//	print "<td>" .$row[21] . "<input type=hidden value=" . $row[21] . ">" ."</td>";
/*受付日
	print "<td>".Rethiduke($row[0]) ."</td>";
*/
//メーカー発注番号
	print "<td>".$row[1] ."</td>";
//メーカー発注日
	print "<td>".Rethiduke($row[2]) ."</td>";
//インストアコード
	print "<td align=right>".$row[3] ."</td>";
//メーカー発注状態
	print "<td align=right>".Retmakerstatuskind($row[4])."</td>";
//注文受付日
	print "<td align=right>".Rethiduke($row[5]) ."</td>";
//回答日
	print "<td align=right>".Rethiduke($row[8]) ."</td>";
//状態
		print "<td>".Retstatuskind($row[6])."</td>";
//キャンセル区分
	print "<td>".Retcancel($row[7])."</td>";
	print "</tr>";
/*
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
*/
//項目名の表示
//	print "<td nowrap width=120>運送会社</td><td nowrap width=200>出荷コード</td><td nowrap width=120>出荷登録日時</td></tr>";
//運送会社
//	print "<tr><td>".Retunsoukaisha($row[8])."</td>";
//出荷コード
//	print "<td>".Rethaisoubangou($row[9]) ."</td>";
//出荷日時
//	print "<td>".Rethidukejikan($row[11]) ."</td>";
//	print "</TR>";
//	print "<table border=1>\n";
//	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
/*
	print "<td nowrap width=120>送付先名</td><td nowrap width=500>送付先住所</td><td nowrap width=120>送付先連絡先</td></tr>";
	print "<tr><td>".$row[12].$row[13]."</td>";
	print "<td>".Retzip($row[14])."<BR>".$row[15].$row[16].$row[17].$row[18].$row[19]."</td>";
	print "<td>".Rettel($row[20])."</td>";
*/
	print "</TR>";
//	print "</table>";
//	print"<HR>";
	$j++;
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>