<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI1"]);
$keyti2 = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "インストアコードが入力されていません．<br>";
}

//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select a.instorecode,d.goodsname,a.costprice,a.newused_kind from d_goods_cost a,m_goods_display d ".
"where a.instorecode=d.instorecode and a.instorecode='".$keyti."' and newused_kind='".$keyti2."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>現在原価検索結果</strong><BR>インストアコード：". $keyti."　在庫区分：".Retzaikokubun($keyti2)."<br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td>インストアコード</td><td>商品名</td><td>原価</td><td>在庫区分</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>". $j."</td>";
//インストアコード
	print "<td>".$row[0]."</td>";
//商品名
	print "<td>".$row[1] ."</td>";
//原価
	print "<td>".$row[2] ."</td>";
//在庫区分
		print "<td>".Retzaikokubun($row[3])."</td>";
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>