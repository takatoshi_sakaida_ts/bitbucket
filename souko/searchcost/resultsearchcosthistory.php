<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI1"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "インストアコード／JANコードが入力されていません．<br>";
}

//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//価格履歴取得SQL
$sqlCost =
"select e.ecprice_kind, e.fromdate, e.ecprice ".
"from d_goods_ecprice e, m_goods_display d ".
"where (e.instorecode='".$keyti."' or d.jancode='".$keyti."')" .
"and e.instorecode=d.instorecode"." order by e.modifydate desc, e.ecprice_kind";
$resCost = $db->query($sqlCost);
if(DB::isError($resCost)){
	$resCost->DB_Error($resCost->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//タイトル取得SQL
$sqlTitle =
"select d.goodsname, d.goodsname_ext1 ".
"from m_goods_display d ".
"where (d.instorecode='".$keyti."' or d.jancode='".$keyti."')";
$resTitle = $db->query($sqlTitle);
if(DB::isError($resTitle)){
	$resTitle->DB_Error($resTitle->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>販売・買取価格履歴検索結果</strong><BR>インストアコード／JANコード：". $keyti."<br><br>\n";
print "<table border=1>\n";
//商品名の表示
while($title = $resTitle->fetchRow()){
	if (empty($title[1])) {
		print "".$title[0]."";
	} else {
		print "".$title[0]."(".$title[1].")";
	}
};
//項目名の表示
print "<tr>\n";
print "<TD></TD><td>区分名称</td><td>EC価格区分</td><td>適用開始日付</td><td>EC価格</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $resCost->fetchRow()){
	print "<tr><td>". $j."</td>";
	//区分名称
	print "<td>".Retecpricekind($row[0])."</td>";
	//EC価格区分
	print "<td>".$row[0] ."</td>";
	//適用開始日付
	print "<td>".$row[1] ."</td>";
	//EC価格
	print "<td>".$row[2]."</td>";
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$resTitle->free();
$resCost->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>