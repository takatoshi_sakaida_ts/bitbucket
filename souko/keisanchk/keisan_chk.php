<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>計算箱数チェック</title>
</head>
<body bgcolor="#ffffff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$keyti= date("Ymd",mktime(0, 0, 0, date("m"), date("d"),   date("Y")));
$sql = "select replace(dcode,'000200','Z200'),gqty,dqty,gqty+dqty,staff from ".
"(select asscode as dcode,sum(qty) as dqty from d_ass_discard ".
"where asscode in ".
"(select asscode ".
"from d_ass d ".
"where  ".
"d.operatedate='" . $keyti.
"' and d.box_recept_qty='1') ".
"group by asscode), ".
"(select asscode as gcode,sum(detailqty) gqty from d_ass_goods ".
"where asscode in  ".
"(select asscode ".
"from d_ass d ".
"where  ".
"d.operatedate='" .$keyti.
"' and d.box_recept_qty='1') ".
"group by asscode), ".
"(select asscode as ncode,operatestaffcode as staff from d_ass where operatedate ='".$keyti.
"' and box_recept_qty='1')".
"where dcode=gcode and dcode=ncode ".
"order by (gqty+dqty) desc";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>計算箱数チェック　". substr($keyti,0,4). "年".substr($keyti,4,2)."月".substr($keyti,6,2)."日"."</strong><BR>\n";
print "<strong><BR>計算箱数が１箱で登録されている買取一覧"."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap></td><td>買取番号</td><td>値段がついた点数</td><td>値段がつかない点数</td><td>合計点数</td><td>計算担当スタッフコード</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
	print "<td>". $j."</td>";
//買取番号
	print "<td>". $row[0]."</td>";
//アイテム数
	print "<td>".$row[1] ."</td>";
	print "<td>".$row[2] ."</td>";
	print "<td>".$row[3] ."</td>";
	print "<td>".$row[4] ."</td></tr>";
	$j++;
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>