<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>遅番用本日作業集計結果</title>
</head>
<body>
<?php
//途中のタイムアウト制限をなくす
set_time_limit(0); 
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$nowtime= date("H");
/*----------------------------------------------------------------------
部門マスタ取得
----------------------------------------------------------------------*/
$sql = "SELECT BUMONCATCODE,".
"BUMONCATNAME ".
"FROM M_BUMONCAT ".
"ORDER BY BUMONCATCODE";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

while($row = $res->fetchRow()){
	$temp_code[$row[0]+0]=$row[0];
	$temp_name[$row[0]+0]=$row[1];
	$temp_count[$row[0]+0]="0";
	$temp_countsi[$row[0]+0]="0";
	$temp_counts[$row[0]+0]="0";
	$temp_countss[$row[0]+0]="0";
}
$res->free();
/*----------------------------------------------------------------------
部門別中古棚入件数取得
----------------------------------------------------------------------*/
$sql = "SELECT G.BUMONCATCODE,".
"C.BUMONCATNAME,".
"COUNT(*) ".
"FROM D_TANA_GOODS D,M_GOODS G,M_BUMONCAT C ".
"WHERE D.MODIFYDATE=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) ".
"AND D.INSTORECODE=G.INSTORECODE ".
"AND G.BUMONCATCODE=C.BUMONCATCODE ".
"AND TANA_KIND IN ('51','52') ".
"GROUP BY G.BUMONCATCODE,C.BUMONCATNAME,G.BUMONCATCODE ".
"ORDER BY G.BUMONCATCODE";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$temp_count[$row[0]+0]=$row[2];
}
print "<strong>部門別棚入点数　". date("Y/m/d")."</strong>";
print "<HR><STRONG>中古</STRONG><table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap>部門コード</td><td nowrap>部門名</td><td nowrap>入庫点数</td></TR>";
print "<TR><TD>".$temp_code[101]."</TD><TD>".$temp_name[101]."</TD><TD>".$temp_count[101]."</TD></TR>";
print "<TR><TD>".$temp_code[102]."</TD><TD>".$temp_name[102]."</TD><TD>".$temp_count[102]."</TD></TR>";
print "<TR><TD>".$temp_code[103]."</TD><TD>".$temp_name[103]."</TD><TD>".$temp_count[103]."</TD></TR>";
print "<TR><TD>".$temp_code[104]."</TD><TD>".$temp_name[104]."</TD><TD>".$temp_count[104]."</TD></TR>";
print "<TR><TD>".$temp_code[121]."</TD><TD>".$temp_name[121]."</TD><TD>".$temp_count[121]."</TD></TR>";
print "<TR><TD>".$temp_code[131]."</TD><TD>".$temp_name[131]."</TD><TD>".$temp_count[131]."</TD></TR>";
print "<TR><TD>".$temp_code[141]."</TD><TD>".$temp_name[141]."</TD><TD>".$temp_count[141]."</TD></TR>";
print "<TR><TD>".$temp_code[111]."</TD><TD>".$temp_name[111]."</TD><TD>".$temp_count[111]."</TD></TR>";
print "<TR><TD>".$temp_code[112]."</TD><TD>".$temp_name[112]."</TD><TD>".$temp_count[112]."</TD></TR>";
print "<TR><TD>".$temp_code[113]."</TD><TD>".$temp_name[113]."</TD><TD>".$temp_count[113]."</TD></TR>";
print "<TR><TD>".$temp_code[150]."</TD><TD>".$temp_name[150]."</TD><TD>".$temp_count[150]."</TD></TR>";
print "<TR><TD>".$temp_code[160]."</TD><TD>".$temp_name[160]."</TD><TD>".$temp_count[160]."</TD></TR>";
print "<TR><TD>".$temp_code[220]."</TD><TD>".$temp_name[220]."</TD><TD>".$temp_count[220]."</TD></TR>";
print "<TR><TD>".$temp_code[190]."</TD><TD>".$temp_name[190]."</TD><TD>".$temp_count[190]."</TD></TR>";
print "<TR><TD>".$temp_code[170]."</TD><TD>".$temp_name[170]."</TD><TD>".$temp_count[170]."</TD></TR>";
print "</table>";
$res->free();
/*----------------------------------------------------------------------
部門別新古棚入件数取得
----------------------------------------------------------------------*/
$sql = "SELECT G.BUMONCATCODE,".
"C.BUMONCATNAME,".
"COUNT(*) ".
"FROM D_TANA_GOODS D,M_GOODS G,M_BUMONCAT C ".
"WHERE D.MODIFYDATE=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) ".
"AND D.INSTORECODE=G.INSTORECODE ".
"AND G.BUMONCATCODE=C.BUMONCATCODE ".
"AND TANA_KIND ='55' ".
"GROUP BY G.BUMONCATCODE,C.BUMONCATNAME,G.BUMONCATCODE ".
"ORDER BY G.BUMONCATCODE";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$temp_countsi[$row[0]+0]=$row[2];
}
print "<HR><STRONG>新古</STRONG><table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap>部門コード</td><td nowrap>部門名</td><td nowrap>入庫点数</td></TR>";
print "<TR><TD>".$temp_code[101]."</TD><TD>".$temp_name[101]."</TD><TD>".$temp_countsi[101]."</TD></TR>";
print "<TR><TD>".$temp_code[102]."</TD><TD>".$temp_name[102]."</TD><TD>".$temp_countsi[102]."</TD></TR>";
print "<TR><TD>".$temp_code[103]."</TD><TD>".$temp_name[103]."</TD><TD>".$temp_countsi[103]."</TD></TR>";
print "<TR><TD>".$temp_code[104]."</TD><TD>".$temp_name[104]."</TD><TD>".$temp_countsi[104]."</TD></TR>";
print "<TR><TD>".$temp_code[121]."</TD><TD>".$temp_name[121]."</TD><TD>".$temp_countsi[121]."</TD></TR>";
print "<TR><TD>".$temp_code[131]."</TD><TD>".$temp_name[131]."</TD><TD>".$temp_countsi[131]."</TD></TR>";
print "<TR><TD>".$temp_code[141]."</TD><TD>".$temp_name[141]."</TD><TD>".$temp_countsi[141]."</TD></TR>";
print "<TR><TD>".$temp_code[111]."</TD><TD>".$temp_name[111]."</TD><TD>".$temp_countsi[111]."</TD></TR>";
print "<TR><TD>".$temp_code[112]."</TD><TD>".$temp_name[112]."</TD><TD>".$temp_countsi[112]."</TD></TR>";
print "<TR><TD>".$temp_code[113]."</TD><TD>".$temp_name[113]."</TD><TD>".$temp_countsi[113]."</TD></TR>";
print "<TR><TD>".$temp_code[150]."</TD><TD>".$temp_name[150]."</TD><TD>".$temp_countsi[150]."</TD></TR>";
print "<TR><TD>".$temp_code[160]."</TD><TD>".$temp_name[160]."</TD><TD>".$temp_countsi[160]."</TD></TR>";
print "<TR><TD>".$temp_code[220]."</TD><TD>".$temp_name[220]."</TD><TD>".$temp_countsi[220]."</TD></TR>";
print "<TR><TD>".$temp_code[190]."</TD><TD>".$temp_name[190]."</TD><TD>".$temp_countsi[190]."</TD></TR>";
print "<TR><TD>".$temp_code[170]."</TD><TD>".$temp_name[170]."</TD><TD>".$temp_countsi[170]."</TD></TR>";
print "</table>";
$res->free();
/*----------------------------------------------------------------------
部門別出荷件数取得
----------------------------------------------------------------------*/
$sql = "SELECT M.BUMONCATCODE,".
"C.BUMONCATNAME,".
"COUNT(*) ".
"FROM D_RECEPTION_SHIPMENT S,D_RECEPTION_GOODS R,M_GOODS M,M_BUMONCAT C ".
"WHERE ".
"S.RECEPTIONCODE=R.RECEPTIONCODE ".
"AND R.INSTORECODE=M.INSTORECODE ".
"AND M.BUMONCATCODE=C.BUMONCATCODE ".
"AND CANCEL_KIND='0' and R.newused_kind='0' ".
"AND SUBSTR(DELIVERYDATETIME,1,8)=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) ".
"GROUP BY M.BUMONCATCODE,C.BUMONCATNAME";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
print "<HR><strong>部門別出荷点数　". date("Y/m/d")."</strong>";
print "<HR><STRONG>中古</STRONG><table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
while($row = $res->fetchRow()){
	$temp_counts[$row[0]+0]=$row[2];
}
print "<td nowrap>部門コード</td><td nowrap>部門名</td><td nowrap>出荷点数</td></TR>";
print "<TR><TD>".$temp_code[101]."</TD><TD>".$temp_name[101]."</TD><TD>".$temp_counts[101]."</TD></TR>";
print "<TR><TD>".$temp_code[102]."</TD><TD>".$temp_name[102]."</TD><TD>".$temp_counts[102]."</TD></TR>";
print "<TR><TD>".$temp_code[103]."</TD><TD>".$temp_name[103]."</TD><TD>".$temp_counts[103]."</TD></TR>";
print "<TR><TD>".$temp_code[104]."</TD><TD>".$temp_name[104]."</TD><TD>".$temp_counts[104]."</TD></TR>";
print "<TR><TD>".$temp_code[121]."</TD><TD>".$temp_name[121]."</TD><TD>".$temp_counts[121]."</TD></TR>";
print "<TR><TD>".$temp_code[131]."</TD><TD>".$temp_name[131]."</TD><TD>".$temp_counts[131]."</TD></TR>";
print "<TR><TD>".$temp_code[141]."</TD><TD>".$temp_name[141]."</TD><TD>".$temp_counts[141]."</TD></TR>";
print "<TR><TD>".$temp_code[111]."</TD><TD>".$temp_name[111]."</TD><TD>".$temp_counts[111]."</TD></TR>";
print "<TR><TD>".$temp_code[112]."</TD><TD>".$temp_name[112]."</TD><TD>".$temp_counts[112]."</TD></TR>";
print "<TR><TD>".$temp_code[113]."</TD><TD>".$temp_name[113]."</TD><TD>".$temp_counts[113]."</TD></TR>";
print "<TR><TD>".$temp_code[150]."</TD><TD>".$temp_name[150]."</TD><TD>".$temp_counts[150]."</TD></TR>";
print "<TR><TD>".$temp_code[160]."</TD><TD>".$temp_name[160]."</TD><TD>".$temp_counts[160]."</TD></TR>";
print "<TR><TD>".$temp_code[220]."</TD><TD>".$temp_name[220]."</TD><TD>".$temp_counts[220]."</TD></TR>";
print "<TR><TD>".$temp_code[190]."</TD><TD>".$temp_name[190]."</TD><TD>".$temp_counts[190]."</TD></TR>";
print "<TR><TD>".$temp_code[170]."</TD><TD>".$temp_name[170]."</TD><TD>".$temp_counts[170]."</TD></TR>";

print "</table>";
$res->free();
/*----------------------------------------------------------------------
部門別出荷件数取得
----------------------------------------------------------------------*/
$sql = "SELECT M.BUMONCATCODE,".
"C.BUMONCATNAME,".
"COUNT(*) ".
"FROM D_RECEPTION_SHIPMENT S,D_RECEPTION_GOODS R,M_GOODS M,M_BUMONCAT C ".
"WHERE ".
"S.RECEPTIONCODE=R.RECEPTIONCODE ".
"AND R.INSTORECODE=M.INSTORECODE ".
"AND M.BUMONCATCODE=C.BUMONCATCODE ".
"AND CANCEL_KIND='0' and R.newused_kind='1' ".
"AND SUBSTR(DELIVERYDATETIME,1,8)=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) ".
"GROUP BY M.BUMONCATCODE,C.BUMONCATNAME";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
print "<HR><strong>新古</strong>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
while($row = $res->fetchRow()){
	$temp_countss[$row[0]+0]=$row[2];
}
print "<td nowrap>部門コード</td><td nowrap>部門名</td><td nowrap>出荷点数</td></TR>";
print "<TR><TD>".$temp_code[101]."</TD><TD>".$temp_name[101]."</TD><TD>".$temp_countss[101]."</TD></TR>";
print "<TR><TD>".$temp_code[102]."</TD><TD>".$temp_name[102]."</TD><TD>".$temp_countss[102]."</TD></TR>";
print "<TR><TD>".$temp_code[103]."</TD><TD>".$temp_name[103]."</TD><TD>".$temp_countss[103]."</TD></TR>";
print "<TR><TD>".$temp_code[104]."</TD><TD>".$temp_name[104]."</TD><TD>".$temp_countss[104]."</TD></TR>";
print "<TR><TD>".$temp_code[121]."</TD><TD>".$temp_name[121]."</TD><TD>".$temp_countss[121]."</TD></TR>";
print "<TR><TD>".$temp_code[131]."</TD><TD>".$temp_name[131]."</TD><TD>".$temp_countss[131]."</TD></TR>";
print "<TR><TD>".$temp_code[141]."</TD><TD>".$temp_name[141]."</TD><TD>".$temp_countss[141]."</TD></TR>";
print "<TR><TD>".$temp_code[111]."</TD><TD>".$temp_name[111]."</TD><TD>".$temp_countss[111]."</TD></TR>";
print "<TR><TD>".$temp_code[112]."</TD><TD>".$temp_name[112]."</TD><TD>".$temp_countss[112]."</TD></TR>";
print "<TR><TD>".$temp_code[113]."</TD><TD>".$temp_name[113]."</TD><TD>".$temp_countss[113]."</TD></TR>";
print "<TR><TD>".$temp_code[150]."</TD><TD>".$temp_name[150]."</TD><TD>".$temp_countss[150]."</TD></TR>";
print "<TR><TD>".$temp_code[160]."</TD><TD>".$temp_name[160]."</TD><TD>".$temp_countss[160]."</TD></TR>";
print "<TR><TD>".$temp_code[220]."</TD><TD>".$temp_name[220]."</TD><TD>".$temp_countss[220]."</TD></TR>";
print "<TR><TD>".$temp_code[190]."</TD><TD>".$temp_name[190]."</TD><TD>".$temp_countss[190]."</TD></TR>";
print "<TR><TD>".$temp_code[170]."</TD><TD>".$temp_name[170]."</TD><TD>".$temp_countss[170]."</TD></TR>";

print "</table>";
$res->free();
/*----------------------------------------------------------------------
承認ベース行先集計
----------------------------------------------------------------------*/
$sql = "SELECT ".
"T.BUMONCODE, ".
"BUMONCATNAME, ".
"CASE  ".
"WHEN ROUTING_KIND='0' THEN 'オンライン行き' ".
"WHEN ROUTING_KIND='1' THEN 'オンライン行き' ".
"WHEN ROUTING_KIND='2' THEN 'ロジ行き' ".
"WHEN ROUTING_KIND='3' THEN 'マスタ有り行き' ".
"WHEN ROUTING_KIND='4' THEN 'マスタ無し行き' ".
"WHEN ROUTING_KIND='5' THEN '5円即D' ".
"END, ".
"COUNT(STOCKNO) ".
"FROM ".
"( ".
"SELECT DISTINCT ".
"G.B2B_SELLCODE, ".
"G.BUMONCODE, ".
"G.UNITPRICE, ".
"G.B2B_SELL_SEQ, ".
"G.STOCKNO, ".
"D.AUXCODE, ".
"S.INSTORECODE ".
"FROM D_B2B_SELL B,D_B2B_SELL_GOODS G,D_SLIP_GOODS S,D_SLIP D ".
"WHERE B.B2B_SELLCODE=G.B2B_SELLCODE ".
"AND G.STOCKNO=S.STOCKNO ".
"AND S.SLIPCODE=D.SLIPCODE ".
"AND B.STATUS_KIND='02' ".
"AND SUBSTR(B.OPERATEDATE,1,8) =(SELECT TO_CHAR(SYSDATE-1,'YYYYMMDD') FROM DUAL) ".
"AND SUBSTR(S.SLIPCODE,1,2) = '20') T, ".
"D_ASS_GOODS A,M_BUMONCAT B ".
"WHERE T.AUXCODE=A.ASSCODE ".
"AND T.INSTORECODE=A.INSTORECODE ".
"AND T.BUMONCODE=B.BUMONCATCODE ".
"GROUP BY T.BUMONCODE,BUMONCATNAME,ROUTING_KIND ".
"ORDER BY T.BUMONCODE,BUMONCATNAME,ROUTING_KIND";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
print "<HR><strong>承認ベース部門別仕入点数　". date("Y/m/d")."</strong>";
print "<HR><STRONG></STRONG><table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap>部門コード</td><td nowrap>部門名</td><td nowrap>行き先</td><td nowrap>仕入点数</td></TR>";
//PRINT $sql;
while($row = $res->fetchRow()){
print "<TR><TD>".$row[0]."</TD><TD>".$row[1]."</TD><TD>".$row[2]."</TD><TD>".$row[3]."</TD></TR>";
}
print "</table>";
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>