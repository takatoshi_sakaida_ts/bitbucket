<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>棚抜き作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "SELECT M.GENRE0CODE,D.NEWUSED_KIND,COUNT(D.STOCKNO) FROM D_STOCK D,M_GOODS M ".
"WHERE D.INSTORECODE=M.INSTORECODE AND D.MODIFYDATE=(SELECT TO_CHAR(SYSDATE-1,'YYYYMMDD') FROM DUAL) ".
"AND SUBSTR(D.LOCATIONCODE,1,1)<'9' ".
"AND D.STOCKRESERVE_KIND='1' ".
"GROUP BY M.GENRE0CODE,D.NEWUSED_KIND ".
"ORDER BY M.GENRE0CODE,D.NEWUSED_KIND";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>昨日の棚抜き作業状況</strong>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td align=right>カテゴリ名</td><td align=right>中古新古区分</td><td align=right>　　点数　　</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$i=0;
while($row = $res->fetchRow()){
	print "<tr>";
//
	print "<td align=right>".$row[0];
	print "</td>";
//
	print "<td align=right>".$row[1];
	print "</td>";
//件数
	print "<td align=right>".$row[2] ."</td>";
	print "</tr>";
$i=$i+$row[2];
}
print "<tr><td>合計</td><td>　</TD><TD align=right>".$i."</td></tr>";
print "</table>";
//データの開放
$res->free();
$sql = "SELECT M.GENRE0CODE,D.NEWUSED_KIND,COUNT(D.STOCKNO) FROM D_STOCK D,M_GOODS M ".
"WHERE D.INSTORECODE=M.INSTORECODE AND D.MODIFYDATE=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) ".
"AND SUBSTR(D.LOCATIONCODE,1,1)<'9' ".
"AND D.STOCKRESERVE_KIND='1' ".
"GROUP BY M.GENRE0CODE,D.NEWUSED_KIND ".
"ORDER BY M.GENRE0CODE,D.NEWUSED_KIND";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>今日の棚抜き作業状況</strong>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td align=right>カテゴリ名</td><td align=right>中古新古区分</td><td align=right>　　点数　　</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$i=0;
while($row = $res->fetchRow()){
	print "<tr>";
//
	print "<td align=right>".$row[0];
	print "</td>";
//
	print "<td align=right>".$row[1];
	print "</td>";
//件数
	print "<td align=right>".$row[2] ."</td>";
	print "</tr>";
$i=$i+$row[2];
}
print "<tr><td>合計</td><td>　</TD><TD align=right>".$i."</td></tr>";
print "</table>";
//データの開放
$res->free();

$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>