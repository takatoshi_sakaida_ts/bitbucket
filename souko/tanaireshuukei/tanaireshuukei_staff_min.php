<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>本日の棚入れ作業状況(ゾーン毎棚入れ集計)</title>
</head>
<body>
<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$nowtime= date("H");
$ScaleMinutes = 5;
$BuffMinutesMax = floor(60/$ScaleMinutes);

$sql = 
"select r.staffcode, f.staffname, r.tanairetime, r.tanaireminute, count(*), r.zonecode, r.newused_kind ".
"from ( ".
"select staffcode, substr(s.modifytime,1,2) as tanairetime, ".
"substr(s.modifytime,3,2) as tanaireminute, ".
"m.zonecode, ".
"s.newused_kind ".
"from ".
"d_tana_goods s, ".
"m_location m ".
"where substr(s.locationcode,1,1)<'9' and tana_kind in ('51','52','55') ".
"and s.locationcode = m.locationcode ".
"and s.modifydate=(select to_char(sysdate,'yyyymmdd') from dual) ".
") r, ".
"m_staff f ".
"where r.staffcode = f.staffcode ".
"group by r.newused_kind, r.tanairetime, r.tanaireminute, r.zonecode, r.staffcode, f.staffname ".
"order by r.staffcode, f.staffname, r.tanairetime,r.tanaireminute,r.zonecode";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>　<BR>本日の棚入れ作業状況（スタッフ・".$ScaleMinutes."分毎集計）</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td colspan=6></td>";
for ($c = 9; $c <= $nowtime; $c++) {
	print "<td colspan=12>".$c."時</td>";
}
print "</tr><tr bgcolor=#ccffff>\n";
print "<td nowrap>スタッフコード</td><td nowrap>スタッフ名</td><td nowrap>総計</td><td nowrap>ゾーン</td><td nowrap>新古区分</td><td nowrap>合計</td>";
for ($c = 9; $c <= $nowtime; $c++) {
	print "<td nowrap>00-04</td><td nowrap>05-09</td><td nowrap>10-14</td><td nowrap>15-19</td><td nowrap>20-24</td><td nowrap>25-29</td><td nowrap>30-34</td><td nowrap>35-39</td><td nowrap>40-44</td><td nowrap>45-49</td><td nowrap>50-54</td><td nowrap>55-59</td>";
}
print "</tr>";
$total_count=0;
$i=0;
$motostaffcode="0";
for($cc = 0; $cc <= 23; $cc++){
	for($min = 0; $min <$BuffMinutesMax; $min++){
		$jikantotal[$cc][$min]=0;
	}
}
//取得データのセット
while($row = $res->fetchRow()){
	$total_count=$total_count+$row[4];
	if ($motostaffcode==($row[0]+0)){
	}else{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$staffzonecnt_a[$i]=0;
		$staffzonecnt_b[$i]=0;
		$staffzonecnt_c[$i]=0;
		$staffzonecnt_d[$i]=0;
		$staffzonecnt_e[$i]=0;
		$staffzonecnt_g[$i]=0;
		$staffzonecnt_h[$i]=0;
		$staffzonecnt_i[$i]=0;
		$staffzonecnt_j[$i]=0;
		$staffzonecnt_k[$i]=0;
		$staffzonecnt_l[$i]=0;
		$staffzonecnt[$i]=0;
		$stafftotal[$i]=0;
		$staffzonetotal_a_old[$i]=0;
		$staffzonetotal_a_new[$i]=0;
		$staffzonetotal_b_old[$i]=0;
		$staffzonetotal_b_new[$i]=0;
		$staffzonetotal_c_old[$i]=0;
		$staffzonetotal_c_new[$i]=0;
		$staffzonetotal_d_old[$i]=0;
		$staffzonetotal_d_new[$i]=0;
		$staffzonetotal_e_old[$i]=0;
		$staffzonetotal_e_new[$i]=0;
		$staffzonetotal_g_old[$i]=0;
		$staffzonetotal_g_new[$i]=0;
		$staffzonetotal_h_old[$i]=0;
		$staffzonetotal_h_new[$i]=0;
		$staffzonetotal_i_old[$i]=0;
		$staffzonetotal_i_new[$i]=0;
		$staffzonetotal_j_old[$i]=0;
		$staffzonetotal_j_new[$i]=0;
		$staffzonetotal_k_old[$i]=0;
		$staffzonetotal_k_new[$i]=0;
		$staffzonetotal_l_old[$i]=0;
		$staffzonetotal_l_new[$i]=0;
		//集計値初期化
		for($cc = 0; $cc <= 23; $cc++) {
			for($min = 0; $min <$BuffMinutesMax; $min++){
				$forhtml_a_old[($i)][($cc)][$min]=0;
				$forhtml_a_new[($i)][($cc)][$min]=0;
				$forhtml_b_old[($i)][($cc)][$min]=0;
				$forhtml_b_new[($i)][($cc)][$min]=0;
				$forhtml_c_old[($i)][($cc)][$min]=0;
				$forhtml_c_new[($i)][($cc)][$min]=0;
				$forhtml_d_old[($i)][($cc)][$min]=0;
				$forhtml_d_new[($i)][($cc)][$min]=0;
				$forhtml_e_old[($i)][($cc)][$min]=0;
				$forhtml_e_new[($i)][($cc)][$min]=0;
				$forhtml_g_old[($i)][($cc)][$min]=0;
				$forhtml_g_new[($i)][($cc)][$min]=0;
				$forhtml_h_old[($i)][($cc)][$min]=0;
				$forhtml_h_new[($i)][($cc)][$min]=0;
				$forhtml_i_old[($i)][($cc)][$min]=0;
				$forhtml_i_new[($i)][($cc)][$min]=0;
				$forhtml_j_old[($i)][($cc)][$min]=0;
				$forhtml_j_new[($i)][($cc)][$min]=0;
				$forhtml_k_old[($i)][($cc)][$min]=0;
				$forhtml_k_new[($i)][($cc)][$min]=0;
				$forhtml_l_old[($i)][($cc)][$min]=0;
				$forhtml_l_new[($i)][($cc)][$min]=0;
			}
		}
	}

	//数値に変換するために+0してます
	switch ($row[5]){
	case "A";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_a_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_a_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_a_old[$i]=$staffzonetotal_a_old[$i]+$row[4];
		}else{
			$forhtml_a_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_a_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_a_new[$i]=$staffzonetotal_a_new[$i]+$row[4];
		}
		$staffzonecnt_a[$i]=1;
		break;
	case "B";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_b_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_b_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_b_old[$i]=$staffzonetotal_b_old[$i]+$row[4];
		}else{
			$forhtml_b_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_b_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_b_new[$i]=$staffzonetotal_b_new[$i]+$row[4];
		}
		$staffzonecnt_b[$i]=1;
		break;
	case "C";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_c_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_c_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_c_old[$i]=$staffzonetotal_c_old[$i]+$row[4];
		}else{
			$forhtml_c_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_c_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_c_new[$i]=$staffzonetotal_c_new[$i]+$row[4];
		}
		$staffzonecnt_c[$i]=1;
		break;
	case "D";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_d_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_d_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_d_old[$i]=$staffzonetotal_d_old[$i]+$row[4];
		}else{
			$forhtml_d_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_d_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_d_new[$i]=$staffzonetotal_d_new[$i]+$row[4];
		}
		$staffzonecnt_d[$i]=1;
		break;
	case "E";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_e_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_e_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_e_old[$i]=$staffzonetotal_e_old[$i]+$row[4];
		}else{
			$forhtml_e_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_e_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_e_new[$i]=$staffzonetotal_e_new[$i]+$row[4];
		}
		$staffzonecnt_e[$i]=1;
		break;
	case "G";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_g_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_g_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_g_old[$i]=$staffzonetotal_g_old[$i]+$row[4];
		}else{
			$forhtml_g_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_g_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_g_new[$i]=$staffzonetotal_g_new[$i]+$row[4];
		}
		$staffzonecnt_g[$i]=1;
		break;
	case "H";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_h_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_h_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_h_old[$i]=$staffzonetotal_h_old[$i]+$row[4];
		}else{
			$forhtml_h_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_h_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_h_new[$i]=$staffzonetotal_h_new[$i]+$row[4];
		}
		$staffzonecnt_h[$i]=1;
		break;
	case "I";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_i_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_i_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_i_old[$i]=$staffzonetotal_i_old[$i]+$row[4];
		}else{
			$forhtml_i_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_i_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_i_new[$i]=$staffzonetotal_i_new[$i]+$row[4];
		}
		$staffzonecnt_i[$i]=1;
		break;
	case "J";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_j_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_j_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_j_old[$i]=$staffzonetotal_j_old[$i]+$row[4];
		}else{
			$forhtml_j_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_j_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_j_new[$i]=$staffzonetotal_j_new[$i]+$row[4];
		}
		$staffzonecnt_j[$i]=1;
		break;
	case "K";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_k_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_k_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_k_old[$i]=$staffzonetotal_k_old[$i]+$row[4];
		}else{
			$forhtml_k_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_k_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_k_new[$i]=$staffzonetotal_k_new[$i]+$row[4];
		}
		$staffzonecnt_k[$i]=1;
		break;
	case "L";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_l_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_l_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_l_old[$i]=$staffzonetotal_l_old[$i]+$row[4];
		}else{
			$forhtml_l_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_l_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_l_new[$i]=$staffzonetotal_l_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	}
	$stafftotal[$i]=$stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
}
//HTML表示
for($ii = 1; $ii <= $i; $ii++) {
	$headerflg=0;
	$staffzonecnt[$ii]=$staffzonecnt_a[$ii]+$staffzonecnt_b[$ii]+$staffzonecnt_c[$ii]+$staffzonecnt_d[$ii]+$staffzonecnt_e[$ii]+$staffzonecnt_g[$ii]+$staffzonecnt_h[$ii]+$staffzonecnt_i[$ii]+$staffzonecnt_j[$ii]+$staffzonecnt_k[$ii]+$staffzonecnt_l[$ii];
	//Azone(old)
	if ($staffzonetotal_a_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>A</td><td>中古</td><td align=right>".$staffzonetotal_a_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_a_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Azone(new)
	if ($staffzonetotal_a_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>A</td><td>新古</td><td align=right>".$staffzonetotal_a_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_a_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Bzone(old)
	if ($staffzonetotal_b_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>B</td><td>中古</td><td align=right>".$staffzonetotal_b_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_b_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Bzone(new)
	if ($staffzonetotal_b_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>B</td><td>新古</td><td align=right>".$staffzonetotal_b_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_b_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Czone(old)
	if ($staffzonetotal_c_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>C</td><td>中古</td><td align=right>".$staffzonetotal_c_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_c_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Czone(new)
	if ($staffzonetotal_c_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>C</td><td>新古</td><td align=right>".$staffzonetotal_c_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_c_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Dzone(old)
	if ($staffzonetotal_d_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>D</td><td>中古</td><td align=right>".$staffzonetotal_d_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_d_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Dzone(new)
	if ($staffzonetotal_d_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>D</td><td>新古</td><td align=right>".$staffzonetotal_d_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_d_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ezone(old)
	if ($staffzonetotal_e_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>E</td><td>中古</td><td align=right>".$staffzonetotal_e_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_e_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ezone(new)
	if ($staffzonetotal_e_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>E</td><td>新古</td><td align=right>".$staffzonetotal_e_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_e_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Gzone(old)
	if ($staffzonetotal_g_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>G</td><td>中古</td><td align=right>".$staffzonetotal_g_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_g_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Gzone(new)
	if ($staffzonetotal_g_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>G</td><td>新古</td><td align=right>".$staffzonetotal_g_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_g_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Hzone(old)
	if ($staffzonetotal_h_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>H</td><td>中古</td><td align=right>".$staffzonetotal_h_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_h_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Hzone(new)
	if ($staffzonetotal_h_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>H</td><td>新古</td><td align=right>".$staffzonetotal_h_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_h_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Izone(old)
	if ($staffzonetotal_i_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>I</td><td>中古</td><td align=right>".$staffzonetotal_i_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_i_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Izone(new)
	if ($staffzonetotal_i_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>I</td><td>新古</td><td align=right>".$staffzonetotal_i_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_i_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Jzone(old)
	if ($staffzonetotal_j_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>J</td><td>中古</td><td align=right>".$staffzonetotal_j_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_j_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Jzone(new)
	if ($staffzonetotal_j_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>J</td><td>新古</td><td align=right>".$staffzonetotal_j_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_j_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Kzone(old)
	if ($staffzonetotal_k_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>K</td><td>中古</td><td align=right>".$staffzonetotal_k_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_k_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Kzone(new)
	if ($staffzonetotal_k_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>K</td><td>新古</td><td align=right>".$staffzonetotal_k_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_k_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Lzone(old)
	if ($staffzonetotal_l_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>L</td><td>中古</td><td align=right>".$staffzonetotal_l_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_l_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
}
print "<tr><td colspan=5 align=center>棚入れ点数合計</td><td align=right>".$total_count."</td>";
for ($c = 9; $c <= $nowtime; $c++) {
	for($min = 0; $min < $BuffMinutesMax; $min++){
		print "<td align=right>".$jikantotal[$c][$min]."</td>";
	}
}
print "</tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>