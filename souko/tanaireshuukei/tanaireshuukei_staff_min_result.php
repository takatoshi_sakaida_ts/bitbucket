<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>棚入れ作業状況(ゾーン毎棚入れ集計)</title>
</head>
<body>
<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
$date = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($date)) {
	$date = date('Ymd');
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$starttime    = 9;
$nowtime      = 23;
$calcdate     = 0;
$ScaleMinutes = 5;
$BuffMinutesMax = floor(60/$ScaleMinutes);

$sql = 
"select r.staffcode, f.staffname, r.tanairetime, r.tanaireminute, count(*), r.zonecode, r.newused_kind ".
"from ( ".
"select staffcode, substr(s.modifytime,1,2) as tanairetime, ".
"substr(s.modifytime,3,2) as tanaireminute, ".
"m.zonecode, ".
"s.newused_kind ".
"from ".
"d_tana_goods s, ".
"m_location m ".
"where substr(s.locationcode,1,1)<'9' and tana_kind in ('51','52','55') ".
"and s.locationcode = m.locationcode ".
"and s.modifydate=$date ".
") r, ".
"m_staff f ".
"where r.staffcode = f.staffcode ".
"group by r.newused_kind, r.tanairetime, r.tanaireminute, r.zonecode, r.staffcode, f.staffname ".
"order by r.staffcode, f.staffname, r.tanairetime,r.tanaireminute,r.zonecode";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>　<BR>棚入れ作業状況（スタッフ・".$ScaleMinutes."分毎集計）  ".$date."</strong><br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td colspan=7></td>";
for ($c = $starttime; $c <= $nowtime; $c++) {
	print "<td colspan=12>".$c."時</td>";
}
print "</tr><tr bgcolor=#ccffff>\n";
print "<td nowrap>スタッフコード</td>";
print "<td nowrap>スタッフ名</td>";
print "<td nowrap>総計</td>";
print "<td nowrap>ゾーン</td>";
print "<td nowrap>新古区分</td>";
print "<td nowrap>合計</td>";
for ($c = 9; $c <= $nowtime; $c++) {
	print "<td nowrap>00-04</td><td nowrap>05-09</td><td nowrap>10-14</td><td nowrap>15-19</td><td nowrap>20-24</td><td nowrap>25-29</td><td nowrap>30-34</td><td nowrap>35-39</td><td nowrap>40-44</td><td nowrap>45-49</td><td nowrap>50-54</td><td nowrap>55-59</td>";
}
print "</tr>";
$total_count=0;
$i=0;
$motostaffcode="0";
for($cc = 0; $cc <= 23; $cc++){
	for($min = 0; $min <$BuffMinutesMax; $min++){
		$jikantotal[$cc][$min]=0;
	}
}
//取得データのセット
while($row = $res->fetchRow()){
	$total_count=$total_count+$row[4];
	if ($motostaffcode==($row[0]+0)){
	}else{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$staffzonecnt_a[$i]=0;
		$staffzonecnt_b[$i]=0;
		$staffzonecnt_c[$i]=0;
		$staffzonecnt_d[$i]=0;
		$staffzonecnt_e[$i]=0;
		$staffzonecnt_g[$i]=0;
		$staffzonecnt_h[$i]=0;
		$staffzonecnt_i[$i]=0;
		$staffzonecnt_j[$i]=0;
		$staffzonecnt_k[$i]=0;
		$staffzonecnt_l[$i]=0;
		$staffzonecnt_m[$i]=0;
		$staffzonecnt_n[$i]=0;
		$staffzonecnt_o[$i]=0;
		$staffzonecnt_p[$i]=0;
		$staffzonecnt_q[$i]=0;
		$staffzonecnt[$i]=0;
		$stafftotal[$i]=0;
		$staffzonetotal_a_old[$i]=0;
		$staffzonetotal_a_new[$i]=0;
		$staffzonetotal_b_old[$i]=0;
		$staffzonetotal_b_new[$i]=0;
		$staffzonetotal_c_old[$i]=0;
		$staffzonetotal_c_new[$i]=0;
		$staffzonetotal_d_old[$i]=0;
		$staffzonetotal_d_new[$i]=0;
		$staffzonetotal_e_old[$i]=0;
		$staffzonetotal_e_new[$i]=0;
		$staffzonetotal_g_old[$i]=0;
		$staffzonetotal_g_new[$i]=0;
		$staffzonetotal_h_old[$i]=0;
		$staffzonetotal_h_new[$i]=0;
		$staffzonetotal_i_old[$i]=0;
		$staffzonetotal_i_new[$i]=0;
		$staffzonetotal_j_old[$i]=0;
		$staffzonetotal_j_new[$i]=0;
		$staffzonetotal_k_old[$i]=0;
		$staffzonetotal_k_new[$i]=0;
		$staffzonetotal_l_old[$i]=0;
		$staffzonetotal_l_new[$i]=0;
		$staffzonetotal_m_old[$i]=0;
		$staffzonetotal_m_new[$i]=0;
		$staffzonetotal_n_old[$i]=0;
		$staffzonetotal_n_new[$i]=0;
		$staffzonetotal_o_old[$i]=0;
		$staffzonetotal_o_new[$i]=0;
		$staffzonetotal_p_old[$i]=0;
		$staffzonetotal_p_new[$i]=0;
		$staffzonetotal_q_old[$i]=0;
		$staffzonetotal_q_new[$i]=0;
		//集計値初期化
		for($cc = 0; $cc <= 23; $cc++) {
			for($min = 0; $min <$BuffMinutesMax; $min++){
				$forhtml_a_old[($i)][($cc)][$min]=0;
				$forhtml_a_new[($i)][($cc)][$min]=0;
				$forhtml_b_old[($i)][($cc)][$min]=0;
				$forhtml_b_new[($i)][($cc)][$min]=0;
				$forhtml_c_old[($i)][($cc)][$min]=0;
				$forhtml_c_new[($i)][($cc)][$min]=0;
				$forhtml_d_old[($i)][($cc)][$min]=0;
				$forhtml_d_new[($i)][($cc)][$min]=0;
				$forhtml_e_old[($i)][($cc)][$min]=0;
				$forhtml_e_new[($i)][($cc)][$min]=0;
				$forhtml_g_old[($i)][($cc)][$min]=0;
				$forhtml_g_new[($i)][($cc)][$min]=0;
				$forhtml_h_old[($i)][($cc)][$min]=0;
				$forhtml_h_new[($i)][($cc)][$min]=0;
				$forhtml_i_old[($i)][($cc)][$min]=0;
				$forhtml_i_new[($i)][($cc)][$min]=0;
				$forhtml_j_old[($i)][($cc)][$min]=0;
				$forhtml_j_new[($i)][($cc)][$min]=0;
				$forhtml_k_old[($i)][($cc)][$min]=0;
				$forhtml_k_new[($i)][($cc)][$min]=0;
				$forhtml_l_old[($i)][($cc)][$min]=0;
				$forhtml_l_new[($i)][($cc)][$min]=0;
				$forhtml_m_old[($i)][($cc)][$min]=0;
				$forhtml_m_new[($i)][($cc)][$min]=0;
				$forhtml_n_old[($i)][($cc)][$min]=0;
				$forhtml_n_new[($i)][($cc)][$min]=0;
				$forhtml_o_old[($i)][($cc)][$min]=0;
				$forhtml_o_new[($i)][($cc)][$min]=0;
				$forhtml_p_old[($i)][($cc)][$min]=0;
				$forhtml_p_new[($i)][($cc)][$min]=0;
				$forhtml_q_old[($i)][($cc)][$min]=0;
				$forhtml_q_new[($i)][($cc)][$min]=0;
			}
		}
	}

	//数値に変換するために+0してます
	switch ($row[5]){
	case "A";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_a_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_a_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_a_old[$i]=$staffzonetotal_a_old[$i]+$row[4];
		}else{
			$forhtml_a_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_a_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_a_new[$i]=$staffzonetotal_a_new[$i]+$row[4];
		}
		$staffzonecnt_a[$i]=1;
		break;
	case "B";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_b_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_b_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_b_old[$i]=$staffzonetotal_b_old[$i]+$row[4];
		}else{
			$forhtml_b_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_b_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_b_new[$i]=$staffzonetotal_b_new[$i]+$row[4];
		}
		$staffzonecnt_b[$i]=1;
		break;
	case "C";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_c_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_c_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_c_old[$i]=$staffzonetotal_c_old[$i]+$row[4];
		}else{
			$forhtml_c_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_c_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_c_new[$i]=$staffzonetotal_c_new[$i]+$row[4];
		}
		$staffzonecnt_c[$i]=1;
		break;
	case "D";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_d_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_d_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_d_old[$i]=$staffzonetotal_d_old[$i]+$row[4];
		}else{
			$forhtml_d_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_d_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_d_new[$i]=$staffzonetotal_d_new[$i]+$row[4];
		}
		$staffzonecnt_d[$i]=1;
		break;
	case "E";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_e_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_e_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_e_old[$i]=$staffzonetotal_e_old[$i]+$row[4];
		}else{
			$forhtml_e_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_e_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_e_new[$i]=$staffzonetotal_e_new[$i]+$row[4];
		}
		$staffzonecnt_e[$i]=1;
		break;
	case "G";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_g_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_g_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_g_old[$i]=$staffzonetotal_g_old[$i]+$row[4];
		}else{
			$forhtml_g_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_g_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_g_new[$i]=$staffzonetotal_g_new[$i]+$row[4];
		}
		$staffzonecnt_g[$i]=1;
		break;
	case "H";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_h_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_h_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_h_old[$i]=$staffzonetotal_h_old[$i]+$row[4];
		}else{
			$forhtml_h_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_h_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_h_new[$i]=$staffzonetotal_h_new[$i]+$row[4];
		}
		$staffzonecnt_h[$i]=1;
		break;
	case "I";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_i_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_i_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_i_old[$i]=$staffzonetotal_i_old[$i]+$row[4];
		}else{
			$forhtml_i_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_i_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_i_new[$i]=$staffzonetotal_i_new[$i]+$row[4];
		}
		$staffzonecnt_i[$i]=1;
		break;
	case "J";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_j_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_j_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_j_old[$i]=$staffzonetotal_j_old[$i]+$row[4];
		}else{
			$forhtml_j_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_j_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_j_new[$i]=$staffzonetotal_j_new[$i]+$row[4];
		}
		$staffzonecnt_j[$i]=1;
		break;
	case "K";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_k_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_k_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_k_old[$i]=$staffzonetotal_k_old[$i]+$row[4];
		}else{
			$forhtml_k_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_k_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_k_new[$i]=$staffzonetotal_k_new[$i]+$row[4];
		}
		$staffzonecnt_k[$i]=1;
		break;
	case "L";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_l_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_l_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_l_old[$i]=$staffzonetotal_l_old[$i]+$row[4];
		}else{
			$forhtml_l_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_l_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_l_new[$i]=$staffzonetotal_l_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	case "M";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_m_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_m_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_m_old[$i]=$staffzonetotal_m_old[$i]+$row[4];
		}else{
			$forhtml_m_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_m_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_m_new[$i]=$staffzonetotal_m_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	case "N";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_n_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_n_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_n_old[$i]=$staffzonetotal_n_old[$i]+$row[4];
		}else{
			$forhtml_n_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_n_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_n_new[$i]=$staffzonetotal_n_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	case "O";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_o_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_o_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_o_old[$i]=$staffzonetotal_o_old[$i]+$row[4];
		}else{
			$forhtml_o_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_o_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_o_new[$i]=$staffzonetotal_o_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	case "P";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_p_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_p_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_p_old[$i]=$staffzonetotal_p_old[$i]+$row[4];
		}else{
			$forhtml_p_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_p_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_p_new[$i]=$staffzonetotal_p_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	case "Q";
		//staffcode,時間,分
		if($row[6] == 0){
			$forhtml_q_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_q_old[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_q_old[$i]=$staffzonetotal_q_old[$i]+$row[4];
		}else{
			$forhtml_q_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_q_new[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
			$staffzonetotal_q_new[$i]=$staffzonetotal_q_new[$i]+$row[4];
		}
		$staffzonecnt_l[$i]=1;
		break;
	}
	$stafftotal[$i]=$stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
}
//HTML表示
for($ii = 1; $ii <= $i; $ii++) {
	$headerflg=0;
	$staffzonecnt[$ii]=                   $staffzonecnt_a[$ii]+$staffzonecnt_b[$ii]+$staffzonecnt_c[$ii]+$staffzonecnt_d[$ii]+$staffzonecnt_e[$ii]+$staffzonecnt_g[$ii];
	$staffzonecnt[$ii]=$staffzonecnt[$ii]+$staffzonecnt_h[$ii]+$staffzonecnt_i[$ii]+$staffzonecnt_j[$ii]+$staffzonecnt_k[$ii]+$staffzonecnt_l[$ii];
	$staffzonecnt[$ii]=$staffzonecnt[$ii]+$staffzonecnt_m[$ii]+$staffzonecnt_n[$ii]+$staffzonecnt_o[$ii]+$staffzonecnt_p[$ii]+$staffzonecnt_q[$ii];
	//Azone(old)
	if ($staffzonetotal_a_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>A</td><td>中古</td><td align=right>".$staffzonetotal_a_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_a_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Azone(new)
	if ($staffzonetotal_a_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>A</td><td>新古</td><td align=right>".$staffzonetotal_a_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_a_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Bzone(old)
	if ($staffzonetotal_b_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>B</td><td>中古</td><td align=right>".$staffzonetotal_b_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_b_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Bzone(new)
	if ($staffzonetotal_b_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>B</td><td>新古</td><td align=right>".$staffzonetotal_b_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_b_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Czone(old)
	if ($staffzonetotal_c_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>C</td><td>中古</td><td align=right>".$staffzonetotal_c_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_c_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Czone(new)
	if ($staffzonetotal_c_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>C</td><td>新古</td><td align=right>".$staffzonetotal_c_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_c_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Dzone(old)
	if ($staffzonetotal_d_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>D</td><td>中古</td><td align=right>".$staffzonetotal_d_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_d_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Dzone(new)
	if ($staffzonetotal_d_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>D</td><td>新古</td><td align=right>".$staffzonetotal_d_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_d_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ezone(old)
	if ($staffzonetotal_e_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>E</td><td>中古</td><td align=right>".$staffzonetotal_e_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_e_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ezone(new)
	if ($staffzonetotal_e_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>E</td><td>新古</td><td align=right>".$staffzonetotal_e_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_e_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Gzone(old)
	if ($staffzonetotal_g_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>G</td><td>中古</td><td align=right>".$staffzonetotal_g_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_g_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Gzone(new)
	if ($staffzonetotal_g_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>G</td><td>新古</td><td align=right>".$staffzonetotal_g_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_g_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Hzone(old)
	if ($staffzonetotal_h_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>H</td><td>中古</td><td align=right>".$staffzonetotal_h_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_h_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Hzone(new)
	if ($staffzonetotal_h_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>H</td><td>新古</td><td align=right>".$staffzonetotal_h_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_h_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Izone(old)
	if ($staffzonetotal_i_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>I</td><td>中古</td><td align=right>".$staffzonetotal_i_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_i_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Izone(new)
	if ($staffzonetotal_i_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>I</td><td>新古</td><td align=right>".$staffzonetotal_i_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_i_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Jzone(old)
	if ($staffzonetotal_j_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>J</td><td>中古</td><td align=right>".$staffzonetotal_j_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_j_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Jzone(new)
	if ($staffzonetotal_j_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>J</td><td>新古</td><td align=right>".$staffzonetotal_j_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_j_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Kzone(old)
	if ($staffzonetotal_k_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>K</td><td>中古</td><td align=right>".$staffzonetotal_k_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_k_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Kzone(new)
	if ($staffzonetotal_k_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>K</td><td>新古</td><td align=right>".$staffzonetotal_k_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_k_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Lzone(old)
	if ($staffzonetotal_l_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>L</td><td>中古</td><td align=right>".$staffzonetotal_l_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_l_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Lzone(new)
	if ($staffzonetotal_l_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>L</td><td>新古</td><td align=right>".$staffzonetotal_l_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_l_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Mzone(old)
	if ($staffzonetotal_m_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>M</td><td>中古</td><td align=right>".$staffzonetotal_m_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_m_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Mzone(new)
	if ($staffzonetotal_m_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>M</td><td>新古</td><td align=right>".$staffzonetotal_m_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_m_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Nzone(old)
	if ($staffzonetotal_n_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>N</td><td>中古</td><td align=right>".$staffzonetotal_n_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_n_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Nzone(new)
	if ($staffzonetotal_n_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>N</td><td>新古</td><td align=right>".$staffzonetotal_n_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_n_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ozone(old)
	if ($staffzonetotal_o_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>O</td><td>中古</td><td align=right>".$staffzonetotal_o_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_o_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ozone(new)
	if ($staffzonetotal_o_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>O</td><td>新古</td><td align=right>".$staffzonetotal_o_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_o_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Pzone(old)
	if ($staffzonetotal_p_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>P</td><td>中古</td><td align=right>".$staffzonetotal_p_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_p_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Pzone(new)
	if ($staffzonetotal_p_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>P</td><td>新古</td><td align=right>".$staffzonetotal_p_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_p_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Qzone(old)
	if ($staffzonetotal_q_old[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>Q</td><td>中古</td><td align=right>".$staffzonetotal_q_old[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_q_old[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Qzone(new)
	if ($staffzonetotal_q_new[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>Q</td><td>新古</td><td align=right>".$staffzonetotal_q_new[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_q_new[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
}
print "<tr><td colspan=5 align=center>棚入れ点数合計</td><td align=right>".$total_count."</td>";
for ($c = 9; $c <= $nowtime; $c++) {
	for($min = 0; $min < $BuffMinutesMax; $min++){
		print "<td align=right>".$jikantotal[$c][$min]."</td>";
	}
}
print "</tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();

unset($motostaffcode,$staffcode,$staffname);
unset($staffzonecnt_a,$staffzonecnt_b,$staffzonecnt_c,$staffzonecnt_d,$staffzonecnt_e,$staffzonecnt_g);
unset($staffzonecnt_h,$staffzonecnt_i,$staffzonecnt_j,$staffzonecnt_k,$staffzonecnt_l);
unset($staffzonecnt_m,$staffzonecnt_n,$staffzonecnt_o,$staffzonecnt_p,$staffzonecnt_q);
unset($staffzonecnt,$stafftotal);
unset($staffzonetotal_a_old,$staffzonetotal_a_new,$staffzonetotal_b_old,$staffzonetotal_b_new,$staffzonetotal_c_old,$staffzonetotal_c_new,$staffzonetotal_d_old,$staffzonetotal_d_new,$staffzonetotal_e_old,$staffzonetotal_e_new,$staffzonetotal_g_old,$staffzonetotal_g_new);
unset($staffzonetotal_h_old,$staffzonetotal_h_new,$staffzonetotal_i_old,$staffzonetotal_i_new,$staffzonetotal_j_old,$staffzonetotal_j_new,$staffzonetotal_k_old,$staffzonetotal_k_new,$staffzonetotal_l_old,$staffzonetotal_l_new);
unset($staffzonetotal_m_old,$staffzonetotal_m_new,$staffzonetotal_n_old,$staffzonetotal_n_new,$staffzonetotal_o_old,$staffzonetotal_o_new,$staffzonetotal_p_old,$staffzonetotal_p_new,$staffzonetotal_q_old,$staffzonetotal_q_new);
unset($forhtml_a_old,$forhtml_a_new,$forhtml_b_old,$forhtml_b_new,$forhtml_c_old,$forhtml_c_new,$forhtml_d_old,$forhtml_d_new,$forhtml_e_old,$forhtml_e_new,$forhtml_g_old,$forhtml_g_new);
unset($forhtml_h_old,$forhtml_h_new,$forhtml_i_old,$forhtml_i_new,$forhtml_j_old,$forhtml_j_new,$forhtml_k_old,$forhtml_k_new);
unset($forhtml_l_old,$forhtml_l_new,$forhtml_m_old,$forhtml_m_new,$forhtml_n_old,$forhtml_n_new,$forhtml_o_old,$forhtml_o_new,$forhtml_p_old,$forhtml_p_new,$forhtml_q_old,$forhtml_q_new);

?>

<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>