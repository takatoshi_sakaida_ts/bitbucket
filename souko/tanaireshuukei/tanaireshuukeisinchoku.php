<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>棚入進捗状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$nowtime= date("H");
//仕分後棚入れ
//SQL文のCOUNT関数を使用
$sql = "select to_number(tanairetime),to_number(tanaireminute),staffname,count(*),staffcode ".
"from ( ".
"select ".
"substr(s.modifytime,1,2) as tanairetime, ".
"case when substr(s.modifytime,3,2)<31 then 0 ".
"else 1  ".
"end as tanaireminute, ".
"m.staffcode, ".
"m.staffname ".
"from d_tana_goods s,m_staff m ".
"where substr(s.locationcode,1,1)<'9' and tana_kind in ('51','52') ".
" and s.staffcode=m.staffcode ".
" and s.modifydate=(select to_char(sysdate,'yyyymmdd') from dual) ".
") ".
"group by tanairetime,tanaireminute,staffcode,staffname ".
"order by staffcode,tanairetime,tanaireminute";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$kekkatotalall=0;
$motostaff="moto";
$staffcount=0;
while($row = $res->fetchRow()){
	if ($motostaff==$row[4])
	{
	}
	else
	{
		$staffcount++;
		for ($i = 8; $i <= $nowtime; $i++)
		{
			$kekka[$staffcount][$i][0]=0;
			$kekka[$staffcount][$i][1]=0;
			$kekkatotal[$staffcount]=0;
		}
	}
	$kekka[$staffcount][$row[0]][$row[1]]=$row[3];
	$kekkatotal[$staffcount]=$kekkatotal[$staffcount]+$row[3];
	$kekkaname[$staffcount]=$row[2];
	$kekkacode[$staffcount]=$row[4];
	$motostaff=$row[4];
}
print "<strong><BR>本日の棚入れ作業状況</strong><br>".date('Y/m/d')."\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap rowspan='2'>スタッフコード</td><td nowrap rowspan='2'>スタッフ名</td><td nowrap rowspan='2'>合計</td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD colspan='2' align='center'>".$i."時</TD>";
	$kekka[$i][0]=0;
	$kekka[$i][1]=0;
	$kekkaall[$i][0]=0;
	$kekkaall[$i][1]=0;	
}
print "</TR>";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
//print "<td></td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD nowrap>00-30</TD>";
	print "<TD nowrap>31-60</TD>";
}
print "</TR>";

for ($c = 1; $c <=$staffcount; $c++) 
{
	print "<TR><td nowrap>".$kekkacode[$c]."</TD><td nowrap>".$kekkaname[$c]."</TD><td nowrap>".$kekkatotal[$c]."</TD>";
	$i=8;
	for ($i = 8; $i <= $nowtime; $i++)
	{
		print "<TD align='center'>".$kekka[$c][$i][0]."</TD>";
		print "<TD align='center'>".$kekka[$c][$i][1]."</TD>";
		if ($kekka[$c][$i][0]>0)
		{
			$kekka[$i][0]++;
		}
		if ($kekka[$c][$i][1]>0)
		{
			$kekka[$i][1]++;
		}
		$kekkaall[$i][0]=$kekkaall[$i][0]+$kekka[$c][$i][0];
		$kekkaall[$i][1]=$kekkaall[$i][1]+$kekka[$c][$i][1];
	}
	print "</TR>";
	$kekkatotalall=$kekkatotalall+$kekkatotal[$c];
}
//total
print "<TR bgcolor='#cccccc'><td nowrap colspan=2>総合計</TD>";
print "<td>".$kekkatotalall."</td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekkaall[$i][0]."</TD>";
	print "<TD align='center'>".$kekkaall[$i][1]."</TD>";
}
print "</tr>";
//TOTAL
print "<TR bgcolor='#cccccc'><td nowrap colspan=2>スタッフ数</TD>";
print "<td>"."　"."</td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka[$i][0]."</TD>";
	print "<TD align='center'>".$kekka[$i][1]."</TD>";
}
print "</tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>