<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>仕分後棚入れ作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$nowtime= date("H");
//仕分後棚入れ
//SQL文のCOUNT関数を使用
$sql = "select to_number(tanairetime),to_number(tanaireminute),to_number(newused_kind),count(*),to_number(genre0code) ".
"from ( ".
"select ".
"substr(s.modifytime,1,2) as tanairetime, ".
"case when substr(s.modifytime,3,2)<31 then 0 ".
"else 1  ".
"end as tanaireminute, ".
"s.NEWUSED_KIND,".
"m.genre0code ".
"from d_tana_goods s,m_goods m ".
"where substr(s.locationcode,1,1)<'9' and tana_kind ='51'".
" and s.instorecode=m.instorecode ".
" and s.modifydate=(select to_char(sysdate,'yyyymmdd') from dual) ".
") ".
"group by tanairetime,tanaireminute,newused_kind,genre0code ".
"order by tanairetime,tanaireminute,newused_kind,genre0code";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$i=8;
for ($i = 8; $i <= 23; $i++)
{
	$kekka_oldbook[$i][0]=0;
	$kekka_oldbook[$i][1]=0;
	$kekka_oldcomic[$i][0]=0;
	$kekka_oldcomic[$i][1]=0;
	$kekka_olddvd[$i][0]=0;
	$kekka_olddvd[$i][1]=0;
	$kekka_oldcd[$i][0]=0;
	$kekka_oldcd[$i][1]=0;
	$kekka_oldgame[$i][0]=0;
	$kekka_oldgame[$i][1]=0;
	$kekka_newbook[$i][0]=0;
	$kekka_newbook[$i][1]=0;
	$kekka_newcomic[$i][0]=0;
	$kekka_newcomic[$i][1]=0;
	$kekka_newdvd[$i][0]=0;
	$kekka_newdvd[$i][1]=0;
	$kekka_newcd[$i][0]=0;
	$kekka_newcd[$i][1]=0;
	$kekka_newgame[$i][0]=0;
	$kekka_newgame[$i][1]=0;
	$kekka_old[$i][0]=0;
	$kekka_old[$i][1]=0;
	$kekka_new[$i][0]=0;
	$kekka_new[$i][1]=0;
//print $i;
}
$kekkatotal_oldbook=0;
$kekkatotal_newbook=0;
$kekkatotal_oldcomic=0;
$kekkatotal_newcomic=0;
$kekkatotal_olddvd=0;
$kekkatotal_newdvd=0;
$kekkatotal_oldcd=0;
$kekkatotal_newcd=0;
$kekkatotal_oldgame=0;
$kekkatotal_newgame=0;
$kekkatotal_old=0;
$kekkatotal_new=0;

while($row = $res->fetchRow()){

switch($row[4]){
case 11:
	if ($row[2]==0)
	{
		$kekka_oldcomic[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newcomic[$row[0]][$row[1]]=$row[3];
	}
	break;
case 12:
	if ($row[2]==0)
	{
		$kekka_oldbook[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newbook[$row[0]][$row[1]]=$row[3];
	}
	break;
case 31:
	if ($row[2]==0)
	{
		$kekka_oldcd[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newcd[$row[0]][$row[1]]=$row[3];
	}
	break;
case 51:
	if ($row[2]==0)
	{
		$kekka_oldgame[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newgame[$row[0]][$row[1]]=$row[3];
	}
	break;

case 71:
	if ($row[2]==0)
	{
		$kekka_olddvd[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newdvd[$row[0]][$row[1]]=$row[3];
	}
	break;
}
}
print "<strong><BR>本日の仕分後棚入れ作業状況</strong><br>".date('Y/m/d')."\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap rowspan='2'>カテゴリ</td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD colspan='2' align='center'>".$i."時</TD>";
}
print "</TR>";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
//print "<td></td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD nowrap>00-30</TD>";
	print "<TD nowrap>31-60</TD>";
}
print "<TD nowrap>合計</TD></TR>";
//書籍（中古）
print "<TR><td nowrap>書籍（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldbook[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldbook[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldbook[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldbook[$i][1];
	$kekkatotal_oldbook=$kekkatotal_oldbook+$kekka_oldbook[$i][0]+$kekka_oldbook[$i][1];
}
print "<td>".$kekkatotal_oldbook."</TD></TR>";
//コミック（中古）
print "<TR><td nowrap>コミック（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldcomic[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldcomic[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldcomic[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldcomic[$i][1];
	$kekkatotal_oldcomic=$kekkatotal_oldcomic+$kekka_oldcomic[$i][0]+$kekka_oldcomic[$i][1];
}
print "<td>".$kekkatotal_oldcomic."</TD></TR>";
//ＣＤ（中古）
print "<TR><td nowrap>ＣＤ（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldcd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldcd[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldcd[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldcd[$i][1];
	$kekkatotal_oldcd=$kekkatotal_oldcd+$kekka_oldcd[$i][0]+$kekka_oldcd[$i][1];
}
print "<td>".$kekkatotal_oldcd."</TD></TR>";
//ＤＶＤ（中古）
print "<TR><td nowrap>ＤＶＤ（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_olddvd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_olddvd[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_olddvd[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_olddvd[$i][1];
	$kekkatotal_olddvd=$kekkatotal_olddvd+$kekka_olddvd[$i][0]+$kekka_olddvd[$i][1];
}
print "<td>".$kekkatotal_olddvd."</TD></TR>";
//ＧＡＭＥ（中古）
print "<TR><td nowrap>ＧＡＭＥ（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldgame[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldgame[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldgame[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldgame[$i][1];
	$kekkatotal_oldgame=$kekkatotal_oldgame+$kekka_oldgame[$i][0]+$kekka_oldgame[$i][1];
}
print "<td>".$kekkatotal_oldgame."</TD></TR>";
//TOTAL（中古）
print "<TR bgcolor='#cccccc'><td nowrap>中古合計</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_old[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_old[$i][1]."</TD>";
	$kekkatotal_old=$kekkatotal_old+$kekka_old[$i][0]+$kekka_old[$i][1];
}
print "<td>".$kekkatotal_old."</TD></TR>";
//書籍（新古）
print "<TR><td nowrap>書籍（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newbook[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newbook[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newbook[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newbook[$i][1];
	$kekkatotal_newbook=$kekkatotal_newbook+$kekka_newbook[$i][0]+$kekka_newbook[$i][1];
}
print "<td>".$kekkatotal_newbook."</TD></TR>";
//コミック（新古）
print "<TR><td nowrap>コミック（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newcomic[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newcomic[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newcomic[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newcomic[$i][1];
	$kekkatotal_newcomic=$kekkatotal_newcomic+$kekka_newcomic[$i][0]+$kekka_newcomic[$i][1];
}
print "<td>".$kekkatotal_newcomic."</TD></TR>";
//ＣＤ（新古）
print "<TR><td nowrap>ＣＤ（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newcd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newcd[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newcd[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newcd[$i][1];
	$kekkatotal_newcd=$kekkatotal_newcd+$kekka_newcd[$i][0]+$kekka_newcd[$i][1];
}
print "<TD>".$kekkatotal_newcd."</TD></TR>";
//game（新古）
print "<TR><td nowrap>ＤＶＤ（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newdvd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newdvd[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newdvd[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newdvd[$i][1];
	$kekkatotal_newdvd=$kekkatotal_newdvd+$kekka_newdvd[$i][0]+$kekka_newdvd[$i][1];
}
print "<td>".$kekkatotal_newdvd."</TD></TR>";
//ＤＶＤ（新古）
print "<TR><td nowrap>ＧＡＭＥ（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newgame[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newgame[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newgame[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newgame[$i][1];
	$kekkatotal_newgame=$kekkatotal_newgame+$kekka_newgame[$i][0]+$kekka_newgame[$i][1];
}
print "<td>".$kekkatotal_newgame."</TD></TR>";
//TOTAL（新古）
print "<TR bgcolor='#cccccc'><td nowrap>新古合計</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_new[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_new[$i][1]."</TD>";
	$kekkatotal_new=$kekkatotal_new+$kekka_new[$i][0]+$kekka_new[$i][1];
}
print "<td>".$kekkatotal_new."</TD></TR>";
//TOTAL（新古）
print "<TR bgcolor='#cccccc'><td nowrap>総合計</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".($kekka_old[$i][0]+$kekka_new[$i][0])."</TD>";
	print "<TD align='center'>".($kekka_old[$i][1]+$kekka_new[$i][1])."</TD>";
}
print "<td>".($kekkatotal_old+$kekkatotal_new)."　</td></tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>