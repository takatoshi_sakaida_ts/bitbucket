<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>棚入れ作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$nowtime= date("H");
$sql = "select to_number(tanairetime),to_number(tanaireminute),count(*),zonecode,to_number(newused_kind) ".
"from ( ".
"select ".
"substr(s.modifytime,1,2) as tanairetime,m.zonecode,".
"case when substr(s.modifytime,3,2)<31 then 0 ".
"else 1  ".
"end as tanaireminute,s.newused_kind ".
"from d_tana_goods s,m_location m ".
"where substr(s.locationcode,1,1)<'9'  and tana_kind in ('51','52','55')".
" and s.locationcode=m.locationcode  ".
"and s.modifydate=(select to_char(sysdate,'yyyymmdd') from dual) ".
") ".
"group by tanairetime,tanaireminute,zonecode,newused_kind ".
"order by tanairetime,tanaireminute,zonecode,newused_kind";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
for ($c = 7; $c <= 23; $c++) {
//D中古
	$forhtmlta_a[$c][0][0]=0;
	$forhtmlta_a[$c][1][0]=0;
	$forhtmlta_b[$c][0][0]=0;
	$forhtmlta_b[$c][1][0]=0;
	$forhtmlta_c[$c][0][0]=0;
	$forhtmlta_c[$c][1][0]=0;
	$forhtmlta_d[$c][0][0]=0;
	$forhtmlta_d[$c][1][0]=0;
	$forhtmlta_e[$c][0][0]=0;
	$forhtmlta_e[$c][1][0]=0;
	$forhtmlta_g[$c][0][0]=0;
	$forhtmlta_g[$c][1][0]=0;
	$forhtmlta_h[$c][0][0]=0;
	$forhtmlta_h[$c][1][0]=0;
	$forhtmlta_i[$c][0][0]=0;
	$forhtmlta_i[$c][1][0]=0;
	$forhtmlta_j[$c][0][0]=0;
	$forhtmlta_j[$c][1][0]=0;
	$forhtmlta_k[$c][0][0]=0;
	$forhtmlta_k[$c][1][0]=0;
	$forhtmlta_l[$c][0][0]=0;
	$forhtmlta_l[$c][1][0]=0;
	$forhtmlta_m[$c][0][0]=0;
	$forhtmlta_m[$c][1][0]=0;
	$forhtmlta_n[$c][0][0]=0;
	$forhtmlta_n[$c][1][0]=0;
	$forhtmlta_o[$c][0][0]=0;
	$forhtmlta_o[$c][1][0]=0;
	$forhtmlta_p[$c][0][0]=0;
	$forhtmlta_p[$c][1][0]=0;
	$forhtmlta_q[$c][0][0]=0;
	$forhtmlta_q[$c][1][0]=0;
//D新古
	$forhtmlta_a[$c][0][1]=0;
	$forhtmlta_a[$c][1][1]=0;
	$forhtmlta_b[$c][0][1]=0;
	$forhtmlta_b[$c][1][1]=0;
	$forhtmlta_c[$c][0][1]=0;
	$forhtmlta_c[$c][1][1]=0;
	$forhtmlta_d[$c][0][1]=0;
	$forhtmlta_d[$c][1][1]=0;
	$forhtmlta_e[$c][0][1]=0;
	$forhtmlta_e[$c][1][1]=0;
	$forhtmlta_g[$c][0][1]=0;
	$forhtmlta_g[$c][1][1]=0;
	$forhtmlta_h[$c][0][1]=0;
	$forhtmlta_h[$c][1][1]=0;
	$forhtmlta_i[$c][0][1]=0;
	$forhtmlta_i[$c][1][1]=0;
	$forhtmlta_j[$c][0][1]=0;
	$forhtmlta_j[$c][1][1]=0;
	$forhtmlta_k[$c][0][1]=0;
	$forhtmlta_k[$c][1][1]=0;
	$forhtmlta_l[$c][0][1]=0;
	$forhtmlta_l[$c][1][1]=0;
	$forhtmlta_m[$c][0][1]=0;
	$forhtmlta_m[$c][1][1]=0;
	$forhtmlta_n[$c][0][1]=0;
	$forhtmlta_n[$c][1][1]=0;
	$forhtmlta_o[$c][0][1]=0;
	$forhtmlta_o[$c][1][1]=0;
	$forhtmlta_p[$c][0][1]=0;
	$forhtmlta_p[$c][1][1]=0;
	$forhtmlta_q[$c][0][1]=0;
	$forhtmlta_q[$c][1][1]=0;
//total
	$forhtmltat_a=0;
	$forhtmltat_b=0;
	$forhtmltat_c=0;
	$forhtmltat_d=0;
	$forhtmltat_e=0;
	$forhtmltat_g=0;
	$forhtmltat_h=0;
	$forhtmltat_i=0;
	$forhtmltat_j=0;
	$forhtmltat_k=0;
	$forhtmltat_l=0;
	$forhtmltat_m=0;
	$forhtmltat_n=0;
	$forhtmltat_o=0;
	$forhtmltat_p=0;
	$forhtmltat_q=0;
	$forhtmlta[$c][0]=0;
	$forhtmlta[$c][1]=0;
	$forhtmlta_sinko  =0;
	$forhtmlta_sinko_a=0;
	$forhtmlta_sinko_b=0;
	$forhtmlta_sinko_c=0;
	$forhtmlta_sinko_d=0;
	$forhtmlta_sinko_g=0;
	$forhtmlta_sinko_h=0;
	$forhtmlta_sinko_i=0;
	$forhtmlta_sinko_j=0;
	$forhtmlta_sinko_k=0;
	$forhtmlta_sinko_l=0;
	$forhtmlta_sinko_m=0;
	$forhtmlta_sinko_n=0;
	$forhtmlta_sinko_o=0;
	$forhtmlta_sinko_p=0;
	$forhtmlta_sinko_q=0;
}
//検索結果の表示
print "<strong><BR>本日の棚入れ作業状況</strong><br>".date('Y/m/d')."\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td rowspan='2'>棚入れ時間</td>";
print "<td colspan='15' align='center'>中古</td>";
print "<td colspan='15' align='center'>新古</td>";
print "<td rowspan='2'>時間帯合計</td></tr>";
print "<tr bgcolor=#ccffff>\n";
print "<td width=40>Ａ</td><td width=40>Ｂ</td><td width=40>Ｃ</td><td width=40>Ｄ</td><td width=40>Ｇ</td>";
print "<td width=40>Ｈ</td><td width=40>Ｉ</td><td width=40>Ｊ</td><td width=40>Ｋ</td><td width=40>Ｌ</td>";
print "<td width=40>Ｍ</td><td width=40>Ｎ</td><td width=40>Ｏ</td><td width=40>Ｐ</td><td width=40>Ｑ</td>";
print "<td width=40>Ａ</td><td width=40>Ｂ</td><td width=40>Ｃ</td><td width=40>Ｄ</td><td width=40>Ｇ</td>";
print "<td width=40>Ｈ</td><td width=40>Ｉ</td><td width=40>Ｊ</td><td width=40>Ｋ</td><td width=40>Ｌ</td>";
print "<td width=40>Ｍ</td><td width=40>Ｎ</td><td width=40>Ｏ</td><td width=40>Ｐ</td><td width=40>Ｑ</td>";
//初期値設定
$i=0;

//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)

while($row = $res->fetchRow()){
	switch ($row[3]){
//数値に変換するために+0してます
	case "A";
		$forhtmlta_a[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "B";
		$forhtmlta_b[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "C";
		$forhtmlta_c[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "D";
		$forhtmlta_d[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "E";
		$forhtmlta_e[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "G";
		$forhtmlta_g[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "H";
		$forhtmlta_h[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "I";
		$forhtmlta_i[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "J";
		$forhtmlta_j[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "K";
		$forhtmlta_k[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "L";
		$forhtmlta_l[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "M";
		$forhtmlta_m[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "N";
		$forhtmlta_n[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "O";
		$forhtmlta_o[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "P";
		$forhtmlta_p[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "Q";
		$forhtmlta_q[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	}
	$forhtmlta[($row[0]+0)][($row[1]+0)]=$forhtmlta[($row[0]+0)][($row[1]+0)]+$row[2];
	$i=$i+$row[2];
}

for ($c = 9; $c <= $nowtime; $c++) {
	print "<tr>";
	print "<td>".$c."時00〜30分</td>";
	print "<td>".$forhtmlta_a[$c][0][0]."</td>";
	print "<td>".$forhtmlta_b[$c][0][0]."</td>";
	print "<td>".$forhtmlta_c[$c][0][0]."</td>";
	print "<td>".$forhtmlta_d[$c][0][0]."</td>";
	print "<td>".$forhtmlta_g[$c][0][0]."</td>";
	print "<td>".$forhtmlta_h[$c][0][0]."</td>";
	print "<td>".$forhtmlta_i[$c][0][0]."</td>";
	print "<td>".$forhtmlta_j[$c][0][0]."</td>";
	print "<td>".$forhtmlta_k[$c][0][0]."</td>";
	print "<td>".$forhtmlta_l[$c][0][0]."</td>";
	print "<td>".$forhtmlta_m[$c][0][0]."</td>";
	print "<td>".$forhtmlta_n[$c][0][0]."</td>";
	print "<td>".$forhtmlta_o[$c][0][0]."</td>";
	print "<td>".$forhtmlta_p[$c][0][0]."</td>";
	print "<td>".$forhtmlta_q[$c][0][0]."</td>";
	print "<td>".$forhtmlta_a[$c][0][1]."</td>";
	print "<td>".$forhtmlta_b[$c][0][1]."</td>";
	print "<td>".$forhtmlta_c[$c][0][1]."</td>";
	print "<td>".$forhtmlta_d[$c][0][1]."</td>";
	print "<td>".$forhtmlta_g[$c][0][1]."</td>";
	print "<td>".$forhtmlta_h[$c][0][1]."</td>";
	print "<td>".$forhtmlta_i[$c][0][1]."</td>";
	print "<td>".$forhtmlta_j[$c][0][1]."</td>";
	print "<td>".$forhtmlta_k[$c][0][1]."</td>";
	print "<td>".$forhtmlta_l[$c][0][1]."</td>";
	print "<td>".$forhtmlta_m[$c][0][1]."</td>";
	print "<td>".$forhtmlta_n[$c][0][1]."</td>";
	print "<td>".$forhtmlta_o[$c][0][1]."</td>";
	print "<td>".$forhtmlta_p[$c][0][1]."</td>";
	print "<td>".$forhtmlta_q[$c][0][1]."</td>";
	print "<td align=right>".$forhtmlta[$c][0]."</td>";
	print "</tr>";
	print "<tr>";
	print "<td>".$c."時31〜60分</td>";
	print "<td>".$forhtmlta_a[$c][1][0]."</td>";
	print "<td>".$forhtmlta_b[$c][1][0]."</td>";
	print "<td>".$forhtmlta_c[$c][1][0]."</td>";
	print "<td>".$forhtmlta_d[$c][1][0]."</td>";
	print "<td>".$forhtmlta_g[$c][1][0]."</td>";
	print "<td>".$forhtmlta_h[$c][1][0]."</td>";
	print "<td>".$forhtmlta_i[$c][1][0]."</td>";
	print "<td>".$forhtmlta_j[$c][1][0]."</td>";
	print "<td>".$forhtmlta_k[$c][1][0]."</td>";
	print "<td>".$forhtmlta_l[$c][1][0]."</td>";
	print "<td>".$forhtmlta_m[$c][1][0]."</td>";
	print "<td>".$forhtmlta_n[$c][1][0]."</td>";
	print "<td>".$forhtmlta_o[$c][1][0]."</td>";
	print "<td>".$forhtmlta_p[$c][1][0]."</td>";
	print "<td>".$forhtmlta_q[$c][1][0]."</td>";
	print "<td>".$forhtmlta_a[$c][1][1]."</td>";
	print "<td>".$forhtmlta_b[$c][1][1]."</td>";
	print "<td>".$forhtmlta_c[$c][1][1]."</td>";
	print "<td>".$forhtmlta_d[$c][1][1]."</td>";
	print "<td>".$forhtmlta_g[$c][1][1]."</td>";
	print "<td>".$forhtmlta_h[$c][1][1]."</td>";
	print "<td>".$forhtmlta_i[$c][1][1]."</td>";
	print "<td>".$forhtmlta_j[$c][1][1]."</td>";
	print "<td>".$forhtmlta_k[$c][1][1]."</td>";
	print "<td>".$forhtmlta_l[$c][1][1]."</td>";
	print "<td>".$forhtmlta_m[$c][1][1]."</td>";
	print "<td>".$forhtmlta_n[$c][1][1]."</td>";
	print "<td>".$forhtmlta_o[$c][1][1]."</td>";
	print "<td>".$forhtmlta_p[$c][1][1]."</td>";
	print "<td>".$forhtmlta_q[$c][1][1]."</td>";
	print "<td align=right>".$forhtmlta[$c][1]."</td>";
	print "</tr>";
	$forhtmltat_a=$forhtmltat_a+$forhtmlta_a[$c][0][0]+$forhtmlta_a[$c][1][0];
	$forhtmltat_b=$forhtmltat_b+$forhtmlta_b[$c][0][0]+$forhtmlta_b[$c][1][0];
	$forhtmltat_c=$forhtmltat_c+$forhtmlta_c[$c][0][0]+$forhtmlta_c[$c][1][0];
	$forhtmltat_d=$forhtmltat_d+$forhtmlta_d[$c][0][0]+$forhtmlta_d[$c][1][0];
	$forhtmltat_g=$forhtmltat_g+$forhtmlta_g[$c][0][0]+$forhtmlta_g[$c][1][0];
	$forhtmltat_h=$forhtmltat_h+$forhtmlta_h[$c][0][0]+$forhtmlta_h[$c][1][0];
	$forhtmltat_i=$forhtmltat_i+$forhtmlta_i[$c][0][0]+$forhtmlta_i[$c][1][0];
	$forhtmltat_j=$forhtmltat_j+$forhtmlta_j[$c][0][0]+$forhtmlta_j[$c][1][0];
	$forhtmltat_k=$forhtmltat_k+$forhtmlta_k[$c][0][0]+$forhtmlta_k[$c][1][0];
	$forhtmltat_l=$forhtmltat_l+$forhtmlta_l[$c][0][0]+$forhtmlta_l[$c][1][0];
	$forhtmltat_m=$forhtmltat_m+$forhtmlta_m[$c][0][0]+$forhtmlta_m[$c][1][0];
	$forhtmltat_n=$forhtmltat_n+$forhtmlta_n[$c][0][0]+$forhtmlta_n[$c][1][0];
	$forhtmltat_o=$forhtmltat_o+$forhtmlta_o[$c][0][0]+$forhtmlta_o[$c][1][0];
	$forhtmltat_p=$forhtmltat_p+$forhtmlta_p[$c][0][0]+$forhtmlta_p[$c][1][0];
	$forhtmltat_q=$forhtmltat_q+$forhtmlta_q[$c][0][0]+$forhtmlta_q[$c][1][0];
	$forhtmlta_sinko_a=$forhtmlta_sinko_a+$forhtmlta_a[$c][0][1]+$forhtmlta_a[$c][1][1];
	$forhtmlta_sinko_b=$forhtmlta_sinko_b+$forhtmlta_b[$c][0][1]+$forhtmlta_b[$c][1][1];
	$forhtmlta_sinko_c=$forhtmlta_sinko_c+$forhtmlta_c[$c][0][1]+$forhtmlta_c[$c][1][1];
	$forhtmlta_sinko_d=$forhtmlta_sinko_d+$forhtmlta_d[$c][0][1]+$forhtmlta_d[$c][1][1];
	$forhtmlta_sinko_g=$forhtmlta_sinko_g+$forhtmlta_g[$c][0][1]+$forhtmlta_g[$c][1][1];
	$forhtmlta_sinko_h=$forhtmlta_sinko_h+$forhtmlta_h[$c][0][1]+$forhtmlta_h[$c][1][1];
	$forhtmlta_sinko_i=$forhtmlta_sinko_i+$forhtmlta_i[$c][0][1]+$forhtmlta_i[$c][1][1];
	$forhtmlta_sinko_j=$forhtmlta_sinko_j+$forhtmlta_j[$c][0][1]+$forhtmlta_j[$c][1][1];
	$forhtmlta_sinko_k=$forhtmlta_sinko_k+$forhtmlta_k[$c][0][1]+$forhtmlta_k[$c][1][1];
	$forhtmlta_sinko_l=$forhtmlta_sinko_l+$forhtmlta_l[$c][0][1]+$forhtmlta_l[$c][1][1];
	$forhtmlta_sinko_m=$forhtmlta_sinko_m+$forhtmlta_m[$c][0][1]+$forhtmlta_m[$c][1][1];
	$forhtmlta_sinko_n=$forhtmlta_sinko_n+$forhtmlta_n[$c][0][1]+$forhtmlta_n[$c][1][1];
	$forhtmlta_sinko_o=$forhtmlta_sinko_o+$forhtmlta_o[$c][0][1]+$forhtmlta_o[$c][1][1];
	$forhtmlta_sinko_p=$forhtmlta_sinko_p+$forhtmlta_p[$c][0][1]+$forhtmlta_p[$c][1][1];
	$forhtmlta_sinko_q=$forhtmlta_sinko_q+$forhtmlta_q[$c][0][1]+$forhtmlta_q[$c][1][1];
	$forhtmlta_sinko  =                 $forhtmlta_a[$c][0][1]+$forhtmlta_b[$c][0][1]+$forhtmlta_c[$c][0][1]+$forhtmlta_d[$c][0][1]+$forhtmlta_g[$c][0][1];
	$forhtmlta_sinko  =$forhtmlta_sinko+$forhtmlta_h[$c][0][1]+$forhtmlta_i[$c][0][1]+$forhtmlta_j[$c][0][1]+$forhtmlta_k[$c][0][1]+$forhtmlta_l[$c][0][1];
	$forhtmlta_sinko  =$forhtmlta_sinko+$forhtmlta_m[$c][0][1]+$forhtmlta_n[$c][0][1]+$forhtmlta_o[$c][0][1]+$forhtmlta_p[$c][0][1]+$forhtmlta_q[$c][0][1];
	$forhtmlta_sinko  =$forhtmlta_sinko+$forhtmlta_a[$c][1][1]+$forhtmlta_b[$c][1][1]+$forhtmlta_c[$c][1][1]+$forhtmlta_d[$c][1][1]+$forhtmlta_g[$c][1][1];
	$forhtmlta_sinko  =$forhtmlta_sinko+$forhtmlta_h[$c][1][1]+$forhtmlta_i[$c][1][1]+$forhtmlta_j[$c][1][1]+$forhtmlta_k[$c][1][1]+$forhtmlta_l[$c][1][1];
	$forhtmlta_sinko  =$forhtmlta_sinko+$forhtmlta_m[$c][1][1]+$forhtmlta_n[$c][1][1]+$forhtmlta_o[$c][1][1]+$forhtmlta_p[$c][1][1]+$forhtmlta_q[$c][1][1];
}
print "<tr><td>ゾーン合計</td><td>".$forhtmltat_a."</TD>";
print "<td>".$forhtmltat_b."</TD>";
print "<td>".$forhtmltat_c."</TD>";
print "<td>".$forhtmltat_d."</TD>";
print "<td>".$forhtmltat_g."</TD>";
print "<td>".$forhtmltat_h."</TD>";
print "<td>".$forhtmltat_i."</TD>";
print "<td>".$forhtmltat_j."</TD>";
print "<td>".$forhtmltat_k."</TD>";
print "<td>".$forhtmltat_l."</TD>";
print "<td>".$forhtmltat_m."</TD>";
print "<td>".$forhtmltat_n."</TD>";
print "<td>".$forhtmltat_o."</TD>";
print "<td>".$forhtmltat_p."</TD>";
print "<td>".$forhtmltat_q."</TD>";
print "<td>".$forhtmlta_sinko_a."</TD>";
print "<td>".$forhtmlta_sinko_b."</TD>";
print "<td>".$forhtmlta_sinko_c."</TD>";
print "<td>".$forhtmlta_sinko_d."</TD>";
print "<td>".$forhtmlta_sinko_g."</TD>";
print "<td>".$forhtmlta_sinko_h."</TD>";
print "<td>".$forhtmlta_sinko_i."</TD>";
print "<td>".$forhtmlta_sinko_j."</TD>";
print "<td>".$forhtmlta_sinko_k."</TD>";
print "<td>".$forhtmlta_sinko_l."</TD>";
print "<td>".$forhtmlta_sinko_m."</TD>";
print "<td>".$forhtmlta_sinko_n."</TD>";
print "<td>".$forhtmlta_sinko_o."</TD>";
print "<td>".$forhtmlta_sinko_p."</TD>";
print "<td>".$forhtmlta_sinko_q."</TD>";
print "<TD></TD></TR>";

print "<tr><td>合計</td><td colspan=31 align=right>　".$i."　</td></tr>";
print "</table>";

//データの開放
$res->free();


//SQL文のCOUNT関数を使用
$sql = "select to_number(tanairetime),to_number(tanaireminute),to_number(newused_kind),count(*),to_number(genre0code) ".
"from ( ".
"select ".
"substr(s.modifytime,1,2) as tanairetime, ".
"case when substr(s.modifytime,3,2)<31 then 0 ".
"else 1  ".
"end as tanaireminute, ".
"s.NEWUSED_KIND,".
"m.genre0code ".
"from d_tana_goods s,m_goods m ".
"where substr(s.locationcode,1,1)<'9' and tana_kind in ('51','52','55')".
" and s.instorecode=m.instorecode ".
" and s.modifydate=(select to_char(sysdate,'yyyymmdd') from dual) ".
") ".
"group by tanairetime,tanaireminute,newused_kind,genre0code ".
"order by tanairetime,tanaireminute,newused_kind,genre0code";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$i=8;
for ($i = 8; $i <= 23; $i++)
{
	$kekka_oldbook[$i][0]=0;
	$kekka_oldbook[$i][1]=0;
	$kekka_oldcomic[$i][0]=0;
	$kekka_oldcomic[$i][1]=0;
	$kekka_olddvd[$i][0]=0;
	$kekka_olddvd[$i][1]=0;
	$kekka_oldcd[$i][0]=0;
	$kekka_oldcd[$i][1]=0;
	$kekka_oldgame[$i][0]=0;
	$kekka_oldgame[$i][1]=0;
	$kekka_newbook[$i][0]=0;
	$kekka_newbook[$i][1]=0;
	$kekka_newcomic[$i][0]=0;
	$kekka_newcomic[$i][1]=0;
	$kekka_newdvd[$i][0]=0;
	$kekka_newdvd[$i][1]=0;
	$kekka_newcd[$i][0]=0;
	$kekka_newcd[$i][1]=0;
	$kekka_newgame[$i][0]=0;
	$kekka_newgame[$i][1]=0;
	$kekka_old[$i][0]=0;
	$kekka_old[$i][1]=0;
	$kekka_new[$i][0]=0;
	$kekka_new[$i][1]=0;
//print $i;
}
$kekkatotal_oldbook=0;
$kekkatotal_newbook=0;
$kekkatotal_oldcomic=0;
$kekkatotal_newcomic=0;
$kekkatotal_olddvd=0;
$kekkatotal_newdvd=0;
$kekkatotal_oldcd=0;
$kekkatotal_newcd=0;
$kekkatotal_oldgame=0;
$kekkatotal_newgame=0;
$kekkatotal_old=0;
$kekkatotal_new=0;

while($row = $res->fetchRow()){

switch($row[4]){
case 11:
	if ($row[2]==0)
	{
		$kekka_oldcomic[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newcomic[$row[0]][$row[1]]=$row[3];
	}
	break;
case 12:
	if ($row[2]==0)
	{
		$kekka_oldbook[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newbook[$row[0]][$row[1]]=$row[3];
	}
	break;
case 31:
	if ($row[2]==0)
	{
		$kekka_oldcd[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newcd[$row[0]][$row[1]]=$row[3];
	}
	break;
case 51:
	if ($row[2]==0)
	{
		$kekka_oldgame[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newgame[$row[0]][$row[1]]=$row[3];
	}
	break;

case 71:
	if ($row[2]==0)
	{
		$kekka_olddvd[$row[0]][$row[1]]=$row[3];
	}else
	{
		$kekka_newdvd[$row[0]][$row[1]]=$row[3];
	}
	break;
}
}
print "<HR><table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap align='center' rowspan='2'>カテゴリ</td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD colspan='2' align='center'>".$i."時</TD>";
}
print "<TD nowrap align='center' rowspan='2'>合計</TD></TR>";
print "</TR>";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
//print "<td></td>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD nowrap>00-30</TD>";
	print "<TD nowrap>31-60</TD>";
}
//コミック（中古）
print "<TR><td nowrap>コミック（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldcomic[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldcomic[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldcomic[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldcomic[$i][1];
	$kekkatotal_oldcomic=$kekkatotal_oldcomic+$kekka_oldcomic[$i][0]+$kekka_oldcomic[$i][1];
}
print "<td>".$kekkatotal_oldcomic."</TD></TR>";
//書籍（中古）
print "<TR><td nowrap>書籍（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldbook[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldbook[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldbook[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldbook[$i][1];
	$kekkatotal_oldbook=$kekkatotal_oldbook+$kekka_oldbook[$i][0]+$kekka_oldbook[$i][1];
}
print "<td>".$kekkatotal_oldbook."</TD></TR>";
//ＣＤ（中古）
print "<TR><td nowrap>ＣＤ（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldcd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldcd[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldcd[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldcd[$i][1];
	$kekkatotal_oldcd=$kekkatotal_oldcd+$kekka_oldcd[$i][0]+$kekka_oldcd[$i][1];
}
print "<td>".$kekkatotal_oldcd."</TD></TR>";
//ＧＡＭＥ（中古）
print "<TR><td nowrap>ＧＡＭＥ（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_oldgame[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_oldgame[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_oldgame[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_oldgame[$i][1];
	$kekkatotal_oldgame=$kekkatotal_oldgame+$kekka_oldgame[$i][0]+$kekka_oldgame[$i][1];
}
print "<td>".$kekkatotal_oldgame."</TD></TR>";
//ＤＶＤ（中古）
print "<TR><td nowrap>ＤＶＤ（中古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_olddvd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_olddvd[$i][1]."</TD>";
	$kekka_old[$i][0]=$kekka_old[$i][0]+$kekka_olddvd[$i][0];
	$kekka_old[$i][1]=$kekka_old[$i][1]+$kekka_olddvd[$i][1];
	$kekkatotal_olddvd=$kekkatotal_olddvd+$kekka_olddvd[$i][0]+$kekka_olddvd[$i][1];
}
print "<td>".$kekkatotal_olddvd."</TD></TR>";
//TOTAL（中古）
print "<TR bgcolor='#cccccc'><td nowrap>中古合計</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_old[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_old[$i][1]."</TD>";
	$kekkatotal_old=$kekkatotal_old+$kekka_old[$i][0]+$kekka_old[$i][1];
}
print "<td>".$kekkatotal_old."</TD></TR>";
//コミック（新古）
print "<TR><td nowrap>コミック（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newcomic[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newcomic[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newcomic[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newcomic[$i][1];
	$kekkatotal_newcomic=$kekkatotal_newcomic+$kekka_newcomic[$i][0]+$kekka_newcomic[$i][1];
}
print "<td>".$kekkatotal_newcomic."</TD></TR>";
//書籍（新古）
print "<TR><td nowrap>書籍（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newbook[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newbook[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newbook[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newbook[$i][1];
	$kekkatotal_newbook=$kekkatotal_newbook+$kekka_newbook[$i][0]+$kekka_newbook[$i][1];
}
print "<td>".$kekkatotal_newbook."</TD></TR>";
//ＣＤ（新古）
print "<TR><td nowrap>ＣＤ（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newcd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newcd[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newcd[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newcd[$i][1];
	$kekkatotal_newcd=$kekkatotal_newcd+$kekka_newcd[$i][0]+$kekka_newcd[$i][1];
}
print "<TD>".$kekkatotal_newcd."</TD></TR>";
//GAME（新古）
print "<TR><td nowrap>ＧＡＭＥ（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newgame[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newgame[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newgame[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newgame[$i][1];
	$kekkatotal_newgame=$kekkatotal_newgame+$kekka_newgame[$i][0]+$kekka_newgame[$i][1];
}
print "<td>".$kekkatotal_newgame."</TD></TR>";
//DVD（新古）
print "<TR><td nowrap>ＤＶＤ（新古）</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_newdvd[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_newdvd[$i][1]."</TD>";
	$kekka_new[$i][0]=$kekka_new[$i][0]+$kekka_newdvd[$i][0];
	$kekka_new[$i][1]=$kekka_new[$i][1]+$kekka_newdvd[$i][1];
	$kekkatotal_newdvd=$kekkatotal_newdvd+$kekka_newdvd[$i][0]+$kekka_newdvd[$i][1];
}
print "<td>".$kekkatotal_newdvd."</TD></TR>";
//TOTAL（新古）
print "<TR bgcolor='#cccccc'><td nowrap>新古合計</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".$kekka_new[$i][0]."</TD>";
	print "<TD align='center'>".$kekka_new[$i][1]."</TD>";
	$kekkatotal_new=$kekkatotal_new+$kekka_new[$i][0]+$kekka_new[$i][1];
}
print "<td>".$kekkatotal_new."</TD></TR>";
//TOTAL（新古）
print "<TR bgcolor='#cccccc'><td nowrap>総合計</TD>";
$i=8;
for ($i = 8; $i <= $nowtime; $i++)
{
	print "<TD align='center'>".($kekka_old[$i][0]+$kekka_new[$i][0])."</TD>";
	print "<TD align='center'>".($kekka_old[$i][1]+$kekka_new[$i][1])."</TD>";
}
print "<td>".($kekkatotal_old+$kekkatotal_new)."　</td></tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>