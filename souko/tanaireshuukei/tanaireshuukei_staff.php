<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>本日の棚入れ作業状況(ゾーン毎棚入れ集計)</title>
</head>
<body>
<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$nowtime= date("H");

$sql = "select r.staffcode, f.staffname, to_number(r.tanairetime),to_number(r.tanaireminute),count(*),r.zonecode ".
"from ( ".
"select ".
"substr(s.modifytime,1,2) as tanairetime,m.zonecode, ".
"case when substr(s.modifytime,3,2)<31 then 0 ".
"else 1  ".
"end as tanaireminute,s.newused_kind, staffcode ".
"from d_tana_goods s,m_location m ".
"where substr(s.locationcode,1,1)<'9'  and tana_kind in ('51','52','55') ".
"and s.locationcode=m.locationcode  ".
"and s.modifydate=(select to_char(sysdate,'yyyymmdd') from dual) ".
") r, m_staff f ".
"where r.staffcode = f.staffcode ".
"group by r.staffcode, f.staffname, r.tanairetime,r.tanaireminute,r.zonecode ".
"order by r.staffcode, f.staffname, r.tanairetime,r.tanaireminute,r.zonecode";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>　<BR>本日の棚入れ作業状況(ゾーン毎棚入れ集計)</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td colspan=5></td>";
for ($c = 9; $c <= $nowtime; $c++) {
print "<td colspan=2>".$c."時</td>";
}
print "</tr><tr bgcolor=#ccffff>\n";
print "<td nowrap>スタッフコード</td><td nowrap>スタッフ名</td><td nowrap>総計</td><td nowrap>ゾーン</td><td nowrap>合計</td>";
for ($c = 9; $c <= $nowtime; $c++) {
print "<td nowrap>00-30</td><td nowrap>31-60</td>";
}
print "</tr>";
$total_count=0;
$i=0;
$motostaffcode="0";
for($cc = 1; $cc <= 24; $cc++) {
	$jikantotal[$cc][0]=0;
	$jikantotal[$cc][1]=0;
}
//取得データのセット
while($row = $res->fetchRow()){
	$total_count=$total_count+$row[4];
	if ($motostaffcode==($row[0]+0))
	{
	}else
	{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$staffzonecnt_a[$i]=0;
		$staffzonecnt_b[$i]=0;
		$staffzonecnt_c[$i]=0;
		$staffzonecnt_d[$i]=0;
		$staffzonecnt_e[$i]=0;
		$staffzonecnt_g[$i]=0;
		$staffzonecnt[$i]=0;
		$stafftotal[$i]=0;
		$staffzonetotal_a[$i]=0;
		$staffzonetotal_b[$i]=0;
		$staffzonetotal_c[$i]=0;
		$staffzonetotal_d[$i]=0;
		$staffzonetotal_e[$i]=0;
		$staffzonetotal_g[$i]=0;
//集計値初期化
		for($cc = 1; $cc <= 24; $cc++) {
			$forhtml_a[($i)][($cc)][0]=0;
			$forhtml_a[($i)][($cc)][1]=0;
			$forhtml_b[($i)][($cc)][0]=0;
			$forhtml_b[($i)][($cc)][1]=0;
			$forhtml_c[($i)][($cc)][0]=0;
			$forhtml_c[($i)][($cc)][1]=0;
			$forhtml_d[($i)][($cc)][0]=0;
			$forhtml_d[($i)][($cc)][1]=0;
			$forhtml_e[($i)][($cc)][0]=0;
			$forhtml_e[($i)][($cc)][1]=0;
			$forhtml_g[($i)][($cc)][0]=0;
			$forhtml_g[($i)][($cc)][1]=0;
		}
	}

//数値に変換するために+0してます
	switch ($row[5]){
	case "A";
//staffcode,時間,分
		$forhtml_a[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_a[$i]=$staffzonetotal_a[$i]+$row[4];
		$staffzonecnt_a[$i]=1;
		break;
	case "B";
		$forhtml_b[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_b[$i]=$staffzonetotal_b[$i]+$row[4];
		$staffzonecnt_b[$i]=1;
		break;
	case "C";
		$forhtml_c[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_c[$i]=$staffzonetotal_c[$i]+$row[4];
		$staffzonecnt_c[$i]=1;
		break;
	case "D";
		$forhtml_d[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_d[$i]=$staffzonetotal_d[$i]+$row[4];
		$staffzonecnt_d[$i]=1;
		break;
	case "E";
		$forhtml_e[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_e[$i]=$staffzonetotal_e[$i]+$row[4];
		$staffzonecnt_e[$i]=1;
		break;
	case "G";
		$forhtml_g[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_g[$i]=$staffzonetotal_g[$i]+$row[4];
		$staffzonecnt_g[$i]=1;
		break;
	}
	$stafftotal[$i]=$stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][($row[3]+0)]=$jikantotal[($row[2]+0)][($row[3]+0)]+$row[4];
}
//HTML表示
for($ii = 1; $ii <= $i; $ii++) {
	$headerflg=0;
	$staffzonecnt[$ii]=$staffzonecnt_a[$ii]+$staffzonecnt_b[$ii]+$staffzonecnt_c[$ii]+$staffzonecnt_d[$ii]+$staffzonecnt_e[$ii]+$staffzonecnt_g[$ii];
	print "<tr><td rowspan=".		$staffzonecnt[$ii].">".$staffcode[$ii]."</td><td nowrap rowspan=".		$staffzonecnt[$ii].">".$staffname[$ii]."</td><td rowspan=".		$staffzonecnt[$ii].">".$stafftotal[$ii]."</td>";
//Azone
	if ($staffzonetotal_a[$ii]>0)
	{
		print "<td>A</td><td>".$staffzonetotal_a[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			print "<td>".$forhtml_a[$ii][$c][0]."</td>";
			print "<td>".$forhtml_a[$ii][$c][1]."</td>";
		}
		print "</tr>";
		$headerflg=1;
	}
//Bzone
	if ($staffzonetotal_b[$ii]>0)
	{
		if ($headerflg>0){print "<tr>";}
		print "<td>B</td><td>".$staffzonetotal_b[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			print "<td>".$forhtml_b[$ii][$c][0]."</td>";
			print "<td>".$forhtml_b[$ii][$c][1]."</td>";
		}
		print "</tr>";
		$headerflg=1;
	}
//Czone
	if ($staffzonetotal_c[$ii]>0)
	{
		if ($headerflg>0){print "<tr>";}
		print "<td>C</td><td>".$staffzonetotal_c[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			print "<td>".$forhtml_c[$ii][$c][0]."</td>";
			print "<td>".$forhtml_c[$ii][$c][1]."</td>";
		}
		print "</tr>";
		$headerflg=1;
	}
//Dzone
	if ($staffzonetotal_d[$ii]>0)
	{
		if ($headerflg>0){print "<tr>";}
		print "<td>D</td><td>".$staffzonetotal_d[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			print "<td>".$forhtml_d[$ii][$c][0]."</td>";
			print "<td>".$forhtml_d[$ii][$c][1]."</td>";
		}
		print "</tr>";
		$headerflg=1;
	}
//Ezone
	if ($staffzonetotal_e[$ii]>0)
	{
		if ($headerflg>0){print "<tr>";}
		print "<td>E</td><td>".$staffzonetotal_e[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			print "<td>".$forhtml_e[$ii][$c][0]."</td>";
			print "<td>".$forhtml_e[$ii][$c][1]."</td>";
		}
		print "</tr>";
		$headerflg=1;
	}
//Gzone
	if ($staffzonetotal_g[$ii]>0)
	{
		if ($headerflg>0){print "<tr>";}
		print "<td>G</td><td>".$staffzonetotal_g[$ii]."</td>";
		for ($c = 9; $c <= $nowtime; $c++) {
			print "<td>".$forhtml_g[$ii][$c][0]."</td>";
			print "<td>".$forhtml_g[$ii][$c][1]."</td>";
		}
		print "</tr>";
		$headerflg=1;
	}
}
print "<tr><td colspan=4 align=center>棚入れ点数合計</td><td>".$total_count."</td>";
for ($c = 9; $c <= $nowtime; $c++) {
	print "<td>".$jikantotal[$c][0]."</td>";
	print "<td>".$jikantotal[$c][1]."</td>";
}
print "</tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>