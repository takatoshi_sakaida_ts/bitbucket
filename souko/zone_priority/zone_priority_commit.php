<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ゾーン優先引当設定変更 完了</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

// 前画面からの入力キーワードを取得する
$zonecode  = addslashes(@$_POST["zonecode"]);
$normal    = addslashes(@$_POST["normal"]);
$tentative = addslashes(@$_POST["tentative"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($zonecode)) {
	print "対象となるゾーンを入力してください！！<br>";
	print "更新ボタンの押下は無効です。<br>";
} else if (empty($normal)) {
	print "優先順位(通常)を入力してください！！<br>";
	print "更新ボタンの押下は無効です。<br>";
} else if (empty($tentative)) {
	print "優先順位(仮)を入力してください！！<br>";
	print "更新ボタンの押下は無効です。<br>";
} else  {
	print "<strong><BR>ゾーン優先引当設定変更 完了</strong><br><HR>";
	//対象ゾーンの優先順位を更新
	$sql = "UPDATE D_ZONE_PRIORITY ".
           "SET NORMAL   = ".$normal.
           ", TENTATIVE  = ".$tentative.
           ", MODIFYDATE = '".date('Ymd')."'".
           ", MODIFYTIME = '".date('His')."'".
           " WHERE ZONECODE = '".$zonecode."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	$sql = "SELECT ZONECODE, NORMAL, TENTATIVE FROM D_ZONE_PRIORITY ORDER BY ZONECODE";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff><th width=100>ゾーン        </th>";
	print "<th width=120>                    優先順位(通常)</th>";
	print "<th width=120>                    優先順位(仮)  </th></tr>\n";
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$total_shuka=0;
	while($row = $res->fetchRow()){
		print "<tr>\n";
		print "<td align='center'>".$row[0]."</td>\n";
		print "<td align='center'>".$row[1]."</td>\n";
		print "<td align='center'>".$row[2]."</td>\n";
		print "</tr>\n";
	}
	print "</table>";

	//データの開放
	$res->free();
	$db->disconnect();
}
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
