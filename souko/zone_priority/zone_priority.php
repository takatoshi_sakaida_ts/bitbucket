<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ゾーン優先引当設定変更</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

$sql = "SELECT ZONECODE, NORMAL, TENTATIVE FROM D_ZONE_PRIORITY ORDER BY ZONECODE";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>ゾーン優先引当設定変更</strong><br><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff><th width=100>ゾーン        </th>";
print "<th width=120>                    優先順位(通常)</th>";
print "<th width=120>                    優先順位(仮)  </th></tr>\n";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>\n";
	print "<td align='center'>".$row[0]."</td>\n";
	print "<td align='center'>".$row[1]."</td>\n";
	print "<td align='center'>".$row[2]."</td>\n";
	print "</tr>\n";
}
print "</table>";

//データの開放
$res->free();
$db->disconnect();

print "<BR>";
print "＜更新禁止時間＞<BR>";
print "　00：15 〜 00：35<BR>";
print "　01：20 〜 01：40<BR>";
print "　07：30 〜 07：50<BR>";
print "　09：50 〜 10：10<BR>";
print "　10：50 〜 11：10<BR>";
print "　11：50 〜 12：10<BR>";
print "　12：50 〜 13：10<BR>";
print "　14：50 〜 15：10<BR>";
print "　15：50 〜 16：10<BR>";
print "　16：50 〜 17：10<BR>";
print "　18：50 〜 19：10<BR>";
print "　21：20 〜 21：40<BR>";

?>

<BR>
<FORM ACTION="/index.php"><INPUT TYPE="submit" VALUE="戻る">
<BR><BR>
</FORM>

<FORM METHOD="POST" ACTION="zone_priority_commit.php">
ゾーン<BR><input type="text" name="zonecode" value=""><BR><BR>
優先順位(通常)<BR><input type="text" name="normal" value=""><BR><BR>
優先順位(仮)<BR><input type="text" name="tentative" value=""><BR><BR>
<INPUT TYPE="SUBMIT" VALUE="更新">
</FORM>

</body>
</html>
