<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>本人確認待ち一覧画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
//ログイン情報の読み込み
require_once("../parts/selectvalue_souko.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
$i=0;
$j="ng";
$sql="select t.pickup_nm_fst,t.pickup_nm_mid,t.sell_stat,t.sell_no,m.ass_dt,t.receipt_tp,t.MEM_ID,t.PICKUP_TEL_NO_2 from tsell t,tsellassessment m where t.sell_no=m.sell_no and (form_arrival='0' or form_arrival is null) and m.ass_dt is not null and t.sell_stat<'06' order by t.sell_no";
//$sql="select t.pickup_nm_fst,t.pickup_nm_mid,t.sell_stat,t.sell_no,m.ass_dt,t.receipt_tp,t.MEM_ID,t.PICKUP_TEL_NO_2 from tsell t,tsellassessment m where t.sell_no=m.sell_no and substr(t.sell_no,1,9)='Z20110411' and t.asses_cnt>'0' and t.sell_stat<'06'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$j="ok";
	$kaitori_name[$i]=$row[0]."　".$row[1];
	$kaitori_stat[$i]=$row[2];
	$kaitori_sell_no[$i]=$row[3];
	$kaitori_ass_dt[$i]=$row[4];
	$kaitori_receipt_tp[$i]=$row[5];
	$kaitori_mem_id[$i]=$row[6];
	$kaitori_tel[$i]=$row[7];
	$i++;
}
//データの開放
$res->free();
$db->disconnect();
//画面表示
print "<strong>本人確認待ち一覧</strong> <br><br>\n";
if ($j!="ng")
{
	$chk_flg="0";
	$i++;
	//検索結果の表示
	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td>　</td><td>買取受付番号</td><td>受付方法</td><td>氏名</td><td>計算日</td><td>会員ID</td><td>日中連絡先</td></tr>";
	for ($i = 0 ; $i <count($kaitori_sell_no); $i++) {
		print "<tr>";
		print "<td>".($i+1)."</td>";
		print "<td>".$kaitori_sell_no[$i]."</td>";
		print "<td>".Retkeiroec($kaitori_receipt_tp[$i])."</td>";
		print "<td>".$kaitori_name[$i]."</td>";
		print "<td>".$kaitori_ass_dt[$i]."</td>";
		print "<td>".$kaitori_mem_id[$i]."　</td>";
		print "<td>".$kaitori_tel[$i]."</td>";
		print "</TR>";
	}
	print "</table>";
}else
{
	$chk_flg="1";
	print "　　本人確認待ちは、ありません";

}
?>
<BR><BR>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
</FORM>
</body>
</html>