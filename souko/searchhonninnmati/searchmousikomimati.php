<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取申込書待ち一覧画面（計算結果あり）</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
//ログイン情報の読み込み
require_once("../parts/selectvalue_souko.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$today = date("YmdHis");
$file_name ="./dlfile/FD_SAIKOKU_" . $today . ".csv";
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
$i=0;
$j="ng";
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$sql="select t.pickup_nm_fst,t.pickup_nm_mid,t.sell_stat,t.sell_no,m.ass_dt,t.receipt_tp,t.MEM_ID,t.PICKUP_TEL_NO_2,"
."t.PICKUP_NM_LAST||t.PICKUP_NM_ETC,m.VALID_ITEM_CNT,m.TOT_ASS_AMT,t.PICKUP_ZIP_CD,"
."t.PICKUP_ZIP_ADDR1||t.PICKUP_ZIP_ADDR2||', '||t.PICKUP_ZIP_ADDR3||','||t.PICKUP_DTL_ADDR"
." from tsell t,tsellassessment m where t.sell_no=m.sell_no and ((form_arrival='0' or form_arrival is null) "
." and BANK_ACC_NO is null and POSTAL_ACC_NO is null and RECEIPT_TP = '2' and t.sell_stat='02' "
. "and substr(t.sell_no,0,2) in ('Z2','Z1','Z6','Z8') and m.ASS_DT<'".date('Ymd',$date_today - (86400*2))
."' and to_date(substr(t.REG_DM,1,8),'yyyymmdd')<'".date('Ymd',$date_today - (86400*9)) 
."' and sell_stat <'06')order by t.sell_no";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$str="";
$fp = fopen($file_name , "w" );
while($row = $res->fetchRow()){
	$j="ok";
	$kaitori_name[$i]=$row[0]."　".$row[1];
	$kaitori_stat[$i]=$row[2];
	$kaitori_sell_no[$i]=$row[3];
	$kaitori_ass_dt[$i]=$row[4];
	$kaitori_receipt_tp[$i]=$row[5];
	$kaitori_mem_id[$i]=$row[6];
	$kaitori_tel[$i]=$row[7];
//ファイル出力
	$str=$kaitori_sell_no[$i] . ",". $kaitori_name[$i] . ",". $row[8] . ",". $row[9] . ",". $row[10] . ",". $kaitori_ass_dt[$i] . ",". $row[11] . ",". $row[12] . "\r\n";
	fputs($fp,$str);
	$i++;
}
//データの開放
$res->free();
$db->disconnect();
fclose( $fp );
//画面表示
print "<strong>買取申込書待ち一覧</strong> <br><br>\n";
if ($j!="ng")
{
	$chk_flg="0";
	//検索結果の表示
	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td>　</td><td>買取受付番号</td><td>受付方法</td><td>氏名</td><td>計算日</td><td>日中連絡先</td></tr>";
	for ($i = 0 ; $i <count($kaitori_sell_no); $i++) {
		print "<tr>";
		print "<td>".($i+1)."</td>";
		print "<td>".$kaitori_sell_no[$i]."</td>";
		print "<td>".Retkeiroec($kaitori_receipt_tp[$i])."</td>";
		print "<td>".$kaitori_name[$i]."</td>";
		print "<td>".$kaitori_ass_dt[$i]."</td>";
		print "<td>".$kaitori_tel[$i]."</td></TR>";
	}
	print "</table>";
	print "<BR><a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR>";
}else
{
	$chk_flg="1";
	print "　　買取申込書待ちはありません";

}
?>
<BR><BR>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
</FORM>
</body>
</html>