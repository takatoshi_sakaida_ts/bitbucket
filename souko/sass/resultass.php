<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "インストアコードが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select instorecode,modifydate,detailqty,tax_price from d_ass_goods where instorecode = '".$keyti."' order by MODIFYDATE";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>検索結果 インストアコード：". $keyti."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td>インストアコード</td><td>計算日付</td><td>数量</td><td>計算金額</td></tr>";
$j=1;
$d=0;
$a=0;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>". $j."</td>";
//インストアコード
	print "<td>".$row[0] ."</td>";
//計算日付
	print "<td>".$row[1] ."</td>";
//計算数量
	print "<td>".$row[2] ."</td>";
//計算金額
	print "<td>".$row[3] ."</td>";
	$j++;
	$d = $d + $row[2];
	$a = $a + $row[3];
	print "</TR>";
}
print "<TR><TD>　</TD><TD>　</TD><TD>　</TD><TD>" . $d . "</TD><TD>" . $a . "</TD></TR>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>