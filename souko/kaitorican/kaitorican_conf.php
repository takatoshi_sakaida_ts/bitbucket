<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>倉庫システム買取受付キャンセル入力画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
$j="ng";
$sql="select pickup_nm_fst,pickup_nm_mid,pickup_nm_last,pickup_nm_etc,sell_stat from tsell where sell_no = '" . $keyti . "'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$j="ok";
	$kaitori_name=$row[0]."　".$row[1];
	$kaitori_name_kana=$row[2]."　".$row[3];
	$kaitori_stat=$row[4];
}
if ($j!="ng")
{
	$chk_flg="0";
	//検索結果の表示
	print "<strong>買取受付番号：". $keyti."</strong> <br><br>\n";
	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td>買取受付番号</td><td>氏名</td><td>カナ</td></tr>";
	print "<tr>";
	print "<td>".$keyti."</td>";
	print "<td>".$kaitori_name."</td>";
	print "<td>".$kaitori_name_kana."</td>";
	print "</TR></table>";
	//データの開放
	$res->free();
	$db->disconnect();
	if ($kaitori_stat=='99'){
	}else
	{
		$chk_flg="1";
		print "<br>ECシステムがキャンセルされていません";
	}
}
else
{
	$chk_flg="1";
	print "買取受付番号に誤りがあります:".$keyti;
}
?>
<BR>
<form method="POST" action="kaitorican_ent.php" name ="kaitorican">
<?PHP
if ($chk_flg=="0")
{
$_SESSION["HSMAP"]["sell_no"]='000'.substr($keyti,1,12);
$_SESSION["HSMAP"]["sell_no_ec"]=$keyti;
//倉庫システムの状態チェック
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
output_log($sql,$_SESSION["user_id"],'kaitorican_update_confirm_sql');
$sql = "select * from d_ass_request where asscode = '".$_SESSION["HSMAP"]["sell_no"]."'";
output_log($sql,$_SESSION["user_id"],'kaitorican_update_confirm_sql');
$res2 = $db->query($sql);
$i=0;
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res2->fetchRow()){
	$i++;
}
//データの開放
$res2->free();
if ($i==0){
	$chk_flg="1";
	print "倉庫システムにIFされていません";
}else
{
	$sql = "select * from d_ass where asscode = '".$_SESSION["HSMAP"]["sell_no"]."'";
	output_log($sql,$_SESSION["user_id"],'kaitorican_update_confirm_sql');
	$res3 = $db->query($sql);
	$i=0;
	if(DB::isError($res3)){
		$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	while($row = $res3->fetchRow()){
		$i++;
	}
	if ($i!==0){
		$chk_flg="1";
		print "すでに計算済です";
	}
	$res3->free();
}
$db->disconnect();
}
//ボタン表示チェック
if ($chk_flg=="0")
{
	print '<input type="submit" value="　　倉庫システム買取キャンセル　　">';
//	print "<br>".$_SESSION["HSMAP"]["sell_no"];
}
?>
<br><BR>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
</FORM>
</body>
</html>