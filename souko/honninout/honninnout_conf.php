<?php
require '../parts/pagechk.inc';
$today = date("YmdHis");
$file_name = "./dlfile/honningentei_" . $today . ".tsv";
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");

function outputcsv_tab($tempres,$tempfilename){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $tempfilename, "w" );
//ヘッダ分記述
fputs($fp,"買取受付番号	会員番号	ユーザID	集荷先姓(漢字)	集荷先名(漢字)	集荷先姓(フリガナ)	集荷先名(フリガナ)	集荷先郵便番号	集荷先都道府県	集荷先市区町村	集荷先番地	集荷先建物名部屋番号	集荷先電話番号	確認番号");
fputs($fp,"\r\n");
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $tempres->fetchRow()){
	$str="";
	for($i=0;$i<$ncols;$i++){
		$str=$str . $row[$i] . '	';//行の列データを取得
	}
//	$str = ereg_replace("\t","",$str);
	fputs($fp,$str);
	fputs($fp,"\r\n");
}
fclose( $fp );
}

// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
}
$new_text = preg_replace("/(\r\n|\n|\r)/", "','", $keyti);
$new_text = preg_replace("/,,/", "", $new_text);
$new_text = preg_replace("/','$/", "", $new_text);
$new_text = strtoupper($new_text);
$new_text = "'".$new_text."'";

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}

$sql = "SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,T.PICKUP_NM_FST,T.PICKUP_NM_MID,T.PICKUP_NM_LAST,T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,T.PICKUP_ZIP_ADDR1,T.PICKUP_ZIP_ADDR2,T.PICKUP_ZIP_ADDR3,T.PICKUP_DTL_ADDR,T.PICKUP_TEL_NO,".
"T.AUTH_KEY_NO FROM TSELL T WHERE T.SELL_NO IN (".$new_text.") AND T.RECEIPT_TP='2' union ".
"SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,A.RELR_NM_FST,A.RELR_NM_MID,A.RELR_NM_LAST,A.RELR_NM_ETC,A.ZIP_CD,A.ZIP_ADDR1,".
"A.ZIP_ADDR2,A.ZIP_ADDR3,A.DTL_ADDR,A.TEL_NO,T.AUTH_KEY_NO FROM TSELL T,TMEMADDR A WHERE T.MEM_NO=A.MEM_NO ".
"AND T.SELL_NO IN (".$new_text.") AND A.ADDR_SEQ='0' AND T.RECEIPT_TP!='2' AND SUBSTR(T.SELL_NO,1,1)='Z' union ".
"SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,T.PICKUP_NM_FST,T.PICKUP_NM_MID,T.PICKUP_NM_LAST,T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,T.PICKUP_ZIP_ADDR1,T.PICKUP_ZIP_ADDR2,T.PICKUP_ZIP_ADDR3,T.PICKUP_DTL_ADDR,T.PICKUP_TEL_NO,".
"T.AUTH_KEY_NO FROM TSELL T WHERE T.SELL_NO IN (".$new_text.") AND SUBSTR(T.SELL_NO,1,1)!='Z'";

//print $sql;

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	outputcsv_tab($res,$file_name);
$res->free();
//
$db->disconnect();
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>本人確認発送用登録データ作成受付画面</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
?>
<FORM action="/index.php"><INPUT TYPE="submit" VALUE="　戻る　"></FORM>
</body>
</html>
