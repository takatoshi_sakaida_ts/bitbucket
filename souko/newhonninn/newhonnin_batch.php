<?PHP
print "買取受領チェック・本人確認一括処理開始".date("YmdHis")."\r\n";
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../log/loger.inc");
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db2 = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db2)){
	echo "Fail\n" . DB::errorMessage($db2) . "\n";
}
$db2->autoCommit( false ); 
//変数
$check=0;
$check_ok=0;
$check_ng=0;
$check_kouza_ng=0;
$check_stop=0;
$check_stop_ok=0;
$check_stop_ng=0;
$check_staterr=0;
$check_stop_staterr=0;
$settoday = date("Ymd");
$today = date("YmdHis");
$file_name_ok = "./dlfile/keisan_ok_" . $today . ".csv";
$file_name_ng = "./dlfile/keisan_ng_" . $today . ".csv";
$file_name= "./dlfile/honningentei_" . $today . ".tsv";
$file_name_kouzaerr= "./dlfile/kouzaerr_" . $today . ".csv";
$file_name_stop_ok = "./dlfile/keisan_stop_ok_" . $today . ".csv";
$file_name_stop_ng = "./dlfile/keisan_ng_stop_" . $today . ".csv";
$file_name_staterr = "./dlfile/keisan_staterr_" . $today . ".csv";
$file_name_stop_staterr = "./dlfile/keisan_stop_staterr_" . $today . ".csv";
//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres."\n";
	fputs($fp,$str);
	fclose( $fp );
}
function outputcsv_tab($tempres,$tempfilename,$sw){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $tempfilename, "a");
//ヘッダ分記述
if ($sw=='0')
{
	fputs($fp,"買取受付番号	会員番号	ユーザID	集荷先姓(漢字)	集荷先名(漢字)	集荷先姓(フリガナ)	集荷先名(フリガナ)	集荷先郵便番号	集荷先都道府県	集荷先市区町村	集荷先番地	集荷先建物名部屋番号	集荷先電話番号	確認番号");
	fputs($fp,"\r\n");
}
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $tempres->fetchRow()){
	$str="";
	for($i=0;$i<$ncols;$i++){
		$str=$str . $row[$i] . '	';//行の列データを取得
	}
	if (substr($row[0],0,2)=="Z2"||substr($row[0],0,2)=="Z6"||substr($row[0],0,2)=="Z8"||substr($row[0],0,2)=="Z9"||substr($row[0],0,2)=="R3")
	{
		fputs($fp,$str);
		fputs($fp,"\r\n");
	}
}
fclose( $fp );
}
function outputcsv_kouzang($tempres,$tempfilename,$t){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $tempfilename, "a");
$fp2 = fopen( str_replace(".csv",".log",$tempfilename), "a");

if ($t==0)
{
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	fputs($fp,"買取受付番号,申込経路,口座名義人名,申込者名,会員番号,口座番号");
	fputs($fp,"\r\n");
}
while($row = $tempres->fetchRow()){
	$str="";
	for($i=0;$i<$ncols;$i++){
		$str=$str . $row[$i] . ',';//行の列データを取得
	}
//口座番号が入力されていればkouzaerr.csvに出力
if (substr($row[0],0,2)=="Z2"||substr($row[0],0,2)=="Z9"||substr($row[0],0,2)=="Z8"||substr($row[0],0,2)=="Z6"||substr($row[0],0,2)=="R3")
{
	if (isset($row[5]))
	{
		fputs($fp,$str);
		fputs($fp,"\r\n");
	}
}
//kouzaerr.logに出力
	fputs($fp2,$str);
	fputs($fp2,"\r\n");
}
fclose( $fp );
fclose( $fp2 );
}
//ファイル読み込み
$contents = @file('keisan.csv');
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
}else{
	foreach($contents as $readkeisanline){
	$print_log="";
//		print $readkeisanline."<br />";
	$sql="SELECT T.SELL_NO,receipt_tp,".
	"(case when t.pay_tp='1' then T.BANK_ACC_NM ".
	"when t.pay_tp='2' then T.POSTAL_NM ".
	"ELSE 'NG' ".
	"end),".
	"(case when t.receipt_tp='1' then M.MEM_NM_LAST||M.MEM_NM_ETC ".
	"when t.receipt_tp='2' then t.pickup_NM_LAST||t.pickup_NM_ETC ".
	"ELSE 'NG' ".
	"end),".
	"m.auth_stat,t.form_arrival,m.mem_no,t.ASSES_CNT,t.sell_stat ".
	"FROM TSELL T,TMEMBER M where ".
	"T.MEM_NO=M.MEM_NO(+) and t.sell_no =".$readkeisanline;
//	print $sql;
	$res = $db2->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
		while($row = $res->fetchRow()){
//			print $row[0].":".$row[2].":";
			$print_log=$row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8];

		if ($row[7]=="0"&& $row[8]<"06")//ステータス、計算回数チェック
		{
			if (($row[2]===$row[3])||(substr($row[0],0,3)==='Z88'))//口座チェック・出張買取なら口座チェックなし
			{
				$sql="update tsell set form_arrival='1',SELL_FORM_GET_DT='".$settoday."' where sell_no=".$readkeisanline;
				$res2 = $db2->query($sql);
				if(DB::isError($res2)){
					print $res2->getMessage();
					$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
				}
				output_log($sql,'batch','keisansumi_update_sql');
				$db2->commit();

		//webは、本人確認を済にする
				if ($row[1]=='1'){
					$sql="update tmember set auth_stat='1' where mem_no=".$row[6];	
					$res2 = $db2->query($sql);
					if(DB::isError($res2)){
						print $res2->getMessage();
						$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
					}
					output_log($sql,'batch','keisansumi_update_sql');
					$db2->commit();
				}
				outputcsv($print_log,$file_name_ok);
	//			print "OK";
				$check_ok++;
			}else//口座チェックＮＧ
			{
				if($row[4]=='1')//口座は、ＮＧだが本人確認済をＯＫにする
				{
					$sql="update tsell set form_arrival='1',SELL_FORM_GET_DT='".$settoday."' where sell_no=".$readkeisanline;
					$res2 = $db2->query($sql);
					if(DB::isError($res2)){
						print $res2->getMessage();
						$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
					}
					output_log($sql,'batch','keisansumi_update_sql');
					$db2->commit();
	//				print "-------------------------------OK";
					outputcsv($print_log.":".$row[6],$file_name_ok);
					$check_ok++;
				}else//本人確認発送用ファイル出力
				{
/*
					$sql = "SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,T.PICKUP_NM_FST,T.PICKUP_NM_MID,T.PICKUP_NM_LAST,T.PICKUP_NM_ETC,".
					"T.PICKUP_ZIP_CD,T.PICKUP_ZIP_ADDR1,T.PICKUP_ZIP_ADDR2,T.PICKUP_ZIP_ADDR3,T.PICKUP_DTL_ADDR,T.PICKUP_TEL_NO,".
					"T.AUTH_KEY_NO FROM TSELL T WHERE T.SELL_NO =".$readkeisanline." AND T.RECEIPT_TP='2' union ".
					"SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,A.RELR_NM_FST,A.RELR_NM_MID,A.RELR_NM_LAST,A.RELR_NM_ETC,A.ZIP_CD,A.ZIP_ADDR1,".
					"A.ZIP_ADDR2,A.ZIP_ADDR3,A.DTL_ADDR,A.TEL_NO,T.AUTH_KEY_NO FROM TSELL T,TMEMADDR A WHERE T.MEM_NO=A.MEM_NO ".
					"AND T.SELL_NO =".$readkeisanline." AND A.ADDR_SEQ='0' AND T.RECEIPT_TP!='2' ";
*/
					$sql="SELECT T.SELL_NO,receipt_tp,".
					"(case when t.pay_tp='1' then T.BANK_ACC_NM ".
					"when t.pay_tp='2' then T.POSTAL_NM ".
					"ELSE 'NG' ".
					"end),".
					"(case when t.receipt_tp='1' then M.MEM_NM_LAST||M.MEM_NM_ETC ".
					"when t.receipt_tp='2' then t.pickup_NM_LAST||t.pickup_NM_ETC ".
					"ELSE 'NG' ".
					"end),".
					"m.mem_no,".
					"(case when t.pay_tp='1' then T.BANK_ACC_NO ".
					"when t.pay_tp='2' then T.POSTAL_ACC_NO ".
					"ELSE NULL ".
					"end) ".
					"FROM TSELL T,TMEMBER M where ".
					"T.MEM_NO=M.MEM_NO(+) and t.sell_no =".$readkeisanline;
					$res3 = $db2->query($sql);
					if(DB::isError($res3)){
						$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
					}
					outputcsv_kouzang($res3,$file_name_kouzaerr,$check_kouza_ng);
					$res3->free();
					outputcsv($print_log,$file_name_ng);
	//				print "NG";
					$check_kouza_ng++;
				}
			}
		}else//ステータスエラー・または計算結果エラー
		{
			$check_staterr++;
			outputcsv($print_log,$file_name_staterr);
		}
//	print "";
	}
	$res->free();
	$check++;
}
print "【".$check."件の計算結果があります】\r\n";
print $check_ok."件の本人確認状態ＯＫ計算結果があります。\r\n";
print $check_kouza_ng."件の本人確認待ち計算結果があります。\r\n";
print $check_staterr."件のステータスエラー・再計算結果があります。\r\n";
}
//一時停止ファイル
//ファイル読み込み
$check_stop_ngsw=$check_ng;
$contents = @file('keisan_stop.csv');
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
}else
{
	foreach($contents as $readkeisanline){
		$sql="SELECT T.SELL_NO,receipt_tp,M.AUTH_STAT,t.ASSES_CNT,t.sell_stat ".
		"FROM TSELL T,TMEMBER M ".
		"WHERE ".
		"T.MEM_NO=M.MEM_NO(+) ".
		"and sell_no =".$readkeisanline;
//	print $sql."";
		$res = $db2->query($sql);
		if(DB::isError($res)){
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}

		while($row = $res->fetchRow()){
//			print $row[0].":".$row[1].":".$row[2]."";
			$print_log=$row[0].",".$row[1].",".$row[2];
			if ($row[1]=="1" && $row[2]=="1"&& $row[3]=="0"&& $row[4]<"06")//WEB申込で本人確認済・計算回数・ステータスチェック
			{
				$sql="update tsell set form_arrival='1',SELL_FORM_GET_DT='".$settoday."' where sell_no=".$readkeisanline;
				$res2 = $db2->query($sql);
				if(DB::isError($res2)){
					print $res2->getMessage();
					$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
				}
				output_log($sql,'batch','keisansumi_update_sql');
				outputcsv($print_log,$file_name_stop_ok);
				$db2->commit();
				$check_stop_ok++;
			}else
			{
				if ($row[3]=="0"&& $row[4]<"06")//計算回数・ステータスチェック
				{
					$sql = "SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,T.PICKUP_NM_FST,T.PICKUP_NM_MID,T.PICKUP_NM_LAST,T.PICKUP_NM_ETC,".
					"T.PICKUP_ZIP_CD,T.PICKUP_ZIP_ADDR1,T.PICKUP_ZIP_ADDR2,T.PICKUP_ZIP_ADDR3,T.PICKUP_DTL_ADDR,T.PICKUP_TEL_NO,".
					"T.AUTH_KEY_NO FROM TSELL T WHERE T.SELL_NO =".$readkeisanline." AND T.RECEIPT_TP='2' union ".
					"SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,A.RELR_NM_FST,A.RELR_NM_MID,A.RELR_NM_LAST,A.RELR_NM_ETC,A.ZIP_CD,A.ZIP_ADDR1,".
					"A.ZIP_ADDR2,A.ZIP_ADDR3,A.DTL_ADDR,A.TEL_NO,T.AUTH_KEY_NO FROM TSELL T,TMEMADDR A WHERE T.MEM_NO=A.MEM_NO ".
					"AND T.SELL_NO =".$readkeisanline." AND A.ADDR_SEQ='0' AND T.RECEIPT_TP!='2' AND SUBSTR(T.SELL_NO,1,1)='Z' union ".
					"SELECT T.SELL_NO,T.MEM_NO,T.MEM_ID,T.PICKUP_NM_FST,T.PICKUP_NM_MID,T.PICKUP_NM_LAST,T.PICKUP_NM_ETC,".
					"T.PICKUP_ZIP_CD,T.PICKUP_ZIP_ADDR1,T.PICKUP_ZIP_ADDR2,T.PICKUP_ZIP_ADDR3,T.PICKUP_DTL_ADDR,T.PICKUP_TEL_NO,".
					"T.AUTH_KEY_NO FROM TSELL T WHERE T.SELL_NO =".$readkeisanline." AND SUBSTR(T.SELL_NO,1,1)!='Z'";
					$res3 = $db2->query($sql);
					if(DB::isError($res3)){
						$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
					}
					outputcsv_tab($res3,$file_name,$check_stop_ngsw);
					$res3->free();
					outputcsv($print_log,$file_name_stop_ng);
	//				print $sql."";
	//				print "STOP_NG"."";
					$check_stop_ng++;
					$check_stop_ngsw++;
				}else//ステータスエラーまたは計算回数エラー
				{
					$check_stop_staterr++;
					outputcsv($print_log,$file_name_stop_staterr);
				}
			}
		}
	//データの開放
	$res->free();
//	print "";
	$check_stop++;
	}
}
print $check_stop."件の一時停止中計算結果があります】\r\n";
print $check_stop_ok."件の本人確認状態ＯＫ計算結果があります。\r\n";
print $check_stop_ng."件の本人確認待ち計算結果があります。\r\n";
print $check_stop_staterr."件のステータスエラー・再計算結果があります。\r\n";
$logword="計算結果:".$check."件,"."本人確認不要:".$check_ok."件,要本人確認:".$check_kouza_ng."件,ステータスエラー：".$check_staterr."件";
$logword=$logword.",一時停止中計算結果:".$check_stop."件,本人確認不要:".$check_stop_ok."件,要本人確認:".$check_stop_ng."件,ステータスエラー：".$check_stop_staterr."件";
//今回のログ出力
output_log($logword,'batch','searchhonninn');
$db2->disconnect();
//計算ファイルのリネーム処理
$filerename=date("YmdHis");
@rename("keisan.csv","keisan_".$filerename.".csv");
@rename("keisan_stop.csv","keisan_stop_".$filerename.".csv");
print "正常終了".date("YmdHis")."\r\n";
?>
