/*2013120411460500*/
update d_stock set locationcode='2200002002' where stockno=139748522;
update d_stock set locationcode='2329005001' where stockno=139857753;
update d_stock set locationcode='2350003004' where stockno=139748524;
update d_stock set locationcode='2379004003' where stockno=139748533;

update d_reception_goods set locationcode='2200002002',picked_kind='0',operatedate=null,operatetime=null,operatestaffcode='999999' where stockno=139748522;
update d_reception_goods set locationcode='2329005001',picked_kind='0',operatedate=null,operatetime=null,operatestaffcode='999999' where stockno=139857753;
update d_reception_goods set locationcode='2350003004',picked_kind='0',operatedate=null,operatetime=null,operatestaffcode='999999' where stockno=139748524;
update d_reception_goods set locationcode='2379004003',picked_kind='0',operatedate=null,operatetime=null,operatestaffcode='999999' where stockno=139748533;

update d_goods_stock set locationcode='2200002002' where instorecode='0016106119' and newused_kind='0' and stockdate='20131209' and qty='1' and RESERVE_QTY='1';
update d_goods_stock set locationcode='2329005001' where instorecode='0015327450' and newused_kind='0' and stockdate='20131210' and qty='1' and RESERVE_QTY='1';
update d_goods_stock set locationcode='2350003004' where instorecode='0015129752' and newused_kind='0' and stockdate='20131209' and qty='1' and RESERVE_QTY='1';
update d_goods_stock set locationcode='2379004003' where instorecode='0015754970' and newused_kind='0' and stockdate='20131209' and qty='1' and RESERVE_QTY='1';

commit;
exit;
