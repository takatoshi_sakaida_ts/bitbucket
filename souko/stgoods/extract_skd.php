<?php
	require_once("../parts/pagechk.inc");
	$fileNamePrefix = "M_SKD_GOODS";
	$ymd = date("YmdHis");
	$fileNameSuffix = ".csv";
	$fileName = $ymd."-".$fileNamePrefix.$fileNameSuffix;
	
	//ファイルの読み込み
	//PEARの利用     -------(1)
	require_once("DB.php");
	//ログイン情報の読み込み
	require_once("../parts/login_souko.php");
	
	//データ取得
	$sql = "select SKDCODE, ORDERSTATUS_KIND, ORDERDISABLEDATE, INSTORECODE, GENRE0CODE, MODIFYDATE, MODIFYTIME, UPDATE_KIND, KINDCODE, ORDER_DEADLINE, DISTRIBUTION_KIND ";
	$sql = $sql."from M_SKD_GOODS where ORDERSTATUS_KIND in (1,2)";
	//$sql = $sql." and rownum < 10";
	//print $sql;
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	
	//ファイル出力
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename=' . $fileName);
	echo "SKDCODE,ORDERSTATUS_KIND,ORDERDISABLEDATE,INSTORECODE,GENRE0CODE,MODIFYDATE,MODIFYTIME,UPDATE_KIND,KINDCODE,ORDER_DEADLINE,DISTRIBUTION_KIND\r\n";
	while($row = $res->fetchRow()){
		$data = $row[0].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8].",".$row[9].",".$row[10]."\r\n";
		echo mb_convert_encoding($data, 'Shift-JIS', 'UTF-8');
	}
	$res->free();
	$db->disconnect();
	exit();
?>