<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$ordNo = trim(addslashes(@$_GET["ordNo"]));
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($ordNo) || is_null($ordNo)){
	print '検索条件を入力してください<br>';
    print '<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>';
    die("");
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
    echo "Fail\n" . DB::errorMessage($db) . "\n";
}

// 決済情報取得SQL
$getPayInfoSql = "select ACCNT_NO, PAYR_NM_MID, CARD_EFF_DT, PG_APPR_NO, PAY_JOB_STAT, PG_PAYMENT_DT2, FIN_INST_NM from TPGPAYDTL where ORD_NO='{$ordNo}'";

// カード会社取得SQL
$getCardCompSql = "select CD_DTL_NM from TCODEDTL where CD_NO = '961' ";

//print $getPayInfoSql;

// 決済情報を取得
$res = $db->query($getPayInfoSql);
if(DB::isError($res)){
    $res->DB_Error($res->getcode(),NULL,NULL,NULL);
}
$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
if(empty($row) || is_null($row)){
	print '<p>検索結果が存在しません。</p>';
    print '<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>';
    die();
}

// 取得データ整形
$payinfo = array();
$payinfo['ACCNT_NO'] = $row['ACCNT_NO'];
$payinfo['CARD_EFF_DT'] = $row['CARD_EFF_DT'];
$payinfo['PG_APPR_NO'] = $row['PG_APPR_NO'];
$payinfo['PAYR_NM_MID'] = $row['PAYR_NM_MID'];
$payinfo['PAY_JOB_STAT'] = $row['PAY_JOB_STAT'];
$payinfo['PG_PAYMENT_DT2'] = $row['PG_PAYMENT_DT2'];
$card_cop_no = $row['FIN_INST_NM'];
$res->free();

// カード会社情報を追加で取得
$execSql = $getCardCompSql . "and CD_DTL_NO = '{$card_cop_no}'";
$res = $db->query($execSql);
if(DB::isError($res)){
    $res->DB_Error($res->getcode(),NULL,NULL,NULL);
}
$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
if(empty($row) || is_null($row)){
    $payinfo['CARD_CO_NO'] = "不明";
} else{
    $payinfo['CARD_CO_NO'] = $row['CD_DTL_NM'];
}

$res->free();

?>

<p>注文番号：<strong><?php echo $ordNo; ?></strong></p>

<table border="0" cellspacing="1" cellpadding="1" >
		<tr><td align="LEFT" class="text1">	<b>[カード決済情報]</b></td>	</tr>
	</table>

	<table width="80%" border="1" cellspacing="1" cellpadding="1">
	<tr>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">カード番号</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo $payinfo['ACCNT_NO']; ?></td>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">有効期限</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo $payinfo['CARD_EFF_DT']; ?></td>
	</tr>
	<tr>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">承認番号</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo $payinfo['PG_APPR_NO']; ?></td>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">注文番号</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo $payinfo['PAYR_NM_MID']; ?></td>
	</tr>
	<tr>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">カード会社</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo $payinfo['CARD_CO_NO']; ?></td>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">決済状態</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo PayjobStatus($payinfo['PAY_JOB_STAT']); ?></td>
	</tr>
	<tr>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">売上確定日</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1"><?php echo $payinfo['PG_PAYMENT_DT2']; ?></td>
	<td align="CENTER" bgcolor="#CCCCCC" class="text1">&nbsp;</td>
	<td align="CENTER" bgcolor="#FFFFFF" class="text1">&nbsp;</td>
	</tr>
	</table>


<?php
	//データの開放
    $db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>