<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出荷個数集計結果</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

// フォーム送信されたかどうか
if( @$_POST["btn_submit"] != "" )
{
	$FromYr  = addslashes(@$_POST["FromYr"]);
	$FromMon = addslashes(@$_POST["FromMon"]);
	$FromD   = addslashes(@$_POST["FromD"]);
	$FromHr  = addslashes(@$_POST["FromHr"]);
	$FromMin = addslashes(@$_POST["FromMin"]);
	$ToYr    = addslashes(@$_POST["ToYr"]);
	$ToMon   = addslashes(@$_POST["ToMon"]);
	$ToD     = addslashes(@$_POST["ToD"]);
	$ToHr    = addslashes(@$_POST["ToHr"]);
	$ToMin   = addslashes(@$_POST["ToMin"]);
}
else
{
	$zentime = strtotime("-1 day");
	$FromYr  = date("Y", $zentime);
	$FromMon = date("m", $zentime);
	$FromD   = date("d", $zentime);
	$FromHr  = "14";
	$FromMin = "00";
	$ToYr    = date("Y");
	$ToMon   = date("m");
	$ToD     = date("d");
	$ToHr    = "14";
	$ToMin   = "00";
}
$FromMon = sprintf("%02d", $FromMon);
$FromD   = sprintf("%02d", $FromD);
$FromHr  = sprintf("%02d", $FromHr);
$FromMin = sprintf("%02d", $FromMin);
$ToMon   = sprintf("%02d", $ToMon);
$ToD     = sprintf("%02d", $ToD);
$ToHr    = sprintf("%02d", $ToHr);
$ToMin   = sprintf("%02d", $ToMin);

//print $FromYr.$FromMon.$FromD.$FromHr.$FromMin."<br>";
//print $ToYr.$ToMon.$ToD.$ToHr.$ToMin;

//SQL文のCOUNT関数を使用
// 意図通りの並び順にするために文字列の先頭に数字を入れてある。
// 表示するときに先頭数字は削除する
$sql =	"select a.transport_kind_nm, a.cnt from ".
		"(select case r.transport_kind".
		"        when '01' THEN '6その他'".
		"        when '03' THEN '4クロネコメール便'".
		"        when '04' THEN '1日本郵便（袋）'".
		"        when '05' THEN '2日本郵便（ダンボール小・茶）'".
		"        when '06' THEN '3日本郵便（ダンボール大・青）'".
		"        when '07' THEN '5ゆうパケット（2〜3cm）'".
		"        when '08' THEN '4ゆうパケット（2cm以下）'".
		"        else r.transport_kind".
		"       end as transport_kind_nm,".
		"       count(r.deliverycode) as cnt".
		"  from d_reception_shipment r".
		"  where substr(r.deliverydatetime, 1, 12) >= '".$FromYr.$FromMon.$FromD.$FromHr.$FromMin."'".
		"  and substr(r.deliverydatetime, 1, 12) < '".$ToYr.$ToMon.$ToD.$ToHr.$ToMin."'".
		"  group by r.transport_kind) a".
		" order by a.transport_kind_nm";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
print "<strong><BR>出荷個数集計結果</strong><br>".date('Y/m/d')."<br><br>\n<HR>";

//検索時間
print "集計期間<br>\n";
print "<form method='POST' action='shukkashuukei_total.php'>\n";
print "<input type='text' name='FromYr'  size=4 maxlength=4 value=".$FromYr.">/\n";
print "<input type='text' name='FromMon' size=2 maxlength=2 value=".$FromMon.">/\n";
print "<input type='text' name='FromD'   size=2 maxlength=2 value=".$FromD.">　\n";
print "<input type='text' name='FromHr'  size=2 maxlength=2 value=".$FromHr.">：\n";
print "<input type='text' name='FromMin' size=2 maxlength=2 value=".$FromMin.">以降<br>\n";

print "<input type='text' name='ToYr'  size=4 maxlength=4 value=".$ToYr.">/\n";
print "<input type='text' name='ToMon' size=2 maxlength=2 value=".$ToMon.">/\n";
print "<input type='text' name='ToD'   size=2 maxlength=2 value=".$ToD.">　\n";
print "<input type='text' name='ToHr'  size=2 maxlength=2 value=".$ToHr.">：\n";
print "<input type='text' name='ToMin' size=2 maxlength=2 value=".$ToMin.">まで<br><br>\n";
print "<input type='submit' name='btn_submit' value='表示'><br><br>\n";
print "</form>\n";

print "<table border=1>\n";
print "<tr bgcolor=#ccffff><th>配送選択</th><th width=70>個数</th></tr>\n";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$total_shuka=0;
while($row = $res->fetchRow()){
	print "<tr>\n";
	// ソート用に名称の先頭に数字を入れているので削除
	print "<td>".substr($row[0], 1)."</td>\n";
	print "<td align='right'>".number_format($row[1]) ."</td>\n";
	print "</tr>\n";
}
print "</table>";

//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='../../index.php'"></FORM>
</body>
</html>
