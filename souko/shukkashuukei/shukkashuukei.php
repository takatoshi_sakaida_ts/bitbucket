<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出荷作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select delvtime,delvminute,count(*) from (".
"select distinct substr(deliverydatetime,9,2) as delvtime,receptioncode,deliverydatetime,".
"case when substr(deliverydatetime,11,2)<31 then 0".
"else 1 ".
"end as delvminute ".
"from d_reception_shipment ".
"where substr(deliverydatetime,1,8)=(select to_char(sysdate,'yyyymmdd') from dual) and TRANSPORT_KIND in ('04','05', '06') ".
") ".
"group by delvtime,delvminute ".
"order by delvtime,delvminute";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>本日の日本郵便出荷作業状況</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td>出荷時間</td><td>件数</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$total_shuka=0;
while($row = $res->fetchRow()){
	print "<tr>";
//時間
	print "<td>".$row[0];
//分
	if ($row[1]==0){print "時00分から30分";}
	else{
	print "時31分から60分";
	}
	print "</td>";
//件数
	print "<td align=right>".$row[2] ."</td>";
	print "</tr>";
$total_shuka=$total_shuka+$row[2];
}
print "<tr><td>合計</td><td align=right>".number_format($total_shuka)."</td></tr>";
print "</table>";
//データの開放
$res->free();
//-----------------------------------------------------------------------------
//SQL文のCOUNT関数を使用
$sql = "select delvtime,delvminute,count(*) from (".
"select distinct substr(deliverydatetime,9,2) as delvtime,receptioncode,deliverydatetime,".
"case when substr(deliverydatetime,11,2)<31 then 0".
"else 1 ".
"end as delvminute ".
"from d_reception_shipment ".
"where substr(deliverydatetime,1,8)=(select to_char(sysdate,'yyyymmdd') from dual) and TRANSPORT_KIND in ('01','02') ".
") ".
"group by delvtime,delvminute ".
"order by delvtime,delvminute";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>本日の佐川出荷作業状況</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td>出荷時間</td><td>件数</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$total_shuka=0;
while($row = $res->fetchRow()){
	print "<tr>";
//時間
	print "<td>".$row[0];
//分
	if ($row[1]==0){print "時00分から30分";}
	else{
	print "時31分から60分";
	}
	print "</td>";
//件数
	print "<td align=right>".$row[2] ."</td>";
	print "</tr>";
$total_shuka=$total_shuka+$row[2];
}
print "<tr><td>合計</td><td align=right>".number_format($total_shuka)."</td></tr>";
print "</table>";
//データの開放
$res->free();
//----------------------------------------------------------------------------------
$sql = "select delvtime,delvminute,count(*) from (".
"select distinct substr(deliverydatetime,9,2) as delvtime,receptioncode,deliverydatetime,".
"case when substr(deliverydatetime,11,2)<31 then 0".
"else 1 ".
"end as delvminute ".
"from d_reception_shipment ".
"where substr(deliverydatetime,1,8)=to_char(sysdate,'yyyymmdd') and TRANSPORT_KIND IN( '07','08') ".
") ".
"group by delvtime,delvminute ".
"order by delvtime,delvminute";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>本日のゆうパケット出荷作業状況</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td>出荷時間</td><td>件数</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$total_shuka=0;
while($row = $res->fetchRow()){
	print "<tr>";
//時間
	print "<td>".$row[0];
//分
	if ($row[1]==0){print "時00分から30分";}
	else{
	print "時31分から60分";
	}
	print "</td>";
//件数
	print "<td align=right>".$row[2] ."</td>";
	print "</tr>";
$total_shuka=$total_shuka+$row[2];
}
print "<tr><td>合計</td><td align=right>".number_format($total_shuka)."</td></tr>";
print "</table>";
//データの開放
$res->free();
//----------------------------------------------------------------------------------
$sql = "select delvtime,delvminute,count(*) from (".
"select distinct substr(deliverydatetime,9,2) as delvtime,receptioncode,deliverydatetime,".
"case when substr(deliverydatetime,11,2)<31 then 0".
"else 1 ".
"end as delvminute ".
"from d_reception_shipment ".
"where substr(deliverydatetime,1,8)=(select to_char(sysdate,'yyyymmdd') from dual) and TRANSPORT_KIND ='03' ".
") ".
"group by delvtime,delvminute ".
"order by delvtime,delvminute";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>本日のヤマト出荷作業状況</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td>出荷時間</td><td>件数</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$total_shuka=0;
while($row = $res->fetchRow()){
	print "<tr>";
//時間
	print "<td>".$row[0];
//分
	if ($row[1]==0){print "時00分から30分";}
	else{
	print "時31分から60分";
	}
	print "</td>";
//件数
	print "<td align=right>".$row[2] ."</td>";
	print "</tr>";
$total_shuka=$total_shuka+$row[2];
}
print "<tr><td>合計</td><td align=right>".number_format($total_shuka)."</td></tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>