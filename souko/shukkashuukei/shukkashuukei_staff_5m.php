<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出荷作業状況（スタッフ毎）</title>
</head>
<body>
<?php
set_time_limit(240);

	//ファイルの読み込み
	//PEARの利用     -------(1)
	require_once("DB.php");
	require_once("../parts/selectvalue_souko.php");
	//ログイン情報の読み込み
	require_once("../parts/login_souko.php");

	$nowtime= date("H");
	$startTime = 7;

	$sql = "SELECT  P.operatestaffcode, F.staffname, P.deliverytime, P.deliveryminute, count(distinct P.receptioncode) ".
	"FROM (SELECT r.receptioncode, d.operatestaffcode, r.deliverydatetime, substr(r.deliverydatetime,9,2) AS deliverytime, ".
	"CASE ".
	"WHEN substr(r.deliverydatetime,11,2)<6 THEN 0 ".
	"WHEN substr(r.deliverydatetime,11,2)<11 THEN 1 ".
	"WHEN substr(r.deliverydatetime,11,2)<16 THEN 2 ".
	"WHEN substr(r.deliverydatetime,11,2)<21 THEN 3 ".
	"WHEN substr(r.deliverydatetime,11,2)<26 THEN 4 ".
	"WHEN substr(r.deliverydatetime,11,2)<31 THEN 5 ".
	"WHEN substr(r.deliverydatetime,11,2)<36 THEN 6 ".
	"WHEN substr(r.deliverydatetime,11,2)<41 THEN 7 ".
	"WHEN substr(r.deliverydatetime,11,2)<46 THEN 8 ".
	"WHEN substr(r.deliverydatetime,11,2)<51 THEN 9 ".
	"WHEN substr(r.deliverydatetime,11,2)<56 THEN 10 ".
	"ELSE 11 ".
	"END AS deliveryminute ".
	"FROM d_reception_shipment r,d_reception d ".
	"WHERE r.receptioncode=d.receptioncode ".
	"AND substr(r.deliverydatetime,1,8)=(select to_char(sysdate,'yyyymmdd') from dual)) P, m_staff F ".
	"WHERE P.operatestaffcode=F.staffcode ".
	"GROUP BY P.deliverytime,P.deliveryminute,P.operatestaffcode,F.staffname ".
	"ORDER BY P.operatestaffcode,F.staffname,P.deliverytime,P.deliveryminute";
	
	
	//print $sql;
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	
	
//検索結果の表示
	print "<strong>　<BR>本日の出荷作業状況（スタッフ毎）</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";

//項目名の表示
	print "<td colspan=3></td>";
	
	for ($c = $startTime; $c <= $nowtime; $c++) {
	
		print "<td colspan=12>".$c."時</td>";
		
	}


	print "</tr><tr bgcolor=#ccffff>\n";
	print "<td nowrap>スタッフコード</td><td nowrap>スタッフ名</td><td nowrap>合計</td>";

	for ($c = $startTime; $c <= $nowtime; $c++) {
		print "<td nowrap>00-05</td><td nowrap>06-10</td><td nowrap>11-15</td><td nowrap>16-20</td><td nowrap>21-25</td><td nowrap>26-30</td><td nowrap>31-35</td><td nowrap>36-40</td><td nowrap>41-45</td><td nowrap>46-50</td><td nowrap>51-55</td><td nowrap>56-60</td>";
	}
	
	print "</tr>";

	$total_count=0;
	$i=0;
	$motostaffcode="0";

	for($cc = 1; $cc <= 24; $cc++) {
	
		$jikantotal[$cc][0]=0;
		$jikantotal[$cc][1]=0;
		$jikantotal[$cc][2]=0;
		$jikantotal[$cc][3]=0;
		$jikantotal[$cc][4]=0;
		$jikantotal[$cc][5]=0;
		$jikantotal[$cc][6]=0;
		$jikantotal[$cc][7]=0;
		$jikantotal[$cc][8]=0;
		$jikantotal[$cc][9]=0;
		$jikantotal[$cc][10]=0;
		$jikantotal[$cc][11]=0;
		
	}


//取得データのセット
while($row = $res->fetchRow()){

	$total_count=$total_count+$row[4];
	
	
	//スタッフコード毎処理
	if ($motostaffcode==($row[0]+0)){
	
		//同一スタッフデータ処理
	
	}else{
	
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$stafftotal[$i]=0;
		
		//1時間毎の時間間隔セット
		for($cc = 1; $cc <= 24; $cc++) {
		
			$forhtml[($i)][($cc)][0]=0;
			$forhtml[($i)][($cc)][1]=0;
			$forhtml[($i)][($cc)][2]=0;
			$forhtml[($i)][($cc)][3]=0;
			$forhtml[($i)][($cc)][4]=0;
			$forhtml[($i)][($cc)][5]=0;
			$forhtml[($i)][($cc)][6]=0;
			$forhtml[($i)][($cc)][7]=0;
			$forhtml[($i)][($cc)][8]=0;
			$forhtml[($i)][($cc)][9]=0;
			$forhtml[($i)][($cc)][10]=0;
			$forhtml[($i)][($cc)][11]=0;
			
		}
		
	}
	
	$forhtml[$i][($row[2]+0)][($row[3]+0)] = $row[4];
	$stafftotal[$i] = $stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][($row[3]+0)] = $jikantotal[($row[2]+0)][($row[3]+0)]+$row[4];
	
}


//HTML表示
for($ii = 1; $ii <= $i; $ii++) {

	print "<tr><td>".$staffcode[$ii]."</td><td nowrap>".$staffname[$ii]."</td><td align='right'>".number_format($stafftotal[$ii])."</td>";
	for ($c = $startTime; $c <= $nowtime; $c++) {
		print "<td align='right'>".$forhtml[$ii][$c][0]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][1]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][2]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][3]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][4]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][5]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][6]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][7]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][8]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][9]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][10]."</td>";
		print "<td align='right'>".$forhtml[$ii][$c][11]."</td>";
	}
	print "</tr>";
}



print "<tr><td colspan=2 align=center>出荷件数合計</td><td>".number_format($total_count)."</td>";

//時間単位集計結果出力
for ($c = $startTime; $c <= $nowtime; $c++) {
	print "<td align='right'>".$jikantotal[$c][0]."</td>";
	print "<td align='right'>".$jikantotal[$c][1]."</td>";
	print "<td align='right'>".$jikantotal[$c][2]."</td>";
	print "<td align='right'>".$jikantotal[$c][3]."</td>";
	print "<td align='right'>".$jikantotal[$c][4]."</td>";
	print "<td align='right'>".$jikantotal[$c][5]."</td>";
	print "<td align='right'>".$jikantotal[$c][6]."</td>";
	print "<td align='right'>".$jikantotal[$c][7]."</td>";
	print "<td align='right'>".$jikantotal[$c][8]."</td>";
	print "<td align='right'>".$jikantotal[$c][9]."</td>";
	print "<td align='right'>".$jikantotal[$c][10]."</td>";
	print "<td align='right'>".$jikantotal[$c][11]."</td>";
}



print "</tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
