<?PHP
function Retvol($category)
{
//カテゴリ名称返却
/* 2019.05.20 ブックラPhase2対応 MOD START */
switch ($category){
	case "Z6001":
		$retvalue= "スターツピタットハウス";
		break;
	case "Z6002":
		$retvalue= "大京アステージ(くらしスクエア)";
		break;
	case "Z6003":
		$retvalue= "大京アステージ(コンシェルジュ)";
		break;
	case "Z6004":
		$retvalue= "三井不動産住宅サービス";
		break;
	case "Z6005":
		$retvalue= "アスク";
		break;
	case "Z6006":
		$retvalue= "インターファーム";
		break;
	case "Z6007":
		$retvalue= "アート引越センター";
		break;
	case "Z6008":
		$retvalue= "クラブパナソニック";
		break;
	case "Z6009":
		$retvalue= "大東建託パートナーズ";
		break;
	case "Z6010":
		$retvalue= "アリさんマークの引越社";
		break;
	case "Z6011":
		$retvalue= "イヌイ運送";
		break;
	case "Z6012":
		$retvalue= "日本通運";
		break;
	case "Z6013":
		$retvalue= "ハウスコム";
		break;
	case "Z6014":
		$retvalue= "オーネット";
		break;
	case "Z6015":
		$retvalue= "バイク王";
		break;
	case "Z6016":
		$retvalue= "三菱地所コミュニティ";
		break;
	case "Z6017":
		$retvalue= "コミュニティワン";
		break;
	case "Z6018":
		$retvalue= "住友不動産建物サービス";
		break;
	case "Z6019":
		$retvalue= "リトル・ママ";
		break;
	case "Z6020":
		$retvalue= "レジデンスクラブ";
		break;
	case "Z6021":
		$retvalue= "JAF";
		break;
	case "Z6022":
		$retvalue= "ぐるなび食市場";
		break;
	case "Z6023":
		$retvalue= "ヤマトホームコンビニエンス（クロネコヤマト引越センター）";
		break;
	case "Z6024":
		$retvalue= "ファミリー引越センター";
		break;
	case "Z6025":
		$retvalue= "ジャパンネット銀行";
		break;
	case "Z6026":
		$retvalue= "暮らしのサポート";
		break;
	case "Z6027":
		$retvalue= "三井のすまいLOOP(ループ)";
		break;
	case "Z6028":
		$retvalue= "エイブル";
		break;
	case "Z6029":
		$retvalue= "MAST(マスト)";
		break;
	case "Z6030":
		$retvalue= "フロムヴイ";
		break;
	case "Z6031":
		$retvalue= "大和ライフネクスト";
		break;
	case "Z6032":
		$retvalue= "レオパレス21";
		break;
	case "Z6033":
		$retvalue= "ズバット引越し比較";
		break;
	case "Z6034":
		$retvalue= "インターネットコンシェルジュサービス";
		break;
	case "Z6035":
		$retvalue= "長谷工コミュニティ";
		break;
	case "Z6036":
		$retvalue= "QLC(コンシェルジュ)";
		break;
	case "Z6037":
		$retvalue= "住宅情報館(城南建設)";
		break;
	case "Z6038":
		$retvalue= "ジャックスカード";
		break;
	case "Z6039":
		$retvalue= "積村ビル管理";
		break;
	case "Z6040":
		$retvalue= "東急住宅リース";
		break;
	case "Z6041":
		$retvalue= "ハッピークラブモール";
		break;
	case "Z6042":
		$retvalue= "おうちCO-OP";
		break;
	case "Z6043":
		$retvalue= "ハイホー";
		break;
	case "Z6044":
		$retvalue= "ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）";
		break;
	case "Z6045":
		$retvalue= "ソフトバンクグループ";
		break;
	case "Z6046":
		$retvalue= "オリックス・クレジット";
		break;
	case "Z6047":
		$retvalue= "リロクラブ";
		break;
	case "Z6048":
		$retvalue= "メディアカフェポパイ";
		break;
	case "Z6049":
		$retvalue= "アメリカン・エキスプレス";
		break;
	case "Z6050":
		$retvalue= "ネクスト・ゴルフ・マネジメント";
		break;
	case "Z6051":
		$retvalue= "エイコータウン";
		break;
	case "Z6052":
		$retvalue= "社員向け_レオパレス21";
		break;
	case "Z6053":
		$retvalue= "macalon+（マカロンプラス）";
		break;
	case "Z6054":
		$retvalue= "タイムズクラブ";
		break;
	case "Z6055":
		$retvalue= "ゆとライフドットコム";
		break;
	case "Z6056":
		$retvalue= "クラスエル";
		break;
	case "Z6057":
		$retvalue= "バイクブロス";
		break;
	case "Z6058":
		$retvalue= "泉友";
		break;
	case "Z6059":
		$retvalue= "富士フイルム生活協同組合";
		break;
	case "Z6060":
		$retvalue= "リコー三愛グループ";
		break;
	case "Z6061":
		$retvalue= "ヴァリック";
		break;
	case "Z6062":
		$retvalue= "LIFULL引越し";
		break;
	case "Z6063":
		$retvalue= "千葉県庁生活協同組合";
		break;
	case "Z6064":
		$retvalue= "エポスカード";
		break;
	case "Z6065":
		$retvalue= "エフコープ";
		break;
	case "Z6066":
		$retvalue= "明電グループ";
		break;
	case "Z6067":
		$retvalue= "青森県庁生協";
		break;
	case "Z6068":
		$retvalue= "おおさかパルコープ";
		break;
	case "Z6069":
		$retvalue= "よどがわ市民生協";
		break;
	case "Z6070":
		$retvalue= "東急リバブル";
		break;
	case "Z6071":
		$retvalue= "auコレトク";
		break;
	case "Z6072":
		$retvalue= "ARUHI暮らしのサービス";
		break;
	case "Z6073":
		$retvalue= "京都生協";
		break;
	case "Z6074":
		$retvalue= "FUKUYA";
		break;
	case "Z6075":
		$retvalue= "群馬県庁生活協同組合";
		break;
	case "Z6076":
		$retvalue= "ライフサポート倶楽部";
		break;
	case "Z6077":
		$retvalue= "ベルスファミリークラブ";
		break;
	case "Z6078":
		$retvalue= "群馬県学校生協";
		break;
	case "Z6079":
		$retvalue= "とくしま生協";
		break;
	case "Z6080":
		$retvalue= "日産グループ";
		break;
	case "Z6081":
		$retvalue= "NHK共済会";
		break;
	case "Z6082":
		$retvalue= "えらべる倶楽部";
		break;
	case "Z6083":
		$retvalue= "茨城県学校生協";
		break;
	case "Z6084":
		$retvalue= "福井県学校生協";
		break;
	case "Z6085":
		$retvalue= "LivLi CLUB（リブリクラブ）";
		break;
	case "Z6086":
		$retvalue= "CHINTAI";
		break;
	case "Z6087":
		$retvalue= "CLASSY LIFE";
		break;		
	case "Z6088":
		$retvalue= "静岡県教職員生協";
		break;		
	case "Z6089":
		$retvalue= "TS ONE";
		break;	
	case "Z6090":
		$retvalue= "長崎県職員生活協同組合";
		break;	
	case "Z6091":
		$retvalue= "安川電機グループ";
		break;	
	case "Z6092":
		$retvalue= "明治安田生命保険相互会社";
		break;	
	case "Z6093":
		$retvalue= "北海道学校生協";
		break;	
	case "Z6094":
		$retvalue= "パレットクラウド";
		break;	
	case "Z6095":
		$retvalue= "TEPCO";
		break;	
	case "Z6096":
		$retvalue= "神奈川県医療福祉施設協同組合";
		break;	
	case "Z6097":
		$retvalue= "FCクラブ・Fukurico";
		break;	
	case "Z6098":
		$retvalue= "情報労連あいねっと倶楽部";
		break;	
	case "Z6099":
		$retvalue= "ブリヂストングループ";
		break;	
		
	default:
		$retvalue= "エラー";
		break;
}
/* 2019.05.20 ブックラPhase2対応 MOD END */	
	return $retvalue;
}
function Rethiduke($index)
{
//日付整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2);
}
	return $retvalue;
}
function Rethidukejikan($index)
{
//日付時間整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2). " ".substr($index,8,2). ":".substr($index,10,2). ":" .substr($index,12,2);
}
	return $retvalue;
}
function Retzip($index)
{
//郵便番号整形
if (empty($index)){
	$retvalue='〒';
}else{
	$retvalue = "〒".substr($index,0,3). "-" .substr($index,3,4);
}
	return $retvalue;
}
function Rettel($index)
{
//電話番号整形
if (empty($index)){
	$retvalue='　';
}else{
	switch (substr($index,1,1)){
		case 3:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		case 6:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		default:
			if (strlen($index)==10){
				$retvalue = substr($index,0,3). "-" .substr($index,3,3). "-" .substr($index,6,4);
			}else{
				$retvalue = substr($index,0,3). "-" .substr($index,3,4). "-" .substr($index,7,4);
			}
			break;
	}
}
	return $retvalue;
}
function RetPrefecture($index)
{
	switch ($index){
		case 0;
			$retvalue = "選択してください";
			break;
		case 1;
			$retvalue = "北海道";
			break;
		case 2;
			$retvalue = "青森県";
			break;
		case 3;
			$retvalue = "岩手県";
			break;
		case 4;
			$retvalue = "秋田県";
			break;
		case 5;
			$retvalue = "宮城県";
			break;
		case 6;
			$retvalue = "山形県";
			break;
		case 7;
			$retvalue = "福島県";
			break;
		case 8;
			$retvalue = "茨城県";
			break;
		case 9;
			$retvalue = "栃木県";
			break;
		case 10;
			$retvalue = "群馬県";
			break;
		case 11;
			$retvalue = "埼玉県";
			break;
		case 12;
			$retvalue = "千葉県";
			break;
		case 13;
			$retvalue = "東京都";
			break;
		case 14;
			$retvalue = "神奈川県";
			break;
		case 15;
			$retvalue = "新潟県";
			break;
		case 16;
			$retvalue = "富山県";
			break;
		case 17;
			$retvalue = "石川県";
			break;
		case 18;
			$retvalue = "福井県";
			break;
		case 19;
			$retvalue = "山梨県";
			break;
		case 20;
			$retvalue = "長野県";
			break;
		case 21;
			$retvalue = "岐阜県";
			break;
		case 22;
			$retvalue = "静岡県";
			break;
		case 23;
			$retvalue = "愛知県";
			break;
		case 24;
			$retvalue = "三重県";
			break;
		case 25;
			$retvalue = "滋賀県";
			break;
		case 26;
			$retvalue = "京都府";
			break;
		case 27;
			$retvalue = "大阪府";
			break;
		case 28;
			$retvalue = "兵庫県";
			break;
		case 29;
			$retvalue = "奈良県";
			break;
		case 30;
			$retvalue = "和歌山県";
			break;
		case 31;
			$retvalue = "鳥取県";
			break;
		case 32;
			$retvalue = "島根県";
			break;
		case 33;
			$retvalue = "岡山県";
			break;
		case 34;
			$retvalue = "広島県";
			break;
		case 35;
			$retvalue = "山口県";
			break;
		case 36;
			$retvalue = "徳島県";
			break;
		case 37;
			$retvalue = "香川県";
			break;
		case 38;
			$retvalue = "愛媛県";
			break;
		case 39;
			$retvalue = "高知県";
			break;
		case 40;
			$retvalue = "福岡県";
			break;
		case 41;
			$retvalue = "佐賀県";
			break;
		case 42;
			$retvalue = "長崎県";
			break;
		case 43;
			$retvalue = "熊本県";
			break;
		case 44;
			$retvalue = "大分県";
			break;
		case 45;
			$retvalue = "宮崎県";
			break;
		case 46;
			$retvalue = "鹿児島県";
			break;
		case 47;
			$retvalue = "沖縄県";
			break;
	}
			return $retvalue;
}
function Retseibetu($index)
{
	switch ($index){
		case 1;
			$retvalue = "男";
			break;
		case 2;
			$retvalue = "女";
			break;
	}
			return $retvalue;
}
function Retgaido($index)
{
	switch ($index){
		case 1;
			$retvalue = "必要";
			break;
		case 2;
			$retvalue = "不要";
			break;
	}
			return $retvalue;
}
function Retshuukajikan($index)
{
	switch ($index){
		case 1;
			$retvalue = "9:00-13:00";
			break;
		case 2;
			$retvalue = "13:00-15:00";
			break;
		case 3;
			$retvalue = "14:00-16:00";
			break;
		case 4;
			$retvalue = "15:00-18:00";
			break;
		case 5;
			$retvalue = "18:00-20:00";
			break;
	}
			return $retvalue;
}
function Retshokugyou($index)
{
	switch ($index){
		case 1;
			$retvalue = "会社員";
			break;
		case 2;
			$retvalue = "公務員";
			break;
		case 3;
			$retvalue = "主婦";
			break;
		case 4;
			$retvalue = "自営業";
			break;
		case 5;
			$retvalue = "学生";
			break;
		case 6;
			$retvalue = "パート";
			break;
		case 7;
			$retvalue = "フリーター";
			break;
		case 8;
			$retvalue = "無職";
			break;
		case 9;
			$retvalue = "その他";
			break;
	}
			return $retvalue;
}
function Rettuuti($index)
{
	switch ($index){
		case 0;
			$retvalue = "通知不要（自動承認）";
			break;
		case 1;
			$retvalue = "通知必要（承認後振込）";
			break;
	}
			return $retvalue;
}
function Retdtaiou($index)
{
	switch ($index){
		case 0;
			$retvalue = "センター引取";
			break;
		case 1;
			$retvalue = "返却希望";
			break;
	}
			return $retvalue;
}
?>