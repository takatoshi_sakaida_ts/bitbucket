<?PHP
/*
ファイル出力用ループ
*/
//カラム数の取得
function outputcsv($tempres,$tempfilename){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $tempfilename, "w" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $tempres->fetchRow()){
	$str="";
	for($i=0;$i<$ncols;$i++){
		$str=$str . $row[$i] . ",";//行の列データを取得
	}
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
}
fclose( $fp );
}
function outputcsv_tab($tempres,$tempfilename){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $tempfilename, "w" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $tempres->fetchRow()){
	$str="";
	for($i=0;$i<$ncols;$i++){
		$str=$str . $row[$i] . '	';//行の列データを取得
	}
//	$str = ereg_replace("\t","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
}
fclose( $fp );
}
function output_both_csv_tab($tempres,$temptsvfilename,$temptsvsplitfilename){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $temptsvfilename, "w" );
$fps = fopen( $temptsvsplitfilename, "w" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $tempres->fetchRow()){
	$str="";
	$strspl="";
	for($i=0;$i<$ncols;$i++){
		if($i==4 || $i==6 || $i==9 || $i==10 || $i==11) {
			$str=$str . $row[$i];//行の列データを取得
		} else {
			$str=$str . $row[$i] . '	';//行の列データを取得
		}
		$strspl=$strspl . $row[$i] . '	';//行の列データを取得(CSV)
	}
//	$str = ereg_replace("\t","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	fputs($fps,$strspl);
	fputs($fps,"\n");
}
fclose( $fp );
fclose( $fps );
}
function output_both_csv_tab_tran($tempres,$temptsvfilename,$temptsvsplitfilename){
$ncols = $tempres->numCols();
$j=1;
$fp = fopen( $temptsvfilename, "w" );
$fpc = fopen( $temptsvsplitfilename, "w" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $tempres->fetchRow()){
	$str="";
	$strspl="";
	for($i=0;$i<$ncols;$i++){
		if($i==5 || $i==7 || $i==10 || $i==11 || $i==12) {
			$str=$str . $row[$i];//行の列データを取得
		} else {
			$str=$str . $row[$i] . '	';//行の列データを取得
		}
		$strspl=$strspl . $row[$i] . '	';//行の列データを取得(CSV)
	}
//	$str = ereg_replace("\t","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	fputs($fpc,$strspl);
	fputs($fpc,"\n");
}
fclose( $fp );
fclose( $fpc );
}
?>