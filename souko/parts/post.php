<?PHP
session_cache_expire(60);
session_start();
require 'utl.inc';
require_once("DB.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>郵便番号検索画面</TITLE>
<script language="javascript">
function SetMsg(strValue1,strValue2,strValue3,strValue4)
{
	window.opener.document.volfm.PICKUP_ZIP_CD.value = strValue1;
	window.opener.document.volfm.PICKUP_ZIP_ADDR1.selectedIndex = strValue2;
	window.opener.document.volfm.PICKUP_ZIP_ADDR2.value = strValue3+strValue4;
//	window.opener.document.volfm.PICKUP_ZIP_ADDR3.value = strValue4;
	window.close();
}
function WinClose()
{
	window.close();
}
//-->
</script>
<?PHP
function RetPrefeindex($prefname)
{
		switch ($prefname){
		case "北海道";
			$retvalue = "1";
			break;
		case "青森県";
			$retvalue = "2";
			break;
		case "岩手県";
			$retvalue = "3";
			break;
		case "秋田県";
			$retvalue = "4";
			break;
		case "宮城県";
			$retvalue = "5";
			break;
		case "山形県";
			$retvalue = "6";
			break;
		case "福島県";
			$retvalue = "7";
			break;
		case "茨城県";
			$retvalue = "8";
			break;
		case "栃木県";
			$retvalue = "9";
			break;
		case "群馬県";
			$retvalue = "10";
			break;
		case "埼玉県";
			$retvalue = "11";
			break;
		case "千葉県";
			$retvalue = "12";
			break;
		case "東京都";
			$retvalue = "13";
			break;
		case "神奈川県";
			$retvalue = "14";
			break;
		case "新潟県";
			$retvalue = "15";
			break;
		case "富山県";
			$retvalue = "16";
			break;
		case "石川県";
			$retvalue = "17";
			break;
		case "福井県";
			$retvalue = "18";
			break;
		case "山梨県";
			$retvalue = "19";
			break;
		case "長野県";
			$retvalue = "20";
			break;
		case "岐阜県";
			$retvalue = "21";
			break;
		case "静岡県";
			$retvalue = "22";
			break;
		case "愛知県";
			$retvalue = "23";
			break;
		case "三重県";
			$retvalue = "24";
			break;
		case "滋賀県";
			$retvalue = "25";
			break;
		case "京都府";
			$retvalue = "26";
			break;
		case "大阪府";
			$retvalue = "27";
			break;
		case "兵庫県";
			$retvalue = "28";
			break;
		case "奈良県";
			$retvalue = "29";
			break;
		case "和歌山県";
			$retvalue = "30";
			break;
		case "鳥取県";
			$retvalue = "31";
			break;
		case "島根県";
			$retvalue = "32";
			break;
		case "岡山県";
			$retvalue = "33";
			break;
		case "広島県";
			$retvalue = "34";
			break;
		case "山口県";
			$retvalue = "35";
			break;
		case "徳島県";
			$retvalue = "36";
			break;
		case "香川県";
			$retvalue = "37";
			break;
		case "愛媛県";
			$retvalue = "38";
			break;
		case "高知県";
			$retvalue = "39";
			break;
		case "福岡県";
			$retvalue = "40";
			break;
		case "佐賀県";
			$retvalue = "41";
			break;
		case "長崎県";
			$retvalue = "42";
			break;
		case "熊本県";
			$retvalue = "43";
			break;
		case "大分県";
			$retvalue = "44";
			break;
		case "宮崎県";
			$retvalue = "45";
			break;
		case "鹿児島県";
			$retvalue = "46";
			break;
		case "沖縄県";
			$retvalue = "47";
			break;
	}

	return $retvalue;
}
if(isset($_GET["postcode"])){
	$postcode = htmlspecialchars(escapevalue($_GET["postcode"]));
}
if(isset($_GET["postname"])){
	$postname = htmlspecialchars(escapevalue($_GET["postname"]));
}
if (empty($postname))
{
	$sql = "SELECT zip_cd,addr1,addr2,addr3,dtl_addr FROM tzipcode where zip_cd='" . $postcode . "'";
}else
{
	$sql = "SELECT zip_cd,addr1,addr2,addr3,dtl_addr FROM tzipcode where (addr1||addr2||addr3||dtl_addr) like '%" . $postname . "%'";
	$postcode = "";
}
//print $sql;
?>
<META http-equiv=Content-Type content="text/html; charset=shift-jis">
<META content="MSHTML 6.00.2900.2802" name=GENERATOR>
</HEAD>
<BODY>
<CENTER>
<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>
</table>
<form method="get" name="frmpostsearch" action='post.php'>
郵便番号：	<input type="text" name="postcode" size="8" maxlength="7" style="ime-mode:inactive;" value="
<?PHP
if(isset($postcode)){
	print $postcode;
}
?>
">
	<BR>
住所　　：	<input type="text" name="postname" size="20" style="ime-mode:active;" value="
<?PHP
if(isset($postname)){
	print $postname;
}
?>
">
<BR>
	<input type="submit" name="go_search" value="検索">
	<input type="button" name="back_input" value="閉じる" onclick="WinClose()">
	<BR><BR>
	住所名に値がある時は、郵便番号での検索はしません。
	<BR><BR>
<?PHP
//ログイン情報の読み込み
require_once("login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
$db->setOption('portability',DB_PORTABILITY_NULL_TO_EMPTY | DB_PORTABILITY_NUMROWS);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
// HTML に結果を出力する
	print "<table border='1'>";
  print "<tr>";
	print "<td>";
	print "郵便番号";
	print "</td>\n";
	print "<td>";
	print "都道府県";
	print "</td>\n";
	print "<td>";
	print "住所";
	print "</td>\n";
while($row = $res->fetchRow()){
   	print "<tr>";
		print "<td>";
		print '<a href="javascript:SetMsg(' . "'". $row[0] . "','". RetPrefeindex($row[1]) . "','"
. $row[2] . "'," ."'". $row[3] . "'" . ')">' . $row[0] . '</a><br>';
		print "</td>\n";
		print "<td>";
		print $row[1];
		print "</td>\n";
		print "<td>";
		print $row[2].$row[3];
		print "</td>\n";
		print "<td>";
	  print "</tr>\n";
}
	print "</table>\n";

// 結果セットを開放する
$res->free();
//データの開放
$db->disconnect();
?>
</FORM>
</BODY>
</HTML>
