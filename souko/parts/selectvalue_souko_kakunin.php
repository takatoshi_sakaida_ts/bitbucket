<?PHP
function Retgoodscategory($category)
{
//カテゴリ名称返却
switch ($category){
	case 11:
		$retvalue= "コミック";
		break;
	case 12:
		$retvalue= "書籍";
		break;
	case 13:
		$retvalue= "雑誌";
		break;
	case 31:
		$retvalue= "CD";
		break;
	case 51:
		$retvalue= "GAME";
		break;
	case 71:
		$retvalue= "DVD";
		break;
	case 91:
		$retvalue= "部門商品";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retgoodsdetailsql($instorecode,$category)
{
//カテゴリ名称と詳細検索用SQL返却
switch ($category){
	case 11:
		$retvalue[0]= "コミック";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.VOLUMENO,b.AUTHORNAME from M_GOODS_BOOK a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	case 12:
		$retvalue[0]= "書籍";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.VOLUME,b.AUTHORNAME from M_GOODS_BOOK a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	case 13:
		$retvalue[0]= "雑誌";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.VOLUMENO,a.PUBLISHNAME from M_GOODS_BOOK a where a.instorecode='". $instorecode ."'";
		break;
	case 31:
		$retvalue[0]= "CD";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.TOTALSUITE,b.AUTHORNAME from M_GOODS_MUSIC a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	case 51:
		$retvalue[0]= "GAME";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.SPECCODE,a.PUBLISHNAME from M_GOODS_GAME a where a.instorecode='". $instorecode ."'";
		break;
	case 71:
		$retvalue[0]= "DVD";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.PUBLISHNAME,b.AUTHORNAME from M_GOODS_VIDEO a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retstatuskind($index)
{
//注文ステータス返却
switch ($index){
	case 0:
		$jyoutai= "納品書未印刷";
		break;
	case 1:
		$jyoutai= "納品書印刷済";
		break;
	case 2:
		$jyoutai= "出荷準備完了";
		break;
	case 3:
		$jyoutai= "出荷完了(EC未連携)";
		break;
	case 4:
		$jyoutai= "出荷完了(EC連携済)";
		break;
	case 9:
		$jyoutai= "全て";
		break;
	default:
		$jyoutai= "エラー";
		break;
}
	return $jyoutai;
}
function Retmakerstatuskind($index)
{
//メーカー注文ステータス返却
switch ($index){
	case 1:
		$jyoutai= "発注依頼済";
		break;
	case 2:
		$jyoutai= "依頼完了";
		break;
	case 3:
		$jyoutai= "発注完了";
		break;
	case 4:
		$jyoutai= "問い合わせ中";
		break;
	case 5:
		$jyoutai= "メーカー欠品";
		break;
	case 6:
		$jyoutai= "入荷待ち";
		break;
	case 7:
		$jyoutai= "入荷欠品";
		break;
	case 8:
		$jyoutai= "入荷済";
		break;
	case 99:
		$jyoutai= "全て";
		break;
	default:
		$jyoutai= "エラー";
		break;
}
	return $jyoutai;
}
function Retkaitoristatuskind($index)
{
//メーカー注文ステータス返却
switch ($index){
	case -1:
		$jyoutai= "一時保存中";
		break;
	case 0:
		$jyoutai= "未承認（査定新規/修正した状態）";
		break;
	case 1:
		$jyoutai= "未承認（ECへ査定結果送信した状態）";
		break;
	case 2:
		$jyoutai= "承認";
		break;
	case 3:
		$jyoutai= "仕入済";
		break;
	case 4:
		$jyoutai= "仕分済";
		break;
	case 99:
		$jyoutai= "全て";
		break;
	default:
		$jyoutai= "エラー";
		break;
}
	return $jyoutai;
}
function Retkessai($index)
{
//決済方法返却
switch (trim($index)){
	case "0":
		$retvalue= "店舗受取";
		break;
	case "1":
		$retvalue= "クレジット";
		break;
	case "2":
		$retvalue= "代引";
		break;
	case "3":
		$retvalue= "Yahooマネー";
		break;
	case "4":
		$retvalue= "全額ポイント支払い";
		break;
	case "5":
		$retvalue= "Amazonにお問合せ下さい";
		break;
	case "6":
		$retvalue= "Yahoo!かんたん決済";
		break;
	case "A":
		$retvalue= "PayPay支払い";
		break;
	case "B":
		$retvalue= "PayPay、クレジットカード支払い";
		break;
	case "C":
		$retvalue= "楽天ペイ";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retunsoukaisha($index)
{
//配送方法返却
if (empty($index)){
	$retvalue='未設定';
}else{
	switch ($index){
		case 1:
			$retvalue= "その他";
			break;
		case 2:
			$retvalue= "佐川急便";
			break;
		case 3:
			$retvalue= "ヤマトメール便";
			break;
		case 4:
			$retvalue= "日本郵便(袋)";
			break;
		case 5:
			$retvalue= "日本郵便(段ボール小)";
			break;
		case 6:
			$retvalue= "日本郵便(段ボール大)";
			break;
		case 7:
			$retvalue= "ゆうパケット(2〜3cm)";
			break;
		case 8:
			$retvalue= "ゆうパケット(2cm以下)";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
}
	return $retvalue;
}
function Retlocation($index)
{
//ロケーション返却
switch ($index){
	case 9000000000:
		$location= "ECフリー";
		break;
	case 9001000000:
		$location= "リザーブフリー";
		break;
	case 9002000000:
		$location= "仕分前フリー";
		break;
	case 9003000000:
		$location= "新品フリー";
		break;
	default:
		if ($index>=1001001){
			if ($index<=1001999){
				$location='リザーブ';
			}else{
				$location=$index;
			}
		}else{
				$location=$index;
		}
		break;
}
	return $location;
}
function Retzaikokubun($index)
{
//商品の区分返却
switch ($index){
	case 0:
		$zaiko= "中古";
		break;
	case 1:
		$zaiko= "新古";
		break;
	case 2:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retzaikokubunforuser($index)
{
//商品の区分返却
switch ($index){
	case 0:
		$zaiko= "中古";
		break;
	case 1:
		$zaiko= "新品";
		break;
	case 2:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retzaikokubunforuserec($index)
{
//商品の区分返却
switch ($index){
	case 1:
		$zaiko= "中古";
		break;
	case 2:
		$zaiko= "新品";
		break;
	case 3:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retpickubun($index)
{
//ピッキング状態返却
switch ($index){
	case 0:
		$retvalue= "未完了";
		break;
	case 1:
		$retvalue= "完了";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retcancel($index)
{
//キャンセル状態返却
	if ($index==1){
		$retvalue = "キャンセル";
	}else{
		$retvalue = "　";
	}
	return $retvalue;
}
function Rethaisoubangou($index)
{
//日通配送番号整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,3). "-" .substr($index,3,2). "-" .substr($index,5,3). "-" .substr($index,8,4);
}
	return $retvalue;
}
function Rethiduke($index)
{
//日付整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2);
}
	return $retvalue;
}
function Rethidukejikan($index)
{
//日付時間整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2). " ".substr($index,8,2). ":".substr($index,10,2). ":" .substr($index,12,2);
}
	return $retvalue;
}
function Retzip($index)
{
//郵便番号整形
if (empty($index)){
	$retvalue='〒';
}else{
	$retvalue = "〒".substr($index,0,3). "-" .substr($index,3,4);
}
	return $retvalue;
}
function Rettel($index)
{
//電話番号整形
if (empty($index)){
	$retvalue='　';
}else{
	switch (substr($index,1,1)){
		case 3:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		case 6:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		default:
			if (strlen($index)==10){
				$retvalue = substr($index,0,3). "-" .substr($index,3,3). "-" .substr($index,6,4);
			}else{
				$retvalue = substr($index,0,3). "-" .substr($index,3,4). "-" .substr($index,7,4);
			}
			break;
	}
}
	return $retvalue;
}
function Rettorihikisaki($index)
{
//発注取引先名称返却
switch ($index){
	case "SKD-":
		$retvalue= "星光堂";
		break;
	case "TYS-":
		$retvalue= "太洋社";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retyoyakukubun($index)
{
//予約区分返却
switch ($index){
	case 0:
		$retvalue= "通常発注";
		break;
	case 1:
		$retvalue= "予約品(未引当)";
		break;
	case 2:
		$retvalue= "予約品(引当済)";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethachustatus($index)
{
//新品・新刊発注ステータス
switch ($index){
	case 1:
		$retvalue= "発注依頼済";
		break;
	case 2:
		$retvalue= "依頼完了";
		break;
	case 3:
		$retvalue= "発注完了";
		break;
	case 4:
		$retvalue= "問い合わせ中";
		break;
	case 5:
		$retvalue= "メーカー欠品";
		break;
	case 6:
		$retvalue= "入荷待ち";
		break;
	case 7:
		$retvalue= "入荷欠品";
		break;
	case 8:
		$retvalue= "入荷済";
		break;
	case 99:
		$retvalue= "入荷済以外";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethachukaitou($index)
{
//発注回答返却
switch ($index){
	case 0:
		$retvalue= "キャンセル受付完了他";
		break;
	case 1:
		$retvalue= "在庫なし取寄せ中";
		break;
	case 2:
		$retvalue= "廃盤";
		break;
	case 3:
		$retvalue= "製造中止";
		break;
	case 4:
		$retvalue= "販売中止";
		break;
	case 5:
		$retvalue= "発売中止";
		break;
	case 6:
		$retvalue= "発売日前";
		break;
	case 7:
		$retvalue= "出荷停止";
		break;
	case 9:
		$retvalue= "データエラー";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethachuzankaitou($index)
{
//発注残回答
switch ($index){
	case 0:
		$retvalue= "キャンセル受付完了他";
		break;
	case 1:
		$retvalue= "入荷";
		break;
	case 2:
		$retvalue= "取寄せ中";
		break;
	case 3:
		$retvalue= "廃盤";
		break;
	case 4:
		$retvalue= "販売中止";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retbooksoftkind($index)
{
//発注残回答
switch ($index){
	case 0:
		$retvalue= "本";
		break;
	case 1:
		$retvalue= "ソフト";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retbooksoftd($value,$shurui){
if ($shurui==0){
	switch ($value){
		case 0:
			$retvalue= "未チェック";
			break;	
		case 1:
			$retvalue= "焼け";
			break;
		case 2:
			$retvalue= "水ぬれ";
			break;
		case 3:
			$retvalue= "カバーがない";
			break;
		case 4:
			$retvalue= "書き込みあり";
			break;
		case 5:
			$retvalue= "破れ";
			break;
		case 6:
			$retvalue= "カビ、シミ";
			break;
		case 7:
			$retvalue= "取扱不可雑誌";
			break;
		case 8:
			$retvalue= "年数経過雑誌";
			break;
		case 9:
			$retvalue= "非売品";
			break;
		case 10:
			$retvalue= "付録のない雑誌";
			break;
		default:
			$retvalue= "エラー　システム部に連絡";
			break;
	}
}else{
	switch ($value){
		case 0:
			$retvalue= "未チェック";
			break;	
		case 1:
			$retvalue= "盤に傷が多い";
			break;
		case 2:
			$retvalue= "PCソフト";
			break;
		case 3:
			$retvalue= "特殊ケース不良";
			break;
		case 4:
			$retvalue= "付属品なし";
			break;
		case 5:
			$retvalue= "ジャケット、歌詞カード不良";
			break;
		case 6:
			$retvalue= "箱・説明書なし";
			break;
		case 7:
			$retvalue= "ソフトのレンタル落ち";
			break;
		case 8:
			$retvalue= "非売品";
			break;
		case 9:
			$retvalue= "取り扱い不可のハード機器";
			break;
		default:
			$retvalue= "エラー　システム部に連絡";
			break;
	}
}
	return $retvalue;
}
function Retkaitoriikisaki($index)
{
//発注残回答
switch ($index){
	case 0:
		$retvalue= "D商品";
		break;
	case 1:
		$retvalue= "BOL行き";
		break;
	case 2:
		$retvalue= "ロジ行き";
		break;
	case 3:
		$retvalue= "マスタ有り";
		break;
	case 4:
		$retvalue= "マスタ無し";
		break;
	case 5:
		$retvalue= "5円買取";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retkeisankekka($index)
{
//計算結果
switch ($index){
	case 0:
		$retvalue= "不要";
		break;
	case 1:
		$retvalue= "要";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function RetDtaiou($index)
{
//D対応
switch ($index){
	case 0:
		$retvalue= "BOLにて引取";
		break;
	case 1:
		$retvalue= "返却希望";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function RetSTOCKRESERVEKIND($index)
{
//引当状況
switch ($index){
	case 0:
		$retvalue= "引当無し";
		break;
	case 1:
		$retvalue= "作業引当";
		break;
	case 2:
		$retvalue= "受注引当";
		break;
	case 3:
		$retvalue= "作業確定引当";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retkeppinkbn($index)
{
//区分返却
switch ($index){
	case 1:
		$Retkeppinkbn= "メーカー欠品";
		break;
	case 2:
		$Retkeppinkbn= "Z納品書";
		break;
	default:
		$Retkeppinkbn= "エラー";
		break;
}
	return $Retkeppinkbn;
}

function RetSAGYOU_STATUS1($index)
{
//区分返却
switch ($index){
	case 0:
		$RetSAGYOU_STATUS1= "未対応";
		break;
	case 1:
		$RetSAGYOU_STATUS1= "発注待ち";
		break;
	case 2:
		$RetSAGYOU_STATUS1= "発注完了";
		break;
	case 3:
		$RetSAGYOU_STATUS1= "引当依頼";
		break;
	case 4:
		$RetSAGYOU_STATUS1= "完了";
		break;
	case 5:
		$RetSAGYOU_STATUS1= "キャンセル";
		break;
	default:
		$RetSAGYOU_STATUS1= "エラー";
		break;
}
	return $RetSAGYOU_STATUS1;
}

function RetCHUUMONNAIYOU($index)
{
//区分返却
switch ($index){
	case 0:
		$RetCHUUMONNAIYOU= "未設定";
		break;
	case 1:
		$RetCHUUMONNAIYOU= "新品あり";
		break;
	case 2:
		$RetCHUUMONNAIYOU= "中古のみ";
		break;
	case 3:
		$RetCHUUMONNAIYOU= "予約商品あり";
		break;
	default:
		$RetCHUUMONNAIYOU= "エラー";
		break;
}
	return $RetCHUUMONNAIYOU;
}
function RetHACCHUUSAKI($index)
{
switch ($index){
	case 0:
		$RetHACCHUUSAKI= "未設定";
		break;
	case 1:
		$RetHACCHUUSAKI= "amazon";
		break;
	case 2:
		$RetHACCHUUSAKI= "";
		break;
	case 3:
		$RetHACCHUUSAKI= "";
		break;
	default:
		$RetHACCHUUSAKI= "エラー";
		break;
}
	return $RetHACCHUUSAKI;
}
function Retsell_stat($index)
{
//
switch ($index){
	case 1:
		$zaiko= "受付";
		break;
	case 2:
		$zaiko= "計算中";
		break;
	case 3:
		$zaiko= "承認待ち";
		break;
	case 4:
		$zaiko= "本人確認待ち";
		break;
	case 5:
		$zaiko= "振込準備中";
		break;
	case 6:
		$zaiko= "完了";
		break;
	case 7:
		$zaiko= "承認保留";
		break;
	case 99:
		$zaiko= "キャンセル";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Rettuutiec($index)
{
//商品の区分返却
switch ($index){
	case 0:
		$zaiko= "不要";
		break;
	case 1:
		$zaiko= "必要";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retkeiroec($index)
{
//商品の区分返却
switch ($index){
	case 1:
		$zaiko= "WEB";
		break;
	case 2:
		$zaiko= "FD";
		break;
	case 3:
		$zaiko= "新品特典買取";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retsyouninec($index)
{
//商品の区分返却
switch ($index){
	case 0:
		$zaiko= "未確認";
		break;
	case 1:
		$zaiko= "確認済";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retuseyn($index)
{
//会員の状態
switch ($index){
	case 0:
		$zaiko= "退会状態";
		break;
	case 1:
		$zaiko= "使用可状態";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retemlyn($index)
{
//Eメール受信状態
switch ($index){
	case 0:
		$zaiko= "受け取らない";
		break;
	case 1:
		$zaiko= "HTMLまたはテキストで受け取る";
		break;
	case 2:
		$zaiko= "テキストで受け取る";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retemlsendto($index)
{
//Eメール受信状態
switch ($index){
	case 0:
		$zaiko= "メールアドレスのみ";
		break;
	case 1:
		$zaiko= "追加メールアドレスのみ";
		break;
	case 2:
		$zaiko= "両方";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retselltime($index)
{
//Eメール受信状態
switch ($index){
	case 0:
		$zaiko= "9:00-12:00のみ";
		break;
	case 1:
		$zaiko= "12:00-15:00";
		break;
	case 2:
		$zaiko= "15:00-18:00";
		break;
	case 3:
		$zaiko= "18:00-21:00";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retsex($index)
{
//性別
switch ($index){
	case 1:
		$zaiko= "男";
		break;
	case 2:
		$zaiko= "女";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retjob($index)
{
//
switch ($index){
	case 1:
		$zaiko= "会社員";
		break;
	case 2:
		$zaiko= "公務員";
		break;
	case 3:
		$zaiko= "主婦";
		break;
	case 4:
		$zaiko= "自営業";
		break;
	case 5:
		$zaiko= "学生";
		break;
	case 6:
		$zaiko= "パート";
		break;
	case 7:
		$zaiko= "フリーター";
		break;
	case 8:
		$zaiko= "無職";
		break;
	case 9:
		$zaiko= "その他";
		break;
	default:
		$zaiko= "　";
		break;
}
	return $zaiko;
}
function RetORDCRTTP($index)
{
//注文申込方法
switch ($index){
	case 1:
		$zaiko= "PC";
		break;
	case 5:
		$zaiko= "MOBILE";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function RetORDSTSCL($index)
{
//EC側注文ステータス
switch ($index){
	case 1:
		$zaiko= "受付完了";
		break;
	case 2:
		$zaiko= "発送準備中";
		break;
	case 3:
		$zaiko= "出荷完了";
		break;
	case 4:
		$zaiko= "注文取消";
		break;
	case 5:
		$zaiko= "受付保留";
		break;
	case 30:
		$zaiko= "注文保留";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retpaytp($index)
{
//決済方法返却
switch ($index){
	case 2:
		$retvalue= "クレジット";
		break;
	case 4:
		$retvalue= "代引";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function RetSELLROUTETP($index)
{
//買取申込方法
switch ($index){
	case 0:
		$retvalue= "PC";
		break;
	case 1:
		$retvalue= "MOBILE";
		break;
	default:
		$retvalue= "ADMIN";
		break;
}
	return $retvalue;
}
function Retauthstat($index)
{
//本人確認状態
switch ($index){
	case 0:
		$zaiko= "未確認";
		break;
	case 1:
		$zaiko= "確認済";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retkaitoricheck($index)
{
//買取確認書チェック
switch ($index){
	case 0:
		$zaiko= "未受領";
		break;
	case 1:
		$zaiko= "受領済";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retzaikokubunec($index)
{
//商品の区分返却
switch ($index){
	case 1:
		$zaiko= "中古";
		break;
	case 2:
		$zaiko= "新古";
		break;
	case 3:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retteam($index)
{
//商品の区分返却
switch ($index){
	case 1:
		$rtvalue="出荷管理チーム";
		break;
	case 2:
		$rtvalue="入庫管理チーム";
		break;
	case 3:
		$rtvalue="宅本便チーム";
		break;
	case 4:
		$rtvalue="人材教育チーム";
		break;
	case 5:
		$rtvalue="EC事業チーム";
		break;
	case 6:
		$rtvalue="宅本便事業チーム";
		break;
	case 7:
		$rtvalue="商品グループ";
		break;
	case 8:
		$rtvalue="商品データベースグループ";
		break;
	case 9:
		$rtvalue="ECカスタマーチーム";
		break;
	case 10:
		$rtvalue="宅本便カスタマーチーム";
		break;
	case 11:
		$rtvalue="総務経理グループ";
		break;
	case 12:
		$rtvalue="経営企画グループ";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $rtvalue;
}
function Retstaffrreqst($index)
{
//商品の区分返却
switch ($index){
	case 1:
		$rtvalue="G長承認待ち";
		break;
	case 2:
		$rtvalue="経理作業待ち";
		break;
	case 3:
		$rtvalue="スタッフ確認待ち";
		break;
	case 4:
		$rtvalue="G長確認済差戻";
		break;
	case 5:
		$rtvalue="経理差戻";
		break;
	case 6:
		$rtvalue="";
		break;
	case 7:
		$rtvalue="";
		break;
	case 8:
		$rtvalue="";
		break;
	case 9:
		$rtvalue="完了";
		break;
	case 99:
		$rtvalue="キャンセル";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $rtvalue;
}
function Retecpricekind($value){
switch ($value) {
	case 00:
		$retvalue= "新品販売";
		break;
	case 10:
		$retvalue= "中古販売";
		break;
	case 11:
		$retvalue= "中古買取";
		break;
	case 21:
		$retvalue= "特典買取";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function StockworkStatus($value){
switch ($value) {
	case 00:
		$retvalue= "未作業";
		break;
	case 01:
		$retvalue= "作業指示印刷済";
		break;
	case 02:
		$retvalue= "棚出済";
		break;
	case 03:
		$retvalue= "作業済";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}

function PayjobStatus($value){
switch ($value) {
	case 01:
		$retvalue= "与信待ち";
		break;
	case 02:
		$retvalue= "与信済み";
		break;
	case 03:
		$retvalue= "売上確定エラー";
		break;
	case 04:
		$retvalue= "売上確定済み";
		break;
	case 11:
		$retvalue= "与信取消待ち";
		break;
	case 12:
		$retvalue= "与信取消済み";
		break;
	case 21:
		$retvalue= "再与信待ち";
		break;
	case 23:
		$retvalue= "再与信エラー";
		break;
	case 33:
		$retvalue= "FRONT再与信エラー";
		break;
	case 91:
		$retvalue= "別途費用回収済み";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}

?>