<?PHP
require_once("DB.php");

// 2018/12/07 kiyotaka.kikuchi
// パスによって接続先DBを自動選択する。

/**
 * 検証環境に接続させるべきか調べる.
 *
 * <p>パスに "_test" を含む場合は検証環境であると仮定する。</p>
 *
 * @return 検証環境の場合は TRUE, それ以外は FALSE を返す。
 */
function __is_test_mode() {
    return strpos($_SERVER['REQUEST_URI'], '_test') !== FALSE;
}

/**
 * 環境名を取得する.
 *
 * @return 環境名文字列。
 */
function detect_env() {
    if (__is_test_mode()) {
        return 'TEST';
    }
    return 'PROD';
}

/**
 * ECデータベースに接続する.
 *
 * <p>同じディレクトリにある db.ini の内容を読み込む。</p>
 *
 * @return 接続オブジェクト。
 */
function connect_to_ec_database() {
    $ini = parse_ini_file(dirname(__FILE__). "/db.ini", true);
    if (__is_test_mode()) {
        // ECD環境
        $settings = $ini["ecd"];

    } else {
        // 本番環境
        $settings = $ini["ecdb"];
    }
    $dsn = "oci8://". $settings["usr"] . ":" . $settings["pwd"] . "@" . $settings["dbn"];
    $db = DB::connect($dsn);
    if (DB::isError($db)) {
        echo "Fail\n" . DB::errorMessage($db) . "\n";
    }
    $db->setOption('portability', DB_PORTABILITY_NULL_TO_EMPTY | DB_PORTABILITY_NUMROWS);
    return $db;
}

/**
 * 倉庫データベースに接続する.
 *
 * <p>同じディレクトリにある db.ini の内容を読み込む。</p>
 *
 * @return 接続オブジェクト。
 */
function connect_to_souko_database() {
    $ini = parse_ini_file(dirname(__FILE__). "/db.ini", true);
    if (__is_test_mode()) {
        // ECD環境
        $settings = $ini["logi_test"];

    } else {
        // 本番環境
        $settings = $ini["logi"];
    }
    $dsn = "oci8://". $settings["usr"] . ":" . $settings["pwd"] . "@" . $settings["dbn"];
    $db = DB::connect($dsn);
    if (DB::isError($db)) {
        echo "Fail\n" . DB::errorMessage($db) . "\n";
    }
    $db->setOption('portability', DB_PORTABILITY_NULL_TO_EMPTY | DB_PORTABILITY_NUMROWS);
    return $db;
}
