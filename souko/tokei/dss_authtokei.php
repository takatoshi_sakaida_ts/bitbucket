<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>自動承認スキップ統計情報</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

// フォーム送信されたかどうか
if( @$_POST["btn_submit"] != "" )
{
	$FromYr  = addslashes(@$_POST["FromYr"]);
	$FromMon = addslashes(@$_POST["FromMon"]);
	$FromD   = addslashes(@$_POST["FromD"]);
	$ToYr    = addslashes(@$_POST["ToYr"]);
	$ToMon   = addslashes(@$_POST["ToMon"]);
	$ToD     = addslashes(@$_POST["ToD"]);
}
else
{
	$zentime = strtotime("-1 day");
	$FromYr  = date("Y", $zentime);
	$FromMon = date("m", $zentime);
	$FromD   = date("d", $zentime);
	$ToYr    = date("Y");
	$ToMon   = date("m");
	$ToD     = date("d");
}
$FromMon = sprintf("%02d", $FromMon);
$FromD   = sprintf("%02d", $FromD);
$ToMon   = sprintf("%02d", $ToMon);
$ToD     = sprintf("%02d", $ToD);

//print $FromYr.$FromMon.$FromD.$FromHr.$FromMin."<br>";
//print $ToYr.$ToMon.$ToD.$ToHr.$ToMin;

//SQL文のCOUNT関数を使用
// 意図通りの並び順にするために文字列の先頭に数字を入れてある。
// 表示するときに先頭数字は削除する
$sql =	"select".
"  cnt_ass_auto.absdate,".
"  cnt_ass_auto.qty as ass_auto_qty,".
"  cnt_ass_total.qty as ass_total_qty,".
"  cnt_asscode_auto.qty as boxrecept_auto_qty,".
"  cnt_asscode_total.qty as boxrecept_total_qty,".
"  cnt_stock_auto.qty as stock_auto_qty,".
"  cnt_stock_total.qty as stock_total_qty  ".
" from ".
" (".
"  select slipdate as absdate,count(asscode) as qty from(".
"  select ".
"    slipdate,".
"    asscode".
"  from(".
"    select ".
"      slipdate,".
"      auxcode,".
"      substr(auxcode,1,15) as asscode,".
"      substr(auxcode,17,4) as branchnum".
"    from d_slip ".
"    where slipdate >= '".$FromYr.$FromMon.$FromD."' and slipdate <= '".$ToYr.$ToMon.$ToD."' and remark like '%自動承認%'".
" ) ".
"  group by slipdate,asscode".
"  )".
"  group by slipdate".
" )cnt_ass_auto,".
" (".
"  select absdate,count(asscode) as qty from( ".
"  select ".
"    OPERATEDATE as absdate,".
"    asscode ".
"  from d_ass ".
"  where OPERATEDATE <= '".$ToYr.$ToMon.$ToD."' and OPERATEDATE >= '".$FromYr.$FromMon.$FromD."'".
"  group by OPERATEDATE,asscode".
"  ) group by absdate".
" )cnt_ass_total,".
" (".
" select ".
"    slipdate as absdate,".
"    sum(dass.BOX_RECEPT_QTY) as qty ".
"  from(".
"    select ".
"      slipdate,".
"      auxcode,".
"      substr(auxcode,1,15) as asscode,".
"      substr(auxcode,17,4) as branchnum".
"    from d_slip ".
"    where slipdate >= '".$FromYr.$FromMon.$FromD."' and slipdate <= '".$ToYr.$ToMon.$ToD."' and remark like '%自動承認%'".
"  )slp".
"  inner join d_ass dass on slp.asscode = dass.asscode and slp.branchnum = dass.branchnum".
"  group by slp.slipdate".
" )cnt_asscode_auto,".
" (".
"    select ".
"      OPERATEDATE as absdate,".
"      sum(box_recept_qty) as qty ".
"    from d_ass ".
"    where OPERATEDATE >= '".$FromYr.$FromMon.$FromD."' and OPERATEDATE <= '".$ToYr.$ToMon.$ToD."'".
"    group by OPERATEDATE".
" )cnt_asscode_total,".
" (".
" select ".
"    org.slipdate as absdate,".
"    sum(asg.DETAILQTY) as qty".
"  from(".
"    select ".
"      slipdate,".
"      auxcode,".
"      substr(auxcode,1,15) as asscode,".
"      substr(auxcode,17,4) as branchnum".
"    from d_slip ".
"    where slipdate >= '".$FromYr.$FromMon.$FromD."' and slipdate <= '".$ToYr.$ToMon.$ToD."' and remark like '%自動承認%'".
"  )org".
"  left join d_ass_goods asg on org.asscode = asg.ASSCODE and org.branchnum = asg.BRANCHNUM".
"  group by org.slipdate".
" )cnt_stock_auto,".
" (".
"     select ".
"      ass.OPERATEDATE as absdate,".
"      sum(assg.DETAILQTY) as qty".
"    from d_ass ass".
"    left join D_ASS_GOODS assg on ass.ASSCODE = assg.ASSCODE and ass.BRANCHNUM = assg.BRANCHNUM".
"    where ass.OPERATEDATE >= '".$FromYr.$FromMon.$FromD."' and ass.OPERATEDATE <= '".$ToYr.$ToMon.$ToD."'".
"    group by ass.OPERATEDATE".
" )cnt_stock_total".
" where ".
"  cnt_ass_auto.absdate = cnt_ass_total.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_asscode_auto.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_asscode_total.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_stock_auto.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_stock_total.absdate (+)".
" order by cnt_ass_auto.absdate desc";

$sql_bus =	"select".
"  cnt_ass_auto.absdate,".
"  cnt_ass_auto.qty as ass_auto_qty,".
"  cnt_ass_total.qty as ass_total_qty,".
"  cnt_asscode_auto.qty as boxrecept_auto_qty,".
"  cnt_asscode_total.qty as boxrecept_total_qty,".
"  cnt_stock_auto.qty as stock_auto_qty,".
"  cnt_stock_total.qty as stock_total_qty ".
" from ".
" (".
"  select ".
"    a.slipdate as absdate,".
"    count(a.asscode) as qty ".
"  from(".
"    select ".
"      slipdate,".
"      auxcode,".
"      substr(auxcode,1,15) as asscode,".
"      substr(auxcode,17,4) as branchnum".
"    from d_slip ".
"    where slipdate >= '".$FromYr.$FromMon.$FromD."' and slipdate <= '".$ToYr.$ToMon.$ToD."' and remark like '%自動承認%'".
"  )a ".
"  where a.asscode like '00088%'".
"  group by a.slipdate".
" )cnt_ass_auto,".
" (".
"  select absdate,count(*) as qty from(".
"  select ".
"    OPERATEDATE as absdate,".
"    asscode ".
"  from d_ass ".
"  where asscode like '00088%' and OPERATEDATE <= '".$ToYr.$ToMon.$ToD."' and OPERATEDATE >= '".$FromYr.$FromMon.$FromD."'".
"  group by OPERATEDATE,asscode".
"  )group by absdate".
" )cnt_ass_total,".
" (".
" select ".
"    slipdate as absdate,".
"    sum(dass.BOX_RECEPT_QTY) as qty ".
"  from(".
"    select ".
"      slipdate,".
"      auxcode,".
"      substr(auxcode,1,15) as asscode,".
"      substr(auxcode,17,4) as branchnum".
"    from d_slip ".
"    where slipdate >= '".$FromYr.$FromMon.$FromD."' and slipdate <= '".$ToYr.$ToMon.$ToD."' and remark like '%自動承認%'".
"  )slp".
"  inner join d_ass dass on slp.asscode = dass.asscode and slp.branchnum = dass.branchnum".
"  where slp.asscode like '00088%'".
"  group by slp.slipdate".
" )cnt_asscode_auto,".
" (".
"    select ".
"      OPERATEDATE as absdate,".
"      sum(box_recept_qty) as qty ".
"    from d_ass ".
"    where asscode like '00088%' and OPERATEDATE >= '".$FromYr.$FromMon.$FromD."' and OPERATEDATE <= '".$ToYr.$ToMon.$ToD."'".
"    group by OPERATEDATE".
" )cnt_asscode_total,".
" (".
" select ".
"    org.slipdate as absdate,".
"    sum(asg.DETAILQTY) as qty".
"  from(".
"    select ".
"      slipdate,".
"      auxcode,".
"      substr(auxcode,1,15) as asscode,".
"      substr(auxcode,17,4) as branchnum".
"    from d_slip ".
"    where slipdate >= '".$FromYr.$FromMon.$FromD."' and slipdate <= '".$ToYr.$ToMon.$ToD."' and remark like '%自動承認%'".
"  )org".
"  left join d_ass_goods asg on org.asscode = asg.ASSCODE and org.branchnum = asg.BRANCHNUM".
"  where org.asscode like '00088%'".
"  group by org.slipdate".
" )cnt_stock_auto,".
" (".
"     select ".
"      ass.OPERATEDATE as absdate,".
"      sum(assg.DETAILQTY) as qty".
"    from d_ass ass".
"    left join D_ASS_GOODS assg on ass.ASSCODE = assg.ASSCODE and ass.BRANCHNUM = assg.BRANCHNUM".
"    where ass.asscode like '00088%' and ass.OPERATEDATE >= '".$FromYr.$FromMon.$FromD."' and ass.OPERATEDATE <= '".$ToYr.$ToMon.$ToD."'".
"    group by ass.OPERATEDATE".
" )cnt_stock_total".
" where ".
"  cnt_ass_auto.absdate = cnt_ass_total.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_asscode_auto.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_asscode_total.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_stock_auto.absdate (+)".
"  and cnt_ass_auto.absdate = cnt_stock_total.absdate (+)".
" order by cnt_ass_auto.absdate desc";



//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

$res_bus = $db->query($sql_bus);
//if(DB::isError($res_bus)){
//	$res_bus->DB_Error($res_bus->getcode(),PEAR_ERROR_DOE,NULL,NULL);
//}

print "<strong><BR>自動承認スキップ集計結果</strong><br>".date('Y/m/d')."<br><br>\n<HR>";

//検索時間
print "集計期間<br>\n";
print "<form method='POST' action='dss_authtokei.php'>\n";
print "<input type='text' name='FromYr'  size=4 maxlength=4 value=".$FromYr.">/\n";
print "<input type='text' name='FromMon' size=2 maxlength=2 value=".$FromMon.">/\n";
print "<input type='text' name='FromD'   size=2 maxlength=2 value=".$FromD.">移行<br>\n";

print "<input type='text' name='ToYr'  size=4 maxlength=4 value=".$ToYr.">/\n";
print "<input type='text' name='ToMon' size=2 maxlength=2 value=".$ToMon.">/\n";
print "<input type='text' name='ToD'   size=2 maxlength=2 value=".$ToD.">まで<br><br>\n";
print "<input type='submit' name='btn_submit' value='集計'><br><br>\n";
print "</form>\n";

print "<p>■全体 自動承認スキップ集計結果</p>";


print "<table border=1>\n";

print "<tr bgcolor=#ccffff>";
print "<th rowspan=2>日付</th><th width=210 colspan=3>査定件数</th><th width=210 colspan=3>査定箱数</th><th width=210 colspan=3>査定点数</th>";
print "</tr>\n";

print "<tr bgcolor=#ccffff>";
print "<th width=70>承認</th><th width=70>全体</th><th width=70>承認割合</th>";
print "<th width=70>承認</th><th width=70>全体</th><th width=70>承認割合</th>";
print "<th width=70>承認</th><th width=70>全体</th><th width=70>承認割合</th>";
print "</tr>\n";


//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)

$total_shuka=0;

while($row = $res->fetchRow()){
	print "<tr>\n";
	// ソート用に名称の先頭に数字を入れているので削除 
	
	print "<td align='right'>".$row[0] ."</td>\n";
	print "<td align='right'>".number_format($row[1])."</td>\n";
	print "<td align='right'>".number_format($row[2])."</td>\n";
	print "<td align='right'>".floor($row[1] * 100 / $row[2])."%</td>\n";
	print "<td align='right'>".number_format($row[3])."</td>\n";
	print "<td align='right'>".number_format($row[4])."</td>\n";
	print "<td align='right'>".floor($row[3] * 100 / $row[4])."%</td>\n";
	print "<td align='right'>".number_format($row[5])."</td>\n";
	print "<td align='right'>".number_format($row[6])."</td>\n";
	print "<td align='right'>".floor($row[5] * 100 / $row[6])."%</td>\n";

	print "</tr>\n";
}

print "</table>";

//出張買取センター統計
print "<br>";
print "<p>■出買自動承認スキップ集計結果</p>";

print "<table border=1>\n";

print "<tr bgcolor=#ccffff>";
print "<th rowspan=2>日付</th><th width=210 colspan=3>査定件数</th><th width=210 colspan=3>査定箱数</th><th width=210 colspan=3>査定点数</th>";
print "</tr>\n";

print "<tr bgcolor=#ccffff>";
print "<th width=70>承認</th><th width=70>全体</th><th width=70>承認割合</th>";
print "<th width=70>承認</th><th width=70>全体</th><th width=70>承認割合</th>";
print "<th width=70>承認</th><th width=70>全体</th><th width=70>承認割合</th>";
print "</tr>\n";


//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)

//$total_shuka=0;

while($row = $res_bus->fetchRow()){
	print "<tr>\n";
	// ソート用に名称の先頭に数字を入れているので削除 
	
	print "<td align='right'>".$row[0] ."</td>\n";
	print "<td align='right'>".number_format($row[1])."</td>\n";
	print "<td align='right'>".number_format($row[2])."</td>\n";
	print "<td align='right'>".floor($row[1] * 100 / $row[2])."%</td>\n";
	print "<td align='right'>".number_format($row[3])."</td>\n";
	print "<td align='right'>".number_format($row[4])."</td>\n";
	print "<td align='right'>".floor($row[3] * 100 / $row[4])."%</td>\n";
	print "<td align='right'>".number_format($row[5])."</td>\n";
	print "<td align='right'>".number_format($row[6])."</td>\n";
	print "<td align='right'>".floor($row[5] * 100 / $row[6])."%</td>\n";

	print "</tr>\n";
}

print "</table>";


//データの開放
$res->free();
$res_bus->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='../../index.php'"></FORM>
</body>
</html>
