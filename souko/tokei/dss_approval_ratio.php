<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>承認比率</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

// フォーム送信されたかどうか
if( @$_POST["btn_submit"] != "" )
{
	$FromYr  = addslashes(@$_POST["FromYr"]);
	$FromMon = addslashes(@$_POST["FromMon"]);
	$FromD   = addslashes(@$_POST["FromD"]);
	$ToYr    = addslashes(@$_POST["ToYr"]);
	$ToMon   = addslashes(@$_POST["ToMon"]);
	$ToD     = addslashes(@$_POST["ToD"]);
}
else
{
	$zentime = strtotime("-1 day");
	$FromYr  = date("Y", $zentime);
	$FromMon = date("m", $zentime);
	$FromD   = date("d", $zentime);
	$ToYr    = date("Y");
	$ToMon   = date("m");
	$ToD     = date("d");
}
$FromMon = sprintf("%02d", $FromMon);
$FromD   = sprintf("%02d", $FromD);
$ToMon   = sprintf("%02d", $ToMon);
$ToD     = sprintf("%02d", $ToD);

//SQL文のCOUNT関数を使用
$sql =	"SELECT OPERATEDATE, STATUS_KIND, COUNT(*) FROM D_ASS ".
        "WHERE OPERATEDATE >= '".$FromYr.$FromMon.$FromD."' ".
        "AND OPERATEDATE <= '".$ToYr.$ToMon.$ToD."' ".
        "GROUP BY OPERATEDATE, STATUS_KIND ".
        "ORDER BY OPERATEDATE DESC, STATUS_KIND";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
print "<strong><BR>承認比率</strong><br>".date('Y/m/d')."<br><br>\n<HR>";

//検索時間
print "集計期間<br>\n";
print "<form method='POST' action='dss_approval_ratio.php'>\n";
print "<input type='text' name='FromYr'  size=4 maxlength=4 value=".$FromYr.">/\n";
print "<input type='text' name='FromMon' size=2 maxlength=2 value=".$FromMon.">/\n";
print "<input type='text' name='FromD'   size=2 maxlength=2 value=".$FromD.">移行<br>\n";

print "<input type='text' name='ToYr'  size=4 maxlength=4 value=".$ToYr.">/\n";
print "<input type='text' name='ToMon' size=2 maxlength=2 value=".$ToMon.">/\n";
print "<input type='text' name='ToD'   size=2 maxlength=2 value=".$ToD.">まで<br><br>\n";
print "<input type='submit' name='btn_submit' value='集計'><br><br>\n";
print "</form>\n";

print "<table border=1>\n";

print "<tr bgcolor=#ccffff>";
print "<th width=150>査定日</th>";
print "<th width=150>全体件数</th>";
print "<th width=150>承認待ち件数</th>";
print "<th width=150>承認待ち比率</th>";
print "</tr>\n";

//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
$operate_date  = $ToYr.$ToMon.$ToD;
$total_ass_num = 0;
$approval_num  = 0;

while($row = $res->fetchRow()){
	if($operate_date != $row[0]) {
		print "<tr>\n";
		print "<td align='right'>".$operate_date ."</td>\n";
		print "<td align='right'>".number_format($total_ass_num)."</td>\n";
		print "<td align='right'>".number_format($approval_num)."</td>\n";
		print "<td align='right'>".floor($approval_num * 100 / $total_ass_num)."%</td>\n";
		print "</tr>\n";
		$operate_date = $row[0];
		$total_ass_num = 0;
		$approval_num  = 0;
	}
	if($row[1] == "01") {
		$approval_num = $row[2];
	}
	$total_ass_num += $row[2];
}
print "<tr>\n";
print "<td align='right'>".$operate_date ."</td>\n";
print "<td align='right'>".number_format($total_ass_num)."</td>\n";
print "<td align='right'>".number_format($approval_num)."</td>\n";
print "<td align='right'>".floor($approval_num * 100 / $total_ass_num)."%</td>\n";
print "</tr>\n";
print "</table>";

//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='../../index.php'"></FORM>
</body>
</html>
