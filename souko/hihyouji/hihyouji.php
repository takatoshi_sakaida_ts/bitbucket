<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>オトナグループ登録済非表示マスタ検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select g.instorecode,d.GOODSNAME,r.groupid,r.groupname from m_goods m,M_GOODS_GROUP_ITEM g,M_GOODS_GROUP r,M_GOODS_DISPLAY d where m.instorecode=g.instorecode and g.instorecode=d.instorecode and g.GROUPID=r.GROUPID and m.DELETE_FLG='1'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>オトナグループ登録済非表示マスタ検索結果</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td align=center>インストアコード</td><td align=center>商品名</td><td align=center>グループID</td><td align=center>グループ名</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>　". $j."　</td>";
//ロケーションコード
	print "<td width=120>".$row[0] ."</td>";
	print "<td width=330>".$row[1] ."</td>";
	print "<td width=80>".$row[2] ."</td>";
	print "<td width=330>".$row[3] ."</td>";
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>