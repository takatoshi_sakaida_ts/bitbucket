<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>デマチメール登録インストア検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//SQL文のCOUNT関数を使用
$sql = "select MEM_ID,d.GRP_ID,d.NEW_INSTORECODE,d.SEND_STAT  from talertmail_demachi d,tmember m where d.mem_no=m.mem_no and new_instorecode = '".$keyti."' order by d.reg_dm";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>検索結果 インストアコード：". $keyti."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td>メールアドレス</td><td>グループID</td><td>状態</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>". $j."</td>";
//メールアドレス
	print "<td>".$row[0] ."</td>";
//グループID
	print "<td>".$row[1] ."</td>";
//状態
	print "<td>".$row[3] ."</td>";
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>