<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ゾーン毎在庫状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$sql = 
"select l.zonecode, count(*) from d_stock s, m_location l ".
"where s.locationcode = l.locationcode ".
"and l.zonecode in ('A', 'B', 'C', 'D', 'G', 'H', 'I', 'J', 'K', 'L') ".
"group by l.zonecode ".
"order by l.zonecode";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>ゾーン毎在庫状況</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=100 align=center>ゾーン</td>
	<td width=100 align=center>在庫数</td>
</tr>
<tr>
<?php

//取得データのセット
while($row = $res->fetchRow()){

	print "<td align=center>". $row[0] ."</td><td width=100 align=right>".number_format($row[1]) ."点</td></tr>";
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
