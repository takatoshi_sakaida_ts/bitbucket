<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ジャンル毎在庫状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$sql = "SELECT ge.GENRENAME, s.NEWUSED_KIND, COUNT(*) FROM D_STOCK s, M_LOCATION l, M_GOODS g, M_GENRE ge ".
       "WHERE s.LOCATIONCODE = l.LOCATIONCODE ".
       "AND g.INSTORECODE = s.INSTORECODE ".
       "AND g.GENRE0CODE = ge.GENRECODE ".
       "AND l.LOCATION_KIND = '00' ".
       "GROUP BY s.NEWUSED_KIND, ge.GENRENAME ".
       "ORDER BY s.NEWUSED_KIND, ge.GENRENAME";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>ジャンル毎在庫状況</strong><br>".date('Y/m/d H:i')."時点";
?>

<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=100 align=center>ジャンル</td>
	<td width=100 align=center>在庫数</td>
	<td width=100 align=center>新古区分</td>
</tr>
<tr>

<?php
//取得データのセット
while($row = $res->fetchRow()){
	if($row[1] == "0") {
		print "<td align=center>". $row[0] ."</td><td width=100 align=right>".number_format($row[2]) ."点</td><td align=center>". "中古" ."</td></tr>";
	} else {
		print "<td align=center>". $row[0] ."</td><td width=100 align=right>".number_format($row[2]) ."点</td><td align=center>". "新古" ."</td></tr>";
	}
}
//データの開放
$res->free();
$db->disconnect();
?>

</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
