<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>領収証発行</title>
</head>
<body>
<style type="text/css">
#wrapper{
/*全体の枠組み決定*/
	width:780px;
	margin-left: 30px;
	/* position:absolute; */
	/* left:30px; */
}
.side  {
  display: flex;
  justify-content: space-between;
}
div.top{
	/* float:left; */
	font-size: 27pt;
	height: 30pt;
	margin-bottom: 10pt;
	/* margin-bottom: 10pt; */
	/* width:780px; */
	/* position: absolute; */
	/* position:absolute; */
	/* top:20px; */
}
div.topright{
	/* float:left; */
	height: 30pt;
	/* float: right; */
	/* margin-left: : 700px; */
	font-size: 13pt;
	/* width:780px; */
	/* position:absolute; */
	/* top: 20px; */
	/* left: 600px; */
}
div.name{
	font-size: 14pt;
	margin-top: 5pt;
	margin-bottom: 10pt;
	/* position: inherit; */
	/* position:absolute; */
	/* top:80px; */
}
div.kingaku {
	font-size: 22pt;
	text-indent:10px;
	/* position:absolute; */
	top:120px;
	width:400px;
	padding-top:20px;
	padding-bottom:20px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 1px solid black;
}
div.sina {
	font-size: 14pt;
	/* position:absolute; */
	/* top:220px; */
}
div.sina2{
	font-size: 13pt;
	text-indent:30px;
	margin-top:5px;
}
div.sina3{
	/* float:left; */
	font-size: 12pt;
	line-height: 130%;
	/* position:absolute; */
	top:390px;
	width:300px;
	padding-top:10px;
	padding-bottom:10px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 1px solid black;
	margin-top:5px;
	margin-bottom: 10px;
}
div.sign {
	/* float:left; */
	/* width:300px; */
	/* margin-left: 500px; */
	/* position:absolute; */
	/* top: 380pt; */
	/* left: 500pt; */
	margin-top: -50px;
}
div.detail {
	font-size: 11pt;
	/* position:absolute; */
	top:450px;
}
div.top_hikae{
	font-size: 27pt;
	/* position:absolute; */
	top:520px;
}
div.topright_hikae{
	font-size: 13pt;
	/* position:absolute; */
	top:520px;
	left:500px;
}
div.name_hikae{
	font-size: 14pt;
	/* position:absolute; */
	top:580px;
}
div.kingaku_hikae {
	font-size: 22pt;
	text-indent:10px;
	/* position:absolute; */
	top:620px;
	width:400px;
	padding-top:20px;
	padding-bottom:20px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 1px solid black;
}
div.sina_hikae {
	font-size: 14pt;
	/* position:absolute; */
	top:720px;
}
div.sina2_hikae{
	font-size: 13pt;
	text-indent:30px;
	margin-top:5px;
}
div.sina3_hikae{
	font-size: 12pt;
	line-height: 130%;
	/* position:absolute; */
	top:370px;
	width:300px;
	padding-top:10px;
	padding-bottom:10px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 1px solid black;
	margin-top:5px;
}
div.sign_hikae {
	/* position:absolute; */
	top:850px;
	left:400px;
}
div.line_hikae{
	font-size: 12pt;
	line-height: 130%;
	/* position:absolute; */
	top:470px;
	width:720px;
	padding-top:10px;
	padding-bottom:10px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 0px solid black;
	margin-top:5px;
}
</style>

<FORM method="POST" action="receipt_coupon.php">
<div id =wrapper>
<?php
require '../parts/pagechk.inc';
//ファイルの読み込み
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
// 前画面からの検索キーワードを取得する
$receptioncode = addslashes(@$_POST["receptioncode"]);
$orderdate = addslashes(@$_POST["orderdate"]);
$count = addslashes(@$_POST["count"]);
$name = stripslashes(addslashes(@$_POST["name"]));
$sinamei = stripslashes(addslashes(@$_POST["sinamei"]));
$syoukei = addslashes(@$_POST["syoukei"]);
$souryou = addslashes(@$_POST["souryou"]);
$souryouc = addslashes(@$_POST["souryouc"]);
$credit = addslashes(@$_POST["credit"]);
$daibiki = addslashes(@$_POST["daibiki"]);
$daibikic = addslashes(@$_POST["daibikic"]);
$couponc = addslashes(@$_POST["couponc"]);
$coupon_amt = addslashes(@$_POST["coupon_amt"]);
$goukei = addslashes(@$_POST["goukei"]);
$tantou = addslashes(@$_POST["tantou"]);
$hakkoubi = addslashes(@$_POST["hakkoubi"]);
//$postdetail = addslashes(@$_POST["detail"]);
$today = getdate();
output_log('orderno:'.$receptioncode,$tantou.":".$_SESSION["user_id"],'receipt_print_ec');
print '<div class=side>';
print '<div class=top>領収証</div>';
print '<div class=topright>発行日：'. $hakkoubi . '</div>';
print '</div>';
print '<div class=name>' . $name . '　様</div>';

print '<div class=kingaku>領収金額：　　' . $goukei . '円　　</div>';
print '<div class=sina><br>但し　' . $sinamei . 'として<br><br>上記正に領収いたしました';
if ($credit=="1"){
print '<div class=sina2>支払方法：クレジットカード払い</div>';
}
print '<div class=sina2>注文番号：'. $receptioncode . '</div>';
if ($souryouc=="1"){
print '<div class=sina2>送料：' . $souryou . '円</div>';
}
if ($daibikic=="1"){
print '<div class=sina2>代引手数料：' . $daibiki . '円</div>';
}
if ($couponc=="1"){
print '<div class=sina2>クーポン割引：' . $coupon_amt . '円</div>';
}
print '</div>';
print '<div class=side>';
print '<div class=sina3>　ご請求金額（税込）' . $syoukei . '円</div>';
print "<div class=sign><img src='bol_sign.gif' width=250 height=105></div>";
print '</div>';
//print '　消費税(5%)' . $zei . '円</div>';
	$k=1;
if (isset($_POST["detail"])){
	$count=$_POST["count"]+0;
	$postdetail = $_POST["detail"];
//print count($postdetail);
//print $postdetail[0];
//print $postdetail[1];
	for ($j=0; $j<count($postdetail); $j++) {
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
		if ($k==1){
			//入力項目
			print '<div class=detail><hr><table border=1>';
			print '<tr>';
	//項目名の表示
			print "<td nowrap width=40>項番</td><td nowrap width=120>区分</td><td nowrap width=380>商品名</td><td nowrap width=120>単価</td><td nowrap width=120>数量</td>";
			print "</tr>";
		}
			print "<tr>";
		//項番
			print "<td>".$k ."</td>";
			print '<td>'.$_POST["kubun".$postdetail[$j]].'</td>';
		//商品名
			print '<td>'.$_POST["item".$postdetail[$j]].'</td>';
		//単価
			print '<td>'.$_POST["tanka".$postdetail[$j]].'</td>';
		//数量
			print '<td>'.$_POST["suu".$postdetail[$j]].'</td>';
			print "</TR>";
			$k++;
	}
		print "</table></div>";
}
?>
</div>
</body>
</html>
