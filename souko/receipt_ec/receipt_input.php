<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
//require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti2 = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) && empty($keyti2)) {
	print "検索条件を入力してください<br>";
}
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
$sql1 = "select d.ord_no,substr(d.ord_no,1,8),d.ORDR_NM_FST,d.ORDR_NM_MID,d.CASH_COM,";
$sql1 = $sql1 . "d.DELV_COM,d.PAY_TP,(d.TOT_INV_AMT-d.ORD_CNCL_AMT),d.TOT_TAX";
$sql2 = "";
if (empty($keyti)) {}else{
	$sql2 = $sql2 . " d.ord_no ='".$keyti."'";
}
if (empty($keyti2)) {}else{
	if (empty($keyti)) {}else{
		$sql2 = $sql2 . " and ";
	}
	$sql2 = $sql2 . " (d.ORDR_NM_FST like '%".$keyti2."%'";
	$sql2 = $sql2 . " or d.ORDR_NM_MID like '%".$keyti2."%'";
	$sql2 = $sql2 . " or d.ORDR_NM_LAST like '%".$keyti2."%'";
	$sql2 = $sql2 . " or d.ORDR_NM_ETC like '%".$keyti2."%')";
}

$sql = $sql1 . " from torder d where " . $sql2;
$sql = $sql . " union all ";
$sql = $sql . $sql1 . " from torder_old d where " . $sql2;

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索結果　領収書を発行する注文番号を選択してください</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>項番</td><td nowrap width=120>注文番号</td><td nowrap width=120>受付日</td><td nowrap width=200>顧客名</td>";
print "<td nowrap>注文金額(税込)</td></tr>";
while($row = $res->fetchRow()){
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//注文番号
	print "<td><a href='receipt_output.php?receptioncode=" .$row[0] . "'>".$row[0] ."</td>";
//受付日
	print "<td>".Rethiduke($row[1]) ."</td>";
//顧客名
	print "<td>".$row[2] .$row[3]."</td>";
//注文金額合計(税込)
	print "<td align=right>".$row[7] ."</td>";
	print "</tr>";
	$j++;
}
	print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>