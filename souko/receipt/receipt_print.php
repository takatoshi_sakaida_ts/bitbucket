<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>領収証発行</title>
</head>
<body>
<style type="text/css">
#wrapper{
/*全体の枠組み決定*/
	width:720px;
	position:absolute;
	left:30px;
}
div.top{
	font-size: 27pt;
	position:absolute;
	top:50px;
}
div.topright{
	font-size: 13pt;
	position:absolute;
	top:100px;
	left:500px;
}
div.name{
	font-size: 14pt;
	position:absolute;
	top:150px;
}
div.kingaku {
	font-size: 22pt;
	text-indent:10px;
	position:absolute;
	top:200px;
	width:400px;
	padding-top:20px;
	padding-bottom:20px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 1px solid black;
}
div.sina {
	font-size: 14pt;
	position:absolute;
	top:300px;
}
div.sina2{
	font-size: 13pt;
	text-indent:30px;
	margin-top:5px;
}
div.sina3{
	font-size: 12pt;
	line-height: 130%;
	position:absolute;
	top:450px;
	width:300px;
	padding-top:10px;
	padding-bottom:10px;
	border-top: 1px solid black;
	border-left: 0px solid black;
	border-right: 0px solid black;
	border-bottom: 1px solid black;
	margin-top:5px;
}
div.sign {
	position:absolute;
	top:430px;
	left:400px;
}
div.detail {
	font-size: 11pt;
	position:absolute;
	top:600px;
}
</style>

<FORM method="POST" action="receipt.php">
<div id =wrapper>
<?php

//ファイルの読み込み
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
// 前画面からの検索キーワードを取得する
$receptioncode = addslashes(@$_POST["receptioncode"]);
$orderdate = addslashes(@$_POST["orderdate"]);
$count = addslashes(@$_POST["count"]);
$name = stripslashes(addslashes(@$_POST["name"]));
$sinamei = stripslashes(addslashes(@$_POST["sinamei"]));
$syoukei = addslashes(@$_POST["syoukei"]);
$souryou = addslashes(@$_POST["souryou"]);
$souryouc = addslashes(@$_POST["souryouc"]);
$daibiki = addslashes(@$_POST["daibiki"]);
$daibikic = addslashes(@$_POST["daibikic"]);
$zei = addslashes(@$_POST["zei"]);
$goukei = addslashes(@$_POST["goukei"]);
$detailc = addslashes(@$_POST["detailc"]);
$tantou = addslashes(@$_POST["tantou"]);
$today = getdate();
output_log('orderno:'.$receptioncode,$tantou,'receipt_print');
print '<div class=top>領収証</div>';
print '<div class=topright>発行日：'. $today["year"] . '/' . $today["mon"] . '/' . $today["mday"] . '</div>';
print '<div class=name>' . $name . '　様</div>';
?>
<div class=sign>
<img src='bol_sign.gif' width=250 height=105>
</div>
<?PHP
print '<div class=kingaku>領収金額：　　' . $goukei . '円　　</div>';
print '<div class=sina>但し　' . $sinamei . 'として<br><br>上記正に領収いたしました';
print '<div class=sina2>注文番号：'. $receptioncode . '</div>';
if ($souryouc=="1"){
print '<div class=sina2>送料：' . $souryou . '円</div>';
}
if ($daibikic=="1"){
print '<div class=sina2>代引手数料：' . $daibiki . '円</div>';
}
print '</div>';
print '<div class=sina3>　税抜金額' . $syoukei . '円<br>';
print '　消費税(5%)' . $zei . '円</div>';
if ($detailc=="1"){
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/login_souko.php");
	//SQL文のCOUNT関数を使用
	$sql = "select d.RECEPTIONCODE,d.RECEPTIONDATE,d.CUSTOMERNAME1,d.CUSTOMERNAME2,d.RECEPTION_AMOUNT,";
	$sql = $sql . "d.RECEPTION_C_TAX,d.COD_PRICE,d.SHIPPINGPRICE,d.EXPENCE_KIND,";
	$sql = $sql . "g.instorecode,g.unitprice,m.goodsname,g.newused_kind,m.goodsname_ext1,m.goodsname_ext2 ";
	$sql = $sql . " from d_reception d,D_RECEPTION_goods g,m_goods_display m";
	$sql = $sql . " where d.RECEPTIONCODE=g.RECEPTIONCODE and g.instorecode=m.instorecode and g.cancel_kind='0' ";
	$sql = $sql . " and d.receptioncode ='".$receptioncode."' order by g.detailno";
	//print $sql;
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	$j=0;
	while($row = $res->fetchRow()){
		if ($j==0){
			//入力項目
			print '<div class=detail><hr><table border=1>';
			print '<tr>';
	//項目名の表示
			print "<td nowrap width=40>項番</td><td nowrap width=120>区分</td><td nowrap width=380>商品名</td><td nowrap width=120>単価</td>";
			print "</tr>";
		}
		$j++;
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
		print "<tr>";
	//項番
		print "<td>".$j ."</td>";
		print "<td>".Retzaikokubunforuser($row[12])."</td>";
	//商品名
		print "<td>".$row[11] ."　" .$row[13] ."　" . $row[14] ."</td>";
	//単価
		print "<td>".$row[10] ."</td>";
		print "</TR>";
	}
		print "</table></div>";
	//データの開放
	$res->free();
	$db->disconnect();
}
?>
</div>
</body>
</html>