<?PHP
require '../parts/pagechk.inc';
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>オトナグループカート登録状況確認</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<?php
//ファイル読込
require_once("DB.php");
require_once("Log.php");
require_once("../log/loger.inc");
$ini = parse_ini_file("../parts/db.ini",true);

//ログファイル生成
$conf = array('mode'=>0777,'lineFormat' => '%1$s %3$s [%2$s] %4$s','timeFormat'=>'%Y-%m-%d %H:%M:%S');
$date = date("Ymd");
$user_id = isset($_SESSION['user_name']) ? $_SESSION['user_name'] : "";
$log = &Log::factory('file', './log/' . $date . '.log', $user_id, $conf);

//画面用変数初期化
$isCart = false;
$saleStat = true;
$msg = "";
$cartCnt = 0;
$grpData = null;
$dtlData = array();

//入力値取得
$grpId = @$_POST["GRP_ID"];
$searchOn = @$_POST["SEARCH"];
$switchOn = @$_POST["SWITCH"];

//バリデーション
if (empty($grpId) && (!empty($searchOn) || !empty($switchOn))) {
	$msg = "グループIDが指定されていません";
} else if (!empty($grpId) && !preg_match('/^8[0-9]{9}$/', $grpId)) {
	$msg = "グループIDを正しく指定して下さい（8で始まる10桁の半角数字）";
}

if (!empty($grpId) && empty($msg)) {
	//ECDBコネクション取得
	// $dsn = "oci8://". $ini['ecd']['usr'] . ":" . $ini['ecd']['pwd'] . "@" . $ini['ecd']['dbn'];//ECD
	$dsn = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];//本番
	$db = DB::connect($dsn);
	if(DB::isError($db)){
		die($db->getMessage());
	}

	//カート登録状況取得
	$cartSql = "select count(*) from ("
				. "select CART_NO from TCART"
				. " where CART_STAT = 1 and GRP_ID = '" . $grpId . "'"
				. " group by CART_NO, GRP_ID"
				. ")";
	$cartRes = $db->query($cartSql);
	if (DB::isError($cartRes)) {
		die($cartRes->getMessage());
	}
	$cartCnt = $cartRes->fetchRow();
	$cartCnt = $cartCnt[0];
	$isCart = $cartCnt > 0;
	$cartRes->free();

	//グループ情報取得
	$grpSql = "select SALE_STAT, GRP_ID, GRP_NM, AUTHOR from TGOODS_GRP"
				." where GRP_ID = '" . $grpId . "'";
	$grpRes = $db->query($grpSql);
	if (DB::isError($grpRes)) {
		die($grpRes->getMessage());
	}
	$grpData = $grpRes->fetchRow();
	if (empty($grpData)) {
		$msg = "存在しないグループIDです";
	} else {
		$saleStat = $grpData[0] == "1";
	}
	$grpRes->free();

	//子商品情報取得
	$dtlSql = "select t1.INSTORECODE, t1.GOODS_NAME1, t1.GOODS_NAME2, t1.GOODS_NAME3,"
				. " t3.STOCK_USED, t3.STOCK_NEW, t3.STOCK_RSV, t3.STOCK_RSV_ENABLE_FLG" 
				. " from TGOODS_BKO t1, TGOODS_GRPDTL t2, TSTOCK_BKO t3"
				. " where t1.INSTORECODE = t2.INSTORECODE"
				. " and t1.INSTORECODE = t3.INSTORECODE"
				. " and t2.GRP_ID = '" . $grpId . "'"
				. " and t1.SALE_STAT != '0'"
				. " order by t2.DISP_PRIR";
	$dtlRes = $db->query($dtlSql);
		if (DB::isError($dtlRes)) {
		die($dtlRes->getMessage());
	}
	while ($dtlRow =& $dtlRes->fetchRow()) {
		$dtlData[] = $dtlRow;
	}
	$dtlRes->free();

	//表示切替処理
	if(!is_null($switchOn)) {
		if ($switchOn == "0" && !$saleStat) {
			$msg = "すでに非表示の状態です";
		} else if ($switchOn == "1" && $saleStat) {
			$msg = "すでに表示の状態です";
		} else {
			$updateSql = "update TGOODS_GRP set SALE_STAT = '" . $switchOn . "' where GRP_ID = '" . $grpId . "'";
			$updateRes = $db->query($updateSql);
			if (DB::isError($updateRes)) {
				$msg = "更新に失敗しました！";
				die($updateRes->getMessage());
			} else {
				if ($updateRes > 0) {
					$msg = ($switchOn == "0" ? "非表示" : "表示") . "に更新しました";
					$saleStat = !$saleStat;
				} else {
					$msg = "更新に失敗しました！";
				}
			}
		}
	}
	$db->disconnect();
}
?>
<body>
<div class="container">
	<h4><strong class="text-success">オトナグループカート登録状況確認</strong><!--<small>　※ECD向き</small>--></h4>
	<form method="post">
		<div class="input-group">
			<span class="input-group-addon">グループID</span>
			<input class="form-control" type="text" name="GRP_ID" value="<?php echo $grpId;?>">
			<span class="input-group-btn">
				<button class="btn btn-default" type="submit" name="SEARCH" value="1">
					<i class='glyphicon glyphicon-search'></i>
				</button>
			</span>
		</div>
	</form>
	<HR>
	<div style="margin-bottom: 20px;">
		<a class="btn btn-default" href="/index.php">戻る</a>
		<span class="text-warning"><strong><?php echo $msg;?></strong></span>
	</div>
	<?php if (!empty($grpData)) {?>
	<table class="table table-bordered">
		<tr class="info">
			<th>グループID</th>
			<th>グループ名</th>
			<th>著者</th>
		</tr>
		<tr>
			<td><?php echo $grpData[1];?></td>
			<td><?php echo $grpData[2];?></td>
			<td><?php echo $grpData[3];?></td>
		</tr>
	</table>

	<table class="table table-bordered">
		<tr class="info">
			<th>カート登録状況</th>
			<th>表示状態</th>
		</tr>
		<tr>
			<td class="<?php echo $isCart ? "text-danger" : "text-info";?>">
				<strong><?php echo $isCart ? "あり（" . $cartCnt . "件）" : "なし";?></strong>
			</td>
			<td>
				<form method="post">
					<?php
					echo $saleStat ? "表示" : "非表示";
					if (!$isCart && !empty($grpData)) {
						if ($saleStat) {
							echo "<button class=\"btn btn-danger btn-sm pull-right\" name=\"SWITCH\" value=\"0\">非表示にする</button>";
						} else {
							echo "<button class=\"btn btn-success btn-sm pull-right\" name=\"SWITCH\" value=\"1\">表示にする</button>";
						}
					}
					?>
					<input type="hidden" name="GRP_ID" value="<?php echo $grpId;?>">
				</form>
			</td>
		</tr>
	</table>

		<table class="table table-bordered">
		<tr class="info">
			<th>インストアコード</th>
			<th>商品名</th>
			<th>中古在庫</th>
			<th>新古在庫</th>
			<th>新品取り扱い</th>
		</tr>
		<?php foreach($dtlData as $dtl) {?>
		<tr>
			<td><?php echo $dtl[0];?></td>
			<td><?php echo $dtl[1] . $dtl[2] . $dtl[3];?></td>
			<td><?php echo $dtl[4];?></td>
			<td><?php echo $dtl[5];?></td>
			<td><?php echo $dtl[6] == 1 && $dtl[7] == 1 ? "○": "×";?></td>
		</tr>
		<?php }?>
	</table>

	<?php }?>
</div>
</body>
</html>
