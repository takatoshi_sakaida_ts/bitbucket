<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
$now=getdate();
$printnow=$now['year'] . '/'. $now['mon'] . '/'. $now['mday'] . ' '. $now['hours'] . ':'. $now['minutes'] . ':'. $now['seconds'];
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "SELECT COUNT(GROUPID) FROM M_GOODS_GROUP WHERE DELETE_FLG=0";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$row = $res->fetchRow();
$group_num=$row[0];
//
$res->free();
$sql = "SELECT COUNT(*) FROM M_GOODS_GROUP_ITEM";
$res2 = $db->query($sql);
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$row = $res2->fetchRow();
$group_child_num=$row[0];
//検索結果の表示
print "<strong>検索結果 登録グループ数 </strong><BR><font size=-1>　　　　取得日時". $printnow ."</font>\n";
print "<br><table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<td>グループ数</td><td>アイテム数</td></tr>";
	print "<tr>";
//在庫番号
	print "<td>". $group_num ."</td>";
//取引日付
	print "<td>". $group_child_num ."</td>";
	print "</TR>";
print "</table><br>";
//データの開放
$res2->free();
$db->disconnect();
//ファイル出力
if (!$fp = fopen("groupcount.log","a")){
	echo "Cannot write to file";
	exit;
}
fwrite($fp,$printnow . "," . $group_num . "," . $group_child_num . "\n");
fclose($fp);
?>
<a href="readcountgroup.php">過去の登録確認はこちらから</a>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>