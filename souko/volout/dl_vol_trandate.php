<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>  ボランティア振込済データダウンロード </title>
</heade>
<strong>ボランティア振込済データダウンロード■■■振込データ出力日絞込み版■■■</strong>
<br><br>
<form method="POST" action="dl_volout_trandate.php" name="frmdlvol">
データ取得対象期間を入力してください（振込データ出力日YYYYMMDD）<br>
開始日付　<input type="text" name="T1" maxlength="8"><BR>
終了日付　<input type="text" name="T2" maxlength="8"><BR><BR>
データ取得対象団体を選択してください<br>
<input type="hidden" name="T3" value="">
<input type="hidden" name="T4" value="0">
<table>
<tr>
<th>ボランティア種類</th>
<td>
<select name="parentS" onchange="createChildOptions(this.form)" style="width:350px;">
<option value="">ボランティア種類を選択して下さい</option>
<option value="1">Z10**　ボランティア</option>
<option value="2">Z60**　提携法人</option>
<option value="3">Z70**　企業ボランティア</option>
<option value="4">Z8***　企業ボランティア</option>
<option value="5">ZP***　企業ボランティア</option>
</select>
</td>
</tr>
<tr>
<th>子ジャンル</th>
<td><!--表示位置--><div id="childS"></div></td>
</tr>
</table>

<script type="text/javascript">
/* 子ジャンル（selectC）用の配列 */
	var item = new Array();

	item[0] = new Array();
	item[0][0]="---------------------";

        /* Z10 */
	item[1] = new Array();
	item[1][0]="子ジャンルを選択して下さい";
	item[1][1]="Z1のすべて";
	item[1][2]="Z1001 Room to Read";
	item[1][3]="Z1002・Z1006 シャンティ国際ボランティア会";
	item[1][4]="Z1003 ピースウィンズ・ジャパン";
	item[1][5]="Z1004 シャプラニール";
	item[1][6]="Z1005 JEN";
	item[1][7]="Z1007 かながわキンタロウ";
	item[1][8]="Z1029 大阪YMCA";
	item[1][9]="Z1030 グッドネーバーズ・ジャパン";
	item[1][10]="Z1031 国連ウィメン日本協会";
	item[1][11]="Z1032 湘南ふじさわシニアネット";
	item[1][12]="Z1033 アジア保健研修所(AHI)";
	item[1][13]="Z1034 ウィメンズアクションネットワーク";
    /* 2016/02/12 add */
	item[1][14]="Z11　日本赤十字社のすべて";
	item[1][15]="Z1100 日本赤十字社 本社";
	item[1][16]="Z1111 日本赤十字社 埼玉";
	item[1][17]="Z1112 日本赤十字社 千葉";
	item[1][18]="Z1113 日本赤十字社 東京";
	item[1][19]="Z1114 日本赤十字社 神奈川";
	item[1][20]="Z1035 アーユス仏教国際協力ネットワーク";
	item[1][21]="Z1036 しみん基金・こうべ";
	item[1][22]="Z1037 売って支援プログラム（熊本地震）";
	item[1][23]="Z1038 東京都日中友好協会";
	item[1][24]="Z1039 結核予防会";
	item[1][25]="Z1040 民際センター";
	item[1][26]="Z1041 しょうがっこうをおくる会";
	item[1][27]="Z1042 神奈川大学";
	item[1][28]="Z1043 ICAN（アイキャン）";
	item[1][29]="Z1044 地球環境基金";
	item[1][30]="Z1045 HFI（Hope and Faith International）";
	item[1][31]="Z1046 ACE（エース）";
	item[1][32]="Z1047 3keys（スリーキーズ）";
	item[1][33]="Z1048 町田ゼルビア";
	item[1][34]="Z1049 SC相模原";
	item[1][35]="Z1050 幼い難民を考える会(CYR)";
	item[1][36]="Z1051 JHP学校をつくる会";
	item[1][37]="Z1052 北海道森と緑の会";
	item[1][38]="Z1053 アクセス";
	item[1][39]="Z1054 ケア・インターナショナルジャパン";
	item[1][40]="Z1055 世界の子どもにワクチンを(JCV)";
	item[1][41]="Z1056 ジャパンハート";
	item[1][42]="Z1057 ブリッジ エーシア ジャパン(BAJ)";
	item[1][43]="Z1058 売って支援プログラム（東日本大震災）";
	item[1][44]="Z1059 よこはまユース";
	item[1][45]="Z1123 日本赤十字社 愛知";
	item[1][46]="Z1060 ボーイスカウト日本連盟";
	item[1][47]="Z1061 国境なき医師団";
	item[1][48]="Z1127 日本赤十字社 大阪";
	item[1][49]="Z1063 国境なき子どもたち";
	item[1][50]="Z1064 ちばのWA";
	item[1][51]="Z1062 全国骨髄バンク";
	item[1][52]="Z1101 日本赤十字社 北海道";
	item[1][53]="Z1102 日本赤十字社 青森";
	item[1][54]="Z1103 日本赤十字社 岩手";
	item[1][55]="Z1104 日本赤十字社 宮城";
	item[1][56]="Z1105 日本赤十字社 秋田";
	item[1][57]="Z1106 日本赤十字社 山形";
	item[1][58]="Z1107 日本赤十字社 福島";
	item[1][59]="Z1108 日本赤十字社 茨城";
	item[1][60]="Z1109 日本赤十字社 栃木";
	item[1][61]="Z1110 日本赤十字社 群馬";
	item[1][62]="Z1115 日本赤十字社 新潟";
	item[1][63]="Z1116 日本赤十字社 富山";
	item[1][64]="Z1117 日本赤十字社 石川";
	item[1][65]="Z1118 日本赤十字社 福井";
	item[1][66]="Z1119 日本赤十字社 山梨";
	item[1][67]="Z1120 日本赤十字社 長野";
	item[1][68]="Z1121 日本赤十字社 岐阜";
	item[1][69]="Z1122 日本赤十字社 静岡";
	item[1][70]="Z1124 日本赤十字社 三重";
	item[1][71]="Z1125 日本赤十字社 滋賀";
	item[1][72]="Z1126 日本赤十字社 京都";
	item[1][73]="Z1128 日本赤十字社 兵庫";
	item[1][74]="Z1129 日本赤十字社 奈良";
	item[1][75]="Z1130 日本赤十字社 和歌山";
	item[1][76]="Z1131 日本赤十字社 鳥取";
	item[1][77]="Z1132 日本赤十字社 島根";
	item[1][78]="Z1133 日本赤十字社 岡山";
	item[1][79]="Z1134 日本赤十字社 広島";
	item[1][80]="Z1135 日本赤十字社 山口";
	item[1][81]="Z1136 日本赤十字社 徳島";
	item[1][82]="Z1137 日本赤十字社 香川";
	item[1][83]="Z1138 日本赤十字社 愛媛";
	item[1][84]="Z1139 日本赤十字社 高知";
	item[1][85]="Z1140 日本赤十字社 福岡";
	item[1][86]="Z1141 日本赤十字社 佐賀";
	item[1][87]="Z1142 日本赤十字社 長崎";
	item[1][88]="Z1143 日本赤十字社 熊本";
	item[1][89]="Z1144 日本赤十字社 大分";
	item[1][90]="Z1145 日本赤十字社 宮崎";
	item[1][91]="Z1146 日本赤十字社 鹿児島";
	item[1][92]="Z1066 売って支援プログラム（平成30年7月豪雨災害）";
	item[1][93]="Z1065 ブルーシー・アンド・グリーンランド財団";
	item[1][94]="Z1067 横浜市社会福祉協議会";
	item[1][95]="Z1068 エンパワメントかながわ";
	item[1][96]="Z1069 日本国際ボランティアセンター";
	item[1][97]="Z1070 アーツサポート関西";

        /* Z60 */
	item[2]= new Array();
	item[2][0]="子ジャンルを選択して下さい";
	item[2][1]="Z6のすべて";
	item[2][2]="Z6001 スターツピタットハウス";
	item[2][3]="Z6002 大京アステージ(くらしスクエア)";
	item[2][4]="Z6003 大京アステージ(コンシェルジュ)";
	item[2][5]="Z6004 三井不動産住宅サービス";
	item[2][6]="Z6005 アスク";
	item[2][7]="Z6006 インターファーム";
	item[2][8]="Z6007 アート引越しセンター";
	item[2][9]="Z6008 クラブパナソニック";
	item[2][10]="Z6009 大東建託パートナーズ";
	item[2][11]="Z6010 アリさんマークの引越社";
	item[2][12]="Z6011 イヌイ運送";
	item[2][13]="Z6012 日本通運";
	item[2][14]="Z6013 ハウスコム";
	item[2][15]="Z6014 オーネット";
	item[2][16]="Z6015 バイク王";
	item[2][17]="Z6016 三菱地所コミュニティ";
	item[2][18]="Z6017 コミュニティワン";
	item[2][19]="Z6018 住友不動産建物サービス";
	item[2][20]="Z6019 リトル・ママ";
	item[2][21]="Z6020 レジデンスクラブ";
	item[2][22]="Z6021 JAF";
	item[2][23]="Z6022 ぐるなび食市場";
	item[2][24]="Z6023 ヤマトホームコンビニエンス（クロネコヤマト引越センター）";
	item[2][25]="Z6024 ファミリー引越センター";
	item[2][26]="Z6025 ジャパンネット銀行";
	item[2][27]="Z6026 暮らしのサポート";
	item[2][28]="Z6027 三井のすまいLOOP(ループ)";
	item[2][29]="Z6028 エイブル";	
	item[2][30]="Z6029 MAST(マスト)";	
	item[2][31]="Z6030 フロムヴイ";	
	item[2][32]="Z6031 大和ライフネクスト";	
	item[2][33]="Z6032 レオパレス21";	
	item[2][34]="Z6033 ズバット引越し比較";	
	item[2][35]="Z6034 インターネットコンシェルジュサービス";
	item[2][36]="Z6035 長谷工コミュニティ";
	item[2][37]="Z6036 QLC(コンシェルジュ)";
	item[2][38]="Z6037 住宅情報館(城南建設)";
	item[2][39]="Z6038 ジャックスカード";
	item[2][40]="Z6039 積村ビル管理";
	item[2][41]="Z6040 東急住宅リース";
	item[2][42]="Z6041 ハッピークラブモール";
	item[2][43]="Z6042 おうちCO-OP";
	item[2][44]="Z6043 ハイホー";
	item[2][45]="Z6044 ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）";
	item[2][46]="Z6045 ソフトバンクグループ";
	item[2][47]="Z6046 オリックス・クレジット";
	item[2][48]="Z6047 リロクラブ";
	item[2][49]="Z6048 メディアカフェポパイ";
	item[2][50]="Z6049 アメリカン・エキスプレス";
	item[2][51]="Z6050 ネクスト・ゴルフ・マネジメント";
	item[2][52]="Z6051 エイコータウン";
	item[2][53]="Z6052 社員向け_レオパレス21";
	item[2][54]="Z6053 macalon+（マカロンプラス）";
	item[2][55]="Z6054 タイムズクラブ";
	item[2][56]="Z6055 ゆとライフドットコム";
	item[2][57]="Z6056 クラスエル";
	item[2][58]="Z6057 バイクブロス";
	item[2][59]="Z6058 泉友";
	item[2][60]="Z6059 富士フイルム生活協同組合";
	item[2][61]="Z6060 リコー三愛グループ";
	item[2][62]="Z6061 ヴァリック";
	item[2][63]="Z6062 LIFULL引越し";
	item[2][64]="Z6063 千葉県庁生活協同組合";
	item[2][65]="Z6064 エポスカード";
	item[2][66]="Z6065 エフコープ";
	item[2][67]="Z6066 明電グループ";
	item[2][68]="Z6067 青森県庁生協";
	item[2][69]="Z6068 おおさかパルコープ";
	item[2][70]="Z6069 よどがわ市民生協";
	item[2][71]="Z6070 東急リバブル";
	item[2][72]="Z6071 auコレトク";
	item[2][73]="Z6072 ARUHI暮らしのサービス";
	item[2][74]="Z6073 京都生協";
	item[2][75]="Z6074 FUKUYA";
	item[2][76]="Z6075 群馬県庁生活協同組合";
	item[2][77]="Z6076 ライフサポート倶楽部";
	item[2][78]="Z6077 ベルスファミリークラブ";
	item[2][79]="Z6078 群馬県学校生協";
	item[2][80]="Z6079 とくしま生協";
	item[2][81]="Z6080 日産グループ";
	item[2][82]="Z6081 NHK共済会";
	item[2][83]="Z6082 えらべる倶楽部";
	item[2][84]="Z6083 茨城県学校生協";
	item[2][85]="Z6084 福井県学校生協";
	item[2][86]="Z6085 LivLi CLUB（リブリクラブ）";
	item[2][87]="Z6086 CHINTAI";
	item[2][88]="Z6087 CLASSY LIFE";
	item[2][89]="Z6088 静岡県教職員生協";
	item[2][90]="Z6089 TS ONE";
	item[2][91]="Z6090 長崎県職員生活協同組合";
	item[2][92]="Z6091 安川電機グループ";
	item[2][93]="Z6092 明治安田生命保険相互会社";
	item[2][94]="Z6093 北海道学校生協";
	item[2][95]="Z6094 パレットクラウド";
	item[2][96]="Z6095 TEPCO";
	item[2][97]="Z6096 神奈川県医療福祉施設協同組合";
	item[2][98]="Z6097 FCクラブ・Fukurico";
	item[2][99]="Z6098 情報労連あいねっと倶楽部";
	item[2][100]="Z6099 ブリヂストングループ";
	
        /* Z70 */
	item[3] = new Array();
	item[3][0]="子ジャンルを選択して下さい";
	item[3][1]="Z7のすべて";
	item[3][2]="Z7001 NTT大阪支店";
	item[3][3]="Z7002 スターバックス";
	item[3][4]="Z7003 帝人";
	item[3][5]="Z7004 セガサミーグループ";
	item[3][6]="Z7005 NTTネオメイト";
	item[3][7]="Z7006 NEC(東日本大震災支援)";
	item[3][8]="Z7007 キャドバリージャパン";
	item[3][9]="Z7008 ファミリーマート";
	item[3][10]="Z7009 結核予防会";
	item[3][11]="Z7010 ナック（国境なき医師団）";
	item[3][12]="Z7011 アイエスエフネット";
	item[3][13]="Z7012 インテリジェンス";
	item[3][14]="Z7013 チャーティス・ファー・イースト・ホールティングス";
	item[3][15]="Z7014 民際センター";
	item[3][16]="Z7015 DNPグループ労連";
	item[3][17]="Z7016 しょうがっこうをおくる会";
	item[3][18]="Z7017 レオパレス21(企業ボランティア宅本便)";
	item[3][19]="Z7018 神奈川大学";
	item[3][20]="Z7019 ICAN（アイキャン）";
	item[3][21]="Z7020 阪急阪神ホール ディングス";
	item[3][22]="Z7021 ソニー";
	item[3][23]="Z7022 双日エアロスペース(地球環境基金)";
	item[3][24]="Z7023 売って支援プログラム（東日本大震災）";
	item[3][25]="Z7024 西友";
	item[3][26]="Z7025 日本経済新聞文化部(日本赤十字社 東京都支部)";
	item[3][27]="Z7026 朝日新聞社論説委員室";
	item[3][28]="Z7027 HFI（Hope and Faith International）";
	item[3][29]="Z7028 ACE（エース）";
	item[3][30]="Z7029 マンパワーグループ";
	item[3][31]="Z7030 中日本ハイウェイ・パトロール東京";
	item[3][32]="Z7031 三菱商事";
	item[3][33]="Z7032 地球の友と歩む会";
	item[3][34]="Z7033 損保ジャパン奈良支店";
	item[3][35]="Z7034 日本興亜損害保険";
	item[3][36]="Z7035 3keys（スリーキーズ）";
	item[3][37]="Z7036 ライフ";
	item[3][38]="Z7037 ★スポット対応企業";
	item[3][39]="Z7038 町田ゼルビア";
	item[3][40]="Z7039 SC相模原";
	item[3][41]="Z7040 幼い難民を考える会（CYR）";
	item[3][42]="Z7041 アフラック";
	item[3][43]="Z7042 JHP学校をつくる会";
	item[3][44]="Z7043 日立建機";
	item[3][45]="Z7044 北海道森と緑の会";
	item[3][46]="Z7045 ユニー";
	item[3][47]="Z7046 埼玉県自動車販売店協会（自販連埼玉支部）";
	item[3][48]="Z7047 マルハン";
	item[3][49]="Z7048 アドビシステムズ";
	item[3][50]="Z7049 FC在庫買取";
	item[3][51]="Z7050 大塚商会(3keys)";
	item[3][52]="Z7051 森ビル都市企画";
	item[3][53]="Z7052 アクセス";
	item[3][54]="Z7053 ブリヂストンスポーツ";
	item[3][55]="Z7054 ケア・インターナショナルジャパン";
	item[3][56]="Z7055 世界の子どもにワクチンを(JCV)";
	item[3][57]="Z7056 共立メンテナンス";
	item[3][58]="Z7057 ジャパンハート";
	item[3][59]="Z7058 MSD";
	item[3][60]="Z7059 ブリッジ エーシア ジャパン(BAJ)";
	item[3][61]="Z7060 TBS";
	item[3][62]="Z7061 TBS(フィリピン台風支援)";
	item[3][63]="Z7062 岡谷鋼機";
	item[3][64]="Z7063 横浜開港祭";
	item[3][65]="Z7064 商船三井";
	item[3][66]="Z7065 広栄化学工業";
	item[3][67]="Z7066 ハグオール";
	item[3][68]="Z7067 NTT労働組合 西日本";
	item[3][69]="Z7068 日本ラグビーフットボール協会";
	item[3][70]="Z7069 店舗在庫買取";
	item[3][71]="Z7070 神戸YMCA";
	item[3][72]="Z7071 北洋銀行";
	item[3][73]="Z7072 なおちゃんを救う会";
	item[3][74]="Z7073 大日本印刷株式会社　honto返品買取";
	item[3][75]="Z7074 シニアライフセラピー研究所";
	item[3][76]="Z7075 NTT労働組合　東日本本部";
	item[3][77]="Z7076 プリック ジャパン ビューティー";
	item[3][78]="Z7077 損害保険ジャパン日本興亜（東日本大震災支援）";
	item[3][79]="Z7078 損害保険ジャパン日本興亜（熊本地震支援）";
	item[3][80]="Z7079 ニッポン放送";
	item[3][81]="Z7080 NHK渋谷";
	item[3][82]="Z7081 NHK名古屋";
	item[3][83]="Z7082 三井不動産レジデンシャル（東日本大震災支援）";
	item[3][84]="Z7083 三井不動産レジデンシャル（熊本地震支援）";
	item[3][85]="Z7084 三菱地所コミュニティ（東日本大震災支援）";
	item[3][86]="Z7085 関西テレビ";
	item[3][87]="Z7086 かわさき市民活動センター";
	item[3][88]="Z7087 山田養蜂場";
	item[3][89]="Z7088 アリさんマークの引越社";
	item[3][90]="Z7089 NTTデータ（東日本大震災支援）";
	item[3][91]="Z7090 日本バルカー工業（東日本大震災支援）";
	item[3][92]="Z7091 グローセル（シャンティ）";
	item[3][93]="Z7092 住友生命札幌支社（シャンティ）";
	item[3][94]="Z7093 三井住友カード（東日本大震災支援）";
	item[3][95]="Z7094 日経印刷（東日本大震災支援）";
	item[3][96]="Z7095 アトミクス";
	item[3][97]="Z7096 LIC";
	item[3][98]="Z7097 JAIFA";
	item[3][99]="Z7098 SOMPOシステムズ（移動図書館支援）";
	item[3][100]="Z7099 日経印刷　長野（東日本大震災支援）";
	item[3][101]="Z7100 フロムヴイ";
	item[3][102]="Z7101 愛知県職員組合(移動図書館支援）";
	item[3][103]="Z7102 ベクトルフラックス";
	item[3][104]="Z7103 バリューブックス";
	item[3][105]="Z7104 東京海上グループ『未来塾』（熊本地震支援）";
	item[3][106]="Z7105 大阪商工信用金庫";
	item[3][107]="Z7106 三菱UFJニコス";
	item[3][108]="Z7107 アカツキ（BUY王）";
	item[3][109]="Z7108 アリアンツ火災海上保険（東日本大震災支援）";
	item[3][110]="Z7109 損害保険ジャパン日本興亜　関西総務部（東日本大震災支援）";
	item[3][111]="Z7110 ブックマーケティング";
	item[3][112]="Z7111 正蓮寺";
	item[3][113]="Z7112 オムロンフィールドエンジニアリング（熊本地震支援）";
	item[3][114]="Z7113 LU（ブリッジ エーシア ジャパン）";
	item[3][115]="Z7114 サスケコーポレーション";
	item[3][116]="Z7115 ABC(Room to Read)";
	item[3][117]="Z7116 楽天";
	item[3][118]="Z7117 神奈川県医療福祉施設協同組合";
	item[3][119]="Z7118 日本通運【ボランティア】（かながわキンタロウ）";
	item[3][120]="Z7119 ★スポット対応企業（東日本支援10％UP）";
	item[3][121]="Z7120 ★スポット対応企業（熊本支援10％UP）";
	item[3][122]="Z7121 ジェーシービー(Room to Read)";
	item[3][123]="Z7122 新宿南エネルギーサービス(地球環境基金)";
	item[3][124]="Z7123 J-WAVE(地球環境基金)";
	item[3][125]="Z7124 西日本高速道路サービス四国";
	item[3][126]="Z7125 セディナ(3keys)";
	item[3][127]="Z7126 三井不動産レジデンシャル（平成30年7月豪雨災害）";
	item[3][128]="Z7127 日本経済新聞　生活情報部（平成30年7月豪雨災害）";
	item[3][129]="Z7128 損害保険ジャパン日本興亜（平成30年7月豪雨災害支援）";
	item[3][130]="Z7130 西日本高速道路サービス四国(売って支援10％UP)";
	item[3][131]="Z7129 こども食堂支援機構";
	item[3][132]="Z7131 座間市社会福祉協議会";
	
	// Z80**
	item[4] = new Array();
	item[4][0]="子ジャンルを選択して下さい";
	item[4][1]="Z8すべて(営業推進T分)";
	item[4][2]="Z8605 アリさんマークの引越社";
	item[4][3]="Z8606 JAF";
	item[4][4]="Z8691 アート引越センター";
	item[4][5]="Z8692 大東建託パートナーズ";
	item[4][6]="Z8693 暮らしのサポート";
	item[4][7]="Z8694 CLUB SPASS";
	item[4][8]="Z8695 ソフトバンクグループ";
	item[4][9]="Z8696 タイムズクラブ";
	item[4][10]="Z8697 バイクブロス";
	item[4][11]="Z8698 LIFULL引越し";
	item[4][12]="Z8699 auコレトク";
	item[4][13]="Z8700 ARUHI暮らしのサービス";
	item[4][14]="Z8701 FUKUYA";
	item[4][15]="Z8702 ベルスファミリークラブ";
	item[4][16]="Z8703 日産グループ";
	item[4][17]="Z8704 NHK共済会";
	item[4][18]="Z8705 えらべる倶楽部";
	item[4][19]="Z8706 TS ONE";
	item[4][20]="Z8707 明治安田生命保険相互会社";
	item[4][21]="Z8708 ハイホー";
	item[4][22]="Z8672 連合三田会";
	item[4][23]="Z8826 情報労連あいねっと倶楽部";
	item[4][24]="Z8886 セゾン・ＵＣカード（クレディセゾン）";
	item[4][25]="Z8887 フラット35（クレディセゾン）";
	item[4][26]="Z8888 五大トラスト";
	item[4][27]="Z8892 リビンマッチ";
	item[4][28]="Z8839 アリさんマークの引越社";
	item[4][29]="Z8840 JAF";
	item[4][30]="Z8916 株式会社ＳＡＫＵＹＡ";
	item[4][31]="Z8898 全国教職員互助団体協議会";
	item[4][32]="Z8956 トレファク引越";
	item[4][33]="Z8939 アート引越センター";
	item[4][34]="Z8940 大東建託パートナーズ";
	item[4][35]="Z8941 暮らしのサポート";
	item[4][36]="Z8942 ＣＬＵＢ　ＳＰＡＳＳ";
	item[4][37]="Z8943 ソフトバンクグループ";
	item[4][38]="Z8944 タイムズクラブ";
	item[4][39]="Z8945 バイクブロス";
	item[4][40]="Z8946 LIFULL引越し";
	item[4][41]="Z8947 auコレトク";
	item[4][42]="Z8948 ARUHI暮らしのサービス";
	item[4][43]="Z8949 FUKUYA";
	item[4][44]="Z8950 ベルスファミリークラブ";
	item[4][45]="Z8951 日産グループ";
	item[4][46]="Z8952 NHK共済会";
	item[4][47]="Z8953 えらべる倶楽部";
	item[4][48]="Z8954 明治安田生命保険相互会社";
	item[4][49]="Z8955 ハイホー";
	item[4][50]="Z8899 BigAdvance";
	item[4][51]="Z8958 BitCash";
	item[4][52]="Z8860 ブリヂストングループ";
	
	// ZP***
	item[5] = new Array();
	item[5][0]="子ジャンルを選択して下さい";
	item[5][1]="ZPのすべて";
	item[5][2]="ZP000 Bcan会員";
	item[5][3]="ZP001 Bcan会員（防衛省）";
	item[5][4]="ZP002 Bcan会員（クレハグループ）";
	item[5][5]="ZP003 パナソニックグループ労働組合連合会";
	item[5][6]="ZP004 すまい―だPLUS";
	item[5][7]="ZP006 全国年金受給者団体連合会";
	item[5][8]="ZP005 野村不動産プライムアーバン新宿夏目坂TR";
	item[5][9]="ZP007 イッツコム";
	item[5][10]="ZP008 アニヴェルセル";
	item[5][11]="ZP00A 野村不動産プラウドフラット神楽坂�U";
	item[5][12]="ZP009 京都生協";
	item[5][13]="ZP00B 東急住宅リース";
	item[5][14]="ZP00D 情報労連あいねっと倶楽部";
	item[5][15]="ZP00C ＯＫＷＡＶＥ感謝経済";
	item[5][16]="ZP00E アリさんマークの引越社";
	item[5][17]="ZP00F JAF";
	item[5][18]="ZP00G ブリヂストングループ";
	item[5][19]="ZP00H セゾン・ＵＣカード（クレディセゾン）";
	item[5][20]="ZP00J フラット35（クレディセゾン）";
	item[5][21]="ZP00K 五大トラスト";
	item[5][22]="ZP00L 三井のすまいLOOP（ループ）";
	item[5][23]="ZP00M 東芝グループ従業員･OBご家族";
	item[5][24]="ZP00N リビンマッチ";
	item[5][25]="ZP00P 全国教職員互助団体協議会";
	item[5][26]="ZP00Q BigAdvance";
	item[5][27]="ZP00R 株式会社ＳＡＫＵＹＡ";
	item[5][28]="ZP00S アート引越センター";
	item[5][29]="ZP00T 大東建託パートナーズ";
	item[5][30]="ZP00U 暮らしのサポート";
	item[5][31]="ZP00V ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）";
	item[5][32]="ZP00W ソフトバンクグループ";
	item[5][33]="ZP00X タイムズクラブ";
	item[5][34]="ZP00Y LIFULL引越し";
	item[5][35]="ZP00Z ARUHI暮らしのサービス";
	item[5][36]="ZP010 FUKUYA";
	item[5][37]="ZP011 ベルスファミリークラブ";
	item[5][38]="ZP012 日産グループ";
	item[5][39]="ZP013 NHK共済会";
	item[5][40]="ZP014 えらべる倶楽部";
	item[5][41]="ZP015 明治安田生命保険相互会社";
	item[5][42]="ZP016 ハイホー";
	
var sqlitem = new Array();
sqlitem[1] = new Array();
sqlitem[2] = new Array();
sqlitem[3] = new Array();
sqlitem[4] = new Array();
sqlitem[5] = new Array();

	sqlitem[1][1]= "(SUBSTR(T.SELL_NO,1,2)='Z1')";
	sqlitem[1][2]= "(SUBSTR(T.SELL_NO,1,5)='Z1001')";
	sqlitem[1][3]= "(SUBSTR(T.SELL_NO,1,5)='Z1002' or SUBSTR(T.SELL_NO,1,5)='Z1006')";
	sqlitem[1][4]= "(SUBSTR(T.SELL_NO,1,5)='Z1003')";
	sqlitem[1][5]= "(SUBSTR(T.SELL_NO,1,5)='Z1004')";
	sqlitem[1][6]= "(SUBSTR(T.SELL_NO,1,5)='Z1005')";
	sqlitem[1][7]= "(SUBSTR(T.SELL_NO,1,5)='Z1007')";
	sqlitem[1][8]= "(SUBSTR(T.SELL_NO,1,5)='Z1029')";
	sqlitem[1][9]= "(SUBSTR(T.SELL_NO,1,5)='Z1030')";
	sqlitem[1][10]= "(SUBSTR(T.SELL_NO,1,5)='Z1031')";
	sqlitem[1][11]= "(SUBSTR(T.SELL_NO,1,5)='Z1032')";
	sqlitem[1][12]= "(SUBSTR(T.SELL_NO,1,5)='Z1033')";
	sqlitem[1][13]= "(SUBSTR(T.SELL_NO,1,5)='Z1034')";
    /* 2016/02/12 add */
	sqlitem[1][14]= "(SUBSTR(T.SELL_NO,1,3)='Z11')";
	sqlitem[1][15]= "(SUBSTR(T.SELL_NO,1,5)='Z1100')";
	sqlitem[1][16]= "(SUBSTR(T.SELL_NO,1,5)='Z1111')";
	sqlitem[1][17]= "(SUBSTR(T.SELL_NO,1,5)='Z1112')";
	sqlitem[1][18]= "(SUBSTR(T.SELL_NO,1,5)='Z1113')";
	sqlitem[1][19]= "(SUBSTR(T.SELL_NO,1,5)='Z1114')";
	sqlitem[1][20]= "(SUBSTR(T.SELL_NO,1,5)='Z1035')";
	sqlitem[1][21]= "(SUBSTR(T.SELL_NO,1,5)='Z1036')";
	sqlitem[1][22]= "(SUBSTR(T.SELL_NO,1,5)='Z1037')";
	sqlitem[1][23]= "(SUBSTR(T.SELL_NO,1,5)='Z1038')";
	sqlitem[1][24]= "(SUBSTR(T.SELL_NO,1,5)='Z1039')";
	sqlitem[1][25]= "(SUBSTR(T.SELL_NO,1,5)='Z1040')";
	sqlitem[1][26]= "(SUBSTR(T.SELL_NO,1,5)='Z1041')";
	sqlitem[1][27]= "(SUBSTR(T.SELL_NO,1,5)='Z1042')";
	sqlitem[1][28]= "(SUBSTR(T.SELL_NO,1,5)='Z1043')";
	sqlitem[1][29]= "(SUBSTR(T.SELL_NO,1,5)='Z1044')";
	sqlitem[1][30]= "(SUBSTR(T.SELL_NO,1,5)='Z1045')";
	sqlitem[1][31]= "(SUBSTR(T.SELL_NO,1,5)='Z1046')";
	sqlitem[1][32]= "(SUBSTR(T.SELL_NO,1,5)='Z1047')";
	sqlitem[1][33]= "(SUBSTR(T.SELL_NO,1,5)='Z1048')";
	sqlitem[1][34]= "(SUBSTR(T.SELL_NO,1,5)='Z1049')";
	sqlitem[1][35]= "(SUBSTR(T.SELL_NO,1,5)='Z1050')";
	sqlitem[1][36]= "(SUBSTR(T.SELL_NO,1,5)='Z1051')";
	sqlitem[1][37]= "(SUBSTR(T.SELL_NO,1,5)='Z1052')";
	sqlitem[1][38]= "(SUBSTR(T.SELL_NO,1,5)='Z1053')";
	sqlitem[1][39]= "(SUBSTR(T.SELL_NO,1,5)='Z1054')";
	sqlitem[1][40]= "(SUBSTR(T.SELL_NO,1,5)='Z1055')";
	sqlitem[1][41]= "(SUBSTR(T.SELL_NO,1,5)='Z1056')";
	sqlitem[1][42]= "(SUBSTR(T.SELL_NO,1,5)='Z1057')";
	sqlitem[1][43]= "(SUBSTR(T.SELL_NO,1,5)='Z1058')";
	sqlitem[1][44]= "(SUBSTR(T.SELL_NO,1,5)='Z1059')";
	sqlitem[1][45]= "(SUBSTR(T.SELL_NO,1,5)='Z1123')";
	sqlitem[1][46]= "(SUBSTR(T.SELL_NO,1,5)='Z1060')";
	sqlitem[1][47]= "(SUBSTR(T.SELL_NO,1,5)='Z1061')";
	sqlitem[1][48]= "(SUBSTR(T.SELL_NO,1,5)='Z1127')";
	sqlitem[1][49]= "(SUBSTR(T.SELL_NO,1,5)='Z1063')";
	sqlitem[1][50]= "(SUBSTR(T.SELL_NO,1,5)='Z1064')";
	sqlitem[1][51]= "(SUBSTR(T.SELL_NO,1,5)='Z1062')";
	sqlitem[1][52]= "(SUBSTR(T.SELL_NO,1,5)='Z1101')";
	sqlitem[1][53]= "(SUBSTR(T.SELL_NO,1,5)='Z1102')";
	sqlitem[1][54]= "(SUBSTR(T.SELL_NO,1,5)='Z1103')";
	sqlitem[1][55]= "(SUBSTR(T.SELL_NO,1,5)='Z1104')";
	sqlitem[1][56]= "(SUBSTR(T.SELL_NO,1,5)='Z1105')";
	sqlitem[1][57]= "(SUBSTR(T.SELL_NO,1,5)='Z1106')";
	sqlitem[1][58]= "(SUBSTR(T.SELL_NO,1,5)='Z1107')";
	sqlitem[1][59]= "(SUBSTR(T.SELL_NO,1,5)='Z1108')";
	sqlitem[1][60]= "(SUBSTR(T.SELL_NO,1,5)='Z1109')";
	sqlitem[1][61]= "(SUBSTR(T.SELL_NO,1,5)='Z1110')";
	sqlitem[1][62]= "(SUBSTR(T.SELL_NO,1,5)='Z1115')";
	sqlitem[1][63]= "(SUBSTR(T.SELL_NO,1,5)='Z1116')";
	sqlitem[1][64]= "(SUBSTR(T.SELL_NO,1,5)='Z1117')";
	sqlitem[1][65]= "(SUBSTR(T.SELL_NO,1,5)='Z1118')";
	sqlitem[1][66]= "(SUBSTR(T.SELL_NO,1,5)='Z1119')";
	sqlitem[1][67]= "(SUBSTR(T.SELL_NO,1,5)='Z1120')";
	sqlitem[1][68]= "(SUBSTR(T.SELL_NO,1,5)='Z1121')";
	sqlitem[1][69]= "(SUBSTR(T.SELL_NO,1,5)='Z1122')";
	sqlitem[1][70]= "(SUBSTR(T.SELL_NO,1,5)='Z1124')";
	sqlitem[1][71]= "(SUBSTR(T.SELL_NO,1,5)='Z1125')";
	sqlitem[1][72]= "(SUBSTR(T.SELL_NO,1,5)='Z1126')";
	sqlitem[1][73]= "(SUBSTR(T.SELL_NO,1,5)='Z1128')";
	sqlitem[1][74]= "(SUBSTR(T.SELL_NO,1,5)='Z1129')";
	sqlitem[1][75]= "(SUBSTR(T.SELL_NO,1,5)='Z1130')";
	sqlitem[1][76]= "(SUBSTR(T.SELL_NO,1,5)='Z1131')";
	sqlitem[1][77]= "(SUBSTR(T.SELL_NO,1,5)='Z1132')";
	sqlitem[1][78]= "(SUBSTR(T.SELL_NO,1,5)='Z1133')";
	sqlitem[1][79]= "(SUBSTR(T.SELL_NO,1,5)='Z1134')";
	sqlitem[1][80]= "(SUBSTR(T.SELL_NO,1,5)='Z1135')";
	sqlitem[1][81]= "(SUBSTR(T.SELL_NO,1,5)='Z1136')";
	sqlitem[1][82]= "(SUBSTR(T.SELL_NO,1,5)='Z1137')";
	sqlitem[1][83]= "(SUBSTR(T.SELL_NO,1,5)='Z1138')";
	sqlitem[1][84]= "(SUBSTR(T.SELL_NO,1,5)='Z1139')";
	sqlitem[1][85]= "(SUBSTR(T.SELL_NO,1,5)='Z1140')";
	sqlitem[1][86]= "(SUBSTR(T.SELL_NO,1,5)='Z1141')";
	sqlitem[1][87]= "(SUBSTR(T.SELL_NO,1,5)='Z1142')";
	sqlitem[1][88]= "(SUBSTR(T.SELL_NO,1,5)='Z1143')";
	sqlitem[1][89]= "(SUBSTR(T.SELL_NO,1,5)='Z1144')";
	sqlitem[1][90]= "(SUBSTR(T.SELL_NO,1,5)='Z1145')";
	sqlitem[1][91]= "(SUBSTR(T.SELL_NO,1,5)='Z1146')";
	sqlitem[1][92]= "(SUBSTR(T.SELL_NO,1,5)='Z1066')";
	sqlitem[1][93]= "(SUBSTR(T.SELL_NO,1,5)='Z1065')";
	sqlitem[1][94]= "(SUBSTR(T.SELL_NO,1,5)='Z1067')";
	sqlitem[1][95]= "(SUBSTR(T.SELL_NO,1,5)='Z1068') ";
	sqlitem[1][96]= "(SUBSTR(T.SELL_NO,1,5)='Z1069') ";
	sqlitem[1][97]= "(SUBSTR(T.SELL_NO,1,5)='Z1070') ";

	sqlitem[2][1]= "SUBSTR(T.SELL_NO,1,2)='Z6' ";
	sqlitem[2][2]= "SUBSTR(T.SELL_NO,1,5)='Z6001' ";
	sqlitem[2][3]= "SUBSTR(T.SELL_NO,1,5)='Z6002' ";
	sqlitem[2][4]= "SUBSTR(T.SELL_NO,1,5)='Z6003' ";
	sqlitem[2][5]= "SUBSTR(T.SELL_NO,1,5)='Z6004' ";
	sqlitem[2][6]= "SUBSTR(T.SELL_NO,1,5)='Z6005' ";
	sqlitem[2][7]= "SUBSTR(T.SELL_NO,1,5)='Z6006' ";
	sqlitem[2][8]= "SUBSTR(T.SELL_NO,1,5)='Z6007' ";
	sqlitem[2][9]= "SUBSTR(T.SELL_NO,1,5)='Z6008' ";
	sqlitem[2][10]= "SUBSTR(T.SELL_NO,1,5)='Z6009' ";
	sqlitem[2][11]= "SUBSTR(T.SELL_NO,1,5)='Z6010' ";
	sqlitem[2][12]= "SUBSTR(T.SELL_NO,1,5)='Z6011' ";
	sqlitem[2][13]= "SUBSTR(T.SELL_NO,1,5)='Z6012' ";
	sqlitem[2][14]= "SUBSTR(T.SELL_NO,1,5)='Z6013' ";
	sqlitem[2][15]= "SUBSTR(T.SELL_NO,1,5)='Z6014' ";
	sqlitem[2][16]= "SUBSTR(T.SELL_NO,1,5)='Z6015' ";
	sqlitem[2][17]= "SUBSTR(T.SELL_NO,1,5)='Z6016' ";
	sqlitem[2][18]= "SUBSTR(T.SELL_NO,1,5)='Z6017' ";
	sqlitem[2][19]= "SUBSTR(T.SELL_NO,1,5)='Z6018' ";
	sqlitem[2][20]= "SUBSTR(T.SELL_NO,1,5)='Z6019' ";
	sqlitem[2][21]= "SUBSTR(T.SELL_NO,1,5)='Z6020' ";
	sqlitem[2][22]= "SUBSTR(T.SELL_NO,1,5)='Z6021' ";
	sqlitem[2][23]= "SUBSTR(T.SELL_NO,1,5)='Z6022' ";
	sqlitem[2][24]= "SUBSTR(T.SELL_NO,1,5)='Z6023' ";
	sqlitem[2][25]= "SUBSTR(T.SELL_NO,1,5)='Z6024' ";
	sqlitem[2][26]= "SUBSTR(T.SELL_NO,1,5)='Z6025' ";
	sqlitem[2][27]= "SUBSTR(T.SELL_NO,1,5)='Z6026' ";
	sqlitem[2][28]= "SUBSTR(T.SELL_NO,1,5)='Z6027' ";
	sqlitem[2][29]= "SUBSTR(T.SELL_NO,1,5)='Z6028' ";
	sqlitem[2][30]= "SUBSTR(T.SELL_NO,1,5)='Z6029' ";
	sqlitem[2][31]= "SUBSTR(T.SELL_NO,1,5)='Z6030' ";
	sqlitem[2][32]= "SUBSTR(T.SELL_NO,1,5)='Z6031' ";
	sqlitem[2][33]= "SUBSTR(T.SELL_NO,1,5)='Z6032' ";
	sqlitem[2][34]= "SUBSTR(T.SELL_NO,1,5)='Z6033' ";
	sqlitem[2][35]= "SUBSTR(T.SELL_NO,1,5)='Z6034' ";
	sqlitem[2][36]= "SUBSTR(T.SELL_NO,1,5)='Z6035' ";
	sqlitem[2][37]= "SUBSTR(T.SELL_NO,1,5)='Z6036' ";
	sqlitem[2][38]= "SUBSTR(T.SELL_NO,1,5)='Z6037' ";
	sqlitem[2][39]= "SUBSTR(T.SELL_NO,1,5)='Z6038' ";
	sqlitem[2][40]= "SUBSTR(T.SELL_NO,1,5)='Z6039' ";
	sqlitem[2][41]= "SUBSTR(T.SELL_NO,1,5)='Z6040' ";
	sqlitem[2][42]= "SUBSTR(T.SELL_NO,1,5)='Z6041' ";
	sqlitem[2][43]= "SUBSTR(T.SELL_NO,1,5)='Z6042' ";
	sqlitem[2][44]= "SUBSTR(T.SELL_NO,1,5)='Z6043' ";
	sqlitem[2][45]= "SUBSTR(T.SELL_NO,1,5)='Z6044' ";
	sqlitem[2][46]= "SUBSTR(T.SELL_NO,1,5)='Z6045' ";
	sqlitem[2][47]= "SUBSTR(T.SELL_NO,1,5)='Z6046' ";
	sqlitem[2][48]= "SUBSTR(T.SELL_NO,1,5)='Z6047' ";
	sqlitem[2][49]= "SUBSTR(T.SELL_NO,1,5)='Z6048' ";
	sqlitem[2][50]= "SUBSTR(T.SELL_NO,1,5)='Z6049' ";
	sqlitem[2][51]= "SUBSTR(T.SELL_NO,1,5)='Z6050' ";
	sqlitem[2][52]= "SUBSTR(T.SELL_NO,1,5)='Z6051' ";
	sqlitem[2][53]= "SUBSTR(T.SELL_NO,1,5)='Z6052' ";
	sqlitem[2][54]= "SUBSTR(T.SELL_NO,1,5)='Z6053' ";
	sqlitem[2][55]= "SUBSTR(T.SELL_NO,1,5)='Z6054' ";
	sqlitem[2][56]= "SUBSTR(T.SELL_NO,1,5)='Z6055' ";
	sqlitem[2][57]= "SUBSTR(T.SELL_NO,1,5)='Z6056' ";
	sqlitem[2][58]= "SUBSTR(T.SELL_NO,1,5)='Z6057' ";
	sqlitem[2][59]= "SUBSTR(T.SELL_NO,1,5)='Z6058' ";
	sqlitem[2][60]= "SUBSTR(T.SELL_NO,1,5)='Z6059' ";
	sqlitem[2][61]= "SUBSTR(T.SELL_NO,1,5)='Z6060' ";
	sqlitem[2][62]= "SUBSTR(T.SELL_NO,1,5)='Z6061' ";
	sqlitem[2][63]= "SUBSTR(T.SELL_NO,1,5)='Z6062' ";
	sqlitem[2][64]= "SUBSTR(T.SELL_NO,1,5)='Z6063' ";
	sqlitem[2][65]= "SUBSTR(T.SELL_NO,1,5)='Z6064' ";
	sqlitem[2][66]= "SUBSTR(T.SELL_NO,1,5)='Z6065' ";
	sqlitem[2][67]= "SUBSTR(T.SELL_NO,1,5)='Z6066' ";
	sqlitem[2][68]= "SUBSTR(T.SELL_NO,1,5)='Z6067' ";
	sqlitem[2][69]= "SUBSTR(T.SELL_NO,1,5)='Z6068' ";
	sqlitem[2][70]= "SUBSTR(T.SELL_NO,1,5)='Z6069' ";
	sqlitem[2][71]= "SUBSTR(T.SELL_NO,1,5)='Z6070' ";
	sqlitem[2][72]= "SUBSTR(T.SELL_NO,1,5)='Z6071' ";
	sqlitem[2][73]= "SUBSTR(T.SELL_NO,1,5)='Z6072' ";
	sqlitem[2][74]= "SUBSTR(T.SELL_NO,1,5)='Z6073' ";
	sqlitem[2][75]= "SUBSTR(T.SELL_NO,1,5)='Z6074' ";
	sqlitem[2][76]= "SUBSTR(T.SELL_NO,1,5)='Z6075' ";
	sqlitem[2][77]= "SUBSTR(T.SELL_NO,1,5)='Z6076' ";
	sqlitem[2][78]= "SUBSTR(T.SELL_NO,1,5)='Z6077' ";
	sqlitem[2][79]= "SUBSTR(T.SELL_NO,1,5)='Z6078' ";
	sqlitem[2][80]= "SUBSTR(T.SELL_NO,1,5)='Z6079' ";
	sqlitem[2][81]= "SUBSTR(T.SELL_NO,1,5)='Z6080' ";
	sqlitem[2][82]= "SUBSTR(T.SELL_NO,1,5)='Z6081' ";
	sqlitem[2][83]= "SUBSTR(T.SELL_NO,1,5)='Z6082' ";
	sqlitem[2][84]= "SUBSTR(T.SELL_NO,1,5)='Z6083' ";
	sqlitem[2][85]= "SUBSTR(T.SELL_NO,1,5)='Z6084' ";
	sqlitem[2][86]= "SUBSTR(T.SELL_NO,1,5)='Z6085' ";
	sqlitem[2][87]= "SUBSTR(T.SELL_NO,1,5)='Z6086' ";
	sqlitem[2][88]= "SUBSTR(T.SELL_NO,1,5)='Z6087' ";
	sqlitem[2][89]= "SUBSTR(T.SELL_NO,1,5)='Z6088' ";
	sqlitem[2][90]= "SUBSTR(T.SELL_NO,1,5)='Z6089' ";
	sqlitem[2][91]= "SUBSTR(T.SELL_NO,1,5)='Z6090' ";
	sqlitem[2][92]= "SUBSTR(T.SELL_NO,1,5)='Z6091' ";
	sqlitem[2][93]= "SUBSTR(T.SELL_NO,1,5)='Z6092' ";
	sqlitem[2][94]= "SUBSTR(T.SELL_NO,1,5)='Z6093' ";
	sqlitem[2][95]= "SUBSTR(T.SELL_NO,1,5)='Z6094' ";
	sqlitem[2][96]= "SUBSTR(T.SELL_NO,1,5)='Z6095' ";
	sqlitem[2][97]= "SUBSTR(T.SELL_NO,1,5)='Z6096' ";
	sqlitem[2][98]= "SUBSTR(T.SELL_NO,1,5)='Z6097' ";
	sqlitem[2][99]= "SUBSTR(T.SELL_NO,1,5)='Z6098' ";
	sqlitem[2][100]= "SUBSTR(T.SELL_NO,1,5)='Z6099' ";

	sqlitem[3][1]= "SUBSTR(T.SELL_NO,1,2)='Z7' ";
	sqlitem[3][2]= "(SUBSTR(T.SELL_NO,1,3)='Z16' or SUBSTR(T.SELL_NO,1,5)='Z7001')";
	sqlitem[3][3]= "(SUBSTR(T.SELL_NO,1,3)='Z17' or SUBSTR(T.SELL_NO,1,5)='Z7002')";
	sqlitem[3][4]= "(SUBSTR(T.SELL_NO,1,3)='Z18' or SUBSTR(T.SELL_NO,1,5)='Z7003')";
	sqlitem[3][5]= "SUBSTR(T.SELL_NO,1,5)='Z7004' ";
	sqlitem[3][6]= "SUBSTR(T.SELL_NO,1,5)='Z7005' ";
	sqlitem[3][7]= "SUBSTR(T.SELL_NO,1,5)='Z7006' ";
	sqlitem[3][8]= "SUBSTR(T.SELL_NO,1,5)='Z7007' ";
	sqlitem[3][9]= "SUBSTR(T.SELL_NO,1,5)='Z7008' ";
	sqlitem[3][10]= "SUBSTR(T.SELL_NO,1,5)='Z7009' ";
	sqlitem[3][11]= "SUBSTR(T.SELL_NO,1,5)='Z7010' ";
	sqlitem[3][12]= "SUBSTR(T.SELL_NO,1,5)='Z7011' ";
	sqlitem[3][13]= "SUBSTR(T.SELL_NO,1,5)='Z7012' ";
	sqlitem[3][14]= "SUBSTR(T.SELL_NO,1,5)='Z7013' ";
	sqlitem[3][15]= "SUBSTR(T.SELL_NO,1,5)='Z7014' ";
	sqlitem[3][16]= "SUBSTR(T.SELL_NO,1,5)='Z7015' ";
	sqlitem[3][17]= "SUBSTR(T.SELL_NO,1,5)='Z7016' ";
	sqlitem[3][18]= "SUBSTR(T.SELL_NO,1,5)='Z7017' ";
	sqlitem[3][19]= "SUBSTR(T.SELL_NO,1,5)='Z7018' ";
	sqlitem[3][20]= "SUBSTR(T.SELL_NO,1,5)='Z7019' ";
	sqlitem[3][21]= "SUBSTR(T.SELL_NO,1,5)='Z7020' ";
	sqlitem[3][22]= "SUBSTR(T.SELL_NO,1,5)='Z7021' ";
	sqlitem[3][23]= "SUBSTR(T.SELL_NO,1,5)='Z7022' ";
	sqlitem[3][24]= "SUBSTR(T.SELL_NO,1,5)='Z7023' ";
	sqlitem[3][25]= "SUBSTR(T.SELL_NO,1,5)='Z7024' ";
	sqlitem[3][26]= "SUBSTR(T.SELL_NO,1,5)='Z7025' ";
	sqlitem[3][27]= "SUBSTR(T.SELL_NO,1,5)='Z7026' ";
	sqlitem[3][28]= "SUBSTR(T.SELL_NO,1,5)='Z7027' ";
	sqlitem[3][29]= "SUBSTR(T.SELL_NO,1,5)='Z7028' ";
	sqlitem[3][30]= "SUBSTR(T.SELL_NO,1,5)='Z7029' ";
	sqlitem[3][31]= "SUBSTR(T.SELL_NO,1,5)='Z7030' ";
	sqlitem[3][32]= "SUBSTR(T.SELL_NO,1,5)='Z7031' ";
	sqlitem[3][33]= "SUBSTR(T.SELL_NO,1,5)='Z7032' ";
	sqlitem[3][34]= "SUBSTR(T.SELL_NO,1,5)='Z7033' ";
	sqlitem[3][35]= "SUBSTR(T.SELL_NO,1,5)='Z7034' ";
	sqlitem[3][36]= "SUBSTR(T.SELL_NO,1,5)='Z7035' ";
	sqlitem[3][37]= "SUBSTR(T.SELL_NO,1,5)='Z7036' ";
	sqlitem[3][38]= "SUBSTR(T.SELL_NO,1,5)='Z7037' ";
	sqlitem[3][39]= "SUBSTR(T.SELL_NO,1,5)='Z7038' ";
	sqlitem[3][40]= "SUBSTR(T.SELL_NO,1,5)='Z7039' ";
	sqlitem[3][41]= "SUBSTR(T.SELL_NO,1,5)='Z7040' ";
	sqlitem[3][42]= "SUBSTR(T.SELL_NO,1,5)='Z7041' ";
	sqlitem[3][43]= "SUBSTR(T.SELL_NO,1,5)='Z7042' ";
	sqlitem[3][44]= "SUBSTR(T.SELL_NO,1,5)='Z7043' ";
	sqlitem[3][45]= "SUBSTR(T.SELL_NO,1,5)='Z7044' ";
	sqlitem[3][46]= "SUBSTR(T.SELL_NO,1,5)='Z7045' ";
	sqlitem[3][47]= "SUBSTR(T.SELL_NO,1,5)='Z7046' ";
	sqlitem[3][48]= "SUBSTR(T.SELL_NO,1,5)='Z7047' ";
	sqlitem[3][49]= "SUBSTR(T.SELL_NO,1,5)='Z7048' ";
	sqlitem[3][50]= "SUBSTR(T.SELL_NO,1,5)='Z7049' ";
	sqlitem[3][51]= "SUBSTR(T.SELL_NO,1,5)='Z7050' ";
	sqlitem[3][52]= "SUBSTR(T.SELL_NO,1,5)='Z7051' ";
	sqlitem[3][53]= "SUBSTR(T.SELL_NO,1,5)='Z7052' ";
	sqlitem[3][54]= "SUBSTR(T.SELL_NO,1,5)='Z7053' ";
	sqlitem[3][55]= "SUBSTR(T.SELL_NO,1,5)='Z7054' ";
	sqlitem[3][56]= "SUBSTR(T.SELL_NO,1,5)='Z7055' ";
	sqlitem[3][57]= "SUBSTR(T.SELL_NO,1,5)='Z7056' ";
	sqlitem[3][58]= "SUBSTR(T.SELL_NO,1,5)='Z7057' ";
	sqlitem[3][59]= "SUBSTR(T.SELL_NO,1,5)='Z7058' ";
	sqlitem[3][60]= "SUBSTR(T.SELL_NO,1,5)='Z7059' ";
	sqlitem[3][61]= "SUBSTR(T.SELL_NO,1,5)='Z7060' ";
	sqlitem[3][62]= "SUBSTR(T.SELL_NO,1,5)='Z7061' ";
	sqlitem[3][63]= "SUBSTR(T.SELL_NO,1,5)='Z7062' ";
	sqlitem[3][64]= "SUBSTR(T.SELL_NO,1,5)='Z7063' ";
	sqlitem[3][65]= "SUBSTR(T.SELL_NO,1,5)='Z7064' ";
	sqlitem[3][66]= "SUBSTR(T.SELL_NO,1,5)='Z7065' ";
	sqlitem[3][67]= "SUBSTR(T.SELL_NO,1,5)='Z7066' ";
	sqlitem[3][68]= "SUBSTR(T.SELL_NO,1,5)='Z7067' ";
	sqlitem[3][69]= "SUBSTR(T.SELL_NO,1,5)='Z7068' ";
	sqlitem[3][70]= "SUBSTR(T.SELL_NO,1,5)='Z7069' ";
	sqlitem[3][71]= "SUBSTR(T.SELL_NO,1,5)='Z7070' ";
	sqlitem[3][72]= "SUBSTR(T.SELL_NO,1,5)='Z7071' ";
	sqlitem[3][73]= "SUBSTR(T.SELL_NO,1,5)='Z7072' ";
	sqlitem[3][74]= "SUBSTR(T.SELL_NO,1,5)='Z7073' ";
	sqlitem[3][75]= "SUBSTR(T.SELL_NO,1,5)='Z7074' ";
	sqlitem[3][76]= "SUBSTR(T.SELL_NO,1,5)='Z7075' ";
	sqlitem[3][77]= "SUBSTR(T.SELL_NO,1,5)='Z7076' ";
	sqlitem[3][78]= "SUBSTR(T.SELL_NO,1,5)='Z7077' ";
	sqlitem[3][79]= "SUBSTR(T.SELL_NO,1,5)='Z7078' ";
	sqlitem[3][80]= "SUBSTR(T.SELL_NO,1,5)='Z7079' ";
	sqlitem[3][81]= "SUBSTR(T.SELL_NO,1,5)='Z7080' ";
	sqlitem[3][82]= "SUBSTR(T.SELL_NO,1,5)='Z7081' ";
	sqlitem[3][83]= "SUBSTR(T.SELL_NO,1,5)='Z7082' ";
	sqlitem[3][84]= "SUBSTR(T.SELL_NO,1,5)='Z7083' ";
	sqlitem[3][85]= "SUBSTR(T.SELL_NO,1,5)='Z7084' ";
	sqlitem[3][86]= "SUBSTR(T.SELL_NO,1,5)='Z7085' ";
	sqlitem[3][87]= "SUBSTR(T.SELL_NO,1,5)='Z7086' ";
	sqlitem[3][88]= "SUBSTR(T.SELL_NO,1,5)='Z7087' ";
	sqlitem[3][89]= "SUBSTR(T.SELL_NO,1,5)='Z7088' ";
	sqlitem[3][90]= "SUBSTR(T.SELL_NO,1,5)='Z7089' ";
	sqlitem[3][91]= "SUBSTR(T.SELL_NO,1,5)='Z7090' ";
	sqlitem[3][92]= "SUBSTR(T.SELL_NO,1,5)='Z7091' ";
	sqlitem[3][93]= "SUBSTR(T.SELL_NO,1,5)='Z7092' ";
	sqlitem[3][94]= "SUBSTR(T.SELL_NO,1,5)='Z7093' ";
	sqlitem[3][95]= "SUBSTR(T.SELL_NO,1,5)='Z7094' ";
	sqlitem[3][96]= "SUBSTR(T.SELL_NO,1,5)='Z7095' ";
	sqlitem[3][97]= "SUBSTR(T.SELL_NO,1,5)='Z7096' ";
	sqlitem[3][98]= "SUBSTR(T.SELL_NO,1,5)='Z7097' ";
	sqlitem[3][99]= "SUBSTR(T.SELL_NO,1,5)='Z7098' ";
	sqlitem[3][100]= "SUBSTR(T.SELL_NO,1,5)='Z7099' ";
	sqlitem[3][101]= "SUBSTR(T.SELL_NO,1,5)='Z7100' ";
	sqlitem[3][102]= "SUBSTR(T.SELL_NO,1,5)='Z7101' ";
	sqlitem[3][103]= "SUBSTR(T.SELL_NO,1,5)='Z7102' ";
	sqlitem[3][104]= "SUBSTR(T.SELL_NO,1,5)='Z7103' ";
	sqlitem[3][105]= "SUBSTR(T.SELL_NO,1,5)='Z7104' ";
	sqlitem[3][106]= "SUBSTR(T.SELL_NO,1,5)='Z7105' ";
	sqlitem[3][107]= "SUBSTR(T.SELL_NO,1,5)='Z7106' ";
	sqlitem[3][108]= "SUBSTR(T.SELL_NO,1,5)='Z7107' ";
	sqlitem[3][109]= "SUBSTR(T.SELL_NO,1,5)='Z7108' ";
	sqlitem[3][110]= "SUBSTR(T.SELL_NO,1,5)='Z7109' ";
	sqlitem[3][111]= "SUBSTR(T.SELL_NO,1,5)='Z7110' ";
	sqlitem[3][112]= "SUBSTR(T.SELL_NO,1,5)='Z7111' ";
	sqlitem[3][113]= "SUBSTR(T.SELL_NO,1,5)='Z7112' ";
	sqlitem[3][114]= "SUBSTR(T.SELL_NO,1,5)='Z7113' ";
	sqlitem[3][115]= "SUBSTR(T.SELL_NO,1,5)='Z7114' ";
	sqlitem[3][116]= "SUBSTR(T.SELL_NO,1,5)='Z7115' ";
	sqlitem[3][117]= "SUBSTR(T.SELL_NO,1,5)='Z7116' ";
	sqlitem[3][118]= "SUBSTR(T.SELL_NO,1,5)='Z7117' ";
	sqlitem[3][119]= "SUBSTR(T.SELL_NO,1,5)='Z7118' ";
	sqlitem[3][120]= "SUBSTR(T.SELL_NO,1,5)='Z7119' ";
	sqlitem[3][121]= "SUBSTR(T.SELL_NO,1,5)='Z7120' ";
	sqlitem[3][122]= "SUBSTR(T.SELL_NO,1,5)='Z7121' ";
	sqlitem[3][123]= "SUBSTR(T.SELL_NO,1,5)='Z7122' ";
	sqlitem[3][124]= "SUBSTR(T.SELL_NO,1,5)='Z7123' ";
	sqlitem[3][125]= "SUBSTR(T.SELL_NO,1,5)='Z7124' ";
	sqlitem[3][126]= "SUBSTR(T.SELL_NO,1,5)='Z7125' ";
	sqlitem[3][127]= "SUBSTR(T.SELL_NO,1,5)='Z7126' ";
	sqlitem[3][128]= "SUBSTR(T.SELL_NO,1,5)='Z7127' ";
	sqlitem[3][129]= "SUBSTR(T.SELL_NO,1,5)='Z7128' ";
	sqlitem[3][130]= "SUBSTR(T.SELL_NO,1,5)='Z7130' ";
	sqlitem[3][131]= "SUBSTR(T.SELL_NO,1,5)='Z7129' ";
	sqlitem[3][132]= "SUBSTR(T.SELL_NO,1,5)='Z7131' ";
	
	sqlitem[4][1]= "SUBSTR(T.SELL_NO,1,5)in('Z8605','Z8606','Z8691','Z8692','Z8693','Z8694','Z8695','Z8696','Z8697','Z8698','Z8699','Z8700','Z8701','Z8702','Z8703','Z8704','Z8705','Z8706','Z8707','Z8708','Z8826','Z8886','Z8887','Z8888','Z8892','Z8839','Z8840','Z8916','Z8898','Z8956','Z8939','Z8940','Z8941','Z8942','Z8943','Z8944','Z8945','Z8946','Z8947','Z8948','Z8949','Z8950','Z8951','Z8952','Z8953','Z8954','Z8955','Z8899','Z8958','Z8860') ";
	sqlitem[4][2]= "SUBSTR(T.SELL_NO,1,5)='Z8605' ";
	sqlitem[4][3]= "SUBSTR(T.SELL_NO,1,5)='Z8606' ";
	sqlitem[4][4]= "SUBSTR(T.SELL_NO,1,5)='Z8691' ";
	sqlitem[4][5]= "SUBSTR(T.SELL_NO,1,5)='Z8692' ";
	sqlitem[4][6]= "SUBSTR(T.SELL_NO,1,5)='Z8693' ";
	sqlitem[4][7]= "SUBSTR(T.SELL_NO,1,5)='Z8694' ";
	sqlitem[4][8]= "SUBSTR(T.SELL_NO,1,5)='Z8695' ";
	sqlitem[4][9]= "SUBSTR(T.SELL_NO,1,5)='Z8696' ";
	sqlitem[4][10]= "SUBSTR(T.SELL_NO,1,5)='Z8697' ";
	sqlitem[4][11]= "SUBSTR(T.SELL_NO,1,5)='Z8698' ";
	sqlitem[4][12]= "SUBSTR(T.SELL_NO,1,5)='Z8699' ";
	sqlitem[4][13]= "SUBSTR(T.SELL_NO,1,5)='Z8700' ";
	sqlitem[4][14]= "SUBSTR(T.SELL_NO,1,5)='Z8701' ";
	sqlitem[4][15]= "SUBSTR(T.SELL_NO,1,5)='Z8702' ";
	sqlitem[4][16]= "SUBSTR(T.SELL_NO,1,5)='Z8703' ";
	sqlitem[4][17]= "SUBSTR(T.SELL_NO,1,5)='Z8704' ";
	sqlitem[4][18]= "SUBSTR(T.SELL_NO,1,5)='Z8705' ";
	sqlitem[4][19]= "SUBSTR(T.SELL_NO,1,5)='Z8706' ";
	sqlitem[4][20]= "SUBSTR(T.SELL_NO,1,5)='Z8707' ";
	sqlitem[4][21]= "SUBSTR(T.SELL_NO,1,5)='Z8708' ";
	sqlitem[4][22]= "SUBSTR(T.SELL_NO,1,5)='Z8672' ";
	sqlitem[4][23]= "SUBSTR(T.SELL_NO,1,5)='Z8826' ";
	sqlitem[4][24]= "SUBSTR(T.SELL_NO,1,5)='Z8886' ";
	sqlitem[4][25]= "SUBSTR(T.SELL_NO,1,5)='Z8887' ";
	sqlitem[4][26]= "SUBSTR(T.SELL_NO,1,5)='Z8888' ";
	sqlitem[4][27]= "SUBSTR(T.SELL_NO,1,5)='Z8892' ";
	sqlitem[4][28]= "SUBSTR(T.SELL_NO,1,5)='Z8839' ";
	sqlitem[4][29]= "SUBSTR(T.SELL_NO,1,5)='Z8840' ";
	sqlitem[4][30]= "SUBSTR(T.SELL_NO,1,5)='Z8916' ";
	sqlitem[4][31]= "SUBSTR(T.SELL_NO,1,5)='Z8898' ";
	sqlitem[4][32]= "SUBSTR(T.SELL_NO,1,5)='Z8956' ";
	sqlitem[4][33]= "SUBSTR(T.SELL_NO,1,5)='Z8939' ";
	sqlitem[4][34]= "SUBSTR(T.SELL_NO,1,5)='Z8940' ";
	sqlitem[4][35]= "SUBSTR(T.SELL_NO,1,5)='Z8941' ";
	sqlitem[4][36]= "SUBSTR(T.SELL_NO,1,5)='Z8942' ";
	sqlitem[4][37]= "SUBSTR(T.SELL_NO,1,5)='Z8943' ";
	sqlitem[4][38]= "SUBSTR(T.SELL_NO,1,5)='Z8944' ";
	sqlitem[4][39]= "SUBSTR(T.SELL_NO,1,5)='Z8945' ";
	sqlitem[4][40]= "SUBSTR(T.SELL_NO,1,5)='Z8946' ";
	sqlitem[4][41]= "SUBSTR(T.SELL_NO,1,5)='Z8947' ";
	sqlitem[4][42]= "SUBSTR(T.SELL_NO,1,5)='Z8948' ";
	sqlitem[4][43]= "SUBSTR(T.SELL_NO,1,5)='Z8949' ";
	sqlitem[4][44]= "SUBSTR(T.SELL_NO,1,5)='Z8950' ";
	sqlitem[4][45]= "SUBSTR(T.SELL_NO,1,5)='Z8951' ";
	sqlitem[4][46]= "SUBSTR(T.SELL_NO,1,5)='Z8952' ";
	sqlitem[4][47]= "SUBSTR(T.SELL_NO,1,5)='Z8953' ";
	sqlitem[4][48]= "SUBSTR(T.SELL_NO,1,5)='Z8954' ";
	sqlitem[4][49]= "SUBSTR(T.SELL_NO,1,5)='Z8955' ";
	sqlitem[4][50]= "SUBSTR(T.SELL_NO,1,5)='Z8899' ";
	sqlitem[4][51]= "SUBSTR(T.SELL_NO,1,5)='Z8958' ";
	sqlitem[4][52]= "SUBSTR(T.SELL_NO,1,5)='Z8860' ";
	
	sqlitem[5][1]= "SUBSTR(T.SELL_NO,1,2)='ZP' ";
	sqlitem[5][2]= "SUBSTR(T.SELL_NO,1,5)='ZP000' ";
	sqlitem[5][3]= "SUBSTR(T.SELL_NO,1,5)='ZP001' ";
	sqlitem[5][4]= "SUBSTR(T.SELL_NO,1,5)='ZP002' ";
	sqlitem[5][5]= "SUBSTR(T.SELL_NO,1,5)='ZP003' ";
	sqlitem[5][6]= "SUBSTR(T.SELL_NO,1,5)='ZP004' ";
	sqlitem[5][7]= "SUBSTR(T.SELL_NO,1,5)='ZP006' ";
	sqlitem[5][8]= "SUBSTR(T.SELL_NO,1,5)='ZP005' ";
	sqlitem[5][9]= "SUBSTR(T.SELL_NO,1,5)='ZP007' ";
	sqlitem[5][10]= "SUBSTR(T.SELL_NO,1,5)='ZP008' ";
	sqlitem[5][11]= "SUBSTR(T.SELL_NO,1,5)='ZP00A' ";
	sqlitem[5][12]= "SUBSTR(T.SELL_NO,1,5)='ZP009' ";
	sqlitem[5][13]= "SUBSTR(T.SELL_NO,1,5)='ZP00B' ";
	sqlitem[5][14]= "SUBSTR(T.SELL_NO,1,5)='ZP00D' ";
	sqlitem[5][15]= "SUBSTR(T.SELL_NO,1,5)='ZP00C' ";
	sqlitem[5][16]= "SUBSTR(T.SELL_NO,1,5)='ZP00E' ";
	sqlitem[5][17]= "SUBSTR(T.SELL_NO,1,5)='ZP00F' ";
	sqlitem[5][18]= "SUBSTR(T.SELL_NO,1,5)='ZP00G' ";
	sqlitem[5][19]= "SUBSTR(T.SELL_NO,1,5)='ZP00H' ";
	sqlitem[5][20]= "SUBSTR(T.SELL_NO,1,5)='ZP00J' ";
	sqlitem[5][21]= "SUBSTR(T.SELL_NO,1,5)='ZP00K' ";
	sqlitem[5][22]= "SUBSTR(T.SELL_NO,1,5)='ZP00L' ";
	sqlitem[5][23]= "SUBSTR(T.SELL_NO,1,5)='ZP00M' ";
	sqlitem[5][24]= "SUBSTR(T.SELL_NO,1,5)='ZP00N' ";
	sqlitem[5][25]= "SUBSTR(T.SELL_NO,1,5)='ZP00P' ";
	sqlitem[5][26]= "SUBSTR(T.SELL_NO,1,5)='ZP00Q' ";
	sqlitem[5][27]= "SUBSTR(T.SELL_NO,1,5)='ZP00R' ";
	sqlitem[5][28]= "SUBSTR(T.SELL_NO,1,5)='ZP00S' ";
	sqlitem[5][29]= "SUBSTR(T.SELL_NO,1,5)='ZP00T' ";
	sqlitem[5][30]= "SUBSTR(T.SELL_NO,1,5)='ZP00U' ";
	sqlitem[5][31]= "SUBSTR(T.SELL_NO,1,5)='ZP00V' ";
	sqlitem[5][32]= "SUBSTR(T.SELL_NO,1,5)='ZP00W' ";
	sqlitem[5][33]= "SUBSTR(T.SELL_NO,1,5)='ZP00X' ";
	sqlitem[5][34]= "SUBSTR(T.SELL_NO,1,5)='ZP00Y' ";
	sqlitem[5][35]= "SUBSTR(T.SELL_NO,1,5)='ZP00Z' ";
	sqlitem[5][36]= "SUBSTR(T.SELL_NO,1,5)='ZP010' ";
	sqlitem[5][37]= "SUBSTR(T.SELL_NO,1,5)='ZP011' ";
	sqlitem[5][38]= "SUBSTR(T.SELL_NO,1,5)='ZP012' ";
	sqlitem[5][39]= "SUBSTR(T.SELL_NO,1,5)='ZP013' ";
	sqlitem[5][40]= "SUBSTR(T.SELL_NO,1,5)='ZP014' ";
	sqlitem[5][41]= "SUBSTR(T.SELL_NO,1,5)='ZP015' ";
	sqlitem[5][42]= "SUBSTR(T.SELL_NO,1,5)='ZP016' ";

    /** 子ジャンルのID名 */
    var idName="childS";

    /** 親ジャンルが変更されたら、子ジャンルを生成 */
    function createChildOptions(frmObj) {
        /** 親ジャンルを変数pObjに格納 */
        var pObj=frmObj.elements["parentS"].options;
        /** 親ジャンルのoption数 */
        var pObjLen=pObj.length;
        var htm="<select name='childS' style='width:300px;'>";
        for(i=0; i<pObjLen; i++ ) {
            /** 親ジャンルの選択値を取得 */
            if(pObj[i].selected>0){
                var itemLen=item[i].length;
                for(j=0; j<itemLen; j++){
                  /** 地球の友と歩む会の選択値を非表示 */
                  /** if (j!=33){ */
                    htm+="<option value='"+j+"'>"+item[i][j]+"<\/option>";
                  /** } */
                }
            }
        }
        htm+="<\/select>";
        /** HTML出力 */
        document.getElementById(idName).innerHTML=htm;
    }

    /** 選択されている値をアラート表示 */
    function chkSelect(frmObj) {

 if (document.frmdlvol.T1.value=="")
	{
	alert("開始日付を入力してください");
	return;
	}
 if (document.frmdlvol.T2.value=="")
	{
	alert("終了日付を入力してください");
	return;
	}

var s="";
var idxP=frmObj.elements['parentS'].selectedIndex;
var idxC=frmObj.elements['childS'].selectedIndex;
if(idxP>0){
	s+="親ジャンルの選択肢："+frmObj.elements['parentS'][idxP].text+"\n";
	if(idxC > 0){
		s+="子ジャンルの選択肢："+frmObj.elements['childS'][idxC].text+"\n";
}else{
	s+="子ジャンルが選択されていません\n";
	alert(s);
	return;
}
}else{
	s+="親ジャンルが選択されていません\n";
	alert(s);
	return;
}
//s+=sqlitem[idxP][idxC]+"\n";
//alert(s);
//スターツピタットハウス特別対応
if ((idxP==2)&&(idxC==2)){
	document.frmdlvol.T4.value=9;
}else
{
	document.frmdlvol.T4.value=0;
}
//where句を付与してサブミット
document.frmdlvol.T3.value=sqlitem[idxP][idxC];
document.frmdlvol.submit();


}

    /* onLoad時にプルダウンを初期化 */
    function init(){
        htm ="<select name='childS' style='width:350px;'>";
        htm+="<option value=''>"+item[0][0]+"<\/option>";
        htm+="<\/select>";
        /* HTML出力 */
        document.getElementById("childS").innerHTML=htm;
    }

    /* ページ読み込み完了時に、プルダウン初期化を実行 */
    window.onload=init;
</script>
<br><br>
<input type="button" value="データ出力" onclick="chkSelect(this.form);" />
<input type="reset" value="リセット">
</FORM>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>