<?php
require '../parts/pagechk.inc';
$today = date("YmdHis");
$file_name = "./dlfile/volunteer_assdate_" . $today . ".csv";
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
output_log("ボランティア振込済データダウンロード",$_SESSION["user_id"],'volout');
// 前画面からの検索キーワードを取得する
$keyti1 = addslashes(@$_POST["T1"]);
$keyti2 = addslashes(@$_POST["T2"]);
$keyti3 = @$_POST["T3"];
$keyti4 = addslashes(@$_POST["T4"]);

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//

$sql = "SELECT DISTINCT * FROM (SELECT ".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z12','シャンティ国際ボランティア会',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z13','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z14','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z15','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z16','NTT西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z17','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1001','Room to Read',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1002','本で寄付するプロジェクト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1003','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1004','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1005','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1006','移動図書館支援',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1007','かながわキンタロウ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1029','大阪YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1030','グッドネーバーズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1031','国連ウィメン日本協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1032','湘南ふじさわシニアネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1033','アジア保健研修所(AHI)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7001','NTT大阪支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7002','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7003','帝人',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6001','スターツピタットハウス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7004','セガサミーホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7005','NTTネオメイト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7006','NEC（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7007','キャドバリージャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7008','ファミリーマート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6002','大京アステージ(くらしスクエア)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6003','大京アステージ(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6004','三井不動産住宅サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7009','結核予防会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7010','ナック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7011','アイエスエフネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7012','インテリジェンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7013','チャーティス・ファー・イースト・ホールティングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7014','民際センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7015','DNPグループ労連',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7016','しょうがっこうをおくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7017','レオパレス２１(企業ボランティア宅本便)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7018','神奈川大学',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7019','ICAN（アイキャン）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6005','アスク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7020','阪急阪神ホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6006','インターファーム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7021','ソニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7022','地球環境基金',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7023','売って支援プログラム（東日本大震災）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7024','西友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7025','日本経済新聞文化部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7026','朝日新聞社論説委員室',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7027','HFI（Hope and Faith International）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7028','ACE（エース）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6007','アート引越しセンター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6008','クラブパナソニック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7029','マンパワーグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7030','中日本ハイウェイ・パトロール東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6009','大東建託パートナーズ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7031','三菱商事',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7032','地球の友と歩む会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7033','損保ジャパン奈良支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6010','アリさんマークの引越社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7034','日本興亜損害保険',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6011','イヌイ運送',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7035','3keys（スリーキーズ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7036','ライフ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7037','★スポット対応企業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7038','町田ゼルビア',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6012','日本通運',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7039','SC相模原',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6013','ハウスコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7040','幼い難民を考える会（CYR）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7041','アフラック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7042','JHP学校をつくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7043','日立建機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7044','北海道森と緑の会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7045','ユニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6014','オーネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6015','バイク王',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6016','三菱地所コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6017','コミュニティワン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6018','住友不動産建物サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6019','リトル・ママ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6020','レジデンスクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6021','JAF',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7046','埼玉県自動車販売店協会（自販連埼玉支部）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6022','ぐるなび食市場',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7047','マルハン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7048','アドビシステムズ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7049','FC在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6023','ヤマトホームコンビニエンス（クロネコヤマト引越センター）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6024','ファミリー引越センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6025','ジャパンネット銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7050','大塚商会(3keys)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7051','森ビル都市企画',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7052','アクセス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6026','暮らしのサポート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7053','ブリヂストンスポーツ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7054','ケア・インターナショナルジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7055','世界の子どもにワクチンを(JCV)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7056','共立メンテナンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6027','三井不動産レジデンシャル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6028','エイブル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6029','MAST(マスト)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6030','フロムヴイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6031','大和ライフネクスト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6032','レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6033','ズバット引越し比較',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6034','インターネットコンシェルジュサービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6035','長谷工コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6036','QLC(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6037','住宅情報館(城南建設)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6038','ジャックスカード',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6039','積村ビル管理',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6040','東急住宅リース',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6041','ハッピークラブモール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6042','おうちCO-OP',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6043','ハイホー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6044','ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7057','ジャパンハート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7058','MSD',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7059','ブリッジ エーシア ジャパン(BAJ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7060','TBS',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7061','TBS(フィリピン台風支援)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7062','岡谷鋼機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7063','横浜開港祭',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7064','商船三井',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7065','広栄化学工業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7066','ハグオール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7067','NTT労働組合 西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7068','日本ラグビーフットボール協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7069','店舗在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7070','神戸YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7071','北洋銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7072','なおちゃんを救う会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7073','honto返品買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7074','シニアライフセラピー研究所',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6045','ソフトバンクグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6046','オリックス・クレジット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6047','リロクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6048','メディアカフェポパイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6049','アメリカン・エキスプレス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6050','オリックス・ゴルフ・マネジメント',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6051','エイコータウン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6052','社員向け_レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6053','macalon+（マカロンプラス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6054','タイムズクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6055','ゆとライフドットコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6056','クラスエル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6057','バイクブロス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6058','泉友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6059','富士フイルム生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6060','リコー三愛グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1034','ウィメンズアクションネットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1100','日本赤十字社 本社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1111','日本赤十字社 埼玉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1112','日本赤十字社 千葉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1113','日本赤十字社 東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1114','日本赤十字社 神奈川',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6061','ヴァリック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6062','LIFULL引越し',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6063','千葉県庁生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6064','エポスカード',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1035','アーユス仏教国際協力ネットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6065','エフコープ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1036','しみん基金・こうべ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1037','売って支援プログラム（熊本地震）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6066','明電グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7075','NTT労働組合　東日本本部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1038','東京都日中友好協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6067','青森県庁生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6068','おおさかパルコープ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6069','よどがわ市民生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6070','東急リバブル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6071','auコレトク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6072','ARUHI暮らしのサービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6073','京都生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7076','プリック ジャパン ビューティー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7077','損害保険ジャパン日本興亜（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7078','損害保険ジャパン日本興亜（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1039','結核予防会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1040','民際センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1041','しょうがっこうをおくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1042','神奈川大学',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1043','ICAN（アイキャン）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1044','地球環境基金',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1045','HFI（Hope and Faith International）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1046','ACE（エース）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1047','3keys（スリーキーズ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1048','町田ゼルビア',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1049','SC相模原',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1050','幼い難民を考える会(CYR)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1051','JHP学校をつくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1052','北海道森と緑の会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1053','アクセス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1054','ケア・インターナショナルジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1055','世界の子どもにワクチンを(JCV)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1056','ジャパンハート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1057','ブリッジ エーシア ジャパン(BAJ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1058','売って支援プログラム（東日本大震災）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7079','ニッポン放送',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7080','NHK渋谷',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7081','NHK名古屋',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7082','三井不動産レジデンシャル（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7083','三井不動産レジデンシャル（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7084','三菱地所コミュニティ（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7085','関西テレビ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7086','かわさき市民活動センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7087','山田養蜂場',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7088','アリさんマークの引越社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7089','NTTデータ（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7090','日本バルカー工業（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7091','ルネサスイーストン（シャンティ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7092','住友生命札幌支社（シャンティ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7093','三井住友カード（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7094','日経印刷（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7095','アトミクス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7096','LIC',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7097','JAIFA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7098','SOMPOシステムズ（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7099','日経印刷　長野（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7100','フロムヴイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7101','愛知県職員組合(移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7102','ベクトルフラックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7103','バリューブックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7104','東京海上グループ『未来塾』（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7105','大阪商工信用金庫',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7106','三菱UFJニコス（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7107','アカツキ（BUY王）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7108','アリアンツ火災海上保険（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7109','損害保険ジャパン日本興亜　関西総務部（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7110','ブックマーケティング',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7111','正蓮寺',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7112','オムロンフィールドエンジニアリング（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6074','FUKUYA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6075','群馬県庁生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6076','ライフサポート倶楽部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6077','ベルスファミリークラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6078','群馬県学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6079','とくしま生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6080','日産グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6081','NHK共済会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6082','えらべる倶楽部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6083','茨城県学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6084','福井県学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6085','LivLi CLUB（リブリクラブ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6086','CHINTAI',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6087','CLASSY LIFE',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6088','静岡県教職員生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6089','TS ONE',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6090','長崎県職員生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6091','安川電機グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6092','明治安田生命保険相互会社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6093','北海道学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1059','よこはまユース',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1060','ボーイスカウト日本連盟',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1061','国境なき医師団',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1063','国境なき子どもたち',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1123','日本赤十字社 愛知',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1127','日本赤十字社 大阪',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z18','帝人','ERR')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) AS AA,".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"A.ASS_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A ".
"WHERE T.SELL_NO=A.SELL_NO AND PAY_TP='1' ".
"AND A.ASS_DT is not null and T.SELL_STAT in ('03','04','05','07','97') AND T.PAY_HOLD_FLG='1' ".
"AND SUBSTR(A.ASS_DT, 1, 8)>='".$keyti1 . "' ".
"AND SUBSTR(A.ASS_DT, 1, 8)<='".$keyti2 . "' ".
"AND ".$keyti3 .
"UNION SELECT ".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z12','シャンティ国際ボランティア会',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z13','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z14','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z15','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z16','NTT西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z17','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1001','Room to Read',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1002','本で寄付するプロジェクト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1003','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1004','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1005','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1006','移動図書館支援',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1007','かながわキンタロウ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1029','大阪YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1030','グッドネーバーズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1031','国連ウィメン日本協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1032','湘南ふじさわシニアネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1033','アジア保健研修所(AHI)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7001','NTT大阪支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7002','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7003','帝人',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6001','スターツピタットハウス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7004','セガサミーホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7005','NTTネオメイト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7006','NEC（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7007','キャドバリージャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7008','ファミリーマート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6002','大京アステージ(くらしスクエア)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6003','大京アステージ(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6004','三井不動産住宅サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7009','結核予防会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7010','ナック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7011','アイエスエフネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7012','インテリジェンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7013','チャーティス・ファー・イースト・ホールティングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7014','民際センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7015','DNPグループ労連',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7016','しょうがっこうをおくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7017','レオパレス２１(企業ボランティア宅本便)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7018','神奈川大学',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7019','ICAN（アイキャン）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6005','アスク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7020','阪急阪神ホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6006','インターファーム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7021','ソニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7022','双日エアロスペース(地球環境基金)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7023','売って支援プログラム（東日本大震災）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7024','西友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7025','日本経済新聞文化部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7026','朝日新聞社論説委員室',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7027','HFI（Hope and Faith International）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7028','ACE（エース）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6007','アート引越しセンター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6008','クラブパナソニック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7029','マンパワーグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7030','中日本ハイウェイ・パトロール東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6009','大東建託パートナーズ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7031','三菱商事',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7032','地球の友と歩む会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7033','損保ジャパン奈良支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6010','アリさんマークの引越社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7034','日本興亜損害保険',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6011','イヌイ運送',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7035','3keys（スリーキーズ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7036','ライフ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7037','★スポット対応企業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7038','町田ゼルビア',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6012','日本通運',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7039','SC相模原',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6013','ハウスコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7040','幼い難民を考える会（CYR）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7041','アフラック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7042','JHP学校をつくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7043','日立建機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7044','北海道森と緑の会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7045','ユニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6014','オーネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6015','バイク王',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6016','三菱地所コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6017','コミュニティワン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6018','住友不動産建物サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6019','リトル・ママ（東京）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6020','レジデンスクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6021','JAF',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7046','埼玉県自動車販売店協会（自販連埼玉支部）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6022','ぐるなび食市場',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7047','マルハン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7048','アドビシステムズ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7049','FC在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6023','ヤマトホームコンビニエンス（クロネコヤマト引越センター）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6024','ファミリー引越センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6025','ジャパンネット銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7050','大塚商会(3keys)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7051','森ビル都市企画',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7052','アクセス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6026','暮らしのサポート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7053','ブリヂストンスポーツ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7054','ケア・インターナショナルジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7055','世界の子どもにワクチンを(JCV)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7056','共立メンテナンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6027','三井不動産レジデンシャル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6028','エイブル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6029','MAST(マスト)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6030','フロムヴイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6031','大和ライフネクスト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6032','レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6033','ズバット引越し比較',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6034','インターネットコンシェルジュサービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6035','長谷工コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6036','QLC(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6037','住宅情報館(城南建設)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6038','ジャックスカード',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6039','積村ビル管理',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6040','東急住宅リース',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6041','ハッピークラブモール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6042','おうちCO-OP',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6043','ハイホー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6044','ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7057','ジャパンハート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7058','MSD',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7059','ブリッジ エーシア ジャパン(BAJ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7060','TBS',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7061','TBS(フィリピン台風支援)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7062','岡谷鋼機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7063','横浜開港祭',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7064','商船三井',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7065','広栄化学工業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7066','ハグオール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7067','NTT労働組合 西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7068','日本ラグビーフットボール協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7069','店舗在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7070','神戸YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7071','北洋銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7072','なおちゃんを救う会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7073','honto返品買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7074','シニアライフセラピー研究所',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6045','ソフトバンクグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6046','オリックス・クレジット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6047','リロクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6048','メディアカフェポパイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6049','アメリカン・エキスプレス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6050','オリックス・ゴルフ・マネジメント',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6051','エイコータウン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6052','社員向け_レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6053','macalon+（マカロンプラス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6054','タイムズクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6055','ゆとライフドットコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6056','クラスエル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6057','バイクブロス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6058','泉友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6059','富士フイルム生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6060','リコー三愛グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1034','ウィメンズアクションネットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1100','日本赤十字社 本社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1111','日本赤十字社 埼玉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1112','日本赤十字社 千葉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1113','日本赤十字社 東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1114','日本赤十字社 神奈川',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6061','ヴァリック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6062','LIFULL引越し',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6063','千葉県庁生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6064','エポスカード',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1035','アーユス仏教国際協力ネットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6065','エフコープ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1036','しみん基金・こうべ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1037','売って支援プログラム（熊本地震）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6066','明電グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7075','NTT労働組合　東日本本部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1038','東京都日中友好協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6067','青森県庁生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6068','おおさかパルコープ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6069','よどがわ市民生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6070','東急リバブル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6071','auコレトク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6072','ARUHI暮らしのサービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6073','京都生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7076','プリック ジャパン ビューティー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7077','損害保険ジャパン日本興亜（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7078','損害保険ジャパン日本興亜（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1039','結核予防会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1040','民際センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1041','しょうがっこうをおくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1042','神奈川大学',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1043','ICAN（アイキャン）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1044','地球環境基金',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1045','HFI（Hope and Faith International）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1046','ACE（エース）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1047','3keys（スリーキーズ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1048','町田ゼルビア',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1049','SC相模原',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1050','幼い難民を考える会(CYR)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1051','JHP学校をつくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1052','北海道森と緑の会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1053','アクセス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1054','ケア・インターナショナルジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1055','世界の子どもにワクチンを(JCV)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1056','ジャパンハート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1057','ブリッジ エーシア ジャパン(BAJ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1058','売って支援プログラム（東日本大震災）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7079','ニッポン放送',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7080','NHK渋谷',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7081','NHK名古屋',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7082','三井不動産レジデンシャル（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7083','三井不動産レジデンシャル（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7084','三菱地所コミュニティ（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7085','関西テレビ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7086','かわさき市民活動センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7087','山田養蜂場',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7088','アリさんマークの引越社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7089','NTTデータ（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7090','日本バルカー工業（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7091','ルネサスイーストン（シャンティ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7092','住友生命札幌支社（シャンティ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7093','三井住友カード（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7094','日経印刷（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7095','アトミクス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7096','LIC',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7097','JAIFA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7098','SOMPOシステムズ（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7099','日経印刷　長野（移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7100','フロムヴイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7101','愛知県職員組合(移動図書館支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7102','ベクトルフラックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7103','バリューブックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7104','東京海上グループ『未来塾』（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7105','大阪商工信用金庫',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7106','三菱UFJニコス（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7107','アカツキ（BUY王）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7108','アリアンツ火災海上保険（東日本大震災支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7109','損害保険ジャパン日本興亜　関西総務部（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7110','ブックマーケティング',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7111','正蓮寺',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7112','オムロンフィールドエンジニアリング（熊本地震支援）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6074','FUKUYA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6075','群馬県庁生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6076','ライフサポート倶楽部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6077','ベルスファミリークラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6078','群馬県学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6079','とくしま生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6080','日産グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6081','NHK共済会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6082','えらべる倶楽部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6083','茨城県学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6084','福井県学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6085','LivLi CLUB（リブリクラブ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6086','CHINTAI',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6087','CLASSY LIFE',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6088','静岡県教職員生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6089','TS ONE',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6090','長崎県職員生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6091','安川電機グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6092','明治安田生命保険相互会社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6093','北海道学校生協',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1059','よこはまユース',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1060','ボーイスカウト日本連盟',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1061','国境なき医師団',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1063','国境なき子どもたち',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1123','日本赤十字社 愛知',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1127','日本赤十字社 大阪',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z18','帝人','ERR')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) AS AA,".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"A.ASS_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A ".
"WHERE T.SELL_NO=A.SELL_NO AND PAY_TP='2' ".
"AND A.ASS_DT is not null and T.SELL_STAT in ('03','04','05','07','97') AND T.PAY_HOLD_FLG='1' ".
"AND SUBSTR(A.ASS_DT, 1, 8)>='".$keyti1 . "' ".
"AND SUBSTR(A.ASS_DT, 1, 8)<='".$keyti2 . "' ".
"AND ".$keyti3 .") ORDER BY SELL_NO";


if ($keyti4==9){
$sql = "SELECT DISTINCT * FROM (SELECT ".
"'スターツピタットハウス',".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"A.ASS_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A ".
"WHERE T.SELL_NO=A.SELL_NO ".
"AND A.ASS_DT is not null and T.SELL_STAT in ('03','04','05','07','97') AND T.PAY_HOLD_FLG='1' AND PAY_TP='1' ".
"AND SUBSTR(A.ASS_DT, 1, 8)>='".$keyti1 . "' ".
"AND SUBSTR(A.ASS_DT, 1, 8)<='".$keyti2 . "' ".
"AND ".$keyti3 .
"UNION SELECT ".
"'スターツピタットハウス',".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"A.ASS_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A ".
"WHERE T.SELL_NO=A.SELL_NO AND PAY_TP='2' ".
"AND A.ASS_DT is not null and T.SELL_STAT in ('03','04','05','07','97') AND T.PAY_HOLD_FLG='1' ".
"AND SUBSTR(A.ASS_DT, 1, 8)>='".$keyti1 . "' ".
"AND SUBSTR(A.ASS_DT, 1, 8)<='".$keyti2 . "' ".
"AND ".$keyti3 .
") ORDER BY SELL_NO";

}

//echo $sql;

$res = $db->query($sql);
if(DB::isError($res)){
    $res->getDebugInfo();
//	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	require '../parts/outputcsv_query.php';
	outputcsv($res,$file_name);
$res->free();
//
$db->disconnect();
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ボランティア振込済データダウンロード</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
//	print $sql;
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>