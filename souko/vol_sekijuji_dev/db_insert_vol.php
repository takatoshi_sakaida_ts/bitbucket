<?PHP
function db_insert(){
//PEARの利用     -------(1)
require_once("DB.php");
require_once("selectvalue_vol.php");
require_once("data_sekijuji.php");
// 銀行情報のセット
require_once("vol_bank.php");
//ログイン情報の読み込み
require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
if(DB::isError($db)){
	die("connect failed:" . $db->getMessage());
}

$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]="";
//買取受付番号の決定
$sql="select max(sell_no) from tsell where sell_no like ?";
$res = $db->query($sql,$sellno_head."%");
if(DB::isError($res)){
	die($res->getMessage());
}
$sell_no = 0;
while($row = $res->fetchRow()){
    if(isset($row[0]) && $row[0] != null){
    	$sell_no=substr($row[0],5,8);
    }
//	print $row[0]."<BR>";
}
$res->free();

//print "sell_noは".$sell_no."<br>";
$sell_no=$sell_no + 1;
//print "sell_no(+1)は".$sell_no."<br>";
$sell_no = sprintf("%08d",$sell_no);
$sell_no= $sellno_head . $sell_no;

//
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='';
}else
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='000000000000000';
}
//ガイド発送が不要なら申込書自動到着
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
}else{
	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
	$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]=date("Ymd");
}

// 自動振込保留 = true
$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='1';

$memo = ":団体名：日本赤十字社 ". $shukaTgt[$sellno_head] ."　ガイド発送:" . Retgaido($_SESSION["HSMAP"]["gaido"]);

//print_r($_SESSION["HSMAP"]);

$sql="INSERT INTO tsell(".
"SELL_NO,".
"SELL_STAT,".
"PICKUP_NM_FST,".
"PICKUP_NM_MID,".
"PICKUP_NM_LAST,".
"PICKUP_NM_ETC,".
"PICKUP_ZIP_CD,".
"PICKUP_ZIP_ADDR1,".
"PICKUP_ZIP_ADDR2,".
"PICKUP_ZIP_ADDR3,".
"PICKUP_DTL_ADDR,".
"PICKUP_TEL_NO,".
"PICKUP_TEL_NO_2,".
"PICKUP_REQ_DT,".
"PICKUP_REQ_TIME,".
"BOX,".
"NOTE_YN,".
"DISPOSE,".
"RECEIPT_TP,".
"MEM_BIRTH_DT,".
"MEM_SEX,".
"MEM_JOB,".
"PAY_TP,".
"DL_STAT_LABEL,".
"BANK_CD,".
"BANK_BRANCH_CD,".
"BANK_NM,".
"BRANCH_NM,".
"BANK_ACC_TP,".
"BANK_ACC_NO,".
"BANK_ACC_NM,".
"MEMO,".
"FORM_ARRIVAL,".
"PICK_DATA_EXPT,".
"SELL_FORM_EXPT,".
"ADMIT_DATA_EXPT,".
"ASSES_CNT,".
"REG_DM,".
"UPD_DM,".
"PAY_HOLD_FLG,".
"AUTH_SEND_FLG,".
"SELL_FORM_GET_DT,".
"USER_ID".
") VALUES('"
	.$sell_no."','"
	."01','"
	.$_SESSION["HSMAP"]["PICKUP_NM_FST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_MID"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_LAST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_ETC"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_REQ_DTY"].$_SESSION["HSMAP"]["PICKUP_REQ_DTM"].$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]."','"
	."0".$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]."','"
	.$_SESSION["HSMAP"]["BOX"]."','"
	."0','"
	."0','"
	."2','"
	."19500101','"
	.$_SESSION["HSMAP"]["MEM_SEX"]."','"
	."09','"
	."1','"
	.$_SESSION["HSMAP"]["DL_STAT_LABEL"]."','"
	.$_SESSION["HSMAP"]["BANK_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_BRANCH_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_NM"]."','"
	.$_SESSION["HSMAP"]["BRANCH_NM"]."','"
	.$_SESSION["HSMAP"]["KOUZASHURUI"]."','"
	.$_SESSION["HSMAP"]["KOUZABANGOU"]."','"
	.$_SESSION["HSMAP"]["KOUZAMEIGI"]."','"
	.$_SESSION["HSMAP"]["MEMO"]. $memo ."','"
	.$_SESSION["HSMAP"]["FORM_ARRIVAL"]."','"
	."1','"
	."1','"
	."1','"
	."0','"
	.date("YmdHis")."','"
	.date("YmdHis")."','".$_SESSION["HSMAP"]["PAY_HOLD_FLG"]."','0','"
	.$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]."','"
	.$_SESSION["user_id"]."')";
//print "<br>".$sql;
$db->autoCommit( false ); 
$res2 = $db->query($sql);
if(DB::isError($res2)){
    $db->rollback();
	die("tsell insert failed:" . $res2->getMessage());
	//$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//
output_log($sql,$_SESSION["user_id"],'vol_entry_sql');
$sql="INSERT INTO tsellassessment (".
"SELL_NO,".
"REG_DM".
") VALUES('"
	.$sell_no."','"
	.date("YmdHis")."')";

$res3 = $db->query($sql);
if(DB::isError($res3)){
    $db->rollback();
	die("tsellassessment insert failed:".$res3->getMessage());
	//$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
		$_SESSION["HSMAP"]["sell_no"]=$sell_no;
$db->commit();
//$db->rollback();
output_log($sell_no,$_SESSION["user_id"],'vol_entry');
output_log($sql,$_SESSION["user_id"],'vol_entry_sql');
//print "<br>".$sql;
//データの開放
$db->disconnect();
}
?>