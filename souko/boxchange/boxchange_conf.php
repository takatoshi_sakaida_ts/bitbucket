<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>箱数・集荷日変更確認画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//買取受付番号の決定
$j="ng";
$sql="select pickup_nm_fst,pickup_nm_mid,sell_stat,receipt_tp,pickup_nm_last,pickup_nm_etc,pickup_req_dt,decode(pickup_req_time,'01','午前','02','午後','03','夕方以降'),box,(pickup_req_time-1) from tsell where sell_no = '" . $keyti . "'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$j="ok";
	$kaitori_name=$row[0]."　".$row[1];
	$kaitori_name_kana=$row[4]."　".$row[5];
	$kaitori_sell_stat=$row[2];
	$kaitori_receipt_tp=$row[3];
	$kaitori_pickup_req_dt=$row[6];
	$kaitori_pickup_req_time=$row[7];
	$kaitori_box=$row[8];
	$_SESSION["HSMAP"]["pickup_req_time"]=$row[9];
}
if ($j!="ng")
{
$chk_flg="0";
	//検索結果の表示
	print "<strong>箱数・集荷日変更受付画面：". $keyti."</strong> <br><br>\n";
	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td>買取受付番号</td><td>集荷先氏名</td><td>集荷先氏名カナ</td><td>集荷予定日</td><td>集荷予定時間帯</td><td>箱数</td></tr>";
	print "<tr>";
	print "<td>".$keyti."</td>";
	print "<td>".$kaitori_name."</td>";
	print "<td>".$kaitori_name_kana."</td>";
	print "<td>".$kaitori_pickup_req_dt."</td>";
	print "<td>".$kaitori_pickup_req_time."</td>";
	print "<td>".$kaitori_box."</td>";
	print "</TR></table>";
	//データの開放
	$res->free();
	$db->disconnect();
	if ($kaitori_sell_stat!='01')
	{
		print "<BR>ステータスが受付中ではありません<BR>";
		$chk_flg="1";
	}
/*
	if ($kaitori_receipt_tp!='2')
	{
		print "受付方法がFDではありません";
		$chk_flg="1";
	}
*/
}
else
{
	$chk_flg="1";
	print "買取受付番号に誤りがあります　:" .$keyti  ;
}
?>
<BR>
<form method="POST" action="boxchange_ent.php" name ="boxchange">
<input type=hidden name=kaitori_no value="
<?PHP
print $keyti;
?>
">
<?PHP
if ($chk_flg=="0")
{
$_SESSION["HSMAP"]["kaitori_no"]=$keyti;
?>
<HR>
<table><TR>
<TD>集荷予定日：</TD>
<TD><input type=input name="hiduke" size="12" maxlength="8" value="
<?PHP
print $kaitori_pickup_req_dt;
?>
"></TD></TR>
<TR><TD>
集荷予定時間帯：</TD>
<TD><select  name="jikan">
<option value="01">午前</option>
<option value="02">午後</option>
<option value="03">夕方以降</option>
</select>
</TD></TR>
<TR><TD>
集荷予定箱数：</TD>
<TD><input type=input name="hako" size="4" maxlength=2" value="
<?PHP
print $kaitori_box;
?>
"></TD></TR></TABLE><BR>
<input type="button" value="　　登録　　" onClick="entchk()">
<input type="hidden" name="pickup_req_time_index" value=
<?PHP
print $_SESSION["HSMAP"]["pickup_req_time"];
?>
>
<script type="text/javascript">
document.boxchange.jikan.selectedIndex=document.boxchange.pickup_req_time_index.value;
function entchk()
{
<!--集荷予定日チェック
	<!--数字チェック
if (!Numchk(document.boxchange.hiduke.value,"集荷予定日")){return false;}
	<!--必須チェック
if (!hissuchk(document.boxchange.hiduke.value,"集荷予定日")){return false;}
<!--箱数チェック
	<!--数字チェック
if (!Numchk(document.boxchange.hako.value,"集荷予定箱数")){return false;}
	<!--必須チェック
if (!hissuchk(document.boxchange.hako.value,"集荷予定箱数")){return false;}

	datestr=document.boxchange.hiduke.value.substr(0, 4) + "/" + document.boxchange.hiduke.value.substr(4, 2) + "/" + document.boxchange.hiduke.value.substr(6, 2)
// 正規表現による書式チェック 
	if(!datestr.match(/^\d{4}\/\d{2}\/\d{2}$/)){ 
		alert("集荷希望日が正しく入力されていません");
		return; 
	}
	var vYear = datestr.substr(0, 4) - 0; 
	var vMonth = datestr.substr(5, 2) - 1; // Javascriptは、0-11で表現 
	var vDay = datestr.substr(8, 2) - 0; 
// 月,日の妥当性チェック 
	if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){ 
		var vDt = new Date(vYear, vMonth, vDay); 
		if(isNaN(vDt)){ 
			alert("集荷希望日が正しく入力されていません");
			return; 
		}else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){ 
		}else{ 
			alert("集荷希望日が正しく入力されていません");
			return; 
		} 
	}else{ 
		alert("集荷希望日が正しく入力されていません");
		return; 
	}
//集荷予定日チェック
var uYear,uMon,uDate,uchkdate;
var nt = new Date();
nt.setTime(nt.getTime()+129600000);
nt.getTime();
uYear = nt.getFullYear();
uMon = nt.getMonth()+1;
uDate = nt.getDate();
uchkdate=zeroUme(uYear,4)+zeroUme(uMon,2)+zeroUme(uDate,2);
if (uchkdate>document.boxchange.hiduke.value){
	alert("集荷希望日が正しく入力されていません"+uchkdate+"以降を入力してください");
	return; 
}
if (confirm(document.boxchange.kaitori_no.value+"の処理を実施してもよろしいでしょうか？")){
	document.boxchange.action="boxchange_ent.php";
	document.boxchange.submit();
}
}
function Numchk(txt,koumoku)
{
<!--数字チェック
	for (i=0; i<txt.length; i++)
	{
		c = txt.charAt(i);
		if ("0123456789".indexOf(c,0) < 0)
		{
			alert(koumoku + "に数値以外が含まれてます");
			return false;
		}
	}
	return true;
}
function hissuchk(txt,koumoku)
{
<!--必須チェック
 if (txt=="")
	{
		alert(koumoku + "を入力してください");
		return false;
	}
	return true;
}
function zeroUme(num,len){
	var sa = len - (num+"").length
	var add0 = ""
	if( sa > 0 ) for ( i=0;i<sa;i++ ){ add0 += "0" }
	return ( add0 + num )
}
</script>
<?PHP
}
?>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
</FORM>
</body>
</html>