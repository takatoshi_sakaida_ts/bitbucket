<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取進捗</title>
</head>
<body bgcolor="#ffffff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}

function hyoujipage($res,$keyti)
{
//検索結果の表示
print "<strong><BR>計算結果不要・必要別計算進捗　". substr($keyti,0,4). "年".substr($keyti,4,2)."月"."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=70>査定日</td><td>査定箱数</td><td>査定点数</td><td nowrap width=100>計算結果不要査定箱数</td><td nowrap width=100>計算結果不要査定点数</td><td nowrap width=100>計算結果必要査定箱数</td><td nowrap width=100>計算結果必要査定点数</td></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
//査定日
print "<td>".$row[0] ."</td>";
//査定数
print "<td>". $row[1] ."</td>";
//未承認数
print "<td>".$row[2] ."</td>";
//承認数
print "<td>".$row[3] ."</td>";
//計算結果不要未承認数
print "<td>".$row[4] ."</td>";
//計算結果必要未承認数
print "<td>".$row[5] ."</td>";
//計算結果不要承認数
print "<td>".$row[6] ."</td>";
//計算結果必要承認数
print "</tr>";
}
print "</table>";
return;
}
//---------------------------------------------------------------------------------------------
//ここから実行部
//---------------------------------------------------------------------------------------------
//５ヶ月前
$keyti= date("Ym",mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")));
$sql = "select a.ass_dt,totalbox,totalsuu ,nobox,nosuu,yesbox,yessuu from ".
"(select ass_dt,sum(box_arrived) as totalbox,sum(valid_item_cnt) as totalsuu from tsellassessment where substr(ass_dt,1,6)='" .$keyti."' group by ass_dt) a, ".
"(select ass_dt,sum(a.box_arrived) as nobox,sum(valid_item_cnt) as nosuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='0' group by ass_dt) b, ".
"(select ass_dt,sum(a.box_arrived) as yesbox,sum(valid_item_cnt) as yessuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='1' group by ass_dt) c ".
"where a.ass_dt=b.ass_dt and a.ass_dt=c.ass_dt order by a.ass_dt";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
hyoujipage($res,$keyti);
//データの開放
$res->free();
//４ヶ月前
$keyti= date("Ym",mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")));
$sql = "select a.ass_dt,totalbox,totalsuu ,nobox,nosuu,yesbox,yessuu from ".
"(select ass_dt,sum(box_arrived) as totalbox,sum(valid_item_cnt) as totalsuu from tsellassessment where substr(ass_dt,1,6)='" .$keyti."' group by ass_dt) a, ".
"(select ass_dt,sum(a.box_arrived) as nobox,sum(valid_item_cnt) as nosuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='0' group by ass_dt) b, ".
"(select ass_dt,sum(a.box_arrived) as yesbox,sum(valid_item_cnt) as yessuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='1' group by ass_dt) c ".
"where a.ass_dt=b.ass_dt and a.ass_dt=c.ass_dt order by a.ass_dt";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
hyoujipage($res,$keyti);
//データの開放
$res->free();
//３ヶ月前
$keyti= date("Ym",mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")));
$sql = "select a.ass_dt,totalbox,totalsuu ,nobox,nosuu,yesbox,yessuu from ".
"(select ass_dt,sum(box_arrived) as totalbox,sum(valid_item_cnt) as totalsuu from tsellassessment where substr(ass_dt,1,6)='" .$keyti."' group by ass_dt) a, ".
"(select ass_dt,sum(a.box_arrived) as nobox,sum(valid_item_cnt) as nosuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='0' group by ass_dt) b, ".
"(select ass_dt,sum(a.box_arrived) as yesbox,sum(valid_item_cnt) as yessuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='1' group by ass_dt) c ".
"where a.ass_dt=b.ass_dt and a.ass_dt=c.ass_dt order by a.ass_dt";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
hyoujipage($res,$keyti);
//データの開放
$res->free();
//---------------------------------------------------------------------------------------------
//２ヶ月前
$keyti= date("Ym",mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")));
$sql = "select a.ass_dt,totalbox,totalsuu ,nobox,nosuu,yesbox,yessuu from ".
"(select ass_dt,sum(box_arrived) as totalbox,sum(valid_item_cnt) as totalsuu from tsellassessment where substr(ass_dt,1,6)='" .$keyti."' group by ass_dt) a, ".
"(select ass_dt,sum(a.box_arrived) as nobox,sum(valid_item_cnt) as nosuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='0' group by ass_dt) b, ".
"(select ass_dt,sum(a.box_arrived) as yesbox,sum(valid_item_cnt) as yessuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='1' group by ass_dt) c ".
"where a.ass_dt=b.ass_dt and a.ass_dt=c.ass_dt order by a.ass_dt";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
hyoujipage($res,$keyti);
//データの開放
$res->free();
//---------------------------------------------------------------------------------------------
//１ヶ月前
$keyti= date("Ym",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
$sql = "select a.ass_dt,totalbox,totalsuu ,nobox,nosuu,yesbox,yessuu from ".
"(select ass_dt,sum(box_arrived) as totalbox,sum(valid_item_cnt) as totalsuu from tsellassessment where substr(ass_dt,1,6)='" .$keyti."' group by ass_dt) a, ".
"(select ass_dt,sum(a.box_arrived) as nobox,sum(valid_item_cnt) as nosuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='0' group by ass_dt) b, ".
"(select ass_dt,sum(a.box_arrived) as yesbox,sum(valid_item_cnt) as yessuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='1' group by ass_dt) c ".
"where a.ass_dt=b.ass_dt and a.ass_dt=c.ass_dt order by a.ass_dt";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
hyoujipage($res,$keyti);
//データの開放
$res->free();
//---------------------------------------------------------------------------------------------
//当月
$keyti= date("Ym",mktime(0, 0, 0, date("m"), date("d"),   date("Y")));
$sql = "select a.ass_dt,totalbox,totalsuu ,nobox,nosuu,yesbox,yessuu from ".
"(select ass_dt,sum(box_arrived) as totalbox,sum(valid_item_cnt) as totalsuu from tsellassessment where substr(ass_dt,1,6)='" .$keyti."' group by ass_dt) a, ".
"(select ass_dt,sum(a.box_arrived) as nobox,sum(valid_item_cnt) as nosuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='0' group by ass_dt) b, ".
"(select ass_dt,sum(a.box_arrived) as yesbox,sum(valid_item_cnt) as yessuu from tsellassessment a,tsell r where substr(ass_dt,1,6)='" .$keyti."' and a.sell_no=r.sell_no and note_yn='1' group by ass_dt) c ".
"where a.ass_dt=b.ass_dt and a.ass_dt=c.ass_dt order by a.ass_dt";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
hyoujipage($res,$keyti);
//データの開放
$res->free();

$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>