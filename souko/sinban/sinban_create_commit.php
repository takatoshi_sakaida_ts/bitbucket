<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>新番号発行配完入力処理完了</title>
</head>
<body bgcolor="#ccccff">
<strong>新番号発行配完入力処理完了</strong>
<br><br>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_GET["receptioncode"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "買取受付番号を入力してください！！<br>";
	print "実行ボタンの押下は無効です。<br>";
} else {
	//ログイン情報の読み込み
	require_once("../parts/login_souko.php");
	//登録済みチェック
	$sql = "select ASSCODE from D_BOL_SGARRIVAL where ASSCODE = '".$keyti."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	$check = 0;
	while($row = $res->fetchRow()){
		$check = 1;
	}
	if($check == 0) {
		//D_BOL_SGARRIVAL を作成
		$sql = "insert into D_BOL_SGARRIVAL ";
		$sql = $sql . "(IMP_DATE, IMP_NUM, ASSCODE, QTY, MODIFYDATE, MODIFYTIME ";
		$sql = $sql . ") VALUES ( ";
		$sql = $sql . "TO_CHAR(SYSDATE-1, 'yyyymmdd'), ";
		$sql = $sql . "(select max(IMP_NUM)+1 from D_BOL_SGARRIVAL where IMP_DATE = TO_CHAR(SYSDATE-1, 'yyyymmdd')), ";
		$sql = $sql . "'".$keyti."', ";
		$sql = $sql . "1, ";
		$sql = $sql . "TO_CHAR(SYSDATE, 'yyyymmdd'), ";
		$sql = $sql . "TO_CHAR(SYSDATE, 'HH24MISS'))";
		$res = $db->query($sql);
		if(DB::isError($res)){
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
		$sql = "select distinct r.asscode, r.customername, b.status_kind ";
		$sql = $sql . "from d_ass b,d_ass_request r,d_ass_discard c, d_ass_ext e ";
		$sql = $sql . "where r.asscode = b.asscode(+) ";
		$sql = $sql . "and r.asscode = c.asscode(+) ";
		$sql = $sql . "and r.asscode = e.asscode(+) ";
		$sql = $sql . "and r.asscode ='".$keyti."' ";
		$sql = $sql . "and b.STATUS_KIND = '00' ";
		$sql = $sql . "and e.STATUS_KIND = '00'";
		$res = $db->query($sql);
		if(DB::isError($res)){
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
		$i=0;
		//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
		while($row = $res->fetchRow()){
			print "<table border=1>\n";
			print "<tr bgcolor=#ccffff>\n";
			//項目名の表示
			print "<td nowrap width=180>買取受付番号</td><td nowrap width=120>氏名</td><td nowrap width=200>状態</td></tr>";
			//項番
			print "<td>".$row[0] ."</td>";
			print "<td>".$row[1] ."</td>";
			print "<td>".Retkaitoristatuskind($row[2]) ."</td>";
			print "</tr></table>";
			print "<HR>";
			print "<font size=5 >新番号発行配完情報の作成が完了しました。</font><br>";
			$i++;
		}
		if($i == 0) {
			$sql = "select distinct r.asscode, r.customername, b.status_kind ";
			$sql = $sql . "from d_ass b,d_ass_request r,d_ass_discard c, d_ass_ext e ";
			$sql = $sql . "where r.asscode = b.asscode(+) ";
			$sql = $sql . "and r.asscode = c.asscode(+) ";
			$sql = $sql . "and r.asscode = e.asscode(+) ";
			$sql = $sql . "and r.asscode ='".$keyti."'";
			$res = $db->query($sql);
			if(DB::isError($res)){
				$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
			}
			$j=0;
			//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
			while($row = $res->fetchRow()){
				if($row[2] == null) {
					print "<table border=1>\n";
					print "<tr bgcolor=#ccffff>\n";
					//項目名の表示
					print "<td nowrap width=180>買取受付番号</td><td nowrap width=120>氏名</td><td nowrap width=200>状態</td></tr>";
					//項番
					print "<td>".$row[0] ."</td>";
					print "<td>".$row[1] ."</td>";
					print "<td>新番号発行</td>";
					print "</tr></table>";
					print "<HR>";
					print "<font size=5 >新番号発行配完情報の作成が完了しました。</font><br>";
					$j++;
				}
			}
			if($j == 0) {
				print "<table border=1>\n";
				print "<tr bgcolor=#ccffff>\n";
				//項目名の表示
				print "<td nowrap width=180>買取受付番号</td><td nowrap width=120>氏名</td><td nowrap width=200>状態</td></tr>";
				//項番
				print "<td>".$keyti ."</td>";
				print "<td>(倉庫側未登録)</td>";
				print "<td>新番号発行</td>";
				print "</tr></table>";
				print "<HR>";
				print "<font size=5 >新番号発行配完情報の作成が完了しました。</font><br>";
			}
		}
	} else {
		print "新番号発行配完入力可能な買取受付番号ではありません！！(登録済み)<br>";
		print "実行ボタンの押下は無効です。<br>";
	}
	//データの開放
	$res->free();
	$db->disconnect();
}

?>
<BR>
<FORM action="/souko/sinban/sinban_create_search.php"><INPUT TYPE="submit" VALUE="検索画面に戻る"></FORM>
</body>
</html>
