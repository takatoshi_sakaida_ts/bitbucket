<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>納品書印刷漏れ確認</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$sql = 
"SELECT DISTINCT RECEPTIONCLOSECODE, INVOICENO, RECEPTIONCODE, ".
"CASE ASSOCIATION_KIND ".
" WHEN '0' THEN '本店PC' ".
" WHEN '1' THEN '本店MB' ".
" WHEN '2' THEN '楽天' ".
" WHEN '3' THEN 'Yahoo' ".
" ELSE 'エラー' ".
"END ".
"FROM ( ".
"SELECT DISTINCT r.RECEPTIONCLOSECODE, r.RECEPTIONCODE, i.INVOICENO, SUM(DISTINCT(g.PICKED_KIND)) as piced, r.ASSOCIATION_KIND ".
"FROM D_RECEPTION r, D_RECEPTION_GOODS g, D_RECEPTION_INVOICENO i ".
"WHERE r.RECEPTIONCODE = g.RECEPTIONCODE ".
"AND r.RECEPTIONCODE = i.RECEPTIONCODE ".
"AND r.STATUS_KIND = '01' ".
"AND g.CANCEL_KIND = '0' ".
"AND i.ZONECODE != 'E' ".
"AND CONCAT(r.MODIFYDATE, r.MODIFYTIME) < TO_CHAR(SYSDATE - 1,'yyyymmddhh24miss') ".
"AND CONCAT(r.MODIFYDATE, r.MODIFYTIME) > TO_CHAR(SYSDATE - 60,'yyyymmddhh24miss') ".
"GROUP BY r.RECEPTIONCODE, r.RECEPTIONCLOSECODE, i.INVOICENO, r.ASSOCIATION_KIND ".
"ORDER BY r.RECEPTIONCLOSECODE, i.INVOICENO ".
") ".
"WHERE piced = '0' ".
"AND RECEPTIONCODE NOT IN ( ".
"SELECT r.RECEPTIONCODE ".
"FROM D_RECEPTION r, D_RECEPTION_GOODS g, D_RECEPTION_INVOICENO i ".
"WHERE r.RECEPTIONCODE = g.RECEPTIONCODE ".
"AND r.RECEPTIONCODE = i.RECEPTIONCODE ".
"AND r.STATUS_KIND = '01' ".
"AND g.CANCEL_KIND = '0' ".
"AND (i.ZONECODE = 'E' OR g.NEWUSED_KIND = '2') ".
"AND CONCAT(r.MODIFYDATE, r.MODIFYTIME) < TO_CHAR(SYSDATE - 1,'yyyymmddhh24miss') ".
"AND CONCAT(r.MODIFYDATE, r.MODIFYTIME) > TO_CHAR(SYSDATE - 60,'yyyymmddhh24miss') ".
"GROUP BY r.RECEPTIONCODE ".
") ".
"ORDER BY RECEPTIONCLOSECODE, INVOICENO, RECEPTIONCODE";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>印刷後24時間以上未ピッキング一覧</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=100 align=center>締番号</td>
	<td width=130 align=center>納品書印刷番号</td>
	<td width=150 align=center>注文番号</td>
	<td width=150 align=center>販売区分</td>
</tr>
<tr>
<?php

//取得データのセット
while($row = $res->fetchRow()){

    print "<td align=center>".$row[0]."</td>";
    print "<td align=center>".$row[1]."</td>";
    print "<td align=center>".$row[2]."</td>";
    print "<td align=center>".$row[3]."</td></tr>";
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
