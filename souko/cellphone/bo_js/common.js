//////////////////////////////////////////////////////////////////////////
//
// スワップイメージ
//
//////////////////////////////////////////////////////////////////////////

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


//////////////////////////////////////////////////////////////////////////
//
// PAGETOPへのするするリンク　不用品トップ専用
//
//////////////////////////////////////////////////////////////////////////


function backToTop() {
  var x1 = x2 = x3 = 0;
  var y1 = y2 = y3 = 0;
  if (document.documentElement) {
      x1 = document.documentElement.scrollLeft || 0;
      y1 = document.documentElement.scrollTop || 0;
  }
  if (document.body) {
      x2 = document.body.scrollLeft || 0;
      y2 = document.body.scrollTop || 0;
  }
  x3 = window.scrollX || 0;
  y3 = window.scrollY || 0;
  var x = Math.max(x1, Math.max(x2, x3));
  var y = Math.max(y1, Math.max(y2, y3));
  window.scrollTo(Math.floor(x / 2), Math.floor(y / 2));
  if (x > 0 || y > 0) {
      window.setTimeout("backToTop()", 15);
  }
}

//////////////////////////////////////////////////////////////////////////
//
//ポップアップ
//
//////////////////////////////////////////////////////////////////////////

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


//////////////////////////////////////////////////////////////////////////
//
//IEで透過PNG
//
//////////////////////////////////////////////////////////////////////////

var ua = navigator.userAgent;

function correctPNG() {
	if (ua.match(/MSIE (\d\.\d+)/)) {
		for(var i=0; i<document.images.length; i++) {
				var img = document.images[i]
				var imgName = img.src.toUpperCase()
				if (imgName.substring(imgName.length-3, imgName.length) == "PNG") {
					var imgID = (img.id) ? "id='" + img.id + "' " : ""
					var imgClass = (img.className) ? "class='" + img.className + "' " : ""
					var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
					var imgStyle = "display:inline-block;" + img.style.cssText 
					if (img.align == "left") imgStyle = "float:left;" + imgStyle
					if (img.align == "right") imgStyle = "float:right;" + imgStyle
					if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle    
					var strNewHTML = "<span " + imgID + imgClass + imgTitle
						+ " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
						+ "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
						+ "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>" 
					img.outerHTML = strNewHTML
					i = i-1
				}
			}
	}
}

if (ua.match(/MSIE (\d\.\d+)/)) {
window.attachEvent("onload", correctPNG); // run this function at page load
}
//////////////////////////////////////////////////////////////////////////
//
//URI移動
//
//////////////////////////////////////////////////////////////////////////

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

//////////////////////////////////////////////////////////////////////////
//
//ジャンプメニュー
//
//////////////////////////////////////////////////////////////////////////

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

//////////////////////////////////////////////////////////////////////////
//
//コピーライト自動更新
//
//////////////////////////////////////////////////////////////////////////


function copyright(){ 
	var path = "/";
	var day=new Date();
  var year=day.getFullYear();
  return document.write("<img src=\""+path+"img/shared/copyright_"+year+".gif\" alt=\"copyright&copy;"+year+" CleanCrew ALL rights reserved.\" width=\"220\" height=\"12\">");
}

//////////////////////////////////////////////////////////////////////////
//
//　FLASH　読み込み
//　v1.0
//　Copyright 2006 Adobe Systems, Inc. All rights reserved.
//
//////////////////////////////////////////////////////////////////////////

function AC_AddExtension(src, ext)
{
  if (src.indexOf('?') != -1)
    return src.replace(/\?/, ext+'?'); 
  else
    return src + ext;
}

function AC_Generateobj(objAttrs, params, embedAttrs) 
{ 
  var str = '<object ';
  for (var i in objAttrs)
    str += i + '="' + objAttrs[i] + '" ';
  str += '>';
  for (var i in params)
    str += '<param name="' + i + '" value="' + params[i] + '" /> ';
  str += '<embed ';
  for (var i in embedAttrs)
    str += i + '="' + embedAttrs[i] + '" ';
  str += ' ></embed></object>';

  document.write(str);
}

function AC_FL_RunContent(){
  var ret = 
    AC_GetArgs
    (  arguments, ".swf", "movie", "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
     , "application/x-shockwave-flash"
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}

function AC_SW_RunContent(){
  var ret = 
    AC_GetArgs
    (  arguments, ".dcr", "src", "clsid:166B1BCA-3F9C-11CF-8075-444553540000"
     , null
    );
  AC_Generateobj(ret.objAttrs, ret.params, ret.embedAttrs);
}

function AC_GetArgs(args, ext, srcParamName, classid, mimeType){
  var ret = new Object();
  ret.embedAttrs = new Object();
  ret.params = new Object();
  ret.objAttrs = new Object();
  for (var i=0; i < args.length; i=i+2){
    var currArg = args[i].toLowerCase();    

    switch (currArg){	
      case "classid":
        break;
      case "pluginspage":
        ret.embedAttrs[args[i]] = args[i+1];
        break;
      case "src":
      case "movie":	
        args[i+1] = AC_AddExtension(args[i+1], ext);
        ret.embedAttrs["src"] = args[i+1];
        ret.params[srcParamName] = args[i+1];
        break;
      case "onafterupdate":
      case "onbeforeupdate":
      case "onblur":
      case "oncellchange":
      case "onclick":
      case "ondblClick":
      case "ondrag":
      case "ondragend":
      case "ondragenter":
      case "ondragleave":
      case "ondragover":
      case "ondrop":
      case "onfinish":
      case "onfocus":
      case "onhelp":
      case "onmousedown":
      case "onmouseup":
      case "onmouseover":
      case "onmousemove":
      case "onmouseout":
      case "onkeypress":
      case "onkeydown":
      case "onkeyup":
      case "onload":
      case "onlosecapture":
      case "onpropertychange":
      case "onreadystatechange":
      case "onrowsdelete":
      case "onrowenter":
      case "onrowexit":
      case "onrowsinserted":
      case "onstart":
      case "onscroll":
      case "onbeforeeditfocus":
      case "onactivate":
      case "onbeforedeactivate":
      case "ondeactivate":
      case "type":
      case "codebase":
        ret.objAttrs[args[i]] = args[i+1];
        break;
      case "width":
      case "height":
      case "align":
      case "vspace": 
      case "hspace":
      case "class":
      case "title":
      case "accesskey":
      case "name":
      case "id":
      case "tabindex":
        ret.embedAttrs[args[i]] = ret.objAttrs[args[i]] = args[i+1];
        break;
      default:
        ret.embedAttrs[args[i]] = ret.params[args[i]] = args[i+1];
    }
  }
  ret.objAttrs["classid"] = classid;
  if (mimeType) ret.embedAttrs["type"] = mimeType;
  return ret;
}

//////////////////////////////////////////////////////////////////////////
//
//エリア表示切り替え
//
//////////////////////////////////////////////////////////////////////////

function disp_ken(){
	if(document.aria.ken.value=="tokyo_area"){	
		document.getElementById("aria_tokyo").style.display = "block";
		document.getElementById("aria_kanagawa").style.display = "none";
	}else if(document.aria.ken.value=="kanagawa_area"){	
		document.getElementById("aria_tokyo").style.display = "none";
		document.getElementById("aria_kanagawa").style.display = "block";
	}else{	
		document.getElementById("aria_tokyo").style.display = "none";
		document.getElementById("aria_kanagawa").style.display = "none";
	}
}

//////////////////////////////////////////////////////////////////////////
//
//FAQ
//
//////////////////////////////////////////////////////////////////////////

var ua = navigator.userAgent;

function answerHide(){
	$("p.answer").css("display","none");
	//$("p.qBtn").css("display","block");
}


function answerView(id){
	var answer = "ANS"+id;
	//var qBtn = "Q"+id;
	var display = $("#"+answer).css("display");
	//var btn = $("#"+qBtn).css("background-image");
	//var rollBtn = btn.replace(/(\.gif|\.jpg|\.png)/, "_o$1");
	//var stayBtn = btn.replace("_o", "");

	if(ua.match(/MSIE 7/)) {
		if(display == "block") {
		$("#"+answer).slideUp("fast");
		//$("#"+qBtn).css("background-image",stayBtn);
		}
		else {
		$("#"+answer).slideDown("fast");
		//$("#"+qBtn).css("background-image",rollBtn);
		}
	}
	else if (ua.match(/MSIE (\d\.\d+)/)) {
		if(display == "block") {
		$("#"+answer).hide();
		//$("#"+qBtn).css("background-image",stayBtn);
		}
		else {
		$("#"+answer).show();
		//$("#"+qBtn).css("background-image",rollBtn);
		}
	}
	else {
		if(display == "block") {
		$("#"+answer).slideUp("fast");
		//$("#"+qBtn).css("background-image",stayBtn);
		}
		else {
		$("#"+answer).slideDown("fast");
		//$("#"+qBtn).css("background-image",rollBtn);
		}
	}

}

//////////////////////////////////////////////////////////////////////////
//
//フォームオプション項目
//
//////////////////////////////////////////////////////////////////////////

var ua = navigator.userAgent;

function optionHide(){
	$("#formOption").css("display","none") ;
	$("#optionBtn").css("display","block");
}

function formOption(){
	var display = $("#formOption").css("display");
	
		if(display == "block") {
		$("#formOption").hide();
		}
		else {
		$("#formOption").show();
		}

}

