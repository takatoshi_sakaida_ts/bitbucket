﻿<?php
$message = "名前：" . $_POST["onamae"] . "\nお問合せ内容：" . $_POST["naiyou"];
if (!mb_send_mail("taira@bookoffonline.jp", $_POST["sentaku"], $message, "From: " . $_POST["sadd"])) {
  exit("error");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" dir="ltr">

<head>
<title>お問合せ | ブックオフの携帯電話　宅配買取サービス</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="ブックオフの携帯電話　宅配買取サービス">
<meta name="keywords" content="宅配，買取，携帯電話，スマートフォン，買い取り">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="shortcut icon" href="http://www.bookoffonline.co.jp/common/files/favicon.ico">
<link rel="stylesheet" href="bo_css/import.css" type="text/css">
<link rel="stylesheet" href="bo_css/cell_style.css" type="text/css" media="all">
<link rel="stylesheet" href="bo_css/mail_form.css" type="text/css" media="all">
<script type="text/javaScript" language="JavaScript" src="bo_js/rank.js"></script>
<link rel="canonical" href="http://www.bookoff.co.jp/cellphone/inquiry.html">
</head>

<body>
<!--main-->
<div id="main">
	<div class="SellHeader">
		<h1>ブックオフの携帯電話 宅配買取サービス</h1>
		<p id="Selltxt">ブックオフでは自宅にいながら携帯電話・スマートフォンをお売りいただける、宅配買取サービスを行っております<br>送料・手数料0円、個人情報もデータクリーニング処理で安心してご利用いただけます</p>
	</div>
<!--header-->
<div id="header">
	<div class="logo"><a href="http://www.bookoff.co.jp"><img src="bo_img/boc_logo.gif" alt="ブックオフ" /></a></div>
</div>
<!--//header//-->

<!--contents-->
<div id="contents">
	<div id="naviFrame">
		<ol>
			<li><a href="http://www.bookoff.co.jp">トップ</a>＞</li>
			<li><a href="index.html">携帯電話宅配買取サービス ご利用の流れ</a>＞</li>
			<li>お問合せ</li>
		</ol>
	</div>
	<div id="wrapper">
		<div id="globalNavi_full">
			<ul>
				<li id="navi01"><a href="index.html">ご利用の流れ</a></li>
				<li id="navi02"><a href="list.html">買取価格表</a></li>
				<li id="navi03"><a href="check.html">発送前チェック</a></li>
				<li id="navi04"><a href="faq.html">よくある質問</a></li>
			</ul>
		</div><!--//globalNavi_full//-->
		<div id="privacy_main">
			<h2>お問合せ（完了）</h2>
			<div id="m_finish">
				<div class="finish_b_txt"><b>お問合せありがとうございました。</b></div>
				<div class="finish_txt">
					お問合せ内容を送信いたしました。<br>当配送センターより返答させていただきます。<br>
					今後ともご愛顧賜りますようよろしくお願い申し上げます。
				</div>
				<div class="finish_txt">ブックオフロジスティクス株式会社</div>
			</div>
		</div><!--//privacy_main//-->
	</div><!--//wrapper//-->
</div>
<!--//contents//-->

<!--related_links-->
<div id="rel_link">
	<div class="link_r"><img width="10" height="10" src="bo_img/top.gif" complete="complete"/><a href="">このページの先頭にもどる</a></div>
</div>
<!--//related_links//-->

<!--footer-->
<div id="footer" class="clr">
	<div class="fotlink">
			<div id="helpBox">
				<div id="ttlHelp">ヘルプ＆ガイド</div>
				<ul id="helpLeft">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="index.html" target="_self">ご利用の流れ</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="list.html#standard" target="_self">買取基準</a></li>
				</ul>
				<ul id="helpRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="list.html#price" target="_self">買取価格表</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="check.html" target="_self">発送前チェック</a></li>
				</ul>
				<ul id="helpRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="faq.html" target="_self">よくある質問</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="inquiry.html" target="_self">お問合せ</a></li>
				</ul>
			</div>
			<div id="serviceBox">
				<div id="ttlservice">携帯電話 宅配買取サービス ご利用にあたって</div>
				<ul id="serviceLeft">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="terms.html" target="_self">利用規約</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="corp_prof.html" target="_self">会社概要</a></li>
				</ul>
				<ul id="serviceRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="policy.html" target="_self">プライバシーポリシー</a></li>
				</ul>
			</div>
	</div>
	<div id="number">神奈川県公安委員会 第452780002911号　ブックオフロジスティクス株式会社</div>
	<div id="copyright">Copyright(C) BOOKOFF LOGISTICS CORPORATION. All rights Reserved.</div>
</div>
<!--//footer//-->
</div>
<!--//main//-->

</body>
</html>
