<?php
//PEARの利用     -------(1)
require_once("DB.php");
require_once("Log.php");

$logger = Log::factory('file','./display_cancel.log','yahooauc');

//POST値を取得
$instorecode = $_POST["iscd"];

//print_r($_POST);

// POST値が存在するかチェック
if (empty($instorecode)) {
	print '<FORM action="search.php"><INPUT TYPE="submit" VALUE="検索画面に戻る"></FORM>';
	die("POST値が不正です。");
}

// データ更新sql
$updateSql = "update TYAHOOAUC_DISPLAY set DISPLAY_STAT = '08', DISPLAY_CNCL_OP_DM = to_char(sysdate,'yyyymmddhh24miss'), ERR_FLG = '0' where INSTORECODE = ?";

// 更新データ
$data = $instorecode;
//print_r($data);

// 設定ファイル読込
$ini = parse_ini_file("../../parts/db.ini",true);

// ECDBコネクション取得
$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
//$dsn_ec = "oci8://". $ini['ecd']['usr'] . ":" . $ini['ecd']['pwd'] . "@" . $ini['ecd']['dbn'];
//データベースへの接続
$ecdb = DB::connect($dsn_ec);
if(DB::isError($ecdb)){
	die($ecdb->getDebugInfo());
}
$ecdb->autoCommit(false);

// ECDB:データ更新
$res = $ecdb->query($updateSql,$data);
if(DB::isError($res)){
    $ecdb->rollback();
	die("更新に失敗しました。" . $res->getDebugInfo());
}

$ecdb->commit();
//$ecdb->rollback();

$ecdb->disconnect();
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<link rel="stylesheet" href="../../common/pure/pure-min.css">
<title>【ヤフオク】オークション取消し</title>

<style>
body {
    font-family: "Raleway", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 10px;
}
</style>
</head>
 
<body>

<p>インストアコード:<?php echo $instorecode; ?></p>

<p>出品取消指示を行いました。<br>しばらく時間をおいて、オークションサイトから取り消されていることを確認してください。</p>
<button type="button" class="pure-button" onclick="location.href='/'">トップに戻る</button>

</body>
</html>