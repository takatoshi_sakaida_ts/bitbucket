<?php
require '../../parts/pagechk.inc';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<link rel="stylesheet" href="../../common/pure/pure-min.css">
<title>【ヤフオク】オークション取消し</title>

<style>
body {
    font-family: "Raleway", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 10px;
}

img.goodsimage{
    width:auto;
    height:auto;
    max-width:300px; 
    max-height:500px; 
    vertical-align: middle;/*IE7*/
}

.button-warning {
    color: white;
    background: rgb(202, 60, 60);
}

</style>
</head>
 
<body>
<?php
//Pearライブラリ読み込み
require_once("DB.php");
require_once("Log.php");

$logger = Log::factory('file','./display_cancel.log','yahooauc');
//$logger->log("これはテストです");
$logger->info("[" .$_SESSION['user_name'] . "] Accessed.");

$DISPLAY_STAT = array(
    '00'=>'未出品（出品取消済み）',
    '02'=>'出品指示',
    '04'=>'出品要求中（USJ）',
    '06'=>'出品中（ヤフオク）',
    '08'=>'出品取消指示',
    '10'=>'出品取消要求中（USJ）'
);

$ERR_FLG = array(
    '0'=>'初期値・未処理',
    '1'=>'エラーあり',
    '2'=>'正常終了',
    '9'=>'処理中'
);

// 検索キーワードを取得
$INSTORECODE = (isset($_POST['iscd']) && $_POST['iscd'] != '') ? $_POST['iscd'] : null;
$JAN_CD = (isset($_POST['jan']) && $_POST['jan'] != '') ? $_POST['jan'] : null;

$sql_search = null;
$data = null;
if(!is_null($INSTORECODE)){
    $sql_search = "select INSTORECODE, JAN_CD, TITLE, IMAGE1, DISPLAY_STAT, ERR_MEMO, ERR_FLG from TYAHOOAUC_DISPLAY where INSTORECODE = ? ";
    $data = $INSTORECODE;
} elseif(!is_null($JAN_CD)){
    $sql_search = "select INSTORECODE, JAN_CD, TITLE, IMAGE1, DISPLAY_STAT, ERR_MEMO, ERR_FLG from TYAHOOAUC_DISPLAY where JAN_CD = ? ";
    $data = $JAN_CD;
} else{
    die('検索キーワードが不正です。<br><a href="search.php" class="pure-button">戻る</a>');
}

$ini = parse_ini_file("../../parts/db.ini",true);
$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
//$dsn_ec = "oci8://". $ini['ecd']['usr'] . ":" . $ini['ecd']['pwd'] . "@" . $ini['ecd']['dbn'];
$db = DB::connect($dsn_ec);
if(DB::isError($db)){
	die($db->getDebugInfo());
}
$db->autoCommit(false);

$res = $db->query($sql_search,$data);
if(DB::isError($res)){
	die("クエリの実行に失敗しました。" . $res->getDebugInfo());
}

$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
if(is_null($row)){
    die('検索結果が存在しません。<br><a href="search.php" class="pure-button">戻る</a>');
} else{
?>

<div class="container">
    <p>【出品情報】</p>
    <table class="pure-table pure-table-bordered">

        <tbody>
            <tr>
                <td>画像</td>
                <td><img class="goodsimage" src="<?php echo $row['IMAGE1']; ?>" alt="商品画像" style="max-width:100px; max-height:200px;"></td>
            </tr>
            <tr>
                <td>インストアコード</td>
                <td><?php echo $row['INSTORECODE']; ?></td>
            </tr>
            <tr>
                <td>JANコード</td>
                <td><?php echo $row['JAN_CD']; ?></td>
            </tr>
            <tr>
                <td>タイトル</td>
                <td><?php echo $row['TITLE']; ?></td>
            </tr>
            <tr>
                <td>出品ステータス</td>
                <td><?php echo $DISPLAY_STAT[$row['DISPLAY_STAT']]; ?></td>
            </tr>
            <tr>
                <td>USJ-IFフラグ</td>
                <td><?php echo $ERR_FLG[$row['ERR_FLG']]; ?></td>
            </tr>
        </tbody>
    </table>

<?php if($row['ERR_FLG'] == '1' ): ?>
    <p>出品情報にエラーが存在します。内容は以下の通りです。<br>[MESSAGE]:<br><font color="red"><pre><?php echo htmlspecialchars($row['ERR_MEMO']); ?></pre></font></p>
    <p>システム部までご連絡下さい。</p>
    <button type="button" class="pure-button" onclick="location.href='./search.php'">戻る</button>
<?php elseif($row['DISPLAY_STAT'] == '00'): ?>
    <p>出品ステータスが未出品（出品取消し済み）です。 <br>オークションが出品状態となっている場合、連携不備が発生している可能性があります。<br>ストア管理画面より取消を行ってください。</p>
    <button type="button" class="pure-button" onclick="location.href='./search.php'">戻る</button>
<?php elseif($row['DISPLAY_STAT'] == '02' ): ?>
    <p>現在出品処理中です。<br>出品完了を確認してから再度取消を行ってください。</p>
    <button type="button" class="pure-button" onclick="location.href='./search.php'">戻る</button>
<?php elseif($row['DISPLAY_STAT'] == '08' || $row['DISPLAY_STAT'] == '10' ): ?>
    <p>現在出品取消し中です。 <br>しばらくたっても取り消されない場合はシステム部までご連絡下さい。</p>
    <button type="button" class="pure-button" onclick="location.href='./search.php'">戻る</button>
<?php else: ?>
    <form action="update.php" method="post" class="pure-form pure-form-stacked" onsubmit="return window.confirm('出品が取り消されます。よろしいですか？')">
        <fieldset>
            <input type="hidden" name="iscd" value="<?php echo $row['INSTORECODE']; ?>">
            <button type="button" class="pure-button" onclick="location.href='./search.php'">戻る</button>
            <button type="submit" class="button-warning pure-button">オークション取消</button>
        </fieldset>
    </form>
<?php endif; ?>

</div>

<?php
}

$db->disconnect();

?>


</body>
</html>