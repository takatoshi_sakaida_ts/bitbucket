<?php
require '../../parts/pagechk.inc';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<link rel="stylesheet" href="../../common/pure/pure-min.css">
<title>【ヤフオク】オークション取消し</title>

<style>
body {
    font-family: "Raleway", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin: 10px;
}
</style>
</head>
 
<body>
<?php
//Pearライブラリ読み込み
require_once("DB.php");
require_once("Log.php");

$logger = Log::factory('file','./display_cancel.log','search');
?>

<form action="search_result.php" method="post" class="pure-form pure-form-stacked">
    <fieldset>
        <legend>取り消したい商品のインストアコード、もしくはJANコードを入力してください。<br>
        ※どちらか一方でOKです。
    </legend>

        <label for="iscd">インストアコード</label>
        <input id="iscd" type="text" name="iscd">

        <label for="jan">JANコード</label>
        <input id="jan" type="text" name="jan">
        
        <button type="button" class="pure-button" onclick="location.href='/'">戻る</button>
        <button type="submit" class="pure-button pure-button-primary">検索</button>
    </fieldset>
</form>

</body>
</html>