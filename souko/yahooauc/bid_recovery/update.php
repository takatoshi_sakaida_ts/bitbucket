<?php
//PEARの利用     -------(1)
require_once("DB.php");

//全画面からのPOST値を取得
$sid = $_POST["sid"];
$aid = $_POST["aid"];
$eml = $_POST["eml"];

//print_r($_POST);

// POST値が存在するかチェック
if (empty($sid)||empty($aid)||empty($eml)) {
	print '<FORM action="view.php"><INPUT TYPE="submit" VALUE="リストに戻る"></FORM>';
	die("POST値が不正です。");
}

// データ更新sql
$updateSql = "update TYAHOOAUC_BID set EMAIL = ?, UPD_DM = ?, ERR_FLG = '0' where aid = ? ";

// 更新データ
$data = array($eml,date("YmdHis"),$aid);
//print_r($data);

// 設定ファイル読込
$ini = parse_ini_file("../../parts/db.ini",true);

// ECDBコネクション取得
$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
//$dsn_ec = "oci8://". $ini['ecd']['usr'] . ":" . $ini['ecd']['pwd'] . "@" . $ini['ecd']['dbn'];
//データベースへの接続
$ecdb = DB::connect($dsn_ec);
if(DB::isError($ecdb)){
	die($ecdb->getMessage());
}
$ecdb->autoCommit(false);

// ECDB:データ更新
$res = $ecdb->query($updateSql,$data);
if(DB::isError($res)){
    $ecdb->rollback();
	die("更新に失敗しました。" . $res->getDebugInfo());
}

$ecdb->commit();
//$ecdb->rollback();

$ecdb->disconnect();

// ページリダイレクト
header("Location: view.php");

