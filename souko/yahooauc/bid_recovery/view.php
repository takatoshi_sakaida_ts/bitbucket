<?php
require '../../parts/pagechk.inc';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="../../common/pure/pure-min.css">
<title>【ヤフオク】落札リスト</title>

<style>
body {
  font-family: "Raleway", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;}
</style>
</head>
 
<body>
<?php
//Pearライブラリ読み込み
require_once("DB.php");
require_once("Log.php");


$logger = Log::factory('file','./test.log','yahooauc');
//$logger->log("これはテストです");
$logger->info("[" .$_SESSION['user_name'] . "] Accessed.");

$sql_getErrList = "select SID, AID, YID, WID, EMAIL, PRICE, QUANTITY, QUANTITY_TOTAL, INSTORECODE, REG_DM, UPD_DM, ERR_MEMO, ERR_FLG from tyahooauc_bid where ERR_FLG in ('0','1')";
//$sql_getErrList = "select SID, AID, YID, WID, EMAIL, PRICE, QUANTITY, QUANTITY_TOTAL, INSTORECODE, REG_DM, UPD_DM, ERR_MEMO, ERR_FLG from tyahooauc_bid where wid = 'lisagas516'";

$ini = parse_ini_file("../../parts/db.ini",true);

$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
//$dsn_ec = "oci8://". $ini['ecd']['usr'] . ":" . $ini['ecd']['pwd'] . "@" . $ini['ecd']['dbn'];
$db = DB::connect($dsn_ec);
if(DB::isError($db)){
	die($db->getDebugInfo());
}
$db->autoCommit(false);

function getErrLevel($errInfo){
    $mailErrRegex = "メールアドレス形式が不正です";
    $exhibitErrRegex = "出品情報が不正です";
    
    if(strpos($errInfo,$mailErrRegex)){
        return 1;
    } elseif(strpos($errInfo,$exhibitErrRegex)){
        return 2;
    } else{
        return 3;
    }
}

?>

<div class="wrapper">

<table class="pure-table pure-table-bordered">
<thead>
    <tr>
        <th>オークションID</th>
        <th>落札者ID</th>
        <th>メールアドレス</th>
        <th>インストアコード</th>
        <th>エラー情報</th>
        <th>登録日時</th>
        <th></th>
    </tr>
</thead>

<tbody>
<?php 
    $res = $db->query($sql_getErrList);
    if(DB::isError($res)){
    die($res->getMessage());
    }
    $count = 0;
    while($err = $res->fetchRow(DB_FETCHMODE_ASSOC)){
?>
        <div id="<?php echo $err['AID'];?>">
            <tr id="data_<?php echo $count++;?>">
            <td id="aid"><?php echo $err['AID'];?></td>
            <td id="wid"><?php echo $err['WID'];?></td>
            <td id="email">
                <div class="emlViewMode">
                    <?php echo $err['EMAIL'];?>
                </div>
                <div class="emlEditMode" style="display:none;">
                    <input type="text" class="" name="email" value="<?php echo $err['EMAIL']; ?>" style="width:200px;">
                </div>
            </td>
            <td id="instorecode"><?php echo $err['INSTORECODE'];?></td>
            <td id="err_memo"><?php echo $err['ERR_MEMO'];?></td>
            <td id="reg_dm"><?php echo $err['REG_DM'];?></td>
            <?php if($err['ERR_FLG'] == '1'):?>
                <?php if(getErrLevel($err['ERR_MEMO']) == '1'):?>
                    <td><a class="pure-button" href="#" onclick="editEml('<?php echo $err['SID'];?>','<?php echo $err['AID'];?>','<?php echo $err['EMAIL'];?>');">修正して再取込</a></td>
                <?php else:?>
                    <td><font color="red">システム部に連絡</font></td>
                <?php endif; ?>
            <?php elseif($err['ERR_FLG'] == '0'):?>
                <td>取込中</td>
            <?php elseif($err['ERR_FLG'] == '2'):?>
                <td>取込済み</td>
            <?php endif; ?>
            </tr>
        </div>
<?php
    }
    $res->free();
?>

</tbody>

</table>


<div class="caption">
<p style="color:red;">
※メールアドレスを補正する際は、入力ミスのないように注意してください。
<p>
</div>

</div>

<script>

function editEml(sid, aid, currentEml){
    var inputVal = window.prompt("AID:" + aid + "\r\n" + "変更後のアドレスを入力してください",currentEml);
    console.log(inputVal);
    
    // 入力アドレスが元と同じ、または入力値が空の場合はアラート
    if(inputVal == currentEml){
        window.alert("アドレスを修正してください。");
    } else if(inputVal == ""){
        window.alert("アドレスが空です。");
    } else if(!inputVal.match(/^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/)){
        window.alert("アドレス形式が不正、もしくは使用できない文字が含まれています");
    } else{
        var sendObj = {'sid':sid, 'aid':aid, 'eml':inputVal};
        // updateに遷移
        execPost("update.php",sendObj);
    }
    
}


function execPost(action, data) {
 // フォームの生成
 var form = document.createElement("form");
 form.setAttribute("action", action);
 form.setAttribute("method", "post");
 form.style.display = "none";
 document.body.appendChild(form);
 // パラメタの設定
 if (data !== undefined) {
  for (var paramName in data) {
   var input = document.createElement('input');
   input.setAttribute('type', 'hidden');
   input.setAttribute('name', paramName);
   input.setAttribute('value', data[paramName]);
   form.appendChild(input);
  }
 }
 // submit
 form.submit();
}

</script>

</body>
</html>