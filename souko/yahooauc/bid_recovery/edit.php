<?php
require '../../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="../../common/pure/pure-min.css">
<title>【ヤフオク】落札リスト</title>

<style>
</style>
</head>
 
<body>
<?php
//Pearライブラリ読み込み
require_once("DB.php");
require_once("Log.php");


$logger = Log::factory('file','./test.log','yahooauc');
//$logger->log("これはテストです");
$logger->info("[" .$_SESSION['user_name'] . "] Accessed.");

$sql_getErrList = "select SID, AID, YID, WID, EMAIL, PRICE, QUANTITY, QUANTITY_TOTAL, INSTORECODE, REG_DM, UPD_DM, ERR_MEMO, ERR_FLG from tyahooauc_bid where ERR_FLG in ('0','1')";
//$sql_getErrList = "select SID, AID, YID, WID, EMAIL, PRICE, QUANTITY, QUANTITY_TOTAL, INSTORECODE, REG_DM, UPD_DM, ERR_MEMO, ERR_FLG from tyahooauc_bid where wid = 'lisagas516'";

$ini = parse_ini_file("../../parts/db.ini",true);

$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
$db = DB::connect($dsn_ec);
if(DB::isError($db)){
	die($db->getMessage());
}
$db->autoCommit(false);

function parseErrLevel($errInfo){
    
    $mailErrRegex = "";

}

?>

<div class="wrapper">

<table class="pure-table pure-table-bordered">
<thead>
<tr>
<th>オークションID</th>
<th>落札者ID</th>
<th>メールアドレス</th>
<th>インストアコード</th>
<th>エラー情報</th>
<th>登録日時</th>
<th></th>
</tr>
</thead>

<tbody>
<?php 
    $res = $db->query($sql_getErrList);
    if(DB::isError($res)){
    die($res->getMessage());
    }
    $count = 0;
    while($err = $res->fetchRow(DB_FETCHMODE_ASSOC)){
?>
        <div id="<?php echo $err['AID'];?>">
            <tr id="data_<?php echo $count++;?>">
            <td id="aid"><?php echo $err['AID'];?></td>
            <td id="wid"><?php echo $err['WID'];?></td>
            <td id="email">
                <div class="emlViewMode" style="display:block;">
                    <?php echo $err['EMAIL'];?>(<a class="emlEdit" href="#" onclick="eml_ChangeEditMode('<?php echo $err['AID'];?>');">編集</a>)
                </div>
                <div class="emlEditMode" style="display:none;">
                    <input type="text" class="" name="email" value="<?php echo $err['EMAIL']; ?>" style="width:200px;">
                </div>
            </td>
            <td id="instorecode"><?php echo $err['INSTORECODE'];?></td>
            <td id="err_memo"><?php echo $err['ERR_MEMO'];?></td>
            <td id="reg_dm"><?php echo $err['REG_DM'];?></td>
            <?php if($err['ERR_FLG'] == '1'):?>
                <td><a class="pure-button pure-button-disabled" href="#">修正して再取込</a></td>
            <?php elseif($err['ERR_FLG'] == '0'):?>
                <td><font color="red">取込中</font></td>
            <?php elseif($err['ERR_FLG'] == '2'):?>
                <td>取込済み</td>
            <?php endif; ?>
            </tr>
        </div>
<?php
    }
    $res->free();
?>

</tbody>

</table>

</div>

<script>

function eml_ChangeEditMode(elmId){
    var hoge =document.getElementById(elmId);
    console.debug(typeof(hoge));
}


</script>

</body>
</html>