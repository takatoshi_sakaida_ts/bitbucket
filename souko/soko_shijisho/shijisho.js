function searchCheck(){
	var workNumber = document.getElementById('workNumber').value;
	var workYear = document.getElementById('workYear').value;
	var workMonth = document.getElementById('workMonth').value;
	var workDay = document.getElementById('workDay').value;
	
	var ymd = '';
	
	var checkNumber = false;
	var checkYear = false;
	var checkMonth = false;
	var checkDay = false;
	
	var alertNumber = false;
	var alertYmd = false;
	
	if(workNumber){
		if(workNumber.match(/[^0-9]/g)){
			alertNumber = true;
			alert("作業番号に数値以外が含まれています");
		} else {
			checkNumber = true;
		}
	}
	
	if(workYear){
		if(workYear.match(/[^0-9]/g)){
			alertYmd = true;
			alert("作成年に数値以外が含まれています");
		} else {
			checkYear = true;
			ymd = ymd + workYear;
		}
	}
	
	if(workMonth){
		if(workMonth.match(/[^0-9]/g)){
			alertYmd = true;
			alert("作成月に数値以外が含まれています");
		} else {
			checkMonth = true;
			ymd = ymd + workMonth;
		}
	}
	
	if(workDay){
		if(workDay.match(/[^0-9]/g)){
			alertYmd = true;
			alert("作成日に数値以外が含まれています");
		} else {
			checkDay = true;
			ymd = ymd + workDay;
		}
	}
	
	var checkYmd = false;
	var ymdLength = ymd.length;
	if(ymdLength == 6 || ymdLength == 7 || ymdLength == 8){
		checkYmd = true;
	}
	
	if(!checkNumber && !checkYmd){
		if(!alertNumber && !alertYmd){
			alert("作業作成日または作業番号を入力してください");
		}
		return false;
	} else {
		return true;
	}
}

function updateCheck(){
	var trackName = document.getElementById('trackName').value;
	if(trackName){
		return true;
	} else {
		alert("トラック名を入力してください");
		return false;
	}
}
