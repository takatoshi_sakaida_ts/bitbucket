<?php
	require_once("../parts/pagechk.inc");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
		<title>倉庫指示書修正</title>
		<script type="text/javascript" src="shijisho.js"></script>
		<link rel="stylesheet" type="text/css" href="shijisho.css">
	</head>
	<body>
		<div style="margin-left:10px; margin-top:30px; font-size:15px;">
			<b>【倉庫指示書修正】</b>
		</div>
		<hr style="margin:20px 0px;">
		<div style="margin-left:10px; margin-top:15px;">
			<B>●検索条件入力</B>　<font color="red">※指示書作成日または作業番号のどちらかは必須です</font><br /><br />
			<form action="detail.php" method="post" onSubmit="return searchCheck()">
				<table class="inputTbl">
					<tr style="border:1px;">
						<th>指示書作成日</th>
						<td>
							(西暦)<input type="text" style="ime-mode: disabled;" id="workYear" name="workYear" size="5" maxlength="4">年
							<input type="text" style="ime-mode: disabled;" id="workMonth" name="workMonth" size="3" maxlength="2">月
							<input type="text" style="ime-mode: disabled;" id="workDay" name="workDay" size="3" maxlength="2">日
						</td>
						<th>作業番号</th>
						<td><input type="text" style="ime-mode: disabled;" id="workNumber" name="workNumber" size="15" maxlength="14"></td>
					</tr>
				</table>
				<br />
				<input type="submit" name="button1" value="検索">
				<input type="button" value="戻る" onClick="location.href='../../index.php'">
			</form>
		</div>
	</body>
</html>