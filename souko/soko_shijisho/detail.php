<?php
	require_once("../parts/pagechk.inc");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
		<title>倉庫指示書修正</title>
		<script type="text/javascript" src="trackinfo.js"></script>
		<link rel="stylesheet" type="text/css" href="shijisho.css">
	</head>
	<body>
		<div style="margin-left:10px; margin-top:30px; font-size:15px;">
			<b>【倉庫指示書修正】</b><br /><br />
			<a href="search.php">検索へ戻る</a>　<a href="../../index.php">トップへ戻る</a>
		</div>
		<hr style="margin:20px 0px;">
		<?php
			$updateFlag = addslashes(@$_GET["updateflag"]);
			$ymd = '';
			$workNumber = '';
			$dataNo = 0;
			if($updateFlag == 'yes'){
				$dataNo = (int)addslashes(@$_GET["dataNo"]);
				$workNumber = addslashes(@$_GET["workNumber"]);
				$ymd = addslashes(@$_GET["inputYmd"]);
			} else {
				$workNumber = addslashes(@$_POST["workNumber"]);
				$workYear = addslashes(@$_POST["workYear"]);
				$workMonth = addslashes(@$_POST["workMonth"]);
				if(mb_strlen($workMonth) == 1){
					$workMonth = "0".$workMonth;
				}
				$workDay = addslashes(@$_POST["workDay"]);
				if(mb_strlen($workDay) == 1){
					$workDay = "0".$workDay;
				}
				$ymd = $workYear.$workMonth.$workDay;
			}
			
			//ファイルの読み込み
			//PEARの利用     -------(1)
			require_once("DB.php");
			//ログイン情報の読み込み
			require_once("../parts/login_souko.php");
			
			//指示書取得
			$selectSql = "select s.stockworkcode, s.stockworkdate, s.status_kind, s.operatestaffcode, st.staffname, s.remark, count(g.instorecode) ";
			$selectSql = $selectSql."from d_stockwork s, d_stockwork_goods g, m_staff st ";
			$selectSql = $selectSql."where s.stockworkcode = g.stockworkcode ";
			$selectSql = $selectSql."and s.operatestaffcode = st.staffcode ";
			if(mb_strlen($ymd) == 8){
				$selectSql = $selectSql."and s.stockworkdate = '".$ymd."' ";
			}
			if(mb_strlen($workNumber) != 0){
				$selectSql = $selectSql."and s.stockworkcode = '".$workNumber."' ";
			}
			$selectSql = $selectSql."and s.stockwork_kind = '09' ";
			$selectSql = $selectSql."group by s.stockworkcode, s.stockworkdate, s.status_kind, s.operatestaffcode, st.staffname, s.remark ";
			$selectSql = $selectSql."order by s.stockworkcode ";
			//print $selectSql;
			$selectRes = $db->query($selectSql);
		?>
		<div style="margin-left:10px; margin-top:15px;">
			<B>●指示書情報</B><br /><br />
			<table class="inputTbl">
				<tr style="border:1px;">
					<th>No</th>
					<th>作業番号</th>
					<th>指示書作成日</th>
					<th>作業ステータス</th>
					<th>スタッフコード</th>
					<th>スタッフ名</th>
					<th>棚移動備考</th>
					<th>作業商品数</th>
					<th>更新</th>
				</tr>
				<?php
					$count = 0;
					while($selectRow = $selectRes->fetchRow()){
						$count++;
						if($count % 2 == 0){
//							if($count != $dataNo){
//								print "<tr style=\"background-color:#96F2A1;\">\r\n";
//							} else {
//								print "<tr style=\"background-color:#F5A9BC;\">\r\n";
//							}
							print "<tr style=\"background-color:#96F2A1;\">\r\n";
						} else {
//							if($count != $dataNo){
//				    			print "<tr>\r\n";
//				    		} else {
//				    			print "<tr style=\"background-color:#F5A9BC;\">\r\n";
//				    		}
				    		print "<tr>\r\n";
				    	}
				    	print "<form action=\"confirm.php\" method=\"post\" onSubmit=\"return updateCheck()\">\r\n";
				    	print "<input type=\"hidden\" name=\"inputYmd\" id=\"inputYmd\" value=\"".$ymd."\">\r\n";
				    	print "<input type=\"hidden\" name=\"workNumber\" id=\"workNumber\" value=\"".$workNumber."\">\r\n";
				    	print "<td><input type=\"hidden\" name=\"dataNo\" id=\"dataNo\" value=\"".$count."\">".$count."</td>\r\n";
				    	print "<td><input type=\"hidden\" name=\"stockworkcode\" id=\"stockworkcode\" value=\"".$selectRow[0]."\">".$selectRow[0]."</td>\r\n";
						print "<td>".substr($selectRow[1], 0, 4)."年".substr($selectRow[1], 4, 2)."月".substr($selectRow[1], 6)."日"."</td>\r\n";
						if($selectRow[2] == '00'){
							print "<td>未作業</td>\r\n";
						} elseif($selectRow[2] == '01') {
							print "<td>作業指示印刷済</td>\r\n";
						} elseif($selectRow[2] == '02') {
							print "<td>棚出済</td>\r\n";
						} elseif($selectRow[2] == '03') {
							print "<td>作業済</td>\r\n";
						}
						print "<td><input type=\"text\" name=\"operatestaffcode\" id=\"operatestaffcode\" size=\"7\" maxlength=\"6\" value=\"".$selectRow[3]."\"></td>\r\n";
						print "<td>".$selectRow[4]."</td>\r\n";
						print "<td><input type=\"text\" name=\"remark\" id=\"remark\" size=\"50\" maxlength=\"100\" value=\"".$selectRow[5]."\"></td>\r\n";
						print "<td>".$selectRow[6]."</td>\r\n";
						print "<td><input type=\"submit\" name=\"button1\" value=\"更新\"></td>\r\n";
						print "</form>\r\n";
						print "</tr>\r\n";
					}
				?>
			</table>
		</div>
	</body>
</html>