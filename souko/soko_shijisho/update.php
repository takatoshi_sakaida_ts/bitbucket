<?php
	require_once("../parts/pagechk.inc");
	//パラメータ
	$dataNo = addslashes(@$_POST["dataNo"]);
	$inputYmd = addslashes(@$_POST["inputYmd"]);
	$workNumber = addslashes(@$_POST["workNumber"]);
	$stockworkcode = addslashes(@$_POST["stockworkcode"]);
	$operatestaffcode = addslashes(@$_POST["operatestaffcode"]);
	$remark = addslashes(@$_POST["remark"]);
	$ymd = date('Ymd');
	$time = date('His');
	
	//ファイルの読み込み
	//PEARの利用     -------(1)
	require_once("DB.php");
	//ログイン情報の読み込み
	require_once("../parts/login_souko.php");
	//SQL生成
	$selectWorkSql = "select OPERATESTAFFCODE, REMARK from D_STOCKWORK where STOCKWORKCODE = '".$stockworkcode."'";
	//print $selectWorkSql;
	$selectWorkRes = $db->query($selectWorkSql);
	if(DB::isError($selectWorkRes)){
		$selectWorkRes->DB_Error($selectWorkRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	
	//update log
	$fLog = fopen("modify.log", "a");
	while($selectWorkRow = $selectWorkRes->fetchRow()){
		$logYmd = date('Y/m/d H:i:s');
		$logTxt = $logYmd." update [".$_SESSION["user_id"]."] KEY[".$stockworkcode."] before[".$selectWorkRow[0].",".$selectWorkRow[1]."] after[".$operatestaffcode.",".$remark."]\r\n";
		fputs ($fLog, $logTxt);
	}
	fclose ($fLog);
	
	//updateSQL生成
	$updateWorkSql = "update D_STOCKWORK";
	$updateWorkSql = $updateWorkSql." set OPERATESTAFFCODE = '".$operatestaffcode."', ";
	$updateWorkSql = $updateWorkSql."REMARK = '".$remark."', ";
	$updateWorkSql = $updateWorkSql."MODIFYDATE = '".$ymd."', ";
	$updateWorkSql = $updateWorkSql."MODIFYTIME = '".$time."' ";
	$updateWorkSql = $updateWorkSql."where STOCKWORKCODE = '".$stockworkcode."'";
	//print $updateWorkSql;
	$updateWorkRes = $db->query($updateWorkSql);
	if(DB::isError($updateWorkRes)){
		$updateWorkRes->DB_Error($updateWorkRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	
	$selectWorkRes->free();
	$db->commit();
	$db->disconnect();
	
	//編集画面へリダイレクト
	if($inputYmd && $workNumber){
		header("Location: detail.php?dataNo=".$dataNo."&inputYmd=".$inputYmd."&workNumber=".$workNumber."&updateflag=yes");
	} elseif(!$inputYmd && $workNumber) {
		header("Location: detail.php?dataNo=".$dataNo."&workNumber=".$workNumber."&updateflag=yes");
	} elseif($inputYmd && !$workNumber) {
		header("Location: detail.php?dataNo=".$dataNo."&inputYmd=".$inputYmd."&updateflag=yes");
	}
	exit();
?>