<?php
	require_once("../parts/pagechk.inc");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
		<title>倉庫指示書修正</title>
		<script type="text/javascript" src="trackinfo.js"></script>
		<link rel="stylesheet" type="text/css" href="shijisho.css">
	</head>
	<body>
		<div style="margin-left:10px; margin-top:30px; font-size:15px;">
			<b>【倉庫指示書修正】</b><br /><br />
			<a href="search.php">検索へ戻る</a>　<a href="../../index.php">トップへ戻る</a>
		</div>
		<hr style="margin:20px 0px;">
		<?php
			$inputYmd = addslashes(@$_POST["inputYmd"]);
			$workNumber = addslashes(@$_POST["workNumber"]);
			$dataNo = addslashes(@$_POST["dataNo"]);
			$stockworkcode = addslashes(@$_POST["stockworkcode"]);
			$operatestaffcode = addslashes(@$_POST["operatestaffcode"]);
			$remark = addslashes(@$_POST["remark"]);
			
			//ファイルの読み込み
			//PEARの利用     -------(1)
			require_once("DB.php");
			//ログイン情報の読み込み
			require_once("../parts/login_souko.php");
			
			//指示書取得
			$selectSql = "select s.stockworkcode, s.stockworkdate, s.status_kind, s.operatestaffcode, st.staffname, s.remark, count(g.instorecode) ";
			$selectSql = $selectSql."from d_stockwork s, d_stockwork_goods g, m_staff st ";
			$selectSql = $selectSql."where s.stockworkcode = g.stockworkcode ";
			$selectSql = $selectSql."and s.operatestaffcode = st.staffcode ";
//			if(mb_strlen($inputYmd) == 8){
//				$selectSql = $selectSql."and s.stockworkdate = '".$inputYmd."' ";
//			}
			if(mb_strlen($stockworkcode) != 0){
				$selectSql = $selectSql."and s.stockworkcode = '".$stockworkcode."' ";
			}
			$selectSql = $selectSql."and s.stockwork_kind = '09' ";
			$selectSql = $selectSql."group by s.stockworkcode, s.stockworkdate, s.status_kind, s.operatestaffcode, st.staffname, s.remark ";
			$selectSql = $selectSql."order by s.stockworkcode ";
			//print $selectSql."<br>";
			$selectRes = $db->query($selectSql);
			
			//staff情報取得
			$selectStaffSql = "select STAFFNAME from M_STAFF where STAFFCODE = '".$operatestaffcode."'";
			$staffRes = $db->query($selectStaffSql);
		?>
		<div style="margin-left:10px; margin-top:15px;">
			<B>●変更情報確認</B>　<font color="red">更新内容を確認してください</font><br /><br />
			<table class="inputTbl">
				<tr style="border:1px;">
					<th></th>
					<th>作業番号</th>
					<th>指示書作成日</th>
					<th>作業ステータス</th>
					<th>スタッフコード</th>
					<th>スタッフ名</th>
					<th>棚移動備考</th>
					<th>作業商品数</th>
				</tr>
				<?php
					$res_0;
					$res_1;
					$res_2;
					$res_3;
					$res_4;
					$res_5;
					$res_6;
					
					while($selectRow = $selectRes->fetchRow()){
						$res_0 = $selectRow[0];
						$res_1 = $selectRow[1];
						$res_2 = $selectRow[2];
						$res_3 = $selectRow[3];
						$res_4 = $selectRow[4];
						$res_5 = $selectRow[5];
						$res_6 = $selectRow[6];
						
						print "<tr>\r\n";
						print "<td style=\"background-color:#F5A9BC;\"><B>変更前</B></td>\r\n";
						print "<td>".$selectRow[0]."</td>\r\n";
						print "<td>".substr($selectRow[1], 0, 4)."年".substr($selectRow[1], 4, 2)."月".substr($selectRow[1], 6)."日"."</td>\r\n";
						if($selectRow[2] == '00'){
							print "<td>未作業</td>\r\n";
						} elseif($selectRow[2] == '01') {
							print "<td>作業指示印刷済</td>\r\n";
						} elseif($selectRow[2] == '02') {
							print "<td>棚出済</td>\r\n";
						} elseif($selectRow[2] == '03') {
							print "<td>作業済</td>\r\n";
						}
						print "<td style=\"background-color:#F5A9BC;\"><B>".$selectRow[3]."</B></td>\r\n";
						print "<td style=\"background-color:#F5A9BC;\"><B>".$selectRow[4]."</B></td>\r\n";
						print "<td style=\"background-color:#F5A9BC;\"><B>".$selectRow[5]."</B></td>\r\n";
						print "<td>".$selectRow[6]."</td>\r\n";
					}
					print "<tr>\r\n";
					print "<td style=\"background-color:#96F2A1;\"><B>変更後</B></td>\r\n";
					print "<td>".$res_0."</td>\r\n";
					print "<td>".substr($res_1, 0, 4)."年".substr($res_1, 4, 2)."月".substr($res_1, 6)."日"."</td>\r\n";
					if($res_2 == '00'){
						print "<td>未作業</td>\r\n";
					} elseif($res_2 == '01') {
						print "<td>作業指示印刷済</td>\r\n";
					} elseif($res_2 == '02') {
						print "<td>棚出済</td>\r\n";
					} elseif($res_2 == '03') {
						print "<td>作業済</td>\r\n";
					}
					print "<td style=\"background-color:#96F2A1;\"><B>".$operatestaffcode."</B></td>\r\n";
					while($staffRow = $staffRes->fetchRow()){
						print "<td style=\"background-color:#96F2A1;\"><B>".$staffRow[0]."</B></td>\r\n";
					}
					print "<td style=\"background-color:#96F2A1;\"><B>".$remark."</B></td>\r\n";
					print "<td>".$res_6."</td>\r\n";
				?>
			</table><br />
			<table>
				<tr>
				<?php
					print "<form action=\"update.php\" method=\"post\" onSubmit=\"return updateCheck()\">\r\n";
					print "<input type=\"hidden\" name=\"inputYmd\" id=\"inputYmd\" value=\"".$inputYmd."\">\r\n";
					print "<input type=\"hidden\" name=\"workNumber\" id=\"workNumber\" value=\"".$workNumber."\">\r\n";
					print "<input type=\"hidden\" name=\"dataNo\" id=\"dataNo\" value=\"".$dataNo."\">\r\n";
					print "<input type=\"hidden\" name=\"stockworkcode\" id=\"stockworkcode\" value=\"".$stockworkcode."\">\r\n";
					print "<input type=\"hidden\" name=\"operatestaffcode\" id=\"operatestaffcode\" value=\"".$operatestaffcode."\">\r\n";
					print "<input type=\"hidden\" name=\"remark\" id=\"remark\" value=\"".$remark."\">\r\n";
					print "<td><input type=\"submit\" name=\"button1\" value=\"更新\"></td>\r\n";
					print "</form>\r\n";
					if($inputYmd && $workNumber){
						print "<td><input type=\"button\" value=\"キャンセル\" onClick=\"location.href='detail.php?dataNo=".$dataNo."&inputYmd=".$inputYmd."&workNumber=".$workNumber."&updateflag=yes'\"></td>\r\n";
					} elseif(!$inputYmd && $workNumber) {
						print "<td><input type=\"button\" value=\"キャンセル\" onClick=\"location.href='detail.php?dataNo=".$dataNo."&workNumber=".$workNumber."&updateflag=yes'\"></td>\r\n";
					} elseif($inputYmd && !$workNumber) {
						print "<td><input type=\"button\" value=\"キャンセル\" onClick=\"location.href='detail.php?dataNo=".$dataNo."&inputYmd=".$inputYmd."&updateflag=yes'\"></td>\r\n";
					}
					$selectRes->free();
					$staffRes->free();
					$db->disconnect();
				?>
				</tr>
			</table>
		</div>
	</body>
</html>