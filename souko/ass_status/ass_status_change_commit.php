<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>再査定開始処理完了</title>
</head>
<body bgcolor="#ccccff">
<strong>再査定開始処理完了</strong>
<br><br>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_GET["receptioncode"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "買取受付番号を入力してください！！<br>";
	print "実行ボタンの押下は無効です。<br>";
} else {
	//ログイン情報の読み込み
	require_once("../parts/login_souko.php");
	//D_ASS の STATUS_KIND を更新 '01' ⇒ '00'
	$sql = "update d_ass a ";
	$sql = $sql . "set a.STATUS_KIND = '00' ";
	$sql = $sql . "where a.ASSCODE = '".$keyti."' ";
	$sql = $sql . "and a.STATUS_KIND = '01'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	//D_ASS_EXT の STATUS_KIND を更新 '01' ⇒ '00'
	$sql = "update D_ASS_EXT e ";
	$sql = $sql . "set e.STATUS_KIND = '00' ";
	$sql = $sql . "where e.ASSCODE = '".$keyti."' ";
	$sql = $sql . "and e.STATUS_KIND = '01'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	//SQL文のCOUNT関数を使用
	$sql = "select distinct r.asscode, r.customername, b.status_kind ";
	$sql = $sql . "from d_ass b,d_ass_request r,d_ass_discard c, d_ass_ext e ";
	$sql = $sql . "where r.asscode = b.asscode(+) ";
	$sql = $sql . "and r.asscode = c.asscode(+) ";
	$sql = $sql . "and r.asscode = e.asscode(+) ";
	$sql = $sql . "and r.asscode ='".$keyti."' ";
	$sql = $sql . "and b.STATUS_KIND = '00' ";
	$sql = $sql . "and e.STATUS_KIND = '00' ";
	$sql = $sql . "order by r.asscode";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	$j=1;
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	while($row = $res->fetchRow()){
		print "<table border=1>\n";
		print "<tr bgcolor=#ccffff>\n";
		//項目名の表示
		print "<td nowrap width=180>買取受付番号</td><td nowrap width=120>氏名</td><td nowrap width=200>状態</td></tr>";
		//項番
		print "<td>".$row[0] ."</td>";
		print "<td>".$row[1] ."</td>";
		print "<td>".Retkaitoristatuskind($row[2]) ."</td>";
		print "</tr></table>";
		print "<HR>";
		print "<font size=5 >更新が完了しました。本日中に再査定を実施して下さい。</font><br>";
		print "<font size=5 >査定完了後に</font>";
		print "<font size=5 color='red'>「査定完了」</font>";
		print "<font size=5 >を実施して下さい。</font><br>";
		$j++;
	}
	if($j == 1) {
		print "更新に失敗しました！！<br>";
		print "システム部に連絡してください。<br>";
	}
	//データの開放
	$res->free();
	$db->disconnect();
}

?>
<BR>
<FORM action="/souko/ass_status/ass_status_change_search.php"><INPUT TYPE="submit" VALUE="検索画面に戻る"></FORM>
</body>
</html>
