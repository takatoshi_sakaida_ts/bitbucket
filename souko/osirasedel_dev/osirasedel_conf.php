<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>お知らせメール・ブックマーク削除確認画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
	print "<br>\n<strong>お知らせメール・ブックマーク削除画面：". $keyti."</strong><br><br>\n<hr>";
$toalcheck=0;
//ブックマーク情報
$j="ng";
$sql="select d.ord_no,d.instorecode,n.GOODS_NAME1||n.GOODS_NAME2||n.GOODS_NAME3,m.mem_no ".
"from TORDERDTL d,tbookmark b,tmember m,torder t,tgoods_bko n ".
"where m.mem_no=b.mem_no and b.instorecode=d.instorecode ".
"and t.ord_no=d.ord_no and d.instorecode=n.instorecode and (GOODS_CNT-ORD_CNCL_CNT)>0 ".
"and m.mem_id='" . $keyti ."' and t.mem_id='" . $keyti ."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		$j="ok";
		$mem_no=$row[3];
		$bookresult_orderno[$i]=$row[0];
		$bookresult[$i]=$row[1];
		$bookresult_name[$i]=$row[2];
		$i=$i+1;
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	$c=0;
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<strong>削除対象ブックマーク</strong> <br>";
	print "<td>注文番号</td><td>インストアコード</td><td>商品名</td></tr>";

	while(isset($bookresult[$c]))
	{
	print "<tr><td>".$bookresult_orderno[$c]."</td><td>".$bookresult[$c]."</td><td>".$bookresult_name[$c];
	print "</td></tr>";
	$c++;
	}
	print "</table>";
}
	else
{
	print "<BR>ブックマークの削除対象はありません。";
}
print "<HR>";
$c=0;
$_SESSION["HSMAP"]["bookresultsql"]="nothing";
while(isset($bookresult[$c]))
{
	if($c==0)
	{
		$_SESSION["HSMAP"]["bookresultsql"]="'".$bookresult[$c]."'";
		$_SESSION["HSMAP"]["bookresultlog"]=date("Y/m/d H:i:s").",".$keyti.",bookmark,".$bookresult[$c].",".$_SESSION["user_id"];
	}else
	{
		$_SESSION["HSMAP"]["bookresultsql"]=$_SESSION["HSMAP"]["bookresultsql"].",'".$bookresult[$c]."'";
		$_SESSION["HSMAP"]["bookresultlog"]=$_SESSION["HSMAP"]["bookresultlog"]."\n".date("Y/m/d H:i:s").",".$keyti.",bookmark,".$bookresult[$c].",".$_SESSION["user_id"];
	}
	$c++;
}
if (isset($bookresult[0]))
{
	$_SESSION["HSMAP"]["bookresultsql"]="delete from tbookmark where mem_no='".$mem_no."' and instorecode in (".$_SESSION["HSMAP"]["bookresultsql"].")";
	$toalcheck=1;
}
//入荷お知らせメール情報
$j="ng";
$sql="select d.ord_no,d.instorecode,n.GOODS_NAME1||n.GOODS_NAME2||n.GOODS_NAME3,m.mem_no ".
"from TORDERDTL d,TALERTMAIL_ARRIVAL b,tmember m,torder t,tgoods_bko n ".
"where m.mem_no=b.mem_no and b.instorecode=d.instorecode ".
"and t.ord_no=d.ord_no and d.instorecode=n.instorecode and (GOODS_CNT-ORD_CNCL_CNT)>0 ".
"and m.mem_id='" . $keyti ."' and t.mem_id='" . $keyti ."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		$j="ok";
		$mem_no=$row[3];
		$arrivalresult_orderno[$i]=$row[0];
		$arrivalresult[$i]=$row[1];
		$arrivalresult_name[$i]=$row[2];
		$i=$i+1;
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	$c=0;
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<strong>削除対象入荷お知らせメール</strong> <br>";
	print "<td>注文番号</td><td>インストアコード</td><td>商品名</td></tr>";

	while(isset($arrivalresult[$c]))
	{
	print "<tr><td>".$arrivalresult_orderno[$c]."</td><td>".$arrivalresult[$c]."</td><td>".$arrivalresult_name[$c];
	print "</td></tr>";
	$c++;
	}
	print "</table>";
}
	else
{
	print "<BR>入荷お知らせメールの削除対象はありません。<BR>";
}
print "<HR>";
$c=0;
$_SESSION["HSMAP"]["arrivalresultsql"]="nothing";
while(isset($arrivalresult[$c]))
{
	if($c==0)
	{
		$_SESSION["HSMAP"]["arrivalresultsql"]="'".$arrivalresult[$c]."'";
		$_SESSION["HSMAP"]["arrivalresultlog"]=date("Y/m/d H:i:s").",".$keyti.",arrival,".$arrivalresult[$c].",".$_SESSION["user_id"];

	}else
	{
		$_SESSION["HSMAP"]["arrivalresultsql"]=$_SESSION["HSMAP"]["arrivalresultsql"].",'".$arrivalresult[$c]."'";
		$_SESSION["HSMAP"]["arrivalresultlog"]=$_SESSION["HSMAP"]["arrivalresultlog"]."\n".date("Y/m/d H:i:s").",".$keyti.",arrival,".$arrivalresult[$c].",".$_SESSION["user_id"];
	}
	$c++;
}
if (isset($arrivalresult[0]))
{
	$_SESSION["HSMAP"]["arrivalresultsql"]="delete from TALERTMAIL_ARRIVAL where mem_no='".$mem_no."' and instorecode in (".$_SESSION["HSMAP"]["arrivalresultsql"].")";
	$toalcheck=1;
}
//値下お知らせメール情報
/*
$j="ng";
$sql="select d.ord_no,d.instorecode,n.GOODS_NAME1||n.GOODS_NAME2||n.GOODS_NAME3,m.mem_no ".
"from TORDERDTL d,TALERTMAIL_discount b,tmember m,torder t,tgoods_bko n ".
"where m.mem_no=b.mem_no and b.instorecode=d.instorecode ".
"and t.ord_no=d.ord_no and d.instorecode=n.instorecode and (GOODS_CNT-ORD_CNCL_CNT)>0 ".
"and m.mem_id='" . $keyti ."' and t.mem_id='" . $keyti ."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		$j="ok";
		$mem_no=$row[3];
		$discountresult_orderno[$i]=$row[0];
		$discountresult[$i]=$row[1];
		$discountresult_name[$i]=$row[2];
		$i=$i+1;
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	$c=0;
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<strong>削除対象値下お知らせメール</strong><br>";
	print "<td>注文番号</td><td>インストアコード</td><td>商品名</td></tr>";

	while(isset($discountresult[$c]))
	{
	print "<tr><td>".$discountresult_orderno[$c]."</td><td>".$discountresult[$c]."</td><td>".$discountresult_name[$c];
	print "</td></tr>";
	$c++;
	}
	print "</table>";
}
	else
{
	print "<BR>値下お知らせメールの削除対象はありません。<BR>";
}
print "<HR>";
$c=0;
*/
$_SESSION["HSMAP"]["discountresultsql"]="nothing";
/*
while(isset($discountresult[$c]))
{
	if($c==0)
	{
		$_SESSION["HSMAP"]["discountresultsql"]="'".$discountresult[$c]."'";
		$_SESSION["HSMAP"]["discountresultlog"]=date("Y/m/d H:i:s").",".$keyti.",discount,".$discountresult[$c].",".$_SESSION["user_id"];
	}else
	{
		$_SESSION["HSMAP"]["discountresultsql"]=$_SESSION["HSMAP"]["discountresultsql"].",'".$discountresult[$c]."'";
		$_SESSION["HSMAP"]["discountresultlog"]=$_SESSION["HSMAP"]["discountresultlog"]."\n".date("Y/m/d H:i:s").",".$keyti.",discount,".$discountresult[$c].",".$_SESSION["user_id"];
	}
	$c++;
}
if (isset($discountresult[0]))
{
	$_SESSION["HSMAP"]["discountresultsql"]="delete from TALERTMAIL_discount where mem_no='".$mem_no."' and instorecode in (".$_SESSION["HSMAP"]["discountresultsql"].")";
	$toalcheck=1;
}
*/
$db->disconnect();

//print $_SESSION["HSMAP"]["bookresultsql"]."<BR>";
//print $_SESSION["HSMAP"]["arrivalresultsql"]."<BR>";
//print $_SESSION["HSMAP"]["discountresultsql"];
if ($toalcheck==1)
{
?>
<BR>
<form method="POST" action="osirasedel_ent.php" name ="osirasedel">
<input type="submit" value="削除実行">
<?PHP
}
?>
　　<INPUT TYPE="BUTTON" VALUE="　戻　る　" onClick="history.back()">
</FORM>
</body>
</html>