<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>お知らせメール・ブックマーク登録確認画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
	print "<br>\n<strong>お知らせメール・ブックマーク登録状況確認画面：". $keyti."</strong><br><br>\n<hr>";
$toalcheck=0;
//ブックマーク情報
$j="ng";
$sql="select b.instorecode,n.GOODS_NAME1||n.GOODS_NAME2||n.GOODS_NAME3,m.mem_no,b.REG_DM ".
"from tbookmark b,tmember m,tgoods_bko n ".
"where m.mem_no=b.mem_no ".
"and b.instorecode=n.instorecode ".
"and m.mem_id='" . $keyti ."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		$j="ok";
		$mem_no=$row[2];
		$bookresult[$i]=$row[0];
		$bookresult_name[$i]=$row[1];
		$bookresult_time[$i]=$row[3];
		$i=$i+1;
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	$c=0;
	$d=1;
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<strong>登録ブックマーク</strong> <br>";
	print "<td width=20>　</td><td width=120>インストアコード</td><td width=1000>商品名</td><td width=120>登録日</td></tr>";

	while(isset($bookresult[$c]))
	{
	print "<tr><td>".$d."</td><td>".$bookresult[$c]."</td><td>".$bookresult_name[$c]."</td><td>".$bookresult_time[$c];
	print "</td></tr>";
	$c++;
	$d++;
	}
	print "</table>";
}
	else
{
	print "<BR>ブックマークの登録はありません。";
}
print "<HR>";

//入荷お知らせメール情報
$j="ng";
$sql="select b.instorecode,n.GOODS_NAME1||n.GOODS_NAME2||n.GOODS_NAME3,m.mem_no,b.REG_DM ".
"from TALERTMAIL_ARRIVAL b,tmember m,tgoods_bko n ".
"where m.mem_no=b.mem_no ".
"and b.instorecode=n.instorecode ".
"and m.mem_id='" . $keyti ."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		$j="ok";
		$mem_no=$row[2];
		$arrivalresult[$i]=$row[0];
		$arrivalresult_name[$i]=$row[1];
		$arrivalresult_time[$i]=$row[3];
		$i=$i+1;
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	$c=0;
	$d=1;
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<strong>お知らせメール</strong> <br>";
	print "<td width=20>　</td><td width=120>インストアコード</td><td width=1000>商品名</td><td width=120>登録日</td></tr>";

	while(isset($arrivalresult[$c]))
	{
	print "<tr><td>".$d."</td><td>".$arrivalresult[$c]."</td><td>".$arrivalresult_name[$c]."</td><td>".$arrivalresult_time[$c];
	print "</td></tr>";
	$c++;
	$d++;
	}
	print "</table>";
}
	else
{
	print "<BR>お知らせメールの登録はありません。<BR>";
}
print "<HR>";
$c=0;
$_SESSION["HSMAP"]["arrivalresultsql"]="nothing";

//値下お知らせメール情報
$j="ng";
/*
$sql="select b.instorecode,n.GOODS_NAME1||n.GOODS_NAME2||n.GOODS_NAME3,m.mem_no,b.REG_DM ".
"from TALERTMAIL_discount b,tmember m,tgoods_bko n ".
"where m.mem_no=b.mem_no ".
"and b.instorecode=n.instorecode ".
"and m.mem_id='" . $keyti ."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		$j="ok";
		$mem_no=$row[2];
		$discountresult[$i]=$row[0];
		$discountresult_name[$i]=$row[1];
		$discountresult_time[$i]=$row[3];
		$i=$i+1;
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	$c=0;
	$d=1;
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<strong>値下お知らせメール</strong><br>";
	print "<td width=20>　</td><td width=120>インストアコード</td><td width=1000>商品名</td><td width=120>登録日</td></tr>";

	while(isset($discountresult[$c]))
	{
	print "<tr><td>".$d."</td><td>".$discountresult[$c]."</td><td>".$discountresult_name[$c]."</td><td>".$discountresult_time[$c];
	print "</td></tr>";
	$c++;
	$d++;
	}
	print "</table>";
}
	else
{
	print "<BR>値下お知らせメールの登録はありません。<BR>";
}
print "<HR>";
$c=0;
$_SESSION["HSMAP"]["discountresultsql"]="nothing";
*/
$db->disconnect();
?>
　　<INPUT TYPE="BUTTON" VALUE="　戻　る　" onClick="history.back()">
</FORM>
</body>
</html>