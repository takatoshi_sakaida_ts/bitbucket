<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select * from d_stock where instorecode = '".$keyti."' order by stockno";
//行数の取得
//$dtcnt = $db->getOne($sql);
// 検索式をSQL文のselectコマンドで作成する(検索式は任意一致検索式)
/*
$sql = "select distinct t.ord_no,t.MEM_ID,l.DELV_DM,l.DELV_ENTER,l.DELV_CHECK,t.ORDR_NM_FST,t.ORDR_NM_MID from torder t,torderdtl d,torderdelv l ";
$sql = $sql . "where t.ord_no = d.ord_no and d.DELV_NO = l.DELV_NO and t.ord_no = '".$keyti."'";
*/
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>検索結果 インストアコード：". $keyti."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td>在庫番号</td><td>取引日付</td><td>ロケーションコード</td><td>ロケーション名</td><td>新古区分</td><td>在庫引当区分</td><td>更新日付</td></tr>";
/*
//カラム数の取得
$ncols = $res->numCols();
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
  while($row = $res->fetchRow()){
        print "<tr><td> $j</td>";
        for($i=0;$i<$ncols;$i++){
                $column_value = $row[$i];//行の列データを取得
                print "<td> ".$column_value ."</td>";
        }
  $j++;
  }
*/
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>". $j."</td>";
//在庫番号
	print "<td>".$row[0] ."</td>";
//取引日付
	print "<td>".$row[1] ."</td>";
//ロケーションコード
	print "<td>".$row[2] ."</td>";
	print "<td>".Retlocation($row[2]) ."</td>";
//在庫区分
switch ($row[4]){
	case 0:
		$zaiko= "中古";
		break;
	case 1:
		$zaiko= "新古";
		break;
	case 2:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
		print "<td>".$zaiko."</td>";
//在庫引当区分
switch ($row[5]){
	case 0:
		$hikiate= "未引当";
		break;
	case 1:
		$hikiate= "作業引当";
		break;
	case 2:
		$hikiate= "注文引当";
		break;
	case 3:
		$hikiate= "作業引当確定";
		break;
	default:
		$hikiate= "エラー";
		break;
}
	print "<td>".$hikiate ."</td>";
//更新日付
	print "<td>".substr($row[6],0,4). "-" .substr($row[6],4,2). "-" .substr($row[6],6,2). "</td>";
//配送方法
/*
	if ($row[3]=='01')
		print print "<td>日通</td>";
	else{
		print print "<td>郵政</td>";
	}
*/
//配送番号
/*
	print "<td> ".substr($row[4],0,3). "-" .substr($row[4],3,2). "-" .substr($row[4],5,3). "-" .substr($row[4],8,4). "</td>";
*/
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>