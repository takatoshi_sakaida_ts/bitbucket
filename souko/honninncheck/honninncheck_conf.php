<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>本人確認状態確認画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("../parts/selectvalue_souko.php");
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
if (preg_match("/.*@.*/", $keyti)) {
	$sql="select m.mem_no,m.mem_nm_fst,m.mem_nm_mid,m.mem_id,m.use_yn,m.auth_stat from tmember m,tsell t where m.mem_no=t.mem_no and m.mem_id = '" . $keyti . "'";
} else {
	$sql="select distinct m.mem_no,m.mem_nm_fst,m.mem_nm_mid,m.mem_id,m.use_yn,m.auth_stat from tmember m,tsell t where m.mem_no=t.mem_no and t.sell_no = '" . $keyti . "'";
}

//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
$j="ng";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$j="ok";
	$mem_no=$row[0];
	$mem_name=$row[1]."　".$row[2];
	$mem_id=$row[3];
	$auth_stat=$row[5];
}
if ($j!="ng")
{
$chk_flg="0";
	//検索結果の表示
	print "<strong>検索文字列：". $keyti."</strong> <br><br>\n";
	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td>会員NO</td><td>会員ID</td><td>氏名</td><td>本人確認状態</td></tr>";
	print "<tr>";
	print "<td>".$mem_no."</td>";
	print "<td>".$mem_id."</td>";
	print "<td>".$mem_name."</td>";
	print "<td>".Retauthstat($auth_stat)."</td>";
	print "</TR></table>";
	//データの開放
	$res->free();
	$db->disconnect();
}
else
{
	$chk_flg="1";
	print "該当のデータが見つかりません。" .$keyti  ;
}
?>
<BR>
<form method="POST">

<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
</FORM>
</body>
</html>