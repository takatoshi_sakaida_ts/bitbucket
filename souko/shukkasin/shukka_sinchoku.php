<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出荷登録進捗</title>
</head>
<body bgcolor="#ffffff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$keyti= date("Ymd");
//$keyti='20091005';
$sql = "select count(distinct r.receptioncode),d.operatestaffcode,f.staffname from d_reception_shipment r,M_staff f,d_reception d　where " .
" r.receptioncode=d.receptioncode and d.operatestaffcode=f.staffcode and substr(r.deliverydatetime,1,8)=(select to_char(sysdate,'yyyymmdd') from dual)  group by d.operatestaffcode,f.staffname order by d.operatestaffcode";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>出荷登録進捗　". substr($keyti,0,4). "年".substr($keyti,4,2)."月".substr($keyti,6,2)."日"."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap nowrap>スタッフコード</td><td width=200>スタッフ名</td><td width=150>出荷登録数</td></tr>";
$j=0;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//スタッフコード
	print "<td>".$row[1] ."</td>";
//スタッフ名
	print "<td>". $row[2] ."</td>";
//件数
	print "<td>".$row[0] ."</td></tr>";
	$j=$j+$row[0];
}
print "<tr><td colspan=2 align=center>　　出荷登録合計　　</td><td>".$j."</td></tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>