<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>  ボランティア振込済データダウンロード </title>
</heade>
<strong>ボランティア振込済データダウンロード■■■申込日絞込み版■■■</strong>
<br><br>
<form method="POST" action="dl_volout_applicationdate.php" name="frmdlvol">
データ取得対象期間を入力してください（申込日YYYYMMDD）<br>
開始日付　<input type="text" name="T1" maxlength="8"><BR>
終了日付　<input type="text" name="T2" maxlength="8"><BR><BR>
データ取得対象団体を選択してください<br>
<input type="hidden" name="T3" value="">
<input type="hidden" name="T4" value="0">
<table>
<tr>
<th>ボランティア種類</th>
<td>
<select name="parentS" onchange="createChildOptions(this.form)" style="width:300px;">
<option value="">ボランティア種類を選択して下さい</option>
<option value="1">Z10**　ボランティア</option>
<option value="2">Z60**　提携法人</option>
<option value="3">Z70**　企業ボランティア</option>
</select>
</td>
</tr>
<tr>
<th>子ジャンル</th>
<td><!--表示位置--><div id="childS"></div></td>
</tr>
</table>

<script type="text/javascript">
/* 子ジャンル（selectC）用の配列 */
	var item = new Array();

	item[0] = new Array();
	item[0][0]="---------------------";

        /* Z10 */
	item[1] = new Array();
	item[1][0]="子ジャンルを選択して下さい";
	item[1][1]="Z1のすべて";
	item[1][2]="Z1001 Room to Read";
	item[1][3]="Z1002・Z1006 シャンティ国際ボランティア会";
	item[1][4]="Z1003 ピースウィンズ・ジャパン";
	item[1][5]="Z1004 シャプラニール";
	item[1][6]="Z1005 JEN";
	item[1][7]="Z1007 かながわキンタロウ";
	item[1][8]="Z1029 大阪YMCA";
	item[1][9]="Z1030 グッドネーバーズ・ジャパン";
	item[1][10]="Z1031 国連ウィメン日本協会";
	item[1][11]="Z1032 湘南ふじさわシニアネット";
	item[1][12]="Z1033 アジア保健研修所(AHI)";
	item[1][13]="Z1034 ウィメンズアクションネットワーク";
    /* 2016/02/12 add */
	item[1][14]="Z11　日本赤十字社のすべて";
	item[1][15]="Z1100 日本赤十字社 本社";
	item[1][16]="Z1111 日本赤十字社 埼玉";
	item[1][17]="Z1112 日本赤十字社 千葉";
	item[1][18]="Z1113 日本赤十字社 東京";
	item[1][19]="Z1114 日本赤十字社 神奈川";

        /* Z60 */
	item[2]= new Array();
	item[2][0]="子ジャンルを選択して下さい";
	item[2][1]="Z6のすべて";
	item[2][2]="Z6001 スターツピタットハウス";
	item[2][3]="Z6002 大京アステージ(くらしスクエア)";
	item[2][4]="Z6003 大京アステージ(コンシェルジュ)";
	item[2][5]="Z6004 三井不動産住宅サービス";
	item[2][6]="Z6005 アスク";
	item[2][7]="Z6006 インターファーム";
	item[2][8]="Z6007 アート引越しセンター";
	item[2][9]="Z6008 クラブパナソニック";
	item[2][10]="Z6009 大東建託";
	item[2][11]="Z6010 アリさんマークの引越社";
	item[2][12]="Z6011 イヌイ運送";
	item[2][13]="Z6012 日本通運";
	item[2][14]="Z6013 ハウスコム";
	item[2][15]="Z6014 オーネット";
	item[2][16]="Z6015 バイク王";
	item[2][17]="Z6016 三菱地所コミュニティ";
	item[2][18]="Z6017 コミュニティワン";
	item[2][19]="Z6018 住友不動産建物サービス";
	item[2][20]="Z6019 リトル・ママ";
	item[2][21]="Z6020 レジデンスクラブ";
	item[2][22]="Z6021 JAF";
	item[2][23]="Z6022 ぐるなび食市場";
	item[2][24]="Z6023 ヤマトホームコンビニエンス（クロネコヤマト引越センター）";
	item[2][25]="Z6024 ファミリー引越センター";
	item[2][26]="Z6025 ジャパンネット銀行";
	item[2][27]="Z6026 暮らしのサポートブック";
	item[2][28]="Z6027 三井のすまいLOOP(ループ)";
	item[2][29]="Z6028 エイブル";	
	item[2][30]="Z6029 MAST(マスト)";	
	item[2][31]="Z6030 フロムヴイ";	
	item[2][32]="Z6031 大和ライフネクスト";	
	item[2][33]="Z6032 レオパレス21";	
	item[2][34]="Z6033 ズバット引越し比較";	
	item[2][35]="Z6034 インターネットコンシェルジュサービス";
	item[2][36]="Z6035 長谷工コミュニティ";
	item[2][37]="Z6036 QLC(コンシェルジュ)";
	item[2][38]="Z6037 住宅情報館(城南建設)";
	item[2][39]="Z6038 ジャックスカード";
	item[2][40]="Z6039 積村ビル管理";
	item[2][41]="Z6040 東急住宅リース";
	item[2][42]="Z6041 ハッピークラブモール";
	item[2][43]="Z6042 おうちCO-OP";
	item[2][44]="Z6043 ハイホー";
	item[2][45]="Z6044 ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）";
	item[2][46]="Z6045 ソフトバンクグループ";
	item[2][47]="Z6046 オリックス・クレジット";
	item[2][48]="Z6047 リロクラブ";
	item[2][49]="Z6048 メディアカフェポパイ";
	item[2][50]="Z6049 アメリカン・エキスプレス";
	item[2][51]="Z6050 オリックス・ゴルフ・マネジメント";
	item[2][52]="Z6051 エイコータウン";
	item[2][53]="Z6052 社員向け_レオパレス21";
	item[2][54]="Z6053 macalon+（マカロンプラス）";
	item[2][55]="Z6054 タイムズクラブ";
	item[2][56]="Z6055 ゆとライフドットコム";
	item[2][57]="Z6056 クラスエル";
	item[2][58]="Z6057 バイクブロス";
	item[2][59]="Z6058 泉友";
	item[2][60]="Z6059 富士フイルム生活協同組合";
	item[2][61]="Z6060 リコー三愛グループ";
	item[2][62]="Z6061 ヴァリック";
	item[2][63]="Z6062 HOME’S 引越し";
	item[2][64]="Z6063 千葉県庁生活協同組合";
	
        /* Z70 */
	item[3] = new Array();
	item[3][0]="子ジャンルを選択して下さい";
	item[3][1]="Z7のすべて";
	item[3][2]="Z7001 NTT西日本";
	item[3][3]="Z7002 スターバックス";
	item[3][4]="Z7003 帝人";
	item[3][5]="Z7004 セガサミーホールディングス";
	item[3][6]="Z7005 NTTネオメイト";
	item[3][7]="Z7006 NEC";
	item[3][8]="Z7007 キャドバリージャパン";
	item[3][9]="Z7008 ファミリーマート";
	item[3][10]="Z7009 結核予防会";
	item[3][11]="Z7010 ナック";
	item[3][12]="Z7011 アイエスエフネット";
	item[3][13]="Z7012 インテリジェンス";
	item[3][14]="Z7013 チャーティス・ファー・イースト・ホールティングス";
	item[3][15]="Z7014 民際センター";
	item[3][16]="Z7015 DNPグループ労連";
	item[3][17]="Z7016 しょうがっこうをおくる会";
	item[3][18]="Z7017 レオパレス21(企業ボランティア宅本便)";
	item[3][19]="Z7018 神奈川大学";
	item[3][20]="Z7019 ICAN（アイキャン）";
	item[3][21]="Z7020 阪急阪神ホール ディングス";
	item[3][22]="Z7021 ソニー";
	item[3][23]="Z7022 地球環境基金";
	item[3][24]="Z7023 売って支援プログラム";
	item[3][25]="Z7024 西友";
	item[3][26]="Z7025 日本経済新聞文化部";
	item[3][27]="Z7026 朝日新聞社論説委員室";
	item[3][28]="Z7027 HFI（Hope and Faith International）";
	item[3][29]="Z7028 ACE（エース）";
	item[3][30]="Z7029 マンパワーグループ";
	item[3][31]="Z7030 中日本ハイウェイ・パトロール東京";
	item[3][32]="Z7031 三菱商事";
	item[3][33]="Z7032 地球の友と歩む会";
	item[3][34]="Z7033 損保ジャパン奈良支店";
	item[3][35]="Z7034 日本興亜損害保険";
	item[3][36]="Z7035 3keys（スリーキーズ）";
	item[3][37]="Z7036 ライフ";
	item[3][38]="Z7037 ★スポット対応企業";
	item[3][39]="Z7038 町田ゼルビア";
	item[3][40]="Z7039 SC相模原";
	item[3][41]="Z7040 幼い難民を考える会（CYR）";
	item[3][42]="Z7041 アフラック";
	item[3][43]="Z7042 JHP学校をつくる会";
	item[3][44]="Z7043 日立建機";
	item[3][45]="Z7044 北海道森と緑の会";
	item[3][46]="Z7045 ユニー";
	item[3][47]="Z7046 埼玉県自動車販売店協会（自販連埼玉支部）";
	item[3][48]="Z7047 マルハン";
	item[3][49]="Z7048 アドビシステムズ";
	item[3][50]="Z7049 FC在庫買取";
	item[3][51]="Z7050 大塚商会";
	item[3][52]="Z7051 森ビル都市企画";
	item[3][53]="Z7052 アクセス";
	item[3][54]="Z7053 ブリヂストンスポーツ";
	item[3][55]="Z7054 ケア・インターナショナルジャパン";
	item[3][56]="Z7055 世界の子どもにワクチンを(JCV)";
	item[3][57]="Z7056 共立メンテナンス";
	item[3][58]="Z7057 ジャパンハート";
	item[3][59]="Z7058 MSD";
	item[3][60]="Z7059 ブリッジ エーシア ジャパン(BAJ)";
	item[3][61]="Z7060 TBS(東日本大震災支援)";
	item[3][62]="Z7061 TBS(フィリピン台風支援)";
	item[3][63]="Z7062 岡谷鋼機";
	item[3][64]="Z7063 横浜開港祭";
	item[3][65]="Z7064 商船三井";
	item[3][66]="Z7065 広栄化学工業";
	item[3][67]="Z7066 ハグオール";
	item[3][68]="Z7067 NTT労働組合 西日本";
	item[3][69]="Z7068 日本ラグビーフットボール協会";
	item[3][70]="Z7069 店舗在庫買取";
	item[3][71]="Z7070 神戸YMCA";
	item[3][72]="Z7071 北洋銀行";

var sqlitem = new Array();
sqlitem[1] = new Array();
sqlitem[2] = new Array();
sqlitem[3] = new Array();

	sqlitem[1][1]= "SUBSTR(T.SELL_NO,1,2)='Z1' ";
	sqlitem[1][2]= "SUBSTR(T.SELL_NO,1,5)='Z1001')";
	sqlitem[1][3]= "SUBSTR(T.SELL_NO,1,5)='Z1002' or SUBSTR(T.SELL_NO,1,5)='Z1006')";
	sqlitem[1][4]= "SUBSTR(T.SELL_NO,1,5)='Z1003')";
	sqlitem[1][5]= "SUBSTR(T.SELL_NO,1,5)='Z1004')";
	sqlitem[1][6]= "SUBSTR(T.SELL_NO,1,5)='Z1005')";
	sqlitem[1][7]= "SUBSTR(T.SELL_NO,1,5)='Z1007')";
	sqlitem[1][8]= "(SUBSTR(T.SELL_NO,1,5)='Z1029')";
	sqlitem[1][9]= "(SUBSTR(T.SELL_NO,1,5)='Z1030')";
	sqlitem[1][10]= "(SUBSTR(T.SELL_NO,1,5)='Z1031')";
	sqlitem[1][11]= "(SUBSTR(T.SELL_NO,1,5)='Z1032')";
	sqlitem[1][12]= "(SUBSTR(T.SELL_NO,1,5)='Z1033')";
	sqlitem[1][13]= "(SUBSTR(T.SELL_NO,1,5)='Z1034')";
    /* 2016/02/12 add */
	sqlitem[1][14]= "(SUBSTR(T.SELL_NO,1,3)='Z11')";
	sqlitem[1][15]= "(SUBSTR(T.SELL_NO,1,5)='Z1100')";
	sqlitem[1][16]= "(SUBSTR(T.SELL_NO,1,5)='Z1111')";
	sqlitem[1][17]= "(SUBSTR(T.SELL_NO,1,5)='Z1112')";
	sqlitem[1][18]= "(SUBSTR(T.SELL_NO,1,5)='Z1113')";
	sqlitem[1][19]= "(SUBSTR(T.SELL_NO,1,5)='Z1114')";


	sqlitem[2][1]= "SUBSTR(T.SELL_NO,1,2)='Z6' ";
	sqlitem[2][2]= "SUBSTR(T.SELL_NO,1,5)='Z6001' ";
	sqlitem[2][3]= "SUBSTR(T.SELL_NO,1,5)='Z6002' ";
	sqlitem[2][4]= "SUBSTR(T.SELL_NO,1,5)='Z6003' ";
	sqlitem[2][5]= "SUBSTR(T.SELL_NO,1,5)='Z6004' ";
	sqlitem[2][6]= "SUBSTR(T.SELL_NO,1,5)='Z6005' ";
	sqlitem[2][7]= "SUBSTR(T.SELL_NO,1,5)='Z6006' ";
	sqlitem[2][8]= "SUBSTR(T.SELL_NO,1,5)='Z6007' ";
	sqlitem[2][9]= "SUBSTR(T.SELL_NO,1,5)='Z6008' ";
	sqlitem[2][10]= "SUBSTR(T.SELL_NO,1,5)='Z6009' ";
	sqlitem[2][11]= "SUBSTR(T.SELL_NO,1,5)='Z6010' ";
	sqlitem[2][12]= "SUBSTR(T.SELL_NO,1,5)='Z6011' ";
	sqlitem[2][13]= "SUBSTR(T.SELL_NO,1,5)='Z6012' ";
	sqlitem[2][14]= "SUBSTR(T.SELL_NO,1,5)='Z6013' ";
	sqlitem[2][15]= "SUBSTR(T.SELL_NO,1,5)='Z6014' ";
	sqlitem[2][16]= "SUBSTR(T.SELL_NO,1,5)='Z6015' ";
	sqlitem[2][17]= "SUBSTR(T.SELL_NO,1,5)='Z6016' ";
	sqlitem[2][18]= "SUBSTR(T.SELL_NO,1,5)='Z6017' ";
	sqlitem[2][19]= "SUBSTR(T.SELL_NO,1,5)='Z6018' ";
	sqlitem[2][20]= "SUBSTR(T.SELL_NO,1,5)='Z6019' ";
	sqlitem[2][21]= "SUBSTR(T.SELL_NO,1,5)='Z6020' ";
	sqlitem[2][22]= "SUBSTR(T.SELL_NO,1,5)='Z6021' ";
	sqlitem[2][23]= "SUBSTR(T.SELL_NO,1,5)='Z6022' ";
	sqlitem[2][24]= "SUBSTR(T.SELL_NO,1,5)='Z6023' ";
	sqlitem[2][25]= "SUBSTR(T.SELL_NO,1,5)='Z6024' ";
	sqlitem[2][26]= "SUBSTR(T.SELL_NO,1,5)='Z6025' ";
	sqlitem[2][27]= "SUBSTR(T.SELL_NO,1,5)='Z6026' ";
	sqlitem[2][28]= "SUBSTR(T.SELL_NO,1,5)='Z6027' ";
	sqlitem[2][29]= "SUBSTR(T.SELL_NO,1,5)='Z6028' ";
	sqlitem[2][30]= "SUBSTR(T.SELL_NO,1,5)='Z6029' ";
	sqlitem[2][31]= "SUBSTR(T.SELL_NO,1,5)='Z6030' ";
	sqlitem[2][32]= "SUBSTR(T.SELL_NO,1,5)='Z6031' ";
	sqlitem[2][33]= "SUBSTR(T.SELL_NO,1,5)='Z6032' ";
	sqlitem[2][34]= "SUBSTR(T.SELL_NO,1,5)='Z6033' ";
	sqlitem[2][35]= "SUBSTR(T.SELL_NO,1,5)='Z6034' ";
	sqlitem[2][36]= "SUBSTR(T.SELL_NO,1,5)='Z6035' ";
	sqlitem[2][37]= "SUBSTR(T.SELL_NO,1,5)='Z6036' ";
	sqlitem[2][38]= "SUBSTR(T.SELL_NO,1,5)='Z6037' ";
	sqlitem[2][39]= "SUBSTR(T.SELL_NO,1,5)='Z6038' ";
	sqlitem[2][40]= "SUBSTR(T.SELL_NO,1,5)='Z6039' ";
	sqlitem[2][41]= "SUBSTR(T.SELL_NO,1,5)='Z6040' ";
	sqlitem[2][42]= "SUBSTR(T.SELL_NO,1,5)='Z6041' ";
	sqlitem[2][43]= "SUBSTR(T.SELL_NO,1,5)='Z6042' ";
	sqlitem[2][44]= "SUBSTR(T.SELL_NO,1,5)='Z6043' ";
	sqlitem[2][45]= "SUBSTR(T.SELL_NO,1,5)='Z6044' ";
	sqlitem[2][46]= "SUBSTR(T.SELL_NO,1,5)='Z6045' ";
	sqlitem[2][47]= "SUBSTR(T.SELL_NO,1,5)='Z6046' ";
	sqlitem[2][48]= "SUBSTR(T.SELL_NO,1,5)='Z6047' ";
	sqlitem[2][49]= "SUBSTR(T.SELL_NO,1,5)='Z6048' ";
	sqlitem[2][50]= "SUBSTR(T.SELL_NO,1,5)='Z6049' ";
	sqlitem[2][51]= "SUBSTR(T.SELL_NO,1,5)='Z6050' ";
	sqlitem[2][52]= "SUBSTR(T.SELL_NO,1,5)='Z6051' ";
	sqlitem[2][53]= "SUBSTR(T.SELL_NO,1,5)='Z6052' ";
	sqlitem[2][54]= "SUBSTR(T.SELL_NO,1,5)='Z6053' ";
	sqlitem[2][55]= "SUBSTR(T.SELL_NO,1,5)='Z6054' ";
	sqlitem[2][56]= "SUBSTR(T.SELL_NO,1,5)='Z6055' ";
	sqlitem[2][57]= "SUBSTR(T.SELL_NO,1,5)='Z6056' ";
	sqlitem[2][58]= "SUBSTR(T.SELL_NO,1,5)='Z6057' ";
	sqlitem[2][59]= "SUBSTR(T.SELL_NO,1,5)='Z6058' ";
	sqlitem[2][60]= "SUBSTR(T.SELL_NO,1,5)='Z6059' ";
	sqlitem[2][61]= "SUBSTR(T.SELL_NO,1,5)='Z6060' ";
	sqlitem[2][62]= "SUBSTR(T.SELL_NO,1,5)='Z6061' ";
	sqlitem[2][63]= "SUBSTR(T.SELL_NO,1,5)='Z6062' ";
	sqlitem[2][64]= "SUBSTR(T.SELL_NO,1,5)='Z6063' ";

	sqlitem[3][1]= "SUBSTR(T.SELL_NO,1,2)='Z7' ";
	sqlitem[3][2]= "(SUBSTR(T.SELL_NO,1,3)='Z16' or SUBSTR(T.SELL_NO,1,5)='Z7001')";
	sqlitem[3][3]= "(SUBSTR(T.SELL_NO,1,3)='Z17' or SUBSTR(T.SELL_NO,1,5)='Z7002')";
	sqlitem[3][4]= "(SUBSTR(T.SELL_NO,1,3)='Z18' or SUBSTR(T.SELL_NO,1,5)='Z7003')";
	sqlitem[3][5]= "SUBSTR(T.SELL_NO,1,5)='Z7004' ";
	sqlitem[3][6]= "SUBSTR(T.SELL_NO,1,5)='Z7005' ";
	sqlitem[3][7]= "SUBSTR(T.SELL_NO,1,5)='Z7006' ";
	sqlitem[3][8]= "SUBSTR(T.SELL_NO,1,5)='Z7007' ";
	sqlitem[3][9]= "SUBSTR(T.SELL_NO,1,5)='Z7008' ";
	sqlitem[3][10]= "SUBSTR(T.SELL_NO,1,5)='Z7009' ";
	sqlitem[3][11]= "SUBSTR(T.SELL_NO,1,5)='Z7010' ";
	sqlitem[3][12]= "SUBSTR(T.SELL_NO,1,5)='Z7011' ";
	sqlitem[3][13]= "SUBSTR(T.SELL_NO,1,5)='Z7012' ";
	sqlitem[3][14]= "SUBSTR(T.SELL_NO,1,5)='Z7013' ";
	sqlitem[3][15]= "SUBSTR(T.SELL_NO,1,5)='Z7014' ";
	sqlitem[3][16]= "SUBSTR(T.SELL_NO,1,5)='Z7015' ";
	sqlitem[3][17]= "SUBSTR(T.SELL_NO,1,5)='Z7016' ";
	sqlitem[3][18]= "SUBSTR(T.SELL_NO,1,5)='Z7017' ";
	sqlitem[3][19]= "SUBSTR(T.SELL_NO,1,5)='Z7018' ";
	sqlitem[3][20]= "SUBSTR(T.SELL_NO,1,5)='Z7019' ";
	sqlitem[3][21]= "SUBSTR(T.SELL_NO,1,5)='Z7020' ";
	sqlitem[3][22]= "SUBSTR(T.SELL_NO,1,5)='Z7021' ";
	sqlitem[3][23]= "SUBSTR(T.SELL_NO,1,5)='Z7022' ";
	sqlitem[3][24]= "SUBSTR(T.SELL_NO,1,5)='Z7023' ";
	sqlitem[3][25]= "SUBSTR(T.SELL_NO,1,5)='Z7024' ";
	sqlitem[3][26]= "SUBSTR(T.SELL_NO,1,5)='Z7025' ";
	sqlitem[3][27]= "SUBSTR(T.SELL_NO,1,5)='Z7026' ";
	sqlitem[3][28]= "SUBSTR(T.SELL_NO,1,5)='Z7027' ";
	sqlitem[3][29]= "SUBSTR(T.SELL_NO,1,5)='Z7028' ";
	sqlitem[3][30]= "SUBSTR(T.SELL_NO,1,5)='Z7029' ";
	sqlitem[3][31]= "SUBSTR(T.SELL_NO,1,5)='Z7030' ";
	sqlitem[3][32]= "SUBSTR(T.SELL_NO,1,5)='Z7031' ";
	sqlitem[3][33]= "SUBSTR(T.SELL_NO,1,5)='Z7032' ";
	sqlitem[3][34]= "SUBSTR(T.SELL_NO,1,5)='Z7033' ";
	sqlitem[3][35]= "SUBSTR(T.SELL_NO,1,5)='Z7034' ";
	sqlitem[3][36]= "SUBSTR(T.SELL_NO,1,5)='Z7035' ";
	sqlitem[3][37]= "SUBSTR(T.SELL_NO,1,5)='Z7036' ";
	sqlitem[3][38]= "SUBSTR(T.SELL_NO,1,5)='Z7037' ";
	sqlitem[3][39]= "SUBSTR(T.SELL_NO,1,5)='Z7038' ";
	sqlitem[3][40]= "SUBSTR(T.SELL_NO,1,5)='Z7039' ";
	sqlitem[3][41]= "SUBSTR(T.SELL_NO,1,5)='Z7040' ";
	sqlitem[3][42]= "SUBSTR(T.SELL_NO,1,5)='Z7041' ";
	sqlitem[3][43]= "SUBSTR(T.SELL_NO,1,5)='Z7042' ";
	sqlitem[3][44]= "SUBSTR(T.SELL_NO,1,5)='Z7043' ";
	sqlitem[3][45]= "SUBSTR(T.SELL_NO,1,5)='Z7044' ";
	sqlitem[3][46]= "SUBSTR(T.SELL_NO,1,5)='Z7045' ";
	sqlitem[3][47]= "SUBSTR(T.SELL_NO,1,5)='Z7046' ";
	sqlitem[3][48]= "SUBSTR(T.SELL_NO,1,5)='Z7047' ";
	sqlitem[3][49]= "SUBSTR(T.SELL_NO,1,5)='Z7048' ";
	sqlitem[3][50]= "SUBSTR(T.SELL_NO,1,5)='Z7049' ";
	sqlitem[3][51]= "SUBSTR(T.SELL_NO,1,5)='Z7050' ";
	sqlitem[3][52]= "SUBSTR(T.SELL_NO,1,5)='Z7051' ";
	sqlitem[3][53]= "SUBSTR(T.SELL_NO,1,5)='Z7052' ";
	sqlitem[3][54]= "SUBSTR(T.SELL_NO,1,5)='Z7053' ";
	sqlitem[3][55]= "SUBSTR(T.SELL_NO,1,5)='Z7054' ";
	sqlitem[3][56]= "SUBSTR(T.SELL_NO,1,5)='Z7055' ";
	sqlitem[3][57]= "SUBSTR(T.SELL_NO,1,5)='Z7056' ";
	sqlitem[3][58]= "SUBSTR(T.SELL_NO,1,5)='Z7057' ";
	sqlitem[3][59]= "SUBSTR(T.SELL_NO,1,5)='Z7058' ";
	sqlitem[3][60]= "SUBSTR(T.SELL_NO,1,5)='Z7059' ";
	sqlitem[3][61]= "SUBSTR(T.SELL_NO,1,5)='Z7060' ";
	sqlitem[3][62]= "SUBSTR(T.SELL_NO,1,5)='Z7061' ";
	sqlitem[3][63]= "SUBSTR(T.SELL_NO,1,5)='Z7062' ";
	sqlitem[3][64]= "SUBSTR(T.SELL_NO,1,5)='Z7063' ";
	sqlitem[3][65]= "SUBSTR(T.SELL_NO,1,5)='Z7064' ";
	sqlitem[3][66]= "SUBSTR(T.SELL_NO,1,5)='Z7065' ";
	sqlitem[3][67]= "SUBSTR(T.SELL_NO,1,5)='Z7066' ";
	sqlitem[3][68]= "SUBSTR(T.SELL_NO,1,5)='Z7067' ";
	sqlitem[3][69]= "SUBSTR(T.SELL_NO,1,5)='Z7068' ";
	sqlitem[3][70]= "SUBSTR(T.SELL_NO,1,5)='Z7069' ";
	sqlitem[3][71]= "SUBSTR(T.SELL_NO,1,5)='Z7070' ";
	sqlitem[3][72]= "SUBSTR(T.SELL_NO,1,5)='Z7071' ";


    /** 子ジャンルのID名 */
    var idName="childS";

    /** 親ジャンルが変更されたら、子ジャンルを生成 */
    function createChildOptions(frmObj) {
        /** 親ジャンルを変数pObjに格納 */
        var pObj=frmObj.elements["parentS"].options;
        /** 親ジャンルのoption数 */
        var pObjLen=pObj.length;
        var htm="<select name='childS' style='width:300px;'>";
        for(i=0; i<pObjLen; i++ ) {
            /** 親ジャンルの選択値を取得 */
            if(pObj[i].selected>0){
                var itemLen=item[i].length;
                for(j=0; j<itemLen; j++){
                  /** 地球の友と歩む会の選択値を非表示 */
                  /** if (j!=33){ */
                    htm+="<option value='"+j+"'>"+item[i][j]+"<\/option>";
                  /** } */
                }
            }
        }
        htm+="<\/select>";
        /** HTML出力 */
        document.getElementById(idName).innerHTML=htm;
    }

    /** 選択されている値をアラート表示 */
    function chkSelect(frmObj) {

 if (document.frmdlvol.T1.value=="")
	{
	alert("開始日付を入力してください");
	return;
	}
 if (document.frmdlvol.T2.value=="")
	{
	alert("終了日付を入力してください");
	return;
	}

var s="";
var idxP=frmObj.elements['parentS'].selectedIndex;
var idxC=frmObj.elements['childS'].selectedIndex;
if(idxP>0){
	s+="親ジャンルの選択肢："+frmObj.elements['parentS'][idxP].text+"\n";
	if(idxC > 0){
		s+="子ジャンルの選択肢："+frmObj.elements['childS'][idxC].text+"\n";
}else{
	s+="子ジャンルが選択されていません\n";
	alert(s);
	return;
}
}else{
	s+="親ジャンルが選択されていません\n";
	alert(s);
	return;
}
//s+=sqlitem[idxP][idxC]+"\n";
//alert(s);
//スターツピタットハウス特別対応
if ((idxP==2)&&(idxC==2)){
	document.frmdlvol.T4.value=9;
}else
{
	document.frmdlvol.T4.value=0;
}
//where句を付与してサブミット
document.frmdlvol.T3.value=sqlitem[idxP][idxC];
document.frmdlvol.submit();


}

    /* onLoad時にプルダウンを初期化 */
    function init(){
        htm ="<select name='childS' style='width:300px;'>";
        htm+="<option value=''>"+item[0][0]+"<\/option>";
        htm+="<\/select>";
        /* HTML出力 */
        document.getElementById("childS").innerHTML=htm;
    }

    /* ページ読み込み完了時に、プルダウン初期化を実行 */
    window.onload=init;
</script>
<br><br>
<input type="button" value="データ出力" onclick="chkSelect(this.form);" />
<input type="reset" value="リセット">
</FORM>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>