<?php
require '../parts/pagechk.inc';
$today = date("YmdHis");
$file_name = "./dlfile/volunteer_" . $today . ".csv";
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
output_log("ボランティア振込済データダウンロード",$_SESSION["user_id"],'volout');
// 前画面からの検索キーワードを取得する
$keyti1 = addslashes(@$_POST["T1"]);
$keyti2 = addslashes(@$_POST["T2"]);
$keyti3 = @$_POST["T3"];
$keyti4 = addslashes(@$_POST["T4"]);

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//

$sql = "SELECT DISTINCT * FROM (SELECT ".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z12','シャンティ国際ボランティア会',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z13','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z14','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z15','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z16','NTT西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z17','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1001','Room to Read',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1002','本で寄付するプロジェクト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1003','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1004','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1005','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1006','移動図書館支援',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1007','かながわキンタロウ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1029','大阪YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1030','グッドネーバーズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1031','国連ウィメン日本協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1032','湘南ふじさわシニアネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1033','アジア保健研修所(AHI)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7001','NTT大阪支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7002','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7003','帝人',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6001','スターツピタットハウス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7004','セガサミーホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7005','NTTネオメイト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7006','NEC',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7007','キャドバリージャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7008','ファミリーマート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6002','大京アステージ(くらしスクエア)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6003','大京アステージ(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6004','三井不動産住宅サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7009','結核予防会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7010','ナック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7011','アイエスエフネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7012','インテリジェンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7013','チャーティス・ファー・イースト・ホールティングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7014','民際センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7015','DNPグループ労連',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7016','しょうがっこうをおくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7017','レオパレス２１(企業ボランティア宅本便)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7018','神奈川大学',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7019','ICAN（アイキャン）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6005','アスク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7020','阪急阪神ホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6006','インターファーム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7021','ソニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7022','地球環境基金',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7023','売って支援プログラム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7024','西友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7025','日本経済新聞文化部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7026','朝日新聞社論説委員室',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7027','HFI（Hope and Faith International）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7028','ACE（エース）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6007','アート引越しセンター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6008','クラブパナソニック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7029','マンパワーグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7030','中日本ハイウェイ・パトロール東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6009','大東建物管理',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7031','三菱商事',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7032','地球の友と歩む会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7033','損保ジャパン奈良支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6010','アリさんマークの引越社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7034','日本興亜損害保険',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6011','イヌイ運送',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7035','3keys（スリーキーズ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7036','ライフ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7037','★スポット対応企業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7038','町田ゼルビア',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6012','日本通運',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7039','SC相模原',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6013','ハウスコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7040','幼い難民を考える会（CYR）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7041','アフラック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7042','JHP学校をつくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7043','日立建機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7044','北海道森と緑の会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7045','ユニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6014','オーネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6015','バイク王',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6016','三菱地所コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6017','コミュニティワン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6018','住友不動産建物サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6019','リトル・ママ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6020','レジデンスクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6021','JAF',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7046','埼玉県自動車販売店協会（自販連埼玉支部）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6022','ぐるなび食市場',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7047','マルハン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7048','アドビシステムズ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7049','FC在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6023','ヤマトホームコンビニエンス（クロネコヤマト引越センター）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6024','ファミリー引越センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6025','ジャパンネット銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7050','大塚商会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7051','森ビル都市企画',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7052','アクセス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6026','住友不動産販売',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7053','ブリヂストンスポーツ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7054','ケア・インターナショナルジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7055','世界の子どもにワクチンを(JCV)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7056','共立メンテナンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6027','三井不動産レジデンシャル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6028','エイブル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6029','MAST(マスト)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6030','フロムヴイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6031','大和ライフネクスト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6032','レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6033','ズバット引越し比較',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6034','インターネットコンシェルジュサービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6035','長谷工コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6036','QLC(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6037','住宅情報館(城南建設)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6038','ジャックスカード',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6039','積村ビル管理',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6040','東急住宅リース',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6041','ハッピークラブモール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6042','おうちCO-OP',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6043','ハイホー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6044','ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7057','ジャパンハート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7058','MSD',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7059','ブリッジ エーシア ジャパン(BAJ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7060','TBS(東日本大震災支援)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7061','TBS(フィリピン台風支援)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7062','岡谷鋼機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7063','横浜開港祭',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7064','商船三井',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7065','広栄化学工業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7066','ハグオール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7067','NTT労働組合 西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7068','日本ラグビーフットボール協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7069','店舗在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7070','神戸YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7071','北洋銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7072','なおちゃんを救う会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7073','honto返品買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7074','シニアライフセラピー研究所',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6045','ソフトバンクグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6046','オリックス・クレジット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6047','リロクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6048','メディアカフェポパイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6049','アメリカン・エキスプレス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6050','オリックス・ゴルフ・マネジメント',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6051','エイコータウン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6052','社員向け_レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6053','macalon+（マカロンプラス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6054','タイムズクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6055','ゆとライフドットコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6056','クラスエル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6057','バイクブロス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6058','泉友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6059','富士フイルム生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6060','リコー三愛グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1034','ウィメンズアクションネットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1100','日本赤十字社 本社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1111','日本赤十字社 埼玉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1112','日本赤十字社 千葉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1113','日本赤十字社 東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1114','日本赤十字社 神奈川',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6061','ヴァリック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6062','HOME’S 引越し',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6063','千葉県庁生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z18','帝人','ERR')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) AS AA,".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"b.TRANSFER_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A,TDOWNLOADHISBANK b ".
"WHERE T.SELL_NO=A.SELL_NO and t.DL_STAT_TRAN=b.DOWN_ID ".
"AND T.SELL_STAT='06'  AND PAY_TP='1' ".
"AND b.TRANSFER_DT>='".$keyti1 . "' ".
"AND b.TRANSFER_DT<='".$keyti2 . "' ".
"AND ".$keyti3 .
"UNION SELECT ".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z12','シャンティ国際ボランティア会',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z13','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z14','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z15','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z16','NTT西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z17','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1001','Room to Read',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1002','本で寄付するプロジェクト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1003','ピースウィンズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1004','シャプラニール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1005','JEN',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1006','移動図書館支援',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1007','かながわキンタロウ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1029','大阪YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1030','グッドネーバーズ・ジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1031','国連ウィメン日本協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1032','湘南ふじさわシニアネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1033','アジア保健研修所(AHI)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7001','NTT大阪支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7002','スターバックス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7003','帝人',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6001','スターツピタットハウス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7004','セガサミーホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7005','NTTネオメイト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7006','NEC',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7007','キャドバリージャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7008','ファミリーマート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6002','大京アステージ(くらしスクエア)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6003','大京アステージ(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6004','三井不動産住宅サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7009','結核予防会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7010','ナック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7011','アイエスエフネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7012','インテリジェンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7013','チャーティス・ファー・イースト・ホールティングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7014','民際センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7015','DNPグループ労連',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7016','しょうがっこうをおくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7017','レオパレス２１(企業ボランティア宅本便)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7018','神奈川大学',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7019','ICAN（アイキャン）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6005','アスク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7020','阪急阪神ホールディングス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6006','インターファーム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7021','ソニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7022','地球環境基金',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7023','売って支援プログラム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7024','西友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7025','日本経済新聞文化部',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7026','朝日新聞社論説委員室',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7027','HFI（Hope and Faith International）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7028','ACE（エース）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6007','アート引越しセンター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6008','クラブパナソニック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7029','マンパワーグループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7030','中日本ハイウェイ・パトロール東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6009','大東建物管理',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7031','三菱商事',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7032','地球の友と歩む会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7033','損保ジャパン奈良支店',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6010','アリさんマークの引越社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7034','日本興亜損害保険',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6011','イヌイ運送',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7035','3keys（スリーキーズ）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7036','ライフ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7037','★スポット対応企業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7038','町田ゼルビア',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6012','日本通運',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7039','SC相模原',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6013','ハウスコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7040','幼い難民を考える会（CYR）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7041','アフラック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7042','JHP学校をつくる会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7043','日立建機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7044','北海道森と緑の会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7045','ユニー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6014','オーネット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6015','バイク王',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6016','三菱地所コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6017','コミュニティワン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6018','住友不動産建物サービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6019','リトル・ママ（東京）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6020','レジデンスクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6021','JAF',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7046','埼玉県自動車販売店協会（自販連埼玉支部）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6022','ぐるなび食市場',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7047','マルハン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7048','アドビシステムズ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7049','FC在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6023','ヤマトホームコンビニエンス（クロネコヤマト引越センター）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6024','ファミリー引越センター',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6025','ジャパンネット銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7050','大塚商会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7051','森ビル都市企画',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7052','アクセス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6026','住友不動産販売',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7053','ブリヂストンスポーツ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7054','ケア・インターナショナルジャパン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7055','世界の子どもにワクチンを(JCV)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7056','共立メンテナンス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6027','三井不動産レジデンシャル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6028','エイブル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6029','MAST(マスト)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6030','フロムヴイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6031','大和ライフネクスト',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6032','レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6033','ズバット引越し比較',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6034','インターネットコンシェルジュサービス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6035','長谷工コミュニティ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6036','QLC(コンシェルジュ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6037','住宅情報館(城南建設)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6038','ジャックスカード',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6039','積村ビル管理',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6040','東急住宅リース',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6041','ハッピークラブモール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6042','おうちCO-OP',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6043','ハイホー',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6044','ＣＬＵＢ　ＳＰＡＳＳ（クラブエスパス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7057','ジャパンハート',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7058','MSD',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7059','ブリッジ エーシア ジャパン(BAJ)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7060','TBS(東日本大震災支援)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7061','TBS(フィリピン台風支援)',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7062','岡谷鋼機',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7063','横浜開港祭',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7064','商船三井',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7065','広栄化学工業',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7066','ハグオール',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7067','NTT労働組合 西日本',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7068','日本ラグビーフットボール協会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7069','店舗在庫買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7070','神戸YMCA',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7071','北洋銀行',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7072','なおちゃんを救う会',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7073','honto返品買取',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z7074','シニアライフセラピー研究所',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6045','ＳＢアットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6046','オリックス・クレジット',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6047','リロクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6048','メディアカフェポパイ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6049','アメリカン・エキスプレス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6050','オリックス・ゴルフ・マネジメント',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6051','エイコータウン',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6052','社員向け_レオパレス21',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6053','macalon+（マカロンプラス）',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6054','タイムズクラブ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6055','ゆとライフドットコム',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6056','クラスエル',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6057','バイクブロス',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6058','泉友',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6059','富士フイルム生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6060','リコー三愛グループ',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1034','ウィメンズアクションネットワーク',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1100','日本赤十字社 本社',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1111','日本赤十字社 埼玉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1112','日本赤十字社 千葉',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1113','日本赤十字社 東京',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z1114','日本赤十字社 神奈川',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6061','ヴァリック',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6062','HOME’S 引越し',".
"DECODE(SUBSTR(T.SELL_NO,1,5),'Z6063','千葉県庁生活協同組合',".
"DECODE(SUBSTR(T.SELL_NO,1,3),'Z18','帝人','ERR')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) AS AA,".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"p.TRANSFER_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A,TDOWNLOADHISPOSTAL p ".
"WHERE T.SELL_NO=A.SELL_NO and t.DL_STAT_TRAN=p.DOWN_ID AND PAY_TP='2' ".
"AND T.SELL_STAT='06' ".
"AND p.TRANSFER_DT>='".$keyti1 . "' ".
"AND p.TRANSFER_DT<='".$keyti2 . "' ".
"AND ".$keyti3 .") ORDER BY SELL_NO";
if ($keyti4==9){
$sql = "SELECT DISTINCT * FROM (SELECT ".
"'スターツピタットハウス',".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"b.TRANSFER_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A,TDOWNLOADHISBANK b ".
"WHERE T.SELL_NO=A.SELL_NO and t.DL_STAT_TRAN=b.DOWN_ID ".
"AND T.SELL_STAT='06'  AND PAY_TP='1' ".
"AND b.TRANSFER_DT>='".$keyti1 . "' ".
"AND b.TRANSFER_DT<='".$keyti2 . "' ".
"AND ".$keyti3 .
"UNION SELECT ".
"'スターツピタットハウス',".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"p.TRANSFER_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A,TDOWNLOADHISPOSTAL p ".
"WHERE T.SELL_NO=A.SELL_NO and t.DL_STAT_TRAN=p.DOWN_ID AND PAY_TP='2' ".
"AND T.SELL_STAT='06' ".
"AND p.TRANSFER_DT>='".$keyti1 . "' ".
"AND p.TRANSFER_DT<='".$keyti2 . "' ".
"AND ".$keyti3 .
" UNION SELECT 'スターツピタットハウス',".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"b.TRANSFER_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A,TDOWNLOADHISBANK b ".
"WHERE T.SELL_NO=A.SELL_NO and t.DL_STAT_TRAN=b.DOWN_ID ".
"AND T.SELL_STAT='06'  AND PAY_TP='1' ".
"AND b.TRANSFER_DT>='".$keyti1 . "' ".
"AND b.TRANSFER_DT<='".$keyti2 . "' ".
"AND memo like '%ピタットハウス%'" .
" UNION SELECT 'スターツピタットハウス',".
"T.SELL_NO,".
"SUBSTR(T.REG_DM,1,8),".
"p.TRANSFER_DT,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"T.PICKUP_NM_LAST||T.PICKUP_NM_ETC,".
"T.PICKUP_ZIP_CD,".
"T.PICKUP_ZIP_ADDR1||T.PICKUP_ZIP_ADDR2||T.PICKUP_ZIP_ADDR3||T.PICKUP_DTL_ADDR,".
"T.PICKUP_TEL_NO,".
"A.BOX_ARRIVED,".
"A.BOOK_MEMO,".
"A.BOOK_ASS_AMT,".
"A.COMIC_MEMO,".
"A.COMIC_ASS_AMT,".
"A.BOX_MEMO,".
"A.BOX_ASS_AMT,".
"A.MUSIC_MEMO,".
"A.MUSIC_ASS_AMT,".
"A.VIDEO_MEMO,".
"A.VIDEO_ASS_AMT,".
"A.GAME_MEMO,".
"A.GAME_ASS_AMT,".
"A.OTHER_MEMO,".
"A.OTHER_ASS_AMT,".
"A.VALID_ITEM_CNT,".
"A.INVALID_ITEM_CNT,".
"A.TOT_ASS_AMT,".
"REPLACE(REPLACE(REPLACE(MEMO,CHR(13),' '),CHR(10),' '),',','') as memo ".
"FROM TSELL T,TSELLASSESSMENT A,TDOWNLOADHISPOSTAL p ".
"WHERE T.SELL_NO=A.SELL_NO and t.DL_STAT_TRAN=p.DOWN_ID AND PAY_TP='2' ".
"AND T.SELL_STAT='06' ".
"AND p.TRANSFER_DT>='".$keyti1 . "' ".
"AND p.TRANSFER_DT<='".$keyti2 . "' ".
"AND memo like '%ピタットハウス%'" .
") ORDER BY SELL_NO";

}
$res = $db->query($sql);
if(DB::isError($res)){
    echo $sql;
    $res->getMessage();
//	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	require '../parts/outputcsv_query.php';
	outputcsv($res,$file_name);
$res->free();
//
$db->disconnect();
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ボランティア振込済データダウンロード</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>