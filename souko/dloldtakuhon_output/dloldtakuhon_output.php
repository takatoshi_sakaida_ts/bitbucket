<?php
//ファイル指定
$today = date("YmdHis");
$file_name = "./dlfile/oldtakuhon_" . $today . ".tsv";
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// SQL文
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
$sql = "SELECT SELL_NO,".
"SELL_STAT,".
"MEM_NO,".
"MEM_ID,".
"PICKUP_NM_FST,".
"PICKUP_NM_MID,".
"PICKUP_NM_LAST,".
"PICKUP_NM_ETC,".
"PICKUP_ZIP_CD,".
"PICKUP_ZIP_ADDR1,".
"PICKUP_ZIP_ADDR2,".
"PICKUP_ZIP_ADDR3,".
"PICKUP_DTL_ADDR,".
"PICKUP_TEL_NO,".
"PICKUP_TEL_NO_2,".
"PICKUP_REQ_DT,".
"PICKUP_REQ_TIME,".
"BOX,".
"NOTE_YN,".
"DISPOSE,".
"RECEIPT_TP,".
"MEM_BIRTH_DT,".
"MEM_SEX,".
"MEM_JOB,".
"PAY_TP,".
"BANK_CD,".
"BANK_BRANCH_CD,".
"BANK_NM,".
"BRANCH_NM,".
"BANK_ACC_TP,".
"BANK_ACC_NO,".
"BANK_ACC_NM,".
"POSTAL_SIGN,".
"POSTAL_ACC_NO,".
"POSTAL_NM,".
"QUESTIONAIRE,".
"MEMO ".
"FROM tsell where (substr(sell_no,1,3)='Z99'  or substr(sell_no,1,5)='Z6007' or substr(sell_no,1,2)='Z1' and pickup_req_dt='20000101')".
"and substr(reg_dm,1,8)=(select to_char(sysdate,'yyyymmdd') from dual) order by sell_no";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	require '../parts/outputcsv_query.php';
	outputcsv_tab($res,$file_name);
//データの開放
$res->free();
$db->disconnect();
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>旧受付宅本便　新番発行ダウンロード</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>