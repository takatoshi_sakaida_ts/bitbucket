<?PHP
	if ($_POST["DANTAI"] == '') {
		die('致命的エラー');
	}
require '../../parts/pagechk.inc';
require '../../parts/utl.inc';
require_once("selectvalue_vol.php");
$_SESSION["HSMAP"]["DANTAI"]=htmlspecialchars(escapevalue($_POST["DANTAI"]));
$_SESSION["HSMAP"]["PICKUP_NM_FST"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_FST"]));
$_SESSION["HSMAP"]["PICKUP_NM_MID"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_MID"]));
$_SESSION["HSMAP"]["PICKUP_NM_LAST"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_LAST"]));
$_SESSION["HSMAP"]["PICKUP_NM_ETC"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_ETC"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_CD"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]=RetPrefecture(htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR1"])));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR2"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR3"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR4"]));
$_SESSION["HSMAP"]["PICKUP_TEL_NO"]=htmlspecialchars(escapevalue($_POST["PICKUP_TEL_NO"]));
$_SESSION["HSMAP"]["MEM_SEX"]=htmlspecialchars(escapevalue($_POST["MEM_SEX"]));
$_SESSION["HSMAP"]["gaido"]=htmlspecialchars(escapevalue($_POST["gaido"]));
$_SESSION["HSMAP"]["BOX"]=htmlspecialchars(escapevalue($_POST["BOX"]));
$_SESSION["HSMAP"]["PICKUP_REQ_DTY"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_DTY"]));
$_SESSION["HSMAP"]["PICKUP_REQ_DTM"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_DTM"]));
$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_DTD"]));
$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_TIME"]));
$_SESSION["HSMAP"]["DANTAI_index"]=htmlspecialchars(escapevalue($_POST["DANTAI_index"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR1_index"]));
$_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_TIME_index"]));
$_SESSION["HSMAP"]["MEM_SEX_index"]=htmlspecialchars(escapevalue($_POST["MEM_SEX_index"]));
$_SESSION["HSMAP"]["gaido_index"]=htmlspecialchars(escapevalue($_POST["gaido_index"]));
$_SESSION["HSMAP"]["MEMO"]=htmlspecialchars(escapevalue($_POST["memo"]));
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ボランティア受付入力確認画面</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" action="vol_ent.php" name ="volcon">
<b>団体名</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>団体名</th>
    <td>
<?PHP
print Retvol($_SESSION["HSMAP"]["DANTAI"]);
?>
    </td>
  </tr>
</TABLE>
<br>
<b>集荷先情報</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>名前</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
?>
    </td>
</TR>
  <tr>
    <th>フリガナ</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_NM_LAST"];
print $_SESSION["HSMAP"]["PICKUP_NM_ETC"];
?>
    </td>
</TR>
  <tr>
    <th>郵便番号</th>
    <td>
<?PHP
print Retzip($_SESSION["HSMAP"]["PICKUP_ZIP_CD"]);
?>
    </td>
  </tr>

  <tr>
    <th>都道府県</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"];
?>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"];
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"];
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"];
?>
    </td>
  </tr>

  <tr>
    <th>電話番号</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_TEL_NO"];
?>
	</td>
  </tr>

  </tr>

  <tr>
    <th>性別</th>
    <td>
<?PHP
print Retseibetu($_SESSION["HSMAP"]["MEM_SEX"]);
?>    </td>
  </tr>
</table>
<BR>
<b>集荷希望日など</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>箱数</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["BOX"];
?>
	</td>
  </tr>
  <tr>
    <th>集荷希望年月日</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_REQ_DTY"];
print '年';
print $_SESSION["HSMAP"]["PICKUP_REQ_DTM"];
print '月';
print $_SESSION["HSMAP"]["PICKUP_REQ_DTD"];
print '日';
?>
    </td>
  </tr>
  <tr>
    <th>集荷希望時間帯</th>
    <td>
<?PHP
print Retshuukajikan($_SESSION["HSMAP"]["PICKUP_REQ_TIME"]);
?>
    </td>
  </tr>
  <tr style="display:none;">
    <th>ガイド発送</th>
    <td>
<?PHP
print Retgaido($_SESSION["HSMAP"]["gaido"]);
?>    </td>
  </tr>
</table>
<BR>
<b>メモ</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>メモ</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["MEMO"];
?>
	</td>
  </tr>


<input type="hidden" name="DANTAI_index" value="
<?PHP
print $_SESSION["HSMAP"]["DANTAI_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
print ($_SESSION["HSMAP"]["MEM_SEX"]);
print '" >';
?>
<input type="hidden" name="gaido_index" value="
<?PHP
print ($_SESSION["HSMAP"]["gaido"]);
print '" >';
?>
</TABLE>
<BR>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="entback()">
<input type="submit" value="　　登録　　">
</FORM>
<script type="text/javascript">
function entback()
{
document.volcon.action="vol.php";
document.volcon.submit();
}
</script>

</body>
</html>