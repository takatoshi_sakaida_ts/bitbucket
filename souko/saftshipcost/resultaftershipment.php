<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti2 = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "注文番号が入力されていません．<br>";
}
if (empty($keyti2)) {
	print "インストアコードが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select s.* from d_slip_goods s,d_reception_goods g where s.instorecode='" .$keyti2."' and g.receptioncode='" . $keyti. "' and s.stockno=g.stockno";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>検索結果</strong><BR> 注文番号：". $keyti."<BR>インストアコード：". $keyti2."<br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td>取引日付</td><td>伝票区分</td><td>ロケーションコード</td><td>ロケーション名</td><td>新古区分</td><td>原価金額</td><td>単価</td><td>消費税金額</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>". $j."</td>";
//取引日付
	print "<td>".substr($row[0],2,4). "-" .substr($row[0],6,2). "-" .substr($row[0],8,2).'/' . $row[0]."</td>";
//伝票区分
switch (substr($row[0],0,2)){
case "00":
	$dkubun="B2C売上出庫";
	break;
case "01":
	$dkubun="B2B売上出庫";
	break;
case "10":
	$dkubun="管転出庫";
	break;
case "11":
	$dkubun="廃棄出庫";
	break;
case "12":
	$dkubun="帳在差異出庫";
	break;
case "13":
	$dkubun="新刊返品出庫";
	break;
case "70":
	$dkubun="B2C仕入取消出庫";
	break;
case "71":
	$dkubun="B2B仕入取消出庫";
	break;
case "72":
	$dkubun="B2B仕入(発注)取消出庫";
	break;
case "20":
	$dkubun="B2C仕入入庫";
	break;
case "21":
	$dkubun="B2B仕入入庫";
	break;
case "22":
	$dkubun="B2B仕入(発注)入庫";
	break;
case "30":
	$dkubun="転換入庫";
	break;
case "32":
	$dkubun="帳在差異入庫";
	break;
case "50":
	$dkubun="B2C売上取消入庫";
	break;
case "51":
	$dkubun="B2B売上取消入庫";
	break;
case "52":
	$dkubun="廃棄取消入庫";
	break;
}

print "<td>".$dkubun. "</td>";

//ロケーションコード
	print "<td>".$row[3] ."</td>";
	print "<td>".Retlocation($row[3]) ."</td>";
//在庫区分
switch ($row[5]){
	case 0:
		$zaiko= "中古";
		break;
	case 1:
		$zaiko= "新古";
		break;
	case 2:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
//新古区分
		print "<td>".$zaiko."</td>";
//原価金額
		print "<td>".$row[6]."</td>";
//単価
		print "<td>".$row[7]."</td>";
//消費税金額
		print "<td>".$row[8]."</td>";
//配送番号
/*
	print "<td> ".substr($row[4],0,3). "-" .substr($row[4],3,2). "-" .substr($row[4],5,3). "-" .substr($row[4],8,4). "</td>";
*/
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>