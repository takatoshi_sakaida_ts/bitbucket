<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>クーポン使用者リスト</title>
<link type="text/css" rel="stylesheet" media="all" href="/css/bolweb-common.css">
<script language="JavaScript" src="/js/jquery-1.4.3.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/bolweb-common.js" type="text/javascript"></script>

</head>
<body bgcolor="#ffffff">
<?php

$hasInvalid = FALSE;
$from = @$_GET["from"];
$to = @$_GET["to"];
$CPNID = @$_GET["CPNID"];
$status = @$_GET["status"];
if (empty($from) && empty($to) && empty($CPNID) && empty($status)) {
  $hasInvalid = TRUE;
}
if (!empty($from) || !empty($to)) {
	if (empty($from)) {
	  $hasInvalid = TRUE;
	}
	if (empty($to)) {
	  $hasInvalid = TRUE;
	}
}

print "<form action=\"/souko/coupon_user_list/coupon_user_list.php\">".
"注文日付<input name=\"from\" type=\"text\" maxlength=\"8\" value=\"".$from."\"/>〜<input name=\"to\" type=\"text\" value=\"".$to."\" maxlength=\"8\"/><br><br>".
"ステータス<select name=\"status\">".
"<option></option>";
if ($status == "09") {
print "<option value=\"09\" selected=\"selected\">09:全て</option>";
} else {
print "<option value=\"09\">09:全て</option>";
}
if ($status == "00") {
print "<option value=\"00\" selected=\"selected\">00:納品書未印刷</option>";
} else {
print "<option value=\"00\">00:納品書未印刷</option>";
}
if ($status == "01") {
print "<option value=\"01\" selected=\"selected\">01:納品書印刷済</option>";
} else {
print "<option value=\"01\">01:納品書印刷済</option>";
}
if ($status == "02") {
print "<option value=\"02\" selected=\"selected\">02:出荷準備完了</option>";
} else {
print "<option value=\"02\">02:出荷準備完了</option>";
}
if ($status == "03") {
print "<option value=\"03\" selected=\"selected\">03:出荷完了(EC連携：未)</option>";
} else {
print "<option value=\"03\">03:出荷完了(EC連携：未)</option>";
}
if ($status == "04") {
print "<option value=\"04\" selected=\"selected\">04:出荷完了(EC連携：済)</option>";
} else {
print "<option value=\"04\">04:出荷完了(EC連携：済)</option>";
}
print "</select><br><br>".
"クーポンID<br>".
"　　　　　<textarea name=\"CPNID\" rows=\"30\" cols=\"20\">".$CPNID."</textarea><br>".
"<input type=\"submit\" value=\"検索\"/>".
"<input type=\"hidden\" name=\"isSubmit\" value=\"on\"/>".
"</form>".
"<FORM action=\"/index.php\"><INPUT TYPE=\"submit\" VALUE=\"戻る\"></FORM>";

if($hasInvalid) {
  print "注文日付の開始と終了日をYYYYMMDD(年月日)8桁の数字で指定してください。";
  print "<br>クーポンIDを複数入力する場合は、改行区切りで入力してください。";
  print "<br>ステータス選択は任意です。";
} else {
//PEARの利用     -------(1)
require_once("DB.php");

//ログイン情報の読み込み
//require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\ec\login_ec.php");
require_once("../parts/login_souko.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
$db = DB::connect($dsn);
if(DB::isError($db)){
  echo "Fail\n" . DB::errorMessage($db) . "\n";
}

$keyti= date("Ymd",mktime(0, 0, 0, date("m"), date("d"),   date("Y")));

$sql = "select ".
"a.receptioncode, ".
"a.customername1, ".
"a.customername2, ".
"a.customertelno1, ".
"a.customer_email, ".
"a.coupon_id, ".
"a.coupon_name, ".
"a.receptiondate, ".
"b.deliverydatetime, ".
"a.status_kind, ".
"a.reception_amount, ".
"R.CNT ".
"from ".
"d_reception a, ".
"d_reception_shipment b, ".
"(SELECT rg.RECEPTIONCODE,count(*) as CNT ".
"FROM D_RECEPTION r, D_RECEPTION_GOODS rg ".
"where r.receptioncode = rg.RECEPTIONCODE ";

if(!empty($from) && !empty($to)) {
$sql = $sql."and r.receptiondate >= '$from"."' ".
"and r.receptiondate <= '$to"."' ";
}

if(!empty($status)) {
$sql = $sql."and r.status_kind = '$status"."' ";
}

if(!empty($CPNID)) {
$sql = $sql." and r.coupon_id in(";
$coupon_id_CPNID = addslashes($CPNID);
$new_text = preg_replace("/(\r\n|\n|\r)/", ",", $coupon_id_CPNID);
$new_text = preg_replace("/,,/", "", $new_text);
$new_text = preg_replace("/,$/", "", $new_text);
$arr_coupon_id_CPNID = explode(",", $new_text);
for ($i = 0; $i < count($arr_coupon_id_CPNID); $i++) {
	if($i == count($arr_coupon_id_CPNID) - 1) {
	$sql = $sql."'".$arr_coupon_id_CPNID[$i]."'";
	} else {
	$sql = $sql."'".$arr_coupon_id_CPNID[$i]."',";
	}
}
$sql = $sql.") ";
}

$sql = $sql."group by rg.receptioncode ) R ".
"where ".
"a.receptioncode = b.receptioncode(+) ".
"and b.receptioncode = R.receptioncode(+) ";
if(!empty($from) && !empty($to)) {
$sql = $sql."and a.receptiondate >= '$from"."' ".
" and a.receptiondate <= '$to"."' ";
}

if (!empty($status)) {
$sql = $sql." and a.status_kind = '$status"."' ";
}

if(!empty($CPNID)) {
$sql = $sql." and a.coupon_id in(";
$coupon_id_CPNID = addslashes($CPNID);
$new_text = preg_replace("/(\r\n|\n|\r)/", ",", $coupon_id_CPNID);
$new_text = preg_replace("/,,/", "", $new_text);
$new_text = preg_replace("/,$/", "", $new_text);
$arr_coupon_id_CPNID = explode(",", $new_text);
for ($i = 0; $i < count($arr_coupon_id_CPNID); $i++) {
	if($i == count($arr_coupon_id_CPNID) - 1) {
	$sql = $sql."'".$arr_coupon_id_CPNID[$i]."'";
	} else {
	$sql = $sql."'".$arr_coupon_id_CPNID[$i]."',";
	}
}
$sql = $sql.") ";
}

$sql = $sql."order by a.coupon_id, a.receptioncode";

//print $status;
// print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<div>";
print "<table>\n";
print "<tr>\n";
//項目名の表示
print "<th>注文番号</th><th>氏名</th><th>電話番号</th><th>メールアドレス</th><th>クーポンコード</th><th>クーポン名</th><th>注文日時</th><th>出荷日時</th><th>ステータス</th><th>注文合計金額</th><th>注文点数</th></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
	print "<td class=\"code\">". $row[0]."</td>";
	print "<td class=\"text\">".$row[1].$row[2] ."</td>";
	print "<td class=\"text\">".$row[3] ."</td>";
	print "<td class=\"text\">".$row[4] ."</td>";
	print "<td class=\"text\">".$row[5] ."</td>";
	print "<td class=\"text\">".$row[6] ."</td>";
	print "<td class=\"text\">".$row[7] ."</td>";
	print "<td class=\"text\">".substr($row[8],0,8) ."</td>";
	if ($row[9] == "00") {
		print "<td class=\"text\">納品書未印刷</td>";
	} else if ($row[9] == "01") {
		print "<td class=\"text\">納品書印刷済</td>";
	} else if ($row[9] == "02") {
		print "<td class=\"text\">出荷準備完了</td>";
	} else if ($row[9] == "03") {
		print "<td class=\"text\">出荷完了(EC連携：未)";
	} else if ($row[9] == "04") {
		print "<td class=\"text\">出荷完了(EC連携：済)";
	} else if ($row[9] == "09") {
		print "<td class=\"text\">全て";
	} else {
		print "<td class=\"text\">不明なステータス</td>";
	}
	print "<td class=\"text\">".$row[10] ."</td>";
	print "<td class=\"text\">".$row[11] ."</td>";
}
print "</table>";
print "</div>";
//データの開放
$res->free();
$db->disconnect();

}
?>
</body>
</html>