<?PHP
print "sinkosoft 処理開始".date("YmdHis")."\r\n";
//変数
$check=0;
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//ファイルの読み込み
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
$today = date("YmdHis");
$todayfile = date("Ymd");
//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres;
	fputs($fp,$str);
	fclose( $fp );
}

$contents = @file("cd.dat");
//*************************************************************************************
//ファイル読み込み
//*************************************************************************************
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
	die(99);
}
foreach($contents as $readkeisanline){
	$print_log="";
	$inline=preg_split('/,/',$readkeisanline);
//		$inline[4]=preg_replace("/(\r\n|\n|\r)/","",$inline[4]);
	$sql="select d.instorecode,d.GOODSNAME,d.GOODSNAMEKANA,a.AUTHORNAME,a.AUTHORNAMEKANA from m_goods_display d,m_goods_author a where d.instorecode=a.instorecode(+)  and d.instorecode='".$inline[0]."' and rownum=1 order by AUTHORROLE_KIND";
	$res = $db->query($sql);
		if(DB::isError($res)){
			print $res->getMessage();
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
	while($row = $res->fetchRow()){
		$print_log=$inline[0].",".$inline[1].",".$inline[2].",".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$inline[3].",".$inline[4].",".$inline[5].",".$inline[6];
	}
//print $print_log."\r\n";
if ($check==0)
{
	outputcsv("インストア,JAN,ジャンルコード,タイトル,タイトルカナ,代表アーティスト,代表アーティストカナ,定価,新品販売価格,新古在庫数,中古在庫数\r\n","./dlfile/sinkozaiko_cd_" . $todayfile . ".csv");
}
outputcsv($print_log,"sinkozaiko_cd_" . $todayfile . ".csv");
$check++;
}
unset($res);
unset($row);
unset($inline);
unset($contents);

$contents = @file("dvd.dat");
$check=0;
//*************************************************************************************
//ファイル読み込み
//*************************************************************************************
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
	die(99);
}
foreach($contents as $readkeisanline){
	$print_log="";
	$inline=preg_split('/,/',$readkeisanline);
//		$inline[4]=preg_replace("/(\r\n|\n|\r)/","",$inline[4]);
	$sql="select d.instorecode,d.GOODSNAME,d.GOODSNAMEKANA,a.AUTHORNAME,a.AUTHORNAMEKANA from m_goods_display d,m_goods_author a where d.instorecode=a.instorecode(+)  and d.instorecode='".$inline[0]."' and rownum=1 order by AUTHORROLE_KIND";
	$res = $db->query($sql);
		if(DB::isError($res)){
			print $res->getMessage();
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
	while($row = $res->fetchRow()){
		$print_log=$inline[0].",".$inline[1].",".$inline[2].",".$row[1].",".$row[2].",'".$row[3].",'".$row[4].",'".$inline[3].",".$inline[4].",".$inline[5].",".$inline[6];
	}
//print $print_log."\r\n";
if ($check==0)
{
	outputcsv("インストア,JAN,ジャンルコード,タイトル,タイトルカナ,代表アーティスト,代表アーティストカナ,定価,新品販売価格,新古在庫数,中古在庫数\r\n","./dlfile/sinkozaiko_dvd_" . $todayfile . ".csv");
}
outputcsv($print_log,"sinkozaiko_dvd_" . $todayfile . ".csv");
$check++;
}
unset($res);
unset($row);
unset($inline);
unset($contents);

$contents = @file("game.dat");
$check=0;
//*************************************************************************************
//ファイル読み込み
//*************************************************************************************
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
	die(99);
}
foreach($contents as $readkeisanline){
	$print_log="";
	$inline=preg_split('/,/',$readkeisanline);
//		$inline[4]=preg_replace("/(\r\n|\n|\r)/","",$inline[4]);
	$sql="select d.instorecode,d.GOODSNAME,d.GOODSNAMEKANA,a.AUTHORNAME,a.AUTHORNAMEKANA from m_goods_display d,m_goods_author a where d.instorecode=a.instorecode(+)  and d.instorecode='".$inline[0]."' and rownum=1 order by AUTHORROLE_KIND";
	$res = $db->query($sql);
		if(DB::isError($res)){
			print $res->getMessage();
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
	while($row = $res->fetchRow()){
		$print_log=$inline[0].",".$inline[1].",".$inline[2].",".$row[1].",".$row[2].",'".$row[3].",'".$row[4].",'".$inline[3].",".$inline[4].",".$inline[5].",".$inline[6];
	}
//print $print_log."\r\n";
if ($check==0)
{
	outputcsv("インストア,JAN,ジャンルコード,タイトル,タイトルカナ,代表アーティスト,代表アーティストカナ,定価,新品販売価格,新古在庫数,中古在庫数\r\n","./dlfile/sinkozaiko_game_" . $todayfile . ".csv");
}
outputcsv($print_log,"sinkozaiko_game_" . $todayfile . ".csv");
$check++;
}
unset($res);
unset($row);
unset($inline);
unset($contents);


$db->disconnect();
print "正常終了".date("YmdHis")."\r\n";
?>
