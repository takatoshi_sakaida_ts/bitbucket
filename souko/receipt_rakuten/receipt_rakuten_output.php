<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>領収書発行</title>
<script type="text/javascript">
function BoxChecked(check){
	var form_name = 'detail[]';
	for (i=0; i<document.fmreceipt.elements[form_name].length; i++ ){
		document.fmreceipt.elements[form_name][i].checked =check;
	}
}
</SCRIPT>
</head>
<body>
<FORM method="POST" action="receipt_rakuten_print.php" name="fmreceipt">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_GET["receptioncode"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) && empty($keyti2)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
$sql1 = "select d.ord_no,substr(d.ord_no,1,8),d.ORDR_NM_FST,d.ORDR_NM_MID,(d.TOT_INV_AMT-d.ORD_CNCL_AMT-decode(d.EXT_USED_POINT,null,0,d.EXT_USED_POINT)),";
$sql1 = $sql1 . "d.TOT_TAX,d.CASH_COM,d.DELV_COM,d.PAY_TP,";
//$sql = $sql . "g.instorecode,(trunc(round(g.REAL_SALE_PR*1.05,2),0)),m.GOODS_NAME1||m.GOODS_NAME2||m.GOODS_NAME3,g.STOCK_TP,m.REMARK2,(GOODS_CNT-ORD_CNCL_CNT), ";
$sql1 = $sql1 . "g.instorecode,g.REAL_SALE_PR,m.GOODS_NAME1||m.GOODS_NAME2||m.GOODS_NAME3,g.STOCK_TP,m.REMARK2,(GOODS_CNT-ORD_CNCL_CNT), ";
$sql1 = $sql1 . "d.EXT_ORD_NO,d.EXT_USED_POINT, ORD_SEQ ";

$sql2 = " where d.ord_no=g.ord_no and g.instorecode=m.instorecode and (g.GOODS_CNT-g.ORD_CNCL_CNT)>0 ";
$sql2 = $sql2 . " and d.ord_no ='".$keyti."'";

$sql = $sql1 . " from torder d,torderdtl g,tgoods_bko m" . $sql2 . " union all ";
$sql = $sql . $sql1 . " from torder_old d,torderdtl_old g,tgoods_bko m" . $sql2 . " order by ORD_SEQ";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$j=0;
while($row = $res->fetchRow()){
	if ($j==0){
		//入力項目
print "<strong><BR>購入内容詳細　注文番号：". $row[15]. " ／ " . $keyti."</strong><HR>";
		print '<table>';
		$goukei_kingaku=($row[4]+$row[16]);
		print '<tr><td></td><td>発行日</td><td><input type="text" name="hakkoubi" value="' . date("Y/m/d") .'"size=12></TD></TR>';
		print '<tr><td></td><td>お名前</td><td><input type="text" name="name" value="' . $row[2].$row[3].'"size=60></TD></TR>';
		print '<tr><td></td><td>商品名</td><td><input type="text" name="sinamei" value="書籍代" size=60></TD></TR>';
		print '<tr><td></td><td>領収金額</td><td><input type="text" name="goukei" value="' . $row[4].'"></TD></TR>';
		print '<tr><td><input type="checkbox" name="credit" value="1"></td><td>支払方法</td><td>クレジットカード払い</TD></TR>';
		print '<tr><td><input type="checkbox" name="souryouc" value="1"></td><td>送料</td><td><input type="text" name="souryou" value="' . $row[7].'"></TD></TR>';
		print '<tr><td><input type="checkbox" name="daibikic" value="1"></td><td>代引手数料</td><td><input type="text" name="daibiki" value="' . $row[6].'"></TD></TR>';
		print '<tr><td></td><td>ご請求金額（税込）</td><td><input type="text" name="syoukei" value="' . $goukei_kingaku .'"></TD></TR>';
		print '<tr><td></td><td>ポイント利用額</td><td><input type="text" name="point" value="' . $row[16] .'"></TD></TR>';
//		print '<tr><td></td><td>消費税(5%)</td><td><input type="text" name="zei" value="' . $row[5].'"></TD></TR>';
		print '</table>';
		print '<input type="hidden" name="receptioncode" value="' . $row[0] . '">';
		print '<input type="hidden" name="orderdate" value="' . $row[1] . '">';
		print '<input type="hidden" name="gaibuno" value="' . $row[15] . '">';
//		print '<HR><font size=-1><input type="checkbox" name="detailc" value="1">すべての商品にチェックする</font>';
?>
		<HR><INPUT TYPE="button" onClick="BoxChecked(true);" VALUE="全て選択"> 
		<INPUT TYPE="button" onClick="BoxChecked(false);" VALUE="全て未選択"><BR>
<?PHP
		print '<table border=1>';
		print '<tr bgcolor=#ff9933>';
//項目名の表示
		print "<td nowrap width=10>　</td><td nowrap width=40>項番</td><td nowrap width=120>区分</td><td nowrap width=300>商品名</td><td nowrap width=120>単価</td><td nowrap width=120>数量</td>";
		print "</tr>";
	}
	$j++;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	print "<tr>";
//チェックボックス
//	print '<td><input type="checkbox" name="detail[]" value='.Retzaikokubunforuserec($row[12]).'$$'.$row[11].'$$'.$row[10].'$$'.$row[14].'"></td>';
	print '<td><input type="checkbox" name="detail[]" value="'.$j.'"></td>';
//項番
	print "<td>".$j ."</td>";
//在庫区分
	print "<td>".Retzaikokubunforuserec($row[12])."</td>";
//商品名
	print "<td>".$row[11] ."</td>";
//単価
	print "<td>".$row[10] ."</td>";
//数量
	print "<td>".$row[14] ."</td>";
//セッション格納
	print '<input type="hidden" name="kubun'.$j.'" value="' . Retzaikokubunforuserec($row[12]) . '">';
	print '<input type="hidden" name="item'.$j.'" value="' . $row[11] . '">';
	print '<input type="hidden" name="tanka'.$j.'" value="' . $row[10] . '">';
	print '<input type="hidden" name="suu'.$j.'" value="' . $row[14] . '">';
	print "</TR>";
}
	print "</table>";

//件数
	print '<input type="hidden" name="count" value="' . $j . '">';
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<input type="submit" value="領収書発行">
<INPUT TYPE="BUTTON" VALUE="　戻　る　" onClick="history.back()"><HR>
担当者名(必ず入力してください)：<input tupe='text' name='tantou'>
</FORM>
</body>
</html>