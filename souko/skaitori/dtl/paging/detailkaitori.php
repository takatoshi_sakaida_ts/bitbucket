<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<link rel="stylesheet" type="text/css" href="styles.css">
<title>【詳細版】買取検索結果</title>
</head>
<body bgcolor="#ccccff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//require_once("../parts/selectvalue_souko.php");
require_once("../../../parts/selectvalue_souko.php");

//ログイン情報の読み込み
//require_once("../parts/login_souko.php");
require_once("../../../parts/login_souko.php");

//関数定義ファイル読み込み
require_once("./functions.php");


// 定数定義
$LIMIT = 500;	// 1ページ最大表示数

// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_GET["receptioncode"]);
$keysort = addslashes(@$_GET["keysort"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ソート順の設定
if ($keysort=="1") {
	$_SESSION["kaitori_sort"]=1;
}else
{
	$_SESSION["kaitori_sort"]=0;
}

//取得データの行数を取得
$dataCount = getRowCount($keyti, $db);

// ページ数を計算
$pageCount = getPageCount($dataCount, $LIMIT);

//ページ位置を指定
if( isset($_GET['page']) ){
	$pageNum = intval($_GET['page']);
}else{
	$pageNum = 1;
}

$sql = getSql($keyti, $pageNum, $LIMIT, $keysort);

//SQL文のCOUNT関数を使用
//$sql = "select d.ASSCODE,d.DETAILNO,d.INSTORECODE,d.DETAILPRICE,d.DETAILQTY,d.BASEBPRICE,d.TAX_PRICE,d.DOWNPRICE,";
//$sql = $sql . "d.MODIFYDATE,d.MODIFYTIME,d.ROUTING_KIND,g.goodsname,g.GOODSNAME_EXT1,m.genre0code ";
//$sql = "select d.ASSCODE,d.DETAILNO,d.INSTORECODE,d.DETAILPRICE,d.DETAILQTY,d.BASEBPRICE,d.TAX_PRICE,d.DOWNPRICE,d.MODIFYDATE,d.MODIFYTIME,d.ROUTING_KIND,g.GOODSNAME,g.GOODSNAME_EXT1,m.GENRE0CODE,g.JANCODE,m.GENRE1CODE,m.GENRE2CODE,m.GENRE3CODE,g.AUTHORNAME ";
//$sql = $sql . "from d_ass_goods d,m_goods_display g,m_goods m ";
//$sql = $sql . "where d.instorecode=g.instorecode and d.instorecode=m.instorecode and d.asscode ='".$keyti ."' ";
if ($_SESSION["kaitori_sort"]==0)
{
	$sql = $sql ."order by DETAILNO";
}else
{
	$sql = $sql ."order by ROUTING_KIND,DETAILNO";
}

$meisai=0;
$suuryou=0;
$kijyun=0;
$goukei=0;
$genten=0;
$boliki=0;
$masutaari=0;
$masutanasi=0;
$logiiki=0;
$dcount=0;
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//ヘッダ部分の表示
print "<strong><BR>買取受付番号：". $keyti."</strong><br><br>\n<div style='margin: 10px; font-size: 13px;'>";
if ($_SESSION["kaitori_sort"]==0)
{
	print "<a href='detailkaitori.php?receptioncode=" .$keyti . "&keysort=1&page=". $pageNum ."'>行き先順に並替</a>";
}else
{
	print "<a href='detailkaitori.php?receptioncode=" .$keyti . "&keysort=0&page=". $pageNum ."'>明細番号順に並替</a>";
}
print "</div>";

// ページング
print "<ul class='pageNav'>";
// 前へ
if( $pageNum > 1 ){
	print "<li><a href='detailkaitori.php?receptioncode=". $keyti ."&keysort=". $keysort ."&page=". ($pageNum-1) ."'><< 前</a></li>";
}
else{
	print "<li><span><< 前</span></li>";
}
for( $i=1; $i<=$pageCount; $i++ ){
	if( $i == $pageNum ){
		print "<li><span>".$i."</span></li>";
	}
	else{
		print "<li><a href='detailkaitori.php?receptioncode=". $keyti ."&keysort=". $keysort ."&page=". $i ."'>". $i ."</a></li>";
	}
}
// 次へ
if( $pageNum < $pageCount ){
	print "<li><a href='detailkaitori.php?receptioncode=". $keyti ."&keysort=". $keysort ."&page=". ($pageNum+1) ."'>次 >></a></li>";
}
else{
	print "<li><span>次 >></span></li>";
}

print "</ul>";

//検索結果の表示
print "<table id='detail' border=1>\n";
//print "<tr bgcolor=#ccffff>\n";

//項目名の表示
print <<< EOM
<tr bgcolor=#ccffff>
<td>明細番号</td>
<td>インストアコード</td>
<td>JANコード</td>
<td>行先</td>
<td>カテゴリ</td>
<td>ジャンル1</td>
<td>ジャンル2</td>
<td>ジャンル3</td>
<td>商品名</td>
<td>著者/アーティスト</td>
<td>数量</td>
<td>基準買取金額</td>
<td>減点金額</td>
<td>買取金額</td>
<td>販売金額</td>
<td>在庫点数</td>
</tr>
EOM;

//print "<td nowrap width=70>明細番号</td><td nowrap width=120>インストアコード</td><td nowrap width=80>行先</td><td nowrap width=80>カテゴリ</td><td nowrap width=300>商品名</td>";
//print "<td nowrap width=40>数量</td><td nowrap width=100>基準買取金額</td>";
//print "<td nowrap width=80>減点金額</td><td nowrap width=80>買取金額</td></tr>";

$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
	print "<tr>";
//明細番号
	print "<td>".$row['DETAILNO'] ."</td>";
//instorecode
		print "<td>".$row['INSTORECODE'] ."</td>";
//JANコード
		print "<td>".$row['JANCODE'] ."</td>";
//行き先
		print "<td>".Retkaitoriikisaki($row['ROUTING_KIND']) ."</td>";
//カテゴリ
		print "<td>".Retgoodscategory($row['GENRE0CODE']) ."</td>";
//ジャンル1
		print "<td>".getGenreName($row['GENRE1CODE'], $db) ."</td>";
//ジャンル2
		print "<td>".getGenreName($row['GENRE2CODE'], $db) ."</td>";
//ジャンル3
		print "<td>".getGenreName($row['GENRE3CODE'], $db) ."</td>";
//商品名
	print "<td>".$row['GOODSNAME'] . "　" .$row['GOODSNAME_EXT1'] ."</td>";
//著者/アーティスト
	print "<td>".$row['AUTHORNAME'] ."</td>";
//数量
	print "<td align=right>".$row['DETAILQTY'] ."</td>";
//基準買取価格
	print "<td align=right>".$row['BASEBPRICE'] ."</td>";
//減点金額
	print "<td align=right>".$row['DOWNPRICE'] ."</td>";
//買取金額金額
	print "<td align=right>".$row['TAX_PRICE'] ."</td>";
//販売金額
	print "<td align=right>".$row['PRICE'] ."</td>";
//在庫点数
	print "<td align=right>".$row['STOCK'] ."</td>";
	print "</tr>";
$meisai=$meisai+$row['DETAILPRICE'];
$suuryou=$suuryou+$row['DETAILQTY'];
$kijyun=$kijyun+$row['BASEBPRICE'];
$goukei=$goukei+$row['TAX_PRICE'];
$genten=$genten+$row['DOWNPRICE'];
	$j++;
//行き先のカウント
	switch ($row['ROUTING_KIND']) {
		case 0:
			$dcount=$dcount+$row['DETAILQTY'];
			break;
		case 1:
			$boliki=$boliki+$row['DETAILQTY'];
			break;
		case 2:
			$logiiki=$logiiki+$row['DETAILQTY'];
			break;
		case 3:
			$masutaari=$masutaari+$row['DETAILQTY'];
			break;
		case 4:
			$masutanasi=$masutanasi+$row['DETAILQTY'];
			break;
		default:
			$retvalue= "エラー";
			break;
	}
}
print "<TR><B><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD align=right>" . $suuryou . "</TD><TD align=right>" . $kijyun . "</TD><TD align=right>" . $genten . "</TD><TD align=right>" . $goukei . "</TD></B></TR>";
print "</table>";
//データの開放
$res->free();

// ページング
print "<ul class='pageNav'>";
// 前へ
if( $pageNum > 1 ){
	print "<li><a href='detailkaitori.php?receptioncode=". $keyti ."&keysort=". $keysort ."&page=". ($pageNum-1) ."'><< 前</a></li>";
}
else{
	print "<li><span><< 前</span></li>";
}
for( $i=1; $i<=$pageCount; $i++ ){
	if( $i == $pageNum ){
		print "<li><span>".$i."</span></li>";
	}
	else{
		print "<li><a href='detailkaitori.php?receptioncode=". $keyti ."&keysort=". $keysort ."&page=". $i ."'>". $i ."</a></li>";
	}
}
// 次へ
if( $pageNum < $pageCount ){
	print "<li><a href='detailkaitori.php?receptioncode=". $keyti ."&keysort=". $keysort ."&page=". ($pageNum+1) ."'>次 >></a></li>";
}
else{
	print "<li><span>次 >></span></li>";
}
print "</ul>";



//行き先集計の表示
print "<strong><BR>行き先毎の商品数</strong>";
print "<TABLE border=1><TR><B><TD>行き先</TD><TD>数量</TD></B>";
print "<TR><TD>BOL行き</TD><TD>".$boliki."</TD></TR>";
print "<TR><TD>ロジ行き</TD><TD>".$logiiki."</TD></TR>";
print "<TR><TD>マスタ有り</TD><TD>".$masutaari."</TD></TR>";
print "<TR><TD>マスタ無し</TD><TD>".$masutanasi."</TD></TR></table>";
//print "<TR><TD>D商品数</TD><TD>".$dcount."</TD></TR></table>";
//
$sql = "select *";
$sql = $sql . " from d_ass_discard";
$sql = $sql . " where asscode ='".$keyti."' order by booksoft_kind";
//print $sql;
$res2 = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>値段のつかない商品</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=70>種類</td><td nowrap width=70>数量</td><td nowrap width=300>廃棄理由区分</td>";
print "<td nowrap width=300>備考</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row2 = $res2->fetchRow()){
	print "<tr>";
//種類
	print "<td>".Retbooksoftkind($row2[1]) ."</td>";
//数量
		print "<td>".$row2[2]."</td>";
//廃棄理由
	print "<td>";
	$num=decbin($row2[3]);
	$i=0;
	$len=strlen($num);
//print "num".decbin($row2[3])."<BR>";
	while($i<$len){
		if (substr($num,$i,1)==1){
//print $len .":".$i."<BR>";
//print ($len-$i) . ":".$row2[1].":".Retbooksoftd($len-$i,$row2[1]). "<BR>";
			print Retbooksoftd($len-$i,$row2[1]). "<BR>";
//			print $len-$i .":".$row2[1];
//print decbin($row2[3]);
		}
		$i++;
	}
	print "　</td>";
//備考
	print "<td>　".$row2[4] ."</td>";
	print "</tr>";
	$j++;
}
print "</table>";
//データの開放
$res2->free();
$db->disconnect();


?>
<BR>
<FORM action="searchkaitori.php"><INPUT TYPE="submit" VALUE="買取検索画面に戻る"></FORM>
</body>
</html>