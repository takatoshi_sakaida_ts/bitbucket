<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>【詳細版】買取検索結果</title>
</head>
<body bgcolor="#ccccff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//require_once("../parts/selectvalue_souko.php");
require_once("../../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti = ereg_replace("Z", "", $keyti);
$keyti = ereg_replace("z", "", $keyti);
$keyti = ereg_replace("R", "", $keyti);
$keyti = ereg_replace("r", "", $keyti);
$keyti = ereg_replace("H", "", $keyti);
$keyti = ereg_replace("h", "", $keyti);
if (strlen($keyti)==12){
	$keyti="000".$keyti;
}
$keyti2 = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) && empty($keyti2)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
//require_once("../parts/login_souko.php");
require_once("../../parts/login_souko_dev.php");
//SQL文のCOUNT関数を使用
$sql = "select distinct a.BRANCHNUM, a.ASSCODE, a.OPERATEDATE, a.MODIFYTIME, ar.CUSTOMERNAME, a.STATUS_KIND, s.MODIFYDATE, a.BOX_RECEPT_QTY ".
       "from D_ASS a, D_ASS_REQUEST ar, D_ASS_DISCARD ad, D_SLIP s ".
       "where a.ASSCODE   = ar.ASSCODE ".
       "and   a.ASSCODE   = ad.ASSCODE ".
       "and   a.BRANCHNUM = ad.BRANCHNUM ".
       "and   s.SLIP_KIND = '20' ".
       "and   a.ASSCODE || '-' || LPAD(a.BRANCHNUM,4,'0') = s.AUXCODE";
if (empty($keyti)) {}else{
	$sql = $sql . " and a.ASSCODE ='".$keyti."'";
}
if (empty($keyti2)) {}else{
	$sql = $sql . " and ar.CUSTOMERNAME like '%".$keyti2."%'";
}
$sql = $sql . " order by a.BRANCHNUM";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索結果". $keyti."</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>枝番</td>";
print "<td nowrap width=120>買取受付番号</td>";
print "<td nowrap width=120>査定日</td>";
print "<td nowrap width=120>査定時間</td>";
print "<td nowrap width=120>氏名</td>";
print "<td nowrap width=150>状態</td>";
print "<td nowrap width=100>承認日</td>";
print "<td nowrap width=120>受取箱数</td>";
print "</tr>";
while($row = $res->fetchRow()){
	print "<tr>";
	//枝番
	print "<td>".$row[0] ."</td>";
	//注文番号
	print "<td><a href='detailkaitori.php?receptioncode=" .$row[1] ."'>".$row[1] ."</td>";
	print "<td>".$row[2] ."</td>";
	print "<td>".$row[3] ."</td>";
	print "<td>".$row[4] ."</td>";
	print "<td>".Retkaitoristatuskind($row[5]) ."</td>";
	print "<td>".$row[6] ."</td>";
	print "<td>".$row[7] ."</td>";
	print "</tr>";
	$j++;
}
print "</table>";
print "<HR>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
