<?php

/* DBファイルは呼び出し元で読み込んでおく */

//ファイルの読み込み
//PEARの利用     -------(1)
//require_once("DB.php");
//require_once("../parts/selectvalue_souko.php");
//require_once("../../parts/selectvalue_souko.php");

function getGenreName($genrecode, &$conn){
    
    static $names = array();
    
    // 以前に取得している場合はキャッシュを返す
    if(isset($names[$genrecode])){
        return $names[$genrecode];
    }
    
	$sql = "select GENRENAME from M_GENRE where GENRECODE = '" . $genrecode ."'";
	
	$res = $conn->query($sql);
	if(DB::isError($res)){
		return "エラー";
	}
	else{
		$row = $res->fetchRow();
        $names[$genrecode] = $row[0]; // コードと名前を記憶
		return $row[0];
	}
}

function getRowCount($asscode ,&$conn){
	
	$sql = "select count(*) from d_ass_goods d, m_goods_display g, m_goods m ";
	$sql .= "where d.instorecode=g.instorecode and d.instorecode=m.instorecode ";
	$sql .= "and d.ASSCODE = '" .$asscode."'";
	
	$res = $conn->query($sql);
	if(DB::isError($res)){
		return 0;
	}
	else{
		$row = $res->fetchRow();
		return $row[0];
	}
}

function getPageCount($rowCount, $limit){
	$count = $rowCount / $limit;
	return ceil($count); // 切り上げ
}

function getSql( $assCode, $sort=0){
    
    // ソート値を設定
    if( $sort == 0 )
        $order = "DETAILNO";
    else
        $order = "ROUTING_KIND,DETAILNO";
    
    // sql生成
    $sql = <<< EOM
        select rownum as line,
        d.ASSCODE as ASSCODE,
        d.DETAILNO as DETAILNO,
        d.INSTORECODE as INSTORECODE,
        d.DETAILPRICE as DETAILPRICE,
        d.DETAILQTY as DETAILQTY,
        d.BASEBPRICE as BASEBPRICE,
        d.TAX_PRICE as TAX_PRICE,
        d.DOWNPRICE as DOWNPRICE,
        d.MODIFYDATE as MODIFYDATE,
        d.MODIFYTIME as MODIFYTIME,
        d.ROUTING_KIND as ROUTING_KIND,
        g.GOODSNAME as GOODSNAME,
        g.GOODSNAME_EXT1 as GOODSNAME_EXT1,
        m.GENRE0CODE as GENRE0CODE,
        g.JANCODE as JANCODE,
        m.GENRE1CODE as GENRE1CODE,
        m.GENRE2CODE as GENRE2CODE,
        m.GENRE3CODE as GENRE3CODE,
        g.AUTHORNAME as AUTHORNAME,
        m.BUMONCATCODE as BUMON,
        (select GENRENAME from M_GENRE where GENRECODE=m.GENRE1CODE ) as GENRE1NAME,
        (select GENRENAME from M_GENRE where GENRECODE=m.GENRE2CODE ) as GENRE2NAME,
        (select GENRENAME from M_GENRE where GENRECODE=m.GENRE3CODE ) as GENRE3NAME,
        (select ecprice from 
            (select * from d_goods_ecprice p order by p.fromdate desc) 
        where instorecode = d.INSTORECODE and ecprice_kind = '10' and fromdate <= d.MODIFYDATE and rownum <= 1) as PRICE,
        (select count(*) from D_STOCK
         where instorecode = d.INSTORECODE
         and locationcode not like '0%' and locationcode not like '9%' and STOCKRESERVE_KIND = '0') as STOCK
        from d_ass_goods d,
        m_goods_display g,
        m_goods m
        where d.instorecode=g.instorecode and d.instorecode=m.instorecode
        and d.ASSCODE = '{$assCode}'
        order by {$order}
EOM;

	return $sql;
}

?>