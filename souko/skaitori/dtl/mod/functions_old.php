<?php

/* DBファイルは呼び出し元で読み込んでおく */

//ファイルの読み込み
//PEARの利用     -------(1)
//require_once("DB.php");
//require_once("../parts/selectvalue_souko.php");
//require_once("../../parts/selectvalue_souko.php");

function getGenreName($genrecode, &$db){
    
    static $names = array();
    
    // 以前に取得している場合はキャッシュを返す
    if(isset($names[$genrecode])){
        return $names[$genrecode];
    }

	$sql = "select GENRENAME from M_GENRE where GENRECODE = '" . $genrecode ."'";
	
	$res = $db->query($sql);
	if(DB::isError($res)){
		return "エラー";
	}
	else{
		$row = $res->fetchRow();
        $names[$genrecode] = $row[0]; // コードと名前を記憶
		return $row[0];
	}
}

function getSellPrice($instorecode, $date, &$conn){
    $sql = <<< EOM
        select ecprice from d_goods_ecprice
        where abs(fromdate - '{$date}') =
        (select min(abs(fromdate - '{$date}'))
            from d_goods_ecprice
            where fromdate < '{$date}' and instorecode='{$instorecode}'
        ) and instorecode='{$instorecode}'
EOM;
    
    $res = $conn->query($sql);
    if(DB::isError($res)){
        return 0;
    }
    else{
        $row = $res->fetchRow();
        return $row[0];
    }
}



?>
