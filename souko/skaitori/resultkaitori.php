<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取検索結果</title>
</head>
<body bgcolor="#ccccff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
/* 2018/11/6 処理変更 後藤 start */
if(substr($keyti,0,1)=="Z" || substr($keyti,0,1)=="z" || substr($keyti,0,1)=="R" || substr($keyti,0,1)=="r" || substr($keyti,0,1)=="H" || substr($keyti,0,1)=="h") {
	$keyti="000".substr($keyti,1);
}
/* 2018/11/6 処理変更 後藤 end */
$keyti2 = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) && empty($keyti2)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select distinct b.branchnum, r.asscode, b.operatedate, c.modifytime, r.customername, b.status_kind, b.box_recept_qty ";
$sql = $sql . "from d_ass b, d_ass_request r, d_ass_discard c ";
$sql = $sql . "where r.asscode = b.asscode(+) ";
$sql = $sql . "and r.asscode = c.asscode(+) ";
$sql = $sql . "and b.branchnum = c.branchnum(+) ";
if (empty($keyti)) {}else{
	$sql = $sql . "and r.asscode ='".$keyti."' ";
}
if (empty($keyti2)) {}else{
	$sql = $sql . "and r.CUSTOMERNAME like '%".$keyti2."%' ";
}
$sql = $sql . "order by b.branchnum";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索結果". $keyti."</strong> <br><br>\n<HR>";
$j=1;
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>枝番</td>";
print "<td nowrap width=120>買取受付番号</td>";
print "<td nowrap width=120>査定日</td>";
print "<td nowrap width=120>査定時間</td>";
print "<td nowrap width=120>氏名</td>";
print "<td nowrap width=200>状態</td>";
print "<td nowrap width=120>受取箱数</td>";
print "</tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	//注文番号
	print "<td>".$row[0] ."</td>";
	print "<td><a href='detailkaitori.php?receptioncode=" .$row[1] ."'>".$row[1] ."</td>";
	print "<td>".$row[2] ."</td>";
	print "<td>".$row[3] ."</td>";
	print "<td>".$row[4] ."</td>";
	print "<td>".Retkaitoristatuskind($row[5]) ."</td>";
	print "<td>".$row[6] ."</td>";
	print "</tr>";
	$j++;
}
print "</tr></table>";
print "<HR>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
