<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取検索結果</title>
</head>
<body bgcolor="#ccccff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti1  = addslashes(@$_GET["receptioncode"]);
$keyti2  = addslashes(@$_GET["branchnum"]);
$keysort = addslashes(@$_GET["keysort"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti1)) {
	print "データが入力されていません．<br>";
}
//ソート順の設定
if ($keysort=="1") {
	$_SESSION["kaitori_sort"]=1;
}else
{
	$_SESSION["kaitori_sort"]=0;
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select d.ASSCODE,d.DETAILNO,d.INSTORECODE,d.DETAILPRICE,d.DETAILQTY,d.BASEBPRICE,d.TAX_PRICE,d.DOWNPRICE,";
$sql = $sql . "d.MODIFYDATE,d.MODIFYTIME,d.ROUTING_KIND,g.goodsname,g.GOODSNAME_EXT1,m.genre0code ";
$sql = $sql . "from d_ass_goods d,m_goods_display g,m_goods m ";
$sql = $sql . "where d.instorecode=g.instorecode ";
$sql = $sql . "and d.instorecode=m.instorecode ";
$sql = $sql . "and d.asscode ='".$keyti1;
if (!empty($keyti2)) {
	$sql = $sql . "' and d.branchnum ='".$keyti2;
}
if ($_SESSION["kaitori_sort"]==0)
{
	$sql = $sql ."' order by d.detailno";
}else
{
	$sql = $sql ."' order by d.ROUTING_KIND,d.detailno";
}
$meisai=0;
$suuryou=0;
$kijyun=0;
$goukei=0;
$genten=0;
$boliki=0;
$masutaari=0;
$masutanasi=0;
$logiiki=0;
$dcount=0;
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//ヘッダ部分の表示
if (empty($keyti2)) {
	print "<strong><BR>買取受付番号：". $keyti1."</strong><BR><BR>\n<div align='right';font-size:'6';>";
} else {
	print "<strong><BR>買取受付番号：". $keyti1."</strong><BR>";
	print "<strong><BR>枝番：". sprintf('%03d', $keyti2)."</strong><BR><BR>\n<div align='right';font-size:'6';>";
}
if ($_SESSION["kaitori_sort"]==0)
{
	print "<a href='detailkaitori.php?receptioncode=" .$keyti1 . "&keysort=1&branchnum=" .$keyti2 ."'>行き先順に並替</a>";
}else
{
	print "<a href='detailkaitori.php?receptioncode=" .$keyti1 . "&keysort=0&branchnum=" .$keyti2 ."'>明細番号順に並替</a>";
}
print "</div>";
//検索結果の表示
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=70>明細番号</td><td nowrap width=120>インストアコード</td><td nowrap width=80>行先</td><td nowrap width=80>カテゴリ</td><td nowrap width=300>商品名</td>";
print "<td nowrap width=40>数量</td><td nowrap width=100>基準買取金額</td>";
print "<td nowrap width=80>減点金額</td><td nowrap width=80>買取金額</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//明細番号
	print "<td>".$row[1] ."</td>";
//instorecode
		print "<td>".$row[2]."</td>";
//行き先
		print "<td>".Retkaitoriikisaki($row[10])."</td>";
//カテゴリ
		print "<td>".Retgoodscategory($row[13])."</td>";
//商品名
	print "<td>".$row[11] . "　" .$row[12] ."</td>";
//数量
	print "<td align=right>".$row[4] ."</td>";
//基準買取価格
	print "<td align=right>".$row[5] ."</td>";
//減点金額
	print "<td align=right>".$row[7] ."</td>";
//買取金額金額
	print "<td align=right>".$row[6] ."</td>";
	print "</tr>";
$meisai=$meisai+$row[3];
$suuryou=$suuryou+$row[4];
$kijyun=$kijyun+$row[5];
$goukei=$goukei+$row[6];
$genten=$genten+$row[7];
	$j++;
//行き先のカウント
	switch ($row[10]) {
		case 0:
			$dcount=$dcount+$row[4];
			break;
		case 1:
			$boliki=$boliki+$row[4];
			break;
		case 2:
			$logiiki=$logiiki+$row[4];
			break;
		case 3:
			$masutaari=$masutaari+$row[4];
			break;
		case 4:
			$masutanasi=$masutanasi+$row[4];
			break;
		default:
			$retvalue= "エラー";
			break;
	}
}
print "<TR><B><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD>　</TD><TD align=right>" . $suuryou . "</TD><TD align=right>" . $kijyun . "</TD><TD align=right>" . $genten . "</TD><TD align=right>" . $goukei . "</TD></B></TR>";
print "</table>";
//データの開放
$res->free();
//行き先集計の表示
print "<strong><BR>行き先毎の商品数</strong>";
print "<TABLE border=1><TR><B><TD>行き先</TD><TD>数量</TD></B>";
print "<TR><TD>BOL行き</TD><TD>".$boliki."</TD></TR>";
print "<TR><TD>ロジ行き</TD><TD>".$logiiki."</TD></TR>";
print "<TR><TD>マスタ有り</TD><TD>".$masutaari."</TD></TR>";
print "<TR><TD>マスタ無し</TD><TD>".$masutanasi."</TD></TR></table>";
//print "<TR><TD>D商品数</TD><TD>".$dcount."</TD></TR></table>";
//
$sql = "select *";
$sql = $sql . " from d_ass_discard";
$sql = $sql . " where asscode ='".$keyti1."'";
/* 20161206 編集 Start */
if($keyti2) {
$sql = $sql . " and branchnum ='".$keyti2."'";
}
$sql = $sql . " order by booksoft_kind";
/* 20161206 End */
//print $sql;
$res2 = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>値段のつかない商品</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=70>種類</td><td nowrap width=70>数量</td><td nowrap width=300>廃棄理由区分</td>";
print "<td nowrap width=300>備考</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row2 = $res2->fetchRow()){
	print "<tr>";
//種類
	print "<td>".Retbooksoftkind($row2[1]) ."</td>";
//数量
		print "<td>".$row2[2]."</td>";
//廃棄理由
	print "<td>";
	$num=decbin($row2[3]);
	$i=0;
	$len=strlen($num);
//print "num".decbin($row2[3])."<BR>";
	while($i<$len){
		if (substr($num,$i,1)==1){
//print $len .":".$i."<BR>";
//print ($len-$i) . ":".$row2[1].":".Retbooksoftd($len-$i,$row2[1]). "<BR>";
			print Retbooksoftd($len-$i,$row2[1]). "<BR>";
//			print $len-$i .":".$row2[1];
//print decbin($row2[3]);
		}
		$i++;
	}
	print "　</td>";
//備考
	print "<td>　".$row2[4] ."</td>";
	print "</tr>";
	$j++;
}
print "</table>";
//データの開放
$res2->free();
$db->disconnect();
?>
<BR>
<FORM action="/souko/skaitori_staff/searchkaitori_staff.php"><INPUT TYPE="submit" VALUE="買取検索画面に戻る"></FORM>
</body>
</html>
