<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取情報検索(一時保存)</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$sql = "select a.ASSCODE, a.BRANCHNUM, a.OPERATEDATE, s.STAFFNAME ".
       "from D_ASS a, M_STAFF s ".
       "where a.STATUS_KIND = '-1' ".
       "and a.OPERATESTAFFCODE = s.STAFFCODE ".
       "order by a.OPERATEDATE";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>買取情報検索(一時保存)</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=180 align=center>買取受付番号</td>
	<td width= 50 align=center>枝番</td>
	<td width=100 align=center>査定日</td>
	<td width=100 align=center>担当者</td>
</tr>
<tr>
<?php

//取得データのセット
while($row = $res->fetchRow()){

	print "<td align=center>". $row[0] ."</td>";
	print "<td align=center>". $row[1]. "</td>";
	print "<td align=center>". $row[2] ."</td>";
	print "<td align=center>". $row[3] ."</td></tr>";
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
