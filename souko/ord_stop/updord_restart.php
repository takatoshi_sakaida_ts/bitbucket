<?PHP
require '../parts/pagechk.inc';
require_once('../parts/db_auto_selector.php');
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文出荷停止再開対象検索結果</title>
<link rel="stylesheet" href="/css/bolweb-env.css"/>
</head>
<body bgcolor="#ccccff" class="env-<?php echo(detect_env()); ?>">
<?php
print "<strong><BR>注文出荷停止注文一覧</strong> <br><br>\n<HR>";
//ファイルの読み込み
require_once("../parts/selectvalue_souko.php");
$db = connect_to_souko_database();
$sql = "select receptioncode,customername1,shippingaddress1||shippingaddress2||shippingaddress3||shippingaddress4||shippingaddress5,reception_amount,expence_kind,status_kind from d_reception";
$sql = $sql . " where SHIPSTOP_KIND='1'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$j=1;
	while($row = $res->fetchRow()){
		if (empty($row[0]))
		{
		print "出荷停止中の注文はありません";
		}
		else{
			if ($j==1)
			{
				print "<table border=1>\n";
				print "<tr bgcolor=#ccffff>\n";
				//項目名の表示
				print "<td nowrap width=120>注文番号</td><td width=100>氏名</td><td width=150>住所</td><td nowrap width=50>金額</td><td nowrap width=80>支払方法</td><td>　</td>";
				print "</tr>";
			}
		}
				print "<tr>";
				print "<td>2".substr($row[0],1,15) ."</td>";
				print "<td  width=100>".$row[1] ."</td>";
				print "<td width=400>".$row[2] ."</td>";
				print "<td>".$row[3] ."</td>";
				print "<td>".Retkessai($row[4]) ."</td>";
				print "</td>";
				print "<td width=150 align=center><font size=+1>";
				print "<a href='updord_stop_confirm.php?receptioncode=" .$row[0] . "&ptn=2'>出荷再開</td>";
				print "</font></tr>";
				$j=2;
		}
	print "</table>";
	print "<HR>";
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
