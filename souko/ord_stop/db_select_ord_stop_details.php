<?php

class OrdStopDetail {
	public $ord_no;
	public $name;
	public $address;
	public $amt;
	public $pay_tp;
	public $status;
	public $stop_yn;
	public $store_nm;
	public $store_cd;

	function is_logi() {
		return "99997" == $this->store_cd;
	}
}

function select_ord_stop_details($db, $ord_no) {
	$sql = <<<SQL
select
	a.ord_no as val0
,	a.ORDR_NM_FST||' '||a.ORDR_NM_MID as val1
,	c.ZIP_ADDR1||c.ZIP_ADDR2||c.ZIP_ADDR3||c.DTL_ADDR as val2
,	(a.TOT_INV_AMT - a.ORD_CNCL_AMT) as val3
,	a.PAY_TP as val4
,	d.STATUS as val5
,	d.SHIP_STOP_YN as val6
,	e.STORE_NM as val7
,	d.STORE_CD as val8
from
TORDER a
,	(select distinct DELV_NO from TORDERDTL where ORD_NO='${ord_no}') b
,	TORDERDELV c
,	TORDERDELV_SPF d
,	TSTORE_TKH e
where
a.ORD_NO = d.ORD_NO
and c.DELV_NO in (select distinct DELV_NO from TORDERDTL where ORD_NO='${ord_no}')
and d.STORE_CD = e.STORE_CD(+)
and a.ORD_NO='${ord_no}'
and d.STATUS in ('0','1','2')
order by
d.STATUS
,	d.STORE_CD
SQL;
	$res = $db->query($sql);
	if (DB::isError($res)) {
		$res->DB_Error($res->getcode(), PEAR_ERROR_DOE, NULL, NULL);
	}
	$result = array();
	if ($res->numRows() > 0) {
		while ($row = $res->fetchRow()) {
			$obj = new OrdStopDetail();
			$obj->ord_no = $row[0];
			$obj->name = $row[1];
			$obj->address = $row[2];
			$obj->amt = $row[3];
			$obj->pay_tp = $row[4];
			$obj->status = $row[5];
			$obj->stop_yn = $row[6];
			$obj->store_nm = $row[7];
			$obj->store_cd = $row[8];
			array_push($result, $obj);
		}
	}
	$res->free();
	return $result;
}
