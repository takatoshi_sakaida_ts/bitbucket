<?PHP
require '../parts/pagechk.inc';
require_once('../parts/db_auto_selector.php');
require_once('./db_select_ord_stop_details.php');
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文出荷停止対象検索結果</title>
<link rel="stylesheet" href="/css/bolweb-env.css"/>
<link rel="stylesheet" href="/css/bolweb-ord-stop.css"/>
</head>
<body class="ord-stop env-<?php echo(detect_env()); ?>">
<style>
td.is-store {
	background-color: #dddddd;
}
</style>
<?php
//ファイルの読み込み
require_once("../parts/selectvalue_souko.php");

// 関数定義
// 倉庫用ステータスラベル変換
function get_status_string($detail, $souko_stat) {
	if (!$detail->is_logi()) {
		return "-";
	}
	return Retstatuskind($souko_stat);
}

// ECDB用決済方法ラベル変換
// /spf/parts/selectvalue_ec.php からコピー。
function get_ec_pay_tp_string($index) {
	switch ($index) {
	case 2:
		return "クレジット";
		break;
	case 4:
		return "代引";
		break;
	case 6:
		return "Yahooマネー";
		break;
	case 7:
		return "全額ポイント支払い";
		break;
	case 8:
		return "Amazonにお問合せ下さい";
		break;
	default:
		return "エラー";
	}
}

// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
}
// 注文情報取得
$sql = <<<SQL
select
	receptioncode
,	customername1
,	shippingaddress1||shippingaddress2||shippingaddress3||shippingaddress4||shippingaddress5
,	reception_amount
,	expence_kind
,	status_kind
,	shipstop_kind
from
	d_reception
where
	receptioncode='${keyti}'
SQL;
//print $sql;
$db = connect_to_souko_database();
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索条件：". $keyti."</strong> <br><br>\n<HR>";
$row = $res->fetchRow();
if (empty($row[0]))
{
print "対象の注文は倉庫システムにありません";
}
else
{
	// 停止可能なステータス
	$stoppable_status = '01';
	// 倉庫側ステータス
	$souko_stat = $row[5];
	// 現在の出荷停止状態
	$stop_stat = $row[6];

	print "<table border=1>\n";
	print "<tr>\n";
	//項目名の表示
	print "<td nowrap width=120>注文番号</td>";
	print "<td nowrap width=100>氏名</td>";
	print "<td width=200>住所</td>";
	print "<td nowrap width=50>金額</td>";
	print "<td nowrap width=80>支払方法</td>";
	print "<td>&nbsp;</td>";
	print "</tr>";
	print "<tr>";
	//注文番号
		print "<td width=100>".$row[0] ."</td>";
		print "<td>".$row[1] ."</td>";
		print "<td width=400>".$row[2] ."</td>";
		print "<td>".$row[3] ."</td>";
		print "<td>".Retkessai($row[4]) ."</td>";
		print "</td>";
		print "<td width=150 class=\"stop-link\">";
		if ($stop_stat == '1') {
			print "停止済み";

		} else if ($souko_stat == $stoppable_status) {
			print "<a href='updord_stop_confirm.php?receptioncode=" .$row[0] . "&ptn=1'>出荷停止</a>";
		}
		print "</td></tr></table>";
		print "<font color='red'><strong>";
		print "※ステータスが＜". Retstatuskind($stoppable_status). "＞の場合のみ出荷停止します。<br>";
		print "※出荷店舗が「BOOKOFF宅本便(99997)」以外の明細は停止対象外です。";
		print "</strong></font>";
		print "<HR>";

	// 引当状況テーブル
	$db = connect_to_ec_database();
	$details = select_ord_stop_details($db, $keyti);
	$db->disconnect();
	$count = count($details);
	if ($count > 0) {
		$j = 1;
		foreach($details as $row) {
			if ($j==1) {
				print "<strong>引当状況</strong>";
				print "<table border=1>\n";
				print "<tr bgcolor=#ccffff>\n";
				//項目名の表示
				print "<td nowrap width=120>注文番号</td>";
				print "<td nowrap width=100>氏名</td>";
				print "<td width=200>住所</td>";
				print "<td nowrap width=50>金額</td>";
				print "<td nowrap width=80>支払方法</td>";
				print "<td>出荷店舗</td>";
				print "<td>ステータス(倉庫)</td>";
				print "</tr>";
				print "<tr bgcolor=#ffffff>";
				print "<td rowspan=".$count." width=100>".$row->ord_no ."</td>";
				print "<td rowspan=".$count.">".$row->name ."</td>";
				print "<td rowspan=".$count." width=400>".$row->address ."</td>";
				print "<td rowspan=".$count.">".$row->amt ."</td>";
				print "<td rowspan=".$count.">".get_ec_pay_tp_string($row->pay_tp) ."</td>";
				print "<td class=\"". ($row->is_logi() ? '' : 'is-store'). "\">".$row->store_nm. "(". $row->store_cd. ")</td>";
				print "<td class=\"". ($row->is_logi() ? '' : 'is-store'). "\">".get_status_string($row, $souko_stat) ."</td>";
				print "</tr>";
			} else {
				print "<tr bgcolor=#ffffff>";
				print "<td class=\"". ($row->is_logi() ? '' : 'is-store'). "\">". $row->store_nm. "(". $row->store_cd. ")</td>";
				print "<td class=\"". ($row->is_logi() ? '' : 'is-store'). "\">".get_status_string($row, $souko_stat) ."</td>";
				print "</tr>";
			}
			$j=2;
		}
		print "</table>";
		print "<a href=\"./updord_restart.php\">出荷停止解除画面はこちら</a>";
		print "<HR>";
	}
}
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
