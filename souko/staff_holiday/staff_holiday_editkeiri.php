<?PHP
require '../parts/pagechk.inc';
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_nextmon = $date_today + (86400*30);
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_GET["reqno"]);
$st = addslashes(@$_GET["st"])+0;
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>有給申請修正・承認画面</title>
<script type="text/javascript">
function EntCheck()
{
<!--承認
if(window.confirm("承認してよろしいですか？")){
	document.frmorderconf.action = "staff_holiday_shoent.php";
	document.frmorderconf.submit();
}else{
//登録キャンセル
}

}
function EntEdit()
{
<!--更新
if(window.confirm("更新してよろしですか？メモしか更新されません。")){
	document.frmorderconf.action = "staff_holiday_keiriedit.php";
	document.frmorderconf.submit();
}else{
//登録キャンセル
}

}
function EntSasi()
{
<!--差し戻し
if(window.confirm("差し戻しをしてよろしですか？")){
	document.frmorderconf.action = "staff_holiday_shomds.php";
	document.frmorderconf.submit();
}else{
//登録キャンセル
}

}
</script>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" name="frmorderconf">
<?PHP
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select R.REQUESTCODE,R.REQUESTDATE,M.STAFFNAME,R.SHOZOKU_KIND,R.STATUS_KIND,R.MEMO2,R.MEMO1,R.STAFFCODE,D.STARTDATE,D.DATE_CNT,A.MAILADDRESS from D_STAFF_REQUEST R,M_STAFF M,D_REQUEST_HOLIDAY D,M_STAFF_MAIL A where R.STAFFCODE=M.STAFFCODE and R.REQUESTCODE=D.REQUESTCODE and R.STAFFCODE=A.STAFFCODE(+) and R.REQUESTCODE='".$keyti."' order by  D.DETAILNO";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$i=0;
while($row = $res->fetchRow()){
$_SESSION["HSMAP"]["REQUESTCODE"]=$row[0];
$_SESSION["HSMAP"]["setstaffcode"]=$row[7];
$_SESSION["HSMAP"]["staffmail"]=$row[10];
$REQUESTDATE=$row[1];
$STAFFNAME=$row[2];
$SHOZOKU_KIND=$row[3];
$STATUS_KIND=$row[4];
$_SESSION["HSMAP"]["status"]=$row[4]+0;
$MEMO2=$row[5];
$MEMO1=$row[6];
$STAFFCODE=$row[7];
$STARTDATE[$i]=$row[8];
$DATE_CNT[$i]=$row[9];
$i++;
}
?>
<TABLE ID="menu1" width="900">
<tr><th colspan='4' align=center>有給申請承認更新入力画面</td>
</TR>
  <tr>
    <th>名前</th>
    <td>
<?PHP
print $STAFFNAME;
?>
    </td>
</TR>
  <tr>
    <th>所属チーム</th>
    <td>
<?PHP
print Retteam($SHOZOKU_KIND);
?>
    </td>
  </tr>

<?PHP
for ($c=0; $c<$i; $c++) {
  print "<tr><th>休暇申請日".($c+1)."</th><td>";
	print $STARTDATE[$c]."から".$DATE_CNT[$c]."日間";
	print "</td></tr>";
}
?>
  <tr>
    <td>メモ（経理用）</td>
    <td colspan=3>
	<textarea name="MEMO" cols="100" rows="5" style="ime-mode:active;">
<?PHP
if(isset($MEMO1)){
	print $MEMO1;
}
print '</textarea>';
?>
	</td>
  </tr>
  <tr>
    <td>メモスタッフより</td>
    <td colspan=3>
<?PHP
if(isset($MEMO2)){
	print $MEMO2;
}
?>
	</td>
  </tr>
</TABLE>
<BR>
<input type="BUTTON" value="承　認　す　る" onClick="EntCheck()">
<input type="BUTTON" value="修　正　す　る　" onClick="EntEdit()">
<input type="BUTTON" value="差　戻　す　る　" onClick="EntSasi()">
<INPUT TYPE="BUTTON" VALUE="元の画面に戻る" onClick="history.back()">
</FORM>
</body>
</html>