<?PHP
require '../parts/pagechk.inc';
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_nextmon = $date_today + (86400*30);

?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="staff_holiday_input_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>有給申請入力画面</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" action="staff_holiday_confirm.php" name="volfm">
<b>
<font color=red>＊注意事項</font><BR>
●有給休暇申請は<U>希望日の１ヶ月前までに提出</U>してください。<BR>
●月をまたぐ取得は別途申請入力してください。<U>（末日締めです）</U><BR>
●有給休暇の残日数は給与明細でご確認ください。<BR><BR>
<font color=red>ロジ部スタッフについては12/30（金）〜1/9（月）の期間の有給申請をしないようにお願いします。</font><BR>
<BR></b>
<TABLE ID="menu1" width="900">
<tr><th colspan='4' align=center>有給申請入力画面</td>
</TR>
<tr>
	<td>給与ID</td>
	<td>
<?PHP
if(isset($_SESSION["user_id"])){
	if (substr($_SESSION["user_id"],0,1)=="9")
	{
		print "0000".$_SESSION["user_id"];
	}	else
	{
		print "931000".substr($_SESSION["user_id"],2,4);
	}
//	print "<BR>".$_SESSION["user_id"];
}else
{
	print "ログインエラー";
}
?>
</td>
</TR>
<tr>
	<td>氏名<font color=red></font></td>
	<td>
	<input type="text" name="REQUEST_NM" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["REQUEST_NM"])){
	print $_SESSION["HSMAP"]["REQUEST_NM"];
}
else
{
	print $_SESSION["user_name"];
}
print '" >';
?>
	<TD>所属チーム名<font color=red></font></td>
	<td>
	<select name="SHOZOKUGR">
		<option value="0" >選択してください
		<option value="1" >出荷管理チーム
		<option value="2" >入庫管理チーム
		<option value="3" >宅本便チーム
		<option value="4" >人材教育チーム
		<option value="5" >EC事業チーム
		<option value="6" >宅本便事業チーム
		<option value="7" >商品グループ
		<option value="8" >商品データベースグループ
		<option value="9" >ECカスタマーチーム
		<option value="10" >宅本便カスタマーチーム
		<option value="11" >総務経理グループ
		<option value="12" >経営企画グループ
	</select>
	</td>
</TR>
<tr>
	<td rowspan=5>休暇申請日<font color=red></font></td>
	<td>
	<input type="text" name="HOLIDAY_Y1" size="8" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_Y1"])){
	print $_SESSION["HSMAP"]["HOLIDAY_Y1"];
}else{
//print date('Y', $date_nextmon);
}
print '" >年';
?>
	<input type="text" name="HOLIDAY_M1" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_M1"])){
	print $_SESSION["HSMAP"]["HOLIDAY_M1"];
}
print '" >月';
?>
	<input type="text" name="HOLIDAY_D1" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_D1"])){
	print $_SESSION["HSMAP"]["HOLIDAY_D1"];
}
print '" >日　から';
?>
	<input type="text" name="HOLIDAY_E1" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_E1"])){
	print $_SESSION["HSMAP"]["HOLIDAY_E1"];
}
print '" >日　まで';
?>
</td>
  </tr>
<tr>
	<td>
	<input type="text" name="HOLIDAY_Y2" size="8" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_Y2"])){
	print $_SESSION["HSMAP"]["HOLIDAY_Y2"];
}else{
//print date('Y', $date_nextmon);
}
print '" >年';
?>
	<input type="text" name="HOLIDAY_M2" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_M2"])){
	print $_SESSION["HSMAP"]["HOLIDAY_M2"];
}
print '" >月';
?>
	<input type="text" name="HOLIDAY_D2" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_D2"])){
	print $_SESSION["HSMAP"]["HOLIDAY_D2"];
}
print '" >日　から';
?>
	<input type="text" name="HOLIDAY_E2" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_E2"])){
	print $_SESSION["HSMAP"]["HOLIDAY_E2"];
}
print '" >日　まで';
?>
</td>
  </tr>
<tr>
	<td>
	<input type="text" name="HOLIDAY_Y3" size="8" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_Y3"])){
	print $_SESSION["HSMAP"]["HOLIDAY_Y3"];
}else{
//print date('Y', $date_nextmon);
}
print '" >年';
?>
	<input type="text" name="HOLIDAY_M3" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_M3"])){
	print $_SESSION["HSMAP"]["HOLIDAY_M3"];
}
print '" >月';
?>
	<input type="text" name="HOLIDAY_D3" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_D3"])){
	print $_SESSION["HSMAP"]["HOLIDAY_D3"];
}
print '" >日　から';
?>
	<input type="text" name="HOLIDAY_E3" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_E3"])){
	print $_SESSION["HSMAP"]["HOLIDAY_E3"];
}
print '" >日　まで';
?>
</td>
  </tr>
<tr>
	<td>
	<input type="text" name="HOLIDAY_Y4" size="8" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_Y4"])){
	print $_SESSION["HSMAP"]["HOLIDAY_Y4"];
}else{
//print date('Y', $date_nextmon);
}
print '" >年';
?>
	<input type="text" name="HOLIDAY_M4" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_M4"])){
	print $_SESSION["HSMAP"]["HOLIDAY_M4"];
}
print '" >月';
?>
	<input type="text" name="HOLIDAY_D4" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_D4"])){
	print $_SESSION["HSMAP"]["HOLIDAY_D4"];
}
print '" >日　から';
?>
	<input type="text" name="HOLIDAY_E4" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_E4"])){
	print $_SESSION["HSMAP"]["HOLIDAY_E4"];
}
print '" >日　まで';
?>
</td>
  </tr>
<tr>
	<td>
	<input type="text" name="HOLIDAY_Y5" size="8" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_Y5"])){
	print $_SESSION["HSMAP"]["HOLIDAY_Y5"];
}else{
//print date('Y', $date_nextmon);
}
print '" >年';
?>
	<input type="text" name="HOLIDAY_M5" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_M5"])){
	print $_SESSION["HSMAP"]["HOLIDAY_M5"];
}
print '" >月';
?>
	<input type="text" name="HOLIDAY_D5" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_D5"])){
	print $_SESSION["HSMAP"]["HOLIDAY_D5"];
}
print '" >日　から';
?>
	<input type="text" name="HOLIDAY_E5" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOLIDAY_E5"])){
	print $_SESSION["HSMAP"]["HOLIDAY_E5"];
}
print '" >日　まで';
?>
</td>
  </tr>

  <tr>
    <td>メモ</td>
    <td colspan=3>
	<textarea name="MEMO" cols="100" rows="5" style="ime-mode:active;">
<?PHP
if(isset($_SESSION["HSMAP"]["MEMO"])){
	print $_SESSION["HSMAP"]["MEMO"];
}
print '</textarea>';
?>
	</td>
  </tr>


<input type="hidden" name="SHOZOKUGR_index" value="
<?PHP

if(isset($_SESSION["HSMAP"]["SHOZOKUGR_index"])){
	print $_SESSION["HSMAP"]["SHOZOKUGR_index"];
}

print '" >';
?>
<input type="hidden" name="input_number" value="
<?PHP

if(isset($_SESSION["HSMAP"]["input_number"])){
	print $_SESSION["HSMAP"]["input_number"];
}

print '" >';
?>
</TABLE>
<BR>
<b>
＊確認事項<BR>
１．有給休暇希望日は就業部署の状況によって変更してもらわなくてはならないこともあります。<BR>
２．有給休暇は、雇用契約日数の範囲内で取得してください<BR>
　＊【有給取得日】＋【実際に出勤した日】＝【雇用契約日数の範囲内】<BR>
３．有給休暇取得日に出勤した場合、【出勤】となります<BR>
　＊事前に変更のご連絡をお願いします。<BR><BR>
</b>
<script type="text/javascript">
document.volfm.SHOZOKUGR.selectedIndex=document.volfm.SHOZOKUGR_index.value;
</script>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="EntCan()">
<input type="BUTTON" value="　　登録　　" onClick="EntCheck()">
</FORM>
</body>
</html>