<?PHP
require '../parts/pagechk.inc';
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_nextmon = $date_today + (86400*30);
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = $_SESSION["HSMAP"]["REQUESTCODE"];
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="staff_holiday_input_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>有給申請修正画面</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<script type="text/javascript">
function EntCheck()
{
<!--修正
	document.frmorderconf.action = "staff_holiday_staffedit.php";
	document.frmorderconf.submit();
}
</script>
<body>
<form method="POST" action="staff_holiday_staffedit.php" name="frmorderconf">
<b>
<font color=red>＊注意事項</font><BR>
●有給休暇申請は<U>希望日の１ヶ月前までに提出</U>してください。<BR>
●月をまたぐ取得は別途申請入力してください。<U>（末日締めです）</U><BR>
●有給休暇の残日数は給与明細でご確認ください。<BR><BR>
</b>
<?PHP
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select R.REQUESTCODE,R.REQUESTDATE,M.STAFFNAME,R.SHOZOKU_KIND,R.STATUS_KIND,R.MEMO2,R.MEMO1,R.STAFFCODE,D.STARTDATE,D.DATE_CNT from D_STAFF_REQUEST R,M_STAFF M,D_REQUEST_HOLIDAY D where R.STAFFCODE=M.STAFFCODE and R.REQUESTCODE=D.REQUESTCODE and R.REQUESTCODE='".$keyti."' order by  D.DETAILNO";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$i=0;
while($row = $res->fetchRow()){
$_SESSION["HSMAP"]["REQUESTCODE"]=$row[0];
$REQUESTDATE=$row[1];
$STAFFNAME=$row[2];
$SHOZOKU_KIND=$row[3];
$STATUS_KIND=$row[4];
$MEMO2=$row[5];
$MEMO1=$row[6];
$STAFFCODE=$row[7];
$STARTDATE[$i]=$row[8];
$DATE_CNT[$i]=$row[9];
$i++;
}

?>
<TABLE ID="menu1" width="900">
<tr><th colspan='4' align=center>有給申請入力画面</td>
</TR>
<tr>
	<th>給与ID</th>
	<td>
<?PHP
if(isset($_SESSION["user_id"])){
	if (substr($_SESSION["user_id"],0,1)=="9")
	{
		print "0000".$_SESSION["user_id"];
	}	else
	{
		print "931000".substr($_SESSION["user_id"],2,4);
	}
//	print "<BR>".$_SESSION["user_id"];
}else
{
	print "ログインエラー";
}
?>
</td>
</TR>
  <tr>
    <th>名前</th>
    <td>
<?PHP
print $STAFFNAME;
?>
    </td>
</TR>
  <tr>
    <th>所属チーム</th>
    <td>
<?PHP
print Retteam($SHOZOKU_KIND);
?>
    </td>
  </tr>
<?PHP
/*
for ($c=0; $c<$i; $c++) {
  print "<tr><th>休暇申請日".($c+1)."</th><td>";
	print '<input type="text" name="HOLIDAY_Y1" size="4" maxlength="4" style="ime-mode:inactive;" value="';
if(isset($STARTDATE[$c])){
	print substr($STARTDATE[$c],0,4);
}else{
//print date('Y', $date_nextmon);
}
print '" >年';
?>
	<input type="text" name="HOLIDAY_M1" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($STARTDATE[$c])){
	print substr($STARTDATE[$c],4,2);
}
print '" >月';
?>
	<input type="text" name="HOLIDAY_D1" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($STARTDATE[$c])){
	print substr($STARTDATE[$c],6,2);
}
print '" >日　から';
?>
	<input type="text" name="HOLIDAY_E1" size="4" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($DATE_CNT[$c])){
	print $DATE_CNT[$c];
}
print '" >日間';
	print "</td></tr>";
}
*/
?>

<?PHP

for ($c=0; $c<$i; $c++) {
  print "<tr><th>休暇申請日".($c+1)."</th><td>";
	print Rethiduke($STARTDATE[$c])."から".$DATE_CNT[$c]."日間";
	print "</td></tr>";
}

?>
  <tr>
    <td>メモ</td>
    <td colspan=3>
	<textarea name="MEMO" cols="100" rows="5" style="ime-mode:active;">
<?PHP
if(isset($MEMO2)){
	print $MEMO2;
}
print '</textarea>';
?>
	</td>
  </tr>
</TABLE>
<BR>
<b>
＊確認事項<BR>
１．有給休暇希望日は就業部署の状況によって変更してもらわなくてはならないこともあります。<BR>
２．有給休暇は、雇用契約日数の範囲内で取得してください<BR>
　＊【有給取得日】＋【実際に出勤した日】＝【雇用契約日数の範囲内】<BR>
３．有給休暇取得日に出勤した場合、【出勤】となります<BR>
　＊事前に変更のご連絡をお願いします。<BR><BR>
</b>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="history.back()">
<input type="BUTTON" value="　　登録　　" onClick="EntCheck()">
</FORM>
</body>
</html>