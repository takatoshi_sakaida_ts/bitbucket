<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko_satei_mongon_henkou.php");
// require_once("../../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko_dev.php");
// require_once("../../parts/login_souko.php");

//関数定義ファイル読み込み
require_once("./functions.php");
require_once("../common/bol_storeList.php");

// GETリクエストパラメータから買取番号を取得
if(isset($_GET['sellNo']) && $_GET['sellNo'] != ""){
    $sellno = $_GET['sellNo'];
} else{
    die("買取受付番号が未指定のため、ダウンロードできません。");
}

// $sellno = "Z706900000078";
$asscode = convertSellNoToAssCode($sellno);
$customerName = getCustomerName($db, $asscode);

$OUTPUTFILENAME = $sellno . ".csv";

// $lines = array();

// 買取店舗リストをヒープ
$storeNo = getStoreList();

// 集計値用
$SUM = array();
$SUM['GOODS']['DETAILPRICE']=0;
$SUM['GOODS']['DETAILQTY']=0;
$SUM['GOODS']['BASEBPRICE']=0;
$SUM['GOODS']['TAX_PRICE']=0;
$SUM['GOODS']['DOWNPRICE']=0;
$SUM['ROUTE']['BOL']=0;
$SUM['ROUTE']['MASTER']=0;
$SUM['ROUTE']['NO_MASTER']=0;
$SUM['ROUTE']['LOGI']=0;
$SUM['ROUTE']['D']=0;

// 直接画面に返す
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename={$OUTPUTFILENAME}");

$fp = fopen('php://output', 'w');


// csvヘッダー情報
$CSV_HEADER = "明細番号,買取受付番号,店舗番号,店舗名,インストアコード,JANコード,行先,カテゴリ,部門コード,ジャンル1,ジャンル2,ジャンル3,商品名,著者/アーティスト,数量,基準買取金額,減点金額,買取金額,販売金額,在庫点数";
fputcsv($fp,explode(",", $CSV_HEADER));

// クエリ実行
$AssessDtlsql = getAssessDtlSql($asscode);
$res = $db->query($AssessDtlsql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){

	fputcsv($fp, array(
		$row['DETAILNO'],
		$sellno,
		$storeNo[$customerName],
		$customerName,
		$row['INSTORECODE'],
		$row['JANCODE'],
		Retkaitoriikisaki($row['ROUTING_KIND']),
		Retgoodscategory($row['GENRE0CODE']),
		$row['BUMON'],
		$row['GENRE1NAME'],
		$row['GENRE2NAME'],
		$row['GENRE3NAME'],
		$row['GOODSNAME'] . "　" .$row['GOODSNAME_EXT1'],
		$row['AUTHORNAME'],
		$row['DETAILQTY'],
		$row['BASEBPRICE'],
		$row['DOWNPRICE'],
		$row['TAX_PRICE'],
		$row['PRICE'],
		$row['STOCK']
	));

	// 各フィールドの合計をカウント
	$SUM['GOODS']['DETAILPRICE'] += $row['DETAILPRICE'];
	$SUM['GOODS']['DETAILQTY'] += $row['DETAILQTY'];
	$SUM['GOODS']['BASEBPRICE']+=$row['BASEBPRICE'];
	$SUM['GOODS']['TAX_PRICE']+=$row['TAX_PRICE'];
	$SUM['GOODS']['DOWNPRICE']+=$row['DOWNPRICE'];

	// 行先ごとにカウント(2015/08/04現在未使用)
	switch ($row['ROUTING_KIND']) {
		case 0:
			$SUM['ROUTE']['D']+=$row['DETAILQTY'];
			break;
		case 1:
			$SUM['ROUTE']['BOL']+=$row['DETAILQTY'];
			break;
		case 2:
			$SUM['ROUTE']['LOGI']+=$row['DETAILQTY'];
			break;
		case 3:
			$SUM['ROUTE']['MASTER']+=$row['DETAILQTY'];
			break;
		case 4:
			$SUM['ROUTE']['NO_MASTER']+=$row['DETAILQTY'];
			break;
		default:
			$retvalue= "エラー";
			break;

	}

}
$res->free();


/*-------- 集計ライン -----------*/
	fputcsv($fp, array(
		"",
		$sellno,
		$storeNo[$customerName],
		$customerName,
		"",
		"",
		"",
		"合計",
		"",
		"",
		"",
		"",
		"",
		"",
		$SUM['GOODS']['DETAILQTY'],
		$SUM['GOODS']['BASEBPRICE'],
		$SUM['GOODS']['DOWNPRICE'],
		$SUM['GOODS']['TAX_PRICE'],
		"",
		""
	));
/*-----------------------------*/


/*  ------- 廃棄商品情報ラインを生成　-------------- */
$discardSql = getDiscardSql($asscode);

$resDiscard = $db->query($discardSql);
if(DB::isError($resDiscard)){
	$resDiscard->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}


$discard = array();
$i = 0;
while($discardRow = $resDiscard->fetchRow(DB_FETCHMODE_ASSOC)){

	$discard[$i] = array();

	$discard[$i]["KIND"] = Retbooksoftkind($discardRow['BOOKSOFT_KIND']);
	$discard[$i]["QTY"] = intval($discardRow['QTY']);

	$reason = str_split(decbin($discardRow['DISCARDREASON_KIND']));
	$binLen = count($reason);
	//print_r($reason);
	$reasonStr = "";
	foreach ($reason as $key => $value) {
		//echo Retbooksoftd($value,$discardRow['BOOKSOFT_KIND']);
		if($value == 0) continue; // 0:「未チェック」は表示しない
		$reasonStr .= Retbooksoftd($binLen - $key,$discardRow['BOOKSOFT_KIND']);
		$reasonStr .= "　";
	}

	$discard[$i]["REASON"] = $reasonStr;
	$discard[$i]["REMARK"] = $discardRow['REMARK'];

	$i++;
}
$resDiscard->free();

foreach ($discard as $key => $value) {

	fputcsv($fp, array(
		"",
		$sellno,
		$storeNo[$customerName],
		$customerName,
		"",
		"",
		"",
		"D商品(".$value['KIND'].")",
		"",
		$value['REASON'] . $value['REMARK'],
		"",
		"",
		"",
		"",
		$value['QTY'],
		"",
		"",
		"",
		"",
		""
	));

}
/*------------------------------------------------------------*/

$db->disconnect();

/*------ csvに出力 ---------*/

// ローカルにファイルを保存せず、直接httpレスポンスとして返却する
// header("Content-Type: application/octet-stream");
// header("Content-Disposition: attachment; filename={$OUTPUTFILENAME}");

// // $fp = fopen($OUTPUTFILENAME, 'w');
// $fp = fopen('php://output', 'w');

// foreach ($lines as $line) {
//    fputcsv($fp, $line);
// //    print_r($line);
// }

fclose($fp);
/*--------------------------------*/