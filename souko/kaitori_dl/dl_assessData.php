<?php

function CurrentDir(){
	return dirname(__FILE__).'/';
}
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once(CurrentDir()."../log/loger.inc");
require_once(CurrentDir()."../parts/selectvalue_souko.php");
// require_once("../../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once(CurrentDir()."../parts/login_souko.php");
// require_once("../../parts/login_souko.php");

//関数定義ファイル読み込み
require_once(CurrentDir()."functions.php");
require_once(CurrentDir()."../common/bol_storeList.php");

// GETリクエストパラメータから買取番号を取得
// if(isset($_GET['sellNo'])){
//     $sellno = $_GET['sellNo'];
// } else{
//     die("買取受付番号が未指定のため、ダウンロードできません。");
// }

// コマンドラインから引数で買取受付番号を受け取る
if(isset($argv[1])){
	$sellno = $argv[1];
} else{
	echo "引数に買取受付番号が指定されていません。";
	exit(0);
}

// $sellno = "Z706900000078";
$asscode = convertSellNoToAssCode($sellno);
$customerName = getCustomerName($db, $asscode);

$OUTPUT_DIR = CurrentDir() . 'extractData/';
$OUTPUT_FILENAME = $OUTPUT_DIR . $sellno . ".csv";


// $lines = array();

// 買取店舗リストをヒープ
$storeNo = getStoreList();

// 集計値用
$SUM = array();
$SUM['GOODS']['DETAILPRICE']=0;
$SUM['GOODS']['DETAILQTY']=0;
$SUM['GOODS']['BASEBPRICE']=0;
$SUM['GOODS']['TAX_PRICE']=0;
$SUM['GOODS']['DOWNPRICE']=0;
$SUM['ROUTE']['BOL']=0;
$SUM['ROUTE']['MASTER']=0;
$SUM['ROUTE']['NO_MASTER']=0;
$SUM['ROUTE']['LOGI']=0;
$SUM['ROUTE']['D']=0;


// クエリ実行
$AssessDtlsql = getAssessDtlSql($asscode);
$res = $db->query($AssessDtlsql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

// 結果に行が1件もなければ処理を終了
if($res->numRows() < 0){
	output_log("結果に行が存在しません。処理を終了します。","dl_assessData","dl_assessData");
	exit(0);
}

$fp = fopen($OUTPUT_FILENAME, 'w');


// csvヘッダー情報
$CSV_HEADER = "明細番号,買取受付番号,店舗番号,店舗名,インストアコード,JANコード,行先,カテゴリ,部門コード,ジャンル1,ジャンル2,ジャンル3,商品名,著者/アーティスト,数量,基準買取金額,減点金額,買取金額,販売金額,在庫点数";
fputcsv($fp,explode(",", $CSV_HEADER));


while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){

	fputcsv($fp, array(
		$row['DETAILNO'],
		$sellno,
		$storeNo[$customerName],
		$customerName,
		$row['INSTORECODE'],
		$row['JANCODE'],
		Retkaitoriikisaki($row['ROUTING_KIND']),
		Retgoodscategory($row['GENRE0CODE']),
		$row['BUMON'],
		$row['GENRE1NAME'],
		$row['GENRE2NAME'],
		$row['GENRE3NAME'],
		$row['GOODSNAME'] . "　" .$row['GOODSNAME_EXT1'],
		$row['AUTHORNAME'],
		$row['DETAILQTY'],
		$row['BASEBPRICE'],
		$row['DOWNPRICE'],
		$row['TAX_PRICE'],
		$row['PRICE'],
		$row['STOCK']
	));

	// 各フィールドの合計をカウント
	$SUM['GOODS']['DETAILPRICE'] += $row['DETAILPRICE'];
	$SUM['GOODS']['DETAILQTY'] += $row['DETAILQTY'];
	$SUM['GOODS']['BASEBPRICE']+=$row['BASEBPRICE'];
	$SUM['GOODS']['TAX_PRICE']+=$row['TAX_PRICE'];
	$SUM['GOODS']['DOWNPRICE']+=$row['DOWNPRICE'];

	// 行先ごとにカウント(2015/08/04現在未使用)
	// switch ($row['ROUTING_KIND']) {
	// 	case 0:
	// 		$SUM['ROUTE']['D']+=$row['DETAILQTY'];
	// 		break;
	// 	case 1:
	// 		$SUM['ROUTE']['BOL']+=$row['DETAILQTY'];
	// 		break;
	// 	case 2:
	// 		$SUM['ROUTE']['LOGI']+=$row['DETAILQTY'];
	// 		break;
	// 	case 3:
	// 		$SUM['ROUTE']['MASTER']+=$row['DETAILQTY'];
	// 		break;
	// 	case 4:
	// 		$SUM['ROUTE']['NO_MASTER']+=$row['DETAILQTY'];
	// 		break;
	// 	default:
	// 		$retvalue= "エラー";
	// 		break;

	// }

}
$res->free();


/*-------- 集計ライン -----------*/
	fputcsv($fp, array(
		"",
		$sellno,
		$storeNo[$customerName],
		$customerName,
		"",
		"",
		"",
		"合計",
		"",
		"",
		"",
		"",
		"",
		"",
		$SUM['GOODS']['DETAILQTY'],
		$SUM['GOODS']['BASEBPRICE'],
		$SUM['GOODS']['DOWNPRICE'],
		$SUM['GOODS']['TAX_PRICE'],
		"",
		""
	));
/*-----------------------------*/


/*  ------- 廃棄商品情報ラインを生成　-------------- */
$discardSql = getDiscardSql($asscode);

$resDiscard = $db->query($discardSql);
if(DB::isError($resDiscard)){
	$resDiscard->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}


$discard = array();
$i = 0;
while($discardRow = $resDiscard->fetchRow(DB_FETCHMODE_ASSOC)){

	$discard[$i] = array();

	$discard[$i]["KIND"] = Retbooksoftkind($discardRow['BOOKSOFT_KIND']);
	$discard[$i]["QTY"] = intval($discardRow['QTY']);

	$reason = str_split($discardRow['DISCARDREASON_KIND']);
	//print_r($reason);
	$reasonStr = "";
	foreach ($reason as $key => $value) {
		//echo Retbooksoftd($value,$discardRow['BOOKSOFT_KIND']);
		if($value == 0) continue; // 0:「未チェック」は表示しない
		$reasonStr .= Retbooksoftd($value,$discardRow['BOOKSOFT_KIND']);
		$reasonStr .= "　";
	}

	$discard[$i]["REASON"] = $reasonStr;
	$discard[$i]["REMARK"] = $discardRow['REMARK'];

	$i++;
}
$resDiscard->free();

foreach ($discard as $key => $value) {

	fputcsv($fp, array(
		"",
		$sellno,
		$storeNo[$customerName],
		$customerName,
		"",
		"",
		"",
		"D商品(".$value['KIND'].")",
		"",
		$value['REASON'] . $value['REMARK'],
		"",
		"",
		"",
		"",
		$value['QTY'],
		"",
		"",
		"",
		"",
		""
	));

}
/*------------------------------------------------------------*/

$db->disconnect();

/*------ csvに出力 ---------*/

// ローカルにファイルを保存せず、直接httpレスポンスとして返却する
// header("Content-Type: application/octet-stream");
// header("Content-Disposition: attachment; filename={$OUTPUTFILENAME}");

// $fp = fopen($OUTPUT_FILENAME, 'w');
// // $fp = fopen('./', 'w');

// foreach ($lines as $line) {
//    fputcsv($fp, $line);
// //    print_r($line);
// }

fclose($fp);
/*--------------------------------*/