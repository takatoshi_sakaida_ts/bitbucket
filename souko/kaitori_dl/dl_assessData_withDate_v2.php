<?php
//ファイルの読み込み
//PEARの利用	 -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// require_once("../../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
// require_once("../../parts/login_souko.php");

//関数定義ファイル読み込み
require_once("./functions.php");
require_once("../common/bol_storeList.php");


// 実行時間を60分に変更(デフォルト30秒だと出力途中で止まる)
set_time_limit(3600);

$assessedSql = <<< EOM
	select s.sell_no as sell_no from tsell s, tsellassessment a 
	where s.sell_no = a.sell_no
	and substr(s.sell_no,1,5) = 'Z7069'
	and s.sell_stat in('05', '06', '07', '97')
EOM;

// GETリクエストパラメータから条件を生成
if(isset($_GET['target']) && $_GET['date_begin'] && $_GET['date_end'] ){
	if($_GET['target'] == "tran_dt"){
		$assessedSql .= " and substr(s.tran_dt,1,8) between '{$_GET['date_begin']}' and '{$_GET['date_end']}' ";
		$OUTPUTFILENAME = "振込日_" . $_GET['date_begin'] . "-" . $_GET['date_end'] . ".csv";
	}
	elseif($_GET['target'] == "ass_dt"){
		$assessedSql .= " and substr(a.ass_dt,1,8) between '{$_GET['date_begin']}' and '{$_GET['date_end']}' ";
		$OUTPUTFILENAME = "計算日_" . $_GET['date_begin'] . "-" . $_GET['date_end'] . ".csv";
	}
} else{
	die("抽出条件を確認してください。");
}

// echo $assessedSql;

// $sellno = "Z706900000078";
// $asscode = convertSellNoToAssCode($sellno);
// $customerName = getCustomerName($db, $asscode);

// $OUTPUTFILENAME = $sellno . ".csv";


// 買取店舗リストをヒープ
$storeNo = getStoreList();

/* -------------- ECDB接続 ---------------- */
$ec_usr = 'c21db';
$ec_pwd = 'C_372902';
$ec_dbn = 'ecdb';
$ec_dsn = "oci8://". $ec_usr . ":" . $ec_pwd . "@" . $ec_dbn;
//データベースへの接続開始
$ecdb = DB::connect($ec_dsn);
//エラーの抽出
if(DB::isError($ecdb)){
	die("Fail\n" . DB::errorMessage($ecdb) . "\n");
}

$assRes = $ecdb->query($assessedSql);
if(DB::isError($assRes)){
	$assRes->DB_Error($assRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$ecdb->disconnect();


header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $OUTPUTFILENAME);


// $DISCARD = array();
// $DISCARD['BOOK']["QTY"] = 0;
// $DISCARD['SOFT']["QTY"] = 0;


// ファイルオープン
// $fp = fopen($OUTPUTFILENAME, 'w');
$fp = fopen("php://output", 'w');

// csvヘッダー情報
$CSV_HEADER = "明細番号,買取受付番号,店舗番号,店舗名,インストアコード,JANコード,行先,カテゴリ,部門コード,ジャンル1,ジャンル2,ジャンル3,商品名,著者/アーティスト,数量,基準買取金額,減点金額,買取金額,販売金額,在庫点数";
fputcsv($fp,explode(",", $CSV_HEADER));

while($assRow = $assRes->fetchRow(DB_FETCHMODE_ASSOC)){

	$lines = array();
	$DISCARD = array();
	$DISCARD['BOOK']["QTY"] = 0;
	$DISCARD['SOFT']["QTY"] = 0;

            // 集計値用
            $SUM = array();
            $SUM['GOODS']['DETAILPRICE']=0;
            $SUM['GOODS']['DETAILQTY']=0;
            $SUM['GOODS']['BASEBPRICE']=0;
            $SUM['GOODS']['TAX_PRICE']=0;
            $SUM['GOODS']['DOWNPRICE']=0;
            $SUM['ROUTE']['BOL']=0;
            $SUM['ROUTE']['MASTER']=0;
            $SUM['ROUTE']['NO_MASTER']=0;
            $SUM['ROUTE']['LOGI']=0;
            $SUM['ROUTE']['D']=0;



	// print_r($assRow['SELL_NO']);
	// continue;
	$sellno = $assRow['SELL_NO'];
	$asscode = convertSellNoToAssCode($sellno);
	$customerName = getCustomerName($db, $asscode);



	// クエリ実行
	$AssessDtlsql = getAssessDtlSql($asscode);
	$res = $db->query($AssessDtlsql);
	// $res = $db->execute($ps_assDtl,$asscode);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}

	while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){

		fputcsv($fp, array(
			$row['DETAILNO'],
			$sellno,
			$storeNo[$customerName],
			$customerName,
			$row['INSTORECODE'],
			$row['JANCODE'],
			Retkaitoriikisaki($row['ROUTING_KIND']),
			Retgoodscategory($row['GENRE0CODE']),
			$row['BUMON'],
			$row['GENRE1NAME'],
			$row['GENRE2NAME'],
			$row['GENRE3NAME'],
			$row['GOODSNAME'] . "　" .$row['GOODSNAME_EXT1'],
			$row['AUTHORNAME'],
			$row['DETAILQTY'],
			$row['BASEBPRICE'],
			$row['DOWNPRICE'],
			$row['TAX_PRICE'],
			$row['PRICE'],
			$row['STOCK']
		));

		// 各フィールドの合計をカウント
		$SUM['GOODS']['DETAILPRICE'] += $row['DETAILPRICE'];
		$SUM['GOODS']['DETAILQTY'] += $row['DETAILQTY'];
		$SUM['GOODS']['BASEBPRICE']+=$row['BASEBPRICE'];
		$SUM['GOODS']['TAX_PRICE']+=$row['TAX_PRICE'];
		$SUM['GOODS']['DOWNPRICE']+=$row['DOWNPRICE'];

		// 行先ごとにカウント(2015/08/04現在未使用)
		// switch ($row['ROUTING_KIND']) {
		// 	case 0:
		// 		$SUM['ROUTE']['D']+=$row['DETAILQTY'];
		// 		break;
		// 	case 1:
		// 		$SUM['ROUTE']['BOL']+=$row['DETAILQTY'];
		// 		break;
		// 	case 2:
		// 		$SUM['ROUTE']['LOGI']+=$row['DETAILQTY'];
		// 		break;
		// 	case 3:
		// 		$SUM['ROUTE']['MASTER']+=$row['DETAILQTY'];
		// 		break;
		// 	case 4:
		// 		$SUM['ROUTE']['NO_MASTER']+=$row['DETAILQTY'];
		// 		break;
		// 	default:
		// 		$retvalue= "エラー";
		// 		break;

		// }

	}
	$res->free();


/*-------- 集計ライン -----------*/
    fputcsv($fp, array(
        "",
        $sellno,
        $storeNo[$customerName],
        $customerName,
        "",
        "",
        "",
        "合計",
        "",
        "",
        "",
        "",
        "",
        "",
        $SUM['GOODS']['DETAILQTY'],
        $SUM['GOODS']['BASEBPRICE'],
        $SUM['GOODS']['DOWNPRICE'],
        $SUM['GOODS']['TAX_PRICE'],
        "",
        ""
    ));
/*-----------------------------*/


	/*  ------- 廃棄商品ラインを生成　-------------- */
	$discardSql = getDiscardSql($asscode);
	$resDiscard = $db->query($discardSql);
	// $resDiscard = $db->execute($ps_discard,$asscode);
	if(DB::isError($resDiscard)){
		$resDiscard->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}

	$i = 0;
	while($discardRow = $resDiscard->fetchRow(DB_FETCHMODE_ASSOC)){

		$discard[$i] = array();

		$discard[$i]["KIND"] = Retbooksoftkind($discardRow['BOOKSOFT_KIND']);
		$discard[$i]["QTY"] = intval($discardRow['QTY']);

		$reason = str_split($discardRow['DISCARDREASON_KIND']);
		//print_r($reason);
		$reasonStr = "";
		foreach ($reason as $key => $value) {
			//echo Retbooksoftd($value,$discardRow['BOOKSOFT_KIND']);
			if($value == 0) continue; // 0:「未チェック」は表示しない
			$reasonStr .= Retbooksoftd($value,$discardRow['BOOKSOFT_KIND']);
			$reasonStr .= "　";
		}

		$discard[$i]["REASON"] = $reasonStr;
		$discard[$i]["REMARK"] = $discardRow['REMARK'];

		$i++;
	}
	$resDiscard->free();

	foreach ($discard as $key => $value) {

		fputcsv($fp, array(
			"",
			$sellno,
			$storeNo[$customerName],
			$customerName,
			"",
			"",
			"",
			"D商品(".$value['KIND'].")",
			"",
			$value['REASON'] . $value['REMARK'],
			"",
			"",
			"",
			"",
			$value['QTY'],
			"",
			"",
			"",
			"",
			""
		));
	}


/*------------------------------------------------------------*/

// csv出力
// foreach ($lines as $line) {
//	fputcsv($fp, $line);
// //	print_r($line);
// }

// unset($line);

} /* 全買取データ出力ここまで */


/*-------- 集計ライン -----------*/
	// fputcsv($fp, array(
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	"合計",
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	"",
	// 	$SUM['GOODS']['DETAILQTY'],
	// 	$SUM['GOODS']['BASEBPRICE'],
	// 	$SUM['GOODS']['DOWNPRICE'],
	// 	$SUM['GOODS']['TAX_PRICE'],
	// 	"",
	// 	""
	// ));
/*-----------------------------*/

// /* 査定廃棄ライン */
// 	fputcsv($fp, array(
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"D商品(本)",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		$DISCARD['BOOK']["QTY"],
// 		"",
// 		"",
// 		"",
// 		"",
// 		""
// 	));

// 	fputcsv($fp, array(
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"D商品(ソフト)",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		"",
// 		$DISCARD['SOFT']["QTY"],
// 		"",
// 		"",
// 		"",
// 		"",
// 		""
// 	));
/*---------------------------------------*/

$db->disconnect();

/*------ csvに出力 ---------*/

// ローカルにファイルを保存せず、直接httpレスポンスとして返却する
// header("Content-Type: application/octet-stream");
// header("Content-Disposition: attachment; filename={$OUTPUTFILENAME}");

// $fp = fopen($OUTPUTFILENAME, 'w');
// // $fp = fopen('php://output', 'w');

// foreach ($lines as $line) {
//	fputcsv($fp, $line);
// //	print_r($line);
// }

fclose($fp);
/*--------------------------------*/


// header('Content-Type: application/octet-stream');
// header('Content-Disposition: attachment; filename=' . $OUTPUTFILENAME);
// header('Content-Transfer-Encoding: binary');
// header('Content-Length: ' . filesize($OUTPUTFILENAME));
// readfile($OUTPUTFILENAME);

?>