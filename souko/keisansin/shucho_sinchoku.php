<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取進捗(出買)</title>
</head>
<body bgcolor="#ffffff">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$keyti= date("Ymd");

$sql = "Select T1.StaffCode, T1.StaffName, T1.F1, T1.F2, T2.F3, T2.F4, T3.F5 From (Select D_ASS.OPERATESTAFFCODE as StaffCode, M_STAFF.STAFFNAME as StaffName, Count(D_ASS.ASSCODE) as F1, Sum(D_ASS.BOX_RECEPT_QTY) as F2 From D_ASS, M_STAFF Where (D_ASS.ASSCODE like '00088002%' or D_ASS.ASSCODE like '00088012%') and D_ASS.OPERATESTAFFCODE = M_STAFF.STAFFCODE and D_ASS.STATUS_KIND <> '-1' and D_ASS.OPERATEDATE = '" .$keyti. 
"' GROUP BY D_ASS.OPERATESTAFFCODE, M_STAFF.STAFFNAME) T1, (Select D_ASS.OPERATESTAFFCODE as StaffCode, Sum(D_ASS_GOODS.DETAILQTY) as F3, Sum(D_ASS_GOODS.TAX_PRICE) as F4 From D_ASS, D_ASS_GOODS Where (D_ASS.ASSCODE like '00088002%' or D_ASS.ASSCODE like '00088012%') and D_ASS.ASSCODE = D_ASS_GOODS.ASSCODE and D_ASS.BRANCHNUM = D_ASS_GOODS.BRANCHNUM and D_ASS.STATUS_KIND <> '-1' and D_ASS_GOODS.MODIFYDATE = '" .$keyti. 
"' GROUP BY D_ASS.OPERATESTAFFCODE) T2, (Select D_ASS.OPERATESTAFFCODE as StaffCode, Sum(D_ASS_DISCARD.QTY) as F5 From  D_ASS, D_ASS_DISCARD Where (D_ASS.ASSCODE like '00088002%' or D_ASS.ASSCODE like '00088012%') and D_ASS.ASSCODE =  D_ASS_DISCARD.ASSCODE and  D_ASS.BRANCHNUM =  D_ASS_DISCARD.BRANCHNUM and D_ASS.STATUS_KIND <> '-1' and D_ASS_DISCARD.MODIFYDATE = '" .$keyti.
"' GROUP BY D_ASS.OPERATESTAFFCODE) T3 Where T1.StaffCode = T2.StaffCode and T1.StaffCode = T3.StaffCode Order by T1.F1 Desc";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>計算進捗(出買)　". substr($keyti,0,4). "年".substr($keyti,4,2)."月".substr($keyti,6,2)."日"."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td width=100>査定件数</td><td width=100>査定箱数</td><td width=100>計算点数</td><td width=100>計算金額</td><td width=100>Ｄ点数</td><td  nowrap nowrap>スタッフコード</td><td width=200>スタッフ名</td></tr>";
$i=0;
$j=0;
$k=0;
$l=0;
$m=0;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//査定件数
	print "<td>".$row[2] ."</td>";
//査定箱数
	print "<td>".$row[3] ."</td>";
//計算点数
	print "<td>".$row[4] ."</td>";
//計算金額
	print "<td>".$row[5] ."</td>";
//Ｄ点数
	print "<td>".$row[6] ."</td>";
//スタッフコード
	print "<td>".$row[0] ."</td>";
//スタッフ名
	print "<td>".$row[1] ."</td></tr>";
	$i=$i+$row[2];
	$j=$j+$row[3];
	$k=$k+$row[4];
	$l=$l+$row[5];
	$m=$m+$row[6];
}
print "<tr><td>".$i."</td><td>".$j."</td><td>".$k."</td><td>".$l."</td><td>".$m."</td><td colspan=2 align=center>　　計算合計　　</td></tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>