<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<?PHP
require_once("../parts/pagechk.inc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>トラック情報削除</title>
</head>
<body>
<HR>
すべてのトラック情報を削除します。<BR>
確認してから、「トラック情報削除」ボタンを押下してください。
<BR>
<?php
//**********************************************************************************************************
//ファイルの読み込み
//PEARの利用     -------(1)
//**********************************************************************************************************
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$_SESSION["HSMAP"]["INSTORECODE"] = trim(addslashes(@$_POST["TI"]));
$_SESSION["HSMAP"]["deltai"] = trim(addslashes(@$_POST["deltai"]));
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($_SESSION["HSMAP"]["INSTORECODE"])) {
	print "<BR><BR><B><font color=red >インストアコードを入力してください</font></B><br>";
}
else
{
$_SESSION["HSMAP"]["INSTORECODE"]=trim($_SESSION["HSMAP"]["INSTORECODE"]);

//出力ファイル名
$_SESSION["HSMAP"]["file_name_track"] = "./result/rmtrack_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
require_once("../log/loger.inc");
		print "削除対象システム：";
switch ($_SESSION["HSMAP"]["deltai"]){
	case '0':
		print "倉庫・ECシステム<BR>";
		$_SESSION["HSMAP"]["total_check_souko"] = '0';
		$_SESSION["HSMAP"]["total_check_ec"] = '0';
		break;
	case '1':
		print "倉庫システム<BR>";
		$_SESSION["HSMAP"]["total_check_ec"]='1';
		$_SESSION["HSMAP"]["total_check_souko"] = '0';
		break;
	case '2':
		print "ECシステム<BR>";
		$_SESSION["HSMAP"]["total_check_souko"]='1';
		$_SESSION["HSMAP"]["total_check_ec"] = '0';
		break;
}
if ($_SESSION["HSMAP"]["deltai"] =='0'||$_SESSION["HSMAP"]["deltai"] =='1')
{
//ログイン情報の読み込み
	require_once("../parts/login_souko.php");
//**********************************************************************************************************
//SQL文のCOUNT関数を使用
	$_SESSION["HSMAP"]["sql_souko"] = "SELECT j.JMDKEY,j.MUSICORDER,j.DISPLAYORDER,j.CONTENTS,j.MODIFYDATE,j.MODIFYTIME".
	" from m_goods_jmd j,m_jmd_goods_key k where k.jmdkey= j.jmdkey and instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."'";
	$res = $db->query($_SESSION["HSMAP"]["sql_souko"]);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($_SESSION["HSMAP"]["sql_souko"],$_SESSION["user_id"],'rmtrack');
//検索結果の表示
	print "<strong><BR>インストアコード：".$_SESSION["HSMAP"]["INSTORECODE"]."</strong> <br><br>\n<HR>";
	$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	print "<strong><BR>倉庫システム　トラック情報</strong><HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap width=120>JMDKEY</td><td nowrap width=40>MUSICORDER</td><td nowrap width=100>DISPLAYORDER</td><td nowrap width=100>CONTENTS</td><td nowrap width=100>MODIFYDATE</td><td nowrap width=120>MODIFYTIME</td></tr>";
//ファイル出力準備
		$fp = fopen($_SESSION["HSMAP"]["file_name_track"], "a" );
		fputs($fp,"JMDKEY,MUSICORDER,DISPLAYORDER,CONTENTS,MODIFYDATE,MODIFYTIME");
		fputs($fp,"\n");
	while($row = $res->fetchRow()){
		$str="";
		$warnmess="<br>";
		print "<tr>";
		$ncols = $res->numCols();
		for($i=0;$i<$ncols;$i++){
				print "<td>". $row[$i] . "</td>";//行の列データを取得
				$str=$str . $row[$i] . ",";//行の列データを取得
		$_SESSION["HSMAP"]["jmdkey"]= $row[0];
		}
		print "</tr>";
//ファイル出力
		$str = ereg_replace("\r|\n","",$str);
		fputs($fp,$str);
		fputs($fp,"\n");
		$j++;
		$_SESSION["HSMAP"]["total_check_souko"]='1';
	}
	print "</table>";
	fclose( $fp );
//データの開放
	$res->free();
}
if ($_SESSION["HSMAP"]["deltai"] =='0'||$_SESSION["HSMAP"]["deltai"] =='2')
{
//**********************************************************************************************************
//EC側処理
//**********************************************************************************************************
	//ログイン情報の読み込み
	require_once("../parts/login_ec.php");
	$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
	//データベースへの接続開始
	$db = DB::connect($dsn);
	//エラーの抽出
	if(DB::isError($db)){
		echo "Fail\n" . DB::errorMessage($db) . "\n";
	}
//
//******************************************************
	$_SESSION["HSMAP"]["sql_ec"]="SELECT INSTORECODE,SUITE_NO,TRACK_NO,TRACK_INFO,MOD_DT,REG_DM,UPD_DM,BATCH_EXEC_DM FROM TGOODS_MUSIC_TRACK WHERE INSTORECODE = '".$_SESSION["HSMAP"]["INSTORECODE"]."' order by SUITE_NO,TRACK_NO+0";
	$res = $db->query($_SESSION["HSMAP"]["sql_ec"]);
	if(DB::isError($res)){
		print $res->getMessage();
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($_SESSION["HSMAP"]["sql_ec"],$_SESSION["user_id"],'rmtrack');
//検索結果の表示
	$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	print "<strong><BR>ECシステム　トラック情報</strong><HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap width=100>INSTORECODE</td><td nowrap width=100>SUITE_NO</td><td nowrap width=100>TRACK_NO</td><td nowrap width=100>TRACK_INFO</td><td nowrap width=100>MOD_DT</td><td nowrap width=100>REG_DM</td><td nowrap width=100>UPD_DM</td><td nowrap width=100>BATCH_EXEC_DM</td></tr>";
//項番
//ファイル出力準備
		$fp = fopen($_SESSION["HSMAP"]["file_name_track"], "a" );
		fputs($fp,"INSTORECODE,SUITE_NO,TRACK_NO,TRACK_INFO,MOD_DT,REG_DM,UPD_DM,BATCH_EXEC_DM");
		fputs($fp,"\n");

	while($row = $res->fetchRow()){
	$str="";
		print "<tr>";
		$ncols = $res->numCols();
		for($i=0;$i<$ncols;$i++){
				print "<td>". $row[$i] . "　</td>";//行の列データを取得
				$str=$str . $row[$i] . ",";//行の列データを取得
		}
		print "</tr>";
//ファイル出力
		$str = ereg_replace("\r|\n","",$str);
		fputs($fp,$str);
		fputs($fp,"\n");
		$j++;
		$_SESSION["HSMAP"]["total_check_ec"]='1';
	}
	print "</table>";
	fclose( $fp );
	$res->free();
}
//******************************************************
$db->disconnect();
?>
<BR>
<?PHP
if ($_SESSION["HSMAP"]["total_check_souko"]=='0' || $_SESSION["HSMAP"]["total_check_ec"]=='0')
{
print "該当のトラック情報はありません。";
}else{
?>
<FORM action="rmtrackent.php"><INPUT TYPE="submit" VALUE="トラック情報削除" size="20"></FORM>
<?PHP
}
//インストアコードの入力チェックのエンド
}
?>
<FORM action="searchrmtrack.php"><INPUT TYPE="submit" VALUE="戻る" size="20"></FORM>
</body>
</html>