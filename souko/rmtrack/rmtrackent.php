<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<?PHP
require_once("../parts/pagechk.inc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>トラック情報削除結果</title>
</head>
<body>
<?php
//**********************************************************************************************************
//ファイルの読み込み
//PEARの利用     -------(1)
//**********************************************************************************************************
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
// 前画面からの検索キーワードを取得する

//ログイン情報の読み込み
require_once("../parts/login_souko.php");
/*
print $_SESSION["HSMAP"]["sql_souko_del"];
print "<BR>";
print $_SESSION["HSMAP"]["sql_ec_del"];
*/
//**********************************************************************************************************
if ($_SESSION["HSMAP"]["deltai"] =='0'||$_SESSION["HSMAP"]["deltai"] =='1')
{
$_SESSION["HSMAP"]["sql_souko_del"]="delete m_goods_jmd where jmdkey='".$_SESSION["HSMAP"]["jmdkey"]."'";
//SQLの実行
	$db->autoCommit( false ); 
	//d_reception_goods
	$sql=$_SESSION["HSMAP"]["sql_souko_del"];
	$res2 = $db->query($sql);
	if(DB::isError($res2)){
		print $res2->getMessage();
		$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($sql,$_SESSION["user_id"],'rmtrack_sql');
	$db->commit();
	$db->disconnect();
}
if ($_SESSION["HSMAP"]["deltai"] =='0'||$_SESSION["HSMAP"]["deltai"] =='2')
{
$_SESSION["HSMAP"]["sql_ec_del"]="delete TGOODS_MUSIC_TRACK where instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."'";
//EC側で他更新
//ログイン情報の読み込み
	require_once("../parts/login_ec.php");
	$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
	$update_chk=0;
//データベースへの接続開始
	$db2 = DB::connect($dsn);
//エラーの抽出
	if(DB::isError($db2)){
		echo "Fail\n" . DB::errorMessage($db2) . "\n";
	}
	$db2->autoCommit( false ); 
	$res = $db2->query($_SESSION["HSMAP"]["sql_ec_del"]);
	if(DB::isError($res)){
		print $res->getMessage();
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
			$update_chk=1;
	}
	output_log($_SESSION["HSMAP"]["sql_ec_del"],$_SESSION["user_id"],'rmtrack_sql');
	$db2->commit();
	$db2->disconnect();
}

?>
<BR>
	<center><font size="+1">正常に削除しました
<br><br>
インストアコード：
<?PHP
print $_SESSION["HSMAP"]["INSTORECODE"];
//データの開放
unset($_SESSION["HSMAP"]);
?>
<BR>
</font>
<?PHP
print '<br><a href="/index.php">メニューに戻る</a><br>';
?>
<BR>
<FORM action="searchrmtrack.php"><INPUT TYPE="submit" VALUE="トラック削除TOPに戻る"></FORM>
</center>
</body>
</html>