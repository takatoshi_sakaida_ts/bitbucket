<?php
require '../parts/pagechk.inc';
$today = date("YmdHis");
$file_name = "./dlfile/new_freecorrugated_rakuten_" . $today . ".tsv";
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
output_log("【新】楽天用無料ダンボール発送用登録データダウンロード",$_SESSION["user_id"],'freecorrugatedrakuten');
// 前画面からの検索キーワードを取得する
$keyti = str_replace("'","",@$_POST["TI"]);
$keyti = addslashes($keyti);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
}
$new_text = preg_replace("/(\r\n|\n|\r)/", ",", $keyti);
$new_text = preg_replace("/,,/", "", $new_text);
$new_text = preg_replace("/,$/", "", $new_text);
$new_text = str_replace(",", "','", $new_text);
$new_text = "'".$new_text."'";
//print $new_text;

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
$sql="select distinct sell_no,"
."mem_no,"
."mem_id,"
."PICKUP_NM_FST||"
."PICKUP_NM_MID,"
."PICKUP_NM_LAST||"
."PICKUP_NM_ETC,"
."PICKUP_ZIP_CD,"
."PICKUP_ZIP_ADDR1||"
."PICKUP_ZIP_ADDR2||"
."PICKUP_ZIP_ADDR3||"
."PICKUP_DTL_ADDR,"
."PICKUP_TEL_NO,"
."'' as blank1,"
."'' as blank2,"
."BOX"
." from tsell"
." where (replace(EXT_SELL_NO,'-','') in (" .$new_text. ")) or (EXT_SELL_NO in (" .$new_text. "))";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	require '../parts/outputcsv_query.php';
	outputcsv_tab($res,$file_name);
$res->free();
//
$db->disconnect();

?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>【新】楽天用無料ダンボール発送用登録データダウンロード</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>