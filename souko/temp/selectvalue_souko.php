<?PHP
function Retgoodscategory($category)
{
//カテゴリ名称返却
switch ($category){
	case 11:
		$retvalue= "コミック";
		break;
	case 12:
		$retvalue= "書籍";
		break;
	case 13:
		$retvalue= "雑誌";
		break;
	case 31:
		$retvalue= "CD";
		break;
	case 51:
		$retvalue= "GAME";
		break;
	case 71:
		$retvalue= "DVD";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retgoodsdetailsql($instorecode,$category)
{
//カテゴリ名称と詳細検索用SQL返却
switch ($category){
	case 11:
		$retvalue[0]= "コミック";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.VOLUMENO,b.AUTHORNAME from M_GOODS_BOOK a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	case 12:
		$retvalue[0]= "書籍";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.VOLUME,b.AUTHORNAME from M_GOODS_BOOK a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	case 13:
		$retvalue[0]= "雑誌";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.VOLUMENO,a.PUBLISHNAME from M_GOODS_BOOK a where a.instorecode='". $instorecode ."'";
		break;
	case 31:
		$retvalue[0]= "CD";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.TOTALSUITE,b.AUTHORNAME from M_GOODS_MUSIC a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	case 51:
		$retvalue[0]= "GAME";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.SPECCODE,a.PUBLISHNAME from M_GOODS_GAME a where a.instorecode='". $instorecode ."'";
		break;
	case 71:
		$retvalue[0]= "DVD";
		$retvalue[1]= "select a.TITLE,a.TITLEKANA,a.PUBLISHNAME,b.AUTHORNAME from M_GOODS_VIDEO a,M_GOODS_AUTHOR b where a.instorecode=b.instorecode(+) and b.AUTHORNO='0' and a.instorecode='". $instorecode ."'";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retstatuskind($index)
{
//注文ステータス返却
switch ($index){
	case 0:
		$jyoutai= "納品書未印刷";
		break;
	case 1:
		$jyoutai= "納品書印刷済";
		break;
	case 2:
		$jyoutai= "出荷準備完了";
		break;
	case 3:
		$jyoutai= "出荷完了(EC未連携)";
		break;
	case 4:
		$jyoutai= "出荷完了(EC連携済)";
		break;
	case 9:
		$jyoutai= "全て";
		break;
	default:
		$jyoutai= "エラー";
		break;
}
	return $jyoutai;
}
function Retmakerstatuskind($index)
{
//メーカー注文ステータス返却
switch ($index){
	case 1:
		$jyoutai= "発注依頼済";
		break;
	case 2:
		$jyoutai= "依頼完了";
		break;
	case 3:
		$jyoutai= "発注完了";
		break;
	case 4:
		$jyoutai= "問い合わせ中";
		break;
	case 5:
		$jyoutai= "メーカー欠品";
		break;
	case 6:
		$jyoutai= "入荷待ち";
		break;
	case 7:
		$jyoutai= "入荷欠品";
		break;
	case 8:
		$jyoutai= "入荷済";
		break;
	case 99:
		$jyoutai= "全て";
		break;
	default:
		$jyoutai= "エラー";
		break;
}
	return $jyoutai;
}
function Retkaitoristatuskind($index)
{
//メーカー注文ステータス返却
switch ($index){
	case 0:
		$jyoutai= "未承認（査定新規/修正した状態）";
		break;
	case 1:
		$jyoutai= "未承認（ECへ査定結果送信した状態）";
		break;
	case 2:
		$jyoutai= "承認";
		break;
	case 3:
		$jyoutai= "仕入済";
		break;
	case 4:
		$jyoutai= "仕分済";
		break;
	case 99:
		$jyoutai= "全て";
		break;
	default:
		$jyoutai= "エラー";
		break;
}
	return $jyoutai;
}
function Retkessai($index)
{
//決済方法返却
switch ($index){
	case 1:
		$retvalue= "クレジット";
		break;
	case 2:
		$retvalue= "代引";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retunsoukaisha($index)
{
//配送方法返却
if (empty($index)){
	$retvalue='未設定';
}else{
	switch ($index){
		case 1:
			$retvalue= "日通";
			break;
		case 2:
			$retvalue= "郵政";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
}
	return $retvalue;
}
function Retlocation($index)
{
//ロケーション返却
switch ($index){
	case 9000000000:
		$location= "ECフリー";
		break;
	case 9001000000:
		$location= "リザーブフリー";
		break;
	case 9002000000:
		$location= "仕分前フリー";
		break;
	case 9003000000:
		$location= "新品フリー";
		break;
	default:
		if ($index>=1001001){
			if ($index<=1001999){
				$location='リザーブ';
			}else{
				$location=$index;
			}
		}else{
				$location=$index;
		}
		break;
}
	return $location;
}
function Retzaikokubun($index)
{
//商品の区分返却
switch ($index){
	case 0:
		$zaiko= "中古";
		break;
	case 1:
		$zaiko= "新古";
		break;
	case 2:
		$zaiko= "新品";
		break;
	default:
		$zaiko= "エラー";
		break;
}
	return $zaiko;
}
function Retpickubun($index)
{
//ピッキング状態返却
switch ($index){
	case 0:
		$retvalue= "未完了";
		break;
	case 1:
		$retvalue= "完了";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retcancel($index)
{
//キャンセル状態返却
	if ($index==1){
		$retvalue = "キャンセル";
	}else{
		$retvalue = "　";
	}
	return $retvalue;
}
function Rethaisoubangou($index)
{
//日通配送番号整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,3). "-" .substr($index,3,2). "-" .substr($index,5,3). "-" .substr($index,8,4);
}
	return $retvalue;
}
function Rethiduke($index)
{
//日付整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2);
}
	return $retvalue;
}
function Rethidukejikan($index)
{
//日付時間整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2). " ".substr($index,8,2). ":".substr($index,10,2). ":" .substr($index,12,2);
}
	return $retvalue;
}
function Retzip($index)
{
//郵便番号整形
if (empty($index)){
	$retvalue='〒';
}else{
	$retvalue = "〒".substr($index,0,3). "-" .substr($index,3,4);
}
	return $retvalue;
}
function Rettel($index)
{
//電話番号整形
if (empty($index)){
	$retvalue='　';
}else{
	switch (substr($index,1,1)){
		case 3:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		case 6:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		default:
			if (strlen($index)==10){
				$retvalue = substr($index,0,3). "-" .substr($index,3,3). "-" .substr($index,6,4);
			}else{
				$retvalue = substr($index,0,3). "-" .substr($index,3,4). "-" .substr($index,7,4);
			}
			break;
	}
}
	return $retvalue;
}
function Rettorihikisaki($index)
{
//発注取引先名称返却
switch ($index){
	case "SKD-":
		$retvalue= "星光堂";
		break;
	case "TYS-":
		$retvalue= "太洋社";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retyoyakukubun($index)
{
//予約区分返却
switch ($index){
	case 0:
		$retvalue= "通常発注";
		break;
	case 1:
		$retvalue= "予約品(未引当)";
		break;
	case 2:
		$retvalue= "予約品(引当済)";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethachustatus($index)
{
//新品・新刊発注ステータス
switch ($index){
	case 1:
		$retvalue= "発注依頼済";
		break;
	case 2:
		$retvalue= "依頼完了";
		break;
	case 3:
		$retvalue= "発注完了";
		break;
	case 4:
		$retvalue= "問い合わせ中";
		break;
	case 5:
		$retvalue= "メーカー欠品";
		break;
	case 6:
		$retvalue= "入荷待ち";
		break;
	case 7:
		$retvalue= "入荷欠品";
		break;
	case 8:
		$retvalue= "入荷済";
		break;
	case 99:
		$retvalue= "入荷済以外";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethachukaitou($index)
{
//発注回答返却
switch ($index){
	case 0:
		$retvalue= "キャンセル受付完了他";
		break;
	case 1:
		$retvalue= "在庫なし取寄せ中";
		break;
	case 2:
		$retvalue= "廃盤";
		break;
	case 3:
		$retvalue= "製造中止";
		break;
	case 4:
		$retvalue= "販売中止";
		break;
	case 5:
		$retvalue= "発売中止";
		break;
	case 6:
		$retvalue= "発売日前";
		break;
	case 7:
		$retvalue= "出荷停止";
		break;
	case 9:
		$retvalue= "データエラー";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethachuzankaitou($index)
{
//発注残回答
switch ($index){
	case 0:
		$retvalue= "キャンセル受付完了他";
		break;
	case 1:
		$retvalue= "入荷";
		break;
	case 2:
		$retvalue= "取寄せ中";
		break;
	case 3:
		$retvalue= "廃盤";
		break;
	case 4:
		$retvalue= "販売中止";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retbooksoftkind($index)
{
//発注残回答
switch ($index){
	case 0:
		$retvalue= "本";
		break;
	case 1:
		$retvalue= "ソフト";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Retbooksoftd($value,$shurui){
if ($shurui==0){
	switch ($value){
		case 0:
			$retvalue= "未チェック";
			break;	
		case 1:
			$retvalue= "焼け";
			break;
		case 2:
			$retvalue= "表紙に日焼け";
			break;
		case 3:
			$retvalue= "カバーがない";
			break;
		case 4:
			$retvalue= "書き込み有";
			break;
		case 5:
			$retvalue= "破れ";
			break;
		case 6:
			$retvalue= "全体的に状態が悪い";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
}else{
	switch ($value){
		case 0:
			$retvalue= "未チェック";
			break;	
		case 1:
			$retvalue= "盤に傷が多い";
			break;
		case 2:
			$retvalue= "全体的に状態が悪い";
			break;
		case 3:
			$retvalue= "特殊ケース不良";
			break;
		case 4:
			$retvalue= "付属品なし";
			break;
		case 5:
			$retvalue= "ジャケット、歌詞カード不良";
			break;
		case 6:
			$retvalue= "箱・説明書なし";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
}
	return $retvalue;
}
?>