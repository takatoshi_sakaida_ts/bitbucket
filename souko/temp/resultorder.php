<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti2 = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) && empty($keyti2)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("login_souko.php");
//dsnの書式　データベースのタイプ://ユーザ名:パスワード@ホスト名/データベース名
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
$sql = "select d.RECEPTIONDATE,d.STATUS_KIND,d.CUSTOMERNAME1,d.COD_PRICE,d.SHIPPINGPRICE,d.EXPENCE_KIND,d.RECEPTION_AMOUNT,";
$sql = $sql . "d.RECEPTION_C_TAX,s.TRANSPORT_KIND,s.DELIVERYCODE,s.DELIVERYWEIGHT,s.DELIVERYDATETIME,";
$sql = $sql . "d.SHIPPINGNAME1,d.SHIPPINGNAME2,d.SHIPPINGPOSTCODE,";
$sql = $sql . "d.SHIPPINGADDRESS1,d.SHIPPINGADDRESS2,d.SHIPPINGADDRESS3,d.SHIPPINGADDRESS4,d.SHIPPINGADDRESS5,d.SHIPPINGTELNO,d.receptioncode ";
$sql = $sql . "from d_reception d,D_RECEPTION_SHIPMENT s";
$sql = $sql . " where d.RECEPTIONCODE=s.RECEPTIONCODE(+)";
if (empty($keyti)) {}else{
	$sql = $sql . " and d.receptioncode ='".$keyti."'";
}
if (empty($keyti2)) {}else{
	$sql = $sql . " and (d.CUSTOMERNAME1 like '%".$keyti2."%'";
	$sql = $sql . " or d.CUSTOMERNAME2 like '%".$keyti2."%'";
	$sql = $sql . " or d.CUSTOMERNAMEKANA1 like '%".$keyti2."%'";
	$sql = $sql . " or d.CUSTOMERNAMEKANA2 like '%".$keyti2."%')";
}
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索結果". $keyti."</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>項番</td><td nowrap width=120>注文番号</td><td nowrap width=120>受付日</td><td nowrap width=200>状態</td><td nowrap width=120>注文者名</td>";
print "<td nowrap width=80>決済方法</td><td nowrap width=80>代引金額</td><td nowrap width=80>送料</td>";
print "<td nowrap>注文金額(税込)</td></tr>";
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//注文番号
	print "<td><a href='detailorder.php?receptioncode=" .$row[21] . "'>".$row[21] ."</td>";
//	print "<td>" .$row[21] . "<input type=hidden value=" . $row[21] . ">" ."</td>";
//受付日
	print "<td>".Rethiduke($row[0]) ."</td>";
//状態
		print "<td>".Retstatuskind($row[1])."</td>";
//顧客名
	print "<td>".$row[2] ."</td>";
//決済方法
	print "<td>".Retkessai($row[5]) ."</td>";
//代引金額
	print "<td align=right>".$row[3] ."</td>";
//送料
	print "<td align=right>".$row[4] ."</td>";
//注文金額合計(税込)
	print "<td align=right>".$row[6] ."</td>";
	print "</tr></table>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap width=120>運送会社</td><td nowrap width=200>出荷コード</td><td nowrap width=120>出荷登録日時</td></tr>";
//運送会社
	print "<tr><td>".Retunsoukaisha($row[8])."</td>";
//出荷コード
	print "<td>".Rethaisoubangou($row[9]) ."</td>";
//出荷日時
	print "<td>".Rethidukejikan($row[11]) ."</td>";
	print "</TR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap width=120>送付先名</td><td nowrap width=500>送付先住所</td><td nowrap width=120>送付先連絡先</td></tr>";
	print "<tr><td>".$row[12].$row[13]."</td>";
	print "<td>".Retzip($row[14])."<BR>".$row[15].$row[16].$row[17].$row[18].$row[19]."</td>";
	print "<td>".Rettel($row[20])."</td>";
	print "</TR>";
	print "</table>";
	print"<HR>";
	$j++;
}
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>