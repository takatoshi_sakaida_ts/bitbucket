<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="vol_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>モジュールテスト</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" action="vol_confirm.php" name="volfm">
<?php
require '../parts/pagechk.inc';
//最大値の取得
$sql='SELECT MAX(SELL_NO) FROM TSELL WHERE SELL_NO='; //団体毎の概念を追加
//最大値に１をプラス
$Retno = '00000000'+1;
//print $Retno;
//桁数の補正
print substr('00000000',1,(8-strlen($Retno))).$Retno;
?>
<b>団体名</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>団体名</th>
    <td>
    <select name="DANTAI">
			<option value="0" >選択してください
			<option value="1" >RoomToRead
			<option value="2" >シャンティ
			<option value="3" >ピースウィンズジャパン
			<option value="4" >シャプラニール
			<option value="5" >JEN
		</select>
    </td>
  </tr>
</TABLE>
<br>
<b>集荷先情報</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>名前</th>
    <td>
		<input type="text" name="PICKUP_NM_FST" size="40" maxlength="25" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
print '" >';
?>
		<input type="text" name="PICKUP_NM_MID" size="40" maxlength="25" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>フリガナ</th>
    <td>
		<input type="text" name="PICKUP_NM_LAST" size="40" maxlength="25" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_NM_LAST"];
print '" >';
?>
		<input type="text" name="PICKUP_NM_ETC" size="40" maxlength="25" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_NM_ETC"];
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>郵便番号</th>
    <td>
	例：1540004<br>
	<input type="text" name="PICKUP_ZIP_CD" size="8" maxlength="7" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_ZIP_CD"];
print '" >';
?>
	<input type="button" name="search_address" value="住所検索" onClick="OpenPostWin()">
    </td>
  </tr>

  <tr>
    <th>都道府県</th>
    <td>
	<select name="PICKUP_ZIP_ADDR1">
		<option value="0" >選択してください
		<option value="1" >北海道
		<option value="2" >青森県
		<option value="3" >岩手県
		<option value="4" >秋田県
		<option value="5" >宮城県
		<option value="6" >山形県
		<option value="7" >福島県
		<option value="8" >茨城県
		<option value="9" >栃木県
		<option value="10" >群馬県
		<option value="11" >埼玉県
		<option value="12" >千葉県
		<option value="13" >東京都
		<option value="14" >神奈川県
		<option value="15" >新潟県
		<option value="16" >富山県
		<option value="17" >石川県
		<option value="18" >福井県
		<option value="19" >山梨県
		<option value="20" >長野県
		<option value="21" >岐阜県
		<option value="22" >静岡県
		<option value="23" >愛知県
		<option value="24" >三重県
		<option value="25" >滋賀県
		<option value="26" >京都府
		<option value="27" >大阪府
		<option value="28" >兵庫県
		<option value="29" >奈良県
		<option value="30" >和歌山県
		<option value="31" >鳥取県
		<option value="32" >島根県
		<option value="33" >岡山県
		<option value="34" >広島県
		<option value="35" >山口県
		<option value="36" >徳島県
		<option value="37" >香川県
		<option value="38" >愛媛県
		<option value="39" >高知県
		<option value="40" >福岡県
		<option value="41" >佐賀県
		<option value="42" >長崎県
		<option value="43" >熊本県
		<option value="44" >大分県
		<option value="45" >宮崎県
		<option value="46" >鹿児島県
		<option value="47" >沖縄県
	</select>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR2" size="70" maxlength="20" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"];
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR3" size="80" maxlength="20" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"];
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR4" size="100" maxlength="20" style="ime-mode:active;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"];
print '" >';
?>
    </td>
  </tr>

  <tr>
    <th>電話番号</th>
    <td>
	<input type="text" name="PICKUP_TEL_NO" size="20" maxlength="13" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_TEL_NO"];
print '"> 	例： 042-786-1235<BR>';
?>
	</td>
  </tr>

  </tr>
  <tr>
    <th>生年月日</th>
    <td>
	<input type="text" name="MEM_BIRTH_DTY" size="6" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["MEM_BIRTH_DTY"];
print '" >年';
?>
	<input type="text" name="MEM_BIRTH_DTM" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["MEM_BIRTH_DTM"];
print '" >月';
?>
	<input type="text" name="MEM_BIRTH_DTD" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["MEM_BIRTH_DTD"];
print '" >日';
?>
    </td>
  </tr>

  <tr>
    <th>性別</th>
    <td>
			<input name="MEM_SEX" type="radio" value="1">男
			<input name="MEM_SEX" type="radio" value="2">女
    </td>
  </tr>
  <tr>
    <th>職業</th>
    <td>
	<select name="MEM_JOB">
		<option value="0" >選択してください
		<option value="1" >会社員
		<option value="2" >公務員
		<option value="3" >主婦
		<option value="4" >自営業
		<option value="5" >学生
		<option value="6" >パート
		<option value="7" >フリーター
		<option value="8" >無職
		<option value="9" >その他
	</select>
    </td>
  </tr>
</table>
<BR>
<b>集荷希望日など</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>箱数</th>
    <td>
	<input type="text" name="BOX" size="13" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["BOX"];
print '" >';
?>
	</td>
  </tr>
  <tr>
    <th>集荷希望年月日</th>
    <td>
	<input type="text" name="PICKUP_REQ_DTY" size="6" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_REQ_DTY"];
print '" >年';
?>
	<input type="text" name="PICKUP_REQ_DTM" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_REQ_DTM"];
print '" >月';
?>
	<input type="text" name="PICKUP_REQ_DTD" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
//print $_SESSION["HSMAP"]["PICKUP_REQ_DTD"];
print '" >日';
?>
    </td>
  </tr>
  <tr>
    <th>集荷希望時間帯</th>
    <td>
	<select name="PICKUP_REQ_TIME">
		<option value="0" >選択してください
		<option value="1" >午前
		<option value="2" >午後
		<option value="3" >夕方以降
	</select>
    </td>
  </tr>


<input type="hidden" name="DANTAI_index" value="
<?PHP
print $_SESSION["HSMAP"]["DANTAI_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
print '" >';
?>
<input type="hidden" name="MEM_JOB_index" value="
<?PHP
print $_SESSION["HSMAP"]["MEM_JOB_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
print ($_SESSION["HSMAP"]["MEM_SEX"]);
print '" >';
?>
</TABLE>
<BR>
<INPUT TYPE="BUTTON" VALUE="　　保存せずに戻る　　" onClick="history.back()">
<input type="BUTTON" value="登録" onClick="EntCheck()">
</FORM>
</body>
</html>