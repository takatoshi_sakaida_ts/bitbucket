<?PHP
// 銀行情報のセット
//振込先が銀行ではなく、ゆうちょ銀行の場合はVOLOUTの改修が必要です
switch ($_SESSION["HSMAP"]["DANTAI"]){
	case 1:
//NTT大阪支店
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="005";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大阪営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="BFB410C4B4DF4027";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニシニホンデンシンデンワ（カ）オオサカシテンビーエースイトウ";
		$sellno_head="01";
		break;
	case 2:
//スターバックス
		$_SESSION["HSMAP"]["BANK_CD"]="0401";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="802";
		$_SESSION["HSMAP"]["BANK_NM"]="シティバンク、エヌ・エイ ";
		$_SESSION["HSMAP"]["BRANCH_NM"]="ヤマブキ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="B99AEB0A5E31552E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="スターバックスコーヒージャパン（カ";
		$sellno_head="02";
		break;
	case 3:
//teijin
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="480";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="横浜支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5B6A3D51CFD0F395";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ブックドリームプロジェクトオカノリコ";
		$sellno_head="03";
		break;
	case 4:
//セガサミー
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="321";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="E3D077304E35FD9F";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="セガサミーホールディングス（カ";
		$sellno_head="04";
		break;
	case 5:
//NTTネオメイト
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="005";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大阪営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="AFF32F6E0A0D67C2";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）エヌティティネオメイト";
		$sellno_head="05";
		break;
	case 6:
//NEC(東日本大震災支援)
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head="06";
		break;
	case 7:
//キャドバリージャパン
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="331";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="神田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="98CEAE99D14B7A07";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="キヤドバリージヤパン（カ";
		$sellno_head="07";
		break;
	case 8:
//ファミリーマート
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="93A3C40DAABFD286";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）フアミリーマート";
		$sellno_head="08";
		break;
	case 9:
//結核予防会
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="532";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="九段支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="9DFAE09FAF71C34C";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）ケツカクヨボウカイ";
		$sellno_head="09";
		break;
	case 10:
//ナック
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="259";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新宿西口支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="68798E7A529E1F67";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ナックシーエスアールイインカイ　ダイヒョウ　タナカ　ヒデカズ";
		$sellno_head="10";
		break;
	case 11:
//アイエスエフネット
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="609";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="赤坂支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="67BFF7B1E1699E01";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンフユーチヤードリーム";
		$sellno_head="11";
		break;
	case 12:
//インテリジェンス
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="211";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="青山支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="A940DA716735F3BE";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）インテリジエンス";
		$sellno_head="12";
		break;
	case 13:
//チャーティス・ファー・イースト・ホールティングス
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="218";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="麹町支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="0204188AB987737B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="チャーティス・ファー・イースト・ホールティングスカブシキガイ";
		$sellno_head="13";
		break;
	case 14:
//民謡センター
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="664";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="飯田橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3FA5273FA4E97B24";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="コウエキザイダンホウジンミンサイセンターリジチョウ";
		$sellno_head="14";
		break;
	case 15:
//ＤＮＰグループ労連
		$_SESSION["HSMAP"]["BANK_CD"]="2963";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="299";
		$_SESSION["HSMAP"]["BANK_NM"]="中央労働金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="市谷支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="743F5CDFCD08B27A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ダイニッポンインサツロウドウクミアイロウキングチ";
		$sellno_head="15";
		break;
	case 16:
//小学校をおくる会
		$_SESSION["HSMAP"]["BANK_CD"]="0168";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="101";
		$_SESSION["HSMAP"]["BANK_NM"]="中国銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="DFE64E61A6A74E49";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ショウガッコウヲオクルカイリジオカムラハルオ";
		$sellno_head="16";
		break;
	case 17:
//レオパレス21(企業ボランティア宅本便)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="300";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="6AA33864A6B043A9";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head="17";
		break;
	case 18:
//神奈川大学
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="251";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="横浜駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="9046F3CCEBDA258E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ガッコウホウジンカナガワダイガク";
		$sellno_head="18";
		break;
	case 19:
//ICAN（アイキャン）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="221";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="名古屋駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="77B91EFA5FB3B975";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンアイキャンダイヒョウリジタグ";
		$sellno_head="19";
		break;
	case 20:
//阪急阪神ホールディングス
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="044";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="梅田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2"; //当座
		$_SESSION["HSMAP"]["KOUZABANGOU"]="7890A037B1B5B717";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ハンキュウハンシンホールデイングス（カ";
		$sellno_head="20";
		break;
	case 21:
//ソニー
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="351";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本郷支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="BA1D59044713B558";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンサペーシジャパン";
		$sellno_head="21";
		break;
	case 22:
//双日エアロスペース(地球環境基金)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="B75425760A780B1A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ドク）カンキヨウサイセイホゼンキコウチキユウカンキヨウキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 23:
//東北大震災義援金プログラム
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャカイフクシホウジンチュウオウキョウドウボキンカイボラサポ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 24:
//西友
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="153";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="十五号支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="69FFF2AEF4EF5A27";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ド）セイユウ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 25:
//日本経済新聞文化部
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="422";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新丸の内支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D02C27A1AD202545";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ブンカブ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 26:
//朝日新聞社論説委員室
		$_SESSION["HSMAP"]["BANK_CD"]="2580";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="002";
		$_SESSION["HSMAP"]["BANK_NM"]="朝日新聞信用組合";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="1D78F0CD9A604B59";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ロンセツドヨウカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 27:
//HFI（Hope and Faith International）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="131";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
//		$_SESSION["HSMAP"]["BRANCH_NM"]="二子玉川支店";
		$_SESSION["HSMAP"]["BRANCH_NM"]="玉川支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
//		$_SESSION["HSMAP"]["KOUZABANGOU"]="650C5D6D33CA5F90";
//		$_SESSION["HSMAP"]["KOUZAMEIGI"]="エイチエフデイジェイ　フクイ";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="78D4583A34376455";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ホープアンドフェイスインターナショナルダイヒョウフク";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 28:
//ACE（エース）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="779";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="上野支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="DB2B6045EF6BBE25";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンエース";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 29:
//マンパワー・ジャパン
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="084";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="青山通支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="7528B0B60BBD042B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="マンパワーグループカブシキカイシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 30:
//中日本ハイウェイ・パトロール東京
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="130";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3D7F68EA62CC4F32";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ナカニホンハイウェイ・パトロールトウキョウ（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 31:
//三菱商事
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="045";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="六本木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3B33A0AD804ADBD8";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンオサナイナンミンヲカンガエ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 32:
//地球の友と歩む会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="052";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="神楽坂支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="734F7A81D081BAA8";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン　チキュウノトモトアユムカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 33:
//損保ジャパン奈良支店
		$_SESSION["HSMAP"]["BANK_CD"]="";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="";
		$_SESSION["HSMAP"]["BANK_NM"]="";
		$_SESSION["HSMAP"]["BRANCH_NM"]="";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 34:
//日本興亜損害保険
		$_SESSION["HSMAP"]["BANK_CD"]="9900";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="019";
		$_SESSION["HSMAP"]["BANK_NM"]="ゆうちょ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="〇一九";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="73B51B4FEB417D4A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="アシナガイクエイカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 35:
//3keys（スリーキーズ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="171";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大塚支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="56FA06378FF96D00";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンスリーキーズ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 36:
//ライフ
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="614";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="浅草橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="812EB45209E1B07A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャライフコーポレーション";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 37:
//★スポット対応企業
		$_SESSION["HSMAP"]["BANK_CD"]="";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="";
		$_SESSION["HSMAP"]["BANK_NM"]="";
		$_SESSION["HSMAP"]["BRANCH_NM"]="";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 38:
//町田ゼルビア
		$_SESSION["HSMAP"]["BANK_CD"]="0138";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="931";
		$_SESSION["HSMAP"]["BANK_NM"]="横浜銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="町田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="808BFBD390B15B2F";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）ゼルビア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 39:
//SC相模原
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="259";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="6368D8AD8494EC45";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャスポーツクラブサガミハラ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 40:
//幼い難民を考える会（CYR）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="045";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="六本木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3B33A0AD804ADBD8";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンオサナイナンミンヲカンガエル";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 41:
//アフラック
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="353";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新宿西口支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="F634784AAB6CF90F";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="アメリカンファミリーライフアシュアランスカンパニーオブコロン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 42:
//JHP学校をつくる会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="045";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="六本木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="E1DC1B6053EB96EE";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン　ジエイエイチピーガツコウオ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 43:
//日立建機
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="321";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="42BB0115755DC69D";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ユタカナダイチ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 44:
//北海道森と緑の会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="637";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="札幌支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="64B41F24FB4BF373";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シヤダンホウジンホツカイドウモリトミドリノカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 45:
//ユニー
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="150";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="名古屋営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="C47CB9830BAB11CD";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ユニーカブシキカイシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 46:
//埼玉県自動車販売店協会（自販連埼玉支部）
		$_SESSION["HSMAP"]["BANK_CD"]="0017";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="500";
		$_SESSION["HSMAP"]["BANK_NM"]="埼玉りそな銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="さいたま営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="C5B5CD7D7F58D989";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）サンワコウコクシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 47:
//マルハン
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="333";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="日本橋中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="1D017CECEFC65919";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャマルハン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 48:
//アドビシステムズ
		$_SESSION["HSMAP"]["BANK_CD"]="9900";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="018";
		$_SESSION["HSMAP"]["BANK_NM"]="ゆうちょ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="〇一八";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="A348BC4770972959";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="アドビシステムズアクションコミッティー";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 49:
//FC在庫買取
		$_SESSION["HSMAP"]["BANK_CD"]="";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="";
		$_SESSION["HSMAP"]["BANK_NM"]="";
		$_SESSION["HSMAP"]["BRANCH_NM"]="";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 50:
//大塚商会(3keys)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="171";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大塚支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="56FA06378FF96D00";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンスリーキーズ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 51:
//森ビル都市企画
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="545";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="江戸川橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="10BA56ED01D1040B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ゼンコクガッコウトショカンキョウギカイ　ゲンキプロジェクト";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 52:
//アクセス
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="501";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="京都中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5F7331091C6280AD";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン アクセス キョウセイシャカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;

	case 53:
//ブリヂストンスポーツ)
		$_SESSION["HSMAP"]["BANK_CD"]="0016";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="112";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほコーポレート銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大手町営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="91A43D9A65C68E8E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ブリヂストンスポーツカブシキカイシヤ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;

	case 54:
//ケア・インターナショナルジャパン
		$_SESSION["HSMAP"]["BANK_CD"]="1341";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="164";
		$_SESSION["HSMAP"]["BANK_NM"]="西武信用金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="池袋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="0D0AFF160638328B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）ケア．インターナショナルジャパン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;

	case 55:
//世界の子どもにワクチンを(JCV)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="043";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="田町支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3E976664CB717FBC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンセカイノコドモニワクチンヲ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;

	case 56:
//共立メンテナンス
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="219";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="神田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="BA86C23412A5B03F";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャキョウリツメンテナンス";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;

	case 57:
//ジャパンハート
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="117";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="蒲田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="8119F78E9518597B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンジャパンハート";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 58:
//MSD
		$_SESSION["HSMAP"]["BANK_CD"]="9900";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="018";
		$_SESSION["HSMAP"]["BANK_NM"]="ゆうちょ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="〇一八";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3CE0C58B40F5A4FF";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ボランティアネットワーク";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 59:
//ブリッジ エーシア ジャパン(BAJ)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="329";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新宿新都心支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="8C97071C6B237569";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンブリッジエーシアジャパンリジ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 60:
//TBS
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="825";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="赤坂支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="ACE65F61E3B72A07";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ジェイエヌエヌジェイアールエヌキョウドウサイガイボキンジムキ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 61:
//TBS(フィリピン台風支援)
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="825";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="赤坂支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="051AF70A9C5E7CA5";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ジェイエヌエヌジェイアールエヌキョウドウサイガイボキン２０１";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 62:
//岡谷鋼機
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="150";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="名古屋営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="02704EB9EEAD2C28";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="オカヤコウキ（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 63:
//横浜開港祭
		$_SESSION["HSMAP"]["BANK_CD"]="0138";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="200";
		$_SESSION["HSMAP"]["BANK_NM"]="横浜銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D36DB763ED8CB309";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ヨコハマカイコウサイキョウギカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 64:
//商船三井
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="200";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="1960AB565207A6DA";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャショウセンミツイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 65:
//広栄化学工業
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="211";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="4361391C83B08A04";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="コウエイカガクコウギヨウ（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 66:
//ハグオール
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="379";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="E6BC55159FAFD4E5";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）ハグオール";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 67:
//NTT労働組合西日本
		$_SESSION["HSMAP"]["BANK_CD"]="2978";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="589";
		$_SESSION["HSMAP"]["BANK_NM"]="近畿労働金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="福島支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="7E3CDB0D200933BF";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="エヌティティロウドウクミアイニシニホンホンブ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 68:
//日本ラグビーフットボール協会
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="210";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="渋谷支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3706DB9BBBA1909C";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ジェイアールエフユーラグビーファミリーシエンキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 69:
//店舗在庫買取
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="379";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="51C6807DF7CA83A1";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ブックオフコーポレーション（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 70:
//神戸YMCA
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="410";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="三宮支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="F10CD3B4E7F215A1";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="コウベワイエムシーエー";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 71:
//北洋銀行
		$_SESSION["HSMAP"]["BANK_CD"]="0501";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="303";
		$_SESSION["HSMAP"]["BANK_NM"]="北洋銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="札幌南支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="74FFA49E65CD1C9A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャホッカイドウシブシブチョウイトウヨシロ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 72:
//なおちゃんを救う会
		$_SESSION["HSMAP"]["BANK_CD"]="0149";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="277";
		$_SESSION["HSMAP"]["BANK_NM"]="静岡銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="富士支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="2C780DF0FA2151ED";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ナオチヤンヲスクウカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 73:
//honto返品買取
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="207";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="市ヶ谷支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="E0D9A56318F1FDD5";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャトゥディファクト";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 74:
//シニアライフセラピー研究所
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="257";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="藤沢支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="0E9CBC27DECDA371";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンシニアライフセラピーケンキユ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 75:
//NTT労働組合　東日本本部
		$_SESSION["HSMAP"]["BANK_CD"]="2963";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="288";
		$_SESSION["HSMAP"]["BANK_NM"]="中央労働金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新宿支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D239B610BF0D4A59";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="エヌティティロウドウクミアイヒガシニホンホンブシッコウイイン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 76:
//プリック ジャパン ビューティー
		$_SESSION["HSMAP"]["BANK_CD"]="9900";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="028";
		$_SESSION["HSMAP"]["BANK_NM"]="ゆうちょ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="〇二八";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="C91E656F4E569A40";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="リビネット";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 77:
//損害保険ジャパン日本興亜（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 78:
//損害保険ジャパン日本興亜（熊本地震支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 79:
//ニッポン放送
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="4B90A6D3D9EEC2CD";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ラジオチヤリテイミユージツクソントウリヤンセキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 80:
//NHK渋谷
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="111";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="内幸町営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="056E7E24988F683D";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニッポンホウソウキョウカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 81:
//NHK名古屋
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="111";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="内幸町営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="056E7E24988F683D";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニッポンホウソウキョウカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 82:
//三井不動産レジデンシャル（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 83:
//三井不動産レジデンシャル（熊本地震支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 84:
//三菱地所コミュニティ（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 85:
//関西テレビ
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="005";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大阪営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5455DCD1C2AD799A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カンサイテレビホウソウ（カブ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 86:
//かわさき市民活動センター
		$_SESSION["HSMAP"]["BANK_CD"]="1283";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="003";
		$_SESSION["HSMAP"]["BANK_NM"]="川崎信用金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="武蔵小杉支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5517606C11C9BB36";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）カワサキシミンカツドウセンター";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 87:
//山田養蜂場
		$_SESSION["HSMAP"]["BANK_CD"]="9900";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="139";
		$_SESSION["HSMAP"]["BANK_NM"]="ゆうちょ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="一三九";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="8BE4551535EF277A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ヤマダヨウホウジョウチャリティーカツドウキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 88:
//アリさんマークの引越社
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="027";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大伝馬町支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="504988CE685F6D16";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）アリサンサービス";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 89:
//NTTデータ（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チユウオウキヨウドウボキンカイ、ボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 90:
//日本バルカー工業（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 91:
//ルネサスイーストン（シャンティ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="770";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="巣鴨支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="20E84DE5F3B254FC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャ）シャンティコクサイボランティア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 92:
//住友生命札幌支社（シャンティ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="770";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="巣鴨支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="20E84DE5F3B254FC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャ）シャンティコクサイボランティア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 93:
//三井住友カード（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 94:
//日経印刷（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チユウオウキヨウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 95:
//アトミクス
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="010";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="神田駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="A96B21A5901B8297";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="アトミクス（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 96:
//LIC
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="064";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="赤坂見附支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D19A53294627AB54";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）リック";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 97:
//JAIFA
		$_SESSION["HSMAP"]["BANK_CD"]="0134";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="111";
		$_SESSION["HSMAP"]["BANK_NM"]="千葉銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="幕張新都心支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="A128275374DDD3A5";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ジェイファチバケンキョウカイハートフルファンデーションダイヒ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 98:
//SOMPOシステムズ（移動図書館支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="770";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="巣鴨支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="20E84DE5F3B254FC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャ）シャンティコクサイボランティア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 99:
//日経印刷　長野（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チユウオウキヨウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 100:
//フロムヴイ
		$_SESSION["HSMAP"]["BANK_CD"]="0525";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="138";
		$_SESSION["HSMAP"]["BANK_NM"]="東日本銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="板橋駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="4F36C32A482D7395";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）ドリームヴイコショカイトリジギョウ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 101:
//愛知県職員組合(移動図書館支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="770";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="巣鴨支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="20E84DE5F3B254FC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャ）シャンティコクサイボランティア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 102:
//ベクトルフラックス
		$_SESSION["HSMAP"]["BANK_CD"]="0036";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="251";
		$_SESSION["HSMAP"]["BANK_NM"]="楽天銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="第一営業";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="7B19649A29F12449";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シヤ）ベクトルフラツクス キフキングチイチ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 103:
//バリューブックス
		$_SESSION["HSMAP"]["BANK_CD"]="0036";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="201";
		$_SESSION["HSMAP"]["BANK_NM"]="楽天銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="ジャズ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="1ECCB44B1E4A0BDA";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）バリューブックス";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 104:
//東京海上グループ『未来塾』（熊本地震支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 105:
//大阪商工信用金庫
		$_SESSION["HSMAP"]["BANK_CD"]="1636";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="大阪商工信用金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="0CE1538859200EC9";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シヨウコウサクラキキンウンエイイインチヨウヤマモト";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 106:
//三菱UFJニコス
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="228";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="町田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="C2F69BEDBB12D9DF";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ガク）ニッポンロウワガッコウコウチョウスズキミノル";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 107:
//アカツキ（BUY王）
		$_SESSION["HSMAP"]["BANK_CD"]="1341";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="164";
		$_SESSION["HSMAP"]["BANK_NM"]="西武信用金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="池袋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="08CF82B0AC40EF03";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）アカツキ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 108:
//アリアンツ火災海上保険（東日本大震災支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 109:
//損害保険ジャパン日本興亜　関西総務部（熊本地震支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 110:
//ブックマーケティング
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="370";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="川崎支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="46CB5531C322408A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）ブックマーケティング";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 111:
//正蓮寺
		$_SESSION["HSMAP"]["BANK_CD"]="0190";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="214";
		$_SESSION["HSMAP"]["BANK_NM"]="西日本シティ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="福間支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="143941AFF7F5533D";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シユウ）シヨウレンジダイヒヨウヤクインエンドウゲンシ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 112:
//オムロンフィールドエンジニアリング（熊本地震支援）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 113:
//LU（ブリッジ エーシア ジャパン）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="329";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新宿新都心支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="8C97071C6B237569";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ブリッジエーシアジャパンリジチョウアライシエツコ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 114:
//サスケコーポレーション
		$_SESSION["HSMAP"]["BANK_CD"]="1356";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="040";
		$_SESSION["HSMAP"]["BANK_NM"]="巣鴨信用金庫";
		$_SESSION["HSMAP"]["BRANCH_NM"]="中青木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="2FA7FE32B7ABBDB4";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）サスケコーポレーション";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 115:
//ABC(Room to Read)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="041";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="虎ノ門支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="863793BE3A450FEC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンルームトウリードジヤパン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 116:
//楽天
		$_SESSION["HSMAP"]["BANK_CD"]="0036";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="705";
		$_SESSION["HSMAP"]["BANK_NM"]="楽天銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="法人第五支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="69A3AF4491E90244";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ラクテン（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 117:
//神奈川県医療福祉施設協同組合
		$_SESSION["HSMAP"]["BANK_CD"]="0138";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="383";
		$_SESSION["HSMAP"]["BANK_NM"]="横浜銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="横浜駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="2467883872EEE0CA";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カナガワケンイリョウフクシシセツキョウドウクミアイリジチョウ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 118:
//日本通運【ボランティア】（かながわキンタロウ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="379";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="51C6807DF7CA83A1";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ブックオフコーポレーション（カ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 119:
//★スポット対応企業（東日本支援10％UP）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="フク）チュウオウキョウドウボキンカイボラサポニ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 120:
//★スポット対応企業（熊本支援10％UP）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 121:
//ジェーシービー(Room to Read)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="041";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="虎ノ門支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="863793BE3A450FEC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンルームトウリードジヤパン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 122:
//新宿南エネルギーサービス(地球環境基金)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="B75425760A780B1A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ドク）カンキヨウサイセイホゼンキコウチキユウカンキヨウキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 123:
//J-WAVE(地球環境基金)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="B75425760A780B1A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ドク）カンキヨウサイセイホゼンキコウチキユウカンキヨウキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 124:
//西日本高速道路サービス四国
		$_SESSION["HSMAP"]["BANK_CD"]="";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="";
		$_SESSION["HSMAP"]["BANK_NM"]="";
		$_SESSION["HSMAP"]["BRANCH_NM"]="";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
//
	default:
		$retvalue= "エラー";
		break;
}
?>