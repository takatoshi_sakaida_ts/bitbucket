<?PHP
require '../parts/pagechk.inc';
/**
プルダウンの選択数を減らすとjsファイルを修正する必要があります

if (isset($_SERVER["HTTP_REFERER"]))
{
	if (rtrim(substr($_SERVER["HTTP_REFERER"],-9,9))=="index.php")
	{	
		if (isset($_SESSION["HSMAP"])){
			unset($_SESSION["HSMAP"]);
		}
	}elseif (rtrim(substr($_SERVER["HTTP_REFERER"],-8,8))=="/bolweb/")
		{
			if (isset($_SESSION["HSMAP"])){
				unset($_SESSION["HSMAP"]);
			}
		}else
	{
		if (rtrim(substr($_SERVER["HTTP_REFERER"],-15,15))=="vol_confirm.php")
		{}else
		{
			print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
			print '<HTML><HEAD><TITLE>BOLWEB管理画面</TITLE>';
			print '<META http-equiv=Content-Type content="text/html; charset=euc-jp">';
			print '<META content="MSHTML 6.00.2900.2802" name=GENERATOR></HEAD>';
			die("不正なアクセスです<BR>" . $_SERVER["REMOTE_ADDR"]);
		}
	}
	}else
	{
			print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
			print '<HTML><HEAD><TITLE>BOLWEB管理画面</TITLE>';
			print '<META http-equiv=Content-Type content="text/html; charset=euc-jp">';
			print '<META content="MSHTML 6.00.2900.2802" name=GENERATOR></HEAD>';
			die("不正なアクセスです<BR>" . $_SERVER["REMOTE_ADDR"]);
}
*/
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="volkigyou_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>企業ボランティア受付入力画面</title>
<STYLE TYPE="text/css"> 


#menu1 { 
border-collapse: collapse; /** 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /** セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /** セルの背景色 */ 
padding: 3px; /** セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /** セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /** セルの背景色 */ 
padding: 3px; /** セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" action="volkigyou_confirm.php" name="volfm">
<b>企業名</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>企業名<font color=red>　*　</font></th>
    <td>
<?PHP
//団体を追加する時は、自動日付設定機能があるので注意が必要です。対象は、ファミリーマート・チャーティス(8)・ファー・イースト・ホールティングス(13)　です。
//今は、その団体は、使用していないのでコメントアウトしています
?>
    <select name="DANTAI" id="dantai" select style="font-size:15pt">
			<option value="0" select style="font-size:10pt">選択してください
			<option value="1" >Z7001　　NTT大阪支店
			<option value="2" >Z7002　　スターバックス
			<option value="3" >Z7003　　帝人
			<option value="4" >Z7004　　セガサミーホールディングス
			<option value="5" >Z7005　　NTTネオメイト
			<option value="6" >Z7006　　NEC(東日本大震災支援)
			<!--<option value="9" >Z7009　　結核予防会-->
			<option value="10" >Z7010　　ナック
			<!--<option value="14" >Z7014　　民際センター-->
			<option value="15" >Z7015　　ＤＮＰグループ労連
			<!--<option value="16" >Z7016　　しょうがっこうをおくる会-->
			<option value="17" >Z7017　　レオパレス21(企業ボランティア宅本便)
			<!--<option value="18" >Z7018　　神奈川大学-->
			<!--<option value="19" >Z7019　　ICAN（アイキャン）-->
			<option value="22" >Z7022　　双日エアロスペース(地球環境基金)
			<!--<option value="23" >Z7023　　売って支援プログラム（東日本大震災）-->
			<option value="25" >Z7025　　日本経済新聞文化部
			<option value="26" >Z7026　　朝日新聞社論説委員室
			<!--<option value="27" >Z7027　　HFI（Hope and Faith International）-->
			<!--<option value="28" >Z7028　　ACE（エース）-->
			<option value="29" >Z7029　　マンパワーグループ
			<option value="30" >Z7030　　中日本ハイウェイ・パトロール東京
			<option value="31" >Z7031　　三菱商事
			<!--<option value="32" >Z7032　　地球の友と歩む会-->
			<option value="34" >Z7034　　日本興亜損害保険
			<!--<option value="35" >Z7035　　3keys（スリーキーズ）-->
			<option value="37" >Z7037　　★スポット対応企業
			<!--<option value="38" >Z7038　　町田ゼルビア-->
			<!--<option value="39" >Z7039　　SC相模原-->
			<!--<option value="40" >Z7040　　幼い難民を考える会（CYR）-->
			<option value="41" >Z7041　　アフラック
			<!--<option value="42" >Z7042　　JHP学校をつくる会-->
			<option value="43" >Z7043　　日立建機
			<!--<option value="44" >Z7044　　北海道森と緑の会-->
			<option value="45" >Z7045　　ユニー
			<option value="46" >Z7046　　埼玉県自動車販売店協会（自販連埼玉支部）
			<option value="48" >Z7048　　アドビシステムズ
			<option value="49" >Z7049　　FC在庫買取
			<option value="50" >Z7050　　大塚商会(3keys)
			<option value="51" >Z7051　　森ビル都市企画
			<!--<option value="52" >Z7052　　アクセス-->
			<option value="53" >Z7053　　ブリヂストンスポーツ
			<!--<option value="54" >Z7054　　ケア・インターナショナルジャパン-->
			<!--<option value="55" >Z7055　　世界の子どもにワクチンを(JCV)-->
			<option value="56" >Z7056　　共立メンテナンス
			<!--<option value="57" >Z7057　　ジャパンハート-->
			<option value="58" >Z7058　　MSD
			<!--<option value="59" >Z7059　　ブリッジ エーシア ジャパン(BAJ)-->
			<option value="60" >Z7060　　TBS
			<!--<option value="61" >Z7061　　TBS(フィリピン台風支援)-->
			<option value="62" >Z7062　　岡谷鋼機
			<option value="63" >Z7063　　横浜開港祭
			<option value="64" >Z7064　　商船三井
			<option value="65" >Z7065　　広栄化学工業
			<option value="66" >Z7066　　ハグオール
			<option value="67" >Z7067　　NTT労働組合 西日本
			<option value="68" >Z7068　　日本ラグビーフットボール協会
			<option value="69" >Z7069　　店舗在庫買取
			<option value="70" >Z7070　　神戸YMCA
			<option value="71" >Z7071　　北洋銀行
			<option value="72" >Z7072　　なおちゃんを救う会
			<option value="73" >Z7073　　honto返品買取
			<option value="74" >Z7074　　シニアライフセラピー研究所
			<option value="75" >Z7075　　NTT労働組合　東日本本部
			<option value="76" >Z7076　　プリック ジャパン ビューティー
			<option value="77" >Z7077　　損害保険ジャパン日本興亜（東日本大震災支援）
			<option value="78" >Z7078　　損害保険ジャパン日本興亜（熊本地震支援）
			<option value="79" >Z7079　　ニッポン放送
			<option value="80" >Z7080　　NHK渋谷
			<option value="81" >Z7081　　NHK名古屋
			<option value="82" >Z7082　　三井不動産レジデンシャル（東日本大震災支援）
			<option value="83" >Z7083　　三井不動産レジデンシャル（熊本地震支援）
			<option value="84" >Z7084　　三菱地所コミュニティ（東日本大震災支援）
			<option value="85" >Z7085　　関西テレビ
			<option value="86" >Z7086　　かわさき市民活動センター
			<option value="87" >Z7087　　山田養蜂場
			<option value="88" >Z7088　　アリさんマークの引越社
			<option value="89" >Z7089　　NTTデータ（東日本大震災支援）
			<option value="90" >Z7090　　日本バルカー工業（東日本大震災支援）
			<option value="91" >Z7091　　ルネサスイーストン（シャンティ）
			<option value="92" >Z7092　　住友生命札幌支社（シャンティ）
			<option value="93" >Z7093　　三井住友カード（東日本大震災支援）
			<option value="94" >Z7094　　日経印刷（東日本大震災支援）
			<option value="95" >Z7095　　アトミクス
			<option value="96" >Z7096　　LIC
			<option value="97" >Z7097　　JAIFA
			<option value="98" >Z7098　　SOMPOシステムズ（移動図書館支援）
			<option value="99" >Z7099　　日経印刷　長野（東日本大震災支援）
			<option value="100" >Z7100　　フロムヴイ
			<!--<option value="101" >Z7101　　愛知県職員組合(移動図書館支援）-->
			<option value="102" > Z7102　　ベクトルフラックス
			<option value="103" > Z7103　　バリューブックス
			<option value="104" > Z7104　　東京海上グループ『未来塾』（熊本地震支援）
			<option value="105" > Z7105　　大阪商工信用金庫
			<option value="106" > Z7106　　三菱UFJニコス
			<option value="107" > Z7107　　アカツキ（BUY王）
			<option value="108" > Z7108　　アリアンツ火災海上保険（東日本大震災支援）
			<option value="109" > Z7109　　損害保険ジャパン日本興亜　関西総務部（熊本地震支援）
			<option value="110" > Z7110　　ブックマーケティング
			<option value="111" > Z7111　　正蓮寺
			<option value="112" > Z7112　　オムロンフィールドエンジニアリング（熊本地震支援）
			<option value="113" > Z7113　　LU（ブリッジ エーシア ジャパン）
			<option value="114" > Z7114　　サスケコーポレーション
			<option value="115" > Z7115　　ABC(Room to Read)
			<option value="116" > Z7116　　楽天
			<option value="117" > Z7117　　神奈川県医療福祉施設協同組合
			<option value="118" > Z7118　　日本通運【ボランティア】（かながわキンタロウ）
			<option value="119" > Z7119　　★スポット対応企業（東日本支援10％UP）
			<option value="120" > Z7120　　★スポット対応企業（熊本支援10％UP）
			<option value="121" > Z7121　　ジェーシービー(Room to Read)
			<option value="122" > Z7122　　新宿南エネルギーサービス(地球環境基金)
			<option value="123" > Z7123　　J-WAVE(地球環境基金)
			<option value="124" > Z7124　　西日本高速道路サービス四国
		</select>
    </td>
  </tr>
</TABLE>
<br>
<b>集荷先情報</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>名前</th>
    <td>
		<input type="text" name="PICKUP_NM_FST" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_FST"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
}
print '" >';
?>
		<input type="text" name="PICKUP_NM_MID" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_MID"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
}
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>フリガナ<font color=red>　*　</font></th>
    <td>
		<input type="text" name="PICKUP_NM_LAST" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_LAST"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_LAST"];
}
print '" >';
?>
		<input type="text" name="PICKUP_NM_ETC" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_ETC"])){
print $_SESSION["HSMAP"]["PICKUP_NM_ETC"];
}
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>郵便番号<font color=red>　*　</font></th>
    <td>
	例：1540004<br>
	<input type="text" name="PICKUP_ZIP_CD" size="8" maxlength="7" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_CD"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_CD"];
}
print '" >';
?>
<input type="button" name="search_address" value="住所検索" onClick="OpenPostWin()">
    </td>
  </tr>

  <tr>
    <th>都道府県<font color=red>　*　</font></th>
    <td>
	<select name="PICKUP_ZIP_ADDR1">
		<option value="0" >選択してください
		<option value="1" >北海道
		<option value="2" >青森県
		<option value="3" >岩手県
		<option value="4" >秋田県
		<option value="5" >宮城県
		<option value="6" >山形県
		<option value="7" >福島県
		<option value="8" >茨城県
		<option value="9" >栃木県
		<option value="10" >群馬県
		<option value="11" >埼玉県
		<option value="12" >千葉県
		<option value="13" >東京都
		<option value="14" >神奈川県
		<option value="15" >新潟県
		<option value="16" >富山県
		<option value="17" >石川県
		<option value="18" >福井県
		<option value="19" >山梨県
		<option value="20" >長野県
		<option value="21" >岐阜県
		<option value="22" >静岡県
		<option value="23" >愛知県
		<option value="24" >三重県
		<option value="25" >滋賀県
		<option value="26" >京都府
		<option value="27" >大阪府
		<option value="28" >兵庫県
		<option value="29" >奈良県
		<option value="30" >和歌山県
		<option value="31" >鳥取県
		<option value="32" >島根県
		<option value="33" >岡山県
		<option value="34" >広島県
		<option value="35" >山口県
		<option value="36" >徳島県
		<option value="37" >香川県
		<option value="38" >愛媛県
		<option value="39" >高知県
		<option value="40" >福岡県
		<option value="41" >佐賀県
		<option value="42" >長崎県
		<option value="43" >熊本県
		<option value="44" >大分県
		<option value="45" >宮崎県
		<option value="46" >鹿児島県
		<option value="47" >沖縄県
	</select>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村<font color=red>　*　</font></th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR2" size="100" maxlength="12" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"];
}
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR3" size="100" maxlength="16" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"];
}
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR4" size="100" maxlength="16" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"];
}
print '" >';
?>
    </td>
  </tr>

  <tr>
    <th>電話番号<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_TEL_NO" size="20" maxlength="13" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_TEL_NO"])){
	print $_SESSION["HSMAP"]["PICKUP_TEL_NO"];
}
print '"> 	例： 042-786-1235<BR>';
?>
	</td>
  </tr>

  </tr>
  <tr>
    <th>性別<font color=red>　*　</font></th>
    <td>
			<input name="MEM_SEX" type="radio" value="1">男
			<input name="MEM_SEX" type="radio" value="2">女
    </td>
  </tr>
</table>
<BR>
<b>集荷希望日など</b><br>（ファミリーマート、チャーティス・ファー・イースト・ホールティングス、ハグオール、LIC、JAIFA、バリューブックス<br>アカツキ（BUY王）、ブックマーケティング、LU（ブリッジ エーシア ジャパン）は箱数のみ入力してください）
<TABLE ID="menu1" width="900">
  <tr>
    <th>箱数<font color=red>　*　</font></th>
    <td>
	<input type="text" name="BOX" size="13" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["BOX"])){
	print $_SESSION["HSMAP"]["BOX"];
}
print '" >';
?>
	</td>
  </tr>
  <tr>
    <th>集荷希望年月日<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_REQ_DTY" size="6" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTY"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTY"];
}
else{
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_yesterday = $date_today + (86400*4);
print date('Y', $date_yesterday);
}
print '" >年';
?>
	<input type="text" name="PICKUP_REQ_DTM" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTM"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTM"];
}
else{
print date('m', $date_yesterday);
}
print '" >月';
?>
	<input type="text" name="PICKUP_REQ_DTD" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTD"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTD"];
}

print '" >日';
?>
    </td>
  </tr>
  <tr>
    <th>集荷希望時間帯<font color=red>　*　</font></th>
    <td>
	<select name="PICKUP_REQ_TIME">
		<option value="0" >選択してください
		<option value="1" > 9:00-13:00
		<option value="2" >13:00-15:00
		<option value="4" >15:00-18:00
		<option value="5" >18:00-20:00
	</select>
　　　<!--【注意】<font color=red>当日集荷</font>の場合「18-21時」のみ選択可。15時過ぎたら翌日以降で。-->
    </td>
  </tr>
  <tr style="display:none;">
    <th>ガイド発送<font color=red>　*　</font></th>
    <td>
			<input name="gaido" type="radio" value="1">必要
			<input name="gaido" type="radio" value="2" checked="checked">不要
    </td>
  </tr>
</table>
<BR>
<b>メモ</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>メモ</th>
    <td>
	<input type="text" name="memo" size="100" maxlength="200" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEMO"])){
	print $_SESSION["HSMAP"]["MEMO"];
}
print '" >';
?>
	</td>
  </tr>


<input type="hidden" name="DANTAI_index" value="
<?PHP

if(isset($_SESSION["HSMAP"]["DANTAI_index"])){
	print $_SESSION["HSMAP"]["DANTAI_index"];
}

print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
}
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
}
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEM_SEX"])){
	print ($_SESSION["HSMAP"]["MEM_SEX"]);
}
print '" >';
?>
<input type="hidden" name="gaido_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["gaido"])){
	print ($_SESSION["HSMAP"]["gaido"]);
}
print '" >';
?>
<input type="hidden" name="postname" value="">

</TABLE>
<BR>
<b>入力確認</b>
<table id="menu1">
	<tr>
		<th>入力確認 Z</th>
		<td>
			<input type="text" id="confMenu" name="confMenu" size="8" maxlength="4" style="ime-mode:active;" value="">
		</td>
	</tr>
</table>
<br>
<script type="text/javascript">
document.volfm.DANTAI.selectedIndex=document.volfm.DANTAI_index.value;
document.volfm.PICKUP_REQ_TIME.selectedIndex=document.volfm.PICKUP_REQ_TIME_index.value;
document.volfm.PICKUP_ZIP_ADDR1.selectedIndex=document.volfm.PICKUP_ZIP_ADDR1_index.value;
var num;
num=(document.volfm.MEM_SEX_index.value - 1);
if(num==-1){
	}else{
	document.volfm.MEM_SEX[(document.volfm.MEM_SEX_index.value-1)].checked=true;
}
num=(document.volfm.gaido_index.value - 1);
if(num==-1){
	}else{
	document.volfm.gaido[(document.volfm.gaido_index.value-1)].checked=true;
}
</script>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="EntCan()">
<input type="BUTTON" value="　　登録　　" onClick="EntCheck()">
</FORM>
</body>
</html>