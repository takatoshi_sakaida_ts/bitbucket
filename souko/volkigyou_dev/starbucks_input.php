<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="volkigyou_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>スターバックス専用受付入力画面</title>
<STYLE TYPE="text/css"> 

#menu1 { 
border-collapse: collapse; /** 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /** セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /** セルの背景色 */ 
padding: 3px; /** セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /** セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /** セルの背景色 */ 
padding: 3px; /** セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" action="starbucks_confirm.php" name="volfm">
<b>スターバックス専用受付入力画面</b><br>

<TABLE ID="menu1" width="900">
  <tr>
    <th>店舗番号</th>
    <td>
		<input type="text" name="TENPO_NO" size="13" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["TENPO_NO"])){
	print $_SESSION["HSMAP"]["TENPO_NO"];
}
print '" >';
?>
    </td>
  </TR>
  <tr>
    <th>箱数<font color=red>　*　</font></th>
    <td>
	<input type="text" name="BOX" size="13" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["BOX"])){
	print $_SESSION["HSMAP"]["BOX"];
}
print '" >';
?>
	</td>
  </tr>
  <tr>
    <th>集荷希望年月日<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_REQ_DTY" size="6" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTY"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTY"];
}
else{
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_yesterday = $date_today + (86400*4);
print date('Y', $date_yesterday);
}
print '" >年';
?>
	<input type="text" name="PICKUP_REQ_DTM" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTM"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTM"];
}
else{
print date('m', $date_yesterday);
}
print '" >月';
?>
	<input type="text" name="PICKUP_REQ_DTD" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTD"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTD"];
}

print '" >日';
?>
    </td>
  </tr>
  <tr>
    <th>集荷希望時間帯<font color=red>　*　</font></th>
    <td>
	<select name="PICKUP_REQ_TIME">
		<option value="0" >選択してください
		<option value="1" > 9:00-13:00
		<option value="2" >13:00-15:00
		<option value="4" >15:00-18:00
		<option value="5" >18:00-20:00
	</select>
　　　<!--【注意】<font color=red>当日集荷</font>の場合「18-21時」のみ選択可。15時過ぎたら翌日以降で。-->
    </td>
  </tr>
</table>

<input type="hidden" name="DANTAI_index" value="
<?PHP

if(isset($_SESSION["HSMAP"]["DANTAI_index"])){
	print $_SESSION["HSMAP"]["DANTAI_index"];
}

print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
}
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
}
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEM_SEX"])){
	print ($_SESSION["HSMAP"]["MEM_SEX"]);
}
print '" >';
?>
<input type="hidden" name="gaido_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["gaido"])){
	print ($_SESSION["HSMAP"]["gaido"]);
}
print '" >';
?>
<input type="hidden" name="postname" value="">

<BR>
<script type="text/javascript">
document.volfm.DANTAI.selectedIndex=document.volfm.DANTAI_index.value;
document.volfm.PICKUP_REQ_TIME.selectedIndex=document.volfm.PICKUP_REQ_TIME_index.value;
document.volfm.PICKUP_ZIP_ADDR1.selectedIndex=document.volfm.PICKUP_ZIP_ADDR1_index.value;
var num;
num=(document.volfm.MEM_SEX_index.value - 1);
if(num==-1){
	}else{
	document.volfm.MEM_SEX[(document.volfm.MEM_SEX_index.value-1)].checked=true;
}
num=(document.volfm.gaido_index.value - 1);
if(num==-1){
	}else{
	document.volfm.gaido[(document.volfm.gaido_index.value-1)].checked=true;
}
</script>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="EntCan()">
<input type="BUTTON" value="　　登録　　" onClick="SBEntCheck()">
</FORM>
</body>
</html>