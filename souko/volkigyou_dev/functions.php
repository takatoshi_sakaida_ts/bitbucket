<?php

/* DBファイルは呼び出し元で読み込んでおく */

/**
 * ファイルポインタから行を取得し、CSVフィールドを処理する
 * @param resource handle
 * @param int length
 * @param string delimiter
 * @param string enclosure
 * @return ファイルの終端に達した場合を含み、エラー時にFALSEを返します。
 */
function fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
    $d = preg_quote($d);
    $e = preg_quote($e);
    $_line = "";
    $eof = false;
    while (($eof != true)and(!feof($handle))) {
        $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
        $itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
        if ($itemcnt % 2 == 0) $eof = true;
    }
    $_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
    $_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
    preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
    $_csv_data = $_csv_matches[1];
    for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
        $_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
        $_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
    }
    return empty($_line) ? false : $_csv_data;
}



function statusToStr($statusCode){
    $status = array(
        '01'=>'受付',
        '02'=>'計算中',
        '03'=>'承認待ち',
        '04'=>'本人確認待ち',
        '05'=>'振り込み準備中',
        '06'=>'完了',
        '07'=>'承認保留',
        '08'=>'受付保留',
        '99'=>'取り消し'
    );
    return $status[$statusCode];
}

function getRowCount(&$conn){
	
	$sql = "select count(*) from tsell where sell_no like 'Z7069%' ";
	
	$res = $conn->query($sql);
	if(DB::isError($res)){
		return 0;
	}
	else{
		$row = $res->fetchRow();
		return $row[0];
	}
}

function getPageCount($rowCount, $limit){
	$count = $rowCount / $limit;
	return ceil($count); // 切り上げ
}
/*
function getSql($pageNum, $limit){
    
    $min = ($pageNum -1) * $limit + 1;
    $max = $pageNum * $limit;

    // sql生成
    $sql = <<< EOM
        select * from 
        ( 
            select 
                rownum as line,
                s.SELL_NO as SELL_NO,
                s.PICKUP_NM_FST as PICKUP_NM_FST,
                s.PICKUP_NM_MID as PICKUP_NM_MID,
                s.SELL_FORM_GET_DT as SELL_FORM_GET_DT,
                s.SELL_STAT as SELL_STAT,
                a.TOT_ASS_AMT as TOT_ASS_AMT
            from tsell s, tsellassessment a
            where s.SELL_NO = a.SELL_NO
            and s.SELL_NO like 'Z7069%'
            order by sell_no desc
        ) t
        where t.line >= {$min} and t.line <= {$max} 
EOM;

	return $sql;
}
*/
/*
function getSql(){
    // sql生成
    $sql = <<< EOM
        select 
            rownum as line,
            s.SELL_NO as SELL_NO,
            s.PICKUP_NM_FST as PICKUP_NM_FST,
            s.PICKUP_NM_MID as PICKUP_NM_MID,
            s.SELL_FORM_GET_DT as SELL_FORM_GET_DT,
            s.SELL_STAT as SELL_STAT,
            a.TOT_ASS_AMT as TOT_ASS_AMT
        from tsell s, tsellassessment a
        where s.SELL_NO = a.SELL_NO
        and s.SELL_NO like 'Z7069%'
        order by sell_no desc
EOM;

    return $sql;
}
*/

function getSql(){
    // sql生成
    $sql = <<< EOM
select 
    rownum as line,
    s.SELL_NO as SELL_NO,
    s.PICKUP_NM_FST as PICKUP_NM_FST,
    s.PICKUP_NM_MID as PICKUP_NM_MID,
    s.SELL_FORM_GET_DT as SELL_FORM_GET_DT,
    s.SELL_STAT as SELL_STAT,
    a.TOT_ASS_AMT as TOT_ASS_AMT,
    a.ASS_DT as ASS_DT,
    s.TRAN_DT as TRAN_DT,
    a.BOX_ARRIVED as BOX,
    a.VALID_ITEM_CNT as VALID_ITEM_CNT,
    a.INVALID_ITEM_CNT as INVALID_ITEM_CNT
from tsell s, tsellassessment a
where s.SELL_NO = a.SELL_NO
and s.SELL_NO like 'Z7069%'
order by sell_no desc
EOM;

    return $sql;
}

function getStoreList($key_index=2, $val_index=0){
    
    $list = array();

    $fp = fopen("【BOL買取】店舗リスト.csv","r");
//    $buf = mb_convert_encoding(file_get_contents("【BOL買取】店舗リスト.csv"),"utf-8","sjis, sjis-win");
//    $fp = tmpfile();
//   fwrite($fp,$buf);
//    rewind($fp);

        // PHP5の仕様?により、csvの2バイト文字が一部化けてしまうので有志のcsv関数を代用
        while($data = fgetcsv_reg($fp,1024) ){
            $list[$data[$key_index]] = $data[$val_index];
        }
    fclose( $fp );

    return $list;
}




?>