<?PHP
function Rethiduke($index)
{
//日付整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2);
}
	return $retvalue;
}
function Rethidukejikan($index)
{
//日付時間整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2). " ".substr($index,8,2). ":".substr($index,10,2). ":" .substr($index,12,2);
}
	return $retvalue;
}
function Retzip($index)
{
//郵便番号整形
if (empty($index)){
	$retvalue='〒';
}else{
	$retvalue = "〒".substr($index,0,3). "-" .substr($index,3,4);
}
	return $retvalue;
}
function Rettel($index)
{
//電話番号整形
if (empty($index)){
	$retvalue='　';
}else{
	switch (substr($index,1,1)){
		case 3:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		case 6:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		default:
			if (strlen($index)==10){
				$retvalue = substr($index,0,3). "-" .substr($index,3,3). "-" .substr($index,6,4);
			}else{
				$retvalue = substr($index,0,3). "-" .substr($index,3,4). "-" .substr($index,7,4);
			}
			break;
	}
}
	return $retvalue;
}
function RetPrefecture($index)
{
	switch ($index){
		case 0;
			$retvalue = "選択してください";
			break;
		case 1;
			$retvalue = "北海道";
			break;
		case 2;
			$retvalue = "青森県";
			break;
		case 3;
			$retvalue = "岩手県";
			break;
		case 4;
			$retvalue = "秋田県";
			break;
		case 5;
			$retvalue = "宮城県";
			break;
		case 6;
			$retvalue = "山形県";
			break;
		case 7;
			$retvalue = "福島県";
			break;
		case 8;
			$retvalue = "茨城県";
			break;
		case 9;
			$retvalue = "栃木県";
			break;
		case 10;
			$retvalue = "群馬県";
			break;
		case 11;
			$retvalue = "埼玉県";
			break;
		case 12;
			$retvalue = "千葉県";
			break;
		case 13;
			$retvalue = "東京都";
			break;
		case 14;
			$retvalue = "神奈川県";
			break;
		case 15;
			$retvalue = "新潟県";
			break;
		case 16;
			$retvalue = "富山県";
			break;
		case 17;
			$retvalue = "石川県";
			break;
		case 18;
			$retvalue = "福井県";
			break;
		case 19;
			$retvalue = "山梨県";
			break;
		case 20;
			$retvalue = "長野県";
			break;
		case 21;
			$retvalue = "岐阜県";
			break;
		case 22;
			$retvalue = "静岡県";
			break;
		case 23;
			$retvalue = "愛知県";
			break;
		case 24;
			$retvalue = "三重県";
			break;
		case 25;
			$retvalue = "滋賀県";
			break;
		case 26;
			$retvalue = "京都府";
			break;
		case 27;
			$retvalue = "大阪府";
			break;
		case 28;
			$retvalue = "兵庫県";
			break;
		case 29;
			$retvalue = "奈良県";
			break;
		case 30;
			$retvalue = "和歌山県";
			break;
		case 31;
			$retvalue = "鳥取県";
			break;
		case 32;
			$retvalue = "島根県";
			break;
		case 33;
			$retvalue = "岡山県";
			break;
		case 34;
			$retvalue = "広島県";
			break;
		case 35;
			$retvalue = "山口県";
			break;
		case 36;
			$retvalue = "徳島県";
			break;
		case 37;
			$retvalue = "香川県";
			break;
		case 38;
			$retvalue = "愛媛県";
			break;
		case 39;
			$retvalue = "高知県";
			break;
		case 40;
			$retvalue = "福岡県";
			break;
		case 41;
			$retvalue = "佐賀県";
			break;
		case 42;
			$retvalue = "長崎県";
			break;
		case 43;
			$retvalue = "熊本県";
			break;
		case 44;
			$retvalue = "大分県";
			break;
		case 45;
			$retvalue = "宮崎県";
			break;
		case 46;
			$retvalue = "鹿児島県";
			break;
		case 47;
			$retvalue = "沖縄県";
			break;
	}
			return $retvalue;
}
function Retseibetu($index)
{
	switch ($index){
		case 1;
			$retvalue = "男";
			break;
		case 2;
			$retvalue = "女";
			break;
	}
			return $retvalue;
}
function Rettuuti($index)
{
	switch ($index){
		case 0;
			$retvalue = "通知不要（自動承認）";
			break;
		case 1;
			$retvalue = "通知必要（承認後振込）";
			break;
	}
			return $retvalue;
}
function Retdtaiou($index)
{
	switch ($index){
		case 0;
			$retvalue = "センター引取";
			break;
		case 1;
			$retvalue = "返却希望";
			break;
	}
			return $retvalue;
}
function Rethonnin($index)
{
	switch ($index){
		case 0;
			$retvalue = "有り";
			break;
		case 1;
			$retvalue = "無し";
			break;
	}
			return $retvalue;
}
function Retsaimousi($index)
{
	switch ($index){
		case 0;
			$retvalue = "希望";
			break;
		case 1;
			$retvalue = "希望しない";
			break;
	}
			return $retvalue;
}
function Retshokugyou($index)
{
	switch ($index){
		case 1;
			$retvalue = "会社員";
			break;
		case 2;
			$retvalue = "公務員";
			break;
		case 3;
			$retvalue = "主婦";
			break;
		case 4;
			$retvalue = "自営業";
			break;
		case 5;
			$retvalue = "学生";
			break;
		case 6;
			$retvalue = "パート";
			break;
		case 7;
			$retvalue = "フリーター";
			break;
		case 8;
			$retvalue = "無職";
			break;
		case 9;
			$retvalue = "その他";
			break;
	}
			return $retvalue;
}
?>