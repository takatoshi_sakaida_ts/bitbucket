<?PHP
require '../parts/pagechk.inc';
/*
if (isset($_SERVER["HTTP_REFERER"]))
{
	if (rtrim(substr($_SERVER["HTTP_REFERER"],-9,9))=="index.php")
	{	
		if (isset($_SESSION["HSMAP"])){
			unset($_SESSION["HSMAP"]);
		}
	}elseif (rtrim(substr($_SERVER["HTTP_REFERER"],-8,8))=="/bolweb/")
		{
			if (isset($_SESSION["HSMAP"])){
				unset($_SESSION["HSMAP"]);
			}
		}else
	{
		if (rtrim(substr($_SERVER["HTTP_REFERER"],-22,22))=="oldtakuhon_confirm.php")
		{}else
		{
			print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
			print '<HTML><HEAD><TITLE>BOLWEB管理画面</TITLE>';
			print '<META http-equiv=Content-Type content="text/html; charset=euc-jp">';
			print '<META content="MSHTML 6.00.2900.2802" name=GENERATOR></HEAD>';
			die("不正なアクセスです<BR>" . $_SERVER["REMOTE_ADDR"]);
		}
}
}else
{
			print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
			print '<HTML><HEAD><TITLE>BOLWEB管理画面</TITLE>';
			print '<META http-equiv=Content-Type content="text/html; charset=euc-jp">';
			print '<META content="MSHTML 6.00.2900.2802" name=GENERATOR></HEAD>';
			die("不正なアクセスです<BR>" . $_SERVER["REMOTE_ADDR"]);
}
*/
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="oldtakuhon_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>旧受付宅本便　新システム入力画面</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #ffff99; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}</style>
</head>
<body>
<form method="POST" action="oldtakuhon_confirm.php" name="volfm">
<b>旧受付宅本便　新システム入力画面</b><BR>
<TABLE ID="menu1" width="900">
  <tr>
    <th>名前</th>
    <td>
		<input type="text" name="PICKUP_NM_FST" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_FST"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
}
print '" >';
?>
		<input type="text" name="PICKUP_NM_MID" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_MID"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
}
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>フリガナ<font color=red>　*　</font></th>
    <td>
		<input type="text" name="PICKUP_NM_LAST" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_LAST"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_LAST"];
}
print '" >';
?>
		<input type="text" name="PICKUP_NM_ETC" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_ETC"])){
print $_SESSION["HSMAP"]["PICKUP_NM_ETC"];
}
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>郵便番号<font color=red>　*　</font></th>
    <td>
	例：1540004<br>
	<input type="text" name="PICKUP_ZIP_CD" size="8" maxlength="7" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_CD"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_CD"];
}
print '" >';
?>
<input type="button" name="search_address" value="住所検索" onClick="OpenPostWin()">
    </td>
  </tr>

  <tr>
    <th>都道府県<font color=red>　*　</font></th>
    <td>
	<select name="PICKUP_ZIP_ADDR1">
		<option value="0" >選択してください
		<option value="1" >北海道
		<option value="2" >青森県
		<option value="3" >岩手県
		<option value="4" >秋田県
		<option value="5" >宮城県
		<option value="6" >山形県
		<option value="7" >福島県
		<option value="8" >茨城県
		<option value="9" >栃木県
		<option value="10" >群馬県
		<option value="11" >埼玉県
		<option value="12" >千葉県
		<option value="13" >東京都
		<option value="14" >神奈川県
		<option value="15" >新潟県
		<option value="16" >富山県
		<option value="17" >石川県
		<option value="18" >福井県
		<option value="19" >山梨県
		<option value="20" >長野県
		<option value="21" >岐阜県
		<option value="22" >静岡県
		<option value="23" >愛知県
		<option value="24" >三重県
		<option value="25" >滋賀県
		<option value="26" >京都府
		<option value="27" >大阪府
		<option value="28" >兵庫県
		<option value="29" >奈良県
		<option value="30" >和歌山県
		<option value="31" >鳥取県
		<option value="32" >島根県
		<option value="33" >岡山県
		<option value="34" >広島県
		<option value="35" >山口県
		<option value="36" >徳島県
		<option value="37" >香川県
		<option value="38" >愛媛県
		<option value="39" >高知県
		<option value="40" >福岡県
		<option value="41" >佐賀県
		<option value="42" >長崎県
		<option value="43" >熊本県
		<option value="44" >大分県
		<option value="45" >宮崎県
		<option value="46" >鹿児島県
		<option value="47" >沖縄県
	</select>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村<font color=red>　*　</font></th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR2" size="100" maxlength="16" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"];
}
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR3" size="100" maxlength="12" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"];
}
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR4" size="100" maxlength="12" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"];
}
print '" >';
?>
    </td>
  </tr>

  <tr>
    <th>電話番号<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_TEL_NO" size="20" maxlength="13" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_TEL_NO"])){
	print $_SESSION["HSMAP"]["PICKUP_TEL_NO"];
}
print '"> 	例： 042-786-1235<BR>';
?>
	</td>
  </tr>

  </tr>

  <tr>
    <th>性別<font color=red>　*　</font></th>
    <td>
			<input name="MEM_SEX" type="radio" value="1">男
			<input name="MEM_SEX" type="radio" value="2">女
    </td>
  </tr>
  <tr>
    <th>買取承認<font color=red>　*　</font></th>
    <td>
			<input name="TUUTI" type="radio" value="1">通知不要（自動承認）
			<input name="TUUTI" type="radio" value="2">通知必要（承認後振込）
    </td>
  </tr>
  <tr>
    <th>お値段のつかない商品<font color=red>　*　</font></th>
    <td>
			<input name="DTAIOU" type="radio" value="1">センター引取
			<input name="DTAIOU" type="radio" value="2">返却希望
    </td>
  </tr>
  <tr>
    <th>旧受付番号</th>
    <td>
	<input type="text" name="OLDNO" size="20" maxlength="13" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["OLDNO"])){
	print $_SESSION["HSMAP"]["OLDNO"];
}
print '">';
?>
	</td>
  </tr>
  <tr>
    <th>申込箱数<font color=red>　*　</font></th>
    <td>
	<input type="text" name="MOUSIKOMIBOX" size="13" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MOUSIKOMIBOX"])){
	print $_SESSION["HSMAP"]["MOUSIKOMIBOX"];
}
print '" >';
?>
	</td>
  </tr>
  <tr>
    <th>到着箱数<font color=red>　*　</font></th>
    <td>
	<input type="text" name="BOX" size="13" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["BOX"])){
	print $_SESSION["HSMAP"]["BOX"];
}
print '" >';
?>
	</td>
  </tr>

  <tr>
    <th>本人確認書類<font color=red>　*　</font></th>
    <td>
			<input name="HONNIN" type="radio" value="1">有り
			<input name="HONNIN" type="radio" value="2">無し
    </td>
  </tr>
  <tr>
    <th>再申込書発送<font color=red>　*　</font></th>
    <td>
			<input name="SAIMOUSI" type="radio" value="1">希望
			<input name="SAIMOUSI" type="radio" value="2">希望しない
    </td>
  </tr>
  <tr>
    <th>保管NO</th>
    <td>
	<input type="text" name="HOKAN" size="13" maxlength="10" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HOKAN"])){
	print $_SESSION["HSMAP"]["HOKAN"];
}
print '" >';
?>
	</td>
  </tr>
</table>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
}
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEM_SEX"])){
	print ($_SESSION["HSMAP"]["MEM_SEX"]);
}
print '" >';
?>
<input type="hidden" name="TUUTI_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["TUUTI"])){
	print ($_SESSION["HSMAP"]["TUUTI"]);
}
print '" >';
?>
<input type="hidden" name="DTAIOU_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["DTAIOU"])){
	print ($_SESSION["HSMAP"]["DTAIOU"]);
}
print '" >';
?>
<input type="hidden" name="HONNIN_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["HONNIN"])){
	print ($_SESSION["HSMAP"]["HONNIN"]);
}
print '" >';
?><input type="hidden" name="SAIMOUSI_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["SAIMOUSI"])){
	print ($_SESSION["HSMAP"]["SAIMOUSI"]);
}
print '" >';
?>
<input type="hidden" name="postname" value="">

</TABLE>
<BR>
<script type="text/javascript">
document.volfm.PICKUP_ZIP_ADDR1.selectedIndex=document.volfm.PICKUP_ZIP_ADDR1_index.value;
var num;
num=(document.volfm.MEM_SEX_index.value - 1);
if(num==-1){
	}else{
	document.volfm.MEM_SEX[(document.volfm.MEM_SEX_index.value-1)].checked=true;
}
num=(document.volfm.TUUTI_index.value - 1);
if(num==-1){
	}else{
	document.volfm.TUUTI[(document.volfm.TUUTI_index.value-1)].checked=true;
}
num=(document.volfm.DTAIOU_index.value - 1);
if(num==-1){
	}else{
	document.volfm.DTAIOU[(document.volfm.DTAIOU_index.value-1)].checked=true;
}
num=(document.volfm.HONNIN_index.value - 1);
if(num==-1){
	}else{
	document.volfm.HONNIN[(document.volfm.HONNIN_index.value-1)].checked=true;
}
num=(document.volfm.SAIMOUSI_index.value - 1);
if(num==-1){
	}else{
	document.volfm.SAIMOUSI[(document.volfm.SAIMOUSI_index.value-1)].checked=true;
}
</script>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="EntCan()">
<input type="BUTTON" value="　　登録　　" onClick="EntCheck()">
</FORM>
</body>
</html>