<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$nowtime= date("H");
$today = date("YmdHis");
$todayfile = date("Ymd");
$dlfile_name = "./dlfile/pickshuukei_staff_" . $todayfile . ".csv";
//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres."\r\n";
	fputs($fp,$str);
	fclose( $fp );
}

$sql = "SELECT R.OPERATESTAFFCODE,F.STAFFNAME,R.PICKTIME,R.PICKMINUTE,COUNT(*),R.ZONECODE ".
"FROM ( ".
"SELECT ".
"D.OPERATESTAFFCODE,SUBSTR(D.OPERATETIME,1,2) AS PICKTIME, ".
"CASE WHEN SUBSTR(D.OPERATETIME,3,2)<31 THEN 0 ".
"ELSE 1  ".
"END AS PICKMINUTE,M.ZONECODE ".
"FROM D_RECEPTION_GOODS D,M_LOCATION M ".
"WHERE D.PICKED_KIND='1' AND D.CANCEL_KIND='0' AND D.LOCATIONCODE=M.LOCATIONCODE ".
"AND D.OPERATEDATE=(SELECT TO_CHAR(SYSDATE-1,'YYYYMMDD') FROM DUAL) ".
") R,M_STAFF F WHERE R.OPERATESTAFFCODE=F.STAFFCODE ".
"GROUP BY R.PICKTIME,R.PICKMINUTE,R.ZONECODE,R.OPERATESTAFFCODE,F.STAFFNAME ".
"ORDER BY R.OPERATESTAFFCODE,F.STAFFNAME,R.PICKTIME,R.PICKMINUTE,R.ZONECODE";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print outputcsv("ピッキング作業実績（ゾーン毎ピッキング集計）".date('Y/m/d', strtotime('-1 day')),$dlfile_name);
$print_temp=",,,,,";
//項目名の表示
for ($c = 9; $c <= $nowtime; $c++) {
	$print_temp=$print_temp.$c."時,";
}
outputcsv($print_temp,$dlfile_name);
$print_temp="スタッフコード,スタッフ名,総計,ゾーン,合計,";
for ($c = 9; $c <= $nowtime; $c++) {
	$print_temp=$print_temp. "00-30,31-60,";
}
outputcsv($print_temp,$dlfile_name);
$total_count=0;
$i=0;
$motostaffcode="0";
for($cc = 1; $cc <= 24; $cc++) {
	$jikantotal[$cc][0]=0;
	$jikantotal[$cc][1]=0;
}
//取得データのセット
while($row = $res->fetchRow()){
	$total_count=$total_count+$row[4];
	if ($motostaffcode==($row[0]+0))
	{
	}else
	{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$staffzonecnt_a[$i]=0;
		$staffzonecnt_b[$i]=0;
		$staffzonecnt_c[$i]=0;
		$staffzonecnt_d[$i]=0;
		$staffzonecnt_e[$i]=0;
		$staffzonecnt_g[$i]=0;
		$staffzonecnt[$i]=0;
		$stafftotal[$i]=0;
		$staffzonetotal_a[$i]=0;
		$staffzonetotal_b[$i]=0;
		$staffzonetotal_c[$i]=0;
		$staffzonetotal_d[$i]=0;
		$staffzonetotal_e[$i]=0;
		$staffzonetotal_g[$i]=0;
//集計値初期化
		for($cc = 1; $cc <= 24; $cc++) {
			$forhtml_a[($i)][($cc)][0]=0;
			$forhtml_a[($i)][($cc)][1]=0;
			$forhtml_b[($i)][($cc)][0]=0;
			$forhtml_b[($i)][($cc)][1]=0;
			$forhtml_c[($i)][($cc)][0]=0;
			$forhtml_c[($i)][($cc)][1]=0;
			$forhtml_d[($i)][($cc)][0]=0;
			$forhtml_d[($i)][($cc)][1]=0;
			$forhtml_e[($i)][($cc)][0]=0;
			$forhtml_e[($i)][($cc)][1]=0;
			$forhtml_g[($i)][($cc)][0]=0;
			$forhtml_g[($i)][($cc)][1]=0;
		}
	}

//数値に変換するために+0してます
	switch ($row[5]){
	case "A";
//staffcode,時間,分
		$forhtml_a[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_a[$i]=$staffzonetotal_a[$i]+$row[4];
		$staffzonecnt_a[$i]=1;
		break;
	case "B";
		$forhtml_b[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_b[$i]=$staffzonetotal_b[$i]+$row[4];
		$staffzonecnt_b[$i]=1;
		break;
	case "C";
		$forhtml_c[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_c[$i]=$staffzonetotal_c[$i]+$row[4];
		$staffzonecnt_c[$i]=1;
		break;
	case "D";
		$forhtml_d[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_d[$i]=$staffzonetotal_d[$i]+$row[4];
		$staffzonecnt_d[$i]=1;
		break;
	case "E";
		$forhtml_e[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_e[$i]=$staffzonetotal_e[$i]+$row[4];
		$staffzonecnt_e[$i]=1;
		break;
	case "G";
		$forhtml_g[$i][($row[2]+0)][($row[3]+0)]=$row[4];
		$staffzonetotal_g[$i]=$staffzonetotal_g[$i]+$row[4];
		$staffzonecnt_g[$i]=1;
		break;
	}
	$stafftotal[$i]=$stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][($row[3]+0)]=$jikantotal[($row[2]+0)][($row[3]+0)]+$row[4];
}
//HTML表示
$print_temp="";
for($ii = 1; $ii <= $i; $ii++) {
	$headerflg=0;
	$staffzonecnt[$ii]=$staffzonecnt_a[$ii]+$staffzonecnt_b[$ii]+$staffzonecnt_c[$ii]+$staffzonecnt_d[$ii]+$staffzonecnt_e[$ii]+$staffzonecnt_g[$ii];
	$print_temp= $staffcode[$ii].",".$staffzonecnt[$ii].",".$staffname[$ii].",".$staffzonecnt[$ii].",".$stafftotal[$ii].",";
//Azone
	if ($staffzonetotal_a[$ii]>0)
	{
		$print_temp=$print_temp. "A,".$staffzonetotal_a[$ii].",";
		for ($c = 9; $c <= $nowtime; $c++) {
			$print_temp= $print_temp.$forhtml_a[$ii][$c][0].",".$forhtml_a[$ii][$c][1].",";
		}
		outputcsv($print_temp,$dlfile_name);
		$headerflg=1;
	}
//Bzone
	if ($staffzonetotal_b[$ii]>0)
	{
		if ($headerflg>0){$print_temp=",,,,,";}
		$print_temp=$print_temp. "B,".$staffzonetotal_b[$ii].",";
		for ($c = 9; $c <= $nowtime; $c++) {
			$print_temp= $print_temp.$forhtml_b[$ii][$c][0].",".$forhtml_b[$ii][$c][1].",";
		}
		outputcsv($print_temp,$dlfile_name);
		$headerflg=1;
	}
//Czone
	if ($staffzonetotal_c[$ii]>0)
	{
		if ($headerflg>0){$print_temp=",,,,,";}
		$print_temp=$print_temp. "C,".$staffzonetotal_c[$ii].",";
		for ($c = 9; $c <= $nowtime; $c++) {
			$print_temp= $print_temp.$forhtml_c[$ii][$c][0].",".$forhtml_c[$ii][$c][1].",";
		}
		outputcsv($print_temp,$dlfile_name);
		$headerflg=1;
	}
//Dzone
	if ($staffzonetotal_d[$ii]>0)
	{
		if ($headerflg>0){$print_temp=",,,,,";}
		$print_temp=$print_temp. "D,".$staffzonetotal_d[$ii].",";
		for ($c = 9; $c <= $nowtime; $c++) {
			$print_temp= $print_temp.$forhtml_d[$ii][$c][0].",".$forhtml_d[$ii][$c][1].",";
		}
		outputcsv($print_temp,$dlfile_name);
		$headerflg=1;
	}
//Ezone
		if ($headerflg>0){$print_temp=",,,,,";}
		{
		$print_temp=$print_temp. "E,".$staffzonetotal_e[$ii].",";
		for ($c = 9; $c <= $nowtime; $c++) {
			$print_temp= $print_temp.$forhtml_e[$ii][$c][0].",".$forhtml_e[$ii][$c][1].",";
		}
		outputcsv($print_temp,$dlfile_name);
		$headerflg=1;
	}
//Gzone
		if ($headerflg>0){$print_temp=",,,,,";}
		{
		$print_temp=$print_temp. "G,".$staffzonetotal_g[$ii].",";
		for ($c = 9; $c <= $nowtime; $c++) {
			$print_temp= $print_temp.$forhtml_g[$ii][$c][0].",".$forhtml_g[$ii][$c][1].",";
		}
		outputcsv($print_temp,$dlfile_name);
		$headerflg=1;
	}
}
$print_temp= ",,,,ピッキング点数合計,".$total_count.",";
for ($c = 9; $c <= $nowtime; $c++) {
	$print_temp= $print_temp.$jikantotal[$c][0].",".$jikantotal[$c][1].",";
}
outputcsv($print_temp,$dlfile_name);
//データの開放
$res->free();
$db->disconnect();
?>
