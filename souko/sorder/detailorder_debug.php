<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_GET["receptioncode"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}

//ログイン情報の読み込み
/* ECDBコネクション生成 */
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
$ECDB = DB::connect($dsn);
if(DB::isError($ECDB)){
	echo "Fail\n" . DB::errorMessage($ECDB) . "\n";
}
$ECDB->setOption('portability',DB_PORTABILITY_NULL_TO_EMPTY | DB_PORTABILITY_NUMROWS);

$psOtona = $ECDB->prepare("select ord_seq, instorecode, goods_cnt, ord_cncl_cnt, grp_id from torderdtl where ord_no = ?");

/* 倉庫DBコネクション生成 */
require_once("../parts/login_souko.php");

//SQL文のCOUNT関数を使用
$sql = "select d.RECEPTIONDATE,d.STATUS_KIND,d.CUSTOMERNAME1,d.COD_PRICE,d.SHIPPINGPRICE,d.EXPENCE_KIND,d.RECEPTION_AMOUNT,";
$sql = $sql . "d.RECEPTION_C_TAX,s.TRANSPORT_KIND,s.DELIVERYCODE,s.DELIVERYWEIGHT,s.DELIVERYDATETIME,";
$sql = $sql . "d.SHIPPINGNAME1,d.SHIPPINGNAME2,d.SHIPPINGPOSTCODE,";
$sql = $sql . "d.SHIPPINGADDRESS1,d.SHIPPINGADDRESS2,d.SHIPPINGADDRESS3,d.SHIPPINGADDRESS4,d.SHIPPINGADDRESS5,d.SHIPPINGTELNO ";
$sql = $sql . "from d_reception d,D_RECEPTION_SHIPMENT s";
$sql = $sql . " where d.RECEPTIONCODE=s.RECEPTIONCODE(+) and d.receptioncode ='".$keyti."'";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>注文番号：". $keyti."</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=120>受付日</td><td nowrap width=200>状態</td><td nowrap width=120>注文者名</td>";
print "<td nowrap width=80>決済方法</td><td nowrap width=80>代引金額</td><td nowrap width=80>送料</td>";
print "<td nowrap>注文金額(税込)</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//受付日
	print "<td>".Rethiduke($row[0]) ."</td>";
//状態
		print "<td>".Retstatuskind($row[1])."</td>";
//顧客名
	print "<td>".$row[2] ."</td>";
//決済方法
	print "<td>".Retkessai($row[5]) ."</td>";
//代引金額
	print "<td align=right>".$row[3] ."</td>";
//送料
	print "<td align=right>".$row[4] ."</td>";
//注文金額合計(税込)
	print "<td align=right>".$row[6] ."</td>";
	print "</tr></table>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap width=120>運送会社</td><td nowrap width=200>出荷コード</td><td nowrap width=120>出荷登録日時</td></tr>";
//運送会社
	print "<tr><td>".Retunsoukaisha($row[8])."</td>";
//出荷コード
//	print "<td>".Rethaisoubangou($row[9]) ."</td>";
	print "<td>".$row[9] ."</td>";
//出荷日時
	print "<td>".Rethidukejikan($row[11]) ."</td>";
	print "</TR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap width=120>送付先名</td><td nowrap width=500>送付先住所</td><td nowrap width=120>送付先連絡先</td></tr>";
	print "<tr><td>".$row[12].$row[13]."</td>";
	print "<td>".Retzip($row[14])."<BR>".$row[15].$row[16].$row[17].$row[18].$row[19]."</td>";
	print "<td>".Rettel($row[20])."</td>";
	print "</TR>";
	$j++;
}
print "</table>";
//データの開放
$res->free();
// 20170306 ピッキング日付日時追加 Start
//明細部の取得
$sql2 = "select substr(m.GENRE1CODE,0,2),g.detailno,g.instorecode,g.locationcode,g.newused_kind,g.unitprice,g.cancel_kind,g.picked_kind,g.operatestaffcode,f.staffname,m.SALEDATE,g.operatedate,g.operatetime from d_reception_goods g,m_goods m,m_staff f where g.instorecode = m.instorecode and g.operatestaffcode=f.staffcode(+) and g.receptioncode = '".$keyti."' order by g.detailno, g.locationcode";
// 20170306 End
$res2 = $db->query($sql2);
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

/* 2015.11.25 オトナ買い判定追加 */
$otonaFlg = array();
$resOtona = $ECDB->execute($psOtona, $keyti);
if(DB::isError($resOtona)){
	$resOtona->DB_Error($resOtona->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$ordSeq = 1;
while($rowOtona = $resOtona->fetchRow(DB_FETCHMODE_ASSOC)){
    for($i = 0; $i < $rowOtona['GOODS_CNT']; $i++ ){
        $otonaFlg[$ordSeq] = $rowOtona['GRP_ID'];
        $ordSeq++;
    }
}
$resOtona->free();
unset($rowOtona);
/* 2015.11.25 オトナ買い判定追加ここまで */

//検索結果の表示
print "<br><strong>注文明細</strong><br>\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap>明細番号</td><td nowrap>INSTORECODE</td><td nowrap>グループID</td><td nowrap>LOCATIONCODE</td><td nowrap>カテゴリ</td><td width='150' nowrap>タイトル名</td><td nowrap>巻数・組数</td><td nowrap>著作者・アーティスト</td><td nowrap>発売日</td><td nowrap>新古区分</td><td nowrap>単価</td><td nowrap>ピッキング状況</td><td  width='100' nowrap>備考</td><td nowrap>ピッキング担当者コード</td><td nowrap>ピッキング担当者名</td><td nowrap>ピッキング日付</td><td nowrap>ピッキング日時</td></tr>";
/*
//カラム数の取得
$ncols = $res->numCols();
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
  while($row = $res->fetchRow()){
        print "<tr><td> $j</td>";
        for($i=0;$i<$ncols;$i++){
                $column_value = $row[$i];//行の列データを取得
                print "<td> ".$column_value ."</td>";
        }
  $j++;
  }
*/
//新品フラグデフォルト値セット1になれば新品・新刊状態表示
$sinpinflg=0;
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row2 = $res2->fetchRow()){
	$getvalue=Retgoodsdetailsql($row2[2],$row2[0]);
	$res3 = $db->query($getvalue[1]);
	if(DB::isError($res3)){
		$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	$row3 = $res3->fetchRow();
    
    /* 2015.11.25 オトナ買い判定追加 */
    $grpID = "";
    if(isset($otonaFlg[$row2[1]]) and $otonaFlg[$row2[1]] != "" ){
//        $otonaFlgStr = "★";
        $grpID = $otonaFlg[$row2[1]];
    }
    /* 2015.11.25 オトナ買い判定追加ここまで */

	print "<tr>";
//明細番号
	print "<td align=center>".$row2[1] ."</td>";
/*
//在庫番号
	print "<td>".$row2[3] ."</td>";
//ロケーションコード
	print "<td>".Retlocation($row2[4]) ."</td>";
*/
//インストアコード
	print "<td align=center>".$row2[2] ."</td>";
//グループID
	print "<td align=center>".$grpID ."</td>";
//ロケーションコード
	print "<td align=center>".$row2[3] ."</td>";
//カテゴリ
	print "<td align=center>".$getvalue[0] ."</td>";
//タイトル名
	print "<td>".$row3[0] ."</td>";
//巻数・組数
	print "<td>　".$row3[2] ."</td>";
//著作者
	print "<td>".$row3[3] ."</td>";
if ($row2[10]>substr($keyti,0,8))
//発売日
{
	print "<td><font color=red>";
}else
{
	print "<td>";
}
/*
if ($row2[10]<substr($keyti,0,8))
//発売日
{
	print "<td>";
}else
{
	if ($row2[10]==substr($keyti,0,8)&&substr($keyti,8,4)<"0750")
	{
		print "<td><font color=red>";
	}else
	{
			print "<td>";
	}
}
*/
//print "●".substr($keyti,8,4)."●";
//print "●".$row2[10]."●";

	print $row2[10] ."</font></td>";
//新古区分
	print "<td align=center>".Retzaikokubun($row2[4])."</td>";
//新品・新刊ならばフラグセット
	if ($row2[4]==2 && $row2[6]=='0'){
		$sinpinflg=1;
	}
//単価
	print "<td align=center>".$row2[5] ."</td>";
//ピック区分
	if ($row2[6]=='0')
	{
		print "<td align=center>".Retpickubun($row2[7])."</td>";
	}else
	{
		print "<td align=center>　</td>";
	}
//キャンセル区分
	print "<td>".Retcancel($row2[6])."</td>";
//スタッフコード
	print "<td>".$row2[8]."</td>";
//スタッフ名
	print "<td>".$row2[9]."</td>";
// 20170306 ピッキング日付日時追加 Start
// ピッキング日付
	if($row2[11]) {
		print "<td>".substr($row2[11],0,4)."/".substr($row2[11],4,2)."/".substr($row2[11],6,2)."</td>";
	}
	else {
		print "<td></td>";
	}
// ピッキング日時
	if($row2[12]) {
		print "<td>".substr($row2[12],0,2).":".substr($row2[12],2,2).":".substr($row2[12],4,2)."</td>";
	}
	else {
		print "<td></td>";
	}
// 20170306 End
	$j++;
	print "</TR>";
	$res3->free();
}
print "</table>";
//データの開放
$res2->free();

if ($sinpinflg==1){
	$sql4 = "SELECT o.RECEPTIONCODE,o.ORDERCODE,o.TRADECODE,o.INSTORECODE,o.RESERVED_KIND,o.STATUS_KIND,";
	$sql4 = $sql4 . "o.FIXDATE,o.ORDERDATE,o.SHIPCODE,o.SHIP_DETAILNO,r.ORDERCODE,r.RESULT_KIND_1,r.ANSWERDATE_1,";
	$sql4 = $sql4 . "r.ORDER_QTY,r.RSVQTY1,r.NO_RSVQTY1,r.CANCELQTY1,r.ANSWERDATE_2,r.RESULT_KIND_2,r.RSVQTY2,r.NO_RSVQTY2,";
	$sql4 = $sql4 . "r.CANCELQTY2 from D_ORDER_GOODS　o,D_ORDER_RESULT r ";
	$sql4 = $sql4 . " where o.ORDERCODE=r.ORDERCODE and o.INSTORECODE=r.INSTORECODE and o.RECEPTIONCODE ='".$keyti."' ORDER BY O.INSTORECODE,O.ORDERCODE";
//print $sql4;
	$res4 = $db->query($sql4);
	if(DB::isError($res4)){
		$res4->DB_Error($res4->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	print "<br><strong>新品・新刊発注状況</strong><br>\n";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
//項目名の表示
	print "<td nowrap>発注コード</td><td nowrap>取引先ｺｰﾄﾞ</td><td nowrap>インストアコード</td>";
	print "<td nowrap>予約区分</td><td nowrap>状態区分</td><td nowrap>確定日</td><td nowrap>発注日</td>";
	print "<td nowrap>納品(出荷)番号</td><td nowrap>納品(出荷)明細番号</td><td nowrap>発注コード</td><td nowrap>受注結果回答</td>";
	print "<td nowrap>回答日(受注結果回答)</td><td nowrap>発注数量</td><td nowrap>引当数量</td><td nowrap>未引当数量</td>";
	print "<td nowrap>ｷｬﾝｾﾙ数量(受注結果回答)</td><td nowrap>回答日(受注残状況)</td><td nowrap>受注残状況</td>";
	print "<td nowrap>受注残引当数量</td><td nowrap>受注残未引当数量</td><td nowrap>ｷｬﾝｾﾙ数量(受注残状況)</td>";
	print "</tr>";
	$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	while($row4 = $res4->fetchRow()){
		print "<tr>";
//発注コード
		print "<td nowrap>".$row4[1] ."</td>";
//取引先ｺｰﾄﾞ
		print "<td>".Rettorihikisaki($row4[2]) ."</td>";
//インストアコード
		print "<td nowrap>".$row4[3] ."</td>";
//予約区分
		print "<td nowrap>".Retyoyakukubun($row4[4]) ."</td>";
//状態区分
		print "<td nowrap>".Rethachustatus($row4[5]) ."</td>";
//確定日
		print "<td nowrap>".Rethiduke($row4[6]) ."</td>";
//発注日
		print "<td nowrap>".Rethiduke($row4[7]) ."</td>";
//納品(出荷)番号
		print "<td nowrap>".$row4[8] ."</td>";
//納品(出荷)明細番号
		print "<td nowrap>".$row4[9] ."</td>";
//発注コード
		print "<td nowrap>".$row4[10] ."</td>";
//受注結果回答
		print "<td nowrap>".Rethachukaitou($row4[11]) ."</td>";
//回答日(受注結果回答)
		print "<td nowrap>".Rethiduke($row4[12]) ."</td>";
//発注数量
		print "<td nowrap>".$row4[13] ."</td>";
//引当数量
		print "<td nowrap>".$row4[14] ."</td>";
//未引当数量
		print "<td nowrap>".$row4[15] ."</td>";
//ｷｬﾝｾﾙ数量(受注結果回答)
		print "<td nowrap>".$row4[16] ."</td>";
//回答日(受注残状況)
		print "<td nowrap>".Rethiduke($row4[17]) ."</td>";
//受注残状況
		print "<td nowrap>".Rethachuzankaitou($row4[18]) ."</td>";
//受注残引当数量
		print "<td nowrap>".$row4[19] ."</td>";
//受注残未引当数量
		print "<td nowrap>".$row4[20] ."</td>";
//ｷｬﾝｾﾙ数量(受注残状況)
		print "<td>".$row4[21] ."</td>";
		print "</TR>";
		$j++;
	}
	print"</table>";
	$res4->free();
}
$db->disconnect();
$ECDB->disconnect();
?>
<BR>
<FORM action="searchorder.php"><INPUT TYPE="submit" VALUE="戻る"></FORM>
</body>
</html>