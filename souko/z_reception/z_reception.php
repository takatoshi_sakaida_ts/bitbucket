<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>Ｚ納品点数</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$sql = "select r.RECEPTIONDATE, count(DISTINCT r.RECEPTIONCODE), count(r.RECEPTIONDATE) from D_RECEPTION_GOODS g, D_RECEPTION r ".
       "where g.RECEPTIONCODE = r.RECEPTIONCODE ".
       "and g.STOCKNO = -1 ".
       "and (r.YAHOO_PICK_KIND = 2 or r.YAHOO_PICK_KIND is null) ".
       "group by r.RECEPTIONDATE, r.RECEPTIONCODE ".
       "order by r.RECEPTIONDATE desc, r.RECEPTIONCODE";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>Ｚ納品点数</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=100 align=center>日付</td>
	<td width=100 align=center>Ｚ納品件数</td>
	<td width=100 align=center>Ｚ納品点数</td>
</tr>
<tr>
<?php

$receptionDate = "";
$reception     = 0;
$productNum    = 0;

//取得データのセット
while($row = $res->fetchRow()){

	if($receptionDate != $row[0]) {
		if($receptionDate != "") {
			if($reception > 99) {
				print "<td align=center>".$receptionDate."</td><td bgcolor=#FF0000 width=100 align=right>".number_format($reception)."件</td><td width=100 align=right>".number_format($productNum)."点</td></tr>";
			} else {
				print "<td align=center>".$receptionDate."</td><td width=100 align=right>".number_format($reception)."件</td><td width=100 align=right>".number_format($productNum)."点</td></tr>";
			}
		}
		$receptionDate = $row[0];
		$reception    = 0;
		$productNum   = 0;
	}
	$reception  = $reception  + $row[1];
	$productNum = $productNum + $row[2];
}
if($reception > 99) {
	print "<td align=center>".$receptionDate."</td>";
	print "<td bgcolor=#FF0000 width=100 align=right>".number_format($reception)."件</td>";
	print "<td width=100 align=right>".number_format($productNum)."点</td></tr>";
} else {
	print "<td align=center>".$receptionDate."</td>";
	print "<td width=100 align=right>".number_format($reception)."件</td>";
	print "<td width=100 align=right>".number_format($productNum)."点</td></tr>";
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
