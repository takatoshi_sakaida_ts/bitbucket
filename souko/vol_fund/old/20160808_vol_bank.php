<?PHP
// 銀行情報のセット
//振込先が銀行ではなく、ゆうちょ銀行の場合はVOLOUTの改修が必要です
switch ($_SESSION["HSMAP"]["DANTAI"]){
	case 1:
//RoomToRead
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="041";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="虎ノ門支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="863793BE3A450FEC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンルームトウリードジヤパン";
		$sellno_head="01";
		break;
	case 2:
//シャンティ
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="770";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="巣鴨支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="20E84DE5F3B254FC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャ）シャンティコクサイボランティア";
		$sellno_head="02";
		break;
	case 3:
//ピースウィンズジャパン
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="138";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="笹塚支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5B365A285CBBF465";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ピースウィンズジャパン";
		$sellno_head="03";
		break;
	case 4:
//シャプラニール
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="053";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="高田馬場支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="206AA679E7967CD3";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）シャプラニールシミンニヨルカイガイキョウリョクノカイ";
		$sellno_head="04";
		break;
	case 5:
//JEN
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="664";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="飯田橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="886D280B9D13AEE4";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウジェン";
		$sellno_head="05";
		break;
	case 6:
//移動図書館支援（シャンティ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="770";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="巣鴨支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="20E84DE5F3B254FC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャ）シャンティコクサイボランティア";
		$sellno_head="06";
		break;
	case 7:
//かながわキンタロウ
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="379";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="51C6807DF7CA83A1";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ブックオフコーポレーション（カ";
		$sellno_head="07";
		break;
	case 8:
//神奈川大学
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="251";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="横浜駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="9046F3CCEBDA258E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ガッコウホウジンカナガワダイガク";
		$sellno_head="08";
		break;
	case 9:
//売って支援プログラム
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="096";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="東京公務部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D937D151E040CD22";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シャカイフクシホウジンチュウオウキョウドウボキンカイボラサポ";
		$sellno_head="09";
		break;
	case 10:
//しょうがっこうをおくる会
		$_SESSION["HSMAP"]["BANK_CD"]="0168";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="101";
		$_SESSION["HSMAP"]["BANK_NM"]="中国銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="DFE64E61A6A74E49";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ショウガッコウヲオクルカイリジオカムラハルオ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 11:
//結核予防会
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="532";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="九段支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="9DFAE09FAF71C34C";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）ケツカクヨボウカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 12:
//ICAN（アイキャン）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="221";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="名古屋駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="77B91EFA5FB3B975";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンアジアニホンソウゴ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 13:
//地球環境基金
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="B75425760A780B1A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ドク）カンキヨウサイセイホゼンキコウチキユウカンキヨウキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 14:
//民謡センター
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="664";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="飯田橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3FA5273FA4E97B24";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="コウエキザイダンホウジンミンサイセンターリジチョウ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 15:
//HFI（Hope and Faith International）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="131";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="玉川支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="78D4583A34376455";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ホープアンドフェイスインターナショナルダイヒョウフク";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 16:
//ACE（エース）
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="779";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="上野支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="DB2B6045EF6BBE25";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンエース";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 17:
//地球の友と歩む会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="052";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="神楽坂支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="734F7A81D081BAA8";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン　チキュウノトモトアユムカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 18:
//3keys（スリーキーズ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="171";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大塚支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="56FA06378FF96D00";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンスリーキーズ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 19:
//町田ゼルビア
		$_SESSION["HSMAP"]["BANK_CD"]="0138";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="931";
		$_SESSION["HSMAP"]["BANK_NM"]="横浜銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="町田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="808BFBD390B15B2F";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）ゼルビア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 20:
//SC相模原
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="259";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="6368D8AD8494EC45";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カブシキガイシャスポーツクラブサガミハラ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 21:
//幼い難民を考える会（CYR）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="045";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="六本木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3B33A0AD804ADBD8";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン オサナイナンミンヲカンガ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 22:
//JHP学校をつくる会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="045";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="六本木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="E1DC1B6053EB96EE";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン　ジエイエイチピーガツコウオ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 23:
//北海道森と緑の会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="637";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="札幌支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="64B41F24FB4BF373";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シヤダンホウジンホツカイドウモリトミドリノカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 24:
//アクセス
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="501";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="京都中央支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5F7331091C6280AD";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン アクセス キョウセイシャカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 25:
//ケア・インターナショナルジャパン
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="174";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="目白駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="0BBC2D49ACECBD66";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="コウエキザイダンホウジン　ケアインターナショナルジャパン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 26:
//世界の子どもにワクチンを(JCV)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="043";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="田町支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3E976664CB717FBC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンセカイノコドモニワクチンヲ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 27:
//ジャパンハート
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="117";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="蒲田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="8119F78E9518597B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジン　ジャパンハート";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 28:
//ブリッジ エーシア ジャパン(BAJ)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="329";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="新宿新都心支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="8C97071C6B237569";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンブリッジエーシアジャパンリジ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 29:
//大阪YMCA
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="108";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大阪為替集中店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="4E6D093D64C5EDEF";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）オオサカワイエムシーエー";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 30:
//グッドネーバーズ・ジャパン
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="351";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本郷支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="46700280D6AF9B6C";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）グッドネーバーズ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 31:
//国連ウィメン日本協会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="635";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="戸塚支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="D5B2E7957F123316";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）コクレンウィメンニホンキョウカイリジサカイマキコ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 32:
//湘南ふじさわシニアネット
		$_SESSION["HSMAP"]["BANK_CD"]="0150";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="300";
		$_SESSION["HSMAP"]["BANK_NM"]="スルガ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="藤沢支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="22526F57C569C578";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ショウナンフジサワシニアネット";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 33:
//アジア保健研修所(AHI)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="095";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="平針支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="47DC837C5092595B";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="サンジョカイイングチコウエキザイダンホウジンアジアホケンケン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 34:
//ウィメンズアクションネットワーク
		$_SESSION["HSMAP"]["BANK_CD"]="9900";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="099";
		$_SESSION["HSMAP"]["BANK_NM"]="ゆうちょ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="〇九九";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="2";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="80D30D44A50594BC";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="エヌピーオーホウジンダブルエーエヌ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 35:
//アーユス仏教国際協力ネットワーク
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="086";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="深川支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="754C667B0A1AD503";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクテイヒエイリカツドウホウジンアーユスブッキョウコクサイキ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 36:
//しみん基金・こうべ
		$_SESSION["HSMAP"]["BANK_CD"]="0009";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="410";
		$_SESSION["HSMAP"]["BANK_NM"]="三井住友銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="三宮支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="5065DA3ABFAA1FD5";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="シミンキキン．コウベ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 37:
//売って支援プログラム（熊本地震）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="805";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="やまびこ支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="FD3FB444702D042E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ニホンセキジュウジシャ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 38:
//東京都日中友好協会
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="331";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京UFJ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="神田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="6CB306E315AEBF83";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="エヌピーオーホウジントウキョウトニホンチュウゴクユウコウキョ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 39:
//結核予防会
		$_SESSION["HSMAP"]["BANK_CD"]="0001";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="532";
		$_SESSION["HSMAP"]["BANK_NM"]="みずほ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="九段支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="9DFAE09FAF71C34C";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）ケツカクヨボウカイ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 40:
//民際センター
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="664";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="飯田橋支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3FA5273FA4E97B24";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ザイ）ミンサイセンターリジチョウ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 41:
//しょうがっこうをおくる会
		$_SESSION["HSMAP"]["BANK_CD"]="0168";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="101";
		$_SESSION["HSMAP"]["BANK_NM"]="中国銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店営業部";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="DFE64E61A6A74E49";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ショウガッコウオオクルカイリジオカムラハルオ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 42:
//神奈川大学
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="251";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="横浜駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="9046F3CCEBDA258E";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ガク）カナガワダイガク";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 43:
//ICAN（アイキャン）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="221";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="名古屋駅前支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="77B91EFA5FB3B975";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）アイキャンダイヒョウリジタグ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 44:
//地球環境基金
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="001";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="本店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="B75425760A780B1A";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="ドク）カンキヨウサイセイホゼンキコウチキユウカンキヨウキキン";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 45:
//HFI（Hope and Faith International）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="131";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="玉川支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="78D4583A34376455";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）ホープアンドフェイスインターナショナルダイヒョウフク";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 46:
//ACE（エース）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="779";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="上野支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="DB2B6045EF6BBE25";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）エース";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 47:
//3keys（スリーキーズ）
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="171";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="大塚支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="56FA06378FF96D00";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）スリーキーズ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 48:
//町田ゼルビア
		$_SESSION["HSMAP"]["BANK_CD"]="0138";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="931";
		$_SESSION["HSMAP"]["BANK_NM"]="横浜銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="町田支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="808BFBD390B15B2F";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）ゼルビア";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 49:
//SC相模原
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="259";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="相模原支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="6368D8AD8494EC45";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="カ）スポーツクラブサガミハラ";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;
	case 50:
//幼い難民を考える会(CYR)
		$_SESSION["HSMAP"]["BANK_CD"]="0005";
		$_SESSION["HSMAP"]["BANK_BRANCH_CD"]="045";
		$_SESSION["HSMAP"]["BANK_NM"]="三菱東京ＵＦＪ銀行";
		$_SESSION["HSMAP"]["BRANCH_NM"]="六本木支店";
		$_SESSION["HSMAP"]["KOUZASHURUI"]="1";
		$_SESSION["HSMAP"]["KOUZABANGOU"]="3B33A0AD804ADBD8";
		$_SESSION["HSMAP"]["KOUZAMEIGI"]="トクヒ）オサナイナンミンヲカンガエル";
		$sellno_head=$_SESSION["HSMAP"]["DANTAI"];
		break;

//
	default:
		$retvalue= "エラー";
		break;
}
?>