function searchCheck(){
	var instoreCode = document.getElementById('instoreCode').value;
	if(instoreCode){
		return true;
	} else {
		alert("インストアコードを入力してください");
		return false;
	}
}

function deleteCheck(dickNo, trackName){
	if(window.confirm('Disk'+ dickNo +'\r\n「' + trackName + '」\r\nを削除します。')){
		return true;
	} else {
		return false;
	}
}

function insertCheck(){
	var checkJmd = false;
	var checkMusic = false;
	var checkDisplay = false;
	var checkContents = false;3
	
	var jmd = document.getElementById('insertJmdKey').value;
	if(jmd){
		if(jmd.match(/[^0-9]/g)){
			alert("JMDKEYに数値以外が含まれています");
		} else {
			checkJmd = true;
		}
	} else {
		alert("JMDKEYを入力してください");
	}
	
	var music = document.getElementById('insertMusicOrder').value;
	if(music){
		if(music.match(/[^0-9]/g)){
			alert("MUSICORDERに数値以外が含まれています");
		} else {
			checkMusic = true;
		}
	} else {
		alert("MUSICORDERを入力してください");
	}
	
	var display = document.getElementById('insertDisplayOrder').value;
	if(display){
		if(display.match(/[^0-9]/g)){
			alert("DISPLAYORDERに数値以外が含まれています");
		} else {
			checkDisplay = true;
		}
	} else {
		alert("DISPLAYORDERを入力してください");
	}
	
	var contents = document.getElementById('insertContents').value;
	if(!contents){
		alert("CONTENTSを入力してください");
	} else {
		checkContents = true;
	}
	
	if(checkJmd && checkMusic && checkDisplay && checkContents){
		return true;
	} else {
		return false;
	}
}

function updateCheck(){
	var trackName = document.getElementById('trackName').value;
	if(trackName){
		return true;
	} else {
		alert("トラック名を入力してください");
		return false;
	}
}
