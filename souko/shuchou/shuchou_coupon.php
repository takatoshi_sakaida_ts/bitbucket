<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="shuchou_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出張買取受付入力画面</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}
-->
</style>
</head>
<body>
<form method="POST" action="shuchou_confirm.php" name="volfm">
<b>集荷先情報</b>
<TABLE ID="menu1" width="900">

  <tr>
    <th>店舗名・受付番号<font color=red>　*　</font></th>
    <td>受付店舗名：
	<select name="PICKUP_STORE">
		<option value="0" >選択してください</option>
		<option value="秋葉原A" >秋葉原A</option>
		<option value="秋葉原B" >秋葉原B</option>
		<option value="飯田橋" >飯田橋</option>
		<option value="自由が丘" >自由が丘</option>
		<option value="東京出張買取センター" >東京出張買取センター</option>
	</select>
受付番号（ID）：
	<input type="text" name="PICKUP_STORENO" size="20" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_STORENO"])){
	print $_SESSION["HSMAP"]["PICKUP_STORENO"];
}
print '">';
?>
	</td>
  </tr>
  <tr>
    <th>箱数<font color=red></font></th>
    <td>
	<input type="text" name="BOX" size="10" maxlength="2" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["BOX"])){
	print $_SESSION["HSMAP"]["BOX"];
}
print '">';
?>

	</td>
  </tr>

  <tr>
    <th>フリガナ<font color=red>　*　</font></th>
    <td>
		<input type="text" name="PICKUP_NM_LAST" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_LAST"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_LAST"];
}
print '" >';
?>
		<input type="text" name="PICKUP_NM_ETC" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_ETC"])){
print $_SESSION["HSMAP"]["PICKUP_NM_ETC"];
}
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>名前</th>
    <td>
		<input type="text" name="PICKUP_NM_FST" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_FST"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
}
print '" >';
?>
		<input type="text" name="PICKUP_NM_MID" size="40" maxlength="19" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_NM_MID"])){
	print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
}
print '" >';
?>
    </td>
</TR>
  <tr>
    <th>生年月日<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_BIRTH_DTY" size="6" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_BIRTH_DTY"])){
	print $_SESSION["HSMAP"]["PICKUP_BIRTH_DTY"];
}
else{
/*
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_yesterday = $date_today + (86400*4);
print date('Y', $date_yesterday);
*/
}
print '" >年';
?>
	<input type="text" name="PICKUP_BIRTH_DTM" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_BIRTH_DTM"])){
	print $_SESSION["HSMAP"]["PICKUP_BIRTH_DTM"];
}
else{
//print date('m', $date_yesterday);
}
print '" >月';
?>
	<input type="text" name="PICKUP_BIRTH_DTD" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_BIRTH_DTD"])){
	print $_SESSION["HSMAP"]["PICKUP_BIRTH_DTD"];
}

print '" >日';
?>
    </td>
  </tr>

  </tr>
  <tr>
    <th>性別<font color=red>　*　</font></th>
    <td>
			<input name="MEM_SEX" type="radio" value="1">男
			<input name="MEM_SEX" type="radio" value="2">女
    </td>
  </tr>

  <tr>
    <th>職業<font color=red>　*　</font></th>
    <td>
	<select name="shokugyou">
		<option value="0" >選択してください
		<option value="1" >会社員
		<option value="2" >公務員
		<option value="3" >主婦
		<option value="4" >自営業
		<option value="5" >学生
		<option value="6" >パート
		<option value="7" >アルバイト
		<option value="8" >無職
		<option value="9" >その他
	</select>
    </td>
  </tr>

  <tr>
    <th>郵便番号<font color=red>　*　</font></th>
    <td>
	例：1540004<br>
	<input type="text" name="PICKUP_ZIP_CD" size="8" maxlength="7" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_CD"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_CD"];
}
print '" >';
?>
<input type="button" name="search_address" value="住所検索" onClick="OpenPostWin()">
    </td>
  </tr>

  <tr>
    <th>都道府県<font color=red>　*　</font></th>
    <td>
	<select name="PICKUP_ZIP_ADDR1">
		<option value="0" >選択してください
		<option value="1" >北海道
		<option value="2" >青森県
		<option value="3" >岩手県
		<option value="4" >秋田県
		<option value="5" >宮城県
		<option value="6" >山形県
		<option value="7" >福島県
		<option value="8" >茨城県
		<option value="9" >栃木県
		<option value="10" >群馬県
		<option value="11" >埼玉県
		<option value="12" >千葉県
		<option value="13" >東京都
		<option value="14" >神奈川県
		<option value="15" >新潟県
		<option value="16" >富山県
		<option value="17" >石川県
		<option value="18" >福井県
		<option value="19" >山梨県
		<option value="20" >長野県
		<option value="21" >岐阜県
		<option value="22" >静岡県
		<option value="23" >愛知県
		<option value="24" >三重県
		<option value="25" >滋賀県
		<option value="26" >京都府
		<option value="27" >大阪府
		<option value="28" >兵庫県
		<option value="29" >奈良県
		<option value="30" >和歌山県
		<option value="31" >鳥取県
		<option value="32" >島根県
		<option value="33" >岡山県
		<option value="34" >広島県
		<option value="35" >山口県
		<option value="36" >徳島県
		<option value="37" >香川県
		<option value="38" >愛媛県
		<option value="39" >高知県
		<option value="40" >福岡県
		<option value="41" >佐賀県
		<option value="42" >長崎県
		<option value="43" >熊本県
		<option value="44" >大分県
		<option value="45" >宮崎県
		<option value="46" >鹿児島県
		<option value="47" >沖縄県
	</select>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村<font color=red>　*　</font></th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR2" size="100" maxlength="12" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"];
}
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR3" size="100" maxlength="16" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"];
}
print '" >';
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
		<input type="text" name="PICKUP_ZIP_ADDR4" size="100" maxlength="16" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"];
}
print '" >';
?>
    </td>
  </tr>

  <tr>
    <th>電話番号<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_TEL_NO" size="20" maxlength="13" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_TEL_NO"])){
	print $_SESSION["HSMAP"]["PICKUP_TEL_NO"];
}
print '"> 	例： 045-123-4567<BR>';
?>
	</td>
  </tr>

  <tr>
    <th>日中連絡先<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_TEL_NO2" size="20" maxlength="13" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_TEL_NO2"])){
	print $_SESSION["HSMAP"]["PICKUP_TEL_NO2"];
}
print '"> 	例： 045-123-4567<BR>';
?>
	</td>
  </tr>

  <tr>
    <th>買取金額の承認と<BR>お値段のつかない品物<font color=red>　*　</font></th>
    <td>
			<input name="DTAIOU" type="radio" value="1">おまかせ承認コース（センター引取り）<BR>
			<input name="DTAIOU" type="radio" value="2">査定結果ハガキを確認してから承認コース（センター引取り）<BR>
			<input name="DTAIOU" type="radio" value="3">査定結果ハガキを確認してから承認コース（返却希望）
    </td>
  </tr>

  <tr>
    <th>メモ</th>
    <td>
	<input type="text" name="memo" size="100" maxlength="200" style="ime-mode:active;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEMO"])){
	print $_SESSION["HSMAP"]["MEMO"];
}
print '" >';
?>
	</td>
  </tr>
</table>
<BR>※銀行口座欄はEC買取情報より入力してください。<BR>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
}
print '" >';
?>
<input type="hidden" name="PICKUP_STORE_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_STORE_index"])){
	print $_SESSION["HSMAP"]["PICKUP_STORE_index"];
}
print '" >';
?>
<input type="hidden" name="shokugyou_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["shokugyou_index"])){
	print $_SESSION["HSMAP"]["shokugyou_index"];
}
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEM_SEX"])){
	print ($_SESSION["HSMAP"]["MEM_SEX"]);
}
print '" >';
?>
<input type="hidden" name="DTAIOU_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["DTAIOU"])){
	print ($_SESSION["HSMAP"]["DTAIOU"]);
}
print '" >';
?>
<input type="hidden" name="postname" value="">

</TABLE>
<BR>
<script type="text/javascript">
document.volfm.PICKUP_ZIP_ADDR1.selectedIndex=document.volfm.PICKUP_ZIP_ADDR1_index.value;
document.volfm.PICKUP_STORE.selectedIndex=document.volfm.PICKUP_STORE_index.value;
document.volfm.shokugyou.selectedIndex=document.volfm.shokugyou_index.value;
var num;
num=(document.volfm.MEM_SEX_index.value - 1);
if(num==-1){
	}else{
	document.volfm.MEM_SEX[(document.volfm.MEM_SEX_index.value-1)].checked=true;
}
num=(document.volfm.DTAIOU_index.value - 1);
if(num==-1){
	}else{
	document.volfm.DTAIOU[(document.volfm.DTAIOU_index.value-1)].checked=true;
}
</script>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="EntCan()">
<input type="BUTTON" value="　　登録　　" onClick="EntCheck()">
</FORM>
</body>
</html>