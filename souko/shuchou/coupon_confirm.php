<?PHP
	if ($_POST["shokugyou"] == '') {
		die('致命的エラー');
	}
require '../parts/pagechk.inc';
require '../parts/utl.inc';
require_once("selectvalue_shuchou.php");
$_SESSION["HSMAP"]["SELL_NO_PREFIX"] = "Z8801";
$_SESSION["HSMAP"]["PICKUP_NM_FST"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_FST"]));
$_SESSION["HSMAP"]["PICKUP_NM_MID"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_MID"]));
$_SESSION["HSMAP"]["PICKUP_NM_LAST"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_LAST"]));
$_SESSION["HSMAP"]["PICKUP_NM_ETC"]=htmlspecialchars(escapevalue($_POST["PICKUP_NM_ETC"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_CD"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]=RetPrefecture(htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR1"])));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR2"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR3"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR4"]));
$_SESSION["HSMAP"]["PICKUP_TEL_NO"]=htmlspecialchars(escapevalue($_POST["PICKUP_TEL_NO"]));
$_SESSION["HSMAP"]["PICKUP_TEL_NO2"]=htmlspecialchars(escapevalue($_POST["PICKUP_TEL_NO2"]));
$_SESSION["HSMAP"]["MEM_SEX"]=htmlspecialchars(escapevalue($_POST["MEM_SEX"]));
$_SESSION["HSMAP"]["shokugyou"]=htmlspecialchars(escapevalue($_POST["shokugyou"]));
$_SESSION["HSMAP"]["PICKUP_STORE"]=htmlspecialchars(escapevalue($_POST["PICKUP_STORE"]));
$_SESSION["HSMAP"]["PICKUP_STORENO"]=htmlspecialchars(escapevalue($_POST["PICKUP_STORENO"]));
$_SESSION["HSMAP"]["DTAIOU"]=htmlspecialchars(escapevalue($_POST["DTAIOU"]));
$_SESSION["HSMAP"]["BOX"]=htmlspecialchars(escapevalue($_POST["BOX"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR1_index"]));
$_SESSION["HSMAP"]["MEM_SEX_index"]=htmlspecialchars(escapevalue($_POST["MEM_SEX_index"]));
$_SESSION["HSMAP"]["shokugyou_index"]=htmlspecialchars(escapevalue($_POST["shokugyou_index"]));
$_SESSION["HSMAP"]["PICKUP_STORE_index"]=htmlspecialchars(escapevalue($_POST["PICKUP_STORE_index"]));
$_SESSION["HSMAP"]["MEMO"]=htmlspecialchars(escapevalue($_POST["memo"]));
$_SESSION["HSMAP"]["PICKUP_BIRTH_DTY"]=htmlspecialchars(escapevalue($_POST["PICKUP_BIRTH_DTY"]));
$_SESSION["HSMAP"]["PICKUP_BIRTH_DTM"]=htmlspecialchars(escapevalue($_POST["PICKUP_BIRTH_DTM"]));
$_SESSION["HSMAP"]["PICKUP_BIRTH_DTD"]=htmlspecialchars(escapevalue($_POST["PICKUP_BIRTH_DTD"]));

?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出張買取受付入力確認画面(クーポン適応)</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}
-->
</style>
</head>
<body>
<form method="POST" action="coupon_ent.php" name ="volcon">
<b>集荷先情報</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>店舗名・受付番号</th>
    <td>店舗名：
<?PHP
print $_SESSION["HSMAP"]["PICKUP_STORE"];
?>
　受付番号：
<?PHP
print $_SESSION["HSMAP"]["PICKUP_STORENO"];
?>
	</td>
  </tr>

  <tr>
    <th>箱数</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["BOX"];
?>
	</td>
  </tr>

  <tr>
    <th>フリガナ</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_NM_LAST"];
print $_SESSION["HSMAP"]["PICKUP_NM_ETC"];
?>
    </td>
</TR>

  <tr>
    <th>名前</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
?>
    </td>
</TR>

  <tr>
    <th>生年月日</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_BIRTH_DTY"];
print '年';
print $_SESSION["HSMAP"]["PICKUP_BIRTH_DTM"];
print '月';
print $_SESSION["HSMAP"]["PICKUP_BIRTH_DTD"];
print '日';
?>
    </td>
  </tr>


  <tr>
    <th>性別</th>
    <td>
<?PHP
print Retseibetu($_SESSION["HSMAP"]["MEM_SEX"]);
?>    </td>
  </tr>

  <tr>
    <th>職業</th>
    <td>
<?PHP
print Retshokugyou($_SESSION["HSMAP"]["shokugyou"]);
?>
    </td>
  </tr>

  <tr>
    <th>郵便番号</th>
    <td>
<?PHP
print Retzip($_SESSION["HSMAP"]["PICKUP_ZIP_CD"]);
?>
    </td>
  </tr>

  <tr>
    <th>都道府県</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"];
?>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"];
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"];
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"];
?>
    </td>
  </tr>

  <tr>
    <th>電話番号</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_TEL_NO"];
?>
	</td>
  </tr>

  <tr>
    <th>日中連絡先</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_TEL_NO2"];
?>
	</td>
  </tr>

  <tr>
    <th>買取金額の承認とお値段のつかない品物</th>
    <td>
<?PHP
print Retdtaiou($_SESSION["HSMAP"]["DTAIOU"]-1);
?>    </td>
  </tr>

  <tr>
    <th>メモ</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["MEMO"];
?>
	</td>
  </tr>


<input type="hidden" name="DANTAI_index" value="
<?PHP
print $_SESSION["HSMAP"]["DANTAI_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
print ($_SESSION["HSMAP"]["MEM_SEX"]);
print '" >';
?>
<input type="hidden" name="TUUTI_index" value="
<?PHP
print ($_SESSION["HSMAP"]["TUUTI"]);
print '" >';
?>
<input type="hidden" name="DTAIOU_index" value="
<?PHP
print ($_SESSION["HSMAP"]["DTAIOU"]);
print '" >';
?>
<input type="hidden" name="gaido_index" value="
<?PHP
print ($_SESSION["HSMAP"]["gaido"]);
print '" >';
?>
</TABLE>
<BR>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="entback()">
<input type="submit" value="　　登録　　">
</FORM>
<script type="text/javascript">
function entback()
{
document.volcon.action="coupon.php";
document.volcon.submit();
}
</script>

</body>
</html>