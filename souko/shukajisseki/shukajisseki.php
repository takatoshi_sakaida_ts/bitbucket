<?PHP
require '../parts/pagechk.inc';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<script type="text/javascript" src="jquery-1.4.3.js" charset="Shift_JIS"></script>
<script type="text/javascript" src="jquery.dataTables.js" charset="Shift_JIS"></script>
<script type="text/javascript" src="jquery.csv.js" charset="Shift_JIS"></script>
<script type="text/javascript" src="ecl.js" charset="Shift_JIS"></script>
<script type="text/javascript" src="jquery.csv2table-0.02-b-4.7.js" charset="Shift_JIS"></script>
<script type="text/javascript" src="shukajisseki.js" charset="Shift_JIS"></script>
<link type="text/css" rel="stylesheet" href="shukajisseki.css"></link>
<title>  集荷実績データダウンロード </title>
</heade>
<body>
<?php
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
?>
<strong>集荷実績データダウンロード</strong>

<table id="table_collect_result">
<thead>
<tr>
 <th></th>
 <th></th>
 <th></th>
 <th></th>
 <th></th>
 <th></th>
 <th></th>
</tr>
</thead>
<tbody>
</tbody>
</table>
<hr>
<br><br>
個別にデータを編集したい場合はファイルをダウンロード(右クリックメニュー→保存)してください。<br>
<table border=0>
<tr>
<td>
<a href="data/all/all_data_sjis.csv">全データ</a>
</td>
</tr>
<?php
	$SearchDirPath = './data';
	$FileList = GetDirList( $SearchDirPath );
	foreach ( $FileList as $FileName )
	{
		$Path = $SearchDirPath . '/' . $FileName;
		if( is_file ( $Path ) )
		{
			print "<tr>\n<td>\n";
			print '<a href="' . $Path . '">' . $FileName . '</a>';
			print "</td>\n</tr>\n";
		}
	}
?>
</table>
<br>
<INPUT TYPE="button" VALUE="戻る" onClick="history.back()">
</body>
</html>

<?php
/**
 * フォルダからファイル名の一覧を取得
 */
function GetDirList( $SearchDirPath )
{
	if ($Handle = opendir( $SearchDirPath )) {

		// フォルダ内のファイル名の一覧を取得
		while( false !== ($FileList[] = readdir($Handle)) );
		closedir($Handle);

		// 降順ソート
		rsort( $FileList );   
		return $FileList;
	}
}
?>
