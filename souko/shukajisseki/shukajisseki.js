var baseGenre1 = "9999";
var itemList = null;
var faceGenre = {};

$(function(){
    createDataTable(baseGenre1);
});

function createDataTable(targetGenre1) {
  itemList = $("#table_collect_result").dataTable({
    "iDisplayLength": -1,
    "bAutoWidth": false,
    "bPaginate": false,
    "bInfo": false,
    "bRetrieve": true,
    "bLengthChange" : false,
    "aoColumns": [
      {"sTitle": "買取受付番号",   "sType": 'string'},
      {"sTitle": "集荷日",         "sType": 'string'},
      {"sTitle": "申し込み者",     "sType": 'string'},
      {"sTitle": "申し込み者カナ", "sType": 'string'},
      {"sTitle": "申し込み箱数",   "sType": 'string'},
      {"sTitle": "実績箱数",       "sType": 'string'},
      {"sTitle": "箱数差分",       "sType": 'string'}
    ],
    "aoColumnDefs": [
      { "bSortable": false, "aTargets": [ 0, 1 ] }
    ],
    "oLanguage": {
      "sSearch": "検索(買取受付番号もしくは氏名)",
      "sEmptyTable": "該当する申し込みがありません",
      "sZeroRecords": "指定されたキーワードに該当する申し込みはありません"
    }
  });

  var cells = new Array();

  $.get('data/all/all_data.csv?' + new Date().getTime(), function(data) {
    var csv = $.csv()(data);
    $(csv).each(function(index) {
      if(this.length != 7) {
        return true;
      }

      var sellNo = jQuery.trim(this[0]);
      var picupDate = jQuery.trim(this[1]);
      var name = jQuery.trim(this[2]);
      var kana = jQuery.trim(this[3]);
      var box_plan = jQuery.trim(this[4]);
      var box_achive = jQuery.trim(this[5]);
      var box_diff = jQuery.trim(this[6]);


      cells = new Array(
        sellNo,
        picupDate,
        name,
        kana,
        box_plan,
        box_achive,
        box_diff
      );
      itemList.fnAddData(cells,false);
    });

    itemList.fnDraw();
  });
}



/* formatted-num は aoColums で独自に追加するデータタイプの名称。
 * 名称は変更しても、データタイプの名称を変更後の名称に修正すれば、問題ない。
 * -desc はこのデータタイプを降順でソートする場合の処理内容が記述されている。
 * -asc は昇順でソートする際の処理内容。
*/
jQuery.fn.dataTableExt.oSort['formatted-num-asc'] = function(x,y) {
    /* 正規表現で数値と小数点以外は削除する */
    x = x.replace(/[^\d\-\.\/]/g, '');
    y = y.replace(/[^\d\-\.\/]/g, '');
    /* 文字列が分数の場合は除算してソートできるようにする */
    if(x.indexOf('/') >= 0) x = eval(x);
    if(y.indexOf('/') >= 0) y = eval(y);
    return x/1 - y/1;
};

jQuery.fn.dataTableExt.oSort['formatted-num-desc'] = function(x,y) {
    /* 正規表現で数値と小数点以外は削除する */
    x = x.replace(/[^\d\-\.\/]/g, '');
    y = y.replace(/[^\d\-\.\/]/g, '');
    /* 文字列が分数の場合は除算してソートできるようにする */
    if(x.indexOf('/') >= 0) x = eval(x);
    if(y.indexOf('/') >= 0) y = eval(y);
    return y/1 - x/1;
};

function multiUpdate(targetId, data) {
  bases = $("[id=" + targetId + "]");
  $.each(bases, function(key,value) {
    $(this).text(data);
  });
}
