<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>  計算　通知要/不要・D引取可否・箱数変更対象検索結果 </title>
</head>
<body bgcolor="#99ffcc">
<form method="POST" action="updsell_upd_confirm.php" name="volfm">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
if (substr($keyti,0,1)=="Z"){$keyti = "000" . substr($keyti,1,12);}
if (substr($keyti,0,1)=="R"){$keyti = "000" . substr($keyti,1,12);}
$_SESSION["HSMAP"]["sell_no"]=$keyti;

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select r.asscode,r.customername,r.box_recept_qty,r.assresult_kind,r.ACCEPTANCE_KIND,a.status_kind from d_ass_request r,d_ass a";
$sql = $sql . " where r.asscode=a.asscode (+) and r.asscode='".$_SESSION["HSMAP"]["sell_no"]."'";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索条件：". $keyti."</strong> <br><br>\n<HR>";
$row = $res->fetchRow();
if (empty($row[0])) {
	print "対象の買取受付は倉庫システムにありません";
} else {
	$row3 = Retkeisankekka($row[3]);
	$row4 = RetDtaiou($row[4]);
	$html = <<< EOD
	<table border=1>
		<tr>
	<!-- 項目名の表示 -->
			<td nowrap width=120>買取受付番号</td><td nowrap width=100>氏名</td><td width=100>箱数</td><td width=100>計算結果要否</td><td width=100>D対応</td>
		</tr>
		<tr>
	<!-- 注文番号 -->
			<td width=100>{$row[0]}</td>
			<td>{$row[1]}</td>
			<td width=100>{$row[2]}</td>
			<td width=100>{$row3}</td>
			<td width=100>{$row4}</td>
		</tr></table>
		<HR>
	<table border=1>
		<tr>
	<!-- 項目名の表示 -->
			<td nowrap width=120>買取受付番号</td><td nowrap width=100>氏名</td><td width=100>箱数</td><td width=100>計算結果要否</td><td width=100>D対応</td>
		</tr>
		<tr>
	<!-- 注文番号 -->
			<td width=100>{$row[0]}</td>
			<td>{$row[1]}</td>
			<td width=100>
				<input type="text" name="box_recept_qty" size="10" maxlength="2" style="ime-mode:inactive;" value="{$row[2]}"></td>
			<td>
				<input name="assresult_kind" type="radio" value="0" onClick="radioCh1()">不要
				<input name="assresult_kind" type="radio" value="1" onClick="radioCh1()">要
			</td>
			<td>
				<input name="ACCEPTANCE_KIND" type="radio" value="0" onClick="radioCh2()">BOLにて引取
				<input name="ACCEPTANCE_KIND" type="radio" value="1" onClick="radioCh2()">返却希望
			</td>
		</tr></table>
<HR>
EOD;
	print $html;
	$_SESSION["HSMAP"]["assresult_kind"]=$row[3];
	$_SESSION["HSMAP"]["ACCEPTANCE_KIND"]=$row[4];
	$html2 = <<< EOD
		<input type="hidden" name="assresult_kind_hid" value="{$row[3]}">
		<input type="hidden" name="ACCEPTANCE_KIND_hid" value="{$row[4]}">
EOD;
	print $html2;
}
$res->free();
$db->disconnect();
?>
<script type="text/javascript">
document.volfm.assresult_kind[(document.volfm.assresult_kind_hid.value)].checked=true;
document.volfm.ACCEPTANCE_KIND[(document.volfm.ACCEPTANCE_KIND_hid.value)].checked=true;

function isNumchk(obj,words){
	txt = obj.value;
	for (i=0; i<txt.length; i++)
	{
		c = txt.charAt(i);
		if ("0123456789".indexOf(c,0) < 0)
		{
			alert(words+"に数値以外が含まれてます");
			return false;
		}
	}
	return true;
}
function ishissuchk(obj,words){
if (obj.value=="")
{
	alert(words+"を入力してください");
	return false;
}
	return true;
}

function EntCheck()
{
<!--箱数チェック
	if (!isNumchk(document.volfm.box_recept_qty,"箱数")){return false;}
	if (!ishissuchk(document.volfm.box_recept_qty,"箱数")){return false;}
 	if (document.volfm.box_recept_qty.value<1 || document.volfm.box_recept_qty.value>99)
	{
			alert("箱数を正しく入力してください");
			return;
	}
	res = confirm("更新してよろしいでしょうか？");
	if(res == true){
		document.volfm.submit();
	}
}
function radioCh1() {
	var element1 = document.getElementsByName("assresult_kind");
	var element2 = document.getElementsByName("ACCEPTANCE_KIND");

	if (element1[0].checked) {
			element2[0].checked = true;
	}
}
function radioCh2() {
	var element1 = document.getElementsByName("assresult_kind");
	var element2 = document.getElementsByName("ACCEPTANCE_KIND");

	if (element2[1].checked) {
			element1[1].checked = true;
	}
}

</script>
<BR>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="history.back()">
<input type="BUTTON" value="　　登録　　" onClick="EntCheck()">
</FORM>
</body>
</html>
