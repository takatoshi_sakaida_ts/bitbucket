<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>フリーロケーション在庫数検索</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select locationcode,STOCKRESERVE_KIND,count(stockno) from d_stock where locationcode like '9%' group by locationcode,STOCKRESERVE_KIND order by locationcode,STOCKRESERVE_KIND";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>フリーロケーション在庫数検索結果</strong> <br><br>\n";
print "<table border=1>\n";
print "<tr>\n";
//項目名の表示
print "<TD></TD><td align=center>ロケーションコード</td><td align=center>ロケーション名</td><td align=center>引当状況</td><td align=center>在庫数</td></tr>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>　". $j."　</td>";
//ロケーションコード
	print "<td width=200>".$row[0] ."</td>";
	print "<td width=200>".Retlocation($row[0]) ."</td>";
	print "<td width=100>".RetSTOCKRESERVEKIND($row[1]) ."</td>";
	print "<td align=right width=100>".$row[2] ."</td>";
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>