<?php
require '../parts/pagechk.inc';
$today = date("YmdHis");
$file_name = "./dlfile/syouninhoyuuout_" . $today . ".csv";
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
output_log("商品保留ユーザダウンロード",$_SESSION["user_id"],'syouninhoyuuout_dl');

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
$sql = "SELECT T.SELL_NO,".
"T.PICKUP_NM_FST||T.PICKUP_NM_MID,".
"S.TOT_ASS_AMT,".
"S.VALID_ITEM_CNT,".
"S.INVALID_ITEM_CNT,".
"DECODE(T.NOTE_YN,0,'計算結果不要',1,'計算結果要'),".
"DECODE(T.DISPOSE,0,'センター引取',1,'返却'),".
"DECODE(T.RECEIPT_TP,1,'WEB',2,'FD'),".
"T.REG_DM,".
"S.ASS_DT ".
"FROM TSELL T,TSELLASSESSMENT S ".
"WHERE ".
"T.SELL_NO=S.SELL_NO ".
"AND T.SELL_STAT='07' ORDER BY T.SELL_NO";


$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	require '../parts/outputcsv_query.php';
	outputcsv($res,$file_name);
$res->free();
//
$db->disconnect();
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>承認保留ユーザダウンロード</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>