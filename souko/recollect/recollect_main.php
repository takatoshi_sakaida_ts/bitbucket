<?php
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>再集荷依頼</title>
</head>
<body>
<?php
//ライブラリの読み込み
require_once("../parts/selectvalue_souko.php");
require_once("DB.php");
require_once("Log.php");

$conf = array('mode'=>0777,'lineFormat' => '%1$s %3$s [%2$s] %4$s','timeFormat'=>'%Y-%m-%d %H:%M:%S');
$date = date("Ymd");
$user_id = isset($_SESSION['user_name']) ? $_SESSION['user_name'] : "";
$log = &Log::factory('file', './log/' . $date . '.log', $user_id, $conf);

// 検索された買取番号を取得
$sellNo = $_GET["sellno"];
if (empty($sellNo)) {
    die("買取受付番号が指定されていません");
}

/* 変数定義 */
$MAX_DATE_INTERVAL_FD = 19; // hasegawa modify
$MAX_DATE_INTERVAL_WEB = 14;
$PICKUP_CORP_NO_SAGAWA = "000000";
$PICKUP_CORP_NM_SHUKA = "";

$ENABLE_UPDATE_FLG = true;
$ERROR_MESSAGE = array();

$sql = "select SELL_NO, SELL_STAT, MEM_NO, MEM_ID, PICKUP_NM_FST, PICKUP_NM_MID, PICKUP_REQ_DT, PICKUP_REQ_TIME, PICK_DATA_EXPT, SELL_FORM_EXPT, REG_DM, PICKUP_CORP_NO, PICKUP_DT, SELL_ROUTE_TP from tsell where SELL_STAT <> '99' and sell_no = ? ";
$getTcodesql = "select CD_DTL_NO, CD_DTL_NM from tcodedtl where cd_no = '1041' ";
$getPickupnmsql = "select PICKUP_CORP_NM from tsell s, tpickup_corp p where s.PICKUP_CORP_NO = p.PICKUP_CORP_NO and s.SELL_STAT <> '99' and s.sell_no = ? ";

$lg_getDass = "select ASSCODE from D_ASS where ASSCODE = ?";

$date_interval = 0; // 

//ログイン情報の読み込み
// require_once("../parts/login_ecd.php");
require_once("recollect_util.php");
require_once("../log/loger.inc");

// 設定ファイル読込
$ini = parse_ini_file("../parts/db.ini",true);

// ECDBコネクション取得
$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
//データベースへの接続開始
$db = DB::connect($dsn_ec);
if(DB::isError($db)){
	die($db->getMessage());
}
$db->autoCommit(false);

// 倉庫DBコネクション取得
$dsn_lg = "oci8://". $ini['logi']['usr'] . ":" . $ini['logi']['pwd'] . "@" . $ini['logi']['dbn'];
//データベースへの接続
$lgdb = DB::connect($dsn_lg);
if(DB::isError($lgdb)){
    die($lgdb->getMessage());
}
$lgdb->autoCommit(false);

// 買取情報を取得
$res = $db->query($sql,$sellNo);
if(DB::isError($res)){
    die($res->getMessage());
}
$sell = $res->fetchRow(DB_FETCHMODE_ASSOC);
$res->free();
if(empty($sell) || is_null($sell)){
	print '<p>指定の買取データが存在しないか、既にキャンセルされています。</p>';
    print '<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onclick="location.href = \'recollect_search.php\'"></FORM>';
    die();
}

/* FD、WEB申込で期限を変える */
if($sell['SELL_ROUTE_TP'] == "" || is_null($sell['SELL_ROUTE_TP'])){
    $date_interval = $MAX_DATE_INTERVAL_FD;
} else{
    $date_interval = $MAX_DATE_INTERVAL_WEB;
}
//echo "<!-- {$date_interval} -->";

/* ### バリデーションチェック ### */
/* 集荷済みでないか */
if(!empty($sell['PICKUP_DT']) || $sell['PICKUP_DT'] != null || $sell['PICKUP_DT'] != ""){
    $ENABLE_UPDATE_FLG = false;
    $ERROR_MESSAGE[] = date("Y/m/d",strtotime($sell['PICKUP_DT'])) . " 集荷済みです。";
}
/* 佐川集荷でないか */
if($sell['PICKUP_CORP_NO'] == $PICKUP_CORP_NO_SAGAWA ){
    $ENABLE_UPDATE_FLG = false;
    $ERROR_MESSAGE[] = "契約トラック集荷ではありません。";
}
/* 再集荷可能な期間内か */
if(day_diff($sell['REG_DM'], date("Ymd",strtotime("now"))) >= $date_interval){
    $ENABLE_UPDATE_FLG = false;
    $ERROR_MESSAGE[] = "再集荷依頼可能な期限を過ぎています。";
}
/* 倉庫DBのD_ASSが存在するか => (有)既に査定が始まっている */
$res = $lgdb->query($lg_getDass,convertAsscode($sell['SELL_NO']));
if(DB::isError($res)){
    die($res->getMessage());
}
if($res->fetchRow()){
    $ENABLE_UPDATE_FLG = false;
    $ERROR_MESSAGE[] = "既に査定が開始されています。";
}
$res->free();

/* Tcode1041(集荷時間帯)を取得 */
$res = $db->query($getTcodesql);
if(DB::isError($res)){
    die($res->getMessage());
}
$tcode1041 = array();
while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
    $tcode1041[] = $row;
}
$res->free();
// print_r($tcode1041);

/* 集荷会社の名前を取得 */
if($sell['PICKUP_CORP_NO'] != $PICKUP_CORP_NO_SAGAWA){ // 佐川じゃない場合
	$res = $db->query($getPickupnmsql,$sellNo);
	if(DB::isError($res)){
		die($res->getMessage());
	}
	$pickupnm = $res->fetchRow(DB_FETCHMODE_ASSOC);
	$res->free();
	
	$PICKUP_CORP_NM_SHUKA = $pickupnm['PICKUP_CORP_NM'];
}else{ // 佐川の場合
	$PICKUP_CORP_NM_SHUKA = "佐川";
}
//echo "集荷会社:".$PICKUP_CORP_NM_SHUKA;
?>

<strong>買取受付番号：<?php echo $sellNo; ?></strong> <br><br>

<form method="POST" action="recollect_update.php">
<table border="1">
    <tr>
        <td>会員No</td>
        <td>会員ID</td>
        <td>氏名</td>
        <td><font color="red">*</font>集荷希望日</td>
        <td><font color="red">*</font>集荷希望時間帯</td>
        <td><font color="red">*</font>集荷依頼出力ステータス</td>
        <td><font color="red">*</font>買取ステータス</td>
        <td>買取申込日</td>
        <td>集荷会社</td>
    </tr>
    <tr>
        <td><?php echo $sell['MEM_NO']; ?></td>
        <td><?php echo $sell['MEM_ID']; ?></td>
        <td><?php echo $sell['PICKUP_NM_FST']." ".$sell['PICKUP_NM_MID']; ?></td>
        <td>
            <?php 
            if($ENABLE_UPDATE_FLG){
                $possibleDates = getPossiblePickDate($sell['REG_DM'], date("Ymd",strtotime("now")),$date_interval);
                echo '<select name="value[PICKUP_REQ_DT]">';
                foreach($possibleDates as $dt):
                    $opt = ($dt == $sell['PICKUP_REQ_DT']) ? "selected" : "";
                    print '<option value="' . $dt . '"' . $opt . '>' . date("Y/m/d",strtotime($dt)) . '</option>';
                endforeach;
                echo '</select>';
            } else{
                echo date("Y/m/d", strtotime($sell['PICKUP_REQ_DT']));;
            }
            ?>
        </td>
        <td>
            <?php 
            if($ENABLE_UPDATE_FLG){
                echo '<select name="value[PICKUP_REQ_TIME]">';
                foreach($tcode1041 as $value):
                    $opt = ($value['CD_DTL_NO'] == $sell['PICKUP_REQ_TIME']) ? "selected" : "";
                    print '<option value="' . $value['CD_DTL_NO'] . '"' . $opt . '>' . $value['CD_DTL_NM'] . '</option>';
                endforeach;
                echo '</select>';
            } else{
                foreach($tcode1041 as $value):
                    if($value['CD_DTL_NO'] == $sell['PICKUP_REQ_TIME']){
                        echo $value['CD_DTL_NM'];
                        break;
                    }
                endforeach;
            }
            ?>
        </td>
        <td><input type="text" value="<?php echo $sell['PICK_DATA_EXPT']; ?>" readonly></td>
        <td><input type="text" value="<?php echo Retsell_stat($sell['SELL_STAT']); ?>" readonly></td>
        <td><?php echo date("Y/m/d", strtotime($sell['REG_DM'])); ?></td>
        <!-- <td><?php echo getCorpName($sell['PICKUP_CORP_NO']); ?></td> -->
        <td><?php echo $PICKUP_CORP_NM_SHUKA; ?></td>
    </TR>
</table>

<!-- hidden values -->
<input type="hidden" name="value[SELL_NO]" value="<?php echo $sell['SELL_NO']; ?>">
<input type="hidden" name="value[PICK_DATA_EXPT]" value="1">
<input type="hidden" name="value[SELL_FORM_EXPT]" value="1">
<input type="hidden" name="value[SELL_STAT]" value="01">
<input type="hidden" name="value[PICKUP_CORP_NO]" value="<?php echo $PICKUP_CORP_NO_SAGAWA;?>">
<input type="hidden" name="value[BEFORE_PICKUP_REQ_DT]" value="<?php echo $sell['PICKUP_REQ_DT'];?>">
<input type="hidden" name="value[BEFORE_PICKUP_REQ_TIME]" value="<?php echo $sell['PICKUP_REQ_TIME'];?>">

<!-- 更新可否でボタンの有効/無効を切り替える -->
<?php if($ENABLE_UPDATE_FLG): ?>
    <p><INPUT TYPE="reset" VALUE="リセット">　<INPUT TYPE="submit" VALUE="再集荷を設定する"></p>
<?php else: ?>
        <p><INPUT TYPE="reset" VALUE="リセット">　<INPUT TYPE="button" VALUE="再集荷を設定する" disabled></p>
        <span>【以下の理由により再集荷依頼は行えません。】</span><br>
        <?php foreach($ERROR_MESSAGE as $msg):?>
            <font color="red">・<?php echo $msg; ?></font></br>
        <?php endforeach; ?>
<?php endif; ?>

</form>

<?php
//データの開放
$res->free();
$db->disconnect();
?>

<INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='recollect_search.php'">

</body>
</html>