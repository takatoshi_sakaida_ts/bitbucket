<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = trim(addslashes(@$_POST["TI"]));
$keyti2 = trim(addslashes(@$_POST["TI2"]));
$keyti3 = trim(addslashes(@$_POST["TI3"]));
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)){
	if (empty($keyti2)){
		print "検索条件を入力してください．<br>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
<?PHP
		die();
	}
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
$j="ng";
$sql="select m.MEM_NO,m.MEM_ID,m.mem_eml_mbl,m.mem_nm_fst||m.mem_nm_mid,m.use_yn,m.eml_yn,m.send_to,m.MEM_NM_LAST||m.MEM_NM_ETC,";
$sql=$sql."a.ZIP_CD,a.ZIP_ADDR1,a.ZIP_ADDR2,a.ZIP_ADDR3,a.DTL_ADDR,a.TEL_NO,m.SEX_TP,m.job,m.BIRTH_DT";
$sql=$sql." from tmember m,TMEMADDR a where m.mem_no=a.mem_no and ADDR_SEQ='0' and";
if (empty($keyti)){
	$sql=$sql." m.mem_no = '" . $keyti2 . "'";
}else
{
	if ($keyti3==0)
	//	$sql=$sql." (m.mem_id = '" . $keyti . "') or (m.mem_eml_mbl= '" . $keyti . "')";
	{
		$sql=$sql." m.mem_id = '" . $keyti . "'";}
	else
	{
		$sql=$sql." m.mem_eml_mbl = '" . $keyti . "'";
	}
}
//print $sql;
	print "<table border=0>\n";
	print "<tr>\n<td width=450><font size=+3>会員情報検索結果</font></td></TR>";
	print "</table>";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$j="ok";
	$mem_no=$row[0];
	$mem_id=$row[1];
	$mem_id2=$row[2];
	$mem_name=$row[3];
	$use_yn=Retuseyn($row[4]);
	$eml_yn=Retemlyn($row[5]);
	$sendto=Retemlsendto($row[6]);
	$mem_namekana=$row[7];
	$zipcd=Retzip($row[8]);
	$zipaddr1=$row[9];
	$zipaddr2=$row[10];
	$zipaddr3=$row[11];
	$zipaddr4=$row[12];
	$telno=$row[13];
	$sextp=Retsex($row[14]);
	$job=Retjob($row[15]);
	$birthdt=Rethiduke($row[16]);
}
if ($j!="ng")
{
$chk_flg="0";
//検索結果の表示
//基本情報
	print "<table border=1>\n";
	print "<tr>\n<td colspan=2 bgcolor=#CCFFCC><font size=+2>基本情報</font></td></TR>";
	//項目名の表示
	print "<tr>\n<td width=150>会員番号</td>";
	print "<td width=300>".$mem_no."</td></TR>";
	print "<tr>\n<td>会員ID</td>";
	print "<td>".$mem_id."　</td></TR>";
	print "<tr>\n<td>追加会員ID</td>";
	print "<td>".$mem_id2."　</td></TR>";
	print "<tr>\n<td>会員状態</td>";
	print "<td>".$use_yn."　</td></TR>";
	print "<tr>\n<td>メール配信先</td>";
	print "<td>".$sendto."　</td></TR>";
	print "<tr>\n<td>e-mail 受信可否</td>";
	print "<td>".$eml_yn."　</td></TR>";
	print "</table>";
	//名前・住所
	print "<BR><table border=1>\n";
	print "<tr>\n<td colspan=2 bgcolor=#CCFFCC><font size=+2>名前・住所</font></td></TR>";
	print "<tr>\n<td width=150>名前（漢字）</td>";
	print "<td width=300>".$mem_name."　</td></TR>";
	print "<tr>\n<td>名前（カナ）</td>";
	print "<td>".$mem_namekana."　</td></TR>";
	print "<tr>\n<td>郵便番号</td>";
	print "<td>".$zipcd."　</td></TR>";
	print "<tr>\n<td>都道府県</td>";
	print "<td>".$zipaddr1."　</td></TR>";
	print "<tr>\n<td>市区町村</td>";
	print "<td>".$zipaddr2."　</td></TR>";
	print "<tr>\n<td>番地</td>";
	print "<td>".$zipaddr3."　</td></TR>";
	print "<tr>\n<td>建物名・部屋番号</td>";
	print "<td>".$zipaddr4."　</td></TR>";
	print "<tr>\n<td>電話番号</td>";
	print "<td>".$telno."　</td>";
	print "</table>";
//その他
	print "<BR><table border=1>\n";
	print "<tr>\n<td colspan=2 bgcolor=#CCFFCC><font size=+2>その他</font></td></TR>";
	print "<tr>\n<td width=150>性別</td>";
	print "<td width=300>".$sextp."　</td></TR>";
	print "<tr>\n<td>職業</td>";
	print "<td>".$job."　</td></TR>";
	print "<tr>\n<td>生年月日</td>";
	print "<td>".$birthdt."　</td></TR>";
	print "</table>";

	//データの開放
	$res->free();

//注文情報
	print "<BR><BR><table border=0>\n";
	print "<tr>\n<td width=450><font size=+2>注文情報</font></td></TR>";
	print "</table>";

$sql="SELECT ORD_NO,ORD_CRT_TP,(TOT_INV_AMT-ORD_CNCL_AMT),ORD_STAT_CL,PAY_TP,MEMO FROM TORDER WHERE MEM_NO= '" . $mem_no . "' ORDER BY ORD_NO DESC";

//print $sql;
$res2 = $db->query($sql);
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$ord_no="none";
$i=0;

while($row2 = $res2->fetchRow()){
	$j="ok";
	$i++;
	if ($i==1)
	{
		print "<table border=1>\n";
		print "<td>　</td>";
		print "<td width=150>注文番号</td>";
		print "<td width=80>申込区分</td>";
		print "<td width=80>金額</td>";
		print "<td width=80>ステータス</td>";
		print "<td width=80>支払方法</td>";
		print "<td width=300>メモ</td></TR>";
	}
	print "<TR><td>".$i."　</td>";
//	print "<td>".$row2[0]."　</td>";
	print "<td><a href='../sorder/detailorder.php?receptioncode=" .$row2[0] . "'>".$row2[0] ."</td>";
	print "<td>".RetORDCRTTP($row2[1])."　</td>";
	print "<td>".$row2[2]."　</td>";
	print "<td>".RetORDSTSCL($row2[3])."　</td>";
	print "<td>".Retpaytp($row2[4])."　</td>";
	print "<td WIDTH=600>".$row2[5]."　</td></TR>";
	$ord_no="ari";
}

if ($ord_no=="none")
{
	print "　　注文受付は存在しません";
}else
{
	print "</table>";
}
	//データの開放
	$res2->free();

//注文情報
	print "<BR><BR><table border=0>\n";
	print "<tr>\n<td width=450><font size=+2>買取情報</font></td></TR>";
	print "</table>";

$sql="SELECT T.SELL_NO,SELL_ROUTE_TP,TOT_ASS_AMT,SELL_STAT,VALID_ITEM_CNT,MEMO FROM TSELL T,TSELLASSESSMENT M WHERE T.SELL_NO=M.SELL_NO AND T.MEM_NO= '" . $mem_no . "' ORDER BY T.REG_DM DESC";

//print $sql;
$res3 = $db->query($sql);
if(DB::isError($res3)){
	$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$ord_no="none";
$i=0;

while($row3 = $res3->fetchRow()){
	$j="ok";
	$i++;
	if ($i==1)
	{
		print "<table border=1>\n";
		print "<td>　</td>";
		print "<td width=150>買取番号</td>";
		print "<td width=80>申込区分</td>";
		print "<td width=80>金額</td>";
		print "<td width=80>ステータス</td>";
		print "<td width=80>点数</td>";
		print "<td width=600>メモ</td></TR>";
	}
	print "<TR><td>".$i."　</td>";
	print "<td><a href='../skaitori/detailkaitori.php?receptioncode=" .substr_replace($row3[0],"000",0,1) ."'>".$row3[0] ."</td>";
	print "<td>".RetSELLROUTETP($row3[1])."　</td>";
	print "<td>".$row3[2]."　</td>";
	print "<td>".Retsell_stat($row3[3])."　</td>";
	print "<td>".$row3[4]."　</td>";
	print "<td>".$row3[5]."　</td></TR>";
	$ord_no="ari";
}

if ($ord_no=="none")
{
	print "　　買取受付は存在しません";
}else
{
	print "</table>";
}
	//データの開放
	$res3->free();
	$db->disconnect();
}
else
{
	$chk_flg="1";
	print "該当の会員は存在しません。" .$keyti  ;
}
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>