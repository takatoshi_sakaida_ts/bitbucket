<?PHP
function db_insert(){
//PEARの利用     -------(1)
require_once("DB.php");
require_once("selectvalue_volteikei.php");
// 銀行情報のセット
/* 2019.05.27 ブックラPhase2対応 MOD START */
//require_once("volteikei_bank.php");
$_SESSION["HSMAP"]["BANK_CD"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_CD"];
$_SESSION["HSMAP"]["BANK_BRANCH_CD"]= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_BRANCH_CD"];
$_SESSION["HSMAP"]["BANK_NM"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_NM"];
$_SESSION["HSMAP"]["BRANCH_NM"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BRANCH_NM"];
$_SESSION["HSMAP"]["KOUZASHURUI"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZASHURUI"];
$_SESSION["HSMAP"]["KOUZABANGOU"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZABANGOU"];
$_SESSION["HSMAP"]["KOUZAMEIGI"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZAMEIGI"];
$sellno_head = $_SESSION["HSMAP"]["DANTAI"];
/* 2019.05.27 ブックラPhase2対応 MOD END */
//ログイン情報の読み込み
require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//買取受付番号の決定
/* 2019.05.27 ブックラPhase2対応 MOD START */
$sql="select max(sell_no) from tsell where sell_no like '" . $sellno_head . "%'";
/* 2019.05.27 ブックラPhase2対応 MOD END */
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
/* 2019.05.27 ブックラPhase2対応 MOD START */
$sell_no = 0;
while($row = $res->fetchRow()){
	 if(isset($row[0]) && $row[0] != null){
    	$sell_no=substr($row[0],5,8);
    }
//	print $row[0]."<BR>";
}
/* 2019.05.27 ブックラPhase2対応 MOD END */
$res->free();
//print "sell_noは".$sell_no."<br>";
$sell_no=$sell_no + 1;
//print "sell_no(+1)は".$sell_no."<br>";
$sell_no = sprintf("%08d",$sell_no);
/* 2019.05.27 ブックラPhase2対応 MOD START */
$sell_no = $sellno_head.$sell_no;
/* 2019.05.27 ブックラPhase2対応 MOD END */
$_SESSION["SELL_FORM_GET_DT"]="";
//
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='';
}else
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='000000000000000';
}

/* 2019.05.20 ブックラPhase2対応 MOD START */
//フロムヴイ
//の場合は自動振込保留
//if ($sellno_head=='30')
//{
//	$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='1';
//}else{
//	$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='0';
//}
/* 2019.05.20 ブックラPhase2対応 MOD END */

/**
2011/8 新本人確認のため申込書自動チェックなし
//ガイド発送が不要なら申込書自動到着
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
}else{
//スターツピタットハウス・インターファーム・クラブパナソニックの場合は申込書の自動チェックはしない
	if ($sellno_head=='01'|| $sellno_head=='06'|| $sellno_head=='08')
	{
		$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
	}else{
		$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
		$_SESSION["SELL_FORM_GET_DT"]=date("Ymd");
	}
}
*/
$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';

/* 2019.05.20 ブックラPhase2対応 MOD START */
//申込書の自動チェック
//フロムヴイ、インターネットコンシェルジュサービス、住宅情報館(城南建設)
//if ($sellno_head=='30'||$sellno_head=='34')
//{
//	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
//	$_SESSION["SELL_FORM_GET_DT"]=date("Ymd");
//}
/* 2019.05.20 ブックラPhase2対応 MOD END */

/* 2019.05.20 ブックラPhase2対応 MOD START */
//大京アステージ・三井不動産住宅サービスは集荷しない
//if ($sellno_head=='02'||$sellno_head=='04')
//三井不動産住宅サービスは集荷しない
//if ($sellno_head=='04')
//{
//	$_SESSION["HSMAP"]["PICKUP_REQ_DTY"]=date("Y");
//	$_SESSION["HSMAP"]["PICKUP_REQ_DTM"]=date("m");
//	$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]=date("d");
//	$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]="1";
//	$_SESSION["HSMAP"]["PICK_DATA_EXPT"]="3";
//}else
//{
	$_SESSION["HSMAP"]["PICK_DATA_EXPT"]="1";
//}
/* 2019.05.20 ブックラPhase2対応 MOD END */

/* 2019.05.20 ブックラPhase2対応 MOD START */
$PAY_TP = "";
// 代金受取方法種別
// フロムヴイは銀行振込
//if ($sellno_head=='30')
//{
//	$PAY_TP = "1";
//}
/* 2019.05.20 ブックラPhase2対応 MOD END */

//print $sell_no."<BR>";
/* 2019.05.20 ブックラPhase2対応 MOD START */
$sql="INSERT INTO tsell(".
"SELL_NO,".
"SELL_STAT,".
"PICKUP_NM_FST,".
"PICKUP_NM_MID,".
"PICKUP_NM_LAST,".
"PICKUP_NM_ETC,".
"PICKUP_ZIP_CD,".
"PICKUP_ZIP_ADDR1,".
"PICKUP_ZIP_ADDR2,".
"PICKUP_ZIP_ADDR3,".
"PICKUP_DTL_ADDR,".
"PICKUP_TEL_NO,".
"PICKUP_TEL_NO_2,".
"PICKUP_REQ_DT,".
"PICKUP_REQ_TIME,".
"BOX,".
"NOTE_YN,".
"DISPOSE,".
"RECEIPT_TP,".
"MEM_BIRTH_DT,".
"MEM_SEX,".
"MEM_JOB,".
"PAY_TP,".
"DL_STAT_LABEL,".
"BANK_CD,".
"BANK_BRANCH_CD,".
"BANK_NM,".
"BRANCH_NM,".
"BANK_ACC_TP,".
"BANK_ACC_NO,".
"BANK_ACC_NM,".
"MEMO,".
"FORM_ARRIVAL,".
"PICK_DATA_EXPT,".
"SELL_FORM_EXPT,".
"ADMIT_DATA_EXPT,".
"ASSES_CNT,".
"REG_DM,".
"UPD_DM,".
"PAY_HOLD_FLG,".
"AUTH_SEND_FLG,".
"SELL_FORM_GET_DT,".
"USER_ID".
") VALUES('"
	.$sell_no."','"
	."01','"
	.$_SESSION["HSMAP"]["PICKUP_NM_FST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_MID"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_LAST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_ETC"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_REQ_DTY"].$_SESSION["HSMAP"]["PICKUP_REQ_DTM"].$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]."','"
	."0".$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]."','"
	.$_SESSION["HSMAP"]["BOX"]."','"
	.($_SESSION["HSMAP"]["TUUTI"]-1)."','"
	.($_SESSION["HSMAP"]["DTAIOU"]-1)."','"
	."2','"
	.$_SESSION["HSMAP"]["PICKUP_BIRTH_DTY"].$_SESSION["HSMAP"]["PICKUP_BIRTH_DTM"].$_SESSION["HSMAP"]["PICKUP_BIRTH_DTD"]."','"
	.$_SESSION["HSMAP"]["MEM_SEX"]."','"
	."09','"
	.$PAY_TP."','"
	.$_SESSION["HSMAP"]["DL_STAT_LABEL"]."','"
	.$_SESSION["HSMAP"]["BANK_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_BRANCH_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_NM"]."','"
	.$_SESSION["HSMAP"]["BRANCH_NM"]."','"
	.$_SESSION["HSMAP"]["KOUZASHURUI"]."','"
	.$_SESSION["HSMAP"]["KOUZABANGOU"]."','"
	.$_SESSION["HSMAP"]["KOUZAMEIGI"]."','"
	.$_SESSION["HSMAP"]["MEMO"]. ":提携企業名：".str_replace("'","",$_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["PARTNER_NM"]). "　ガイド発送:" . Retgaido($_SESSION["HSMAP"]["gaido"]) . "','"
	.$_SESSION["HSMAP"]["FORM_ARRIVAL"]."','"
	.$_SESSION["HSMAP"]["PICK_DATA_EXPT"]."','"
	."1','"
	."1','"
	."0','"
	.date("YmdHis")."','"
	.date("YmdHis")."','"
	.$_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["PAY_HOLD_FLG"]."','"
	."0','"
	.$_SESSION["SELL_FORM_GET_DT"]."','"
	.$_SESSION["user_id"]."')";
/* 2019.05.20 ブックラPhase2対応 MOD END */
//print "<br>".$sql;
$db->autoCommit( false ); 
$res2 = $db->query($sql);
if(DB::isError($res2)){
	print $res2->getMessage();
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//
output_log($sql,$_SESSION["user_id"],'vol_entry_sql');
$sql="INSERT INTO tsellassessment (".
"SELL_NO,".
"REG_DM".
") VALUES('"
	.$sell_no."','"
	.date("YmdHis")."')";

$res3 = $db->query($sql);
if(DB::isError($res3)){
	print $res3->getMessage();
	$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}


		$_SESSION["HSMAP"]["sell_no"]=$sell_no;
$db->commit();
output_log($sell_no,$_SESSION["user_id"],'volteikei_entry');
output_log($sql,$_SESSION["user_id"],'volteikei_entry_sql');
//print "<br>".$sql;
//データの開放
$db->disconnect();
}
?>