<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ピッキング作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$nowtime= date("H");
$sql = "SELECT PICKTIME,PICKMINUTE,COUNT(*),ZONECODE,NEWUSED_KIND ".
"FROM ( ".
"SELECT ".
"SUBSTR(D.OPERATETIME,1,2) AS PICKTIME, ".
"CASE WHEN SUBSTR(D.OPERATETIME,3,2)<31 THEN 0 ".
"ELSE 1  ".
"END AS PICKMINUTE,M.ZONECODE,D.NEWUSED_KIND ".
"FROM D_RECEPTION_GOODS D,M_LOCATION M ".
"WHERE D.PICKED_KIND='1' AND D.CANCEL_KIND='0' AND D.LOCATIONCODE=M.LOCATIONCODE ".
"AND D.OPERATEDATE=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) ".
") ".
"GROUP BY PICKTIME,PICKMINUTE,ZONECODE,NEWUSED_KIND ".
"ORDER BY PICKTIME,PICKMINUTE,ZONECODE";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>本日のピッキング作業状況</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td align=center rowspan=2>ピッキング時間</td>";
print "<td align=center colspan=15>中古</td><td align=center colspan=15>新古</td>";
print "<td width=40 align=center rowspan=2>新品</td><td align=center rowspan=2>時間帯合計</td>";
print "</tr>";
print "<tr bgcolor=#ccffff>\n";
print "<td width=40>Ａ</td><td width=40>Ｂ</td><td width=40>Ｃ</td><td width=40>Ｄ</td><td width=40>Ｇ</td>";
print "<td width=40>Ｈ</td><td width=40>Ｉ</td><td width=40>Ｊ</td><td width=40>Ｋ</td><td width=40>Ｌ</td>";
print "<td width=40>Ｍ</td><td width=40>Ｎ</td><td width=40>Ｏ</td><td width=40>Ｐ</td><td width=40>Ｑ</td>";
print "<td width=40>Ａ</td><td width=40>Ｂ</td><td width=40>Ｃ</td><td width=40>Ｄ</td><td width=40>Ｇ</td>";
print "<td width=40>Ｈ</td><td width=40>Ｉ</td><td width=40>Ｊ</td><td width=40>Ｋ</td><td width=40>Ｌ</td>";
print "<td width=40>Ｍ</td><td width=40>Ｎ</td><td width=40>Ｏ</td><td width=40>Ｐ</td><td width=40>Ｑ</td>";
print "</tr>";
//初期値設定
$i=0;
for ($c = 7; $c <= 23; $c++) {
//中古
	$forhtml_a[$c][0][0]=0;
	$forhtml_a[$c][1][0]=0;
	$forhtml_b[$c][0][0]=0;
	$forhtml_b[$c][1][0]=0;
	$forhtml_c[$c][0][0]=0;
	$forhtml_c[$c][1][0]=0;
	$forhtml_d[$c][0][0]=0;
	$forhtml_d[$c][1][0]=0;
	$forhtml_e[$c][0][0]=0;
	$forhtml_e[$c][1][0]=0;
	$forhtml_g[$c][0][0]=0;
	$forhtml_g[$c][1][0]=0;
	$forhtml_h[$c][0][0]=0;
	$forhtml_h[$c][1][0]=0;
	$forhtml_i[$c][0][0]=0;
	$forhtml_i[$c][1][0]=0;
	$forhtml_j[$c][0][0]=0;
	$forhtml_j[$c][1][0]=0;
	$forhtml_k[$c][0][0]=0;
	$forhtml_k[$c][1][0]=0;
	$forhtml_l[$c][0][0]=0;
	$forhtml_l[$c][1][0]=0;
	$forhtml_m[$c][0][0]=0;
	$forhtml_m[$c][1][0]=0;
	$forhtml_n[$c][0][0]=0;
	$forhtml_n[$c][1][0]=0;
	$forhtml_o[$c][0][0]=0;
	$forhtml_o[$c][1][0]=0;
	$forhtml_p[$c][0][0]=0;
	$forhtml_p[$c][1][0]=0;
	$forhtml_q[$c][0][0]=0;
	$forhtml_q[$c][1][0]=0;
//新古
	$forhtml_a[$c][0][1]=0;
	$forhtml_a[$c][1][1]=0;
	$forhtml_b[$c][0][1]=0;
	$forhtml_b[$c][1][1]=0;
	$forhtml_c[$c][0][1]=0;
	$forhtml_c[$c][1][1]=0;
	$forhtml_d[$c][0][1]=0;
	$forhtml_d[$c][1][1]=0;
	$forhtml_e[$c][0][1]=0;
	$forhtml_e[$c][1][1]=0;
	$forhtml_g[$c][0][1]=0;
	$forhtml_g[$c][1][1]=0;
	$forhtml_h[$c][0][1]=0;
	$forhtml_h[$c][1][1]=0;
	$forhtml_i[$c][0][1]=0;
	$forhtml_i[$c][1][1]=0;
	$forhtml_j[$c][0][1]=0;
	$forhtml_j[$c][1][1]=0;
	$forhtml_k[$c][0][1]=0;
	$forhtml_k[$c][1][1]=0;
	$forhtml_l[$c][0][1]=0;
	$forhtml_l[$c][1][1]=0;
	$forhtml_m[$c][0][1]=0;
	$forhtml_m[$c][1][1]=0;
	$forhtml_n[$c][0][1]=0;
	$forhtml_n[$c][1][1]=0;
	$forhtml_o[$c][0][1]=0;
	$forhtml_o[$c][1][1]=0;
	$forhtml_p[$c][0][1]=0;
	$forhtml_p[$c][1][1]=0;
	$forhtml_q[$c][0][1]=0;
	$forhtml_q[$c][1][1]=0;
//新品
	$forhtml_a[$c][0][2]=0;
	$forhtml_a[$c][1][2]=0;
	$forhtml_b[$c][0][2]=0;
	$forhtml_b[$c][1][2]=0;
	$forhtml_c[$c][0][2]=0;
	$forhtml_c[$c][1][2]=0;
	$forhtml_d[$c][0][2]=0;
	$forhtml_d[$c][1][2]=0;
	$forhtml_e[$c][0][2]=0;
	$forhtml_e[$c][1][2]=0;
	$forhtml_g[$c][0][2]=0;
	$forhtml_g[$c][1][2]=0;
	$forhtml_h[$c][0][2]=0;
	$forhtml_h[$c][1][2]=0;
	$forhtml_i[$c][0][2]=0;
	$forhtml_i[$c][1][2]=0;
	$forhtml_j[$c][0][2]=0;
	$forhtml_j[$c][1][2]=0;
	$forhtml_k[$c][0][2]=0;
	$forhtml_k[$c][1][2]=0;
	$forhtml_l[$c][0][2]=0;
	$forhtml_l[$c][1][2]=0;
	$forhtml_m[$c][0][2]=0;
	$forhtml_m[$c][1][2]=0;
	$forhtml_n[$c][0][2]=0;
	$forhtml_n[$c][1][2]=0;
	$forhtml_o[$c][0][2]=0;
	$forhtml_o[$c][1][2]=0;
	$forhtml_p[$c][0][2]=0;
	$forhtml_p[$c][1][2]=0;
	$forhtml_q[$c][0][2]=0;
	$forhtml_q[$c][1][2]=0;
//total
	$forhtmlt_a=0;
	$forhtmlt_b=0;
	$forhtmlt_c=0;
	$forhtmlt_d=0;
	$forhtmlt_e=0;
	$forhtmlt_g=0;
	$forhtmlt_h=0;
	$forhtmlt_i=0;
	$forhtmlt_j=0;
	$forhtmlt_k=0;
	$forhtmlt_l=0;
	$forhtmlt_m=0;
	$forhtmlt_n=0;
	$forhtmlt_o=0;
	$forhtmlt_p=0;
	$forhtmlt_q=0;
	$forhtml[$c][0]=0;
	$forhtml[$c][1]=0;
}
$forhtmltn_a=0;
$forhtmltn_b=0;
$forhtmltn_c=0;
$forhtmltn_d=0;
$forhtmltn_g=0;
$forhtmltn_h=0;
$forhtmltn_i=0;
$forhtmltn_j=0;
$forhtmltn_k=0;
$forhtmltn_l=0;
$forhtmltn_m=0;
$forhtmltn_n=0;
$forhtmltn_o=0;
$forhtmltn_p=0;
$forhtmltn_q=0;
$forhtml_sinpin=0;
$forhtml_sinko=0;
//取得データのセット
while($row = $res->fetchRow()){
	switch ($row[3]){
//数値に変換するために+0してます
	case "A";
		$forhtml_a[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "B";
		$forhtml_b[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "C";
		$forhtml_c[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "D";
		$forhtml_d[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "E";
		$forhtml_e[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "G";
		$forhtml_g[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "H";
		$forhtml_h[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "I";
		$forhtml_i[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "J";
		$forhtml_j[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "K";
		$forhtml_k[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "L";
		$forhtml_l[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "M";
		$forhtml_m[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "N";
		$forhtml_n[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "O";
		$forhtml_o[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "P";
		$forhtml_p[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	case "Q";
		$forhtml_q[($row[0]+0)][($row[1]+0)][($row[4]+0)]=$row[2];
		break;
	}
	$forhtml[($row[0]+0)][($row[1]+0)]=$forhtml[($row[0]+0)][($row[1]+0)]+$row[2];
	$i=$i+$row[2];
}
for ($c = 7; $c <= $nowtime; $c++) {
	print "<tr>";
	print "<td>　".$c."時00分から30分　</td>";
	print "<td>".$forhtml_a[$c][0][0]."</td>";
	print "<td>".$forhtml_b[$c][0][0]."</td>";
	print "<td>".$forhtml_c[$c][0][0]."</td>";
	print "<td>".$forhtml_d[$c][0][0]."</td>";
	print "<td>".$forhtml_g[$c][0][0]."</td>";
	print "<td>".$forhtml_h[$c][0][0]."</td>";
	print "<td>".$forhtml_i[$c][0][0]."</td>";
	print "<td>".$forhtml_j[$c][0][0]."</td>";
	print "<td>".$forhtml_k[$c][0][0]."</td>";
	print "<td>".$forhtml_l[$c][0][0]."</td>";
	print "<td>".$forhtml_m[$c][0][0]."</td>";
	print "<td>".$forhtml_n[$c][0][0]."</td>";
	print "<td>".$forhtml_o[$c][0][0]."</td>";
	print "<td>".$forhtml_p[$c][0][0]."</td>";
	print "<td>".$forhtml_q[$c][0][0]."</td>";
	print "<td>".$forhtml_a[$c][0][1]."</td>";
	print "<td>".$forhtml_b[$c][0][1]."</td>";
	print "<td>".$forhtml_c[$c][0][1]."</td>";
	print "<td>".$forhtml_d[$c][0][1]."</td>";
	print "<td>".$forhtml_g[$c][0][1]."</td>";
	print "<td>".$forhtml_h[$c][0][1]."</td>";
	print "<td>".$forhtml_i[$c][0][1]."</td>";
	print "<td>".$forhtml_j[$c][0][1]."</td>";
	print "<td>".$forhtml_k[$c][0][1]."</td>";
	print "<td>".$forhtml_l[$c][0][1]."</td>";
	print "<td>".$forhtml_m[$c][0][1]."</td>";
	print "<td>".$forhtml_n[$c][0][1]."</td>";
	print "<td>".$forhtml_o[$c][0][1]."</td>";
	print "<td>".$forhtml_p[$c][0][1]."</td>";
	print "<td>".$forhtml_q[$c][0][1]."</td>";
	print "<td>".($forhtml_a[$c][0][2]+$forhtml_b[$c][0][2]+$forhtml_c[$c][0][2]+$forhtml_d[$c][0][2]+$forhtml_e[$c][0][2]+$forhtml_g[$c][0][2]+$forhtml_h[$c][0][2]+$forhtml_i[$c][0][2]+$forhtml_j[$c][0][2]+$forhtml_k[$c][0][2]+$forhtml_l[$c][0][2]+$forhtml_m[$c][0][2]+$forhtml_n[$c][0][2]+$forhtml_o[$c][0][2]+$forhtml_p[$c][0][2]+$forhtml_q[$c][0][2])."</td>";
	print "<td align=right>".$forhtml[$c][0]."</td>";
	print "</tr>";
	print "<tr>";
	print "<td>　".$c."時31分から60分　</td>";
	print "<td>".$forhtml_a[$c][1][0]."</td>";
	print "<td>".$forhtml_b[$c][1][0]."</td>";
	print "<td>".$forhtml_c[$c][1][0]."</td>";
	print "<td>".$forhtml_d[$c][1][0]."</td>";
	print "<td>".$forhtml_g[$c][1][0]."</td>";
	print "<td>".$forhtml_h[$c][1][0]."</td>";
	print "<td>".$forhtml_i[$c][1][0]."</td>";
	print "<td>".$forhtml_j[$c][1][0]."</td>";
	print "<td>".$forhtml_k[$c][1][0]."</td>";
	print "<td>".$forhtml_l[$c][1][0]."</td>";
	print "<td>".$forhtml_m[$c][1][0]."</td>";
	print "<td>".$forhtml_n[$c][1][0]."</td>";
	print "<td>".$forhtml_o[$c][1][0]."</td>";
	print "<td>".$forhtml_p[$c][1][0]."</td>";
	print "<td>".$forhtml_q[$c][1][0]."</td>";
	print "<td>".$forhtml_a[$c][1][1]."</td>";
	print "<td>".$forhtml_b[$c][1][1]."</td>";
	print "<td>".$forhtml_c[$c][1][1]."</td>";
	print "<td>".$forhtml_d[$c][1][1]."</td>";
	print "<td>".$forhtml_g[$c][1][1]."</td>";
	print "<td>".$forhtml_h[$c][1][1]."</td>";
	print "<td>".$forhtml_i[$c][1][1]."</td>";
	print "<td>".$forhtml_j[$c][1][1]."</td>";
	print "<td>".$forhtml_k[$c][1][1]."</td>";
	print "<td>".$forhtml_l[$c][1][1]."</td>";
	print "<td>".$forhtml_m[$c][1][1]."</td>";
	print "<td>".$forhtml_n[$c][1][1]."</td>";
	print "<td>".$forhtml_o[$c][1][1]."</td>";
	print "<td>".$forhtml_p[$c][1][1]."</td>";
	print "<td>".$forhtml_q[$c][1][1]."</td>";
	print "<td>".($forhtml_a[$c][1][2]+$forhtml_b[$c][1][2]+$forhtml_c[$c][1][2]+$forhtml_d[$c][1][2]+$forhtml_e[$c][1][2]+$forhtml_g[$c][1][2]+$forhtml_h[$c][1][2]+$forhtml_i[$c][1][2]+$forhtml_j[$c][1][2]+$forhtml_k[$c][1][2]+$forhtml_l[$c][1][2]+$forhtml_m[$c][1][2]+$forhtml_n[$c][1][2]+$forhtml_o[$c][1][2]+$forhtml_p[$c][1][2]+$forhtml_q[$c][1][2])."</td>";
	print "<td align=right>".$forhtml[$c][1]."</td>";
	print "</tr>";
	$forhtmlt_a=$forhtmlt_a+$forhtml_a[$c][0][0]+$forhtml_a[$c][1][0];
	$forhtmlt_b=$forhtmlt_b+$forhtml_b[$c][0][0]+$forhtml_b[$c][1][0];
	$forhtmlt_c=$forhtmlt_c+$forhtml_c[$c][0][0]+$forhtml_c[$c][1][0];
	$forhtmlt_d=$forhtmlt_d+$forhtml_d[$c][0][0]+$forhtml_d[$c][1][0];
	$forhtmlt_e=$forhtmlt_e+$forhtml_e[$c][0][0]+$forhtml_e[$c][1][0];
	$forhtmlt_g=$forhtmlt_g+$forhtml_g[$c][0][0]+$forhtml_g[$c][1][0];
	$forhtmlt_h=$forhtmlt_h+$forhtml_h[$c][0][0]+$forhtml_h[$c][1][0];
	$forhtmlt_i=$forhtmlt_i+$forhtml_i[$c][0][0]+$forhtml_i[$c][1][0];
	$forhtmlt_j=$forhtmlt_j+$forhtml_j[$c][0][0]+$forhtml_j[$c][1][0];
	$forhtmlt_k=$forhtmlt_k+$forhtml_k[$c][0][0]+$forhtml_k[$c][1][0];
	$forhtmlt_l=$forhtmlt_l+$forhtml_l[$c][0][0]+$forhtml_l[$c][1][0];
	$forhtmlt_m=$forhtmlt_m+$forhtml_m[$c][0][0]+$forhtml_m[$c][1][0];
	$forhtmlt_n=$forhtmlt_n+$forhtml_n[$c][0][0]+$forhtml_n[$c][1][0];
	$forhtmlt_o=$forhtmlt_o+$forhtml_o[$c][0][0]+$forhtml_o[$c][1][0];
	$forhtmlt_p=$forhtmlt_p+$forhtml_p[$c][0][0]+$forhtml_p[$c][1][0];
	$forhtmlt_q=$forhtmlt_q+$forhtml_q[$c][0][0]+$forhtml_q[$c][1][0];
	$forhtmltn_a=$forhtmltn_a+$forhtml_a[$c][0][1]+$forhtml_a[$c][1][1];
	$forhtmltn_b=$forhtmltn_b+$forhtml_b[$c][0][1]+$forhtml_b[$c][1][1];
	$forhtmltn_c=$forhtmltn_c+$forhtml_c[$c][0][1]+$forhtml_c[$c][1][1];
	$forhtmltn_d=$forhtmltn_d+$forhtml_d[$c][0][1]+$forhtml_d[$c][1][1];
	$forhtmltn_g=$forhtmltn_g+$forhtml_g[$c][0][1]+$forhtml_g[$c][1][1];
	$forhtmltn_h=$forhtmltn_h+$forhtml_h[$c][0][1]+$forhtml_h[$c][1][1];
	$forhtmltn_i=$forhtmltn_i+$forhtml_i[$c][0][1]+$forhtml_i[$c][1][1];
	$forhtmltn_j=$forhtmltn_j+$forhtml_j[$c][0][1]+$forhtml_j[$c][1][1];
	$forhtmltn_k=$forhtmltn_k+$forhtml_k[$c][0][1]+$forhtml_k[$c][1][1];
	$forhtmltn_l=$forhtmltn_l+$forhtml_l[$c][0][1]+$forhtml_l[$c][1][1];
	$forhtmltn_m=$forhtmltn_m+$forhtml_m[$c][0][1]+$forhtml_m[$c][1][1];
	$forhtmltn_n=$forhtmltn_n+$forhtml_n[$c][0][1]+$forhtml_n[$c][1][1];
	$forhtmltn_o=$forhtmltn_o+$forhtml_o[$c][0][1]+$forhtml_o[$c][1][1];
	$forhtmltn_p=$forhtmltn_p+$forhtml_p[$c][0][1]+$forhtml_p[$c][1][1];
	$forhtmltn_q=$forhtmltn_q+$forhtml_q[$c][0][1]+$forhtml_q[$c][1][1];
	$forhtml_sinpin = $forhtml_sinpin + $forhtml_a[$c][0][2] + $forhtml_b[$c][0][2] + $forhtml_c[$c][0][2] + $forhtml_d[$c][0][2] + $forhtml_e[$c][0][2] + $forhtml_g[$c][0][2];
	$forhtml_sinpin = $forhtml_sinpin + $forhtml_h[$c][0][2] + $forhtml_i[$c][0][2] + $forhtml_j[$c][0][2] + $forhtml_k[$c][0][2] + $forhtml_l[$c][0][2];
	$forhtml_sinpin = $forhtml_sinpin + $forhtml_m[$c][0][2] + $forhtml_n[$c][0][2] + $forhtml_o[$c][0][2] + $forhtml_p[$c][0][2] + $forhtml_q[$c][0][2];
	$forhtml_sinpin = $forhtml_sinpin + $forhtml_a[$c][1][2] + $forhtml_b[$c][1][2] + $forhtml_c[$c][1][2] + $forhtml_d[$c][1][2] + $forhtml_e[$c][1][2] + $forhtml_g[$c][1][2];
	$forhtml_sinpin = $forhtml_sinpin + $forhtml_h[$c][1][2] + $forhtml_i[$c][1][2] + $forhtml_j[$c][1][2] + $forhtml_k[$c][1][2] + $forhtml_l[$c][1][2];
	$forhtml_sinpin = $forhtml_sinpin + $forhtml_m[$c][1][2] + $forhtml_n[$c][1][2] + $forhtml_o[$c][1][2] + $forhtml_p[$c][1][2] + $forhtml_q[$c][1][2];
	$forhtml_sinko  = $forhtml_sinko  + $forhtml_a[$c][0][1] + $forhtml_b[$c][0][1] + $forhtml_c[$c][0][1] + $forhtml_d[$c][0][1] + $forhtml_e[$c][0][1] + $forhtml_g[$c][0][1];
	$forhtml_sinko  = $forhtml_sinko  + $forhtml_a[$c][1][1] + $forhtml_b[$c][1][1] + $forhtml_c[$c][1][1] + $forhtml_d[$c][1][1] + $forhtml_e[$c][1][1] + $forhtml_g[$c][1][1];
}
print "<tr><td>ゾーン合計</td>";
print "<td>".$forhtmlt_a."</TD>";
print "<td>".$forhtmlt_b."</TD>";
print "<td>".$forhtmlt_c."</TD>";
print "<td>".$forhtmlt_d."</TD>";
print "<td>".$forhtmlt_g."</TD>";
print "<td>".$forhtmlt_h."</TD>";
print "<td>".$forhtmlt_i."</TD>";
print "<td>".$forhtmlt_j."</TD>";
print "<td>".$forhtmlt_k."</TD>";
print "<td>".$forhtmlt_l."</TD>";
print "<td>".$forhtmlt_m."</TD>";
print "<td>".$forhtmlt_n."</TD>";
print "<td>".$forhtmlt_o."</TD>";
print "<td>".$forhtmlt_p."</TD>";
print "<td>".$forhtmlt_q."</TD>";
print "<td>".$forhtmltn_a."</TD>";
print "<td>".$forhtmltn_b."</TD>";
print "<td>".$forhtmltn_c."</TD>";
print "<td>".$forhtmltn_d."</TD>";
print "<td>".$forhtmltn_g."</TD>";
print "<td>".$forhtmltn_h."</TD>";
print "<td>".$forhtmltn_i."</TD>";
print "<td>".$forhtmltn_j."</TD>";
print "<td>".$forhtmltn_k."</TD>";
print "<td>".$forhtmltn_l."</TD>";
print "<td>".$forhtmltn_m."</TD>";
print "<td>".$forhtmltn_n."</TD>";
print "<td>".$forhtmltn_o."</TD>";
print "<td>".$forhtmltn_p."</TD>";
print "<td>".$forhtmltn_q."</TD>";
print "<td>".$forhtml_sinpin."</TD><TD>　</TD></TR>";
print "<tr><td>合計</td><td colspan=32 align=right>　".$i."　</td></tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>