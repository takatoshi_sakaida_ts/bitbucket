<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ピッキング実施可能状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
// 2014/1/1以前のデータはピッキング道場などで使用している注文もあるので除外
// 点数取得
$sql = 
"select  l.zonecode, r.receptioncode from d_reception r, d_reception_goods g, m_location l ".
"where r.receptioncode = g.receptioncode ".
"and g.locationcode = l.locationcode ".
"and r.receptiondate >= '20140101' ".
"and r.status_kind = '01' ".
"and g.picked_kind = '0' ".
"and g.cancel_kind = '0' ".
"and l.zonecode != 'Z' ".
"order by r.receptioncode, l.zonecode";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>ピッキング実施可能状況</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width= 80 align=center rowspan=2>ゾーン</td>
	<td width=100 align=center rowspan=1 colspan=2>実施可能</td>
	<td width=150 align=center rowspan=1>前ゾーン未実施</td>
	<td width= 80 align=center rowspan=2>点数合計</td>
</tr>
<tr bgcolor=#ccffff>
	<td width=100 align=center colspan=1>件数</td>
	<td width=100 align=center colspan=1>点数</td>
	<td width=150 align=center colspan=1>点数</td>
</tr>
<?php

$lastReceptioncode = "";
$lastZonecode = "";
$setFlug = 1;
$zoneNum = 16;

for($i = 0; $i < $zoneNum; $i++){
	$data[$i][0] = 0;
	$data[$i][1] = 0;
	$data[$i][2] = 0;
}

//検索結果の表示
//取得データのセット
while($row = $res->fetchRow()){
	if((strcmp($lastReceptioncode, $row[1]) != 0) ||
	   (strcmp($lastReceptioncode, $row[1]) == 0 && strcmp($lastZonecode, $row[0]) == 0 && $setFlug == 1)){
		$setFlug = 1;
		switch( $row[0] ){
		case "A":
			$data[0][0] = $data[0][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[0][1] = $data[0][1] + 1;
			}
			break;
		case "B":
			$data[1][0] = $data[1][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[1][1] = $data[1][1] + 1;
			}
			break;
		case "C":
			$data[2][0] = $data[2][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[2][1] = $data[2][1] + 1;
			}
			break;
		case "D":
			$data[3][0] = $data[3][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[3][1] = $data[3][1] + 1;
			}
			break;
		case "E":
			$data[4][0] = $data[4][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[4][1] = $data[4][1] + 1;
			}
			break;
		case "G":
			$data[5][0] = $data[5][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[5][1] = $data[5][1] + 1;
			}
			break;
		case "H":
			$data[6][0] = $data[6][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[6][1] = $data[6][1] + 1;
			}
			break;
		case "I":
			$data[7][0] = $data[7][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[7][1] = $data[7][1] + 1;
			}
			break;
		case "J":
			$data[8][0] = $data[8][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[8][1] = $data[8][1] + 1;
			}
			break;
		case "K":
			$data[9][0] = $data[9][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[9][1] = $data[9][1] + 1;
			}
			break;
		case "L":
			$data[10][0] = $data[10][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[10][1] = $data[10][1] + 1;
			}
			break;
		case "M":
			$data[11][0] = $data[11][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[11][1] = $data[11][1] + 1;
			}
			break;
		case "N":
			$data[12][0] = $data[12][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[12][1] = $data[12][1] + 1;
			}
			break;
		case "O":
			$data[13][0] = $data[13][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[13][1] = $data[13][1] + 1;
			}
			break;
		case "P":
			$data[14][0] = $data[14][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[14][1] = $data[14][1] + 1;
			}
			break;
		case "Q":
			$data[15][0] = $data[15][0] + 1;
			if(strcmp($lastReceptioncode, $row[1]) != 0){
				$data[15][1] = $data[15][1] + 1;
			}
			break;
		}
	}else{
		$setFlug = 0;
		switch( $row[0] ){
		case "A":
			$data[0][2] = $data[0][2] + 1;
			break;
		case "B":
			$data[1][2] = $data[1][2] + 1;
			break;
		case "C":
			$data[2][2] = $data[2][2] + 1;
			break;
		case "D":
			$data[3][2] = $data[3][2] + 1;
			break;
		case "E":
			$data[4][2] = $data[4][2] + 1;
			break;
		case "G":
			$data[5][2] = $data[5][2] + 1;
			break;
		case "H":
			$data[6][2] = $data[6][2] + 1;
			break;
		case "I":
			$data[7][2] = $data[7][2] + 1;
			break;
		case "J":
			$data[8][2] = $data[8][2] + 1;
			break;
		case "K":
			$data[9][2] = $data[9][2] + 1;
			break;
		case "L":
			$data[10][2] = $data[10][2] + 1;
			break;
		case "M":
			$data[11][2] = $data[11][2] + 1;
			break;
		case "N":
			$data[12][2] = $data[12][2] + 1;
			break;
		case "O":
			$data[10][2] = $data[13][2] + 1;
			break;
		case "P":
			$data[14][2] = $data[14][2] + 1;
			break;
		case "Q":
			$data[15][2] = $data[15][2] + 1;
			break;
		}
	}
	$lastZonecode = $row[0];
	$lastReceptioncode = $row[1];
}

for($i = 0; $i < $zoneNum + 1; $i++){
	switch($i){
	case 0:
		print "<tr><td bgcolor=#ccffff align=center>A</td>";
		break;
	case 1:
		print "<tr><td bgcolor=#ccffff align=center>B</td>";
		break;
	case 2:
		print "<tr><td bgcolor=#ccffff align=center>C</td>";
		break;
	case 3:
		print "<tr><td bgcolor=#ccffff align=center>D</td>";
		break;
	case 4:
		print "<tr><td bgcolor=#ccffff align=center>E</td>";
		break;
	case 5:
		print "<tr><td bgcolor=#ccffff align=center>G</td>";
		break;
	case 6:
		print "<tr><td bgcolor=#ccffff align=center>H</td>";
		break;
	case 7:
		print "<tr><td bgcolor=#ccffff align=center>I</td>";
		break;
	case 8:
		print "<tr><td bgcolor=#ccffff align=center>J</td>";
		break;
	case 9:
		print "<tr><td bgcolor=#ccffff align=center>K</td>";
		break;
	case 10:
		print "<tr><td bgcolor=#ccffff align=center>L</td>";
		break;
	case 11:
		print "<tr><td bgcolor=#ccffff align=center>M</td>";
		break;
	case 12:
		print "<tr><td bgcolor=#ccffff align=center>N</td>";
		break;
	case 13:
		print "<tr><td bgcolor=#ccffff align=center>O</td>";
		break;
	case 14:
		print "<tr><td bgcolor=#ccffff align=center>P</td>";
		break;
	case 15:
		print "<tr><td bgcolor=#ccffff align=center>Q</td>";
		break;
	case 16:
		print "<tr><td bgcolor=#ccffff align=center>合計</td>";
		break;
	}
	if($i < $zoneNum){
		print "<td align=right>".$data[$i][1]."</td>";
		print "<td align=right>".$data[$i][0]."</td>";
		print "<td align=right>".$data[$i][2]."</td>";
		$zoneTotal = $data[$i][0] + $data[$i][2];
		print "<td align=right>".$zoneTotal."</td>";
		print "</tr>";
	}else{
		$receptionTotal =                   $data[0][1]  + $data[1][1]  + $data[2][1]  + $data[3][1]  + $data[4][1] + $data[5][1];
		$receptionTotal = $receptionTotal + $data[6][1]  + $data[7][1]  + $data[8][1]  + $data[9][1]  + $data[10][1];
		$receptionTotal = $receptionTotal + $data[11][1] + $data[12][1] + $data[13][1] + $data[14][1] + $data[15][1];
		$picksTotal1    =                $data[0][0]  + $data[1][0]  + $data[2][0]  + $data[3][0]  + $data[4][0] + $data[5][0];
		$picksTotal1    = $picksTotal1 + $data[6][0]  + $data[7][0]  + $data[8][0]  + $data[9][0]  + $data[10][0];
		$picksTotal1    = $picksTotal1 + $data[11][0] + $data[12][0] + $data[13][0] + $data[14][0] + $data[15][0];
		$picksTotal2    =                $data[0][2]  + $data[1][2]  + $data[2][2]  + $data[3][2]  + $data[4][2] + $data[5][2];
		$picksTotal2    = $picksTotal2 + $data[6][2]  + $data[7][2]  + $data[8][2]  + $data[9][2]  + $data[10][2];
		$picksTotal2    = $picksTotal2 + $data[11][2] + $data[12][2] + $data[13][2] + $data[14][2] + $data[15][2];
		$picksTotal3    = $picksTotal1 + $picksTotal2;
		print "<td align=right>".$receptionTotal."</td>";
		print "<td align=right>".$picksTotal1."</td>";
		print "<td align=right>".$picksTotal2."</td>";
		print "<td align=right>".$picksTotal3."</td>";
	}
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
