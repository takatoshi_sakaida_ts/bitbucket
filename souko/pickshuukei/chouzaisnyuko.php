<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>帳在差異入庫作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)１２出庫　３２入庫
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//初期値設定
$i=0;
for ($c = 7; $c <= 23; $c++) {
//D中古
	$forhtmlcs_a[$c][0][0]=0;
	$forhtmlcs_a[$c][1][0]=0;
	$forhtmlcs_b[$c][0][0]=0;
	$forhtmlcs_b[$c][1][0]=0;
	$forhtmlcs_c[$c][0][0]=0;
	$forhtmlcs_c[$c][1][0]=0;
	$forhtmlcs_d[$c][0][0]=0;
	$forhtmlcs_d[$c][1][0]=0;
	$forhtmlcs_e[$c][0][0]=0;
	$forhtmlcs_e[$c][1][0]=0;
	$forhtmlcs_g[$c][0][0]=0;
	$forhtmlcs_g[$c][1][0]=0;
//D新古
	$forhtmlcs_a[$c][0][1]=0;
	$forhtmlcs_a[$c][1][1]=0;
	$forhtmlcs_b[$c][0][1]=0;
	$forhtmlcs_b[$c][1][1]=0;
	$forhtmlcs_c[$c][0][1]=0;
	$forhtmlcs_c[$c][1][1]=0;
	$forhtmlcs_d[$c][0][1]=0;
	$forhtmlcs_d[$c][1][1]=0;
	$forhtmlcs_e[$c][0][1]=0;
	$forhtmlcs_e[$c][1][1]=0;
	$forhtmlcs_g[$c][0][1]=0;
	$forhtmlcs_g[$c][1][1]=0;
//total
	$forhtmlt_a=0;
	$forhtmlt_b=0;
	$forhtmlt_c=0;
	$forhtmlt_d=0;
	$forhtmlt_e=0;
	$forhtmlt_g=0;
	$forhtml[$c][0]=0;
	$forhtml[$c][1]=0;
	$forhtml_sinko=0;
	$forhtml_sinkoa=0;
	$forhtml_sinkob=0;
	$forhtml_sinkoc=0;
	$forhtml_sinkod=0;
	$forhtml_sinkog=0;}
//検索結果の表示
print "<strong><BR>本日の帳在差異入庫作業状況</strong><br>".date('Y/m/d')."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td>帳在差異入庫時間</td><td>中古Ａ</td><td>中古Ｂ</td><td>中古Ｃ</td><td>中古Ｄ</td><td>中古Ｇ</td><td>新古Ａ</td><td>新古Ｂ</td><td>新古Ｃ</td><td>新古Ｄ</td><td>新古Ｇ</td><td>時間帯合計</td></tr>";
//初期値設定
$i=0;
$nowtime= date("H");
//D点数調査
$sql = "select dtime,dminute,count(*),zonecode,NEWUSED_KIND from ".
"(select substr(g.modifytime,1,2) as dtime,".
"case when substr(g.modifytime,3,2)<31 then 0 ".
"else 1 ".
"end as dminute,".
"g.modifytime,zonecode,g.NEWUSED_KIND from D_TANA_GOODS g,m_location m ".
"where g.TANA_KIND='57' AND g.MODIFYDATE=to_char((select sysdate from dual),'yyyymmdd') ".
"and g.locationcode=m.locationcode ".
"and substr(g.locationcode,1,1)<'9' ".
") ".
"group by dtime,dminute,zonecode,NEWUSED_KIND ".
"order by dtime,dminute,zonecode,NEWUSED_KIND";
//print $sql;
$res2 = $db->query($sql);
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row2 = $res2->fetchRow()){
	switch ($row2[3]){
//数値に変換するために+0してます
	case "A";
		$forhtmlcs_a[($row2[0]+0)][($row2[1]+0)][($row2[4]+0)]=$row2[2];
		break;
	case "B";
		$forhtmlcs_b[($row2[0]+0)][($row2[1]+0)][($row2[4]+0)]=$row2[2];
		break;
	case "C";
		$forhtmlcs_c[($row2[0]+0)][($row2[1]+0)][($row2[4]+0)]=$row2[2];
		break;
	case "D";
		$forhtmlcs_d[($row2[0]+0)][($row2[1]+0)][($row2[4]+0)]=$row2[2];
		break;
	case "E";
		$forhtmlcs_e[($row2[0]+0)][($row2[1]+0)][($row2[4]+0)]=$row2[2];
		break;
	case "G";
		$forhtmlcs_g[($row2[0]+0)][($row2[1]+0)][($row2[4]+0)]=$row2[2];
		break;
	}
	$forhtml[($row2[0]+0)][($row2[1]+0)]=$forhtml[($row2[0]+0)][($row2[1]+0)]+$row2[2];
	$i=$i+$row2[2];
}

for ($c = 9; $c <= $nowtime; $c++) {
	print "<tr>";
	print "<td>".$c."時00分から30分</td>";
	print "<td>".$forhtmlcs_a[$c][0][0]."</td>";
	print "<td>".$forhtmlcs_b[$c][0][0]."</td>";
	print "<td>".$forhtmlcs_c[$c][0][0]."</td>";
	print "<td>".$forhtmlcs_d[$c][0][0]."</td>";
	print "<td>".$forhtmlcs_g[$c][0][0]."</td>";
	print "<td>".$forhtmlcs_a[$c][0][1]."</td>";
	print "<td>".$forhtmlcs_b[$c][0][1]."</td>";
	print "<td>".$forhtmlcs_c[$c][0][1]."</td>";
	print "<td>".$forhtmlcs_d[$c][0][1]."</td>";
	print "<td>".$forhtmlcs_g[$c][0][1]."</td>";
	print "<td>".$forhtml[$c][0]."</td>";
	print "</tr>";
	print "<tr>";
	print "<td>".$c."時31分から60分</td>";
	print "<td>".$forhtmlcs_a[$c][1][0]."</td>";
	print "<td>".$forhtmlcs_b[$c][1][0]."</td>";
	print "<td>".$forhtmlcs_c[$c][1][0]."</td>";
	print "<td>".$forhtmlcs_d[$c][1][0]."</td>";
	print "<td>".$forhtmlcs_g[$c][1][0]."</td>";
	print "<td>".$forhtmlcs_a[$c][1][1]."</td>";
	print "<td>".$forhtmlcs_b[$c][1][1]."</td>";
	print "<td>".$forhtmlcs_c[$c][1][1]."</td>";
	print "<td>".$forhtmlcs_d[$c][1][1]."</td>";
	print "<td>".$forhtmlcs_g[$c][1][1]."</td>";
	print "<td>".$forhtml[$c][1]."</td>";
	print "</tr>";
	$forhtmlt_a=$forhtmlt_a+$forhtmlcs_a[$c][0][0]+$forhtmlcs_a[$c][1][0];
	$forhtmlt_b=$forhtmlt_b+$forhtmlcs_b[$c][0][0]+$forhtmlcs_b[$c][1][0];
	$forhtmlt_c=$forhtmlt_c+$forhtmlcs_c[$c][0][0]+$forhtmlcs_c[$c][1][0];
	$forhtmlt_d=$forhtmlt_d+$forhtmlcs_d[$c][0][0]+$forhtmlcs_d[$c][1][0];
	$forhtmlt_g=$forhtmlt_g+$forhtmlcs_g[$c][0][0]+$forhtmlcs_g[$c][1][0];
	$forhtml_sinkoa=$forhtml_sinkoa+$forhtmlcs_a[$c][0][1]+$forhtmlcs_a[$c][1][1];
	$forhtml_sinkob=$forhtml_sinkob+$forhtmlcs_b[$c][0][1]+$forhtmlcs_b[$c][1][1];
	$forhtml_sinkoc=$forhtml_sinkoc+$forhtmlcs_c[$c][0][1]+$forhtmlcs_c[$c][1][1];
	$forhtml_sinkod=$forhtml_sinkod+$forhtmlcs_d[$c][0][1]+$forhtmlcs_d[$c][1][1];
	$forhtml_sinkog=$forhtml_sinkog+$forhtmlcs_g[$c][0][1]+$forhtmlcs_g[$c][1][1];
	$forhtml_sinko=$forhtml_sinko+$forhtmlcs_a[$c][0][1]+$forhtmlcs_b[$c][0][1]+$forhtmlcs_c[$c][0][1]+$forhtmlcs_d[$c][0][1]+$forhtmlcs_g[$c][0][1]+$forhtmlcs_a[$c][1][1]+$forhtmlcs_b[$c][1][1]+$forhtmlcs_c[$c][1][1]+$forhtmlcs_d[$c][1][1]+$forhtmlcs_g[$c][1][1];
}
print "<tr><td>　</td><td>".$forhtmlt_a."</TD>";
print "<td>".$forhtmlt_b."</TD>";
print "<td>".$forhtmlt_c."</TD>";
print "<td>".$forhtmlt_d."</TD>";
print "<td>".$forhtmlt_g."</TD>";
print "<td>".$forhtml_sinkoa."</TD>";
print "<td>".$forhtml_sinkob."</TD>";
print "<td>".$forhtml_sinkoc."</TD>";
print "<td>".$forhtml_sinkod."</TD>";
print "<td>".$forhtml_sinkog."</TD><TD>　</TD></TR>";

print "<tr><td>合計</td><td colspan=11 align=right>　".$i."　</td></tr>";
print "</table>";

//データの開放
$res2->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>