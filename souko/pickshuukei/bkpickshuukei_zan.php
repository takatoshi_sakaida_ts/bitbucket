<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ピッキング残数状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$sql = "select count(distinct g.receptioncode), count(g.receptioncode)".
" from d_reception r, d_reception_goods g".
" where r.receptioncode = g.receptioncode".
" and r.status_kind = '01'".
" and g.picked_kind = '0'".
" and g.cancel_kind = '0'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>ピッキング残数状況</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=100 align=center>注文件数</td>
	<td width=100 align=center>注文点数</td>
</tr>
<tr>
<?php

//検索結果の表示
//取得データのセット
while($row = $res->fetchRow()){

//項目名の表示
print "<td align=right>". number_format($row[0]) ."件</td><td width=100 align=right>".number_format($row[1]) ."点</td></tr>";
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
