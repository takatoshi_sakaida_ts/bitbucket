<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ピッキング点数（スタッフ毎・5分毎集計）</title>
</head>
<body>
<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
$date = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($date)) {
	$date = date('Ymd');
}
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
$starttime    = 7;
$nowtime      = 23;
$calcdate     = 0;
$ScaleMinutes = 5;
$BuffMinutesMax = floor(60/$ScaleMinutes);
$dlfile_name = "./pickshuukei_staff_" .date('Ymd', strtotime($calcdate.' day')) . ".csv";
//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres."\r\n";
	fputs($fp,$str);
	fclose( $fp );
}

$sql = "SELECT R.OPERATESTAFFCODE,F.STAFFNAME,R.PICKTIME,R.PICKMINUTE,COUNT(*),R.ZONECODE ".
"FROM (SELECT D.OPERATESTAFFCODE,SUBSTR(D.OPERATETIME,1,2) AS PICKTIME, ".
"SUBSTR(D.OPERATETIME,3,2) AS PICKMINUTE,M.ZONECODE ".
"FROM D_RECEPTION_GOODS D,M_LOCATION M ".
"WHERE D.PICKED_KIND='1' AND D.LOCATIONCODE=M.LOCATIONCODE ".
"AND D.OPERATEDATE=$date ".
") R,M_STAFF F WHERE R.OPERATESTAFFCODE=F.STAFFCODE ".
"GROUP BY R.PICKTIME,R.PICKMINUTE,R.ZONECODE,R.OPERATESTAFFCODE,F.STAFFNAME ".
"ORDER BY R.OPERATESTAFFCODE,F.STAFFNAME,R.PICKTIME,R.PICKMINUTE,R.ZONECODE";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>　<BR>ピッキング点数（スタッフ・".$ScaleMinutes."分毎集計）</strong><br>".$date."<br><br>\n<HR>";
print "※作業時間は１分以内に１点以上のピッキングした場合、１分として算出\n";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td colspan=7></td>";
for ($c = $starttime; $c <= $nowtime; $c++) {
	print "<td colspan=12>".$c."時</td>";
}
print "</tr><tr bgcolor=#ccffff>\n";
print "<td nowrap>スタッフコード</td>";
print "<td nowrap>スタッフ名</td>";
print "<td nowrap>総計</td>";
print "<td nowrap>ゾーン</td>";
print "<td nowrap>作業時間</td>";
print "<td nowrap>1時間あたり</td>";
print "<td nowrap>合計</td>";
for ($c = $starttime; $c <= $nowtime; $c++) {
	print "<td nowrap>00-04</td>";
	print "<td nowrap>05-09</td>";
	print "<td nowrap>10-14</td>";
	print "<td nowrap>15-19</td>";
	print "<td nowrap>20-24</td>";
	print "<td nowrap>25-29</td>";
	print "<td nowrap>30-34</td>";
	print "<td nowrap>35-39</td>";
	print "<td nowrap>40-44</td>";
	print "<td nowrap>45-49</td>";
	print "<td nowrap>50-54</td>";
	print "<td nowrap>55-59</td>";
}
print "</tr>";
$total_count=0;
$i=0;
$motostaffcode="0";
for($cc = 0; $cc <= 23; $cc++) {
	for($min = 0; $min <$BuffMinutesMax; $min++){
		$jikantotal[$cc][$min]=0;
	}
}
//取得データのセット
while($row = $res->fetchRow()){
	$total_count=$total_count+$row[4];
	if ($motostaffcode==($row[0]+0))
	{
	}else
	{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$staffzonecnt_a[$i]=0;
		$staffzonecnt_b[$i]=0;
		$staffzonecnt_c[$i]=0;
		$staffzonecnt_d[$i]=0;
		$staffzonecnt_e[$i]=0;
		$staffzonecnt_g[$i]=0;
		$staffzonecnt_h[$i]=0;
		$staffzonecnt_i[$i]=0;
		$staffzonecnt_j[$i]=0;
		$staffzonecnt_k[$i]=0;
		$staffzonecnt_l[$i]=0;
		$staffzonecnt_m[$i]=0;
		$staffzonecnt_n[$i]=0;
		$staffzonecnt_o[$i]=0;
		$staffzonecnt_p[$i]=0;
		$staffzonecnt_q[$i]=0;
		$stafftotal[$i]=0;
		$staffworkinghours_a[$i]=0;
		$staffworkinghours_b[$i]=0;
		$staffworkinghours_c[$i]=0;
		$staffworkinghours_d[$i]=0;
		$staffworkinghours_e[$i]=0;
		$staffworkinghours_g[$i]=0;
		$staffworkinghours_h[$i]=0;
		$staffworkinghours_i[$i]=0;
		$staffworkinghours_j[$i]=0;
		$staffworkinghours_k[$i]=0;
		$staffworkinghours_l[$i]=0;
		$staffworkinghours_m[$i]=0;
		$staffworkinghours_n[$i]=0;
		$staffworkinghours_o[$i]=0;
		$staffworkinghours_p[$i]=0;
		$staffworkinghours_q[$i]=0;
		$staffzonetotal_a[$i]=0;
		$staffzonetotal_b[$i]=0;
		$staffzonetotal_c[$i]=0;
		$staffzonetotal_d[$i]=0;
		$staffzonetotal_e[$i]=0;
		$staffzonetotal_g[$i]=0;
		$staffzonetotal_h[$i]=0;
		$staffzonetotal_i[$i]=0;
		$staffzonetotal_j[$i]=0;
		$staffzonetotal_k[$i]=0;
		$staffzonetotal_l[$i]=0;
		$staffzonetotal_m[$i]=0;
		$staffzonetotal_n[$i]=0;
		$staffzonetotal_o[$i]=0;
		$staffzonetotal_p[$i]=0;
		$staffzonetotal_q[$i]=0;
		//集計値初期化
		for($cc = 0; $cc <= 23; $cc++) {
			for($min = 0; $min <$BuffMinutesMax; $min++){
				$forhtml_a[($i)][($cc)][$min]=0;
				$forhtml_b[($i)][($cc)][$min]=0;
				$forhtml_c[($i)][($cc)][$min]=0;
				$forhtml_d[($i)][($cc)][$min]=0;
				$forhtml_e[($i)][($cc)][$min]=0;
				$forhtml_g[($i)][($cc)][$min]=0;
				$forhtml_h[($i)][($cc)][$min]=0;
				$forhtml_i[($i)][($cc)][$min]=0;
				$forhtml_j[($i)][($cc)][$min]=0;
				$forhtml_k[($i)][($cc)][$min]=0;
				$forhtml_l[($i)][($cc)][$min]=0;
				$forhtml_m[($i)][($cc)][$min]=0;
				$forhtml_n[($i)][($cc)][$min]=0;
				$forhtml_o[($i)][($cc)][$min]=0;
				$forhtml_p[($i)][($cc)][$min]=0;
				$forhtml_q[($i)][($cc)][$min]=0;
			}
		}
	}

	//数値に変換するために+0してます
	//staffcode,時間,分
	switch ($row[5]){
	case "A";
		$forhtml_a[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_a[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
		$staffzonetotal_a[$i]=$staffzonetotal_a[$i]+$row[4];
		$staffzonecnt_a[$i]=1;
		$staffworkinghours_a[$i] = $staffworkinghours_a[$i]+1;
		break;
	case "B";
		$forhtml_b[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_b[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_b[$i]=$staffzonetotal_b[$i]+$row[4];
		$staffzonecnt_b[$i]=1;
		$staffworkinghours_b[$i] = $staffworkinghours_b[$i]+1;
		break;
	case "C";
		$forhtml_c[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_c[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_c[$i]=$staffzonetotal_c[$i]+$row[4];
		$staffzonecnt_c[$i]=1;
		$staffworkinghours_c[$i] = $staffworkinghours_c[$i]+1;
		break;
	case "D";
		$forhtml_d[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_d[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_d[$i]=$staffzonetotal_d[$i]+$row[4];
		$staffzonecnt_d[$i]=1;
		$staffworkinghours_d[$i] = $staffworkinghours_d[$i]+1;
		break;
	case "E";
		$forhtml_e[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_e[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_e[$i]=$staffzonetotal_e[$i]+$row[4];
		$staffzonecnt_e[$i]=1;
		$staffworkinghours_e[$i] = $staffworkinghours_e[$i]+1;
		break;
	case "G";
		$forhtml_g[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_g[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_g[$i]=$staffzonetotal_g[$i]+$row[4];
		$staffzonecnt_g[$i]=1;
		$staffworkinghours_g[$i] = $staffworkinghours_g[$i]+1;
		break;
	case "H";
		$forhtml_h[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_h[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_h[$i]=$staffzonetotal_h[$i]+$row[4];
		$staffzonecnt_h[$i]=1;
		$staffworkinghours_h[$i] = $staffworkinghours_h[$i]+1;
		break;
	case "I";
		$forhtml_i[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_i[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_i[$i]=$staffzonetotal_i[$i]+$row[4];
		$staffzonecnt_i[$i]=1;
		$staffworkinghours_i[$i] = $staffworkinghours_i[$i]+1;
		break;
	case "J";
		$forhtml_j[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_j[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_j[$i]=$staffzonetotal_j[$i]+$row[4];
		$staffzonecnt_j[$i]=1;
		$staffworkinghours_j[$i] = $staffworkinghours_j[$i]+1;
		break;
	case "K";
		$forhtml_k[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_k[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_k[$i]=$staffzonetotal_k[$i]+$row[4];
		$staffzonecnt_k[$i]=1;
		$staffworkinghours_k[$i] = $staffworkinghours_k[$i]+1;
		break;
	case "L";
		$forhtml_l[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_l[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_l[$i]=$staffzonetotal_l[$i]+$row[4];
		$staffzonecnt_l[$i]=1;
		$staffworkinghours_l[$i] = $staffworkinghours_l[$i]+1;
		break;
	case "M";
		$forhtml_m[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_m[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_m[$i]=$staffzonetotal_m[$i]+$row[4];
		$staffzonecnt_m[$i]=1;
		$staffworkinghours_m[$i] = $staffworkinghours_m[$i]+1;
		break;
	case "N";
		$forhtml_n[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_n[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_n[$i]=$staffzonetotal_n[$i]+$row[4];
		$staffzonecnt_n[$i]=1;
		$staffworkinghours_n[$i] = $staffworkinghours_n[$i]+1;
		break;
	case "O";
		$forhtml_o[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_o[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_o[$i]=$staffzonetotal_o[$i]+$row[4];
		$staffzonecnt_o[$i]=1;
		$staffworkinghours_o[$i] = $staffworkinghours_o[$i]+1;
		break;
	case "P";
		$forhtml_p[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_p[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_p[$i]=$staffzonetotal_p[$i]+$row[4];
		$staffzonecnt_p[$i]=1;
		$staffworkinghours_p[$i] = $staffworkinghours_p[$i]+1;
		break;
	case "Q";
		$forhtml_q[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_q[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_q[$i]=$staffzonetotal_q[$i]+$row[4];
		$staffzonecnt_q[$i]=1;
		$staffworkinghours_q[$i] = $staffworkinghours_q[$i]+1;
		break;
	}
	$stafftotal[$i]=$stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
}
//HTML表示
for($ii = 1; $ii <= $i; $ii++) {
	$headerflg = 0;
	//Azone
	if ($staffzonetotal_a[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>A</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_a[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_a[$ii]/($staffworkinghours_a[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_a[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_a[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Bzone
	if ($staffzonetotal_b[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>B</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_b[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_b[$ii]/($staffworkinghours_b[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_b[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_b[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Czone
	if ($staffzonetotal_c[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>C</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_c[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_c[$ii]/($staffworkinghours_c[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_c[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_c[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Dzone
	if ($staffzonetotal_d[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>D</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_d[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_d[$ii]/($staffworkinghours_d[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_d[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_d[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ezone
	if ($staffzonetotal_e[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>E</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_e[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_e[$ii]/($staffworkinghours_e[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_e[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_e[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Gzone
	if ($staffzonetotal_g[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>G</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_g[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_g[$ii]/($staffworkinghours_g[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_g[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_g[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Hzone
	if ($staffzonetotal_h[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>H</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_h[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_h[$ii]/($staffworkinghours_h[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_h[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_h[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Izone
	if ($staffzonetotal_i[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>I</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_i[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_i[$ii]/($staffworkinghours_i[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_i[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_i[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Jzone
	if ($staffzonetotal_j[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>J</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_j[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_j[$ii]/($staffworkinghours_j[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_j[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_j[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Kzone
	if ($staffzonetotal_k[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>K</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_k[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_k[$ii]/($staffworkinghours_k[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_k[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_k[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Lzone
	if ($staffzonetotal_l[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>L</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_l[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_l[$ii]/($staffworkinghours_l[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_l[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_l[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Mzone
	if ($staffzonetotal_m[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>M</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_m[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_m[$ii]/($staffworkinghours_m[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_m[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_m[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Nzone
	if ($staffzonetotal_n[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>N</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_n[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_n[$ii]/($staffworkinghours_n[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_n[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_n[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Ozone
	if ($staffzonetotal_o[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>O</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_o[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_o[$ii]/($staffworkinghours_o[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_o[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_o[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Pzone
	if ($staffzonetotal_p[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>P</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_p[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_p[$ii]/($staffworkinghours_p[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_p[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_p[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
	//Qzone
	if ($staffzonetotal_q[$ii]>0)
	{
		print "<tr><td>".$staffcode[$ii]."</td>";
		print "<td nowrap>".$staffname[$ii]."</td>";
		print "<td align=right>".$stafftotal[$ii]."</td>";
		print "<td align=center>Q</td>";
		print "<td align=center>".sprintf("%1.2f", $staffworkinghours_q[$ii] / 60)."</td>";
		print "<td align=right>".sprintf("%d", $staffzonetotal_q[$ii]/($staffworkinghours_q[$ii] / 60))."</td>";
		print "<td align=right>".$staffzonetotal_q[$ii]."</td>";
		for ($c = $starttime; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				print "<td align=right>".$forhtml_q[$ii][$c][$min]."</td>";
			}
		}
		print "</tr>";
		$headerflg=1;
	}
}
print "<tr><td colspan=6 align=center>ピッキング点数合計</td><td>".number_format($total_count)."</td>";
for ($c = $starttime; $c <= $nowtime; $c++) {
	for($min = 0; $min < $BuffMinutesMax; $min++){
		print "<td align=right>".number_format($jikantotal[$c][$min])."</td>";
	}
}
print "</tr>";
print "</table>";
//データの開放
$res->free();
$db->disconnect();

unset($row, $motostaffcode,$staffcode,$staffname);
unset($staffzonecnt_a, $staffzonecnt_b, $staffzonecnt_c, $staffzonecnt_d, $staffzonecnt_e, $staffzonecnt_g);
unset($staffzonecnt_h, $staffzonecnt_i, $staffzonecnt_j, $staffzonecnt_k, $staffzonecnt_l);
unset($staffzonecnt_m, $staffzonecnt_n, $staffzonecnt_o, $staffzonecnt_p, $staffzonecnt_q);
unset($staffworkinghours_a, $staffworkinghours_b, $staffworkinghours_c, $staffworkinghours_d, $staffworkinghours_e, $staffworkinghours_g);
unset($staffworkinghours_h, $staffworkinghours_i, $staffworkinghours_j, $staffworkinghours_k, $staffworkinghours_l);
unset($staffworkinghours_m, $staffworkinghours_n, $staffworkinghours_o, $staffworkinghours_p, $staffworkinghours_q);
unset($staffzonetotal_a, $staffzonetotal_b, $staffzonetotal_c, $staffzonetotal_d, $staffzonetotal_e, $staffzonetotal_g);
unset($staffzonetotal_h, $staffzonetotal_i, $staffzonetotal_j, $staffzonetotal_k, $staffzonetotal_l);
unset($staffzonetotal_m, $staffzonetotal_n, $staffzonetotal_o, $staffzonetotal_p, $staffzonetotal_q);
unset($forhtml_a, $forhtml_b, $forhtml_c, $forhtml_d, $forhtml_e, $forhtml_g);
unset($forhtml_h, $forhtml_i, $forhtml_j, $forhtml_k, $forhtml_l);
unset($forhtml_m, $forhtml_n, $forhtml_o, $forhtml_p, $forhtml_q);

?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='../../index.php'"></FORM>
</body>
</html>
