<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ピッキング件数・点数</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

$date = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($date)) {
	print "データが入力されていません．<br>";
}

$sql =	"SELECT ".
		"	R.OPERATESTAFFCODE, ".
		"	F.STAFFNAME, ".
		"  COUNT(DISTINCT R.RECEPTIONCODE), ".
		"  COUNT(R.RECEPTIONCODE) ".
		"FROM ".
		"	( ".
		"		SELECT ".
		"			D.OPERATESTAFFCODE, ".
		"			D.RECEPTIONCODE ".
		"		FROM ".
		"			D_RECEPTION_GOODS D, ".
		"			M_LOCATION M ".
		"		WHERE ".
		"			D.PICKED_KIND='1' AND ".
		"			D.CANCEL_KIND='0' AND ".
		"			D.LOCATIONCODE=M.LOCATIONCODE AND ".
		"			D.OPERATEDATE=$date ".
		"	)R, ".
		"	M_STAFF F ".
		"WHERE ".
		"	R.OPERATESTAFFCODE=F.STAFFCODE ".
		"GROUP BY ".
		"	R.OPERATESTAFFCODE, ".
		"	F.STAFFNAME ".
		"ORDER BY ".
		"	R.OPERATESTAFFCODE, ".
		"	F.STAFFNAME";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

//検索結果の表示
print "<strong>　<BR>ピッキング件数・点数</strong><br>".$date."<br><br>\n<HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff><th>スタッフコード</th><th width=70>スタッフ名</th><th>件数</th><th>点数</th></tr>\n";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>\n";
	// ソート用に名称の先頭に数字を入れているので削除
	print "<td>".$row[0]."</td>\n";
	print "<td>".$row[1]."</td>\n";
	print "<td align='right'>".number_format($row[2]) ."</td>\n";
	print "<td align='right'>".number_format($row[3]) ."</td>\n";
	print "</tr>\n";
}
print "</table>";

//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='../../index.php'"></FORM>
</body>
</html>
