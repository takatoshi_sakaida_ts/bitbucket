<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>ピッキング残点数状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
// 2014/1/1以前のデータはピッキング道場などで使用している注文もあるので除外
// 点数取得
$sql = "select r.receptiondate, l.zonecode, count(g.receptioncode)".
" from d_reception r, d_reception_goods g, m_location l".
" where r.receptioncode = g.receptioncode".
" and g.locationcode = l.locationcode".
" and r.status_kind = '01'".
" and g.picked_kind = '0'".
" and g.cancel_kind = '0'".
" and l.zonecode <> 'Z'".
" and r.receptiondate >= '20140101'".
" group by r.receptiondate, l.zonecode".
" order by r.receptiondate, l.zonecode";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>ピッキング残点数状況</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=100 align=center rowspan=2>日付</td>
	<td width=100 align=center colspan=16>ゾーン(点数)</td>
</tr>
<tr bgcolor=#ccffff>
	<td width=100 align=center>Ａ</td>
	<td width=100 align=center>Ｂ</td>
	<td width=100 align=center>Ｃ</td>
	<td width=100 align=center>Ｄ</td>
	<td width=100 align=center>Ｅ</td>
	<td width=100 align=center>Ｇ</td>
	<td width=100 align=center>Ｈ</td>
	<td width=100 align=center>Ｉ</td>
	<td width=100 align=center>Ｊ</td>
	<td width=100 align=center>Ｋ</td>
	<td width=100 align=center>Ｌ</td>
	<td width=100 align=center>Ｍ</td>
	<td width=100 align=center>Ｎ</td>
	<td width=100 align=center>Ｏ</td>
	<td width=100 align=center>Ｐ</td>
	<td width=100 align=center>Ｑ</td>
</tr>
<?php

$checkdate = "20000101";
$i = -1;
$max = 16;
//検索結果の表示
//取得データのセット
while($row = $res->fetchRow()){

	if( strcmp($checkdate, $row[0]) != 0 )
	{
		$i = $i + 1;
		$receptdate[$i] = $row[0];
		$checkdate = $receptdate[$i];
		for ($n = 0; $n < $max; $n++ )
		{
			$data[$i][$n] = 0;
		}
	}

	switch( $row[1] ){
	case "A":
		$data[$i][0] = $row[2];
		break;
	case "B":
		$data[$i][1] = $row[2];
		break;
	case "C":
		$data[$i][2] = $row[2];
		break;
	case "D":
		$data[$i][3] = $row[2];
		break;
	case "E":
		$data[$i][4] = $row[2];
		break;
	case "G":
		$data[$i][5] = $row[2];
		break;
	case "H":
		$data[$i][6] = $row[2];
		break;
	case "I":
		$data[$i][7] = $row[2];
		break;
	case "J":
		$data[$i][8] = $row[2];
		break;
	case "K":
		$data[$i][9] = $row[2];
		break;
	case "L":
		$data[$i][10] = $row[2];
		break;
	case "M":
		$data[$i][11] = $row[2];
		break;
	case "N":
		$data[$i][12] = $row[2];
		break;
	case "O":
		$data[$i][13] = $row[2];
		break;
	case "P":
		$data[$i][14] = $row[2];
		break;
	case "Q":
		$data[$i][15] = $row[2];
		break;
	}
}

$i = $i + 1;
//HTML表示
for ($n = 0; $n < $max; $n++ )
{
	$total[$n] = 0;
}
for ($ii = 0; $ii < $i; $ii++ )
{
	print "<tr>\n<td>". Rethiduke($receptdate[$ii]) . "</td>";

	for ($n = 0; $n < $max; $n++ )
	{
		print "<td align=right>". number_format($data[$ii][$n]) ."</td>";
		$total[$n] = $total[$n] + $data[$ii][$n];

	}
	print "</td>\n</tr>";
}

// 合計を表示
print "<tr>\n<td bgcolor=#ccffff>合計</td>";
for ($n = 0; $n < $max; $n++ )
{
	print "<td bgcolor=#99ccff align=right>". number_format($total[$n]) ."</td>";
}
print "</td>\n</tr>";

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
