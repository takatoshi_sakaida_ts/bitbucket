<?PHP
print "TAIRAX 処理開始".date("YmdHis")."\r\n";
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");

//変数
$check=0;
$today = date("YmdHis");
$todayfile = date("Ymd");
$dlfile_name = "./dlfile/tairax_" . $todayfile . ".csv";

//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres;
	fputs($fp,$str);
	fclose( $fp );
}
//*************************************************************************************
//ファイル読み込み
//*************************************************************************************
$contents = @file('tairax.csv');
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
	die(99);
}
foreach($contents as $readkeisanline){
	$print_log="";
	$inline=preg_split('/,/',$readkeisanline);
/*
	print $inline[0]."\r\n";//instorecode
	print $inline[1]."\r\n";//genre3
	print $inline[2]."\r\n";//市場価格
	print $inline[3]."\r\n";//中古価格
	print $inline[4];//中古在庫数
*/
	$inline[4]=preg_replace("/(\r\n|\n|\r)/","",$inline[4]);
	$sql="select m.instorecode,m.JANCODE,m.TITLE,m.TITLEKANA,a.AUTHORNAME,a.AUTHORNAMEKANA,m.VOLUME,c.COSTPRICE ".
	"from M_GOODS_BOOK m,M_GOODS_author a,d_goods_cost c ".
	"where m.instorecode=a.instorecode(+) ".
	"and m.instorecode=c.instorecode(+) ".
	"and c.newused_kind='0' ".
//	"and a.AUTHORROLE_KIND in ('00','10','11') ".
	"and m.instorecode='".$inline[0]."' order by a.AUTHORROLE_KIND";
//	print $sql."\r\n";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}

	while($row = $res->fetchRow()){
		$sql="select count(receptioncode||instorecode) from d_reception_goods where substr(receptioncode,1,8)=(SELECT TO_CHAR(SYSDATE-1,'YYYYMMDD') FROM DUAL) and newused_kind='0' and cancel_kind='0' and instorecode='".$inline[0]."'";
		$res2 = $db->query($sql);
			if(DB::isError($res2)){
				print $res2->getMessage();
				$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
			}
		while($row2 = $res2->fetchRow()){
			$print_log=$inline[0].",".$row[1].",".$inline[1].",'".$row[2].",'".$row[3].",'".$row[4].",'".$row[5].",".$row[6].",".$inline[2].",".$inline[3].",".$row[7].",".$inline[4].",". $row2[0].",".$inline[5];
		}
		break;
	}
//print $print_log."\r\n";
if ($check==0)
{
	outputcsv("iscd,JANコード,ジャンル3コード,タイトル名,タイトル名カナ,著者名,著者名カナ,巻次数,市場価格,中古販売価格,原価,中古EC在庫数,注文数,レビュー\r\n",$dlfile_name);
}
outputcsv($print_log,$dlfile_name);
$check++;
}

$db->disconnect();
//計算ファイルのリネーム処理
@rename("tairax.csv","tairax_fin_".$today.".csv");
print "正常終了".date("YmdHis")."\r\n";
?>
