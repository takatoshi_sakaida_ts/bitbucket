<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出荷登録データリカバリ 完了</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
// 前画面からの入力キーワードを取得する
$receptioncode = addslashes(@$_POST["receptioncode"]);
$shipmentcode  = addslashes(@$_POST["shipmentcode"]);
$transportkind = addslashes(@$_POST["transportkind"]);

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($shipmentcode)) {
	print "送り状番号を入力してください！！<br>";
	print "更新ボタンの押下は無効です。<br>";
} else if (empty($transportkind)) {
	print "配送種別を選択してください！！<br>";
	print "更新ボタンの押下は無効です。<br>";
} else {
	print "<strong><BR>出荷登録データリカバリ 完了</strong><br><HR>";
	// 注文データのステータスを出荷完了に変更
	$sql = "UPDATE D_RECEPTION ".
           "SET STATUS_KIND = '03' ".
           "WHERE RECEPTIONCODE = '".$receptioncode."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	// 送り状データを作成
	$sql = "INSERT INTO D_RECEPTION_SHIPMENT ".
           "(RECEPTIONCODE, TRANSPORT_KIND, DELIVERYCODE, DELIVERYWEIGHT, DELIVERYDATETIME, MODIFYDATE, MODIFYTIME) ".
           "VALUES('".$receptioncode."','".$transportkind."','".$shipmentcode."','0','".date('YmdHis')."','".date('Ymd')."','".date('His')."')";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	// 更新後のデータを確認
	$sql = "SELECT STATUS_KIND FROM D_RECEPTION WHERE RECEPTIONCODE = '".$receptioncode."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff><th width=150>注文番号</th>";
	print "<th width=180>                    注文データの状態</th>";
	print "<th width=180>                    送り状データ</th></tr>";
	// 注文番号
	print "<td align='center'>".$receptioncode."</td>";
	// 注文データの状態
	$statusKind = "注文番号不正";
	while($row = $res->fetchRow()){
		$statusKind = Retstatuskind($row[0]);
	}
	print "<td align='center'>" .$statusKind. "</td>";
	// 対象の送り状番号データを確認
	$sql = "SELECT TRANSPORT_KIND FROM D_RECEPTION_SHIPMENT WHERE RECEPTIONCODE = '".$receptioncode."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	// 注文データの状態
	$shipment = "未登録";
	while($row = $res->fetchRow()){
		$shipment = Retunsoukaisha($row[0]);
	}
	print "<td align='center'>".$shipment."</td>\n";
	print "</table>";

	//データの開放
	$res->free();
	$db->disconnect();
}

?>
<BR>
<FORM ACTION="/souko/make_shipment/make_shipment_serch.php"><INPUT TYPE="submit" VALUE="戻る"></FORM>
</body>
</html>
