<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>出荷登録データリカバリ</title>
</head>
<body>

<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
// 前画面からの入力キーワードを取得する
$receptioncode = addslashes(@$_POST["receptioncode"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($receptioncode)) {
	print "対象となる注文番号を入力してください！！<br>";
	print "更新ボタンの押下は無効です。<br>";
} else  {
	print "<strong><BR>出荷登録データリカバリ</strong><br><HR>";
	// 対象の注文データを確認
	$sql = "SELECT STATUS_KIND FROM D_RECEPTION WHERE RECEPTIONCODE = '".$receptioncode."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff><th width=150>注文番号</th>";
	print "<th width=180>                    注文データの状態</th>";
	print "<th width=180>                    送り状データ</th></tr>";
	// 注文番号
	print "<td align='center'>".$receptioncode."</td>";
	// 注文データの状態
	$statusKind = "注文番号不正";
	while($row = $res->fetchRow()){
		$statusKind = Retstatuskind($row[0]);
	}
	print "<td align='center'>" .$statusKind. "</td>";
	// 対象の送り状番号データを確認
	$sql = "SELECT TRANSPORT_KIND FROM D_RECEPTION_SHIPMENT WHERE RECEPTIONCODE = '".$receptioncode."'";
	$res = $db->query($sql);
	if(DB::isError($res)){
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	// 注文データの状態
	$shipment = "未登録";
	while($row = $res->fetchRow()){
		$shipment = Retunsoukaisha($row[0]);
	}
	print "<td align='center'>".$shipment."</td>\n";
	print "</table>";
	
	print "<br><font color='red' size=5>送り状が印刷されているにも関わらず、</font><br>";
	print "<br><font color='red' size=5>送り状データ「未登録」の状態。</font><br>";
	print "<br><font color='red' size=5>上記の場合には、出荷登録リカバリをお願いします。</font><br>";

	//データの開放
	$res->free();
	$db->disconnect();
}
?>




<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</FORM>

<FORM METHOD="POST" ACTION="make_shipment_commit.php">

注文番号<BR>
<?php
print "<input type=\"text\" value=\"".addslashes(@$_POST["receptioncode"])."\" disabled><BR><BR>";
print "<input type=\"hidden\" name=\"receptioncode\" value=\"".addslashes(@$_POST["receptioncode"])."\">";
?>
送り状番号<BR>
<input type="text" name="shipmentcode" value=""><BR><BR>
配送種別<BR>
<select name="transportkind">
<option value="">--- 選択 ---</option>
<option value="01">その他</option>
<option value="02">佐川急便</option>
<option value="03">ヤマトメール便</option>
<option value="04">日本郵便(袋)</option>
<option value="05">日本郵便(段ボール小)</option>
<option value="06">日本郵便(段ボール大)</option>
<option value="07">ゆうパケット(2〜3cm)</option>
<option value="08">ゆうパケット(2cm以下)</option>
</select><BR><BR>
<INPUT TYPE="SUBMIT" VALUE="リカバリ実行">
</FORM>

</body>
</html>
