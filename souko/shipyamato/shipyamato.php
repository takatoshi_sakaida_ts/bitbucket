<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<?PHP
require_once("../parts/pagechk.inc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>メール便出荷件数取得</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select m.DELIVERYAREA_KBN,count(distinct d.receptioncode)".
" from D_RECEPTION d,d_reception_shipment s,M_POSTCODE_PRIORITY m where ".
"d.receptioncode=s.receptioncode ".
"and d.SHIPPINGADDRESS1=m.PREFECTURE ".
"and s.TRANSPORT_KIND='03' ".
"and substr(s.DELIVERYDATETIME,1,8)=(SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL) " .
"group by m.DELIVERYAREA_KBN ".
"order by m.DELIVERYAREA_KBN ";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
require_once("../log/loger.inc");
output_log($sql,$_SESSION["user_id"],'shipyamato');
//検索結果の表示
print "<strong><BR>本日のメール便出荷作業状況</strong><br>".date('Y/m/d')."<br><br>\n<HR>";

$j=1;
$deliveryarea[1]=0;
$deliveryarea[2]=0;
$deliveryarea[3]=0;
$deliveryarea[4]=0;
$deliveryarea[5]=0;
$deliveryarea[6]=0;
$deliveryarea[7]=0;
$deliveryarea[8]=0;
$deliveryarea[9]=0;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	$deliveryarea[($row[0]+0)]=$row[1];
	$j++;
}
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>項番</td><td nowrap width=120>エリア名</td><td nowrap width=120>発送件数</td></tr>";
	print "<tr><td>1</td><td>北海道</td><td>".$deliveryarea[1] ."</td></tr>";
	print "<tr><td>2</td><td>東北</td><td>".$deliveryarea[2] ."</td></tr>";
	print "<tr><td>3</td><td>関東</td><td>".$deliveryarea[3] ."</td></tr>";
	print "<tr><td>4</td><td>北信越</td><td>".$deliveryarea[4] ."</td></tr>";
	print "<tr><td>5</td><td>中部</td><td>".$deliveryarea[5] ."</td></tr>";
	print "<tr><td>6</td><td>関西</td><td>".$deliveryarea[6] ."</td></tr>";
	print "<tr><td>7</td><td>中国</td><td>".$deliveryarea[7] ."</td></tr>";
	print "<tr><td>8</td><td>四国</td><td>".$deliveryarea[8] ."</td></tr>";
	print "<tr><td>9</td><td>九州・沖縄</td><td>".$deliveryarea[9] ."</td></tr>";
	print "<tr><td colspan='2'>合計</td><td>".($deliveryarea[1]+$deliveryarea[2]+$deliveryarea[3]+$deliveryarea[4]+$deliveryarea[5]+$deliveryarea[6]+$deliveryarea[7]+$deliveryarea[8]+$deliveryarea[9]) ."</td></tr>";
	print "</table>";

//データの開放
$res->free();
$db->disconnect();
print '<BR><FORM action="/index.php"><INPUT TYPE="submit" VALUE="戻る"></FORM>';
?>

</body>
</html>