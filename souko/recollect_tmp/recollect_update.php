<?php
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>再集荷依頼完了</title>
</head>
<body>

<?php
//PEARの利用     -------(1)
require_once("DB.php");
require_once("Log.php");
//ログイン情報の読み込み
//require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
require_once("recollect_util.php");
require_once("../parts/selectvalue_souko.php");

// ロガーインスタンスを生成
$conf = array('mode'=>0777,'lineFormat' => '%1$s %3$s [%2$s] %4$s','timeFormat'=>'%Y-%m-%d %H:%M:%S');
$date = date("Ymd");
$user_id = $_SESSION['user_name'];
$log = &Log::factory('file', './log/' . $date . '.log', $user_id, $conf);

//全画面からのPOST値を取得
$value = $_POST["value"];

// POST値が存在するかチェック
if (empty($value)) {
	print '<FORM action="recollect_search.php"><INPUT TYPE="submit" VALUE="戻る"></FORM>';
	die("更新データが不正です。");
}

/* 倉庫DB更新情報 */
$deleteSql_lg = "delete from D_ASS_REQUEST where ASSCODE = ? ";

// 更新後データ取得sql
$selectSql = "select s.SELL_NO, s.SELL_STAT, s.MEM_NO, s.MEM_ID, s.PICKUP_NM_FST, s.PICKUP_NM_MID, s.PICKUP_REQ_DT, c.CD_DTL_NM  as PICKUP_REQ_TIME, s.PICK_DATA_EXPT, s.REG_DM from tsell s, tcodedtl c where c.CD_NO = '1041' and s.PICKUP_REQ_TIME = c.CD_DTL_NO and sell_no = ? ";

// データ更新sql(メモ欄の不要文言も削除する)
$updateSql = <<< EOF
    update TSELL
    set
        PICKUP_REQ_DT = ?,
        PICKUP_REQ_TIME = ?,
        SELL_STAT = ?,
        PICK_DATA_EXPT = ?,
        SELL_FORM_EXPT = ?,
        PICKUP_CORP_NO = ?,
        UPD_DM = ?,
        MEMO = REGEXP_REPLACE(MEMO,CHR(13) || CHR(10) ||'\[[0-9]+.+集荷予定\]|\[[0-9]+.+集荷予定\]','')
    where SELL_NO = ?
EOF;

// 更新データ
$data = array(
    $value['PICKUP_REQ_DT'],
    $value['PICKUP_REQ_TIME'],
    $value['SELL_STAT'],
    $value['PICK_DATA_EXPT'],
    $value['SELL_FORM_EXPT'],
    $value['PICKUP_CORP_NO'],
    date("YmdHis"),
    $value['SELL_NO'],
);

//print $sql;
//print_r($data);

// 設定ファイル読込
$ini = parse_ini_file("../parts/db.ini",true);

$log->info($value['SELL_NO'] . " START");
$log->info($value['SELL_NO'] . " (" . $value['BEFORE_PICKUP_REQ_DT'] . "," . $value['BEFORE_PICKUP_REQ_TIME'] . ") -> " . "(" . $value['PICKUP_REQ_DT'] . "," . $value['PICKUP_REQ_TIME'] . ")");

// ECDBコネクション取得
$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
//データベースへの接続
$ecdb = DB::connect($dsn_ec);
if(DB::isError($ecdb)){
    $log->err($ecdb->getDebugInfo());
    die("ECDBの接続に失敗しました。<br>" . $ecdb->getDebugInfo());
}
$ecdb->autoCommit(false);

// 倉庫DBコネクション取得
$dsn_lg = "oci8://". $ini['logi']['usr'] . ":" . $ini['logi']['pwd'] . "@" . $ini['logi']['dbn'];
//データベースへの接続
$lgdb = DB::connect($dsn_lg);
if(DB::isError($lgdb)){
    $log->err($lgdb->getDebugInfo());
    die("倉庫DBの接続に失敗しました。<br>" . $lgdb->getDebugInfo());
}
$lgdb->autoCommit(false);

// ECDB:データ更新
$res = $ecdb->query($updateSql,$data);
if(DB::isError($res)){
    $ecdb->rollback();
    $log->err($res->getDebugInfo());
    die("買取データ更新処理に失敗したため、処理を中断しました。<br>" . $res->getDebugInfo());
}

// logi:査定依頼を削除
$res = $lgdb->query($deleteSql_lg,convertAsscode($value['SELL_NO']));
if(DB::isError($res)){
    $ecdb->rollback();
    $lgdb->rollback();
    $log->err($res->getDebugInfo());
    die('倉庫査定依頼の削除に失敗しました。<br>' . $res->getDebugInfo());
}


$ecdb->commit();
$lgdb->commit();
// $ecdb->rollback();
//$lgdb->rollback();

$log->info($value['SELL_NO'] . " REQUEST SUCCESS");
$log->info($value['SELL_NO'] . " END");

// ECDB:更新後データを取得
$res = $ecdb->query($selectSql,$value['SELL_NO']);
if(DB::isError($res)){
    die($res->getDebugInfo());
}
$sell = $res->fetchRow(DB_FETCHMODE_ASSOC);
$res->free();

$lgdb->disconnect();
$ecdb->disconnect();

?>

<h3>以下の設定で再集荷を依頼しました。</h3>
<strong>買取受付番号：<?php echo $value['SELL_NO']; ?></strong> <br><br>

<table border="1">
    <tr>
        <td>会員No</td>
        <td>会員ID</td>
        <td>氏名</td>
        <td>伝票番号</td>
        <td>集荷希望日</td>
        <td>集荷希望時間帯</td>
        <td>集荷依頼出力ステータス</td>
        <td>買取ステータス</td>
        <td>買取申込日</td>
        <td>集荷会社</td>
    </tr>
   <tr>
        <td><?php echo $sell['MEM_NO']; ?></td>
        <td><?php echo $sell['MEM_ID']; ?></td>
        <td><?php echo $sell['PICKUP_NM_FST']." ".$sell['PICKUP_NM_MID']; ?></td>
        <td><?php echo ""; ?></td>
        <td><?php echo date("Y/m/d", strtotime($sell['PICKUP_REQ_DT'])); ?></td>
        <td><?php echo $sell['PICKUP_REQ_TIME']; ?></td>
        <td><?php echo $sell['PICK_DATA_EXPT']; ?></td>
        <td><?php echo Retsell_stat($sell['SELL_STAT']); ?></td>
        <td><?php echo date("Y/m/d", strtotime($sell['REG_DM'])); ?></td>
        <td><?php echo ""; ?></td>
    </TR>
</table>

<p><INPUT TYPE="button" VALUE="トップに戻る" onclick="location.href='/'"></p>

</body>
</html>
