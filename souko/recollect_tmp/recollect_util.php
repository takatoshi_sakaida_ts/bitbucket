<?php

function formatDateTime($str){
    /*
        yyyymmdd => yyyy/mm/dd
        yyyymmddhhmmss => yyyy/mm/dd hh:mm:ss
    */
    if(strlen($str) == 8 && preg_match("/^[0-9]+$/",$str)){
        return substr($str,0,4)."/".substr($str,4,2)."/".substr($str,6,2);
    } else if(strlen($str) == 14 && preg_match("/^[0-9]+$/",$str)){
        return substr($str,0,4)."/".substr($str,4,2)."/".substr($str,6,2)." ".substr($str,8,2).":".substr($str,10,2).":".substr($str,12,2);
    } else{
        return $str;
    }
}

function getPossiblePickDate($firstDt, $currentDt, $interval){
    /*
        *Dt引数はstrtotimeで変換可能な形式であることを前提とする
        戻り値は選択可能な日付を格納した配列
    */
    $dates = array();
    for($i = 1; $i <= $interval; $i++){
        $tmpDt = date("Y/m/d" , strtotime("{$firstDt} +{$i} day"));
        if(strtotime($tmpDt) > strtotime($currentDt)){
            // 明日以降の日付であれば選択対象にする
            $dates[] = date("Ymd" , strtotime("{$firstDt} +{$i} day"));
        }
    }
    return $dates;
}

function day_diff($date1, $date2) {
 
    // 日付をUNIXタイムスタンプに変換
    $timestamp1 = strtotime($date1);
    $timestamp2 = strtotime($date2);
 
    // 何秒離れているかを計算
    $seconddiff = abs($timestamp2 - $timestamp1);
 
    // 日数に変換(小数点切り上げ)
    $daydiff = ceil($seconddiff / (60 * 60 * 24));

    return $daydiff;
}

function convertAsscode($sellno){
    
    $prefix = "000";
    
    $str = $sellno;
    $str = ereg_replace("Z", "", $str);
    $str = ereg_replace("z", "", $str);
    $str = ereg_replace("R", "", $str);
    $str = ereg_replace("r", "", $str);
    $str = ereg_replace("H", "", $str);
    $str = ereg_replace("h", "", $str);
    if (strlen($str)==12){
    	 return $prefix . $str;
    } else{
        return $sellno;
    }
}


function getCorpName($corpNo){

    $PICKUP_CORP_NO_SAGAWA = "000000";

    if($corpNo == $PICKUP_CORP_NO_SAGAWA){
        $ret = "佐川";
    } else{
        $ret = "佐川以外";
    }
    return $ret;
}

?>