<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<link rel="stylesheet" type="text/css" href="z7069_list.css">
<title>Z7069発行済リスト一覧</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");

//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}

//関数定義ファイル読み込み
require_once("./functions.php");

/*
// 定数定義
$LIMIT = 10000;	// 1ページ最大表示数

//取得データの行数を取得
$dataCount = getRowCount($db);
// ページ数を計算
$pageCount = getPageCount($dataCount, $LIMIT);

//ページ位置を指定
if( isset($_GET['page']) ){
	$pageNum = intval($_GET['page']);
}else{
	$pageNum = 1;
}
*/
//$sql = getSql($pageNum, $LIMIT);
$sql = getSql();

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),NULL,NULL,NULL);
}

print "<h2>Z7069発行済リスト一覧</h2>";


// ページング
/*
if ( $pageCount > 1 ){
    print "<ul class='pageNav'>";
    // 前へ
    if( $pageNum > 1 ){
    	print "<li><a href='javascript:void(0)'>"."<< 前</a></li>";
    }
    else{
    	print "<li><span><< 前</span></li>";
    }
    for( $i=1; $i<=$pageCount; $i++ ){
    	if( $i == $pageNum ){
    		print "<li><span>".$i."</span></li>";
    	}
    	else{
    		print "<li><a href='javascript:void(0)'>". $i ."</a></li>";
    	}
    }
    // 次へ
    if( $pageNum < $pageCount ){
    	print "<li><a href='javascript:void(0)'>次 >></a></li>";
    }
    else{
    	print "<li><span>次 >></span></li>";
    }

    print "</ul>";
}*
*/
//検索結果の表示
print "<table id='detail' border='1'>\n";
//print "<tr bgcolor=#ccffff>\n";

//項目名の表示
print <<< EOM
<tr>
<th>新番号</th>
<th>発行日</th>
<th>計算日</th>
<th>振込日</th>
<th>店舗番号</th>
<th>店舗名</th>
<th>買取ステータス</th>
<th>計算箱数</th>
<th>計算点数</th>
<th>計算金額</th>
</tr>
EOM;

// 店舗名リスト(店舗番号をキーに名前を取得)
$storeNo = getStoreList();
//print_r($storeNo);
$j=1;
//検索結果の内容の表示処理
while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){

// 表示するのは99999「一時保管在庫買取」のみ
$store_no = ( isset($storeNo[$row['PICKUP_NM_MID']]) ) ? $storeNo[$row['PICKUP_NM_MID']] : '';
if($store_no != $storeNo["一時保管在庫買取"]){
    continue;
}


$sell_no = $row['SELL_NO'];
$date = date("Y/m/d",strtotime($row['SELL_FORM_GET_DT']));
$ass_dt = ( $row['ASS_DT'] != '' ) ? date("Y/m/d",strtotime($row['ASS_DT'])) : '';
$tran_dt = ( $row['TRAN_DT'] != '' ) ? date("Y/m/d",strtotime($row['TRAN_DT'])) : '';
// $store_no = ( isset($storeNo[$row['PICKUP_NM_MID']]) ) ? $storeNo[$row['PICKUP_NM_MID']] : '';
$name = $row['PICKUP_NM_FST'] .$row['PICKUP_NM_MID'];
$status = statusToStr($row['SELL_STAT']);
$box = $row['BOX'];
$amount = ( $row['TOT_ASS_AMT'] != '' ) ? number_format($row['TOT_ASS_AMT']) : '';
$valid_item_cnt = ( $row['VALID_ITEM_CNT'] != '' ) ? $row['VALID_ITEM_CNT'] : '';

	print "<tr>";
//買取受付番号
	print "<td>".$sell_no ."</td>";
//発行日   
    print "<td>".$date ."</td>";
    // 計算日
    print "<td>".$ass_dt ."</td>";
    // 振込日
    print "<td>".$tran_dt ."</td>";
    // 店舗番号
    print "<td>".$store_no ."</td>";
    //店舗名
    print "<td>".$name ."</td>";
    //買取ステータス
    print "<td>".$status ."</td>";
    //計算箱数
    print "<td>".$box ."</td>";
    //計算点数
    print "<td>".$valid_item_cnt ."</td>";
    //計算金額
    print "<td>".$amount ."</td>";
print "</tr>";
}
print "</table>";

// ページング
/*
if ( $pageCount > 1 ){
    print "<ul class='pageNav'>";
    // 前へ
    if( $pageNum > 1 ){
    	print "<li><a href='javascript:void(0)'><< 前</a></li>";
    }
    else{
    	print "<li><span><< 前</span></li>";
    }
    for( $i=1; $i<=$pageCount; $i++ ){
    	if( $i == $pageNum ){
    		print "<li><span>".$i."</span></li>";
    	}
    	else{
    		print "<li><a href='javascript:void(0)'>". $i ."</a></li>";
    	}
    }
    // 次へ
    if( $pageNum < $pageCount ){
    	print "<li><a href='javascript:void(0)'>次 >></a></li>";
    }
    else{
    	print "<li><span>次 >></span></li>";
    }
    print "</ul>";
}
*/
//データの開放
$res->free();
$db->disconnect();


?>
<BR>
<FORM action="/index.php" method="POST"><INPUT TYPE="submit" VALUE="メニューに戻る"></FORM>

<script>



</script>


</body>
</html>