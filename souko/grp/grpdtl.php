<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>グループ登録状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//買取受付番号の決定
$sql="select substr(m.genre3code,1,2),count(*) from tgoods_grpdtl p,tgoods_bko m where p.instorecode=m.instorecode group by substr(m.genre3code,1,2) order by substr(m.genre3code,1,2)";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
//	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>グループ登録状況</strong> <br><br>\n<HR>";
$j=1;
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td>カテゴリーコード</td><td>登録グループ件数</td>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
//項番
//	print "<td>".$j ."</td>";
//注文番号
//カテゴリ
	print "<td>".$row[0] ."</td>";
//件数
	print "<td align=right>".$row[1] ."</td>";
	print "</tr>";
	$j++;
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>