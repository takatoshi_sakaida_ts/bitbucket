<?php
if (isset($_SESSION["VOLPARTNER"])){
	unset($_SESSION["VOLPARTNER"]);
}
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
if(DB::isError($db)){
	die("connect failed:" . $db->getMessage());
}

/* 2019.05.20 ブックラPhase2対応 MOD START */
$sql = "select PARTNER_CD, PARTNER_NM, BANK_CD, BANK_BRANCH_CD, BANK_NM, BRANCH_NM, BANK_ACC_TP, BANK_ACC_NO, BANK_ACC_NM, PAY_HOLD_FLG";
$sql = $sql . " from TVOL_PARTNER where PARTNER_CD not like 'Z11__' and PARTNER_TP = 'Z1' and PULLDOWN_DISP_FLG = '1' order by PARTNER_CD asc ";
/* 2019.05.20 ブックラPhase2対応 MOD END */

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

$count = $res->numRows();

if ($count!='0') {
	while($row = $res->fetchRow()){
		/* 2019.05.20 ブックラPhase2対応 MOD START */
		// セッションに多次元配列として格納する
		$_SESSION['VOLPARTNER'][$row[0]] = array(
			'SELECT_DISP_NM'	=> $row[0].'　　'.$row[1],
			'PARTNER_CD'		=> $row[0], // 提携先コード
			'PARTNER_NM'		=> $row[1], // 提携先名称
			'BANK_CD'			=> $row[2], // 銀行コード
			'BANK_BRANCH_CD'	=> $row[3], // 支店コード
			'BANK_NM'			=> $row[4], // 銀行名
			'BRANCH_NM'			=> $row[5], // 支店名
			'KOUZASHURUI'		=> $row[6], // 口座種類
			'KOUZABANGOU'		=> $row[7], // 口座番号（暗号化済み）
			'KOUZAMEIGI'		=> $row[8], // 口座名義人
			'PAY_HOLD_FLG'		=> $row[9]  // 振込保留フラグ
		/* 2019.05.20 ブックラPhase2対応 MOD END */
		);
	}
	//print var_export($_SESSION['VOLPARTNER'] , true);
}
$res->free();
$db->disconnect();
?>
