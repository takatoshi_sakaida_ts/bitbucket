<?PHP
function db_insert(){
//PEARの利用     -------(1)
require_once("DB.php");
require_once("selectvalue_vol.php");
// 銀行情報のセット
/* 20190129 買取ファンディング MOD START */
//require_once("vol_bank.php");
$_SESSION["HSMAP"]["BANK_CD"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_CD"];
$_SESSION["HSMAP"]["BANK_BRANCH_CD"]= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_BRANCH_CD"];
$_SESSION["HSMAP"]["BANK_NM"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_NM"];
$_SESSION["HSMAP"]["BRANCH_NM"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BRANCH_NM"];
$_SESSION["HSMAP"]["KOUZASHURUI"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZASHURUI"];
$_SESSION["HSMAP"]["KOUZABANGOU"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZABANGOU"];
$_SESSION["HSMAP"]["KOUZAMEIGI"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZAMEIGI"];
$sellno_head = $_SESSION["HSMAP"]["DANTAI"];
/* 20190129 買取ファンディング MOD END */

//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
/* 20190328 買取ファンディング MOD START */
	//echo "Fail\n" . DB::errorMessage($db) . "\n";
	die("connect failed:" . $db->getMessage());
/* 20190328 買取ファンディング MOD END */
}
$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]="";
//買取受付番号の決定
/* 20161220 編集 Start */
/* 20190328 買取ファンディング MOD START */
//$sql="select max(sell_no) from tsell where sell_no like ";
/*
if($sellno_head < '100') {
$sql = $sql."'Z10" . $sellno_head . "%'";
} else if($sellno_head >= '100') {
$sql = $sql."'Z1" . $sellno_head . "%'";
}
*/
/* 20161220 End */
//$res = $db->query($sql);
$sql="select max(sell_no) from tsell where sell_no like ?";
$res = $db->query($sql,$sellno_head."%");
/* 20190328 買取ファンディング MOD END */
if(DB::isError($res)){
/* 20190328 買取ファンディング MOD START */
	//$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	die($res->getMessage());
/* 20190328 買取ファンディング MOD END */
}
/* 20190328 買取ファンディング MOD START */
//while($row = $res->fetchRow()){
/* 20161220 編集 Start */
//if($row[0] == null && $sellno_head >= '100') {
//	$sell_no = "00000000";
//} else {
//	$sell_no=substr($row[0],5,8);
//	print $row[0]."<BR>";
//}
/* 20161220 編集 End */
//}
$sell_no = 0;
while($row = $res->fetchRow()){
    if(isset($row[0]) && $row[0] != null){
    	$sell_no=substr($row[0],5,8);
    }
//	print $row[0]."<BR>";
}
/* 20190328 買取ファンディング MOD END */

$res->free();
//print "sell_noは".$sell_no."<br>";
$sell_no=$sell_no + 1;
//print "sell_no(+1)は".$sell_no."<br>";
$sell_no = sprintf("%08d",$sell_no);
/* 20161220 編集 Start */
/* 20190328 買取ファンディング MOD START */
/*
if($sellno_head < '100') {
$sell_no="Z10".$sellno_head.$sell_no;
} else if($sellno_head >= '100') {
$sell_no = "Z1".$sellno_head.$sell_no;
}
*/
/* 20161220 End */
$sell_no=$sellno_head.$sell_no;
/* 20190328 買取ファンディング MOD END */
//
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='';
}else
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='000000000000000';
}
//ガイド発送が不要なら申込書自動到着
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
}else{
	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
	$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]=date("Ymd");

//RoomToReadの場合は申込書の自動チェックはしない
/**
	if ($sellno_head=='01')
	{
		$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
	}else{
		$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
	}
*/
}

/* 2019.05.20 ブックラPhase2対応 MOD START 「振込保留」設定は提携先情報から取得する */
//◆「振込保留」にする場合、下記に追記すること◆
//Room to Read、ピースウィンズ・ジャパン、シャンティ、シャプラニール、JEN、かながわキンタロウ、売って支援プログラム、しょうがっこうをおくる会、結核予防会、ACE（エース）
//町田ゼルビア、大阪YMCA、グッドネーバーズ・ジャパン(30)、国連ウィメン日本協会、湘南ふじさわシニアネット、アジア保健研修所、ウィメンズアクションネットワーク、アーユス仏教国際協力ネットワーク
//しみん基金・こうべ、売って支援プログラム（熊本地震）、東京都日中友好協会、結核予防会、民際センター、しょうがっこうをおくる会、神奈川大学、ICAN（アイキャン）
//地球環境基金、HFI（Hope and Faith International）、ACE（エース）、3keys（スリーキーズ）、町田ゼルビア、SC相模原、幼い難民を考える会(CYR)、JHP学校をつくる会
//北海道森と緑の会、アクセス、ケア・インターナショナルジャパン、世界の子どもにワクチンを(JCV)、ジャパンハート、ブリッジ エーシア ジャパン(BAJ)
//売って支援プログラム（東日本大震災）、よこはまユース、ボーイスカウト日本連盟、国境なき医師団、全国骨髄バンク、国境なき子どもたち、ちばのWA

/* 2019.01.29 買取ファンディング対応 MOD START 現在分岐は不要で一律PAY_HOLD_FLG='1' */
//if ($sellno_head=='01'||$sellno_head=='02'||$sellno_head=='03'||$sellno_head=='04'||$sellno_head=='05'||$sellno_head=='07'||$sellno_head=='09'||$sellno_head=='10'||$sellno_head=='11'||$sellno_head=='16'||$sellno_head=='19'||$sellno_head=='29'||$sellno_head=='30'||$sellno_head=='31'||$sellno_head=='32'||$sellno_head=='33'||$sellno_head=='34'||$sellno_head=='35'||$sellno_head=='36'||$sellno_head=='37'||$sellno_head=='38'||$sellno_head=='39'||$sellno_head=='40'||$sellno_head=='41'||$sellno_head=='42'||$sellno_head=='43'||$sellno_head=='44'||$sellno_head=='45'||$sellno_head=='46'||$sellno_head=='47'||$sellno_head=='48'||$sellno_head=='49'||$sellno_head=='50'||$sellno_head=='51'||$sellno_head=='52'||$sellno_head=='53'||$sellno_head=='54'||$sellno_head=='55'||$sellno_head=='56'||$sellno_head=='57'||$sellno_head=='58'||$sellno_head=='59'||$sellno_head=='60'||$sellno_head=='61'||$sellno_head=='62'||$sellno_head=='63'||$sellno_head=='64'||$sellno_head=='65'||$sellno_head=='66'||$sellno_head=='67'||$sellno_head=='68'){
//	$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='1';
//}else{
//	$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='0';
//}
/* 2019.01.29 買取ファンディング対応 MOD END 現在分岐は不要で一律PAY_HOLD_FLG='1' */
/* 2019.05.20 ブックラPhase2対応 MOD END 「振込保留」設定は提携先情報から取得する */

//◆「買取申込書受領」に自動チェックを入れる場合、下記に追記すること◆
//移動図書館支援（シャンティ）、神奈川大学、売って支援プログラム、しょうがっこうをおくる会、結核予防会、地球環境基金、民際センター
//HFI（Hope and Faith International）、ACE（エース）、地球の友と歩む会、3keys（スリーキーズ）、町田ゼルビア、SC相模原、幼い難民を考える会(CYR)
//JHP学校をつくる会、北海道森と緑の会、アクセス、ケア・インターナショナルジャパン、世界の子どもにワクチンを(JCV)、ジャパンハート、ブリッジ エーシア ジャパン(BAJ)、
//大阪YMCA、グッドネーバーズ・ジャパン、国連ウィメン日本協会、湘南ふじさわシニアネット、アジア保健研修所(AHI)、ウィメンズアクションネットワーク
//アーユス仏教国際協力ネットワーク、しみん基金・こうべ、売って支援プログラム（熊本地震）、東京都日中友好協会、結核予防会、民際センター、しょうがっこうをおくる会
//神奈川大学、ICAN（アイキャン）、地球環境基金、HFI（Hope and Faith International）、ACE（エース）、3keys（スリーキーズ）、町田ゼルビア、SC相模原
//幼い難民を考える会(CYR)、JHP学校をつくる会、北海道森と緑の会、アクセス、ケア・インターナショナルジャパン、世界の子どもにワクチンを(JCV)、ジャパンハート
//ブリッジ エーシア ジャパン(BAJ)、売って支援プログラム（東日本大震災）、よこはまユース、ボーイスカウト日本連盟、国境なき医師団、全国骨髄バンク、国境なき子どもたち、ちばのWA

/* 2019.01.29 買取ファンディング対応 MOD START 現在すべてガイド不要のため一律FORM_ARRIVAL='1' */
//if ($sellno_head=='06'||$sellno_head=='07'||$sellno_head=='08'||$sellno_head=='09'||$sellno_head=='10'||$sellno_head=='11'||$sellno_head=='13'||$sellno_head=='14'||$sellno_head=='15'||$sellno_head=='16'||$sellno_head=='17'||$sellno_head=='18'||$sellno_head=='19'||$sellno_head=='20'||$sellno_head=='21'||$sellno_head=='22'||$sellno_head=='23'||$sellno_head=='24'||$sellno_head=='25'||$sellno_head=='26'||$sellno_head=='27'||$sellno_head=='28'||$sellno_head=='29'||$sellno_head=='30'||$sellno_head=='31'||$sellno_head=='32'||$sellno_head=='33'||$sellno_head=='34'||$sellno_head=='35'||$sellno_head=='36'||$sellno_head=='37'||$sellno_head=='38'||$sellno_head=='39'||$sellno_head=='40'||$sellno_head=='41'||$sellno_head=='42'||$sellno_head=='43'||$sellno_head=='44'||$sellno_head=='45'||$sellno_head=='46'||$sellno_head=='47'||$sellno_head=='48'||$sellno_head=='49'||$sellno_head=='50'||$sellno_head=='51'||$sellno_head=='52'||$sellno_head=='53'||$sellno_head=='54'||$sellno_head=='55'||$sellno_head=='56'||$sellno_head=='57'||$sellno_head=='58'||$sellno_head=='59'||$sellno_head=='60'||$sellno_head=='61'||$sellno_head=='62'||$sellno_head=='63'||$sellno_head=='64'||$sellno_head=='65'||$sellno_head=='66'||$sellno_head=='67'||$sellno_head=='68'){
/* 2019.05.27 ブックラPhase2対応 MOD START */
//	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
/* 2019.05.27 ブックラPhase2対応 MOD END */
//}
/* 2019.01.29 買取ファンディング対応 MOD END 現在すべてガイド不要のため一律FORM_ARRIVAL='1' */

//print $sell_no."<BR>";
/* 2019.05.20 ブックラPhase2対応 MOD START */
$sql="INSERT INTO tsell(".
"SELL_NO,".
"SELL_STAT,".
"PICKUP_NM_FST,".
"PICKUP_NM_MID,".
"PICKUP_NM_LAST,".
"PICKUP_NM_ETC,".
"PICKUP_ZIP_CD,".
"PICKUP_ZIP_ADDR1,".
"PICKUP_ZIP_ADDR2,".
"PICKUP_ZIP_ADDR3,".
"PICKUP_DTL_ADDR,".
"PICKUP_TEL_NO,".
"PICKUP_TEL_NO_2,".
"PICKUP_REQ_DT,".
"PICKUP_REQ_TIME,".
"BOX,".
"NOTE_YN,".
"DISPOSE,".
"RECEIPT_TP,".
"MEM_BIRTH_DT,".
"MEM_SEX,".
"MEM_JOB,".
"PAY_TP,".
"DL_STAT_LABEL,".
"BANK_CD,".
"BANK_BRANCH_CD,".
"BANK_NM,".
"BRANCH_NM,".
"BANK_ACC_TP,".
"BANK_ACC_NO,".
"BANK_ACC_NM,".
"MEMO,".
"FORM_ARRIVAL,".
"PICK_DATA_EXPT,".
"SELL_FORM_EXPT,".
"ADMIT_DATA_EXPT,".
"ASSES_CNT,".
"REG_DM,".
"UPD_DM,".
"PAY_HOLD_FLG,".
"AUTH_SEND_FLG,".
"SELL_FORM_GET_DT,".
"USER_ID".
") VALUES('"
	.$sell_no."','"
	."01','"
	.$_SESSION["HSMAP"]["PICKUP_NM_FST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_MID"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_LAST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_ETC"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_REQ_DTY"].$_SESSION["HSMAP"]["PICKUP_REQ_DTM"].$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]."','"
	."0".$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]."','"
	.$_SESSION["HSMAP"]["BOX"]."','"
	."0','"
	."0','"
	."2','"
	."19500101','"
	.$_SESSION["HSMAP"]["MEM_SEX"]."','"
	."09','"
	."1','"
	.$_SESSION["HSMAP"]["DL_STAT_LABEL"]."','"
	.$_SESSION["HSMAP"]["BANK_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_BRANCH_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_NM"]."','"
	.$_SESSION["HSMAP"]["BRANCH_NM"]."','"
	.$_SESSION["HSMAP"]["KOUZASHURUI"]."','"
	.$_SESSION["HSMAP"]["KOUZABANGOU"]."','"
	.$_SESSION["HSMAP"]["KOUZAMEIGI"]."','"
	.$_SESSION["HSMAP"]["MEMO"]. ":団体名：".str_replace("'","",$_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["PARTNER_NM"]). "　ガイド発送:" . Retgaido($_SESSION["HSMAP"]["gaido"]) . "','"
	.$_SESSION["HSMAP"]["FORM_ARRIVAL"]."','"
	."1','"
	."1','"
	."1','"
	."0','"
	.date("YmdHis")."','"
	.date("YmdHis")."','".$_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["PAY_HOLD_FLG"]."','0','"
	.$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]."','"
	.$_SESSION["user_id"]."')";
/* 2019.05.20 ブックラPhase2対応 MOD END */
//print "<br>".$sql;
$db->autoCommit( false ); 
$res2 = $db->query($sql);
if(DB::isError($res2)){
/* 20190328 買取ファンディング MOD START */
	//print $res2->getMessage();
    //$res2->getDebugInfo();
	//$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
    $db->rollback();
	die("tsell insert failed:" . $res2->getMessage());
/* 20190328 買取ファンディング MOD END */
}
//
output_log($sql,$_SESSION["user_id"],'vol_entry_sql');
$sql="INSERT INTO tsellassessment (".
"SELL_NO,".
"REG_DM".
") VALUES('"
	.$sell_no."','"
	.date("YmdHis")."')";

$res3 = $db->query($sql);
if(DB::isError($res3)){
/* 20190328 買取ファンディング MOD START */
	//print $res3->getMessage();
    //$res3->getDebugInfo();
	//$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
    $db->rollback();
	die("tsellassessment insert failed:".$res3->getMessage());
/* 20190328 買取ファンディング MOD END */
}
		$_SESSION["HSMAP"]["sell_no"]=$sell_no;
$db->commit();
output_log($sell_no,$_SESSION["user_id"],'vol_entry');
output_log($sql,$_SESSION["user_id"],'vol_entry_sql');
//print "<br>".$sql;
//データの開放
$db->disconnect();
}
?>