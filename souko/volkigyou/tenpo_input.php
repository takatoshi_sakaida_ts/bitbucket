<?PHP
require '../parts/pagechk.inc';
/* 2019.07.18 ブックラPhase2対応 ADD START */
require 'volkigyou_partner_info_Individual.php';
/* 2019.07.18 ブックラPhase2対応 ADD END */
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<script type="text/javascript" src="volkigyou_check.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>店舗在庫買取受付画面(Z7069)</title>
<STYLE TYPE="text/css"> 
<!-- 
#menu1 { 
border-collapse: collapse; /** 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /** セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /** セルの背景色 */ 
padding: 3px; /** セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /** セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /** セルの背景色 */ 
padding: 3px; /** セル内の余白 */ 
font-weight:normal;
width:20%;
}

#suggest {
    position: absolute;
    background-color: #FFFFFF;
    border: 1px solid #CCCCFF;
    font-size: 90%;
    width: 400px;
}
#suggest div {
    display: block;
    width: 400px;
    overflow: hidden;
    white-space: nowrap;
}
#suggest div.select{ /* キー上下で選択した場合のスタイル */
    color: #FFFFFF;
    background-color: #3366FF;
}
#suggest div.over{ /* マウスオーバ時のスタイル */
    background-color: #99CCFF;
}

-->
</style>
</head>
<body>
<form method="POST" action="tenpo_confirm.php" name="volfm" onsubmit="return beforeSubmit();">
<b>Z7069 店舗在庫買取受付画面</b><br>

<p>店舗番号を入力してください</p>
<TABLE ID="menu1" width="900">
  <tr>
    <th><font color="red">*</font>店舗番号</th>
    <td>
	<input type="text"a id="storeNo" name="TENPO_NO" size="10" maxlength="5" style="ime-inactive;" value="<?PHP if(isset($_SESSION["HSMAP"]["TENPO_NO"])) print $_SESSION["HSMAP"]["TENPO_NO"];?>" onkeyup="storeCheck();"> 該当店舗：<span id="store"></span>
    </td>
  </tr>
<!--
  <tr>
    <th>店舗名</th>
    <td>
		<input type="text"a id="keyword" name="TENPO_NAME" size="50" style="display:block;" value="<?PHP if(isset($_SESSION["HSMAP"]["TENPO_NAME"])) print $_SESSION["HSMAP"]["TENPO_NAME"];?>">
        <div id="suggest" style="display:none;"></div>
    </td>
  </tr>
-->
<!--
  <tr>
    <th>集荷希望年月日<font color=red>　*　</font></th>
    <td>
	<input type="text" name="PICKUP_REQ_DTY" size="6" maxlength="4" style="ime-mode:inactive;" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTY"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTY"];
}
else{
$date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
$date_yesterday = $date_today + (86400*4);
print date('Y', $date_yesterday);
}
print '" >年';
?>
	<input type="text" name="PICKUP_REQ_DTM" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTM"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTM"];
}
else{
print date('m', $date_yesterday);
}
print '" >月';
?>
	<input type="text" name="PICKUP_REQ_DTD" size="6" maxlength="2" style="ime-mode:inactive;" value="
<?PHP

if(isset($_SESSION["HSMAP"]["PICKUP_REQ_DTD"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_DTD"];
}

print '" >日';
?>
    </td>
  </tr>
  <tr>
    <th>集荷希望時間帯<font color=red>　*　</font></th>
    <td>
	<select name="PICKUP_REQ_TIME">
		<option value="0" >選択してください
		<option value="1" > 9:00-13:00
		<option value="2" >13:00-15:00
		<option value="4" >15:00-18:00
		<option value="5" >18:00-20:00
	</select>
　　　【注意】<font color=red>当日集荷</font>の場合「18-21時」のみ選択可。15時過ぎたら翌日以降で。
    </td>
  </tr>
-->
  <tr>
    <th>メモ</th>
    <td>
	<textarea name="MEMO" cols="80" rows="4">
<?PHP
if(isset($_SESSION["HSMAP"]["MEMO"])){ 
    print $_SESSION["HSMAP"]["MEMO"]; 
}
?>
</textarea>
    </td>
  </tr>

</table>

<input type="hidden" name="BOX" value="1">
<input type="hidden" name="PICKUP_REQ_DTY" value="<?php echo date('Y');?>">
<input type="hidden" name="PICKUP_REQ_DTM" value="<?php echo date('m');?>">
<input type="hidden" name="PICKUP_REQ_DTD" value="<?php echo date('d') + 1;?>">
<input type="hidden" name="PICKUP_REQ_TIME" value="5">

<input type="hidden" name="DANTAI_index" value="
<?PHP

if(isset($_SESSION["HSMAP"]["DANTAI_index"])){
	print $_SESSION["HSMAP"]["DANTAI_index"];
}

print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"])){
	print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
}
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"])){
	print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
}
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["MEM_SEX"])){
	print $_SESSION["HSMAP"]["MEM_SEX"];
}
print '" >';
?>
<input type="hidden" name="gaido_index" value="
<?PHP
if(isset($_SESSION["HSMAP"]["gaido"])){
	print ($_SESSION["HSMAP"]["gaido"]);
}
print '" >';
?>
<input type="hidden" name="postname" value="">

<BR>

<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="EntCan()">
<input type="submit" value="　　登録　　" onClick="SBEntCheck()">
</FORM>

<script type="text/javascript" src="ajax.js"></script>
<?php if(false):?>
<script type="text/javascript" src="suggest.js"></script>
<script type="text/javascript" src="json2.js"></script>
<?php endif;?>

<script>

var storeSelected = false;
var errText = "<font color='red'>該当なし</font>";


function beforeSubmit(){
    if(!storeSelected){
        //alert("正しい店舗番号を入力してください。");
        document.getElementById("store").innerHTML = errText;
        return false;
    }
    else return true;
}

function storeCheck(){
    var storeNo = document.getElementById("storeNo");
    var store = document.getElementById("store");
    
    // 数値以外 or 5桁以外は度外視
    var val = storeNo.value;
    if(!(val.match(/^[0-9]+/)) || val.length != 5){
        store.innerHTML = errText;
        storeSelected = false;
        return;
    }
    // ajaxで該当店舗名を取得
    url = "returnStoreName.php?storeNo=" + storeNo.value;
    //console.log(url);
    ajax.get(url, function(res){
        //console.debug("res:" + res);
        if( res != errText ){
            storeSelected = true;
            store.innerHTML = res;
        }
        else{
            storeSelected = false;
        }
    });
}
// セッション保存時のアクセス対策
if(document.getElementById("storeNo").value.length > 0) storeCheck();
/*
// suggest初期化
function startSuggest(){
    var lists;
    ajax.get('./getStoreList.php', function(res){
        var lists = JSON.parse(res);
        new Suggest.Local(
            "keyword",    // 入力のエレメントID
            "suggest", // 補完候補を表示するエリアのID
            lists,      // 補完候補の検索対象となる配列
            {dispMax: 15, interval: 1000}); // オプション
    });
}
window.onLoad = startSuggest();
*/
</script>

</body>
</html>