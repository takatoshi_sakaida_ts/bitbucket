<?PHP
function Retvol($category)
{
	//カテゴリ名称返却
	/* 2019.05.20 ブックラPhase2対応 MOD START */
	switch ($category){
		case "Z7001":
			$retvalue= "NTT大阪支店";
			break;
		case "Z7002":
			$retvalue= "スターバックス";
			break;
		case "Z7003":
			$retvalue= "帝人";
			break;
		case "Z7004":
			$retvalue= "セガサミーグループ";
			break;
		case "Z7005":
			$retvalue= "NTTネオメイト";
			break;
		case "Z7006":
			$retvalue= "NEC(東日本大震災支援)";
			break;
		case "Z7007":
			$retvalue= "キャドバリージャパン";
			break;
		case "Z7008":
			$retvalue= "ファミリーマート";
			break;
		case "Z7009":
			$retvalue= "結核予防会";
			break;
		case "Z7010":
			$retvalue= "ナック（国境なき医師団）";
			break;
		case "Z7011":
			$retvalue= "アイエスエフネット";
			break;
		case "Z7012":
			$retvalue= "インテリジェンス";
			break;
		case "Z7013":
			$retvalue= "チャーティス・ファー・イースト・ホールティングス";
			break;
		case "Z7014":
			$retvalue= "民際センター";
			break;
		case "Z7015":
			$retvalue= "ＤＮＰグループ労連";
			break;
		case "Z7016":
			$retvalue= "しょうがっこうをおくる会";
			break;
		case "Z7017":
			$retvalue= "レオパレス21";
			break;
		case "Z7018":
			$retvalue= "神奈川大学";
			break;
		case "Z7019":
			$retvalue= "ICAN（アイキャン）";
			break;
		case "Z7020":
			$retvalue= "阪急阪神ホールディングス";
			break;
		case "Z7021":
			$retvalue= "ソニー";
			break;
		case "Z7022":
			$retvalue= "双日エアロスペース(地球環境基金)";
			break;
		case "Z7023":
			$retvalue= "売って支援プログラム（東日本大震災）";
			break;
		case "Z7024":
			$retvalue= "西友";
			break;
		case "Z7025":
			$retvalue= "日本経済新聞文化部(日本赤十字社 東京都支部)";
			break;
		case "Z7026":
			$retvalue= "朝日新聞社論説委員室";
			break;
		case "Z7027":
			$retvalue= "HFI（Hope and Faith International）";
			break;
		case "Z7028":
			$retvalue= "ACE（エース）";
			break;
		case "Z7029":
			$retvalue= "マンパワー・ジャパン";
			break;
		case "Z7030":
			$retvalue= "中日本ハイウェイ・パトロール東京";
			break;
		case "Z7031":
			$retvalue= "三菱商事";
			break;
		case "Z7032":
			$retvalue= "地球の友と歩む会";
			break;
		case "Z7033":
			$retvalue= "損保ジャパン奈良支店";
			break;
		case "Z7034":
			$retvalue= "日本興亜損害保険";
			break;
		case "Z7035":
			$retvalue= "3keys（スリーキーズ）";
			break;
		case "Z7036":
			$retvalue= "ライフ";
			break;
		case "Z7037":
			$retvalue= "★スポット対応企業";
			break;
		case "Z7038":
			$retvalue= "町田ゼルビア";
			break;
		case "Z7039":
			$retvalue= "SC相模原";
			break;
		case "Z7040":
			$retvalue= "幼い難民を考える会（CYR）";
			break;
		case "Z7041":
			$retvalue= "アフラック";
			break;
		case "Z7042":
			$retvalue= "JHP学校をつくる会";
			break;
		case "Z7043":
			$retvalue= "日立建機";
			break;
		case "Z7044":
			$retvalue= "北海道森と緑の会";
			break;
		case "Z7045":
			$retvalue= "ユニー";
			break;
		case "Z7046":
			$retvalue= "埼玉県自動車販売店協会（自販連埼玉支部）";
			break;
		case "Z7047":
			$retvalue= "マルハン";
			break;
		case "Z7048":
			$retvalue= "アドビシステムズ";
			break;
		case "Z7049":
			$retvalue= "FC在庫買取";
			break;
		case "Z7050":
			$retvalue= "大塚商会(3keys)";
			break;
		case "Z7051":
			$retvalue= "森ビル都市企画";
			break;
		case "Z7052":
			$retvalue= "アクセス";
			break;
		case "Z7053":
			$retvalue= "ブリヂストンスポーツ";
			break;
		case "Z7054":
			$retvalue= "ケア・インターナショナルジャパン";
			break;
		case "Z7055":
			$retvalue= "世界の子どもにワクチンを(JCV)";
			break;
		case "Z7056":
			$retvalue= "共立メンテナンス";
			break;
		case "Z7057":
			$retvalue= "ジャパンハート";
			break;
		case "Z7058":
			$retvalue= "MSD";
			break;
		case "Z7059":
			$retvalue= "ブリッジ エーシア ジャパン(BAJ)";
			break;
		case "Z7060":
			$retvalue= "TBS";
			break;
		case "Z7061":
			$retvalue= "TBS(フィリピン台風支援)";
			break;
		case "Z7062":
			$retvalue= "岡谷鋼機";
			break;
		case "Z7063":
			$retvalue= "横浜開港祭";
			break;
		case "Z7064":
			$retvalue= "商船三井";
			break;
		case "Z7065":
			$retvalue= "広栄化学工業";
			break;
		case "Z7066":
			$retvalue= "ハグオール";
			break;
		case "Z7067":
			$retvalue= "NTT労働組合 西日本";
			break;
		case "Z7068":
			$retvalue= "日本ラグビーフットボール協会";
			break;
		case "Z7069":
			$retvalue= "店舗在庫買取";
			break;
		case "Z7070":
			$retvalue= "神戸YMCA";
			break;
		case "Z7071":
			$retvalue= "北洋銀行";
			break;
		case "Z7072":
			$retvalue= "なおちゃんを救う会";
			break;
		case "Z7073":
			$retvalue= "大日本印刷株式会社　honto返品買取";
			break;
		case "Z7074":
			$retvalue= "シニアライフセラピー研究所";
			break;
		case "Z7075":
			$retvalue= "NTT労働組合　東日本本部";
			break;
		case "Z7076":
			$retvalue= "プリック ジャパン ビューティー";
			break;
		case "Z7077":
			$retvalue= "損害保険ジャパン日本興亜（東日本大震災支援）";
			break;
		case "Z7078":
			$retvalue= "損害保険ジャパン日本興亜（熊本地震支援）";
			break;
		case "Z7079":
			$retvalue= "ニッポン放送";
			break;
		case "Z7080":
			$retvalue= "NHK渋谷";
			break;
		case "Z7081":
			$retvalue= "NHK名古屋";
			break;
		case "Z7082":
			$retvalue= "三井不動産レジデンシャル（東日本大震災支援）";
			break;
		case "Z7083":
			$retvalue= "三井不動産レジデンシャル（熊本地震支援）";
			break;
		case "Z7084":
			$retvalue= "三菱地所コミュニティ（東日本大震災支援）";
			break;
		case "Z7085":
			$retvalue= "関西テレビ";
			break;
		case "Z7086":
			$retvalue= "かわさき市民活動センター";
			break;
		case "Z7087":
			$retvalue= "山田養蜂場";
			break;
		case "Z7088":
			$retvalue= "アリさんマークの引越社";
			break;
		case "Z7089":
			$retvalue= "NTTデータ（東日本大震災支援）";
			break;
		case "Z7090":
			$retvalue= "日本バルカー工業（東日本大震災支援）";
			break;
		case "Z7091":
			$retvalue= "ルネサスイーストン（シャンティ）";
			break;
		case "Z7092":
			$retvalue= "住友生命札幌支社（シャンティ）";
			break;
		case "Z7093":
			$retvalue= "三井住友カード（東日本大震災支援）";
			break;
		case "Z7094":
			$retvalue= "日経印刷（東日本大震災支援）";
			break;
		case "Z7095":
			$retvalue= "アトミクス";
			break;
		case "Z7096":
			$retvalue= "LIC";
			break;
		case "Z7097":
			$retvalue= "JAIFA";
			break;		
		case "Z7098":
			$retvalue= "SOMPOシステムズ（移動図書館支援）";
			break;	
		case "Z7099":
			$retvalue= "日経印刷　長野（東日本大震災支援）";
			break;	
		case "Z7100":
			$retvalue= "フロムヴイ";
			break;	
		case "Z7101":
			$retvalue= "愛知県職員組合(移動図書館支援）";
			break;	
		case "Z7102":
			$retvalue= "ベクトルフラックス";
			break;	
		case "Z7103":
			$retvalue= "バリューブックス";
			break;	
		case "Z7104":
			$retvalue= "東京海上グループ『未来塾』（熊本地震支援）";
			break;	
		case "Z7105":
			$retvalue= "大阪商工信用金庫";
			break;	
		case "Z7106":
			$retvalue= "三菱UFJニコス";
			break;	
		case "Z7107":
			$retvalue= "アカツキ（BUY王）";
			break;	
		case "Z7108":
			$retvalue= "アリアンツ火災海上保険（東日本大震災支援）";
			break;	
		case "Z7109":
			$retvalue= "損害保険ジャパン日本興亜　関西総務部（東日本大震災支援）";
			break;	
		case "Z7110":
			$retvalue= "ブックマーケティング";
			break;	
		case "Z7111":
			$retvalue= "正蓮寺";
			break;	
		case "Z7112":
			$retvalue= "オムロンフィールドエンジニアリング（熊本地震支援）";
			break;	
		case "Z7113":
			$retvalue= "LU（ブリッジ エーシア ジャパン）";
			break;	
		case "Z7114":
			$retvalue= "サスケコーポレーション";
			break;	
		case "Z7115":
			$retvalue= "ABC（Room to Read）";
			break;	
		case "Z7116":
			$retvalue= "楽天";
			break;	
		case "Z7117":
			$retvalue= "神奈川県医療福祉施設協同組合";
			break;	
		case "Z7118":
			$retvalue= "日本通運【ボランティア】（かながわキンタロウ）";
			break;	
		case "Z7119":
			$retvalue= "★スポット対応企業（東日本支援10％UP）";
			break;
		case "Z7120":
			$retvalue= "★スポット対応企業（熊本支援10％UP）";
			break;
		case "Z7121":
			$retvalue= "ジェーシービー(Room to Read)";
			break;
		case "Z7122":
			$retvalue= "新宿南エネルギーサービス(地球環境基金)";
			break;
		case "Z7123":
			$retvalue= "J-WAVE(地球環境基金)";
			break;
		case "Z7124":
			$retvalue= "西日本高速道路サービス四国";
			break;
		case "Z7125":
			$retvalue= "セディナ(3keys)";
			break;
		case "Z7126":
			$retvalue= "三井不動産レジデンシャル（平成30年7月豪雨災害）";
			break;
		case "Z7127":
			$retvalue= "日本経済新聞　生活情報部（平成30年7月豪雨災害）";
			break;
		case "Z7128":
			$retvalue= "損害保険ジャパン日本興亜（平成30年7月豪雨災害支援）";
			break;
		case "Z7129":
			$retvalue= "こども食堂支援機構　";
			break;
		case "Z7130":
			$retvalue= "西日本高速道路サービス四国（売って支援10％UP）";
			break;
		case "Z7131":
			$retvalue= "座間市社会福祉協議会";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
/* 2019.05.20 ブックラPhase2対応 MOD END */
	return $retvalue;
}

function Rethiduke($index)
{
//日付整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2);
}
	return $retvalue;
}
function Rethidukejikan($index)
{
//日付時間整形
if (empty($index)){
	$retvalue='未設定';
}else{
	$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2). " ".substr($index,8,2). ":".substr($index,10,2). ":" .substr($index,12,2);
}
	return $retvalue;
}
function Retzip($index)
{
//郵便番号整形
if (empty($index)){
	$retvalue='〒';
}else{
	$retvalue = "〒".substr($index,0,3). "-" .substr($index,3,4);
}
	return $retvalue;
}
function Rettel($index)
{
//電話番号整形
if (empty($index)){
	$retvalue='　';
}else{
	switch (substr($index,1,1)){
		case 3:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		case 6:
			$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
			break;
		default:
			if (strlen($index)==10){
				$retvalue = substr($index,0,3). "-" .substr($index,3,3). "-" .substr($index,6,4);
			}else{
				$retvalue = substr($index,0,3). "-" .substr($index,3,4). "-" .substr($index,7,4);
			}
			break;
	}
}
	return $retvalue;
}
function RetPrefecture($index)
{
	switch ($index){
		case 0;
			$retvalue = "選択してください";
			break;
		case 1;
			$retvalue = "北海道";
			break;
		case 2;
			$retvalue = "青森県";
			break;
		case 3;
			$retvalue = "岩手県";
			break;
		case 4;
			$retvalue = "秋田県";
			break;
		case 5;
			$retvalue = "宮城県";
			break;
		case 6;
			$retvalue = "山形県";
			break;
		case 7;
			$retvalue = "福島県";
			break;
		case 8;
			$retvalue = "茨城県";
			break;
		case 9;
			$retvalue = "栃木県";
			break;
		case 10;
			$retvalue = "群馬県";
			break;
		case 11;
			$retvalue = "埼玉県";
			break;
		case 12;
			$retvalue = "千葉県";
			break;
		case 13;
			$retvalue = "東京都";
			break;
		case 14;
			$retvalue = "神奈川県";
			break;
		case 15;
			$retvalue = "新潟県";
			break;
		case 16;
			$retvalue = "富山県";
			break;
		case 17;
			$retvalue = "石川県";
			break;
		case 18;
			$retvalue = "福井県";
			break;
		case 19;
			$retvalue = "山梨県";
			break;
		case 20;
			$retvalue = "長野県";
			break;
		case 21;
			$retvalue = "岐阜県";
			break;
		case 22;
			$retvalue = "静岡県";
			break;
		case 23;
			$retvalue = "愛知県";
			break;
		case 24;
			$retvalue = "三重県";
			break;
		case 25;
			$retvalue = "滋賀県";
			break;
		case 26;
			$retvalue = "京都府";
			break;
		case 27;
			$retvalue = "大阪府";
			break;
		case 28;
			$retvalue = "兵庫県";
			break;
		case 29;
			$retvalue = "奈良県";
			break;
		case 30;
			$retvalue = "和歌山県";
			break;
		case 31;
			$retvalue = "鳥取県";
			break;
		case 32;
			$retvalue = "島根県";
			break;
		case 33;
			$retvalue = "岡山県";
			break;
		case 34;
			$retvalue = "広島県";
			break;
		case 35;
			$retvalue = "山口県";
			break;
		case 36;
			$retvalue = "徳島県";
			break;
		case 37;
			$retvalue = "香川県";
			break;
		case 38;
			$retvalue = "愛媛県";
			break;
		case 39;
			$retvalue = "高知県";
			break;
		case 40;
			$retvalue = "福岡県";
			break;
		case 41;
			$retvalue = "佐賀県";
			break;
		case 42;
			$retvalue = "長崎県";
			break;
		case 43;
			$retvalue = "熊本県";
			break;
		case 44;
			$retvalue = "大分県";
			break;
		case 45;
			$retvalue = "宮崎県";
			break;
		case 46;
			$retvalue = "鹿児島県";
			break;
		case 47;
			$retvalue = "沖縄県";
			break;
	}
			return $retvalue;
}
function Retseibetu($index)
{
	switch ($index){
		case 1;
			$retvalue = "男";
			break;
		case 2;
			$retvalue = "女";
			break;
	}
			return $retvalue;
}
function Retgaido($index)
{
	switch ($index){
		case 1;
			$retvalue = "必要";
			break;
		case 2;
			$retvalue = "不要";
			break;
	}
			return $retvalue;
}
function Retshuukajikan($index)
{
	switch ($index){
		case 1;
			$retvalue = "9:00-13:00";
			break;
		case 2;
			$retvalue = "13:00-15:00";
			break;
		case 3;
			$retvalue = "14:00-16:00";
			break;
		case 4;
			$retvalue = "15:00-18:00";
			break;
		case 5;
			$retvalue = "18:00-20:00";
			break;
	}
			return $retvalue;
}
function Retshokugyou($index)
{
	switch ($index){
		case 1;
			$retvalue = "会社員";
			break;
		case 2;
			$retvalue = "公務員";
			break;
		case 3;
			$retvalue = "主婦";
			break;
		case 4;
			$retvalue = "自営業";
			break;
		case 5;
			$retvalue = "学生";
			break;
		case 6;
			$retvalue = "パート";
			break;
		case 7;
			$retvalue = "フリーター";
			break;
		case 8;
			$retvalue = "無職";
			break;
		case 9;
			$retvalue = "その他";
			break;
	}
			return $retvalue;
}
?>