<?php
	require '../parts/pagechk.inc';

	// ライブラリ読み込み
	require_once("../parts/selectvalue_souko.php");
	require_once("DB.php");
	require_once("Log.php");
	
	$sellNo = $_GET['sellno'];
	/****************************/
	/*	バリデーションチェック	*/
	/****************************/
	// 値が空の場合
	if($sellNo==""){
		die("買取番号を入力してください。");
	}
	// 桁数チェック
	$sellNolen = mb_strlen($_GET['sellno']);
	if($sellNolen!=13){
		die("桁数が足りません。");
	}
	// sql文
	$sql = "select SELL_NO,ADMIT_DM from TSELL where SELL_NO = ?";
	
	// 設定ファイル読込
	$ini = parse_ini_file("../parts/db.ini",true);

	// ECDBコネクション取得
	$dsn_ec = "oci8://". $ini['ecdb']['usr'] . ":" . $ini['ecdb']['pwd'] . "@" . $ini['ecdb']['dbn'];
	//データベースへの接続開始
	$db = DB::connect($dsn_ec);
	if(DB::isError($db)){
		die($db->getMessage());
	}
	$db->autoCommit(false);

	// 情報を取得
	$res = $db->query($sql,$sellNo);
	if(DB::isError($res)){
		die($res->getMessage());
	}
	$sell = $res->fetchRow(DB_FETCHMODE_ASSOC);
	$res->free();
	
	// 承認日時
	$admit = $sell['ADMIT_DM'];
	
	// 承認日時をフォーマット
	$admitday = new DateTime($admit);
//	echo $admitday->format('Y/m/d H時i分s秒');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="test/html; charset=SHIFT-JIS">
	</head>
	<body>
		<strong>承認日時検索結果</strong>
		<br><br>
		<?php
		echo "買取番号：".$sellNo;
		echo "<br><br>";
		echo "承認日時：".$admitday->format('Y/m/d H:i:s');
		?>
		<br><br>
		<input type="button" value="戻る" onClick="location.href='dsp_admit.php'">
		<input type="button" value="トップに戻る" onClick="location.href='/'">
	</body>
</html>