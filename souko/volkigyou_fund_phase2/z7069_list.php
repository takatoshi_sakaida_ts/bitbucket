<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<link rel="stylesheet" type="text/css" href="z7069_list.css">
<title>Z7069発行済リスト一覧</title>
</head>
<body>
<?php

// 実行時間を5分に変更(デフォルト30秒だと出力途中で止まる)
set_time_limit(300);

//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
//関数定義ファイル読み込み
require_once("./functions.php");
require_once("../parts/selectvalue_souko.php");

//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}

// 倉庫DBコネクション生成
$logi_usr = 'bol';
$logi_pwd = 'bol';
$logi_dbn = 'bol';
$logi_dsn = "oci8://". $logi_usr . ":" . $logi_pwd . "@" . $logi_dbn;

$logidb = DB::connect($logi_dsn);
$logidb->setOption('portability',DB_PORTABILITY_NULL_TO_EMPTY | DB_PORTABILITY_NUMROWS);
// エラーチェック
if(DB::isError($logidb)){
    echo "Fail\n" . DB::errorMessage($logidb) . "\n";
}

// 101、マスタ無し商品集計用
$agtSql = "select a.instorecode as INSTORECODE, a.TAX_PRICE as TAX_PRICE, g.GENRE0CODE as GENRE0CODE, g.BUMONCATCODE as BUMONCATCODE from d_ass_goods a, m_goods g where a.instorecode = g.instorecode and asscode = ?";
// 買取抽出sql
$sql = getSql();

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),NULL,NULL,NULL);
}

print "<h2>Z7069発行済リスト一覧</h2>";


//検索結果の表示
print "<table id='detail' border='1'>\n";
//print "<tr bgcolor=#ccffff>\n";

//項目名の表示
print <<< EOM
<tr>
<th>新番号</th>
<th>発行日</th>
<th>計算日</th>
<th>振込日</th>
<th>店舗番号</th>
<th>店舗名</th>
<th>買取ステータス</th>
<th>計算箱数</th>
<th>計算点数</th>
<th>計算金額</th>
<th>D点数</th>
<th>101点数</th>
<th>101金額</th>
<th>102点数</th>
<th>102金額</th>
<th>103点数</th>
<th>103金額</th>
<th>104点数</th>
<th>104金額</th>
<th>105点数</th>
<th>105金額</th>
<th>111点数</th>
<th>111金額</th>
<th>112点数</th>
<th>112金額</th>
<th>113点数</th>
<th>113金額</th>
<th>121点数</th>
<th>121金額</th>
<th>131点数</th>
<th>131金額</th>
<th>208点数</th>
<th>208金額</th>
<th>マスタなし点数</th>
<th>マスタなし金額</th>
</tr>
EOM;

// 店舗名リスト(店舗番号をキーに名前を取得)
$storeNo = getStoreList();
//print_r($storeNo);

$stmt = $logidb->prepare($agtSql);

//検索結果の内容の表示処理
while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){

    $store_no = ( isset($storeNo[$row['PICKUP_NM_MID']]) ) ? $storeNo[$row['PICKUP_NM_MID']] : '';
    if($store_no == $storeNo['一時保管在庫買取']){
        continue;
    }

    $sell_no = $row['SELL_NO'];
    $date = date("Y/m/d",strtotime($row['SELL_FORM_GET_DT']));
    $ass_dt = ( $row['ASS_DT'] != '' ) ? date("Y/m/d",strtotime($row['ASS_DT'])) : '';
    $tran_dt = ( $row['TRAN_DT'] != '' ) ? date("Y/m/d",strtotime($row['TRAN_DT'])) : '';
//    $store_no = ( isset($storeNo[$row['PICKUP_NM_MID']]) ) ? $storeNo[$row['PICKUP_NM_MID']] : '';
    $name = $row['PICKUP_NM_FST'] .$row['PICKUP_NM_MID'];
    $status = statusToStr($row['SELL_STAT']);
    $box = $row['BOX'];
    $amount = ( $row['TOT_ASS_AMT'] != '' ) ? number_format($row['TOT_ASS_AMT']) : '';
    $valid_item_cnt = ( $row['VALID_ITEM_CNT'] != '' ) ? $row['VALID_ITEM_CNT'] : '';
    $invalid_item_cnt = ( $row['INVALID_ITEM_CNT'] != '' ) ? $row['INVALID_ITEM_CNT'] : '';

    $aggregate = array();
    $aggregate['101Count'] = null;
    $aggregate['101Amount'] = null;
    $aggregate['102Count'] = null;
    $aggregate['102Amount'] = null;
    $aggregate['103Count'] = null;
    $aggregate['103Amount'] = null;
    $aggregate['104Count'] = null;
    $aggregate['104Amount'] = null;
    $aggregate['105Count'] = null;
    $aggregate['105Amount'] = null;
    $aggregate['111Count'] = null;
    $aggregate['111Amount'] = null;
    $aggregate['112Count'] = null;
    $aggregate['112Amount'] = null;
    $aggregate['113Count'] = null;
    $aggregate['113Amount'] = null;
    $aggregate['121Count'] = null;
    $aggregate['121Amount'] = null;
    $aggregate['131Count'] = null;
    $aggregate['131Amount'] = null;
    $aggregate['208Count'] = null;
    $aggregate['208Amount'] = null;
    $aggregate['noMasterCount'] = null;
    $aggregate['noMasterAmount'] = null;

    if($row['SELL_STAT'] == '05' || $row['SELL_STAT'] == '06'){
        
        $aggregate['101Count'] = 0;
        $aggregate['101Amount'] = 0;
        $aggregate['102Count'] = 0;
        $aggregate['102Amount'] = 0;
        $aggregate['103Count'] = 0;
        $aggregate['103Amount'] = 0;
        $aggregate['104Count'] = 0;
        $aggregate['104Amount'] = 0;
        $aggregate['105Count'] = 0;
        $aggregate['105Amount'] = 0;
        $aggregate['111Count'] = 0;
        $aggregate['111Amount'] = 0;
        $aggregate['112Count'] = 0;
        $aggregate['112Amount'] = 0;
        $aggregate['113Count'] = 0;
        $aggregate['113Amount'] = 0;
        $aggregate['121Count'] = 0;
        $aggregate['121Amount'] = 0;
        $aggregate['131Count'] = 0;
        $aggregate['131Amount'] = 0;
        $aggregate['208Count'] = 0;
        $aggregate['208Amount'] = 0;
        $aggregate['noMasterCount'] = 0;
        $aggregate['noMasterAmount'] = 0;
        
        $asscode = str_replace("Z", "000",$sell_no);
        $assRes = $logidb->execute($stmt, $asscode);
        while($row2 = $assRes->fetchRow(DB_FETCHMODE_ASSOC)){

            //カテゴリ「書籍」かつ部門コード「101」の買取点数合計
            if(Retgoodscategory($row2['GENRE0CODE']) == "書籍" && $row2['BUMONCATCODE'] == '101'){
                $aggregate['101Count'] += 1;
                $aggregate['101Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "雑誌" && $row2['BUMONCATCODE'] == '102'){
                $aggregate['102Count'] += 1;
                $aggregate['102Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "書籍" && $row2['BUMONCATCODE'] == '103'){
                $aggregate['103Count'] += 1;
                $aggregate['103Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "書籍" && $row2['BUMONCATCODE'] == '104'){
                $aggregate['104Count'] += 1;
                $aggregate['104Amount'] += intval($row2['TAX_PRICE']);
            }
            else if((Retgoodscategory($row2['GENRE0CODE']) == "書籍" || Retgoodscategory($row2['GENRE0CODE']) == "コミック") &&
                    ($row2['BUMONCATCODE'] == '105')){
                $aggregate['105Count'] += 1;
                $aggregate['105Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "コミック" && $row2['BUMONCATCODE'] == '111'){
                $aggregate['111Count'] += 1;
                $aggregate['111Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "コミック" && $row2['BUMONCATCODE'] == '112'){
                $aggregate['112Count'] += 1;
                $aggregate['112Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "コミック" && $row2['BUMONCATCODE'] == '113'){
                $aggregate['113Count'] += 1;
                $aggregate['113Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "書籍" && $row2['BUMONCATCODE'] == '121'){
                $aggregate['121Count'] += 1;
                $aggregate['121Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "書籍" && $row2['BUMONCATCODE'] == '131'){
                $aggregate['131Count'] += 1;
                $aggregate['131Amount'] += intval($row2['TAX_PRICE']);
            }
            else if(Retgoodscategory($row2['GENRE0CODE']) == "部門商品"){
                if($row2['BUMONCATCODE'] == '200'){
                    $aggregate['208Count'] += 1;
                    $aggregate['208Amount'] += intval($row2['TAX_PRICE']);
                } else {
                    $aggregate['noMasterCount'] += 1;
                    $aggregate['noMasterAmount'] += intval($row2['TAX_PRICE']);
                }
            }
        }
    }

	print "<tr>";
//買取受付番号
	print "<td>".$sell_no ."</td>";
//発行日   
    print "<td>".$date ."</td>";
    // 計算日
    print "<td>".$ass_dt ."</td>";
    // 振込日
    print "<td>".$tran_dt ."</td>";
    // 店舗番号
    print "<td>".$store_no ."</td>";
    //店舗名
    print "<td>".$name ."</td>";
    //買取ステータス
    print "<td>".$status ."</td>";
    //計算箱数
    print "<td>".$box ."</td>";
    //計算点数
    print "<td>".$valid_item_cnt ."</td>";
    //計算金額
    print "<td>".$amount ."</td>";
    //D商品
    print "<td>".$invalid_item_cnt ."</td>";
    //101点数
    print "<td>".$aggregate['101Count'] ."</td>";
    //101金額
    print "<td>".$aggregate['101Amount'] ."</td>";
    //102点数
    print "<td>".$aggregate['102Count'] ."</td>";
    //102金額
    print "<td>".$aggregate['102Amount'] ."</td>";
    //103点数
    print "<td>".$aggregate['103Count'] ."</td>";
    //103金額
    print "<td>".$aggregate['103Amount'] ."</td>";
    //104点数
    print "<td>".$aggregate['104Count'] ."</td>";
    //104金額
    print "<td>".$aggregate['104Amount'] ."</td>";
    //105点数
    print "<td>".$aggregate['105Count'] ."</td>";
    //105金額
    print "<td>".$aggregate['105Amount'] ."</td>";
    //111点数
    print "<td>".$aggregate['111Count'] ."</td>";
    //111金額
    print "<td>".$aggregate['111Amount'] ."</td>";
    //112点数
    print "<td>".$aggregate['112Count'] ."</td>";
    //112金額
    print "<td>".$aggregate['112Amount'] ."</td>";
    //113点数
    print "<td>".$aggregate['113Count'] ."</td>";
    //113金額
    print "<td>".$aggregate['113Amount'] ."</td>";
    //121点数
    print "<td>".$aggregate['121Count'] ."</td>";
    //121金額
    print "<td>".$aggregate['121Amount'] ."</td>";
    //131点数
    print "<td>".$aggregate['131Count'] ."</td>";
    //131金額
    print "<td>".$aggregate['131Amount'] ."</td>";
    //208点数
    print "<td>".$aggregate['208Count'] ."</td>";
    //208金額
    print "<td>".$aggregate['208Amount'] ."</td>";
    //マスタなし点数
    print "<td>".$aggregate['noMasterCount'] ."</td>";
    //マスタなし金額
    print "<td>".$aggregate['noMasterAmount'] ."</td>";
print "</tr>";
}
print "</table>";

//データの開放
$res->free();
$db->disconnect();
$logidb->disconnect();


?>
<BR>
<FORM action="/index.php" method="POST"><INPUT TYPE="submit" VALUE="メニューに戻る"></FORM>


</body>
</html>