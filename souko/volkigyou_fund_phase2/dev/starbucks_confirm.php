<?PHP

require_once("../functions.php");

if ($_POST["TENPO_NO"] == '') {
	die('店舗番号を入力して下さい。');
}

$PICKUP_NM_FST="";
$PICKUP_NM_MID="";
$PICKUP_NM_LAST="";
$PICKUP_NM_ETC="";
$PICKUP_ZIP_CD="";
$PICKUP_ZIP_ADDR1="";
$PICKUP_ZIP_ADDR2="";
$PICKUP_ZIP_ADDR3="";
$PICKUP_ZIP_ADDR4="";
$PICKUP_TEL_NO="";
$MEMO="";

$bFlg = false;
$fp = fopen("starbucks.csv","r");
while($data = fgetcsv_reg($fp, 1024, "\t") ){
	for( $i=0; $i < count($data); $i++ ){
		if( intval($_POST["TENPO_NO"]) == intval($data[0]) ){
			$bFlg = true;
			$PICKUP_NM_FST=$data[1];
			$PICKUP_NM_MID=$data[2];
			$PICKUP_NM_LAST=$data[3];
			$PICKUP_NM_ETC=$data[4];
			$PICKUP_ZIP_CD=$data[5];
			$PICKUP_ZIP_ADDR1=$data[6];
			$PICKUP_ZIP_ADDR2=$data[7];
			$PICKUP_ZIP_ADDR3=$data[8];
			$PICKUP_ZIP_ADDR4=$data[9];
			$PICKUP_TEL_NO=$data[10];
			$MEMO=$data[21];
			break;
		}
	}
}
fclose( $fp );
if( $bFlg == false ){
	die('該当する店舗番号がありません。');
}

require '../../parts/pagechk.inc';
require '../../parts/utl.inc';
require_once("../selectvalue_volkigyou.php");

$_SESSION["HSMAP"]["TENPO_NO"]=htmlspecialchars($_POST["TENPO_NO"]);
$_SESSION["HSMAP"]["DANTAI"]="2";
$_SESSION["HSMAP"]["PICKUP_NM_FST"]=htmlspecialchars($PICKUP_NM_FST);
$_SESSION["HSMAP"]["PICKUP_NM_MID"]=htmlspecialchars($PICKUP_NM_MID);
$_SESSION["HSMAP"]["PICKUP_NM_LAST"]=htmlspecialchars($PICKUP_NM_LAST);
$_SESSION["HSMAP"]["PICKUP_NM_ETC"]=htmlspecialchars($PICKUP_NM_ETC);
$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]=htmlspecialchars($PICKUP_ZIP_CD);
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]=htmlspecialchars($PICKUP_ZIP_ADDR1);
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]=htmlspecialchars($PICKUP_ZIP_ADDR2);
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]=htmlspecialchars($PICKUP_ZIP_ADDR3);
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]=htmlspecialchars($PICKUP_ZIP_ADDR4);
$_SESSION["HSMAP"]["PICKUP_TEL_NO"]=htmlspecialchars($PICKUP_TEL_NO);
$_SESSION["HSMAP"]["MEM_SEX"]=htmlspecialchars("1");
$_SESSION["HSMAP"]["gaido"]=htmlspecialchars("2");
$_SESSION["HSMAP"]["BOX"]=htmlspecialchars(escapevalue($_POST["BOX"]));
$_SESSION["HSMAP"]["PICKUP_REQ_DTY"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_DTY"]));
$_SESSION["HSMAP"]["PICKUP_REQ_DTM"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_DTM"]));
$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_DTD"]));
$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_TIME"]));
$_SESSION["HSMAP"]["DANTAI_index"]=htmlspecialchars(escapevalue($_POST["DANTAI_index"]));
$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"]=htmlspecialchars(escapevalue($_POST["PICKUP_ZIP_ADDR1_index"]));
$_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"]=htmlspecialchars(escapevalue($_POST["PICKUP_REQ_TIME_index"]));
$_SESSION["HSMAP"]["MEM_SEX_index"]=htmlspecialchars(escapevalue($_POST["MEM_SEX_index"]));
$_SESSION["HSMAP"]["gaido_index"]=htmlspecialchars(escapevalue($_POST["gaido_index"]));
$_SESSION["HSMAP"]["MEMO"]=htmlspecialchars($MEMO);
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>スターバックス専用受付入力確認画面</title>
<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}
-->
</style>
</head>
<body>
<form method="POST" action="" name ="volcon">
<b>スターバックス専用受付入力確認</b>
<TABLE ID="menu1" width="900">
  <tr>
    <th>店舗番号</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["TENPO_NO"];
?>
    </td>
  </TR>
  <tr>
    <th>店舗名</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_NM_FST"];
print $_SESSION["HSMAP"]["PICKUP_NM_MID"];
?>
    </td>
  </TR>
  <tr>
    <th>箱数</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["BOX"];
?>
	</td>
  </tr>
  <tr>
    <th>集荷希望年月日</th>
    <td>
<?PHP
print $_SESSION["HSMAP"]["PICKUP_REQ_DTY"];
print '年';
print $_SESSION["HSMAP"]["PICKUP_REQ_DTM"];
print '月';
print $_SESSION["HSMAP"]["PICKUP_REQ_DTD"];
print '日';
?>
    </td>
  </tr>
  <tr>
    <th>集荷希望時間帯</th>
    <td>
<?PHP
print Retshuukajikan($_SESSION["HSMAP"]["PICKUP_REQ_TIME"]);
?>
    </td>
  </tr>
  <tr>
    <th>ガイド発送</th>
    <td>
<?PHP
print Retgaido($_SESSION["HSMAP"]["gaido"]);
?>    </td>
  </tr>
</table>

<input type="hidden" name="DANTAI_index" value="
<?PHP
print $_SESSION["HSMAP"]["DANTAI_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_ZIP_ADDR1_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1_index"];
print '" >';
?>
<input type="hidden" name="PICKUP_REQ_TIME_index" value="
<?PHP
print $_SESSION["HSMAP"]["PICKUP_REQ_TIME_index"];
print '" >';
?>
<input type="hidden" name="MEM_SEX_index" value="
<?PHP
print ($_SESSION["HSMAP"]["MEM_SEX"]);
print '" >';
?>
<input type="hidden" name="gaido_index" value="
<?PHP
print ($_SESSION["HSMAP"]["gaido"]);
print '" >';
?>

<BR>
<INPUT TYPE="BUTTON" VALUE="　　戻る　　" onClick="entback()">
<input type="submit" value="　　登録　　">
</FORM>
<script type="text/javascript">
function entback()
{

}
</script>

</body>
</html>