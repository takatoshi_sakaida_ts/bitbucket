<?PHP
function db_insert(){
//PEARの利用     -------(1)
require_once("DB.php");
require_once("selectvalue_volkigyou.php");
// 銀行情報のセット
/* 2019.05.27 ブックラPhase2対応 MOD START */
//require_once("volkigyou_bank.php");
$_SESSION["HSMAP"]["BANK_CD"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_CD"];
$_SESSION["HSMAP"]["BANK_BRANCH_CD"]= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_BRANCH_CD"];
$_SESSION["HSMAP"]["BANK_NM"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BANK_NM"];
$_SESSION["HSMAP"]["BRANCH_NM"]		= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["BRANCH_NM"];
$_SESSION["HSMAP"]["KOUZASHURUI"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZASHURUI"];
$_SESSION["HSMAP"]["KOUZABANGOU"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZABANGOU"];
$_SESSION["HSMAP"]["KOUZAMEIGI"]	= $_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["KOUZAMEIGI"];
$sellno_head = $_SESSION["HSMAP"]["DANTAI"];
/* 2019.05.27 ブックラPhase2対応 MOD END */
//ログイン情報の読み込み
require_once("../parts/login_ecd.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]="";
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//買取受付番号の決定
/* 2019.05.27 ブックラPhase2対応 MOD START */
/* 20161124 編集 Start */
$sql="select max(sell_no) from tsell where sell_no like '" . $sellno_head . "%'";
//if($sellno_head < '100') {
//$sql=$sql."'Z70" . $sellno_head . "%'";
//} else if($sellno_head >= '100') {
//$sql=$sql."'Z7" . $sellno_head . "%'";
//}
/* 20161124 End */
/* 2019.05.27 ブックラPhase2対応 MOD END */
//print "sql:".$sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
/* 2019.05.27 ブックラPhase2対応 MOD START */
$sell_no = 0;
/* 2019.05.27 ブックラPhase2対応 MOD END */
while($row = $res->fetchRow()){
//print $row[0]."<BR>";
/* 2019.05.27 ブックラPhase2対応 MOD START */
/* 20161124 追加 Start */
//if($row[0] == null && $sellno_head >= '100') {
//	$sell_no="00000000";
//} else {
//	$sell_no=substr($row[0],5,8);
//	print $row[0]."<BR>";
//}
 	if(isset($row[0]) && $row[0] != null){
    	$sell_no=substr($row[0],5,8);
    }

}
/* 20161124 End */
/* 2019.05.27 ブックラPhase2対応 MOD END */
$res->free();
//print "sell_noは".$sell_no."<br>";
$sell_no=$sell_no + 1;
//print "sell_no(+1)は".$sell_no."<br>";
$sell_no = sprintf("%08d",$sell_no);
/* 2019.05.27 ブックラPhase2対応 MOD START */
/* 20161124 編集 Start */
//if($sellno_head < '100') {
//$sell_no="Z70".$sellno_head.$sell_no;
//} else if($sellno_head >= '100') {
//$sell_no=$sellno_head.$sell_no;
//}
/* 20161124 End */
$sell_no = $sellno_head.$sell_no;
/* 2019.05.27 ブックラPhase2対応 MOD END */
//
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='';
}else
{
	$_SESSION["HSMAP"]["DL_STAT_LABEL"]='000000000000000';
}
//ガイド発送が不要なら申込書自動到着
if ($_SESSION["HSMAP"]["gaido"]=='1')
{
	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
}else{
/* 2019.05.20 ブックラPhase2対応 MOD START */
//・NHK渋谷・NHK名古屋・FC在庫買取の場合は申込書の自動チェックはしない
//	if ($sellno_head=='01'|| $sellno_head=='07')
	if ($sellno_head=='Z7049'|| $sellno_head=='Z7080'|| $sellno_head=='Z7081')
	{
		$_SESSION["HSMAP"]["FORM_ARRIVAL"]='0';
	}else{
		$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
		$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]=date("Ymd");
	}
/* 2019.05.20 ブックラPhase2対応 MOD END */
}

/* 2019.05.20 ブックラPhase2対応 MOD START */
// ◆「買取申込書受領」に自動チェックを入れる場合、下記に追記すること◆
//NTT大阪支店、スターバックス、帝人、NEC(東日本大震災支援)・結核予防会・アイエスエフネット・民謡センター・ＤＮＰグループ労連・しょうがっこうをおくる会・神奈川大学・レオパレス２１
//阪急阪神ホールディングス・双日エアロスペース(地球環境基金)・東北大震災義捐金・西友・日本経済新聞文化部(日本赤十字社 東京都支部)・朝日新聞社論説委員室・
//HFI（Hope and Faith International）・ACE（エース）・マン・パワージャパン・中日本ハイウェイパトロール東京・三菱商事・地球の友と歩む会
//損保ジャパン奈良支店の場合・日本興亜損害保険・3keys（スリーキーズ）・ライフ・★スポット対応企業・町田ゼルビア・SC相模原・幼い難民を考える会（CYR）・アフラック
//JHP学校をつくる会・日立建機・北海道森と緑の会・ユニー・埼玉県自動車販売店協会・マルハン・アドビシステムズ・大塚商会(3keys)・森ビル都市企画・アクセス・ブリヂストンスポーツ
//ケア・インターナショナルジャパン・世界の子どもにワクチンを(JCV)・共立メンテナンス・ジャパンハート・ブリッジ エーシア ジャパン(BAJ)・横浜開港祭・商船三井・広栄化学工業
//ハグオール、NTT労働組合　西日本・日本ラグビーフットボール協会・店舗在庫買取・神戸YMCA・北洋銀行・なおちゃんを救う会・大日本印刷株式会社　honto返品買取・シニアライフセラピー研究所
//NTT労働組合　東日本本部・プリック ジャパン ビューティー・損害保険ジャパン日本興亜（東日本大震災支援）・損害保険ジャパン日本興亜（熊本地震支援）・ニッポン放送
//三井不動産レジデンシャル（東日本大震災支援）・三井不動産レジデンシャル（熊本地震支援）・三菱地所コミュニティ（東日本大震災支援）・関西テレビ・かわさき市民活動センター
//山田養蜂場・アリさんマークの引越社・日本バルカー工業（東日本大震災支援）・JAIFA・SOMPOシステムズ（移動図書館支援）、日経印刷　長野（東日本大震災支援）、フロムヴイ、愛知県教職員組合(移動図書館支援）
//ベクトルフラックス、バリューブックス、東京海上グループ『未来塾』（熊本地震支援）、大阪商工信用金庫、三菱UFJニコス、アカツキ（BUY王）、アリアンツ火災海上保険（東日本大震災支援）
//損害保険ジャパン日本興亜　関西総務部（東日本大震災支援）、ブックマーケティング、正蓮寺、オムロンフィールドエンジニアリング（熊本地震支援）、LU（ブリッジ エーシア ジャパン）、サスケコーポレーション、ABC(Room to Read)
//楽天、神奈川県医療福祉施設協同組合、日本通運【ボランティア】（かながわキンタロウ）、★スポット対応企業（東日本支援10％UP）、★スポット対応企業（熊本支援10％UP）、ジェーシービー(Room to Read)
//新宿南エネルギーサービス(地球環境基金)、J-WAVE(地球環境基金)、セディナ(3keys)、三井不動産レジデンシャル（平成30年7月豪雨災害）、日本経済新聞　生活情報部（平成30年7月豪雨災害）
//損害保険ジャパン日本興亜（平成30年7月豪雨災害支援）、こども食堂支援機構
//
//if ($sellno_head=='01'||$sellno_head=='07')
//if ($sellno_head=='02'||$sellno_head=='03'||$sellno_head=='06'||$sellno_head=='09'||$sellno_head=='11'||$sellno_head=='14'||$sellno_head=='15'||$sellno_head=='16'||$sellno_head=='17'||$sellno_head=='18'||$sellno_head=='20'||$sellno_head=='22'||$sellno_head=='23'||$sellno_head=='24'||$sellno_head=='25'||$sellno_head=='26'||$sellno_head=='27'||$sellno_head=='28'||$sellno_head=='29'||$sellno_head=='30'||$sellno_head=='31'||$sellno_head=='32'||$sellno_head=='33'||$sellno_head=='34'||$sellno_head=='35'||$sellno_head=='36'||$sellno_head=='37'||$sellno_head=='38'||$sellno_head=='39'||$sellno_head=='40'||$sellno_head=='41'||$sellno_head=='42'||$sellno_head=='43'||$sellno_head=='44'||$sellno_head=='45'||$sellno_head=='46'||$sellno_head=='47'||$sellno_head=='48'||$sellno_head=='50'||$sellno_head=='51'||$sellno_head=='52'||$sellno_head=='53'||$sellno_head=='54'||$sellno_head=='55'||$sellno_head=='56'||$sellno_head=='57'||$sellno_head=='58'||$sellno_head=='59'||$sellno_head=='60'||$sellno_head=='61'||$sellno_head=='62'||$sellno_head=='63'||$sellno_head=='64'||$sellno_head=='65'||$sellno_head=='66'||$sellno_head=='67'||$sellno_head=='68'||$sellno_head=='69'||$sellno_head=='70'||$sellno_head=='71'||$sellno_head=='72'||$sellno_head=='73'||$sellno_head=='74'||$sellno_head=='75'||$sellno_head=='76'||$sellno_head=='77'||$sellno_head=='78'||$sellno_head=='79'||$sellno_head=='82'||$sellno_head=='83'||$sellno_head=='84'||$sellno_head=='85'||$sellno_head=='86'||$sellno_head=='87'||$sellno_head=='88'||$sellno_head=='90'||$sellno_head=='97'||$sellno_head=='98'||$sellno_head=='99'||$sellno_head=='100'||$sellno_head=='101'||$sellno_head=='102'||$sellno_head=='103'||$sellno_head=='104'||$sellno_head=='105'||$sellno_head=='106'||$sellno_head=='107'||$sellno_head=='108'||$sellno_head=='109'||$sellno_head=='110'||$sellno_head=='111'||$sellno_head=='112'||$sellno_head=='113'||$sellno_head=='114'||$sellno_head=='115'||$sellno_head=='116'||$sellno_head=='117'||$sellno_head=='118'||$sellno_head=='119'||$sellno_head=='120'||$sellno_head=='121'||$sellno_head=='122'||$sellno_head=='123'||$sellno_head=='125'||$sellno_head=='126'||$sellno_head=='127'||$sellno_head=='128'||$sellno_head=='129')
//{
//	$_SESSION["HSMAP"]["FORM_ARRIVAL"]='1';
//	$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]=date("Ymd");
//}
/* 2019.05.20 ブックラPhase2対応 MOD END */


// ◆「集荷をしない」場合、下記に追記すること◆
//店舗在庫買取、LIC、JAIFA、バリューブックス、アカツキ（BUY王）、ブックマーケティング、LU（ブリッジ エーシア ジャパン）、
/* 2019.05.20 ブックラPhase2対応 MOD START */
if ($sellno_head=='Z7069'|| $sellno_head=='Z7096'|| $sellno_head=='Z7097'|| $sellno_head=='Z7103'|| $sellno_head=='Z7107'|| $sellno_head=='Z7110'|| $sellno_head=='Z7113')
/* 2019.05.20 ブックラPhase2対応 MOD END */
{
	$_SESSION["HSMAP"]["PICKUP_REQ_DTY"]=date("Y");
	$_SESSION["HSMAP"]["PICKUP_REQ_DTM"]=date("m");
	$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]=date("d");
	$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]="1";
	$_SESSION["HSMAP"]["PICK_DATA_EXPT"]="3";
}else{
	$_SESSION["HSMAP"]["PICK_DATA_EXPT"]='1';
}

/* 2019.05.20 ブックラPhase2対応 MOD START */
// ◆自動的に「振込保留」とする場合、下記に追加すること◆
//NTT大阪支店、スターバックス、帝人、セガサミーグループ、NEC(東日本大震災支援)、ファミリーマート・ＤＮＰグループ労連・結核予防会・ナック（国境なき医師団）・民謡センター・東北大震災義捐金・西友・日本経済新聞文化部(日本赤十字社 東京都支部)・ACE・ICAN
//双日エアロスペース(地球環境基金)・中日本ハイウェイ(30)・三菱商事(31)、損保ジャパン奈良支店、HFI、パトロール東京・ライフ、★スポット対応企業町田ゼルビア、アフラック
//幼い難民を考える会・3keys・埼玉県自動車販売店協会・アドビシステムズ・大塚商会(3keys)(50)・森ビル都市企画・JHP学校をつくる会(42)
//北海道森と緑の会・ブリヂストンスポーツ・ブリッジ エーシア ジャパン(BAJ)、岡谷鋼機、横浜開港祭、アクセス、商船三井、オール、世界の子どもにワクチンを、NTTネオメイト
//ジャパンハート、店舗在庫買取、しょうがっこうをおくる会、ケア・インターナショナルジャパン、神奈川大学、神戸YMCA、北洋銀行、大日本印刷株式会社　honto返品買取、プリック ジャパン ビューティー
//損害保険ジャパン日本興亜（東日本大震災支援）・損害保険ジャパン日本興亜（熊本地震支援）、ニッポン放送、三井不動産レジデンシャル（東日本大震災支援）
//三井不動産レジデンシャル（熊本地震支援）、三菱地所コミュニティ（東日本大震災支援）、関西テレビ、山田養蜂場、アリさんマークの引越社、NTTデータ（東日本大震災支援）
//日本バルカー工業（東日本大震災支援）、ルネサスイーストン（シャンティ）、住友生命札幌支社（シャンティ）、三井住友カード（東日本大震災支援）
//日経印刷（東日本大震災支援）、アトミクス、LIC、SOMPOシステムズ（移動図書館支援）、日経印刷　長野（東日本大震災支援）、フロムヴイ、愛知県教職員組合(移動図書館支援）
//ベクトルフラックス、バリューブックス、東京海上グループ『未来塾』（熊本地震支援）、大阪商工信用金庫、三菱UFJニコス、アカツキ（BUY王）、アリアンツ火災海上保険（東日本大震災支援）
//損害保険ジャパン日本興亜　関西総務部（東日本大震災支援）、ブックマーケティング、正蓮寺、オムロンフィールドエンジニアリング（熊本地震支援）、LU（ブリッジ エーシア ジャパン）、サスケコーポレーション、ABC(Room to Read)
//楽天、神奈川県医療福祉施設協同組合、日本通運【ボランティア】（かながわキンタロウ）、★スポット対応企業（東日本支援10％UP）、★スポット対応企業（熊本支援10％UP）、ジェーシービー(Room to Read)
//新宿南エネルギーサービス(地球環境基金)、J-WAVE(地球環境基金)、西日本高速道路サービス四国、セディナ(3keys)、三井不動産レジデンシャル（平成30年7月豪雨災害）、日本経済新聞　生活情報部（平成30年7月豪雨災害）
//損害保険ジャパン日本興亜（平成30年7月豪雨災害支援）、西日本高速道路サービス四国(売って支援10％UP)、こども食堂支援機構
//
//if ($sellno_head=='01'||$sellno_head=='02'||$sellno_head=='03'||$sellno_head=='4'||$sellno_head=='5'||$sellno_head=='06'||$sellno_head=='08'||$sellno_head=='09'||$sellno_head=='10'||$sellno_head=='14'||$sellno_head=='15'||$sellno_head=='16'||$sellno_head=='18'||$sellno_head=='19'||$sellno_head=='22'||$sellno_head=='23'||$sellno_head=='24'||$sellno_head=='25'||$sellno_head=='27'||$sellno_head=='28'||$sellno_head=='30'||$sellno_head=='31'||$sellno_head=='33'||$sellno_head=='34'||$sellno_head=='35'||$sellno_head=='36'||$sellno_head=='37'||$sellno_head=='38'||$sellno_head=='40'||$sellno_head=='41'||$sellno_head=='42'||$sellno_head=='44'||$sellno_head=='46'||$sellno_head=='48'||$sellno_head=='50'||$sellno_head=='51'||$sellno_head=='52'||$sellno_head=='53'||$sellno_head=='54'||$sellno_head=='55'||$sellno_head=='56'||$sellno_head=='57'||$sellno_head=='58'||$sellno_head=='59'||$sellno_head=='60'||$sellno_head=='61'||$sellno_head=='62'||$sellno_head=='63'||$sellno_head=='64'||$sellno_head=='66'||$sellno_head=='67'||$sellno_head=='69'||$sellno_head=='70'||$sellno_head=='71'||$sellno_head=='73'||$sellno_head=='76'||$sellno_head=='77'||$sellno_head=='78'||$sellno_head=='79'||$sellno_head=='82'||$sellno_head=='83'||$sellno_head=='84'||$sellno_head=='85'||$sellno_head=='87'||$sellno_head=='88'||$sellno_head=='89'||$sellno_head=='90'||$sellno_head=='91'||$sellno_head=='92'||$sellno_head=='93'||$sellno_head=='94'||$sellno_head=='95'||$sellno_head=='96'||$sellno_head=='98'||$sellno_head=='99'||$sellno_head=='100'||$sellno_head=='101'||$sellno_head=='102'||$sellno_head=='103'||$sellno_head=='104'||$sellno_head=='105'||$sellno_head=='106'||$sellno_head=='107'||$sellno_head=='108'||$sellno_head=='109'||$sellno_head=='110'||$sellno_head=='111'||$sellno_head=='112'||$sellno_head=='113'||$sellno_head=='114'||$sellno_head=='115'||$sellno_head=='116'||$sellno_head=='117'||$sellno_head=='118'||$sellno_head=='119'||$sellno_head=='120'||$sellno_head=='121'||$sellno_head=='122'|| $sellno_head=='123'|| $sellno_head=='124'|| $sellno_head=='125'|| $sellno_head=='126'||$sellno_head=='127'||$sellno_head=='128'||$sellno_head=='129'||$sellno_head=='130')
//{
//	$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='1';
//}else{
//	$_SESSION["HSMAP"]["PAY_HOLD_FLG"]='0';
//}
/* 2019.05.20 ブックラPhase2対応 MOD END */

// ◆ �A査定結果を確認してから承認コース（お値段のつかない品物は処分）を指定したい場合◆
// 日本経済新聞文化部(日本赤十字社 東京都支部)
//if($sellno_head=='25'){
//	$_SESSION["HSMAP"]["NOTE_YN"]=1;
//	$_SESSION["HSMAP"]["DISPOSE"]=0;
// ◆�B査定結果を確認してから承認コース（お値段のつかない品物は返却希望）を指定したい場合◆
// FC在庫買取
/* 2019.05.27 ブックラPhase2対応 MOD START */
if($sellno_head=='Z7049'){
/* 2019.05.27 ブックラPhase2対応 MOD END */
	$_SESSION["HSMAP"]["NOTE_YN"]='1';
	$_SESSION["HSMAP"]["DISPOSE"]='1';
// ◆上記以外は、�@おまかせ承認コース（スピード入金・お値段のつかない品物は処分）になります◆
}else{
	$_SESSION["HSMAP"]["NOTE_YN"]=0;
	$_SESSION["HSMAP"]["DISPOSE"]=0;
}

//print $sell_no."<BR>";
/* 2019.05.20 ブックラPhase2対応 MOD START */
$sql="INSERT INTO tsell(".
"SELL_NO,".
"SELL_STAT,".
"PICKUP_NM_FST,".
"PICKUP_NM_MID,".
"PICKUP_NM_LAST,".
"PICKUP_NM_ETC,".
"PICKUP_ZIP_CD,".
"PICKUP_ZIP_ADDR1,".
"PICKUP_ZIP_ADDR2,".
"PICKUP_ZIP_ADDR3,".
"PICKUP_DTL_ADDR,".
"PICKUP_TEL_NO,".
"PICKUP_TEL_NO_2,".
"PICKUP_REQ_DT,".
"PICKUP_REQ_TIME,".
"BOX,".
"NOTE_YN,".
"DISPOSE,".
"RECEIPT_TP,".
"MEM_BIRTH_DT,".
"MEM_SEX,".
"MEM_JOB,".
"PAY_TP,".
"DL_STAT_LABEL,".
"BANK_CD,".
"BANK_BRANCH_CD,".
"BANK_NM,".
"BRANCH_NM,".
"BANK_ACC_TP,".
"BANK_ACC_NO,".
"BANK_ACC_NM,".
"MEMO,".
"FORM_ARRIVAL,".
"PICK_DATA_EXPT,".
"SELL_FORM_EXPT,".
"ADMIT_DATA_EXPT,".
"ASSES_CNT,".
"REG_DM,".
"UPD_DM,".
"PAY_HOLD_FLG,".
"AUTH_SEND_FLG,".
"SELL_FORM_GET_DT,".
"USER_ID".
") VALUES('"
	.$sell_no."','"
	."01','"
	.$_SESSION["HSMAP"]["PICKUP_NM_FST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_MID"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_LAST"]."','"
	.$_SESSION["HSMAP"]["PICKUP_NM_ETC"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_CD"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR1"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR2"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR3"]."','"
	.$_SESSION["HSMAP"]["PICKUP_ZIP_ADDR4"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_TEL_NO"]."','"
	.$_SESSION["HSMAP"]["PICKUP_REQ_DTY"].$_SESSION["HSMAP"]["PICKUP_REQ_DTM"].$_SESSION["HSMAP"]["PICKUP_REQ_DTD"]."','"
	."0".$_SESSION["HSMAP"]["PICKUP_REQ_TIME"]."','"
	.$_SESSION["HSMAP"]["BOX"]."','"
	.$_SESSION["HSMAP"]["NOTE_YN"]."','"
	.$_SESSION["HSMAP"]["DISPOSE"]."','"
	."2','"
	."19500101','"
	.$_SESSION["HSMAP"]["MEM_SEX"]."','"
	."09','"
	."1','"
	.$_SESSION["HSMAP"]["DL_STAT_LABEL"]."','"
	.$_SESSION["HSMAP"]["BANK_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_BRANCH_CD"]."','"
	.$_SESSION["HSMAP"]["BANK_NM"]."','"
	.$_SESSION["HSMAP"]["BRANCH_NM"]."','"
	.$_SESSION["HSMAP"]["KOUZASHURUI"]."','"
	.$_SESSION["HSMAP"]["KOUZABANGOU"]."','"
	.$_SESSION["HSMAP"]["KOUZAMEIGI"]."','"
	.$_SESSION["HSMAP"]["MEMO"]. ":企業名：".str_replace("'","",$_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["PARTNER_NM"]). "　ガイド発送:" . Retgaido($_SESSION["HSMAP"]["gaido"]) . "','"
	.$_SESSION["HSMAP"]["FORM_ARRIVAL"]."','"
	.$_SESSION["HSMAP"]["PICK_DATA_EXPT"]."','"
	."1','"
	."1','"
	."0','"
	.date("YmdHis")."','"
	.date("YmdHis")."','".$_SESSION["VOLPARTNER"][$_SESSION["HSMAP"]["DANTAI"]]["PAY_HOLD_FLG"]."','0','"
	.$_SESSION["HSMAP"]["SELL_FORM_GET_DT"]."','"
	.$_SESSION["user_id"]."')";
/* 2019.05.20 ブックラPhase2対応 MOD END */
//print "<br>".$sql;
$db->autoCommit( false ); 
$res2 = $db->query($sql);
if(DB::isError($res2)){
	print $res2->getMessage()."<br>";
	print $res2->getDebugInfo();
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//
output_log($sql,$_SESSION["user_id"],'vol_entry_sql');
$sql="INSERT INTO tsellassessment (".
"SELL_NO,".
"REG_DM".
") VALUES('"
	.$sell_no."','"
	.date("YmdHis")."')";

//print "<br>".$sql;
$res3 = $db->query($sql);
if(DB::isError($res3)){
	print $res3->getMessage()."<br>";
	print $res3->getDebugInfo();
	$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
		$_SESSION["HSMAP"]["sell_no"]=$sell_no;
$db->commit();
output_log($sell_no,$_SESSION["user_id"],'volkigyou_entry');
output_log($sql,$_SESSION["user_id"],'volkigyou_entry_sql');
//print "<br>".$sql;
//データの開放
$db->disconnect();
}
?>