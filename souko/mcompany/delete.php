<?php
	require_once("../parts/pagechk.inc");
	//パラメータ
	$deleteKey = addslashes(@$_POST["deleteKey"]);
	//ファイルの読み込み
	//PEARの利用     -------(1)
	require_once("DB.php");
	//ログイン情報の読み込み
	require_once("../parts/login_souko.php");
	//DeleteSQL生成
	$selectSql = "select COMPANYCODE, COMPANYNAME, COMPANYNAMEKANA from M_COMPANY where COMPANYCODE = '".$deleteKey."'";
	$selectRes = $db->query($selectSql);
	if(DB::isError($selectRes)){
		$selectRes->DB_Error($selectRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	//DeleteLog write
	$fLog = fopen("delete.log", "a");
	while($row = $selectRes->fetchRow()){
		$logYmd = date('Y/m/d H:i:s');
		$logTxt = $logYmd." Delete [".$_SESSION["user_id"]."] [".$row[0].",".$row[1].",".$row[2]."]\r\n";
		fputs ($fLog, $logTxt);
	}
	fclose ($fLog);
	//Delete
	$deleteSql = "delete from M_COMPANY where COMPANYCODE = '".$deleteKey."'";
//	print $deleteSql;
	$deleteRes = $db->query($deleteSql);
	if(DB::isError($deleteRes)){
		$deleteRes->DB_Error($deleteRes->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	$db->commit();
	$db->disconnect();
	//編集画面へリダイレクト
	header("Location: regist.php");
	exit();
?>