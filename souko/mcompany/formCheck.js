function checkForm(){
	var checkCode = false;
	var checkName = false;
	var checkKana = false;
	var code = document.getElementById('companyCode').value;
	if(code){
		if(code.match(/[^0-9A-Z]/g)){
			alert("企業コードに数値・大文字アルファベット以外が含まれています");
		} else {
			checkCode = true;
		}
	} else {
		alert("企業コードを入力してください");
	}
	var name = document.getElementById('companyName').value;
	if(!name){
		alert("企業名を入力してください");
	} else {
		checkName = true;
	}
	var kana = document.getElementById('companyNameKana').value;
	if(!kana){
		alert ("企業名カナを入力してください");
	} else {
		checkKana = true;
	}
	if(checkCode && checkName && checkKana){
		return true;
	} else {
		return false;
	}
}