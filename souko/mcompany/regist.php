<?php
	require_once("../parts/pagechk.inc");
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
		<title>企業コード追加</title>
		<script type="text/javascript" src="formCheck.js"></script>
		<style type="text/css">
			body {
				font-size : 12px;
			}
			table.inputTbl {
				font-size : 12px;
				border : 0px;
				border-collapse: collapse;
			}
			table.inputTbl th {
				border : 1px #000000 solid;
				text-align : left;
				padding : 5px 5px;
				background-color : #0000CD;
				color : #FFFFFF;
				vertical-align : middle;
			}
			table.inputTbl td {
				border : 1px #000000 solid;
				padding : 2px 2px;
				vertical-align : middle;
			}
			table.dataTbl {
				font-size : 12px;
				border : 0px;
				border-collapse: collapse;
			}
			table.dataTbl th {
				border : 1px #000000 solid;
				text-align : left;
				padding : 5px 5px;
				background-color : #0000CD;
				color : #FFFFFF;
				vertical-align : middle;
			}
			table.dataTbl td {
				border : 1px #000000 solid;
				padding : 5px 5px;
				vertical-align : middle;
			}
		</style>
	</head>
	<body>
	<?php
		//ファイルの読み込み
		//PEARの利用     -------(1)
		require_once("DB.php");
		require_once("../parts/selectvalue_souko.php");
		//ログイン情報の読み込み
		require_once("../parts/login_souko.php");
		//テーブルデータ取得用SQL
		$selectSql = "select COMPANYCODE, COMPANYNAME, COMPANYNAMEKANA, MODIFYDATE, MODIFYTIME from M_COMPANY order by COMPANYCODE";
		$selectList = $db->query($selectSql);
		if(DB::isError($selectList)){
			$selectList->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
	?>
		<div style="margin-left:10px; margin-top:30px; font-size:15px;">
			<b>【企業コード追加】</b>
		</div>
		<hr>
		<div style="margin-left:10px; margin-top:15px;">
		    <B>●追加データ入力</B><br /><br />
		    <form action="insert.php" method="post" onSubmit="return checkForm()">
		        <table class="inputTbl">
			        <tr style="border:1px;">
			            <th>企業コード</th>
			            <td><input type="text" style="ime-mode: disabled;" id="companyCode" name="companyCode" size="20" maxlength="4"> (半角数字4桁) </td>
			        </tr>
			        <tr>
			            <th>企業名</th>
			            <td><input type="text" id="companyName" name="companyName" size="50" maxlength="25"> (全角25文字) </td>
			        </tr>
			        <tr>
			            <th>企業名カナ</th>
			            <td><input type="text" id="companyNameKana" name="companyNameKana" size="50" maxlength="25"> (全角25文字) </td>
			        </tr>
		        </table>
		        <br />
		        <input type="submit" name="button1" value="登録">
		        <input type="button" value="戻る" onClick="location.href='../../index.php'">
		    </form>
		</div>
		
		<hr style="margin:20px 0px;">

		<div style="margin-left:10px;">
		    <B>●登録済データ</B><br /><br />
		    <table class="dataTbl">
		    	<tr>
		    		<th>No</th>
		    		<th>企業コード</th>
		    		<th>企業名</th>
		    		<th>企業名カナ</th>
		    		<th>登録日時</th>
<?php
/** 基本削除しないのでコメントアウト
		    		<th>削除</th>
*/
?>
		    	</tr>
	    		<?php
	    			$count = 0;
	    		    while($row = $selectList->fetchRow()){
	    		    	$count++;
	    		    	print "<tr>\r\n";
	    		    	print "<td>".$count."</td>\r\n";
						print "<td>".$row[0]."</td>\r\n";
						print "<td>".$row[1]."</td>\r\n";
						print "<td>".$row[2]."</td>\r\n";
						print "<td>".substr($row[3],0,4)."/".substr($row[3],4,2)."/".substr($row[3],6,2)." ".substr($row[4],0,2).":".substr($row[4],2,2).":".substr($row[4],4,2)."</td>\r\n";
//						print "<td style=\"padding-top:12px;\"><form action=\"delete.php\" method=\"post\"><input type=\"hidden\" name=\"deleteKey\" value="."\"".$row[0]."\""."><input type=\"submit\" value=\"削除\"></form></td>\r\n";
						print "</tr>\r\n";
					}
					$selectList->free();
					$db->disconnect();
	    		?>
		    </table>
	    </div>
	</body>
</html>