<?php
	print "******************* ".date("Y/m/d H:i:s")."　本人確認発送伝票番号取込処理開始\r\n";
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//*************************************************************************************
$today = date("Ymd");
$todaymmdd = date("m")."/".date("d");
$contents = @file('Eoa'.$today.'.csv');
if (empty($contents))
{
	print "対象のファイルがありません。\r\n";
	print "******************* ".date("Y/m/d H:i:s")."　本人確認発送伝票番号取込処理異常終了\r\n";
	die(99);
}
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\r\n";
}

//全体の確認チェックフラグ
$_SESSION["HSMAP"]["chk_flg"]="0";
$_SESSION["HSMAP"]["updok"]=0;
$_SESSION["HSMAP"]["updng"]=0;
$i=1;
foreach($contents as $readkeisanline){
	if ($i==0){
		$inline=preg_split('/,/',$readkeisanline);
		$inline[2]=preg_replace("/(\r\n|\n|\r)/","",$inline[2]);
//		print $inline[0].":".$inline[1].":".$inline[2]."\r\n";
		$sql="update  tsell set memo='［".$todaymmdd."　身分証明書による本人確認手続き不可のため、受取人確認書類発送。伝票番号:" .$inline[0]."］'||CHR(13)||memo where sell_no='".$inline[2]."'";
//		print $sql."\r\n";
		$res = $db->query($sql);
		if(DB::isError($res)){
			$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
		}
		output_log($sql,"batch",'honninndenpyou_update_sql');
		if ($db->affectedRows()==0)
	{$tempcheck=1;}
		$db->commit();
//データの開放
	if ($tempcheck==0)
	{
		print "買取受付番号：".$inline[2]."　伝票番号：".$inline[0]."　正常終了\r\n";
	$_SESSION["HSMAP"]["updok"]++;
	}
	else
	{
		print "買取受付番号：".$inline[2]."　伝票番号：".$inline[0]."　未更新\r\n";
		$_SESSION["HSMAP"]["updng"]++;
	}
}
$i=0;
$tempcheck=0;
}
$db->disconnect();
print "処理件数：".($_SESSION["HSMAP"]["updok"]+$_SESSION["HSMAP"]["updng"])."件\r\n";
print "正常終了：".$_SESSION["HSMAP"]["updok"]."件\r\n";
print "異常終了：".$_SESSION["HSMAP"]["updng"]."件\r\n";
print "******************* ".date("Y/m/d H:i:s")."　本人確認発送伝票番号取込処理終了\r\n";
?>
