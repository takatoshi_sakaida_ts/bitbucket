<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文情報抽出結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti2 = addslashes(@$_POST["TI2"]);
$keyti3 = addslashes(@$_POST["TI3"]);
$keyti4 = addslashes(@$_POST["TI4"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) and empty($keyti4)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../parts/selectvalue_souko.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
$j="ng";
$i=0;
$sql="select distinct d.instorecode,d.stock_tp,m.GOODS_NAME1||m.GOODS_NAME2||m.GOODS_NAME3,m.jan,substr(m.genre3code,1,4),substr(m.genre3code,1,6),substr(m.genre3code,1,8),m.MKT_PRICE,d.REAL_SALE_PR,t.ORD_DM,d.ord_no,d.ord_seq,(GOODS_CNT-ORD_CNCL_CNT),v.DELV_DM from torder t,torderdtl d,tgoods_bko m,TORDERDELV v where t.ord_no=d.ord_no and d.delv_no=v.delv_no and (d.GOODS_CNT-d.ORD_CNCL_CNT)>0 and d.instorecode=m.instorecode ";
if (empty($keyti))
{
	if (empty($keyti4)) {}else
	{
		$sql=$sql." and (t.ORDR_ZIP_ADDR1 || t.ORDR_ZIP_ADDR2 || t.ORDR_ZIP_ADDR2) like '%" . $keyti4 . "%' ";
	}
}else
{
	$sql=$sql." and t.mem_id = '" . $keyti . "' ";
}
if (empty($keyti2)) {}else
{
	$sql=$sql." and substr(v.delv_DM,1,8) >= '" . $keyti2 . "' ";
}
if (empty($keyti3)) {}else
{
	$sql=$sql." and substr(v.delv_DM,1,8) <= '" . $keyti3 . "' ";
}

$sql=$sql." union select distinct dd.instorecode,dd.stock_tp,mm.GOODS_NAME1||mm.GOODS_NAME2||mm.GOODS_NAME3,mm.jan,substr(mm.genre3code,1,4),substr(mm.genre3code,1,6),substr(mm.genre3code,1,8),mm.MKT_PRICE,dd.REAL_SALE_PR,tt.ORD_DM,dd.ord_no,dd.ord_seq,(dd.GOODS_CNT-dd.ORD_CNCL_CNT),vv.DELV_DM from torder_old tt,torderdtl_old dd,tgoods_bko mm,TORDERDELV_old vv where tt.mem_id = '" . $keyti . "' and tt.ord_no=dd.ord_no and dd.delv_no=vv.delv_no and (dd.GOODS_CNT-dd.ORD_CNCL_CNT)>0 and dd.instorecode=mm.instorecode ";
if (empty($keyti))
{
	if (empty($keyti4)) {}else
	{
		$sql=$sql." and (tt.ORDR_ZIP_ADDR1 || tt.ORDR_ZIP_ADDR2 || tt.ORDR_ZIP_ADDR2) like '%" . $keyti4 . "%' ";
	}
}else
{
	$sql=$sql." and tt.mem_id = '" . $keyti . "' ";
}
if (empty($keyti2)) {}else
{
	$sql=$sql." and substr(vv.delv_DM,1,8) >= '" . $keyti2 . "' ";
}
if (empty($keyti3)) {}else
{
	$sql=$sql." and substr(vv.delv_DM,1,8) <= '" . $keyti3 . "' ";
}
$sql=$sql." order by ord_no,ord_seq";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
while($row = $res->fetchRow()){
	$j="ok";
	$instorecode[$i]=$row[0];
	$stock_tp[$i]=$row[1];
	$GOODS_NM[$i]=$row[2];
	$jan[$i]=$row[3];
	$genre1code[$i]=$row[4];
	$genre2code[$i]=$row[5];
	$genre3code[$i]=$row[6];
	$MKT_PRICE[$i]=$row[7];
	$REAL_SALE_PR[$i]=$row[8];
	$ORD_DM[$i]=$row[9];
	$ORD_no[$i]=$row[10];
	$ORD_seq[$i]=$row[11];
	$suu[$i]=$row[12];
	$DELV_DM[$i]=$row[13];
	$i++;
}
if ($j!="ng")
{
$chk_flg="0";
	//検索結果の表示
	print "<strong>注文情報抽出結果：". $keyti."</strong> <br><br>\n";
	print "<table border=1>\n";
		//項目名の表示
	print "<tr><td nowrap>注文番号</td><td nowrap>詳細番号</td><td nowrap>注文数量</td><td nowrap>インストアコード</td><td nowrap>新古区分</td><td nowrap>商品名</td><td nowrap>JANコード</td><td nowrap>ジャンルコード１</td><td nowrap>ジャンルコード２</td><td nowrap>ジャンルコード３</td><td nowrap>定価</td><td nowrap>販売単価</td><td nowrap>出荷日時</td></tr>";
	for ($i = 0 ; $i <count($instorecode); $i++) {
		print "<tr><td nowrap>".$ORD_no[$i]."</td>";
		print "<td nowrap>".$ORD_seq[$i]."</td>";
		print "<td nowrap>".$suu[$i]."</td>";
		print "<td nowrap>".$instorecode[$i]."</td>";
		print "<td nowrap>".Retzaikokubunec($stock_tp[$i])."</td>";
		print "<td nowrap>".$GOODS_NM[$i]."</td>";
		print "<td nowrap>".$jan[$i]."</td>";
		print "<td nowrap>".$genre1code[$i]."</td>";
		print "<td nowrap>".$genre2code[$i]."</td>";
		print "<td nowrap>".$genre3code[$i]."</td>";
		print "<td nowrap>".$MKT_PRICE[$i]."</td>";
		print "<td nowrap>".$REAL_SALE_PR[$i]."</td>";
		print "<td nowrap>".$DELV_DM[$i]."</td></TR>";
	}
	print "</table>";
	//データの開放
	$res->free();
	$db->disconnect();
}
else
{
	$chk_flg="1";
	print "会員IDに誤りがあるかまたは、注文されていません:" .$keyti  ;
}
?>
<BR>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
</FORM>
</body>
</html>