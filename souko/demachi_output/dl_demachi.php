<?php
require '../parts/pagechk.inc';
$today = date("YmdHis");
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
output_log("デマチメール登録者ダウンロード",$_SESSION["user_id"],'demachiout');
// 前画面からの検索キーワードを取得する
$keyti1 = addslashes(@$_POST["TI"]);
$file_name = "./dlfile/demachi_" . $today."_".$keyti1 . ".csv";
$file_name_mobile = "./dlfile/demachi_mobile_" . $today."_".$keyti1 . ".csv";
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//pc
$sql = "select '0',MEM_ID from talertmail_demachi d,tmember m where d.mem_no=m.mem_no and SEND_TO in ('0','2') and EML_DOMAIN_TP1='0' and new_instorecode = '".$keyti1."'";
$sql = $sql." union select '0',MEM_EML_MBL from talertmail_demachi d,tmember m where d.mem_no=m.mem_no and SEND_TO in ('1','2') and EML_DOMAIN_TP2='0' and new_instorecode = '".$keyti1."'";

//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	require '../parts/outputcsv_query.php';
	outputcsv($res,$file_name);
$res->free();
//mobile
$sql = "select '0',MEM_ID from talertmail_demachi d,tmember m where d.mem_no=m.mem_no and SEND_TO in ('0','2') and EML_DOMAIN_TP1='1' and new_instorecode = '".$keyti1."'";
$sql = $sql." union select '0',MEM_EML_MBL from talertmail_demachi d,tmember m where d.mem_no=m.mem_no and SEND_TO in ('1','2') and EML_DOMAIN_TP2='1' and new_instorecode = '".$keyti1."'";

//print $sql;
$res2 = $db->query($sql);
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	outputcsv($res2,$file_name_mobile);
$res2->free();

$db->disconnect();
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>デマチメール登録情報ダウンロード</title>
</head>
<BODY BGCOLOR="#FFFFFF">
<?PHP
	print "<a href='" .$file_name . "'>こちらからダウンロードしてください</a><BR><BR>";
	print "<a href='" .$file_name_mobile . "'>こちらからダウンロードしてください（モバイル）</a><BR><BR>";
?>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>