<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>組み戻し買取口座修正確認画面</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
require_once("../log/loger.inc");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
	print "<br>\n<strong>組み戻し買取口座修正画面：". $keyti."</strong><br><br>\n<hr>";
$toalcheck=0;
//ブックマーク情報
$j="ng";
$sql="SELECT "
."T.MEM_NO,"
."M.BANK_CD,"
."M.BANK_BRANCH_CD,"
."M.BANK_NM,"
."M.BRANCH_NM,"
."M.BANK_ACC_TP,"
."M.BANK_ACC_NO,"
."M.BANK_ACC_NM,"
."M.POSTAL_SIGN,"
."M.POSTAL_ACC_NO,"
."M.POSTAL_NM,"
."T.BANK_CD,"
."T.BANK_BRANCH_CD,"
."T.BANK_NM,"
."T.BRANCH_NM,"
."T.BANK_ACC_TP,"
."T.BANK_ACC_NO,"
."T.BANK_ACC_NM,"
."T.POSTAL_SIGN,"
."T.POSTAL_ACC_NO,"
."T.POSTAL_NM "
."FROM TSELL T,TMEM_ACCOUNT M "
."WHERE T.MEM_NO=M.MEM_NO "
."AND T.RECEIPT_TP='1' AND T.SELL_NO='" . $keyti ."'";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
	$i=0;
	while($row = $res->fetchRow()){
		if (isset($row[0]))
		{
			$j="ok";
			$_SESSION["HSMAP"]["MEM_NO"]=$row[0];
			$_SESSION["HSMAP"]["BANK_CD"]=$row[1];
			$_SESSION["HSMAP"]["BANK_BRANCH_CD"]=$row[2];
			$_SESSION["HSMAP"]["BANK_NM"]=$row[3];
			$_SESSION["HSMAP"]["BRANCH_NM"]=$row[4];
			$_SESSION["HSMAP"]["BANK_ACC_TP"]=$row[5];
			$_SESSION["HSMAP"]["BANK_ACC_NO"]=$row[6];
			$_SESSION["HSMAP"]["BANK_ACC_NM"]=$row[7];
			$_SESSION["HSMAP"]["POSTAL_SIGN"]=$row[8];
			$_SESSION["HSMAP"]["POSTAL_ACC_NO"]=$row[9];
			$_SESSION["HSMAP"]["POSTAL_NM"]=$row[10];
			$_SESSION["HSMAP"]["OLD_BANK_CD"]=$row[11];
			$_SESSION["HSMAP"]["OLD_BANK_BRANCH_CD"]=$row[12];
			$_SESSION["HSMAP"]["OLD_BANK_NM"]=$row[13];
			$_SESSION["HSMAP"]["OLD_BRANCH_NM"]=$row[14];
			$_SESSION["HSMAP"]["OLD_BANK_ACC_TP"]=$row[15];
			$_SESSION["HSMAP"]["OLD_BANK_ACC_NO"]=$row[16];
			$_SESSION["HSMAP"]["OLD_BANK_ACC_NM"]=$row[17];
			$_SESSION["HSMAP"]["OLD_POSTAL_SIGN"]=$row[18];
			$_SESSION["HSMAP"]["OLD_POSTAL_ACC_NO"]=$row[19];
			$_SESSION["HSMAP"]["OLD_POSTAL_NM"]=$row[20];
		}else{
			$j="ng";
		}
	}
//データの開放
	$res->free();
if ($j!="ng")
{
	print "会員情報の値が、買取情報の値に修正されます。<br>口座番号は、暗号化されていますので値が違うことを確認にしてください。<br><br>";
	print "<table border=1>\n";
	print "<tr>\n";
	//検索結果の表示
	//項目名の表示
	print "<td>　</td><td bgcolor=#CCFFCC>会員情報</td><td align=center>←←←</td><td bgcolor=#CCFFCC>買取情報（正しい値）</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>銀行名</td><td>　".$_SESSION["HSMAP"]["BANK_NM"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_BANK_NM"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>銀行支店名</td><td>　".$_SESSION["HSMAP"]["BRANCH_NM"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_BRANCH_NM"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>銀行口座種別</td><td>　".$_SESSION["HSMAP"]["BANK_ACC_TP"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_BANK_ACC_TP"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>銀行口座番号</td><td>　".$_SESSION["HSMAP"]["BANK_ACC_NO"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_BANK_ACC_NO"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>銀行口座名義</td><td>　".$_SESSION["HSMAP"]["BANK_ACC_NM"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_BANK_ACC_NM"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>郵便口座記号</td><td>　".$_SESSION["HSMAP"]["POSTAL_SIGN"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_POSTAL_SIGN"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>郵便口座番号</td><td>　".$_SESSION["HSMAP"]["POSTAL_ACC_NO"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_POSTAL_ACC_NO"]."</td></tr>";
	print "<tr><td bgcolor=#CCFFCC>郵便口座番号</td><td>　".$_SESSION["HSMAP"]["POSTAL_NM"]."</td><td>　</td><td>　".$_SESSION["HSMAP"]["OLD_POSTAL_NM"]."</td></tr>";
	print "</table>";
?>
	<BR>
	<form method="POST" action="hurikomiud_ent.php" name ="osirasedel">
	<input type="submit" value="修正実行">
<?PHP
}
	else
{
	print "<BR>会員情報に登録されていません。<BR><BR>";
}

$db->disconnect();
?>
　　<INPUT TYPE="BUTTON" VALUE="　戻　る　" onClick="history.back()">
</FORM>
</body>
</html>