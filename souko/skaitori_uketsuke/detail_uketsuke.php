<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>買取受付検索結果</title>

<STYLE TYPE="text/css"> 
<!-- 

#menu1 { 
border-collapse: collapse; /* 枠線の表示方法（重ねる） */ 
} 

#menu1 TD { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #FFFFFF; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
} 
#menu1 TH { 
border: 1px #000000 solid; /* セルの枠線（太さ・色・スタイル） */ 
background-color: #cccccc; /* セルの背景色 */ 
padding: 3px; /* セル内の余白 */ 
font-weight:normal;
width:20%;
}
-->
</style>

</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("selectvalue_search.php");
// 前画面からの検索キーワードを取得する
$sellno = addslashes(@$_GET["sellno"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($sellno)) {
	print "データが入力されていません．<br>";
return;
}

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	die($db->getMessage());
}

$sql = "select * from tsell where sell_no = '{$sellno}'";
//echo $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),NULL,NULL,NULL);
}

$row = $res->fetchRow(DB_FETCHMODE_ASSOC);
?>

<b>受付情報</b>
<TABLE ID="menu1" width="900">
<!--
  <tr>
    <th>店舗名・受付番号</th>
    <td>店舗名：
<?PHP
//print $row["PICKUP_STORE"];
?>
　受付番号：
<?PHP
//print $row["PICKUP_STORENO"];
?>
	</td>
  </tr>
-->
  <tr>
    <th>買取受付番号</th>
    <td>
<?PHP
print $row["SELL_NO"];
?>
	</td>
  </tr>
  <tr>
    <th>箱数</th>
    <td>
<?PHP
print $row["BOX"];
?>
	</td>
  </tr>

  <tr>
    <th>フリガナ</th>
    <td>
<?PHP
print $row["PICKUP_NM_LAST"];
print $row["PICKUP_NM_ETC"];
?>
    </td>
</TR>

  <tr>
    <th>名前</th>
    <td>
<?PHP
print $row["PICKUP_NM_FST"];
print $row["PICKUP_NM_MID"];
?>
    </td>
</TR>

  <tr>
    <th>生年月日</th>
    <td>
<?PHP
print $row["MEM_BIRTH_DT"];
?>
    </td>
  </tr>


  <tr>
    <th>性別</th>
    <td>
<?PHP
print Retseibetu($row["MEM_SEX"]);
?>    </td>
  </tr>

  <tr>
    <th>職業</th>
    <td>
<?PHP
print Retshokugyou($row["MEM_JOB"]);
?>
    </td>
  </tr>

  <tr>
    <th>郵便番号</th>
    <td>
<?PHP
print Retzip($row["PICKUP_ZIP_CD"]);
?>
    </td>
  </tr>

  <tr>
    <th>都道府県</th>
    <td>
<?PHP
print $row["PICKUP_ZIP_ADDR1"];
?>
    </td>
  </tr>
  <tr>
    <th>住所 市区町村</th>
    <td>
<?PHP
print $row["PICKUP_ZIP_ADDR2"];
?>
    </td>
  </tr>
  <tr>
    <th>　　 番地</th>
    <td>
<?PHP
print $row["PICKUP_ZIP_ADDR3"];
?>
    </td>
  </tr>
  <tr>
    <th>　　 建物名・部屋番号</th>
    <td>
<?PHP
print $row["PICKUP_DTL_ADDR"];
?>
    </td>
  </tr>

  <tr>
    <th>電話番号</th>
    <td>
<?PHP
print $row["PICKUP_TEL_NO"];
?>
	</td>
  </tr>

  <tr>
    <th>日中連絡先</th>
    <td>
<?PHP
print $row["PICKUP_TEL_NO_2"];
?>
	</td>
  </tr>

  <tr>
    <th>買取金額の承認とお値段のつかない品物</th>
    <td>
<?PHP
print Retdtaiou($row["DISPOSE"]);
?>    </td>
  </tr>

  <tr>
    <th>メモ</th>
    <td>
<?PHP
print $row["MEMO"];
?>
	</td>
  </tr>
</TABLE>
<br><br>
<INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()">
<button type="button"onclick="location.href='/'">メニューに戻る</button>

<?php
    //データの開放
    $res->free();
    $db->disconnect();
?>


</body>
</html>