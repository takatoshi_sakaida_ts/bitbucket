<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>受付番号検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/login_ec.php");
require_once("selectvalue_search.php");
// 前画面からの検索キーワードを取得する
$id = addslashes(@$_GET["id"]);

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($id)) {
	print "正しい検索条件を入力してください<br>";
}

$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	die($db->getMessage());
}

$sql = "select SELL_NO, PICKUP_NM_FST, PICKUP_NM_MID, SELL_STAT, SELL_FORM_GET_DT, MEMO from tsell where memo like '%受付番号（ID):{$id}　%'";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong><BR>検索結果 ID:". $id."</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)){
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=40>項番</td><td nowrap width='140'>買取受付番号</td><td nowrap width=120>受付日</td><td nowrap width=200>氏名</td><td nowrap width=80>状態</td><td nowrap width='100'>備考</td>";
print "</tr>";
	print "<tr>";
//項番
	print "<td>".$j ."</td>";
//注文番号
	print "<td><a href='detail_uketsuke.php?sellno=" .$row["SELL_NO"] ."'>".$row["SELL_NO"] ."</td>";
	print "<td>".$row["SELL_FORM_GET_DT"] ."</td>";
	print "<td>".$row["PICKUP_NM_FST"] .$row["PICKUP_NM_MID"] ."</td>";
	print "<td>".$row["SELL_STAT"] ."</td>";
	print "<td>".$row["MEMO"] ."</td>";
	print "</tr></table>";
	print "<HR>";
	$j++;
}
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
