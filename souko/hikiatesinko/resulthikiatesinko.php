<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<?PHP
require_once("../parts/pagechk.inc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>強制引当作業結果</title>
</head>
<body>
<?php
//**********************************************************************************************************
//ファイルの読み込み
//PEARの利用     -------(1)
//**********************************************************************************************************
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$_SESSION["HSMAP"]["RECEPTIONCODE"] = addslashes(@$_POST["TI"]);
$_SESSION["HSMAP"]["INSTORECODE"] = addslashes(@$_POST["TI2"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($_SESSION["HSMAP"]["RECEPTIONCODE"]) && empty($_SESSION["HSMAP"]["INSTORECODE"])) {
	print "検索条件を入力してください<br>";
}
//出力ファイル名
$_SESSION["HSMAP"]["file_name_d_reception_goods"] = "./result/d_reception_goods_".$_SESSION["HSMAP"]["RECEPTIONCODE"]."_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
$_SESSION["HSMAP"]["file_name_d_stock"] = "./result/d_stock_".$_SESSION["HSMAP"]["RECEPTIONCODE"]."_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
$_SESSION["HSMAP"]["file_name_d_goods_stock"] = "./result/d_goods_stock_".$_SESSION["HSMAP"]["RECEPTIONCODE"]."_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
$_SESSION["HSMAP"]["file_name_d_order_goods"] = "./result/d_order_goods_".$_SESSION["HSMAP"]["RECEPTIONCODE"]."_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
$_SESSION["HSMAP"]["file_name_d_reception_invoiceno"] = "./result/d_reception_invoiceno_".$_SESSION["HSMAP"]["RECEPTIONCODE"]."_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
$_SESSION["HSMAP"]["file_name_torderdtl"]= "./result/torderdtl_".$_SESSION["HSMAP"]["RECEPTIONCODE"]."_".$_SESSION["HSMAP"]["INSTORECODE"].".csv";
$total_check = "0";
$total_check_ec = "0";
//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//**********************************************************************************************************
//SQL文のCOUNT関数を使用
$_SESSION["HSMAP"]["sql_D_RECEPTION"] = "SELECT RECEPTIONCODE,DETAILNO,STOCKNO,LOCATIONCODE,INSTORECODE,NEWUSED_KIND,PICKED_KIND,CANCEL_KIND,";
$_SESSION["HSMAP"]["sql_D_RECEPTION"] = $_SESSION["HSMAP"]["sql_D_RECEPTION"]."ADV_RESERVE_KIND FROM D_RECEPTION_GOODS WHERE RECEPTIONCODE='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."'and instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."'";
//print $_SESSION["HSMAP"]["sql_D_RECEPTION"];
$res = $db->query($_SESSION["HSMAP"]["sql_D_RECEPTION"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
require_once("../log/loger.inc");
output_log($_SESSION["HSMAP"]["sql_D_RECEPTION"],$_SESSION["user_id"],'hikiatesinko');
//検索結果の表示
print "<strong><BR>注文番号：". $_SESSION["HSMAP"]["RECEPTIONCODE"]."インストアコード：".$_SESSION["HSMAP"]["INSTORECODE"]."</strong> <br><br>\n<HR>";
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>注文詳細</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=120>receptioncode</td><td nowrap width=40>detailno</td><td nowrap width=100>stockno</td><td nowrap width=100>locationcode</td><td nowrap width=100>instorecode</td><td nowrap width=120>newused_kind</td>";
print "<td nowrap width=80>pick_kind</td><td nowrap width=80>cancel_kind</td><td nowrap width=80>adv_reserve_kind</td></tr>";
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_reception_goods"], "a" );
	fputs($fp,"RECEPTIONCODE,DETAILNO,STOCKNO,LOCATIONCODE,INSTORECODE,NEWUSED_KIND,PICKED_KIND,CANCEL_KIND,ADV_RESERVE_KIND");
	fputs($fp,"\n");
while($row = $res->fetchRow()){
	$str="";
	$warnmess="<br>";
	print "<tr>";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
	if ($row[2]=="-1"){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."stocknoが不正です。<BR>";
	}
	if ($row[3]=="9900000000"){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."ロケーションが不正です。<BR>";
	}
	if ($row[5]=="1"){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."newused_kindが不正です。<BR>";
	}
	if ($row[6]=="1"){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."pick_kindが不正です。<BR>";
	}
	if ($row[7]=="0"){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."cancel_kindが不正です。<BR>";
	}
	if ($row[8]=="0"){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."adv_recerve_kindが不正です。。<BR>";
	}

}
print "</table>";
fclose( $fp );
//データの開放
if ($j=="2"){
}else{
	$total_check = "1";
	$warnmess =$warnmess."該当行数が不正です。<BR>";
}
print "<font color=red><b>".$warnmess."</b></font>";

$res->free();
//**********************************************************************************************************
//在庫テーブルチェック
//**********************************************************************************************************
$_SESSION["HSMAP"]["setlocationcode"]="ng";
$_SESSION["HSMAP"]["setstockno"]="ng";
$_SESSION["HSMAP"]["sql_D_STOCK"] = "SELECT s.STOCKNO,s.STOCKDATE,s.LOCATIONCODE,s.INSTORECODE,s.NEWUSED_KIND,s.STOCKRESERVE_KIND,m.zonecode";
$_SESSION["HSMAP"]["sql_D_STOCK"] = $_SESSION["HSMAP"]["sql_D_STOCK"]." FROM D_STOCK s,m_location m WHERE s.LOCATIONCODE=m.LOCATIONCODE and s.instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."'";
//print $_SESSION["HSMAP"]["sql_D_STOCK"];
$res = $db->query($_SESSION["HSMAP"]["sql_D_STOCK"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_STOCK"],$_SESSION["user_id"],'hikiatesinko');
//検索結果の表示
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>在庫詳細</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=120>stockno</td><td nowrap width=40>stockdate</td><td nowrap width=100>locationcode</td><td nowrap width=100>instorecode</td><td nowrap width=120>newused_kind</td>";
print "<td nowrap width=80>STOCKRESERVE_KIND</td><td nowrap width=80>ZONECODE</td></tr>";
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_stock"], "a" );
	fputs($fp,"STOCKNO,STOCKDATE,LOCATIONCODE,INSTORECODE,NEWUSED_KIND,STOCKRESERVE_KIND,ZONECODE");
	fputs($fp,"\n");
while($row = $res->fetchRow()){
	print "<tr>";
	$str="";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
	if ($row[4]=="1" && $row[5]=="0" && mb_substr($row[2],0,1)<"9") {
		$_SESSION["HSMAP"]["setlocationcode"]=$row[2];
		$_SESSION["HSMAP"]["setstockno"]=$row[0];
		$_SESSION["HSMAP"]["zonecode"]=$row[6];
	}
}
fputs($fp,"select location:".$_SESSION["HSMAP"]["setlocationcode"]." select stockno:".$_SESSION["HSMAP"]["setstockno"]."\n");
print "</table>";
fclose( $fp );
if ($_SESSION["HSMAP"]["setlocationcode"]=="ng")
{
	print "<font color=red><b>在庫がありません。<br></b></font>";
	$total_check = "1";
}
//データの開放
$res->free();
//**********************************************************************************************************
//商品在庫テーブルチェック
//**********************************************************************************************************
$_SESSION["HSMAP"]["sql_D_GOODS_STOCK"] = "SELECT LOCATIONCODE,INSTORECODE,NEWUSED_KIND,QTY,RESERVE_QTY";
$_SESSION["HSMAP"]["sql_D_GOODS_STOCK"] = $_SESSION["HSMAP"]["sql_D_GOODS_STOCK"]." FROM D_GOODS_STOCK WHERE instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."' and locationcode='".$_SESSION["HSMAP"]["setlocationcode"]."'";
//print $_SESSION["HSMAP"]["sql_D_GOODS_STOCK"];
$res = $db->query($_SESSION["HSMAP"]["sql_D_GOODS_STOCK"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_GOODS_STOCK"],$_SESSION["user_id"],'hikiatesinko');
//検索結果の表示
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>商品在庫詳細</strong><HR>";
print "予定引当在庫　locationcode".$_SESSION["HSMAP"]["setlocationcode"]."　在庫番号".$_SESSION["HSMAP"]["setstockno"];
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=100>locationcode</td><td nowrap width=100>instorecode</td><td nowrap width=120>newused_kind</td>";
print "<td nowrap width=80>QTY</td><td nowrap width=80>RESERVE_QTY</td></tr>";
//項番
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_goods_stock"], "a" );
	fputs($fp,"LOCATIONCODE,INSTORECODE,NEWUSED_KIND,QTY,RESERVE_QTY");
	fputs($fp,"\n");
while($row = $res->fetchRow()){
	$str="";
	print "<tr>";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
	if ($row[2]=="1" && $row[3]>$row[4]){}else
	{
		$total_check = "1";
		$warnmess =$warnmess."在庫がありません。。<BR>";
	}
}
print "</table>";
fclose( $fp );
//データの開放
$res->free();

//**********************************************************************************************************
//納品書テーブルチェック
//**********************************************************************************************************
$_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"] = "SELECT * FROM D_RECEPTION_INVOICENO WHERE RECEPTIONCODE='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."' order by invoiceno";
//print $_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"];
$invoicechk_d="ng";
$res = $db->query($_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"]);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"],$_SESSION["user_id"],'hikiatesinko');
//検索結果の表示
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	print "<strong><BR>納品書データ</strong><HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	//項目名の表示
	print "<td nowrap width=100>invoiceno</td><td nowrap width=100>ord_no</td><td nowrap width=100>zonecode</td><td nowrap width=100>modifydate</td><td nowrap width=100>modifytime</td></tr>";
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_d_reception_invoiceno"], "a" );
	fputs($fp,$_SESSION["HSMAP"]["sql_D_RECEPTION_INVOICENO"]."\n");
while($row = $res->fetchRow()){
	$str="";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
	print "</TR>";
	if ($row[2]==$_SESSION["HSMAP"]["zonecode"]){
		$invoicechk_d="ok";
	}
}
print "</table>";
fclose( $fp );
if ($invoicechk_d=="ok"){
print $_SESSION["HSMAP"]["zonecode"]."ゾーンの納品書は存在します<BR>";
	$dzonechk=1;
}
else
{
print $_SESSION["HSMAP"]["zonecode"]."ゾーンの納品書は存在しませんので作成してください<BR>";
	$dzonechk=0;
}
//データの開放
$res->free();
$db->disconnect();
//**********************************************************************************************************
//EC側処理
//**********************************************************************************************************
//ログイン情報の読み込み
require_once("../parts/login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//
//******************************************************
$_SESSION["HSMAP"]["sql_ec"]="select ord_no,instorecode,stock_tp,GOODS_CNT,ORD_CNCL_CNT from torderdtl where ord_no='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."' and instorecode='". $_SESSION["HSMAP"]["INSTORECODE"]."'";
$res = $db->query($_SESSION["HSMAP"]["sql_ec"]);
if(DB::isError($res)){
	print $res->getMessage();
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
output_log($_SESSION["HSMAP"]["sql_ec"],$_SESSION["user_id"],'hikiatesinko');
//検索結果の表示
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
print "<strong><BR>EC注文状況</strong><HR>";
print "<table border=1>\n";
print "<tr bgcolor=#ccffff>\n";
//項目名の表示
print "<td nowrap width=100>ord_no</td><td nowrap width=100>instorecode</td><td nowrap width=100>stock_tp</td><td nowrap width=100>GOODS_CNT</td><td nowrap width=100>ORD_CNCL_CNT</td></tr>";
//項番
//ファイル出力準備
	$fp = fopen($_SESSION["HSMAP"]["file_name_torderdtl"], "a" );
	fputs($fp,"ord_no,instorecode,stock_tp,GOODS_CNT,ORD_CNCL_CNT");
	fputs($fp,"\n");

while($row = $res->fetchRow()){
$str="";
	print "<tr>";
	$ncols = $res->numCols();
	for($i=0;$i<$ncols;$i++){
			print "<td>". $row[$i] . "</td>";//行の列データを取得
			$str=$str . $row[$i] . ",";//行の列データを取得
	}
	if ($row[2]=="2"){}else
	{
		$total_check_ec = "1";
		print "<font color=red><b>新古区分が不正です。</b></font><BR>";
	}
	if (($row[3]-$row[4])=="0")
	{
		$total_check_ec = "1";
		print "<font color=red><b>キャンセルされています。</b></font><BR>";
	}
	print "</tr>";
//ファイル出力
	$str = ereg_replace("\r|\n","",$str);
	fputs($fp,$str);
	fputs($fp,"\n");
	$j++;
}
print "</table>";
fclose( $fp );
$res->free();
//******************************************************

//
if ($total_check_ec == "0" && $total_check=="0")
{
	$_SESSION["HSMAP"]["sql_ec_upd"]="update torderdtl set stock_tp='2' where stock_tp='3' and ord_no='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."' and instorecode='". $_SESSION["HSMAP"]["INSTORECODE"]."'";
/*
	$db->autoCommit( false ); 
	$res = $db->query($_SESSION["HSMAP"]["sql_ec_upd"]);
	if(DB::isError($res)){
		print $res->getMessage();
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($_SESSION["HSMAP"]["sql_ec_upd"],$_SESSION["user_id"],'hikiatesinko');
	$db->commit();
	print "EC側データベースの新古区分が更新されました。";
	$_SESSION["HSMAP"]["hikiate_chk"]=0;
//データの開放
//******************************************************
	$_SESSION["HSMAP"]["sql_ec"]="select ord_no,instorecode,stock_tp,GOODS_CNT,ORD_CNCL_CNT from torderdtl where ord_no='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."' and instorecode='". $_SESSION["HSMAP"]["INSTORECODE"]."'";
	$res = $db->query($_SESSION["HSMAP"]["sql_ec"]);
	if(DB::isError($res)){
		print $res->getMessage();
		$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($_SESSION["HSMAP"]["sql_ec"],$_SESSION["user_id"],'hikiatesinko');
	//検索結果の表示
	$j=1;
	//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	print "<strong><BR>EC注文状況（データ更新後）</strong><HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	//項目名の表示
	print "<td nowrap width=100>ord_no</td><td nowrap width=100>instorecode</td><td nowrap width=100>stock_tp</td><td nowrap width=100>GOODS_CNT</td><td nowrap width=100>ORD_CNCL_CNT</td></tr>";
	//項番
	//ファイル出力準備
		$fp = fopen($_SESSION["HSMAP"]["file_name_torderdtl"], "a" );
		fputs($fp,"ord_no,instorecode,stock_tp,GOODS_CNT,ORD_CNCL_CNT");
		fputs($fp,"\n");

	while($row = $res->fetchRow()){
	$str="";
		print "<tr>";
		$ncols = $res->numCols();
		for($i=0;$i<$ncols;$i++){
				print "<td>". $row[$i] . "</td>";//行の列データを取得
				$str=$str . $row[$i] . ",";//行の列データを取得
		}
		print "</tr>";
	//ファイル出力
		$str = ereg_replace("\r|\n","",$str);
		fputs($fp,$str);
		fputs($fp,"\n");
		$j++;
	}
	print "</table>";
	fclose( $fp );
	$res->free();
*/
	$_SESSION["HSMAP"]["hikiate_chk"]=0;
}
else
{
//	print "<font color=blue><b>ECの新古区分は更新していません。</b></font><BR>";
	$_SESSION["HSMAP"]["hikiate_chk"]=1;
}
//******************************************************
$db->disconnect();
print "<hr>";
$_SESSION["HSMAP"]["sql_d_reception_goods"]="update d_reception_goods set PICKED_KIND='0',locationcode='".$_SESSION["HSMAP"]["setlocationcode"]."',stockno='".$_SESSION["HSMAP"]["setstockno"];
$_SESSION["HSMAP"]["sql_d_reception_goods"]= $_SESSION["HSMAP"]["sql_d_reception_goods"]."',adv_reserve_kind='0',newused_kind='1' where receptioncode='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."' and instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."' and stockno='-1' and cancel_kind='0'";
$_SESSION["HSMAP"]["sql_d_stock"]= "update d_stock set stockreserve_kind='2' where stockreserve_kind='0' and instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."' and STOCKNO='".$_SESSION["HSMAP"]["setstockno"]."'";
$_SESSION["HSMAP"]["sql_d_goods_stock"]= "update d_goods_stock set reserve_qty=reserve_qty+1 where newused_kind='1' and qty>reserve_qty and instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."' and locationcode='".$_SESSION["HSMAP"]["setlocationcode"]."'";
$_SESSION["HSMAP"]["sql_d_order_goods"]= "delete from d_order_goods where instorecode='".$_SESSION["HSMAP"]["INSTORECODE"]."' and receptioncode='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."'";
$_SESSION["HSMAP"]["reception_chk"]=1;
$_SESSION["HSMAP"]["sql_d_reception_invoiceno"]="default";
if ($dzonechk==0)
{
	$_SESSION["HSMAP"]["sql_d_reception_invoiceno"]="insert into d_reception_invoiceno(invoiceno,receptioncode,zonecode,modifydate,modifytime) values(seq_invoiceno.nextval,'".$_SESSION["HSMAP"]["RECEPTIONCODE"]."','".$_SESSION["HSMAP"]["zonecode"]."',(select to_char(sysdate,'yyyymmdd') from dual),(select to_char(sysdate,'HH24MISS') from dual))";
	$_SESSION["HSMAP"]["reception_chk"]=0;
}
print "<hr>";

print "delete from  d_reception_invoiceno where zonecode='Z' and receptioncode='".$_SESSION["HSMAP"]["RECEPTIONCODE"]."';<BR>";

print $_SESSION["HSMAP"]["sql_d_reception_goods"].";<BR>";
print $_SESSION["HSMAP"]["sql_d_stock"].";<BR>";
print $_SESSION["HSMAP"]["sql_d_goods_stock"].";<BR>";
print $_SESSION["HSMAP"]["sql_d_reception_invoiceno"]."<BR>";
?>
<BR>
<?PHP
if 	($_SESSION["HSMAP"]["hikiate_chk"]==0)
{
?>
<!--
<FORM action="hikiatesubsinko.php"><INPUT TYPE="submit" VALUE="引当実施" size="20"></FORM>
-->
<?PHP
}else
{
print "システム部に連絡してください";
}
?>
<FORM action="searchhikiatesinko.php"><INPUT TYPE="submit" VALUE="戻る" size="20"></FORM>
</body>
</html>