set dt=%date:~-10%
set yy=%dt:~0,4%
set mm=%dt:~5,2%
set dd=%dt:~8,2%
del *.csv
sqlplus -s c21db/C_372902@ecdb @rdlist.txt %yy% %mm% %dd%
rename *.lst *.csv
php rdlist.php >> rdlist_batch.log