<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../../parts/login_souko.php");
//$nowtime= date("H");
$nowtime= 23;
$calcdate=-1;
$ScaleMinutes=5;
$BuffMinutesMax = floor(60/$ScaleMinutes);
$dlfile_name = "./pickshuukei_staff_" .date('Ymd', strtotime($calcdate.' day')) . ".csv";
//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres."\r\n";
	fputs($fp,$str);
	fclose( $fp );
}

$sql = "SELECT R.OPERATESTAFFCODE,F.STAFFNAME,R.PICKTIME,R.PICKMINUTE,COUNT(*),R.ZONECODE ".
"FROM (SELECT D.OPERATESTAFFCODE,SUBSTR(D.OPERATETIME,1,2) AS PICKTIME, ".
"SUBSTR(D.OPERATETIME,3,2) AS PICKMINUTE,M.ZONECODE ".
"FROM D_RECEPTION_GOODS D,M_LOCATION M ".
"WHERE D.PICKED_KIND='1' AND D.CANCEL_KIND='0' AND D.LOCATIONCODE=M.LOCATIONCODE ".
"AND D.OPERATEDATE=(SELECT TO_CHAR(SYSDATE+$calcdate,'YYYYMMDD') FROM DUAL) ".
") R,M_STAFF F WHERE R.OPERATESTAFFCODE=F.STAFFCODE ".
"GROUP BY R.PICKTIME,R.PICKMINUTE,R.ZONECODE,R.OPERATESTAFFCODE,F.STAFFNAME ".
"ORDER BY R.OPERATESTAFFCODE,F.STAFFNAME,R.PICKTIME,R.PICKMINUTE,R.ZONECODE";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print outputcsv("本日のピッキング作業状況（スタッフ毎）".date('Y/m/d', strtotime($calcdate.' day')),$dlfile_name);
$print_temp=",,,";

//項目名の表示
for ($c = 9; $c <= $nowtime; $c++) {
	$print_temp=$print_temp.$c."時,,,,,,,,,,,,";
}
outputcsv($print_temp,$dlfile_name);
$print_temp="スタッフコード,スタッフ名,ゾーン,";
for ($c = 9; $c <= $nowtime; $c++) {
	$print_temp=$print_temp. "'00-04,'05-09,'10-14,'15-19,'20-24,'25-29,'30-34,'35-39,'40-44,'45-49,'50-54,'55-59,";
}
outputcsv($print_temp,$dlfile_name);

$i=0;
$motostaffcode="0";
//取得データのセット
while($row = $res->fetchRow()){
	if ($motostaffcode==($row[0]+0))
	{
	}else
	{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$staffzonecnt_a[$i]=0;
		$staffzonecnt_b[$i]=0;
		$staffzonecnt_c[$i]=0;
		$staffzonecnt_d[$i]=0;
		$staffzonecnt_e[$i]=0;
		$staffzonecnt_g[$i]=0;
		$staffzonetotal_a[$i]=0;
		$staffzonetotal_b[$i]=0;
		$staffzonetotal_c[$i]=0;
		$staffzonetotal_d[$i]=0;
		$staffzonetotal_e[$i]=0;
		$staffzonetotal_g[$i]=0;
//集計値初期化
		for($cc = 1; $cc <= 24; $cc++) {
			for($min = 0; $min <$BuffMinutesMax; $min++){
				$forhtml_a[($i)][($cc)][$min]=0;
				$forhtml_b[($i)][($cc)][$min]=0;
				$forhtml_c[($i)][($cc)][$min]=0;
				$forhtml_d[($i)][($cc)][$min]=0;
				$forhtml_e[($i)][($cc)][$min]=0;
				$forhtml_g[($i)][($cc)][$min]=0;
			}
		}
	}

//数値に変換するために+0してます
	switch ($row[5]){
	case "A";
//staffcode,時間,分
		$forhtml_a[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]=$forhtml_a[$i][($row[2]+0)][floor($row[3]/$ScaleMinutes)]+$row[4];
		$staffzonetotal_a[$i]=$staffzonetotal_a[$i]+$row[4];
		$staffzonecnt_a[$i]=1;
		break;
	case "B";
		$forhtml_b[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_b[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_b[$i]=$staffzonetotal_b[$i]+$row[4];
		$staffzonecnt_b[$i]=1;
		break;
	case "C";
		$forhtml_c[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_c[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_c[$i]=$staffzonetotal_c[$i]+$row[4];
		$staffzonecnt_c[$i]=1;
		break;
	case "D";
		$forhtml_d[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_d[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_d[$i]=$staffzonetotal_d[$i]+$row[4];
		$staffzonecnt_d[$i]=1;
		break;
	case "E";
		$forhtml_e[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_e[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_e[$i]=$staffzonetotal_e[$i]+$row[4];
		$staffzonecnt_e[$i]=1;
		break;
	case "G";
		$forhtml_g[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml_g[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
		$staffzonetotal_g[$i]=$staffzonetotal_g[$i]+$row[4];
		$staffzonecnt_g[$i]=1;
		break;
	}
}


//HTML表示
$print_temp="";
for($ii = 1; $ii <= $i; $ii++) {
//Azone
	if ($staffzonetotal_a[$ii]>0)
	{
		$print_temp=$staffcode[$ii].",".$staffname[$ii].",A,";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				$print_temp=$print_temp.$forhtml_a[$ii][$c][$min].",";
			}
		}
		outputcsv($print_temp,$dlfile_name);
	}
//Bzone
	if ($staffzonetotal_b[$ii]>0)
	{
		$print_temp=$staffcode[$ii].",".$staffname[$ii].",B,";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				$print_temp=$print_temp.$forhtml_b[$ii][$c][$min].",";
			}
		}
		outputcsv($print_temp,$dlfile_name);
	}
//Czone
	if ($staffzonetotal_c[$ii]>0)
	{
		$print_temp=$staffcode[$ii].",".$staffname[$ii].",C,";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				$print_temp=$print_temp.$forhtml_c[$ii][$c][$min].",";
			}
		}
		outputcsv($print_temp,$dlfile_name);
	}
//Dzone
	if ($staffzonetotal_d[$ii]>0)
	{
		$print_temp=$staffcode[$ii].",".$staffname[$ii].",D,";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				$print_temp=$print_temp.$forhtml_d[$ii][$c][$min].",";
			}
		}
		outputcsv($print_temp,$dlfile_name);
	}
//Ezone
	if ($staffzonetotal_e[$ii]>0)
	{
		$print_temp=$staffcode[$ii].",".$staffname[$ii].",E,";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				$print_temp=$print_temp.$forhtml_e[$ii][$c][$min].",";
			}
		}
		outputcsv($print_temp,$dlfile_name);
	}
//Gzone
	if ($staffzonetotal_g[$ii]>0)
	{
		$print_temp=$staffcode[$ii].",".$staffname[$ii].",G,";
		for ($c = 9; $c <= $nowtime; $c++) {
			for($min = 0; $min < $BuffMinutesMax; $min++){
				$print_temp=$print_temp.$forhtml_g[$ii][$c][$min].",";
			}
		}
		outputcsv($print_temp,$dlfile_name);
	}
}

//データの開放
$res->free();
$db->disconnect();

unset($row, $motostaffcode,$staffcode,$staffname);
unset($staffzonecnt_a, $staffzonecnt_b, $staffzonecnt_c, $staffzonecnt_d, $staffzonecnt_e, $staffzonecnt_g);
unset($staffzonetotal_a, $staffzonetotal_b, $staffzonetotal_c, $staffzonetotal_d, $staffzonetotal_e, $staffzonetotal_g);
unset($forhtml_a, $forhtml_b, $forhtml_c, $forhtml_d, $forhtml_e, $forhtml_g);

?>
