set linesize 1000
set pagesize 0
set trimspool on
set feedback off
set heading off
set verify off
spool yesterday_order_zaikonasi
select to_char(sysdate,'YYYY/MM/DD ')
|| to_char(sysdate,'HH24') || ':'
|| to_char(sysdate,'MI') || ':'
|| to_char(sysdate,'SS') from dual;
select distinct
g.instorecode||','||
substr(g.GENRE3CODE,1,4)||','||
substr(g.GENRE3CODE,1,6)||','||
substr(g.GENRE3CODE,1,8)||','||
g.GOODS_NAME1||g.GOODS_NAME2||','||
g.MKT_PRICE||','||
g.SALE_PR_USED||','||
jan
from tgoods_bko g,torderdtl d,tstock_bko s
where g.instorecode=d.instorecode
and g.instorecode=s.instorecode
and substr(ord_no,1,8)=(SELECT TO_CHAR(SYSDATE-1,'YYYYMMDD') FROM DUAL)
and (GOODS_CNT-ORD_CNCL_CNT)>0
and d.stock_tp='1'
and stock_used='0';
/*
select to_char(sysdate,'YYYY/MM/DD ')
|| to_char(sysdate,'HH24') || ':'
|| to_char(sysdate,'MI') || ':'
|| to_char(sysdate,'SS') from dual;
select  distinct
g.instorecode||','||
substr(g.GENRE3CODE,1,4)||','||
substr(g.GENRE3CODE,1,6)||','||
substr(g.GENRE3CODE,1,8)||','||
g.GOODS_NAME1||g.GOODS_NAME2||','||
g.MKT_PRICE||','||
g.SALE_PR_USED||','||
jan
from
(
select distinct instorecode
from torderdtl
where
substr(ord_no,1,8)=(SELECT TO_CHAR(SYSDATE-1,'YYYYMMDD') FROM DUAL)
and (GOODS_CNT-ORD_CNCL_CNT)>0
and stock_tp='1'
) d,tgoods_bko g,tstock_bko  s
where g.instorecode=d.instorecode
and g.instorecode=s.instorecode
and stock_used='0';
select to_char(sysdate,'YYYY/MM/DD ')
|| to_char(sysdate,'HH24') || ':'
|| to_char(sysdate,'MI') || ':'
|| to_char(sysdate,'SS') from dual;
*/
exit;