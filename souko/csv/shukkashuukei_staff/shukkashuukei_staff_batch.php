<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../../parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../../parts/login_souko.php");
//$nowtime= date("H");
$nowtime= 23;
$calcdate=-1;
$ScaleMinutes=5;
$dlfile_name = "./shukkashuukei_staff_" .date('Ymd', strtotime($calcdate.' day')) . ".csv";
//ファイル出力関数
function outputcsv($tempres,$tempfilename){
$fp = fopen( $tempfilename, "a" );
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
	$str=$tempres."\r\n";
	fputs($fp,$str);
	fclose( $fp );
}

$sql = "SELECT  P.operatestaffcode, F.staffname, P.deliverytime, P.deliveryminute, count(distinct P.receptioncode) ".
"FROM (SELECT r.receptioncode, d.operatestaffcode, r.deliverydatetime, substr(r.deliverydatetime,9,2) AS deliverytime, ".
"substr(r.deliverydatetime,11,2) AS deliveryminute ".
"FROM d_reception_shipment r,d_reception d ".
"WHERE r.receptioncode=d.receptioncode ".
"AND substr(r.deliverydatetime,1,8)=(select to_char(sysdate+$calcdate,'yyyymmdd') from dual)) P, m_staff F ".
"WHERE P.operatestaffcode=F.staffcode ".
"GROUP BY P.deliverytime,P.deliveryminute,P.operatestaffcode,F.staffname ".
"ORDER BY P.operatestaffcode,F.staffname,P.deliverytime,P.deliveryminute";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print outputcsv("本日の出荷作業状況（スタッフ毎）".date('Y/m/d', strtotime($calcdate.' day')),$dlfile_name);
$print_temp=",,,";

//項目名の表示
for ($c = 9; $c <= $nowtime; $c++) {
	$print_temp=$print_temp.$c."時,,,,,,,,,,,,";
}
outputcsv($print_temp,$dlfile_name);
$print_temp="スタッフコード,スタッフ名,合計,";
for ($c = 9; $c <= $nowtime; $c++) {
//	$print_temp=$print_temp. "00-30,31-60,";
	$print_temp=$print_temp. "'00-04,'05-09,'10-14,'15-19,'20-24,'25-29,'30-34,'35-39,'40-44,'45-49,'50-54,'55-59,";
}
outputcsv($print_temp,$dlfile_name);

$total_count=0;
$i=0;
$motostaffcode="0";
for($cc = 1; $cc <= 24; $cc++) {

	for($min = 0; $min <floor(60/$ScaleMinutes); $min++){
		$jikantotal[$cc][$min]=0;
	}
}
//取得データのセット
while($row = $res->fetchRow()){
	$total_count=$total_count+$row[4];
	if ($motostaffcode==($row[0]+0))
	{
	}else
	{
		$motostaffcode=$row[0];
		$i++;
		$staffcode[$i]=$row[0];
		$staffname[$i]=$row[1];
		$stafftotal[$i]=0;
		for($cc = 1; $cc <= 24; $cc++) {
			for($min = 0; $min <floor(60/$ScaleMinutes); $min++){
				$forhtml[($i)][($cc)][$min]=0;
			}
		}
		
	}
	$forhtml[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]=$forhtml[$i][($row[2]+0)][(floor($row[3]/$ScaleMinutes))]+$row[4];
	$stafftotal[$i]=$stafftotal[$i]+$row[4];
	$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes)+0)]=$jikantotal[($row[2]+0)][(floor($row[3]/$ScaleMinutes)+0)]+$row[4];
}


//HTML表示
$print_temp="";
for($ii = 1; $ii <= $i; $ii++) {
	$print_temp= $staffcode[$ii].",".$staffname[$ii].",".$stafftotal[$ii].",";
	for ($c = 9; $c <= $nowtime; $c++) {
		for($min = 0; $min < floor(60/$ScaleMinutes); $min++){
			$print_temp=$print_temp.$forhtml[$ii][$c][$min].",";
		}
	}
	outputcsv($print_temp,$dlfile_name);
}

$print_temp= ",出荷件数合計,".$total_count.",";
for ($c = 9; $c <= $nowtime; $c++) {
	for($min = 0; $min < floor(60/$ScaleMinutes); $min++){
		$print_temp= $print_temp.$jikantotal[$c][$min].",";
	}
}
outputcsv($print_temp,$dlfile_name);

//データの開放
$res->free();
$db->disconnect();
?>
