set linesize 1000
set pagesize 0
set trimspool on
set feedback off
set heading off
set verify off
spool all_zaiko2
SELECT 'ファイル出力日時　'||(SELECT TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS') FROM DUAL) FROM DUAL;
SELECT 'インストア,ジャンル１,ジャンル２,ジャンル３,タイトル,巻次数,中古在庫数,販売価格,JAN,入庫日,アーティスト,アーティストカナ,部門,商品ランク,中古在庫原価' FROM DUAL;
SELECT S.INSTORECODE||','||
       G.GENRE1CODE||','||
       G.GENRE2CODE||','||
       G.GENRE3CODE||','||
       REPLACE(REPLACE(REPLACE(D.GOODSNAME,CHR(13),' '),CHR(10),' '),',',' ')||','||
       REPLACE(REPLACE(REPLACE(D.GOODSNAME_EXT1,CHR(13),' '),CHR(10),' '),',',' ')||','||
       COUNT(S.INSTORECODE)||','||
       P.SPRICE||','||
       D.JANCODE||','||
       MIN(STOCKDATE)||','||
       REPLACE(REPLACE(REPLACE(D.AUTHORNAME,CHR(13),' '),CHR(10),' '),',',' ')||','||
       REPLACE(REPLACE(REPLACE(D.AUTHORNAMEKANA,CHR(13),' '),CHR(10),' '),',',' ')||','||
       G.BUMONCATCODE||','||
       G.GOODSRANK||','||
       C.COSTPRICE
FROM M_GOODS G,
     D_STOCK S,
     M_GOODS_DISPLAY D,
     M_GOODS_PRICE P,
     D_GOODS_COST C
WHERE G.INSTORECODE=S.INSTORECODE
  AND G.INSTORECODE=D.INSTORECODE
  AND G.INSTORECODE=P.INSTORECODE
  AND G.INSTORECODE=C.INSTORECODE
  AND S.NEWUSED_KIND=C.NEWUSED_KIND
  AND SUBSTR(S.LOCATIONCODE,1,1)<'9'
  AND S.LOCATIONCODE!='0001001001'
  AND S.NEWUSED_KIND='0'
  AND S.STOCKRESERVE_KIND='0'
GROUP BY S.INSTORECODE,
         G.GENRE1CODE,
         G.GENRE2CODE,
         G.GENRE3CODE,
         D.GOODSNAME,
         P.SPRICE,
         D.JANCODE,
         D.AUTHORNAME,
         G.BUMONCATCODE,
         G.GOODSRANK,
         D.GOODSNAME_EXT1,
         D.GOODSNAME_EXT2,
         D.AUTHORNAMEKANA,
         C.COSTPRICE
HAVING COUNT(S.INSTORECODE)>0
;
exit;