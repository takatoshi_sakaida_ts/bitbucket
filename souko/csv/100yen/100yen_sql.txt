set linesize 1000
set pagesize 0
set trimspool on
set feedback off
set heading off
set verify off
spool 100yen
select '書籍' from dual;
select g.instorecode||','||
g.genre3code||','||
g.GOODS_NAME1||','||
g.AUTHORNAME
from tgoods_bko g,tstock_bko s
where
g.instorecode=s.instorecode
and substr(genre3code,1,2)='12'
and SALE_PR_USED<99
and STOCK_USED='0';

select 'コミック' from dual;
select g.instorecode||','||
g.genre3code||','||
g.GOODS_NAME1||','||
g.AUTHORNAME
from tgoods_bko g,tstock_bko s
where
g.instorecode=s.instorecode
and substr(genre3code,1,2)='11'
and SALE_PR_USED<99
and STOCK_USED='0';

select 'CD' from dual;
select g.instorecode||','||
g.genre3code||','||
g.GOODS_NAME1||','||
g.AUTHORNAME
from tgoods_bko g,tstock_bko s
where
g.instorecode=s.instorecode
and substr(genre3code,1,2)='31'
and SALE_PR_USED<104
and STOCK_USED='0';

select 'DVD' from dual;
select g.instorecode||','||
g.genre3code||','||
g.GOODS_NAME1||','||
g.AUTHORNAME
from tgoods_bko g,tstock_bko s
where
g.instorecode=s.instorecode
and substr(genre3code,1,2)='71'
and SALE_PR_USED<104
and STOCK_USED='0';

select 'GAME' from dual;
select g.instorecode||','||
g.genre3code||','||
g.GOODS_NAME1||','||
g.AUTHORNAME
from tgoods_bko g,tstock_bko s
where
g.instorecode=s.instorecode
and substr(genre3code,1,2)='51'
and SALE_PR_USED<104
and STOCK_USED='0';

exit;