set linesize 1000
set pagesize 0
set trimspool on
set feedback off
set heading off
set verify off
spool shukkazan
select '注文数集計' from dual;
select '0:中古のみ'||'1:新古または中古'||'2:新品を含む' from dual;
select kind||','||count(*) from
(
select d.receptioncode,max(newused_kind) as kind,status_kind
from d_reception d,d_reception_goods g
where d.receptioncode=g.receptioncode
and status_kind in ('00','01')
and substr(d.receptioncode,1,8)<(select to_char(to_date(&1&2&3,'YYYYMMDD'),'yyyymmdd') from dual)
and g.CANCEL_KIND='0'
group by d.receptioncode,status_kind
)
group by kind
order by kind;
select 'アイテム数集計' from dual;
select '0:中古'||'1:新古'||'2:新品' from dual;
select g.NEWUSED_KIND||','||count(g.receptioncode||g.detailno)
from d_reception d,d_reception_goods g
where d.receptioncode=g.receptioncode
and substr(d.receptioncode,1,8)<(select to_char(to_date(&1&2&3,'YYYYMMDD'),'yyyymmdd') from dual)
and d.status_kind in ('00','01')
and g.CANCEL_KIND='0'
group by g.NEWUSED_KIND
order by g.NEWUSED_KIND;
exit;