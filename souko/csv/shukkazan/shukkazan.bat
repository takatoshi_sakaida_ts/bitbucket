set dt=%date:~-10%
set yy=%dt:~0,4%
set mm=%dt:~5,2%
set dd=%dt:~8,2%
ren shukkazan.csv shukkazan_%yy%%mm%%dd%.csv
del shukkazan.csv
sqlplus -s bol/bol@bol @shukkazan_sql.txt %yy% %mm% %dd%
rename *.lst *.csv