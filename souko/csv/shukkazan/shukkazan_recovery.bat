rem set dt=%date:~-10%
rem set yy=%dt:~0,4%
rem set mm=%dt:~5,2%
rem set dd=%dt:~8,2%

set yy=2015
set mm=10
set dd=15

ren shukkazan.csv shukkazan_%yy%%mm%%dd%.csv
del shukkazan.csv
sqlplus -s bol/bol@bol @shukkazan_sql_recovery.txt %yy% %mm% %dd%
rename *.lst *.csv