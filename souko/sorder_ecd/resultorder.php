<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<?PHP
require_once("../parts/pagechk.inc");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>注文検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_souko.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
$keyti2 = addslashes(@$_POST["TI2"]);
$keyti3 = addslashes(@$_POST["TI3"]);
$keyti4 = addslashes(@$_POST["TI4"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti) && empty($keyti2) && empty($keyti3)) {
	print "検索条件を入力してください<br>";
}
// 日販DSヤマトURL生成
$yamato = "http://jizen.kuronekoyamato.co.jp/jizen/servlet/crjz.b.NQ0010?id=";

// 設定ファイル読込
$ini = parse_ini_file("../parts/db.ini",true);
// ECDBコネクション取得
$dsn_ec = "oci8://". $ini['ecd']['usr'] . ":" . $ini['ecd']['pwd'] . "@" . $ini['ecd']['dbn'];
//データベースへの接続
$ecdb = DB::connect($dsn_ec);
if(DB::isError($ecdb)){
    $log->err($ecdb->getDebugInfo());
    die("ECDBの接続に失敗しました。<br>" . $ecdb->getDebugInfo());
}
$ecdb->autoCommit(false);

//ログイン情報の読み込み
require_once("../parts/login_souko.php");
//SQL文のCOUNT関数を使用
$sql = "select d.RECEPTIONDATE,d.STATUS_KIND,d.CUSTOMERNAME1,d.COD_PRICE,d.SHIPPINGPRICE,d.EXPENCE_KIND,d.RECEPTION_AMOUNT,";
$sql = $sql . "d.RECEPTION_C_TAX,s.TRANSPORT_KIND,s.DELIVERYCODE,s.DELIVERYWEIGHT,s.DELIVERYDATETIME,";
$sql = $sql . "d.SHIPPINGNAME1,d.SHIPPINGNAME2,d.SHIPPINGPOSTCODE,";
$sql = $sql . "d.SHIPPINGADDRESS1,d.SHIPPINGADDRESS2,d.SHIPPINGADDRESS3,d.SHIPPINGADDRESS4,d.SHIPPINGADDRESS5,d.SHIPPINGTELNO,d.receptioncode,d.operatestaffcode,m.staffname,d.customer_email ";
$sql = $sql . "from d_reception d,D_RECEPTION_SHIPMENT s,m_staff m";
if (empty($keyti3)) {}
else{
	$sql = $sql . ",d_reception_goods g";
}
$sql = $sql . " where d.RECEPTIONCODE=s.RECEPTIONCODE(+) and d.operatestaffcode=m.staffcode(+) ";
if (empty($keyti)) {}else{
	$sql = $sql . " and d.receptioncode ='".$keyti."'";
}
if (empty($keyti2)) {}else{
	$sql = $sql . " and (d.CUSTOMERNAME1 like '%".$keyti2."%'";
	$sql = $sql . " or d.CUSTOMERNAME2 like '%".$keyti2."%'";
	$sql = $sql . " or d.CUSTOMERNAMEKANA1 like '%".$keyti2."%'";
	$sql = $sql . " or d.CUSTOMERNAMEKANA2 like '%".$keyti2."%')";
}
if (empty($keyti4)) {}else{
	$sql = $sql . " and s.DELIVERYCODE='".$keyti4."'";
}
if (empty($keyti3)) {}else{
	$sql = $sql . " and d.receptioncode=g.receptioncode and g.instorecode='".$keyti3."'";
}
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

require_once("../log/loger.inc");
output_log($sql,$_SESSION["user_id"],'searchorder');
//検索結果の表示
print "<strong><BR>検索結果". $keyti."</strong> <br><br>\n<HR>";
$j=1;

//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	// 日販データを取得
	$selectSql = "";
	$selectSql .= "select dtl.ORD_NO_EX, dtl.ORD_SEQ_EX, dtl.L_STATUS, dtl.SHIP_DM, dtl.SHIP_CODE, dtl.INSTORECODE, dtl.GOODS_NO";
	$selectSql .= "  from TORDER_DS_GOODS_DTL dtl";
	$selectSql .= " where dtl.ORD_NO = ?";
	$selectSql .= " order by dtl.ORD_NO_EX, dtl.ORD_SEQ_EX";

	$orderres = $ecdb->query($selectSql, $row[21]);
	if(DB::isError($orderres)){
	    die($res->getDebugInfo());
	}

	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	//項目名の表示
	print "<td nowrap width=40>項番</td><td nowrap width=120>注文番号</td><td nowrap width=120>受付日</td><td nowrap width=200>状態</td><td nowrap width=120>注文者名</td>";
	print "<td nowrap width=80>決済方法</td><td nowrap width=80>代引金額</td><td nowrap width=80>送料</td>";
	print "<td nowrap>注文金額(税込)</td></tr>";
	print "<tr>";
	//項番
	print "<td>".$j ."</td>";
	//注文番号
	print "<td><a href='detailorder.php?receptioncode=" .$row[21] . "'>".$row[21] ."</td>";
	//	print "<td>" .$row[21] . "<input type=hidden value=" . $row[21] . ">" ."</td>";
	//受付日
	print "<td>".Rethiduke($row[0]) ."</td>";
	//状態
	print "<td>".Retstatuskind($row[1])."</td>";
	//顧客名
	print "<td>".$row[2] ."</td>";
	//決済方法
	print "<td>".Retkessai($row[5]) ."</td>";
	//代引金額
	print "<td align=right>".$row[3] ."</td>";
	//送料
	print "<td align=right>".$row[4] ."</td>";
	//注文金額合計(税込)
	print "<td align=right>".$row[6] ."</td>";
	print "</tr></table>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	//項目名の表示
	print "<td nowrap width=120>運送会社</td><td nowrap width=200>出荷コード</td><td nowrap width=120>出荷登録日時</td><td nowrap width=120>出荷担当者コード</td><td nowrap width=100>出荷担当者名</td></tr>";
	//運送会社
	print "<tr><td>".Retunsoukaisha($row[8])."</td>";
	//出荷コード
	//	print "<td>".Rethaisoubangou($row[9]) ."</td>";
	print "<td>".$row[9] ."</td>";
	//出荷日時
	print "<td>".Rethidukejikan($row[11]) ."</td>";
	//出荷担当者
	print "<td>".$row[22] ."</td>";
	print "<td>".$row[23] ."</td>";
	print "</TR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	//項目名の表示
	print "<td nowrap width=120>送付先名</td><td nowrap width=500>送付先住所</td><td nowrap width=120>送付先連絡先</td><td nowrap width=120>MEM_ID</td></tr>";
	print "<tr><td>".$row[12].$row[13]."</td>";
	print "<td>".Retzip($row[14])."<BR>".$row[15].$row[16].$row[17].$row[18].$row[19]."</td>";
	print "<td>".Rettel($row[20])."</td><td>".$row[24]."</td>";
	print "</TR>";
	print "</table>";
//	print"<HR>";
	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";

	//日販の表示
	while($order = $orderres->fetchRow()){
		print "<td nowrap width=100>日販注文番号</td><td nowrap width=100>日販注文明細番号</td><td nowrap width=110>インストアコード</td><td nowrap width=110>日販商品コード</td><td nowrap width=200>日販注文ステータス</td><td nowrap width=100>出荷日時</td><td nowrap width=730>宅配荷物番号(PC版＝http://jizen.kuronekoyamato.co.jp/jizen/servlet/crjz.b.NQ0010?id=【宅配荷物番号】、MB版＝http://m.9625.jp)</td></tr>";
		print "<TR>";
		print "<td>".$order[0]."</td>";
		print "<td>".$order[1]."</td>";
		print "<td>".$order[5]."</td>";
		print "<td>".$order[6]."</td>";
		if ($order[2] == '1') {
			$order[2] = $order[2].'（注文準備中）';
		} elseif ($order[2] == '2') {
			$order[2] = $order[2].'（調達中）';
		} elseif ($order[2] == '5') {
			$order[2] = $order[2].'（品切れ）';
		} elseif ($order[2] == '8') {
			$order[2] = $order[2].'（配送中※出荷完了）';
		} elseif ($order[2] == 'X') {
			$order[2] = $order[2].'（キャンセル)';
		} elseif ($order[2] == 'Y') {
			$order[2] = $order[2].'（返品)';
		} elseif ($order[2] == 'Z') {
			$order[2] = $order[2].'（事故)';
		}
		print "<td>".$order[2]."</td>";
		print "<td>".$order[3]."</td>";
		print '<td><a href="'.$yamato.$order[4].'" target="_blank">'.$order[4].'</a></td>';
	}
	print "</TR>";
	print "</table>";
	print"<HR>";
	$j++;
}

//データの開放
$res->free();
$db->disconnect();

//$orderres->free();
$ecdb->disconnect();


?>
<BR>
<FORM action="searchorder.php"><INPUT TYPE="submit" VALUE="戻る"></FORM>
</body>
</html>