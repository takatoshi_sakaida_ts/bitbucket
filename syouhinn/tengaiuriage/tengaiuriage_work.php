<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>店外売上作業状況</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../../souko/parts/selectvalue_souko.php");
//ログイン情報の読み込み
require_once("../../souko/parts/login_souko.php");
//店外売上の未作業データを取得
$sql =
"select s.status_kind, s.stockworkcode, s.remark ".
"from D_STOCKWORK s ".
"where s.status_kind　in ('00', '01', '02') ".
"and s.stockwork_kind = '09' ".
"order by s.stockworkcode";
$res1 = $db->query($sql);
if(DB::isError($res1)){
	$res1->DB_Error($res1->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$sql =
"select s.stockworkcode, sg.status_kind, count(*) from D_STOCKWORK_GOODS sg, D_STOCKWORK s ".
"where sg.stockworkcode = s.stockworkcode ".
"and s.status_kind　in ('00', '01', '02') ".
"and s.stockwork_kind = '09' ".
"group by s.stockworkcode, sg.status_kind ".
"order by s.stockworkcode";
$res2 = $db->query($sql);
if(DB::isError($res2)){
	$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$sql =
"select s.stockworkcode, min(sg.locationcode), max(sg.locationcode) from D_STOCKWORK_GOODS sg, D_STOCKWORK s ".
"where sg.stockworkcode = s.stockworkcode ".
"and s.status_kind　in ('00', '01', '02') ".
"and s.stockwork_kind = '09' ".
"group by s.stockworkcode ".
"order by s.stockworkcode";
$res3 = $db->query($sql);
if(DB::isError($res3)){
	$res3->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$sql =
"select s.stockworkcode, sg.LOCATIONCODE, l.ZONECODE from D_STOCKWORK_GOODS sg, D_STOCKWORK s, m_location l ".
"where sg.stockworkcode = s.stockworkcode ".
"and sg.LOCATIONCODE = l.LOCATIONCODE ".
"and s.status_kind　in ('00', '01', '02') ".
"and s.stockwork_kind = '09' ".
"order by l.locationcode";
$res4 = $db->query($sql);
if(DB::isError($res3)){
	$res4->DB_Error($res3->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>店外売上作業データ一覧</strong><br>".date('Y/m/d H:i')."時点";
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=140 align=center>作業ステータス</td>
	<td width=120 align=center>作業番号</td>
	<td width=100 align=center>作業商品数</td>
	<td width=100 align=center>作業完了商品数</td>
	<td width=100 align=center>最少ロケーション</td>
	<td width=100 align=center>最大ロケーション</td>
	<td width=100 align=center>棚移動備考</td>
	<td width=50 align=center>2F</td>
	<td width=50 align=center>3F</td>
	<td width=50 align=center>4F</td>
	<td width=100 align=center>4F(移動棚)</td>
</tr>
<?php

$itemCount = 7;
//検索結果の表示
//取得データのセット
$i = 0;
while($row = $res1->fetchRow()){
	$data[$i][0] = $row[0];
	$data[$i][1] = $row[1];
	$data[$i][2] = 0;
	$data[$i][3] = 0;
	$data[$i][4] = 0;
	$data[$i][5] = 0;
	$data[$i][6] = $row[2];
	$data[$i][7] = 0;
	$data[$i][8] = 0;
	$data[$i][9] = 0;
	$data[$i][10] = 0;
	$i = $i + 1;
}
while($row = $res2->fetchRow()){
	for($ii = 0; $ii < $i; $ii++ ) {
		if($data[$ii][1] == $row[0]) {
			$data[$ii][2] = $data[$ii][2] + $row[2];
			if($data[$ii][1] == "01") {
				$data[$ii][3] = $row[2];
			}
			break;
		}
	}
}
while($row = $res3->fetchRow()){
	for($ii = 0; $ii < $i; $ii++ ) {
		if($data[$ii][1] == $row[0]) {
			$data[$ii][4] = $row[1];
			$data[$ii][5] = $row[2];
			break;
		}
	}
}

while($row = $res4->fetchRow()){
	for($ii = 0; $ii < $i; $ii++) {
		if($data[$ii][1] == $row[0]) {
			if($row[2] == "A" || $row[2] == "B" || $row[2] == "C" || $row[2] == "D" || $row[2] == "G") {
				if($row[1] >= 7001000000 && $row[1] <= 7991999999) {
					$data[$ii][10] = $data[$ii][10] + 1;
				} else {
					$data[$ii][9] = $data[$ii][9] + 1;
				}
			} else if($row[2] == "H" || $row[2] == "I" || $row[2] == "J" || $row[2] == "K" || $row[2] == "L") {
				$data[$ii][7] = $data[$ii][7] + 1;
			} else if($row[2] == "M" || $row[2] == "N" || $row[2] == "O" || $row[2] == "P" || $row[2] == "Q") {
				$data[$ii][8] = $data[$ii][8] + 1;
			}
		}
	}
}

//HTML表示
for ($ii = 0; $ii < $i; $ii++ )
{
	print "<td align=center>". StockworkStatus($data[$ii][0]) ."</td>";
	print "<td align=right>" . $data[$ii][1] ."</td>";
	print "<td align=right>" . number_format($data[$ii][2]) ."</td>";
	print "<td align=right>" . number_format($data[$ii][3]) ."</td>";
	print "<td align=right>" . $data[$ii][4] ."</td>";
	print "<td align=right>" . $data[$ii][5] ."</td>";
	print "<td align=left>"  . $data[$ii][6] ."</td>";
	if($data[$ii][7] == 0) {
		print "<td align=right bgcolor=#CCCCCC>" . $data[$ii][7] ."</td>";
	} else {
		print "<td align=right>" . $data[$ii][7] ."</td>";
	}
	if($data[$ii][8] == 0) {
		print "<td align=right bgcolor=#CCCCCC>" . $data[$ii][8] ."</td>";
	} else {
		print "<td align=right>" . $data[$ii][8] ."</td>";
	}
	if($data[$ii][9] == 0) {
		print "<td align=right bgcolor=#CCCCCC>" . $data[$ii][9] ."</td>";
	} else {
		print "<td align=right>" . $data[$ii][9] ."</td>";
	}
	if($data[$ii][10] == 0) {
		print "<td align=right bgcolor=#CCCCCC>" . $data[$ii][10] ."</td>";
	} else {
		print "<td align=right>" . $data[$ii][10] ."</td>";
	}
	print "</td>\n</tr>";
}

//データの開放
$res1->free();
$res2->free();
$res3->free();
$res4->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
