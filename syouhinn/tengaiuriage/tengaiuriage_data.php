<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>店外売上データ</title>
</head>
<body>
<?php
set_time_limit(240);
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../../souko/parts/selectvalue_souko.php");
$sellcode = addslashes(@$_POST["TI"]);
//ログイン情報の読み込み
require_once("../../souko/parts/login_souko.php");
//店外売上の未作業データを取得
$sql =
"SELECT a.BUMONNAME, a.GOODSNAME, a.INSTORECODE, a.GOODSNAME_EXT1, a.GOODSNAME_EXT2, a.SPECCODE, a.UNITPRICE_TAX ".
"FROM   (SELECT b.BUMONNAME, d.GOODSNAME, d.INSTORECODE, d.GOODSNAME_EXT1, LPAD(d.GOODSNAME_EXT1, 10, '0') as narabi, d.GOODSNAME_EXT2, d.SPECCODE, (s.UNITPRICE + s.TAX_PRICE) as UNITPRICE_TAX ".
"        FROM D_B2B_SELL_GOODS s, M_BUMON b, M_GOODS_DISPLAY d ".
"        WHERE B2B_SELLCODE = 'LOGI2016051621' ".
"        AND s.BUMONCODE = b.BUMONCODE ".
"        AND s.INSTORECODE = d.INSTORECODE) a ".
"ORDER BY a.BUMONNAME, a.GOODSNAME, a.narabi";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>店外売上データ</strong><br>". "BtoB売上コード：".$sellcode;
?>
<br><br><hr>
<table border=1>
<tr bgcolor=#ccffff>
	<td width=140 align=center>部門名</td>
	<td width=140 align=center>商品名</td>
	<td width=140 align=center>インストアコード</td>
	<td width=140 align=center>巻次</td>
	<td width=140 align=center>シリーズ情報</td>
	<td width=140 align=center>規格品番コード</td>
	<td width=140 align=center>単価</td>
</tr>
<?php

//検索結果の表示
while($row = $res->fetchRow()){
	print "<td align=center>". $row[0] ."</td>";
	print "<td align=center>". $row[1] ."</td>";
	print "<td align=center>". $row[2] ."</td>";
	print "<td align=center>". $row[3] ."</td>";
	print "<td align=center>". $row[4] ."</td>";
	print "<td align=center>". $row[5] ."</td>";
	print "<td align=center>". $row[6] ."</td>";
	print "</td>\n</tr>";
}

//データの開放
$res->free();
$db->disconnect();
?>
</table>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
