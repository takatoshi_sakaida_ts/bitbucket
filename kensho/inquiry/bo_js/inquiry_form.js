/* HTML Form checker by ARTEMIS[www.artemis.ac] hidemaru<script> */
var FChk = {
	/*設定1 フォームのID */
	id:'myform'
,
	/*設定2 送信ボタンのID (submitと言う名前は使わない事) */
	btn:'sendbtn'

,
	/*設定3 最初にフォーカスをあてるフォームエレメントの name。不要なら '' に */
	first:''

,
	/*設定4 送信先(フォームのactionに設定するURI) */
	action:'./inquiry_conf.php'

,
	/*設定5 エラー表示の色 */
	errcol:'#ffeeee'

,
	/*設定6 本来の表示の色 '' でも良い */
	defcol:''

,
	/*設定7 1箇所(id=errormsg)にエラーを出す場合。最初の1個だけ出す場合は0、全部出す場合は 1 */
	msgtype:1

,
	/*設定8 空チェックする必須項目の登録 HPの解説を読んでください */
	names:{

select:['お問合せの種類',2],
name:['お名前',1],
email:['返信用メールアドレス',1,1,'emailcopy'],
message:['お問合せ内容',1],

dummy:[]}

,
	/*設定6 1項目複数チェックボックスの場合 HPの解説を読んでください */
	box:[



]

,	by:function(id){ if(document.getElementById) return document.getElementById(id); }
,	addEv:function(obj, type, func){ if(obj.addEventListener){ obj.addEventListener(type, func, true); }else{ if(obj.attachEvent){ obj.attachEvent('on' + type, func); }else{ var preOnload = obj['on' + type]; obj['on' + type] = function(e){ if(preOnload){ return preOnload(e); } return func(e || window.event); }; } } }
,	set:function(O){
		if(!document.getElementById){ return false; }
		O.addEv(window,'load',Loadset);
		var oForm, aInput = [],bln = true;
		function Loadset(){ oForm = O.by(O.id); O.addEv(oForm, 'submit',function(e){ if(e.preventDefault){ e.preventDefault();} return false; }); O.addEv(oForm, 'reset', DoReset); O.addEv(O.by(O.btn), 'click', ChkForm); if(O.first){ oForm[O.first].focus(); } }
		function DoReset(){ if(aInput.length >0){ for(var key in aInput){ if(aInput[key] != ''){ ChgCol(true, aInput[key]); } if(O.by(aInput[key] + '_msg')){ O.by(aInput[key] + '_msg').innerHTML = ''; } if(O.by('errormsg')){ O.by('errormsg').innerHTML = ''; } } } }
		function ChkForm(){
			oForm.action = O.action;
bln = true;
			if(O.by('errormsg')){ O.by('errormsg').innerHTML = ''; }
			for(var key in O.names){
				 if(key != '' && oForm[key]){
					switch(O.names[key][1]){
						case 1:	//input,textarea
							ChgCol( ((oForm[key].value == '')? false : true) , key , O.names[key][0] + 'を入力してください' );
							if(oForm[key].value != '' && O.names[key][2]){
								switch(O.names[key][2]){
									case 1:	//Mail
										var mailchk = false;
										if(oForm[key].value.match(/^[0-9a-zA-Z][0-9a-zA-Z\-_\.]+\@[0-9a-zA-Z]+[0-9a-zA-Z\-_\.]*\.[0-9a-zA-Z]{2,4}$/)){ mailchk = true; }
										if(O.names[key][3] && mailchk){ //mail conf
											if(oForm[O.names[key][3]]){ mailchk = (oForm[key].value == oForm[O.names[key][3]].value)? true : false; }
											ChgCol(mailchk , key , O.names[key][0] + 'が確認用と一致しません');
											ChgCol(mailchk,O.names[key][3]);
										}else{
											if(O.names[key][3]) ChgCol(mailchk,O.names[key][3]);
											ChgCol(mailchk , key , O.names[key][0] + 'が正しくありません');
										}
										break;



								}
							}else{	if(O.names[key][2] && O.names[key][2] == 1 && O.names[key][3]) ChgCol(false,O.names[key][3]); }
							break;
						case 2:	//select (sigle,multiple)
							var multi = 0;
							for(var i=0; i <oForm[key].length; i++){ if(oForm[key][i].selected == true && oForm[key][i].value != '') multi++; }
							O.names[key][2] = ( O.names[key][2] || 1 );
							O.names[key][3] = ( O.names[key][3] || 1 );
							if(O.names[key][2] > O.names[key][3]) O.names[key][2] = O.names[key][3];
							O.errmsg = '';
							if( (O.names[key][2] == O.names[key][3]) && O.names[key][2] == 1){
								if(multi < 1) O.errmsg = 'を選択してください';

							}else{
								if(multi < O.names[key][2]) O.errmsg = 'は最低' + O.names[key][2] + 'つ 選択してください。';

							}
							ChgCol( ((O.errmsg)? false : true), key, O.names[key][0] + O.errmsg);
							break;


					}
				}
			}
			if(bln){ oForm.submit(); }
		}
		function ChgCol(_flag,_key,_err){
			if(_flag){
				if(O.by(_key + '_outer')){ O.by(_key + '_outer').style.backgroundColor = O.defcol; }
				if(O.by(_key + '_msg')){ O.by(_key + '_msg').innerHTML = ''; }
			}else{
				if(O.by(_key + '_outer')){ O.by(_key + '_outer').style.backgroundColor = O.errcol; }
				if(O.by('errormsg') && _err){ if(O.msgtype){ O.by('errormsg').innerHTML += _err + '<br>'; }else{ if(bln) O.by('errormsg').innerHTML += _err; } }
				if(O.by(_key + '_msg')){ O.by(_key + '_msg').innerHTML = _err; }
				aInput.push(_key);
				if(bln && oForm[_key] && oForm[_key].focus){ oForm[_key].focus(); }
				bln = false;
			}
		}
	}
};
FChk.set(FChk);
