<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" dir="ltr">

<head>
<title>お問合せ | ブックオフの携帯電話　宅配買取サービス</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta name="description" content="ブックオフの携帯電話　宅配買取サービス">
<meta name="keywords" content="宅配，買取，携帯電話，スマートフォン，買い取り">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="stylesheet" href="bo_css/import.css" type="text/css">
<link rel="stylesheet" href="bo_css/cell_style.css" type="text/css" media="all">
<link rel="stylesheet" href="bo_css/mail_form.css" type="text/css" media="all">
</head>
<?php
function escapevalue($value){
//DB登録に不要な文字の削除
	$before1 = "'";
	$value = str_replace($before1,"",$value);
	$before1 = ",";
	$value = str_replace($before1,"",$value);
	$before1 = '"';
	$value = str_replace($before1,"",$value);
	return $value;
}

mb_language('japanese');
mb_internal_encoding("EUC-JP");
$select=htmlspecialchars(escapevalue($_POST['select']));
$email=htmlspecialchars(escapevalue($_POST['email']));
$name=htmlspecialchars(escapevalue($_POST['name']));
$message=htmlspecialchars(escapevalue($_POST['message']));
$to="rikiya.takahashi@boc.bookoff.co.jp";
//$to="uemura@bookoffonline.jp";
//$to="taira@bookoffonline.jp";
$subject = "お問い合わせがありました";
$message="お問合せの種類: ".$select."\r\n返信用メールアドレス:".$email."\r\nお名前: ".$name."\r\nお問合せ内容: ".$message."\r\n";
//ヘッダエンコード
$mailfrom = 'From: ' . $email . "\r\n"
. 'Reply-To: '. $to . "\r\n"
. 'Return-Path:' . $to;
?>
<body>
<!--2012/02/22 19:20-->
<!--main-->
<div id="main">
	<div class="SellHeader">
		<h1>ブックオフの携帯電話 宅配買取サービス</h1>
		<p id="Selltxt">ブックオフでは自宅にいながら携帯電話・スマートフォンをお売りいただける、宅配買取サービスを行っております<br>送料・手数料0円、個人情報もデータクリーニング処理で安心してご利用いただけます</p>
	</div>
<!--header-->
<div id="header">
	<div class="logo"><a href="http://www.bookoff.co.jp"><img src="bo_img/boc_logo.gif" alt="ブックオフ" /></a></div>
</div>
<!--//header//-->

<!--contents-->
<div id="contents">
	<div id="naviFrame">
		<ol>
			<li><a href="http://www.bookoff.co.jp">トップ</a>＞</li>
			<li><a href="http://www.bookoff.co.jp/cellphone/index.html">携帯電話宅配買取サービス ご利用の流れ</a>＞</li>
			<li>お問合せ</li>
		</ol>
	</div>
	<div id="wrapper">
		<div id="globalNavi_full">
			<ul>
				<li id="navi01"><a href="http://www.bookoff.co.jp/cellphone/index.html">ご利用の流れ</a></li>
				<li id="navi02"><a href="http://www.bookoff.co.jp/cellphone/list.html">買取価格表</a></li>
				<li id="navi03"><a href="http://www.bookoff.co.jp/cellphone/check.html">発送前チェック</a></li>
				<li id="navi04"><a href="http://www.bookoff.co.jp/cellphone/faq.html">よくある質問</a></li>
			</ul>
		</div><!--//globalNavi_full//-->
<?php
if (rtrim($_SERVER["HTTP_REFERER"])=="https://2902.jp/inquiry/inquiry_conf.php")
{	
//メール送信　ロジ
mb_send_mail($to, $subject, $message, $mailfrom);

$subject2="【携帯電話宅配買取サービス】お問合せを受付けました\r\n";
$message2="携帯電話宅配買取サービス　です。\r\n"
."\r\n"
."いつもご利用いただき、誠にありがとうございます。\r\n"
."\r\n"
."お問合せを受付けいたしました。\r\n"
."\r\n"
."お問合せ内容を確認次第、メールにてご連絡いたします。\r\n"
."\r\n"
."\r\n"
."━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\r\n"
."お問合せ（携帯電話宅配買取サービス）\r\n"
."https://2902.jp/inquiry/inquiry.html\r\n"
."\r\n"
."このメールは、送信専用メールアドレスから配信されています。\r\n"
."ご返信いただいてもお答えできませんので、ご了承ください。\r\n"
."\r\n"
."個人情報の取扱いについては個人情報保護方針をご覧下さい。\r\n"
."http://www.bookoff.co.jp/cellphone/policy.html\r\n"
."http://www.bookoff.co.jp/cellphone/index.html\r\n"
."━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\r\n";
$mailfrom2='From: ' . $to . "\r\n";
//メール送信　ユーザ
mb_send_mail($email, $subject2, $message2, $mailfrom2);
?>
		<div id="privacy_main">
			<h2>お問合せ（完了）</h2>
			<div id="m_finish">
				<div class="finish_b_txt"><b>お問合せありがとうございました。</b></div>
				<div class="finish_txt">
					お問合せ内容を送信いたしました。<br>当センターより返答させていただきます。<br>
					今後ともご愛顧賜りますようよろしくお願い申し上げます。
				</div>
				<div class="finish_txt"><a href="http://www.bookoff.co.jp/cellphone/index.html">トップページにもどる</a></div>
			</div>
		</div><!--//privacy_main//-->
	</div><!--//wrapper//-->
</div>
<?PHP
}else{
	print '<font color="red" size="6"><BR><BR>不正な画面遷移です<BR><BR></font>';
}
?>
<!--//contents//-->

<!--footer-->
<div id="footer" class="clr">
	<div class="fotlink">
			<div id="helpBox">
				<div id="ttlHelp">ヘルプ＆ガイド</div>
				<ul id="helpLeft">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/index.html" target="_self">ご利用の流れ</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/list.html#standard" target="_self">買取基準</a></li>
				</ul>
				<ul id="helpRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/list.html#price" target="_self">買取価格表</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/check.html" target="_self">発送前チェック</a></li>
				</ul>
				<ul id="helpRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/faq.html" target="_self">よくある質問</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="https://2902.jp/inquiry/inquiry.html" target="_self">お問合せ</a></li>
				</ul>
			</div>
			<div id="serviceBox">
				<div id="ttlservice">携帯電話 宅配買取サービス ご利用にあたって</div>
				<ul id="serviceLeft">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/terms.html" target="_self">利用規約</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/corp_prof.html" target="_self">会社概要</a></li>
				</ul>
				<ul id="serviceRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/policy.html" target="_self">プライバシーポリシー</a></li>
				</ul>
			</div>
	</div>
	<div id="number">神奈川県公安委員会 第452780002911号　ブックオフロジスティクス株式会社</div>
	<div id="copyright">Copyright(C) BOOKOFF LOGISTICS CORPORATION. All rights Reserved.</div>
</div>
<!--//footer//-->
</div>
<!--//main//-->
<?PHP
unset($select);
unset($email);
unset($name);
unset($message);
unset($to);

?>
</body>
</html>
