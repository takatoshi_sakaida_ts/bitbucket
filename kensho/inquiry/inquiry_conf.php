<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" dir="ltr">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<title>お問合せ | ブックオフの携帯電話　宅配買取サービス</title>
<meta name="description" content="ブックオフの携帯電話　宅配買取サービス">
<meta name="keywords" content="宅配，買取，携帯電話，スマートフォン，買い取り">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="stylesheet" href="bo_css/import.css" type="text/css">
<link rel="stylesheet" href="bo_css/cell_style.css" type="text/css" media="all">
<link rel="stylesheet" href="bo_css/mail_form.css" type="text/css" media="all">
</head>
<?php
function escapevalue($value){
//DB登録に不要な文字の削除
	$before1 = "'";
	$value = str_replace($before1,"",$value);
	$before1 = ",";
	$value = str_replace($before1,"",$value);
	$before1 = '"';
	$value = str_replace($before1,"",$value);
	return $value;
}
$input_select=htmlspecialchars(escapevalue($_POST["select"]));
$input_email=htmlspecialchars(escapevalue($_POST["email"]));
$input_name=htmlspecialchars(escapevalue($_POST["name"]));
$input_message=htmlspecialchars(escapevalue($_POST["message"]));
//print $input_email.":".$input_name.":".$input_message;
?>
<body>
<!--main-->
<div id="main">
	<div class="SellHeader">
		<h1>ブックオフの携帯電話 宅配買取サービス</h1>
		<p id="Selltxt">ブックオフでは自宅にいながら携帯電話・スマートフォンをお売りいただける、宅配買取サービスを行っております<br>送料・手数料0円、個人情報もデータクリーニング処理で安心してご利用いただけます</p>
	</div>
<!--header-->
<div id="header">
	<div class="logo"><a href="http://www.bookoff.co.jp"><img src="bo_img/boc_logo.gif" alt="ブックオフ" /></a></div>
</div>
<!--//header//-->

<!--contents-->
<div id="contents">
	<div id="naviFrame">
		<ol>
			<li><a href="http://www.bookoff.co.jp">トップ</a>＞</li>
			<li><a href="http://www.bookoff.co.jp/cellphone/index.html">携帯電話宅配買取サービス ご利用の流れ</a>＞</li>
			<li>お問合せ</li>
		</ol>
	</div>
	<div id="wrapper">
		<div id="globalNavi_full">
			<ul>
				<li id="navi01"><a href="http://www.bookoff.co.jp/cellphone/index.html">ご利用の流れ</a></li>
				<li id="navi02"><a href="http://www.bookoff.co.jp/cellphone/list.html">買取価格表</a></li>
				<li id="navi03"><a href="http://www.bookoff.co.jp/cellphone/check.html">発送前チェック</a></li>
				<li id="navi04"><a href="http://www.bookoff.co.jp/cellphone/faq.html">よくある質問</a></li>
			</ul>
		</div><!--//globalNavi_full//-->
		<div id="privacy_main">
			<h2>お問合せ（確認）</h2>
			<div class="txt_content">入力内容をご確認ください。</div>
<!--お問合せフォーム-->
			<form action="inquiry_finish.php" method="POST" id="m-form">
			<table>
				<tr>
					<th valign="top">お問合せの種類</th>
					<td>
					<?PHP
					print $input_select;
					?>
					</td>
				</tr>
				<tr>
					<th valign="top">返信用メールアドレス</th>
					<td>
					<?PHP
					print $input_email;
					?>
					</td>
				</tr>
				<tr>
					<th valign="top">お名前</th>
					<td>
					<?PHP
					print $input_name;
					?>
					</td>
				</tr>
				<tr>
					<th valign="top">お問合せ内容</th>
					<td>
					<?PHP
					print $input_message;
					?>
					</td>
				</tr>
			</table>
			<input type="hidden" name="select" value=
			<?PHP
			print '"'.$input_select.'">';
			?>
			<input type="hidden" name="email" value=
			<?PHP
			print '"'.$input_email.'">';
			?>
			<input type="hidden" name="name" value=
			<?PHP
			print '"'.$input_name.'">';
			?>
			<input type="hidden" name="message" value=
			<?PHP
			print '"'.$input_message.'">';
			?>
<?PHP
/**
if (rtrim($_SERVER["HTTP_REFERER"])=="https://2902.jp/inquiry/inquiry.html")
	{	
*/
?>
			<div class="send"><a href="javascript:history.back()"><img src="bo_img/backbtn.gif" alt="入力画面にもどる" border="0"></a>　<input type="image" src="bo_img/sendbtn.gif" alt="送信する" name="button_SendMail" value="送信"/></div>
<?PHP
/**
}else{
	print '<font color="red" size="6"><BR>不正な画面遷移です</font>';
}
*/
?>
			</form>
<!--//お問合せフォーム//-->
		</div><!--//privacy_main//-->
	</div><!--//wrapper//-->
</div>
<!--//contents//-->

<!--footer-->
<div id="footer" class="clr">
	<div class="fotlink">
			<div id="helpBox">
				<div id="ttlHelp">ヘルプ＆ガイド</div>
				<ul id="helpLeft">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/index.html" target="_self">ご利用の流れ</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/list.html#standard" target="_self">買取基準</a></li>
				</ul>
				<ul id="helpRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/list.html#price" target="_self">買取価格表</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/check.html" target="_self">発送前チェック</a></li>
				</ul>
				<ul id="helpRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/faq.html" target="_self">よくある質問</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="https://2902.jp/inquiry/inquiry.html" target="_self">お問合せ</a></li>
				</ul>
			</div>
			<div id="serviceBox">
				<div id="ttlservice">携帯電話 宅配買取サービス ご利用にあたって</div>
				<ul id="serviceLeft">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/terms.html" target="_self">利用規約</a></li>
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/corp_prof.html" target="_self">会社概要</a></li>
				</ul>
				<ul id="serviceRight">
					<li><img width="8" height="10" src="bo_img/arrow.gif" complete="complete"/><a href="http://www.bookoff.co.jp/cellphone/policy.html" target="_self">プライバシーポリシー</a></li>
				</ul>
			</div>
	</div>
	<div id="number">神奈川県公安委員会 第452780002911号　ブックオフロジスティクス株式会社</div>
	<div id="copyright">Copyright(C) BOOKOFF LOGISTICS CORPORATION. All rights Reserved.</div>
</div>
<!--//footer//-->
</div>
<!--//main//-->
</body>
</html>
