<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>商品マスタ連携予定数</title>
</head>
<body>

<?php
//ファイルの読み込み
require_once("DB.php");
//ログイン情報の読み込み
require_once("../../souko/parts/login_souko.php");

$sql =	"SELECT mge.GENRENAME, COUNT(*) ".
		"FROM M_GOODS mgo, M_GENRE mge ".
		"where INSTORECODE IN ".
		"( ".
		"  SELECT INSTORECODE FROM D_GOODS_MODIFY ".
		"  UNION ".
		"  SELECT INSTORECODE FROM D_GOODS_PERIODPRICE ".
		"  WHERE FROMDATE = '".date('Ymd', strtotime("+1 day"))."' ".
		"  OR TODATE = '".date('Ymd')."' ".
		"  UNION ".
		"  SELECT INSTORECODE FROM D_GOODS_PRICE_MODIFY ".
		") ".
		"AND mgo.GENRE0CODE = mge.GENRECODE ".
		"GROUP BY mgo.GENRE0CODE, mge.GENRENAME";

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<strong><BR>商品マスタ連携予定数</strong><br>".date('Y/m/d')."<br><br>\n<HR>";

print "<table border=1>\n";
print "<tr bgcolor=#ccffff><th width=120>商品分類</th><th width=120>件数</th></tr>\n";

$total = 0;
while($row = $res->fetchRow()){
	print "<tr>\n";
	print "<td>".$row[0]."</td>\n";
	if($row[0] == "ＤＶＤ" || $row[0] == "ＣＤ") {
		if($row[1] > 19999) {
			print "<td bgcolor=#FF0000 align='right'>".number_format($row[1]) ."</td>\n";
		} else {
			print "<td align='right'>".number_format($row[1]) ."</td>\n";
		}
	} else {
		print "<td align='right'>".number_format($row[1]) ."</td>\n";
	}
	print "</tr>\n";
	$total= $total + $row[1];
}
print "<tr>\n";
print "<td bgcolor=#66FF66>合計</td>\n";
if($total > 149999) {
	print "<td bgcolor=#FFFF00 align='right'>".number_format($total) ."</td>\n";
} else {
	print "<td bgcolor=#66FF66 align='right'>".number_format($total) ."</td>\n";
}
print "</tr>\n";
print "</table>";

//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="location.href='../../index.php'"></FORM>
</body>
</html>
