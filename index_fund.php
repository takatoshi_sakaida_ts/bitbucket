<?PHP
session_cache_expire(60);
session_start();
//初期値設定
$_SESSION["kaitori_sort"]=0;
/*
0:管理者                (ADMIN)
1:カスタマーユーザー    (CUSTOMER)
2:DBチーム              (DB)
3:マーケティングスタッフ(MARKET)
4:ロジ                  (LOGI)
5:カスタマーリーダー    (CUSTOMER_L)

treeMenu23を使っています。次は24です
*/

define("ADMIN",'0');
define("CUSTOMER",'1');
define("DB",'2');
define("MARKET",'3');
define("LOGI",'4');
define("CUSTOMER_L",'5');

function getGroupName(){

	if(!isset($_SESSION["user_group"]) || $_SESSION["user_group"] == ''){
		// 未ログイン時はログイン画面へのリンクを表示
 		return "<a href='/login.php'>ログイン</a>";
	}
	$grpID = $_SESSION["user_group"];
	$name = array(
		'0'=>'管理者',
		'1'=>'カスタマーユーザー',
		'2'=>'DBチーム',
		'3'=>'マーケティングスタッフ',
		'4'=>'ロジ',
		'5'=>'カスタマーリーダー',
	);
	return $name[$grpID];
}

/**
    認証メソッド
    使い方：authGroup({メニュー観覧を許可したいグループ})
    複数設定したい場合はカンマ区切りで。
    例：authGroup(ADMIN,CUSTOMER)
*/
function authGroup($args){
	if( !isset($_SESSION["user_group"]) ) return false;
	unset($_SESSION["HSMAP"]);
	$grpID = func_get_args();
	return in_array($_SESSION["user_group"], $grpID);
}

?>
<html>
<head>
<link rel="shortcut icon" href="http://bolweb/icons/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="http://bolweb/icons/favicon.ico" type="image/vnd.microsoft.icon">
<STYLE>
a:link {text-decoration:none;}
a:visited {text-decoration:none;}
a:active {text-decoration:none;}
a:hover {text-decoration:none;}

div#loginInfo{
	position: absolute;
	top: 0;
	right: 0;
	margin: 10px;
}
</STYLE>
<SCRIPT language="JavaScript">
<!--
// ツリーメニュー
flag = false;
function treeMenu(tName) {
  tMenu = document.all[tName].style;
  if(tMenu.display == 'none') tMenu.display = "block";
  else tMenu.display = "none";
}
//-->
</SCRIPT>
</head>
<body><h1>BOOKOFF ONLINE　運用ツールTOP画面
<BR>
<font color=red size=4>
メニューをまとめました。■　のあるメニューは、クリックするとサブメニューがでます。<br>
新トップは<a href="/index_next.php">こちら</a>
</font>
</h1>
<div id="loginInfo"><?php echo getGroupName(); ?></div>
<?PHP if( authGroup(ADMIN,CUSTOMER,DB)): ?>
<!--
	<A href="http://10.169.33.27/projects/default">EC端末からの販売Trac</a><BR>
	<BR>
	<A href="http://10.169.33.28/projects/default">EC端末からの買取Trac</a><BR>
	<BR>
-->
<?php  endif; ?>

<?php if( authGroup(ADMIN) ): ?>
	<A href="javaScript:treeMenu('treeMenu16')">■ システム部ツール</a><br>
	<DIV id="treeMenu16" style="display:none">
	┣ <A href="./souko/shukkaerr/shukkaerr_input.php">ECシステム出荷エラーリカバリ</a><BR>
	┣ <A href="./souko/honninkakunindenpyou/searchhonnindenpyou.php">受取人確認書類発送伝票番号取込処理</a><BR>
	┃ <BR>
	┃ <A href="./souko/recollect/recollect_search.php">【店舗外仕入】再集荷設定</a><BR>
	┣ <A href="./souko/recollect_ecd/recollect_search.php">【店舗外仕入】再集荷設定（ECD）</a><BR>
	┃ <BR>
	┣ <A href="./souko/hikiate_rsv_test/searchhikiate.php">【検証環境】強制引当（新品）</a><BR>
    ┣ <A href="./souko/osirasedel_dev/osirasedel_input.php">お知らせメール・ブックマーク登録確認　 購入済お知らせメール・ブックマーク削除（検証用）</a><BR>
	┣ <A href="./souko/ord_stop_dev/searchord_stop.php">緊急出荷停止（検証用）</a><BR>
	┣ <A href="./souko/ord_stop_dev/updord_restart.php">緊急出荷再開処理（検証用）</a><BR>
	┣ <A href="./souko/vol_dev/vol.php">【検証環境】【Z1】NPOボランティア宅本便</a><BR>
	┣ <A href="./souko/volteikei_dev/volteikei.php">【検証環境】【Z6】サービス提携宅本便</a><BR>
	┣ <A href="./souko/volkigyou_dev/volkigyou.php">【検証環境】【Z7】企業ボランティア宅本便(CSR)</a><BR>
	┣ <A href="./souko/volkigyou_dev/starbucks_input.php">【検証環境】スターバックス専用入力画面(CSR)</a><BR>
	┣ <A href="./souko/vol_sekijuji_dev/vol.php">【検証環境】日本赤十字社専用入力画面</a><BR>
	┣ <A href="javaScript:treeMenu('treeMenu28')">【検証環境】販売プラットフォーム店舗出荷停止</a><br>
		<DIV id="treeMenu28" style="display:none">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;┣ <A href="./spf/spf_store_ord_stop_dev/searchord_stop.php">店舗出荷停止</a><BR>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;┗ <A href="./spf/spf_store_ord_stop_dev/updord_restart.php">店舗出荷再開（キャンセルをする前に必ず実施）</a><BR>
		</DIV>
	┗ <A href="javaScript:treeMenu('treeMenu29')">【検証環境】販売プラットフォーム返金処理</a><br>
		<DIV id="treeMenu29" style="display:none">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;┣ <A href="http://172.16.24.159/bol-web-tools/spf/repayment/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">【検証環境】返金対象注文検索・返金登録・返金訂正</a><BR>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;┗ <A href="http://172.16.24.159/bol-web-tools/spf/depositStatusUpdate?phpUserId=<?PHP print $_SESSION['user_id'];?>">【検証環境】入金ステータス更新</a><BR>
		</DIV>
	</DIV><BR>
<?php endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,DB,MARKET,LOGI,CUSTOMER_L)): ?>
	<A href="javaScript:treeMenu('treeMenu2')">■ 強制引当</a><br>
	<DIV id="treeMenu2" style="display:none">
	┣ <A href="./souko/hikiate/searchhikiate.php">強制引当（新品）</a><BR>
	┣ <A href="./souko/hikiate_new/searchhikiate_new.php">強制引当（新古）</a><BR>
	┗ <A href="./souko/hikiate_old/searchhikiate_old.php">強制引当（中古）</a><BR>
	</DIV><BR>
<?php endif; ?>

<!--<A href="./souko/makerkeppintool/searchmakerkeppin.php">メーカー欠品・Z納品書完納管理ツール</a><BR><BR>-->

<?php if(authGroup(ADMIN,CUSTOMER,LOGI,CUSTOMER_L,DB)): ?>
	<A href="javaScript:treeMenu('treeMenu13')">■ 注文・買取・会員情報検索</a><br>
	<DIV id="treeMenu13" style="display:none">
	┣ <A href="./souko/sorder/searchorder.php">注文情報検索</a><BR>
	┣ <A href="./souko/sorder_ecd/searchorder.php">注文情報検索（ECD）</a><BR>
	┣ <A href="./souko/sorder/searchdsorder.php">日販事故検索</a><BR>
	┣ <A href="./souko/pay_info/searchpayinfo.php">カード決済情報検索</a><BR>
	┃ <BR>
	┣ <A href="./souko/skaitori/searchkaitori.php">買取情報検索</a><BR>
	┣ <A href="./souko/skaitori/dtl/searchkaitori.php">買取情報検索(詳細版)</a><BR>
	┣ <A href="./souko/skaitori/temporarily.php">買取情報検索(一時保存)</a><BR>
	┣ <A href="./souko/searchcost/searchcosthistory.php">販売・買取価格履歴検索 </a><BR>
	┣ <A href="./souko/skaitori_uketsuke/search_uketsuke.php">買取受付情報検索(ID検索)</a><BR>
	┣ <A href="./souko/skaitori/kaitoriBox_search.php">集荷実績・配達完了箱数検索</a><BR>
	┃ <BR>
	┗ <A href="./souko/smember/searchmember.php">会員情報検索</a><BR>
	</DIV><BR>
<!--	<A href="./souko/shukajisseki/shukajisseki.php">集荷実績データダウンロード</a><BR><BR> -->
<?php endif; ?>

<A href="javaScript:treeMenu('treeMenu7')">■ 買取申込受領日検索・買取情報検索(計算担当スタッフから検索)・買取情報申込件数・集荷予定箱数・承認日時検索</a><br>
<DIV id="treeMenu7" style="display:none">
┣ <A href="./souko/jyuryoubi/search_jyuryoubi.php">買取申込受領日検索</a><BR>
┣ <A href="./souko/skaitori_staff/searchkaitori_staff.php">買取情報検索(計算担当スタッフから検索)</a><BR>
┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/kaitorisearch/">買取情報検索(計算スタッフより検索：新バージョン)</a><BR>
┣ <A href="./souko/takuhonmousikomi/takuhonmousikomi.php">買取情報申込件数・集荷予定箱数</a><BR>
┣ <A href="./souko/takuhonmousikomi_shucho/takuhonmousikomi_shucho.php">買取情報申込件数・集荷予定箱数(出買版)</a><BR>
┗ <A href="./souko/syoninnichiji/dsp_admit.php">承認日時検索</a><BR>

</DIV><BR>
<A href="javaScript:treeMenu('treeMenu8')">■ 在庫情報検索・フリーロケーション在庫数検索・メーカー欠品・入荷待ち検索・直近１週間高額注文検索結果</a><br>
<DIV id="treeMenu8" style="display:none">
┣ <A href="./souko/sstock/searchstock.php">在庫情報検索</a><BR>
┣ <A href="./souko/freelocation/freelocation.php">フリーロケーション在庫数検索</a><BR>
┣ <A href="./souko/makerkp/makerkeppin.php">メーカー欠品・入荷待ち検索</a><BR>
<?php if(authGroup(ADMIN)): ?>
┣ <A href="./souko/makerkp/makerkeppin_dev.php">メーカー欠品・入荷待ち検索(検証用)</a><BR>
<?php endif; ?>
┣ <A href="./souko/csv/makerkp/makerkp_shuukei.csv">メーカー欠品・入荷待ちデータダウンロード</a><BR>
┗ <A href="./souko/kougaku/kougakucheck.php">直近１週間高額注文検索結果</a><BR>
</DIV><BR>
<!-- <A href="./souko/searchcost/searchcost.php">現在原価検索</a><BR> -->
<!-- <A href="./souko/searchcost/searchcosthistory.php">販売・買取価格履歴検索 </a><BR> -->
<A href="/logistics/stock/now_stat_location_9002000000.php">【工事中】仕分け前フリーの現時点部門在庫状況</a><BR>
<BR>
<A href="./souko/btoborder/searchbtoborder.php">BtoB販売ツール　注文詳細ダウンロード</a><BR>
<br>
<A href="javaScript:treeMenu('treeMenu3')">■ 倉庫業務</a><br>
<DIV id="treeMenu3" style="display:none">
┣ <A href="./souko/shukkashuukei/shukkashuukei_total.php">出荷個数</a><BR>
┣ <A href="./souko/zaiko/zaikoGenre.php">ジャンル毎在庫数</a><BR>
┣ <A href="./souko/shukkashuukei/shukkashuukei.php">本日の出荷作業状況</a><BR>
┣ <A href="./souko/shukkashuukei/shukkashuukei_staff.php">本日の出荷作業状況（スタッフ毎）</a><BR>
┣ <A href="./souko/pickshuukei/pickshuukei.php">本日のピッキング作業状況</a><BR>
┣ <A href="./souko/pickshuukei/pickshuukei_staff_min_search.php">ピッキング点数（スタッフ・5分毎集計）</a><BR>
┣ <A href="./souko/pickshuukei/cancelPickshuukei_staff_min_search.php">キャンセルピッキング点数（スタッフ・5分毎集計）</a><BR>
┣ <A href="./souko/tanaireshuukei/tanaireshuukei.php">本日の棚入れ作業状況</a><BR>
┣ <A href="./souko/tanaireshuukei/tanaireshuukei_staff_min_search.php">棚入れ作業状況（スタッフ・5分毎集計）</a><BR>
┃ <BR>
┣ <A href="./souko/tanaireshuukei/osobanshuukei.php">遅番用本日集計結果</a><BR>
┃ <BR>
┣ <A href="./souko/syouninchk/syounin_sinchoku.php">買取承認進捗状況</a><BR>
┣ <A href="./souko/keisansin/satei_sinchoku.php">計算進捗状況</a><BR>
┣ <A href="./souko/keisansin/shucho_sinchoku.php">計算進捗状況(出買版)</a><BR>
┣ <A href="http://172.16.1.147:8080/bol-web-tools/assprogressmonitor/assprogressmonitor/">計算進捗状況(新バージョン)</a><BR>
┣ <A href="./souko/syouninchk/satei_sinchoku.php">計算結果不要・必要別計算進捗状況</a><BR>
┣ <A href="./souko/pickshuukei/pickshuukei_zan.php">ピッキング残点数状況</a><BR>
┣ <A href="./souko/pickshuukei/pickshuukei_zan_zone.php">ピッキング実施可能状況(非推奨)</a><BR>
┣ <A href="http://172.16.1.147:8080/bol-web-tools/pickingcountsecond/pickingcountsecond/">ピッキング実施可能状況(新バージョン)</a><BR>
┣ <A href="./souko/csv/keisan/keisanshuukei_<?PHP print date("Ymd");?>.csv">計算結果詳細（昨日実績分）</a><BR>
┃ <BR>
┣ <A href="./souko/z_reception/z_reception.php">Ｚ納品点数</a><BR>
┣ <A href="./souko/print_reception/print_reception.php">納品書印刷漏れ確認</a><BR>
┣ <A href="http://172.16.1.147:8080/bol-web-tools/printreception/printreception/">納品書印刷漏れ確認(新バージョン)</a><BR>
┃ <BR>
┣ <A href="./souko/make_shipment/make_shipment_serch.php">※使用不可※　出荷登録データリカバリ</a><BR>
┃ <BR>
┣ <A href="./souko/coupon_user_list/coupon_user_list.php">クーポン使用者検索</a><BR>
┃ <BR>
┗ <A href="http://172.16.1.147:8080/bol-web-tools/orderzone/orderzone/">出荷遅延検索</a><BR>

<!--┣ <A href="./souko/tanaireshuukei/tanaireshuukei_staff.php">本日の棚入れ作業状況(ゾーン毎棚入れ集計)</a><BR>-->
<!--┣ <A href="./souko/tanaireshuukei/tanaireshuukei_staff_min.php">本日の棚入れ作業状況(スタッフ・5分毎集計)</a><BR>-->
<!--┣ <A href="./souko/pickshuukei/pickshuukei_staff_kensuu_search.php">ピッキング件数・点数</a><BR>-->
<!--┣ <A href="./souko/zaiko/zaikoZone.php">ゾーン毎在庫数</a><BR>-->
<!--┣ <A href="./souko/pickshuukei/pickshuukei_staff_min_fix.php">本日のピッキング点数（スタッフ・5分毎集計）</a><BR>-->
<!--┣ <A href="./souko/pickshuukei/pickshuukei_staff.php">本日のピッキング作業状況（ゾーン毎ピッキング集計）</a><BR>-->
<!--┣ <A href="./souko/pickshuukei/dshuukei.php">本日の廃棄作業状況</a><BR>-->
<!--┣ <A href="./souko/pickshuukei/chouzaishukko.php">本日の帳在出庫作業状況</a><BR>-->
<!--┣ <A href="./souko/pickshuukei/chouzaisnyuko.php">本日の帳在入庫作業状況</a><BR>-->
<!--┣ <A href="./souko/tanaireshuukei/tanaireshuukeisiwakemae.php">本日の仕分前棚入れ作業状況</a><BR>-->
<!--┣ <A href="./souko/tanaireshuukei/tanaireshuukeisiwakeato.php">本日の仕分後棚入れ作業状況</a><BR>-->
<!--┣ <A href="./souko/tanaireshuukei/tananukishuukei.php">本日の棚抜き作業状況</a><BR>-->
<!--┣ <A href="./souko/shipyamato/shipyamato.php">本日のメール便出荷件数</a><BR>-->
<!--┣ <A href="./souko/saftshipcost/searchaftershipment.php">出荷時原価検索</a><BR>-->
<!--┣ <A href="./souko/keisanchk/keisan_chk.php">荷受数チェック(１１：３０と１９：３０に毎日確認)</a><BR>-->
<!--┣ <A href="./souko/picksin/pick_sinchoku.php">ピッキング進捗状況</a><BR>-->
<!--┣ <A href="./souko/shukkasin/shukka_sinchoku.php">出荷登録進捗状況</a><BR>-->
<!--┣ <A href="./souko/tanaireshuukei/tanaireshuukeisinchoku.php">棚入進捗状況</a><BR>-->
<!--┣ <A href="./souko/csv/misyouninlist/misyouninlist.csv">未承認リスト</a><BR>-->
<!--┣ <A href="./souko/csv/pick/pickshuukei_<?PHP print date("Ymd");?>.csv">ピッキング集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/pickshuukei_staff_batch/dlfile/pickshuukei_staff_<?PHP print date("Ymd");?>.csv">ピッキング集計結果（昨日実績分スタッフ・ゾーン毎）</a><BR>-->
<!--┣ <A href="./souko/csv/shukka/shukkashuukei_<?PHP print date("Ymd");?>.csv">出荷集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/tananukishuukei_<?PHP print date("Ymd");?>.csv">棚抜・棚入集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/tananukishuukei_cat_<?PHP print date("Ymd");?>.csv">棚抜（カテゴリ毎）集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/tanaireshuukei_genre1_<?PHP print date("Ymd");?>.csv">棚入（ジャンル１毎）集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/tanaireshuukei_bumon_<?PHP print date("Ymd");?>.csv">棚入（部門毎）集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/tanaireshuukei_staff_<?PHP print date("Ymd");?>.csv">棚入（スタッフ毎）集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/keisan_bumon_<?PHP print date("Ymd");?>.csv">計算（部門毎）集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanaire/hanbai_bumon_<?PHP print date("Ymd");?>.csv">販売（部門毎）集計結果（昨日実績分）</a><BR>-->
<!--┣ <A href="./souko/csv/tanazaiko/tanazaiko_bumon_<?PHP print date("Ymd");?>.csv">EC棚在庫数（部門毎）集計結果（本日朝実績分）</a><BR>-->
<!--┣<A href="./souko/csv/shukkazan/shukkazan.csv">出荷待ち注文・アイテム抽出（21:45に最新版となります）</a><BR>-->
<!--┗ <A href="./souko/pickingtool/pickingtool.php">ピッキングツールリセット</a><BR>-->
</DIV><BR>

<?php if(authGroup(ADMIN,LOGI,CUSTOMER,CUSTOMER_L)): ?>
	<A href="javaScript:treeMenu('logi_inputMenu')">■ BOL買取　新番号発行</a><br>
	<DIV id="logi_inputMenu" style="display:none">
	┣ <A href="./souko/volkigyou/tenpo_input.php">【Z7069】新番号発行</a><BR>
	┣ <A href="./souko/volkigyou/z7069_list.php">【BOL買取】発行済リスト確認</a><BR>
	┣ <A href="./souko/volkigyou/z7069_hokan_list.php">【一時在庫保管買取】発行済リスト確認</a><BR>
	┣ <A href="./souko/kaitori_dl/dl_kaitoriMenu.php">【BOL買取】買取データダウンロード</a><BR>
	┗ <A href="./souko/shuchou/dl_shuchou.php">出張買取エントリーデータダウンロード</a><BR>
	</DIV><BR>
<?php endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,CUSTOMER_L)): ?>
	<A href="javaScript:treeMenu('treeMenu4')">■ ボランティア入力</a><br>
	<DIV id="treeMenu4" style="display:none">
	┣ <A href="./souko/vol/vol.php">【Z1】NPOボランティア宅本便</a><BR>
	<!-- ┣ <A href="./souko/volteikei/volteikei.php">【Z6】サービス提携宅本便</a><BR> -->
	┣ <A href="./souko/volkigyou/volkigyou.php">【Z7】企業ボランティア宅本便(CSR)</a><BR>
	┣ <A href="./souko/volkigyou/starbucks_input.php">スターバックス専用入力画面</a><BR>
	┣ <A href="./souko/vol_sekijuji/vol.php">日本赤十字社専用入力画面</a><BR>
    <!-- ┣ <A href="http://172.16.1.147:8080/bol-web-tools/btpurchase/btpinput/">出張買取受付入力画面</a><BR> -->
	<!-- ┣ <A href="./souko/shuchou/dl_shuchou.php">出張買取エントリーデータダウンロード</a><BR> -->

	<!--┣ <A href="./souko/oldtakuhon/oldtakuhon.php">旧受付宅本便　新システム入力画面</a><BR>-->
	<!--┣ <A href="./souko/oldvol/oldtakuhon.php">旧受付ボランティア宅本便　新システム入力画面</a><BR>-->
	<!--┗ <A href="./souko/shanaitakuhon/shanaitakuhon.php">社内宅本便受付入力画面</a><BR>-->
	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu30')">■ 出張買取関連</a><br>
	<DIV id="treeMenu30" style="display:none">
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/btpurchase/btpinput/">出張買取受付入力画面</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/btpurchase/btpStatusUpdate/?mode=1">ステータス変更(本人確認待ち→承認待ち)</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/btpurchase/btpStatusUpdate/?mode=2">ステータス変更(承認待ち→振込手続き待ち)</a><BR>
	┣ <A href="./souko/shuchou/dl_shuchou.php">出張買取エントリーデータダウンロード</a><BR>
	</DIV><BR>

	<A href="javaScript:treeMenu('treeMenu31')">■ サービス提携宅本</a><br>
	<DIV id="treeMenu31" style="display:none">
	┣ <A href="./souko/volteikei/volteikei.php">【Z6】サービス提携宅本便</a><BR>
	</DIV><BR>

	<!--<A href="javaScript:treeMenu('treeMenu21')">■ 楽天関連</a><br> -->
	<!--<DIV id="treeMenu21" style="display:none"> -->
	<!--┗ <A href="./souko/receipt_rakuten/receipt_rakuten.php">楽天領収書発行</a><BR> -->
	<!--┗ <A href="./souko/rakutenhoryuu/rakutenhoryuu_input.php">楽天注文保留リカバリ（全額代引決済）</a><BR> -->
	<!--</DIV><BR> -->


	<A href="javaScript:treeMenu('treeMenu5')">■ お知らせメール関連</a><br>
	<DIV id="treeMenu5" style="display:none">
	┣ <A href="./souko/osirase/osirasemail.php">お知らせメール配信解除</a><BR>
	┗ <A href="./souko/osirasedel/osirasedel_input.php">お知らせメール・ブックマーク登録確認　 購入済お知らせメール・ブックマーク削除</a><BR>
	<!--┗ <A href="./souko/osirasedelzassi/osirasedelzassi_input.php">雑誌専用お知らせメール・ブックマーク削除確認</a><BR>-->
	</DIV><BR>
<?php endif; ?>

<!--	<A href="./souko/kaitoriupd/searchkaitoriupd.php">買取情報（計算結果・D対応）</a><BR> -->

<?php if(authGroup(ADMIN,CUSTOMER_L)): ?>
<!--
	<A href="javaScript:treeMenu('treeMenu15')">■ ボランティアデータダウンロード</a><br>
	<DIV id="treeMenu15" style="display:none">
-->
	<!--┣ <A href="./souko/dloldtakuhon_output/dloldtakuhon_output.php">旧受付宅本便　新システム入力データダウンロード</a><BR>-->
<!--
	┣ <A href="./souko/volout/dl_vol.php">ボランティア振込データダウンロード</a><BR>
	┣ <A href="./souko/volout/dl_vol_applicationdate.php">ボランティア振込データダウンロード（申込日絞込み）</a><BR>
	┣ <A href="./souko/volout/dl_vol_assdate.php">ボランティア振込データダウンロード（査定日絞込み）</a><BR>
	┗ <A href="./souko/volout/dl_vol_trandate.php">ボランティア振込データダウンロード（振込みデータ出力日絞込み）</a><BR>
	</DIV><BR>
 -->
	<A href="http://172.16.1.147:8080/bol-web-tools/volunteer/voldl/?phpUserId=<?PHP print $_SESSION['user_id'];?>">ボランティアデータダウンロード</a><BR><BR>
<?php endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,CUSTOMER_L)): ?>
<!--
	<A href="javaScript:treeMenu('treeMenu18')">■ 到着お知らせメールデータ作成</a><br>
	<DIV id="treeMenu18" style="display:none">
	┣ <A href="./souko/asslate/asslate_input.php">到着お知らせメールデータ作成</a><BR>
	┗ <A href="./souko/assmail/assmail_input.php">ヤマト用到着お知らせメールデータ作成</a><BR>
	</DIV><BR>
-->
<?php endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,CUSTOMER_L)): ?>
<!--
	<A href="javaScript:treeMenu('treeMenu20')">■無料ダンボール発送用登録データ作成受付画面</a><br>
	<DIV id="treeMenu20" style="display:none">
	┗ <A href="./souko/freecorrugated/new_searchfreecorrugated.php">【新】無料ダンボール発送用登録データ作成受付画面</a><BR>
	</DIV><BR>
-->
<?php endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,DB,MARKET,LOGI,CUSTOMER_L)): ?>
	<A href="javaScript:treeMenu('treeMenu6')">■ 買取情報更新関連</a><br>
	<DIV id="treeMenu6" style="display:none">
	┣ <A href="./souko/sinban/sinban_input.php">新番発行ガイド停止・集荷中止入力</a><BR>
	┣ <A href="./souko/sinban/sinban_create_search.php">倉庫システム　新番号発行配完入力</a><BR>
	┣ <A href="./souko/sinban_guide/sinbanguide_input.php">ガイド停止入力</a><BR>
	┣ <A href="./souko/sinban_shuuka/sinbanshuuka_input.php">集荷中止入力</a><BR>
	┣ <A href="./souko/regdm_upd/reg_dm_search.php">買取申込日変更</a><BR>
	┃ <BR>
	┣ <A href="./souko/sell_stop/searchsell_stop.php">計算一時停止</a><BR>
	┣ <A href="./souko/sell_stop/updsell_restart.php">計算再開処理</a><BR>
	┣ <A href="./souko/sell_statuscng/searchsell_statuscng.php">買取ステータス変更（本人確認待ち⇒計算中）</a><BR>
	┣ <A href="./souko/sell_statuscng2/searchsell_statuscng2.php">買取ステータス変更（キャンセル⇒計算中）</a><BR>
	┣ <A href="./souko/searchsell_ec/searchsell_ec.php">買取ステータス確認</a><BR>
	┣ <A href="./souko/kaitorican/kaitorican_input.php">買取受付キャンセル（倉庫システムのみECをキャンセルしてから実施）</a><BR>
	┣ <A href="./souko/asscancel/searchasscancel.php">倉庫システム　買取キャンセル</a><BR>
	┣ <A href="./souko/sell_upd/searchsell_upd.php">倉庫システム　買取受付箱数・計算結果要否・D引取変更</a><BR>
	┣ <A href="./souko/hurikomiud/hurikomiud_input.php">組み戻し買取口座修正（買取受付情報を正しい値にしてから実施してください）</a><BR>
	┣ <A href="./souko/sell_updpayhold/searchupdsell_payhold.php">振込保留一括解除</a><BR>
	┃ <BR>
	┗ <A href="./souko/ass_status/ass_status_complete_search.php">査定完了</a><BR>
	<!--┣ <A href="./souko/boxchange/boxchange_input.php">宅本便集荷予定日・箱数変更画面</a><BR>-->
	<!--┣ <A href="./souko/honninmatiout/dl_honninmati.php">本人確認待ちユーザダウンロード</a><BR>-->
	<!--┣ <A href="./souko/syouninhoyuuout/dl_syouninhoyuuout.php">承認保留ユーザダウンロード</a><BR>-->

	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu9')">■ 会員ID復活・強制退会</a><br>
	<DIV id="treeMenu9" style="display:none">
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaiin/kaiin/?displayPtn=1">会員ID復活（退会前の状態に戻す場合のみ）</a><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/kaiin/kaiin/?displayPtn=0">強制退会</a><BR>
	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu10')">■ 出荷停止・再開</a><br>
	<DIV id="treeMenu10" style="display:none">
	┣ <A href="./souko/ord_stop/searchord_stop.php">緊急出荷停止</a><BR>
	┣ <A href="./souko/ord_stop/updord_restart.php">緊急出荷再開処理（キャンセルをする前に必ず実施）</a><BR>
	┃ <BR>
	┣ <A href="./spf/spf_store_ord_stop/searchord_stop.php">【店舗】緊急出荷停止</a><BR>
	┣ <A href="./spf/spf_store_ord_stop/updord_restart.php">【店舗】出荷再開</a><BR>
	┃ <BR>
	┣ <A href="./souko/subjectcause/searchord_subjectcause.php">納品注意喚起メッセージ入力更新画面</a><BR>
	┃ <BR>
	┗ <A href="./souko/shukkaerr/shukkaerr_input.php">ECシステム　オーソリエラー出荷エラーリカバリ</a><BR>
	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu32')">■ 入金・返金関連</a><br>
	<DIV id="treeMenu32" style="display:none">
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/spf/repayment/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">返金対象注文検索・返金登録・返金訂正</a><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/spf/depositStatusUpdate?phpUserId=<?PHP print $_SESSION['user_id'];?>">入金ステータス更新</a><BR>
	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu33')">■ 領収書発行ツール</a><br>
	<DIV id="treeMenu33" style="display:none">
	┣ <A href="./souko/receipt_ec/receipt.php">本サイト</a><BR>
	┗ <A href="./souko/receipt_rakuten/receipt_rakuten.php">楽天</a><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,LOGI)): ?>
	<A href="javaScript:treeMenu('treeMenu14')">■ 計算･出荷停止・買取ステータス確認</a><br>
	<DIV id="treeMenu14" style="display:none">
	┣ <A href="./souko/sell_stop/searchsell_stop.php">計算一時停止</a><BR>
	┣ <A href="./souko/sell_stop/updsell_restart.php">計算再開処理</a><BR>
	┃ <BR>
	┣ <A href="./souko/ord_stop/searchord_stop.php">緊急出荷停止</a><BR>
	┣ <A href="./souko/ord_stop/updord_restart.php">緊急出荷再開処理</a><BR>
	┃ <BR>
	┣ <A href="./souko/searchsell_ec/searchsell_ec.php">買取ステータス確認</a><BR>
	┣ <A href="./souko/skaitori/temporarily.php">買取情報検索(一時保存)</a><BR>
	┃ <BR>
	┣ <A href="./souko/ass_status/ass_status_change_search.php">再査定開始</a><BR>
	┗ <A href="./souko/ass_status/ass_status_complete_search.php">査定完了</a><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,DB)): ?>
	<A href="javaScript:treeMenu('treeMenu11')">■ オトナグループ検索</a><br>
	<DIV id="treeMenu11" style="display:none">
	┣ <A href="./souko/otonacnt/countgroup.php">オトナグループ登録状況確認</a><BR>
	┣ <A href="./souko/otonacnt/readcountgroup.php">オトナグループ登録状況履歴確認</a><BR>
	┣ <A href="./souko/hihyouji/hihyouji.php">オトナグループ登録済非表示マスタ検索</a><BR>
	┗ <A href="./souko/otonacnt/cartstatus.php">オトナグループカート登録状況確認</A><BR>
	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu12')">■ データダウンロード</a><br>
	<DIV id="treeMenu12" style="display:none">
	┣ <A href="./souko/csv/osirasecount/osirasecount_book_comic.csv">入荷お知らせメール登録データ（書籍・コミック）</a><BR>
	┣ <A href="./souko/csv/osirasecount/osirasecount_cd.csv">入荷お知らせメール登録データ（CD）</a><BR>
	┣ <A href="./souko/csv/osirasecount/osirasecount_dvd.csv">入荷お知らせメール登録データ（DVD）</a><BR>
	┣ <A href="./souko/csv/osirasecount/osirasecount_game.csv">入荷お知らせメール登録データ（GAME）</a><BR>
	┣ <A href="./souko/csv/comic_zaiko2/all_zaiko2.csv">中古在庫１以上　データダウンロード</a><BR>
	┃ <BR>
	┣ <A href="./souko/demachi_output/searchdldemachi.php">デマチメール登録者アドレスダウンロード</a><BR>
	┃ <BR>
	┗ <A href="./souko/sorder_market/sorder_market.php">注文情報抽出</a><BR>
	<!--┣ <A href="./souko/csv/henpin/henpinshuukei.csv">返品集計（過去90日以内）</a><BR>-->
	<!--┣ <A href="./souko/csv/bumon/bumon.zip">インストアコード・部門コードリンク用</a><BR>-->
	<!--┃ <BR>-->
	<!--┣ <A href="./souko/csv/all100/all100yen.csv">100円商品データダウンロード</a><BR>-->
	<!--┣ <A href="./souko/csv/100yen/100yen.csv">100円商品在庫なしリストデータダウンロード</a><BR>-->
	<!--┣ <A href="./souko/csv/shuuyou_limit/shuuyoulimit_genre1code.csv">在庫数の分布データダウンロード</a><BR>-->
	<!--┣ <A href="./souko/csv/shuuyou_limit/shuuyoulimit_instorecode.csv">ジャンル１ごとの100円点数データダウンロード</a><BR>-->
	<!--┣ <A href="./souko/csv/yesterday_order_zaikonasi/yesterday_order_zaikonasi.csv">昨日注文アイテム在庫なしデータダウンロード</a><BR> -->
	<!--┣ <A href="./souko/csv/kakakuchousa/kakakuchousa.csv">市場価格・中古価格差異データダウンロード</a><BR> -->
	<!--┣ <A href="./souko/csv/sinpinteika/sinpinteikawaribiki.csv">新品定価割引率不具合データダウンロード</a><BR> -->
	<!--┣ <A href="./souko/osirasedel/osirasedel_input.php">お知らせメール・ブックマーク登録確認　 購入済お知らせメール・ブックマーク削除</a><BR>-->
	<!--┣ <A href="./souko/csv/skdhaiban/skdhaiban.csv">星光堂廃盤情報ダウンロード</a><BR> -->
	<!--┃ <BR> -->
	<!--┃ <BR>-->
	<!--┣ <A href="./souko/csv/rdlist/rdlist_<?PHP print date("Ymd");?>.csv">ランクダウン候補抽出結果</a><BR>-->
	<!--┃ <BR>-->
	<!--┗ <A href="./souko/tairax/dlfile/tairax_<?PHP print date("Ymd");?>.csv">棚入お知らせメール送信なしアイテム抽出</a><BR>-->
	</DIV><BR>
	<A href="javaScript:treeMenu('treeMenu19')">■ 緊急トラック情報削除</a><br>
	<DIV id="treeMenu19" style="display:none">
	┗ <A href="./souko/rmtrack/searchrmtrack.php">緊急トラック情報削除</a><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,DB,CUSTOMER_L)): ?>
	<A href="./souko/demachi/searchdemachi.php">デマチメール登録インストア検索</a><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,DB)): ?>
<!--	<br><A href="./souko/tanaorosi/searchtanaorosi.php">棚卸データ作成</a><BR><BR> -->
<?PHP endif; ?>

<?php if(authGroup(ADMIN,MARKET)): ?>
<!--	<A href="./souko/otonaout/dl_otona.php">EDM用メールアドレス表示</a><BR> -->
<!--	<A href="./souko/sass/searchass.php">計算結果　商品検索</a><BR> -->
<?PHP endif; ?>

<?php /*if(authGroup(ADMIN))*/ if(false): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu1')">■ 買取関連(管理者用)</a><br>
	<DIV id="treeMenu1" style="display:none">
	<!--┣ <A href="./souko/honninkakunin/searchhonnin.php">ECステータス一括確認</a><BR>-->
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/bulksearch?phpUserId=<?PHP print $_SESSION['user_id'];?>">ECステータス一括確認</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/bulkcancel?phpUserId=<?PHP print $_SESSION['user_id'];?>">ECステータス一括取消</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/bulkdownload?phpUserId=<?PHP print $_SESSION['user_id'];?>">ECステータス一括取消ダウンロード</a><BR>
	┣ <A href="./souko/kaitorishohazusi/kaitorishohazusi_input.php">買取申込書受領チェック外し受付画面</A><BR>
	┃ <BR>
	┣ <A href="./souko/honninout/searchhonninout.php">本人確認発送用登録データ作成受付画面</A><BR>
	┃ <BR>
	┣ <A href="./souko/csv/honningazou/honningazou_<?PHP print date("Ymd");?>.csv">本人確認用画像未確認リスト</A><BR>
	┃ <BR>
	┣ <A href="./souko/honninncheck/honninncheck.php">本人確認状態確認画面</A><BR>
	┗ <A href="./souko/searchhonninnmati/searchfirstmousikomimati.php">買取申込書待ち一覧画面（初回催促対象）</A><BR>
	<!--┣ <A href="./souko/newhonninn/newhonnin_conf.php">買取一覧振分処理(毎日実施)</A><BR>-->
	<!--┃ <BR>-->
	<!--┣ <A href="./souko/searchhonninnmati/searchhonninnmati.php">本人確認待ち一覧画面</A><BR>-->
	<!--┣ <A href="./souko/searchhonninnmati/searchmousikomimati.php">買取申込書待ち一覧画面（全検索）</A><BR>-->
	</DIV>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,CUSTOMER,CUSTOMER_L)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu17')">■ 買取関連</a><br>
	<DIV id="treeMenu17" style="display:none">
	<!--┣ <A href="./souko/honninkakunin/searchhonnin.php">ECステータス一括確認</a><BR>-->
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/bulksearch?phpUserId=<?PHP print $_SESSION['user_id'];?>">ECステータス一括確認</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/bulkcancel?phpUserId=<?PHP print $_SESSION['user_id'];?>">ECステータス一括取消</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/kaitori/bulkdownload?phpUserId=<?PHP print $_SESSION['user_id'];?>">ECステータス一括取消ダウンロード</a><BR>
	┣ <A href="./souko/kaitorishohazusi/kaitorishohazusi_input.php">買取申込書受領チェック外し受付画面</A><BR>
	┃ <BR>
	┣ <A href="./souko/honninout/searchhonninout.php">本人確認発送用登録データ作成受付画面</A><BR>
	┃ <BR>
	┣ <A href="./souko/csv/honningazou/honningazou_<?PHP print date("Ymd");?>.csv">本人確認用画像未確認リスト</A><BR>
	┃ <BR>
	┣ <A href="./souko/honninncheck/honninncheck.php">本人確認状態確認画面</A><BR>
	┣ <A href="./souko/searchhonninnmati/searchhonninnmati.php">本人確認待ち一覧画面</A><BR>
	┣ <A href="./souko/searchhonninnmati/searchmousikomimati.php">買取申込書待ち一覧画面（全検索）</A><BR>
	┣ <A href="./souko/searchhonninnmati/searchfirstmousikomimati.php">買取申込書待ち一覧画面（初回催促対象）</A><BR>
	┣ <A href="/takuhon/no_account/no_account_user_sell_stat.php">WEB非会員申込一覧</A><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/sell/sellsearch/">WEB非会員申込一覧(新バージョン)</A><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if( authGroup(ADMIN,CUSTOMER,CUSTOMER_L,DB,LOGI) ): ?>
	<A href="javaScript:treeMenu('yahooSell')">■ Y!買取ツール</a><br>
	<DIV id="yahooSell" style="display:none">
	<!--┣ <A href="http://172.16.1.147:8080/bol-web-tools/yahuoff/sell/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">検索画面(廃止予定)</a><BR>-->
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/yahuoff/sellsimple/searchsimple?phpUserId=<?PHP print $_SESSION['user_id'];?>">検索画面(簡易版)</a><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/yahuoff/download?phpUserId=<?PHP print $_SESSION['user_id'];?>">受取人確認サポートCSVダウンロード画面</a><BR>
	</DIV><BR>
<?php endif; ?>

<?php if(authGroup(ADMIN,LOGI)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu23')">■ 商品管理</a><br>
	<DIV id="treeMenu23" style="display:none">
	┣ <A href="./syouhinn/tengaiuriage/tengaiuriage_work.php">店外売上作業確認</A><BR>
	┣ <A href="./syouhinn/tengaiuriage/tengaiuriage_data_search.php">店外売上データ確認</A><BR>
	┣ <A href="./souko/tri_output/dsp_tengaiuriage.php">TRiデータ再出力</a><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/tengaiuriage/tengai?phpUserId=<?PHP print $_SESSION['user_id'];?>">店外売上リアルタイム検索機能</A><BR>  
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,LOGI)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu24')">■ 統計情報</a><br>
	<DIV id="treeMenu24" style="display:none">
	┣ <A href="./souko/tokei/dss_authtokei.php">自動承認スキップ統計情報</A><BR>
	┣ <A href="./souko/tokei/dss_approval_ratio.php">承認待ち比率</A><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/beforekuzushi/beforekuzushi/">崩し待ち点数</A><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,DB)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu25')">■ 商品マスタ</a><br>
	<DIV id="treeMenu25" style="display:none">
	┗ <A href="./master/dataCoordinationNum/dataCoordinationNum.php">商品マスタ連携予定数</A><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if( authGroup(ADMIN,CUSTOMER,CUSTOMER_L) ): ?>
	<A href="javaScript:treeMenu('tenposhiire')">■ 店舗外仕入れ</a><br>
	<DIV id="tenposhiire" style="display:none">
<!--	┗ <A href="./souko/recollect_ecd/recollect_search.php">再集荷依頼（ECD）</a><BR> -->
	┗ <A href="./souko/recollect/recollect_search.php">再集荷依頼</a><BR>
	</DIV><BR>
<?php endif; ?>

<?php if( authGroup(ADMIN,CUSTOMER,CUSTOMER_L,DB,LOGI) ): ?>
	<A href="javaScript:treeMenu('yahooauc')">■ ヤフオク！ツール</a><br>
	<DIV id="yahooauc" style="display:none">
	┣ <A href="./souko/yahooauc/bid_recovery/view.php">【bookoff2016】取込エラー一覧<BR>
	┣ <A href="./souko/yahooauc/display_cancel/search.php">【bookoff2016】オークション取消し</a><BR>
	┗ 【bookoff2016】評価要否切り替え</a><BR>
	</DIV><BR>
<?php endif; ?>

<?php if(authGroup(ADMIN,DB,LOGI,CUSTOMER_L)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu22')">■ 倉庫システムメンテナンス</a><br>
	<DIV id="treeMenu22" style="display:none">
	┣ <A href="./souko/mcompany/regist.php">%UP企業マスタ登録</a><BR>
	┣ <A href="./souko/trackinfo/search.php">トラック情報修正</a><BR>
	┣ <A href="./souko/soko_shijisho/search.php">倉庫指示書修正</a><BR>
	┣ <A href="./souko/zone_priority/zone_priority.php">ゾーン優先引当設定変更</a><BR>
	┣ <A href="./souko/pickshuukei/test_pickshuukei_staff_min_search.php">(テスト)ピッキング点数（スタッフ・5分毎集計）</a><BR>
	┣ <A href="./souko/test/dsp_tengaiuriage.php">TRiデータ再出力</a><BR>
	┣ <A href="./souko/test/dl_vol_trandate.php">ボランティア振込データダウンロード（振込みデータ出力日絞込み）</a><BR>
	┗ <A href="./souko/test/searchkaitori_staff.php">買取情報検索(計算スタッフより検索)</a><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,DB,LOGI,CUSTOMER)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu26')">■ 宅本便梱包材申込</a><br>
	<DIV id="treeMenu26" style="display:none">
	┣ <A href="http://172.16.24.159/bol-web-tools/tbag/sell/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">梱包材申込検索画面（検証環境）</a><BR>
	┗ <A href="http://172.16.24.159/bol-web-tools/tbag/download?phpUserId=<?PHP print $_SESSION['user_id'];?>">ゆうプリR取込用CSVダウンロード画面（検証環境）</a><BR>
	┗ <A href="http://172.16.24.159/bol-web-tools/tbag/locker/download?phpUserId=<?PHP print $_SESSION['user_id'];?>">e秘伝�U取込用CSVダウンロード画面（検証環境）</a><BR>
	┗ <A href="http://172.16.24.159/bol-web-tools/tbag/locker/locker?phpUserId=<?PHP print $_SESSION['user_id'];?>">FTSマンションロッカーメンテナンス画面（検証環境）</a><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN,MARKET,CUSTOMER_L)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu27')">■ ブックラ申込</a><br>
	<DIV id="treeMenu27" style="display:none">
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/funding/partnersearch/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">提携先情報検索画面</a><BR>
	┣ <A href="http://172.16.1.147:8080/bol-web-tools/funding/sellsearch/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">申込情報検索画面</a><BR>
	┗ <A href="http://172.16.1.147:8080/bol-web-tools/funding/delvdl/?phpUserId=<?PHP print $_SESSION['user_id'];?>">リターン配送情報ダウンロード画面</a><BR>
	</DIV><BR>
<?PHP endif; ?>

<?php if(authGroup(ADMIN)): ?>
	<BR>
	<A href="javaScript:treeMenu('treeMenu34')">■ ブックラ申込（検証環境）</a><br>
	<DIV id="treeMenu34" style="display:none">
	┣ <A href="http://172.16.24.159/bol-web-tools/funding/partnersearch/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">提携先情報検索画面（検証環境）</a><BR>
	┣ <A href="http://172.16.24.159/bol-web-tools/funding/sellsearch/search?phpUserId=<?PHP print $_SESSION['user_id'];?>">申込情報検索画面（検証環境）</a><BR>
	┣ <A href="http://172.16.24.159/bol-web-tools/volunteer/voldl/?phpUserId=<?PHP print $_SESSION['user_id'];?>">ボランティアデータダウンロード画面（検証環境）</a><BR>
	┗ <A href="http://172.16.24.159/bol-web-tools/funding/delvdl/?phpUserId=<?PHP print $_SESSION['user_id'];?>">リターン配送情報ダウンロード画面（検証環境）</a><BR>
	</DIV><BR>
<?PHP endif; ?>

</body>
</html>
