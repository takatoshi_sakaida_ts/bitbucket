<?PHP
require '../parts/pagechk.inc';
require_once('../../souko/parts/db_auto_selector.php');
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>SPF店舗出荷停止再開対象検索結果</title>
<link rel="stylesheet" href="/css/bolweb-env.css"/>
</head>
<body bgcolor="#ffa07a" class="env-<?php echo(detect_env()); ?>">
<?php
print "<BR><font size=5><strong>SPF店舗出荷停止注文一覧</strong></font><br><br>\n<HR>";
//ファイルの読み込み
require_once("../parts/selectvalue_ec.php");
//データベースへの接続開始
$db = connect_to_ec_database();
$sql = <<<SQL
select
	ord.ORD_NO as val0
,	ord.ORDR_NM_FST||' '||ord.ORDR_NM_MID  as val1
,	delv.ZIP_ADDR1||delv.ZIP_ADDR2||delv.ZIP_ADDR3||delv.DTL_ADDR as val2
,	(ord.TOT_INV_AMT - ord.ORD_CNCL_AMT) as val3
,	ord.PAY_TP as val4
,	spf.val5 as val5
from
	(
		select
			a.ORD_NO
		,	LISTAGG(b.STORE_NM||'('||a.STORE_CD||')', '<br>')
			WITHIN GROUP (order by a.ORD_NO, a.STATUS, a.STORE_CD) as val5
		from TORDERDELV_SPF a
		,	TSTORE_TKH b
		where
			a.STORE_CD=b.STORE_CD
		and a.SHIP_STOP_YN='1'
		group by a.ORD_NO
	) spf
,	TORDER ord
,	TORDERDELV delv
where
	spf.ORD_NO=ord.ORD_NO
and delv.DELV_NO in (select distinct delv_no from TORDERDTL where ord_no=spf.ord_no and delv_no is not null)
SQL;

$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
$j=1;
$count = $res->numRows();

if ($count==0) {
	print "出荷停止中の注文はありません";
} else {

	while($row = $res->fetchRow()){
		if ($j==1) {
			print "<table border=1>\n";
			print "<tr bgcolor=#ccffff>\n";
			//項目名の表示
			print "<td nowrap width=120>注文番号</td><td width=100>氏名</td><td width=150>配送先住所</td><td nowrap width=50>金額</td><td nowrap width=80>支払方法</td><td nowrap width=100>出荷停止中店舗</td><td>　</td>";
			print "</tr>";
		}
		print "<tr bgcolor=#ffffff>";
		print "<td>".$row[0] ."</td>";
		print "<td  width=100>".$row[1] ."</td>";
		print "<td width=400>".$row[2] ."</td>";
		print "<td>".$row[3] ."</td>";
		print "<td>".Retkessai($row[4]) ."</td>";
		print "<td>".$row[5] ."</td>";
		print "<td width=150 align=center><font size=+1>";
		print "<a href='updord_stop_confirm.php?ord_no=" .$row[0] . "&ptn=2'>出荷再開</a>";
		print "</font></td>";
		print "</tr>";
		$j=2;
	}
	print "</table>";
	print "<HR>";
}
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
