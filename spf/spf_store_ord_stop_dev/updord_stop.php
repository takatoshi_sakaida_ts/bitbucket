<?PHP
require '../parts/pagechk.inc';
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>SPF店舗出荷停止対象検索結果</title>
</head>
<body bgcolor="#f0e68c">
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
require_once("../parts/selectvalue_ec.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
}
//ログイン情報の読み込み
require_once("../../souko/parts/login_ecd.php");
//SQL文のCOUNT関数を使用
$sql = "select a.ord_no as val0, a.ORDR_NM_FST||' '||a.ORDR_NM_MID as val1, c.ZIP_ADDR1||c.ZIP_ADDR2||c.ZIP_ADDR3||c.DTL_ADDR as val2, (a.TOT_INV_AMT - a.ORD_CNCL_AMT) as val3, a.PAY_TP as val4, e.STORE_NM||'('||d.STORE_CD||')' as val5, d.STATUS as val6, d.SHIP_STOP_YN as val7";
$sql = $sql . " from TORDER a";
$sql = $sql . ", (select distinct DELV_NO from TORDERDTL where ORD_NO='".$keyti."') b";
$sql = $sql . ", TORDERDELV c";
$sql = $sql . ", TORDERDELV_SPF d";
$sql = $sql . ", TSTORE_TKH e";
$sql = $sql . " where a.ORD_NO = d.ORD_NO and c.DELV_NO in (select distinct DELV_NO from TORDERDTL where ORD_NO='".$keyti."') and d.STORE_CD = e.STORE_CD(+)";
$sql = $sql . " and a.ORD_NO='".$keyti."' and d.STATUS in ('0','1','2')";
$sql = $sql . " order by d.STATUS, d.STORE_CD";
//print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}

print "<BR><font size=5><strong>SPF店舗出荷停止対象検索結果</strong></font><br><br>";

//検索結果の表示
print "<strong><BR>検索条件：". $keyti."</strong> <br><br>\n<HR>";
$j=1;
$count = $res->numRows();

if ($count==0) {
	print "対象の注文は存在しません";
} else {
	while($row = $res->fetchRow()){
		if ($j==1) {
			print "<font color=#ff0000><strong>ステータスが＜ピッキング待ち＞または＜出荷待ち＞の店舗のみ出荷停止します<br>※スタータスが＜出荷待ち＞の場合、店舗作業状況次第では出荷済みの可能性があります</strong></font>";
			print "<table border=1>\n";
			print "<tr bgcolor=#ccffff>\n";
			//項目名の表示
			print "<td nowrap width=120>注文番号</td><td nowrap width=100>氏名</td><td width=200>住所</td><td nowrap width=50>金額</td><td nowrap width=80>支払方法</td><td>出荷店舗</td><td>ステータス</td><td>　</td>";
			print "</tr>";
			print "<tr bgcolor=#ffffff>";
			//注文番号
			print "<td rowspan=".$count." width=100>".$row[0] ."</td>";
			print "<td rowspan=".$count.">".$row[1] ."</td>";
			print "<td rowspan=".$count." width=400>".$row[2] ."</td>";
			print "<td rowspan=".$count.">".$row[3] ."</td>";
			print "<td rowspan=".$count.">".Retkessai($row[4]) ."</td>";
			print "<td>".$row[5] ."</td>";
			print "<td>".RetStoreShipStatus($row[6]) ."</td>";
			print "<td rowspan=".$count." width=150 align=center><font size=+1>";
			if (($row[6]=='0' || $row[6]=='1') && ($row[7]=='0')) {
				print "<a href='updord_stop_confirm.php?ord_no=" .$row[0] . "&ptn=1'>出荷停止</a>";
			} elseif ($row[7]=='1') {
				print "停止済み";
			} else {
				print "-";
			}
			print "</font></td>";
			print "</tr>";
		} else {
			print "<tr bgcolor=#ffffff>";
			print "<td>".$row[5] ."</td>";
			print "<td>".RetStoreShipStatus($row[6]) ."</td>";
			print "</tr>";
		}
		$j=2;
	}
	print "</table>";
	print "<HR>";
}
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
