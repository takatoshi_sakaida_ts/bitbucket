<?PHP
function db_update($db = null){
	if ($db == null) {
		//PEARの利用     -------(1)
		require_once("DB.php");
		//ログイン情報の読み込み
		require_once("../../souko/parts/login_ec.php");
		//dsnの書式　データベースのタイプ://ユーザ名:パスワード@ホスト名/データベース名
		$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
		//データベースへの接続開始
		$db = DB::connect($dsn);
		//エラーの抽出
		if(DB::isError($db)){
			echo "Fail\n" . DB::errorMessage($db) . "\n";
		}
		$db->setOption('portability',DB_PORTABILITY_NULL_TO_EMPTY | DB_PORTABILITY_NUMROWS);
	}
	require_once("../log/loger.inc");
	$sql="update TORDERDELV_SPF set ";

	switch ($_SESSION["HSMAP"]["ptn"]){
		case 1:
			$sql=$sql."SHIP_STOP_YN='1', SHIP_STOP_DM=TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS'), UPD_DM=TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS') ";
			break;
		case 2:
			$sql=$sql."SHIP_STOP_YN='0', SHIP_RESTART_DM=TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS'), UPD_DM=TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS') ";
			break;
		default:
			$retvalue= "エラー";
			break;
	}

	$sql=$sql."where ORD_NO='".$_SESSION["HSMAP"]["ord_no"]."' and STATUS in ('0', '1') ";
	$db->autoCommit( false ); 
	$res2 = $db->query($sql);
	if(DB::isError($res2)){
		print $res2->getMessage();
		$res2->DB_Error($res2->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	}
	output_log($sql,$_SESSION["user_id"],'spf_ord_stopupd_update_sql');
	$db->commit();
	//print "<br>".$sql;
	//データの開放
	$db->disconnect();
}
?>