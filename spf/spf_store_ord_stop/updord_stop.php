<?PHP
require '../parts/pagechk.inc';
require_once('../../souko/parts/db_auto_selector.php');
require_once('../../souko/ord_stop/db_select_ord_stop_details.php');
?>
<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>SPF店舗出荷停止対象検索結果</title>
<link rel="stylesheet" href="/css/bolweb-env.css"/>
<link rel="stylesheet" href="/css/bolweb-ord-stop.css"/>
</head>
<body class="spf-ord-stop env-<?php echo(detect_env()); ?>">
<style>
/* 倉庫引当分セル */
td.is-logi {
	background-color: #dddddd;
}
</style>
<?php
//ファイルの読み込み
require_once("../parts/selectvalue_ec.php");

// ステータスコードをラベル文字列に変換する。
// 倉庫引当分については "-" 固定。
function get_status_string($detail) {
	if ($detail->is_logi()) {
		return "-";
	}
	return RetStoreShipStatus($detail->status);
}

// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);

// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "検索条件を入力してください<br>";
	$search_result = array();

} else {
	// データベースから注文情報取得
	$db = connect_to_ec_database();
	$search_result = select_ord_stop_details($db, $keyti);
	$db->disconnect();
}

print "<BR><font size=5><strong>SPF店舗出荷停止対象検索結果</strong></font><br><br>";

//検索結果の表示
print "<strong><BR>検索条件：". $keyti."</strong> <br><br>\n<HR>";
$j=1;
$count = count($search_result);
if ($count==0) {
	print "対象の注文は存在しません";

} else {
	print "<font color=#ff0000><strong>ステータスが＜ピッキング待ち＞または＜出荷待ち＞の店舗のみ出荷停止します<br>";
	print "※ステータスが＜出荷待ち＞の場合、店舗作業状況次第では出荷済みの可能性があります<br>";
	print "※出荷店舗が「BOOKOFF宅本便(99997)」の明細は停止対象外です。</strong></font>";

	// 店舗引当分と倉庫引当分をそれぞれ連続するようまとめる。
	function sort_store_first($a, $b) {
		if ($a->is_logi()) {
			if ($b->is_logi()) {
				return 0;
			}
			return 1;

		} else {
			if ($b->is_logi()) {
				return -1;
			}
			return 0;
		}
	}
	usort($search_result, "sort_store_first");

	// 店舗引当分と倉庫引当分がそれぞれ何行あるか数える。
	$count_logi = 0;
	$count_store = 0;
	foreach ($search_result as $row) {
		if ($row->is_logi()) {
			$count_logi++;
		} else {
			$count_store++;
		}
	}

	print "<table border=1>\n";
	print "<tr bgcolor=#ccffff>\n";
	print "<td nowrap width=120>注文番号</td>";
	print "<td nowrap width=100>氏名</td>";
	print "<td width=200>住所</td>";
	print "<td nowrap width=50>金額</td>";
	print "<td nowrap width=80>支払方法</td>";
	print "<td>出荷店舗</td>";
	print "<td>ステータス</td>";
	print "<td>&nbsp;</td>";
	print "</tr>";

	// 店舗引当分・倉庫引当分をそれぞれグループ化するためのフラグ。
	$logi_shows = false;
	$store_shows = false;
	foreach($search_result as $row) {
		$is_logi = $row->is_logi();

		// 倉庫引当分、店舗引当分はグループ化し、それぞれの先頭行のみに
		// rowspan を出力する。
		$sub_rowspan = 0;
		if ($is_logi && !$logi_shows) {
			$sub_rowspan = $count_logi;
			$logi_shows = true;

		} else if (!$is_logi && !$store_shows) {
			$sub_rowspan = $count_store;
			$store_shows = true;
		}
		if ($j==1) {
			print "<tr bgcolor=#ffffff>";
			print "<td rowspan=".$count." width=100>".$row->ord_no ."</td>";
			print "<td rowspan=".$count.">".$row->name ."</td>";
			print "<td rowspan=".$count." width=400>".$row->address ."</td>";
			print "<td rowspan=".$count.">".$row->amt ."</td>";
			print "<td rowspan=".$count.">".Retkessai($row->pay_tp) ."</td>";
			print "<td class=\"". ($is_logi ? 'is-logi' : ''). "\">".$row->store_nm. "(". $row->store_cd. ")</td>";
			print "<td class=\"". ($is_logi ? 'is-logi' : ''). "\">".get_status_string($row) ."</td>";
			if ($sub_rowspan > 0) {
				print "<td rowspan=".$sub_rowspan." width=150 class=\"stop-link ". ($is_logi ? 'is-logi' : ''). "\">";
				if ($is_logi) {
					print "-";

				} else if (($row->status=='0' || $row->status=='1') && ($row->stop_yn=='0')) {
					print "<a href='updord_stop_confirm.php?ord_no=" .$row->ord_no . "&ptn=1'>出荷停止</a>";

				} elseif ($row->stop_yn=='1') {
					print "停止済み";

				} else {
					print "-";
				}
				print "</td>";
			}
			print "</tr>";

		} else {
			print "<tr bgcolor=#ffffff>";
			print "<td class=\"". ($is_logi ? 'is-logi' : ''). "\">". $row->store_nm. "(". $row->store_cd. ")</td>";
			print "<td class=\"". ($is_logi ? 'is-logi' : ''). "\">".get_status_string($row) ."</td>";
			if ($sub_rowspan > 0) {
				print "<td rowspan=".$sub_rowspan." width=150 class=\"stop-link ". ($is_logi ? 'is-logi' : ''). "\">";
				if ($is_logi) {
					print "-";

				} else if (($row->status=='0' || $row->status=='1') && ($row->stop_yn=='0')) {
					print "<a href='updord_stop_confirm.php?ord_no=" .$row->ord_no . "&ptn=1'>出荷停止</a>";

				} elseif ($row->stop_yn=='1') {
					print "停止済み";

				} else {
					print "-";
				}
				print "</td>";
			}
			print "</tr>";
		}
		$j=2;
	}
	print "</table>";
	print "<a href=\"./updord_restart.php\">出荷停止解除画面はこちら</a>";
	print "<HR>";
}
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>
