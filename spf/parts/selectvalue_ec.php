<?PHP
function Retgoodscategory($category)
{
//カテゴリ名称返却
switch ($category){
	case 11:
		$retvalue= "コミック";
		break;
	case 12:
		$retvalue= "書籍";
		break;
	case 13:
		$retvalue= "雑誌";
		break;
	case 31:
		$retvalue= "CD";
		break;
	case 51:
		$retvalue= "GAME";
		break;
	case 71:
		$retvalue= "DVD";
		break;
	case 91:
		$retvalue= "部門商品";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}

function Retkessai($index)
{
//決済方法返却
switch ($index){
	case 2:
		$retvalue= "クレジット";
		break;
	case 4:
		$retvalue= "代引";
		break;
	case 6:
		$retvalue= "Yahooマネー";
		break;
	case 7:
		$retvalue= "全額ポイント支払い";
		break;
	case 8:
		$retvalue= "Amazonにお問合せ下さい";
		break;
	default:
		$retvalue= "エラー";
		break;
}
	return $retvalue;
}
function Rethiduke($index)
{
	//日付整形
	if (empty($index)){
		$retvalue='未設定';
	}else{
		$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2);
	}
	return $retvalue;
}
function Rethidukejikan($index)
{
	//日付時間整形
	if (empty($index)){
		$retvalue='未設定';
	}else{
		$retvalue = substr($index,0,4). "-" .substr($index,4,2). "-" .substr($index,6,2). " ".substr($index,8,2). ":".substr($index,10,2). ":" .substr($index,12,2);
	}
	return $retvalue;
}
function Retzip($index)
{
	//郵便番号整形
	if (empty($index)){
		$retvalue='〒';
	}else{
		$retvalue = "〒".substr($index,0,3). "-" .substr($index,3,4);
	}
	return $retvalue;
}
function Rettel($index)
{
	//電話番号整形
	if (empty($index)){
		$retvalue='　';
	}else{
		switch (substr($index,1,1)){
			case 3:
				$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
				break;
			case 6:
				$retvalue = substr($index,0,2). "-" .substr($index,2,4). "-" .substr($index,6,4);
				break;
			default:
				if (strlen($index)==10){
					$retvalue = substr($index,0,3). "-" .substr($index,3,3). "-" .substr($index,6,4);
				}else{
					$retvalue = substr($index,0,3). "-" .substr($index,3,4). "-" .substr($index,7,4);
				}
				break;
		}
	}
	return $retvalue;
}
function RetORDSTSCL($index)
{
	//EC側注文ステータス
	switch ($index){
		case 1:
			$zaiko= "受付完了";
			break;
		case 2:
			$zaiko= "発送準備中";
			break;
		case 3:
			$zaiko= "出荷完了";
			break;
		case 4:
			$zaiko= "注文取消";
			break;
		case 5:
			$zaiko= "受付保留";
			break;
		case 30:
			$zaiko= "注文保留";
			break;
		default:
			$zaiko= "エラー";
			break;
	}
	return $zaiko;
}
function Retzaikokubunec($index)
{
	//商品の区分返却
	switch ($index){
		case 1:
			$zaiko= "中古";
			break;
		case 2:
			$zaiko= "新古";
			break;
		case 3:
			$zaiko= "新品";
			break;
		default:
			$zaiko= "エラー";
			break;
	}
	return $zaiko;
}

function PayjobStatus($value)
{
	switch ($value) {
		case 01:
			$retvalue= "与信待ち";
			break;
		case 02:
			$retvalue= "与信済み";
			break;
		case 03:
			$retvalue= "売上確定エラー";
			break;
		case 04:
			$retvalue= "売上確定済み";
			break;
		case 11:
			$retvalue= "与信取消待ち";
			break;
		case 12:
			$retvalue= "与信取消済み";
			break;
		case 21:
			$retvalue= "再与信待ち";
			break;
		case 23:
			$retvalue= "再与信エラー";
			break;
		case 33:
			$retvalue= "FRONT再与信エラー";
			break;
		case 91:
			$retvalue= "別途費用回収済み";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
	return $retvalue;
}

function RetStoreShipStatus($value)
{
	// SPF店舗出荷状況ステータス
	switch ($value) {
		case 0:
			$retvalue= "ピッキング待ち";
			break;
		case 1:
			$retvalue= "出荷待ち";
			break;
		case 2:
			$retvalue= "出荷完了";
			break;
		case 3:
			$retvalue= "出荷取消";
			break;
		default:
			$retvalue= "エラー";
			break;
	}
	return $retvalue;
}

?>