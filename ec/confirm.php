<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>検索結果</title>
</head>
<body>
<?php
//ファイルの読み込み
//PEARの利用     -------(1)
require_once("DB.php");
// 前画面からの検索キーワードを取得する
$keyti = addslashes(@$_POST["TI"]);
// データが入力されたかどうかチェックする． empty()関数を利用した場合
if (empty($keyti)) {
	print "データが入力されていません．<br>";
}
//ログイン情報の読み込み
require_once("login_ec.php");
//dsnの書式　データベースのタイプ://ユーザ名:パスワード@ホスト名/データベース名
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
//データベースへの接続開始
$db = DB::connect($dsn);
//エラーの抽出
if(DB::isError($db)){
	echo "Fail\n" . DB::errorMessage($db) . "\n";
}
//SQL文のCOUNT関数を使用
//$sql = "select count(*) from torder where ord_no = '".$keyti."'";
//行数の取得
//$dtcnt = $db->getOne($sql);
// 検索式をSQL文のselectコマンドで作成する(検索式は任意一致検索式)
$sql = "select distinct t.ord_no,t.MEM_ID,l.DELV_DM,l.DELV_ENTER,l.DELV_CHECK,t.ORDR_NM_FST,t.ORDR_NM_MID from torder t,torderdtl d,torderdelv l ";
$sql = $sql . "where t.ord_no = d.ord_no and d.DELV_NO = l.DELV_NO and t.ord_no = '".$keyti."'";
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<strong>検索結果</strong> <br>\n";
print "<table border=1>\n";
print "<tr><td></td>\n";
//項目名の表示
print "<td>注文番号</td>";
print "<td>会員ID</td><td>名前</td><td>配送登録日</td><td>配送方法</td><td>配送番号</td></tr>";
/*
//カラム数の取得
$ncols = $res->numCols();
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
  while($row = $res->fetchRow()){
        print "<tr><td> $j</td>";
        for($i=0;$i<$ncols;$i++){
                $column_value = $row[$i];//行の列データを取得
                print "<td> ".$column_value ."</td>";
        }
  $j++;
  }
*/
$j=1;
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr><td>". $j."</td>";
//注文番号
	print "<td> ".$row[0] ."</td>";
//会員ID
	print "<td> ".$row[1] ."</td>";
//名前
	print "<td> ".$row[5] ." " .$row[6] ."</td>";
//配送日
	print "<td> ".substr($row[2],0,4). "-" .substr($row[2],4,2). "-" .substr($row[2],6,2). " ".substr($row[2],8,2). ":".substr($row[2],10,2). ":" .substr($row[2],12,2). "</td>";
//配送方法
	if ($row[3]=='01')
		print "<td>日通</td>";
	else{
		print "<td>郵政</td>";
	}
//配送番号
	print "<td> ".substr($row[4],0,3). "-" .substr($row[4],3,2). "-" .substr($row[4],5,3). "-" .substr($row[4],8,4). "</td>";
	$j++;
	print "</TR>";
}
print "</table>";
//データの開放
$res->free();
$db->disconnect();
?>
<BR>
<FORM><INPUT TYPE="BUTTON" VALUE="戻る" onClick="history.back()"></FORM>
</body>
</html>