<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
<title>非会員の買取取引状況一覧</title>
<link type="text/css" rel="stylesheet" media="all" href="/css/bolweb-common.css">
<script language="JavaScript" src="/js/jquery-1.4.3.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/bolweb-common.js" type="text/javascript"></script>

</head>
<body bgcolor="#ffffff">
<?php

$hasInvalid = FALSE;

$from = @$_GET["from"];
if (empty($from)) {
  // $from = $keyti;
  $hasInvalid = TRUE;
}
$to = @$_GET["to"];
if (empty($to)) {
  // $to = $keyti;
  $hasInvalid = TRUE;
}

$memo = @$_GET["memo"];
if(empty($memo)) {
  $isSubmit = @$_GET["isSubmit"];
  if(empty($isSubmit)) {
    $memo = "WEB非会員申込";
  } else {
    ;
  }
}

print "<form action=\"/takuhon/no_account/no_account_user_sell_stat.php\">".
"申込み期間<input name=\"from\" type=\"text\" maxlength=\"8\" value=\"".$from."\"/>〜<input name=\"to\" type=\"text\" value=\"".$to."\" maxlength=\"8\"/><br>".
"メモ検索文字<input type=\"text\" name=\"memo\" value=\"$memo\"><input type=\"submit\"/>".
"<input type=\"hidden\" name=\"isSubmit\" value=\"on\"/>".
"</form>";

if($hasInvalid) {
  print "申し込み期間の開始と終了日をYYYYMMDD(年月日)8桁の数字で指定してください。";
} else {

//PEARの利用     -------(1)
require_once("DB.php");

//ログイン情報の読み込み
require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\ec\login_ec.php");
$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
$db = DB::connect($dsn);
if(DB::isError($db)){
  echo "Fail\n" . DB::errorMessage($db) . "\n";
}

$keyti= date("Ymd",mktime(0, 0, 0, date("m"), date("d"),   date("Y")));


$sql = "select ".
"tsell.sell_no, ".
"case ".
"when tsell.sell_stat = '01' then '受付' ".
"when tsell.sell_stat = '02' then '計算中' ".
"when tsell.sell_stat = '03' then '承認待ち' ".
"when tsell.sell_stat = '04' then '本人確認待ち' ".
"when tsell.sell_stat = '05' then '振り込み準備中' ".
"when tsell.sell_stat = '06' then '完了' ".
"when tsell.sell_stat = '07' then '承認保留' ".
"when tsell.sell_stat = '08' then '受付保留' ".
"when tsell.sell_stat = '97' then '査定完了（振込なし）' ".
"when tsell.sell_stat = '99' then '取り消し' ".
"else '不明なステータス' ".
"end sell_stat, ".
"tsell.sell_stat sell_stat_cd, ".
"substr(tsell.reg_dm,1,8), ".
"tsell.PICKUP_DT, ".
"substr(tsell.ADMIT_DM,1,8), ".
"tsell.AUTH_DT, ".
"tsell.TRAN_DT, ".
"tsell.CANCEL_DT, ".
"tsell.PICKUP_NM_FST, ".
"tsell.PICKUP_NM_MID, ".
"tsell.PICKUP_NM_LAST, ".
"tsell.PICKUP_NM_ETC, ".
"tsell.PICKUP_ZIP_CD, ".
"tsell.PICKUP_ZIP_ADDR1, ".
"tsell.PICKUP_ZIP_ADDR2, ".
"tsell.PICKUP_ZIP_ADDR3, ".
"tsell.PICKUP_DTL_ADDR, ".
"tsell.BOX, ".
"tsellassessment.BOOK_MEMO, ".
"tsellassessment.BOOK_ASS_AMT, ".
"tsellassessment.COMIC_MEMO, ".
"tsellassessment.COMIC_ASS_AMT, ".
"tsellassessment.MUSIC_MEMO, ".
"tsellassessment.MUSIC_ASS_AMT, ".
"tsellassessment.VIDEO_MEMO, ".
"tsellassessment.VIDEO_ASS_AMT, ".
"tsellassessment.GAME_MEMO, ".
"tsellassessment.GAME_ASS_AMT, ".
"tsellassessment.OTHER_MEMO, ".
"tsellassessment.OTHER_ASS_AMT, ".
"tsellassessment.VALID_ITEM_CNT, ".
"tsellassessment.INVALID_ITEM_CNT, ".
"tsellassessment.TOT_ASS_AMT, ".
"tsell.MEMO ".
"from ".
"tsell, ".
"tsellassessment ".
"where ".
"tsell.sell_no = tsellassessment.sell_no and ".
"tsell.reg_dm > '$from"."000000' and ".
"tsell.reg_dm < '$to"."235959' ";

if(!empty($memo)) {
$sql = $sql." and tsell.memo like '%".$memo."%'";
}

// print $sql;
$res = $db->query($sql);
if(DB::isError($res)){
	$res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
}
//検索結果の表示
print "<div>";
print "<table>\n";
print "<tr>\n";
//項目名の表示
print "<th>買取受付番号</th><th>買取ステータス名</th><th>買取ステータス</th><th>申込日</th><th>集荷実績日</th><th>承認日時</th><th>本人確認完了日</th><th>振込みデータDL日</th><th>取り消し日</th><th>集荷先お名前(姓)</th><th>集荷先お名前(名)</th><th>集荷先フリガナ(セイ)</th><th>集荷先フリガナ(メイ)</th><th>集荷先郵便番号</th><th>集荷先住所1</th><th>集荷先住所1</th><th>集荷先住所1</th><th>集荷先住所1</th><th>申込箱数</th><th>書籍点数</th><th>書籍金額</th><th>コミック点数</th><th>コミック金額</th><th>CD点数</th><th>CD金額</th><th>DVD点数</th><th>DVD金額</th><th>ゲーム点数</th><th>ゲーム金額</th><th>その他点数</th><th>その他金額</th><th>買取点数</th><th>D点数</th><th>計算金額合計</th><th>メモ</th></tr>";
//検索結果の内容の表示処理(検索結果カラム数分を繰り返す)
while($row = $res->fetchRow()){
	print "<tr>";
	print "<td class=\"code\">". $row[0]."</td>";
	print "<td>".$row[1] ."</td>";
	print "<td class=\"text\">".$row[2] ."</td>";
	print "<td class=\"text\">".$row[3] ."</td>";
	print "<td class=\"text\">".$row[4] ."</td>";
	print "<td class=\"text\">".$row[5] ."</td>";
	print "<td class=\"text\">".$row[6] ."</td>";
	print "<td class=\"text\">".$row[7] ."</td>";
	print "<td class=\"text\">".$row[8] ."</td>";
	print "<td class=\"text\">".$row[9] ."</td>";
	print "<td class=\"text\">".$row[10] ."</td>";
	print "<td class=\"text\">".$row[11] ."</td>";
	print "<td class=\"text\">".$row[12] ."</td>";
	print "<td class=\"text\">".$row[13] ."</td>";
	print "<td class=\"text\">".$row[14] ."</td>";
	print "<td class=\"text\">".$row[15] ."</td>";
	print "<td class=\"text\">".$row[16] ."</td>";
	print "<td class=\"text\">".$row[17] ."</td>";
	print "<td class=\"text\">".$row[18] ."</td>";
	print "<td class=\"text\">".$row[19] ."</td>";
	print "<td class=\"text\">".$row[20] ."</td>";
	print "<td class=\"text\">".$row[21] ."</td>";
	print "<td class=\"text\">".$row[22] ."</td>";
	print "<td class=\"text\">".$row[23] ."</td>";
	print "<td class=\"text\">".$row[24] ."</td>";
	print "<td class=\"text\">".$row[25] ."</td>";
	print "<td class=\"text\">".$row[26] ."</td>";
	print "<td class=\"text\">".$row[27] ."</td>";
	print "<td class=\"text\">".$row[28] ."</td>";
	print "<td class=\"text\">".$row[29] ."</td>";
	print "<td class=\"text\">".$row[30] ."</td>";
	print "<td class=\"text\">".$row[31] ."</td>";
	print "<td class=\"text\">".$row[32] ."</td>";
	print "<td class=\"text\">".$row[33] ."</td>";
	print "<td class=\"text\">".$row[34] ."</td></tr>";
}
print "</table>";
print "</div>";
//データの開放
$res->free();
$db->disconnect();

}
?>
</body>
</html>
