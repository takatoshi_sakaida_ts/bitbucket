<CTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=SHIFT-JIS">
  <title>在庫状況検索</title>
  <link type="text/css" rel="stylesheet" media="all" href="/css/bolweb-common.css">
  <script language="JavaScript" src="/js/jquery-1.4.3.js" type="text/javascript"></script>
  <script language="JavaScript" src="/js/bolweb-common.js" type="text/javascript"></script>
</head>
<body bgcolor="#ffffff">
  <?php
    require_once("DB.php");
    require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\souko\parts\selectvalue_souko.php");
    require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\souko\parts\login_souko.php");

    $instorecode = @$_GET["instorecode"];

    print "<form action=\"/search/item/itemInfo.php\" method=\"get\">インストアコード <input type=\"txt\" name=\"instorecode\" value=\"$instorecode\"><input type=\"submit\" value=\"検索\"></form>";

    if(!empty($instorecode)) {

      $sql = "select ".
        " stock.LOCATION_KIND, ".
        " stock.STOCKRESERVE_KIND, ".
        " stock.LOCATIONCODE, ".
        " stock.ZONECODE, ".
        " stock.STOCKNO, ".
        " stock.STOCKDATE, ".
        " stock.NEWUSED_KIND, ".
        " stock.MODIFYDATE, ".
        " stock.MODIFYTIME, ".
        " reception.receptioncode, ".
        " max(tana_in.tana_kind), ".
        " max(tana_out.tana_kind) ".
        "from ".
        "( ".
        "select ".
        " d_stock.INSTORECODE, ".
        " M_LOCATION.LOCATION_KIND, ".
        " d_stock.LOCATIONCODE, ".
        " M_LOCATION.ZONECODE, ".
        " d_stock.STOCKNO, ".
        " d_stock.STOCKDATE, ".
        " d_stock.NEWUSED_KIND, ".
        " d_stock.STOCKRESERVE_KIND, ".
        " d_stock.MODIFYDATE, ".
        " d_stock.MODIFYTIME ".
        "from ".
        " d_stock, ".
        " m_location  ".
        "where ".
        " d_stock.instorecode = '".$instorecode."' and ".
        " d_stock.LOCATIONCODE = m_location.LOCATIONCODE ".
        ") stock, ".
        "( ".
        "select ".
        " d_reception_goods.stockno, ".
        " d_reception_goods.receptioncode ".
        "from ".
        " d_reception_goods ".
        "where ".
        " d_reception_goods.instorecode = '".$instorecode."' ".
        ") reception, ".
        "( ".
        "select ".
        " d_tana_goods.stockno, ".
        " d_tana_goods.tana_kind ".
        "from ".
        " d_tana_goods ".
        "where ".
        " d_tana_goods.instorecode = '".$instorecode."' and".
        " d_tana_goods.tana_kind >= '50'".
        ") tana_in, ".
        "( ".
        "select ".
        " d_tana_goods.stockno, ".
        " d_tana_goods.tana_kind ".
        "from ".
        " d_tana_goods ".
        "where ".
        " d_tana_goods.instorecode = '".$instorecode."' and".
        " d_tana_goods.tana_kind < '50'".
        ") tana_out ".
        "where ".
        " stock.stockno = reception.stockno(+) and ".
        " stock.stockno = tana_in.stockno(+) and ".
        " stock.stockno = tana_out.stockno(+) ".
        "group by ".
        " stock.LOCATION_KIND, ".
        " stock.STOCKRESERVE_KIND, ".
        " stock.LOCATIONCODE, ".
        " stock.ZONECODE, ".
        " stock.STOCKNO, ".
        " stock.STOCKDATE, ".
        " stock.NEWUSED_KIND, ".
        " stock.MODIFYDATE, ".
        " stock.MODIFYTIME, ".
        " reception.receptioncode ".
        "order by ".
        " stock.LOCATION_KIND, stock.STOCKRESERVE_KIND";

	$res_stockInfo = $db->query($sql);
	if(DB::isError($res_stockInfo)){
	  $res_stockInfo->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	  print $sql;
	}

	print "<div>d_stock(+m_location/d_tana_goods/d_reception_goods)";
	print "<table>";
        $no = 1;
	print "<tr><th>No</th><th>ロケーション区分</th><th>在庫引当区分</th><th>ロケーションコード</th><th>ゾーンコード</th><th>在庫番号</th><th>取引日付</th><th>新古区分</th><th>更新日付</th><th>更新時間</th><th>注文番号</th><th>棚区分(in)</th><th>棚区分(out)</th></tr>";
	while($row = $res_stockInfo->fetchRow()) {
	  print "<tr>".
	  "<td>". $no."</td>".
	  "<td>". $row[0]."</td>".
	  "<td>". $row[1]."</td>".
	  "<td>". $row[2]."</td>".
	  "<td>". $row[3]."</td>".
	  "<td>". $row[4]."</td>".
	  "<td>". $row[5]."</td>".
	  "<td>". $row[6]."</td>".
	  "<td>". $row[7]."</td>".
	  "<td>". $row[8]."</td>".
	  "<td>". $row[9]."</td>".
	  "<td>". $row[10]."</td>".
	  "<td>". $row[11]."</td>".
	  "</tr>";
          $no++;
	}
	print "</table>";
	print "</div>";

	$sql = "select ".
	 "D_SLIP.SLIPCODE, ".
	 "D_SLIP.SLIPDATE, ".
	 "m_staff.staffname, ".
	 "D_SLIP.SLIP_KIND, ".
	 "D_SLIP.SLIPNO, ".
	 "D_SLIP.REMARK, ".
	 "D_SLIP.AUXCODE, ".
	 "D_SLIP.MODIFYDATE, ".
	 "D_SLIP.MODIFYTIME, ".
	 "D_SLIP_GOODS.DETAILNO, ".
	 "D_SLIP_GOODS.STOCKNO, ".
	 "D_SLIP_GOODS.LOCATIONCODE, ".
	 "D_SLIP_GOODS.INSTORECODE, ".
	 "D_SLIP_GOODS.NEWUSED_KIND ".
	" from d_slip,d_slip_goods,M_STAFF where d_slip.slipcode = d_slip_goods.slipcode and d_slip_goods.instorecode = '".$instorecode."' and d_slip.SLIPSTAFFCODE = m_staff.staffcode order by d_slip.slipdate desc";

	$res_slipInfo = $db->query($sql);
	if(DB::isError($res_slipInfo)){
	  $res_slipInfo->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	  print $sql;
	}

	$db->disconnect();

	require_once("C:\Program Files\Apache Software Foundation\Apache2.2\htdocs\ec\login_ec.php");
	$dsn = "oci8://". $usr . ":" . $pwd . "@" . $dbn;
	$db = DB::connect($dsn);
	if(DB::isError($db)){
	  echo "Fail\n" . DB::errorMessage($db) . "\n";
	}

	$sql = "select ".
	 "STOCK_NEW, ".
	 "STOCK_NEW_REC, ".
	 "STOCK_NEW_UPD_DM, ".
	 "STOCK_NEW_BATCH_EXEC_DM, ".
	 "STOCK_USED, ".
	 "STOCK_USED_REC, ".
	 "STOCK_USED_UPD_DM, ".
	 "STOCK_USED_BATCH_EXEC_DM, ".
	 "EXT_COP_STOCK_FLAG, ".
	 "EXT_COP_STOCK_ADD_FLAG ".
	"from tstock_bko where tstock_bko.instorecode = '".$instorecode."'";

	$res = $db->query($sql);

	if(DB::isError($res)){
	  $res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	  print $sql;
	}
	print "<div>tgoods_bko";
	print "<table>";
	print "<tr></th><th>新古品在庫</th><th>新古品直近在庫</th><th>新古品在庫更新日時</th><th>新古用バッチ開始日時</th><th>中古品在庫</th><th>中古品直近在庫</th><th>中古品在庫更新日時</th><th>中古用バッチ開始日時</th><th>外部在庫連携フラグ</th><th>外部在庫加算連携フラグ</th></tr>";
	while($row = $res->fetchRow()) {
	  print "<tr>".
	  "<td>". $row[0]."</td>".
	  "<td>". $row[1]."</td>".
	  "<td>". $row[2]."</td>".
	  "<td>". $row[3]."</td>".
	  "<td>". $row[4]."</td>".
	  "<td>". $row[5]."</td>".
	  "<td>". $row[6]."</td>".
	  "<td>". $row[7]."</td>".
	  "<td>". $row[8]."</td>".
	  "<td>". $row[9]."</td>".
	  "</tr>";
	}
	print "</table>";
	print "</div>";


	$sql = "select ".
	"(torderdtl.goods_cnt - torderdtl.ORD_CNCL_CNT), ".
	"torderdtl.ORD_NO, ".
	"torderdtl.GOODS_CNT, ".
	"torderdtl.ORD_DM, ".
	"torderdtl.ORD_CNCL_CNT, ".
	"torderdtl.ORD_CNCL_DM, ".
	"torderdtl.DELV_NO, ".
	"torderdtl.CLM_YN, ".
	"torder.ORD_STAT_CL, ".
	"torderdtl.UPD_DM, ".
	"torderdtl.USER_ID, ".
	"torderdtl.STOCK_TP, ".
	"torder.EXT_ORD_NO ".
	"from ".
	"torder, ".
	"torderdtl ".
	"where ".
	"torderdtl.instorecode = '".$instorecode."' and ".
	"torder.ord_stat_cl not in ('03','04') and ".
	"torder.ord_no = torderdtl.ord_no ".
	"order by torder.ord_dm";

	$res = $db->query($sql);
	if(DB::isError($res)){
	  $res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	  print $sql;
	}
	print "<div>torderdtl(注文以降出荷前)";
	print "<table>";
	print "<tr><th>実質出荷点数</th><th>注文番号</th><th>注文数量</th><th>注文日時</th><th>注文取消数量</th><th>注文取消日時</th><th>配送番号</th><th>クレーム発生有無</th><th>注文ステータス</th><th>データ更新日時</th><th>担当者ID</th><th>新品/新古/中古フラグ</th><th>外部システム注文番号</th></tr>";
	while($row = $res->fetchRow()) {
	  print "<tr>".
	  "<td>". $row[0]."</td>".
	  "<td>". $row[1]."</td>".
	  "<td>". $row[2]."</td>".
	  "<td>". $row[3]."</td>".
	  "<td>". $row[4]."</td>".
	  "<td>". $row[5]."</td>".
	  "<td>". $row[6]."</td>".
	  "<td>". $row[7]."</td>".
	  "<td>". $row[8]."</td>".
	  "<td>". $row[9]."</td>".
	  "<td>". $row[10]."</td>".
	  "<td>". $row[11]."</td>".
	  "<td>". $row[12]."</td>".
	  "</tr>";
	}
	print "</table>";
	print "</div>";



	print "<div>d_slip";
	print "<table>";
	print "<tr><th>伝票コード</th><th>取引日付</th><th>処理スタッフ</th><th>伝票区分</th><th>伝票番号</th><th>備考</th><th>補助コード</th><th>更新日付</th><th>更新時間</th><th>明細番号</th><th>在庫番号</th><th>ロケーションコード</th><th>新古区分</th></tr>";
	while($row = $res_slipInfo->fetchRow()) {
	  print "<tr>".
	  "<td>". $row[0]."</td>".
	  "<td>". $row[1]."</td>".
	  "<td>". $row[2]."</td>".
	  "<td>". $row[3]."</td>".
	  "<td>". $row[4]."</td>".
	  "<td>". $row[5]."</td>".
	  "<td>". $row[6]."</td>".
	  "<td>". $row[7]."</td>".
	  "<td>". $row[8]."</td>".
	  "<td>". $row[9]."</td>".
	  "<td>". $row[10]."</td>".
	  "<td>". $row[11]."</td>".
	  "<td>". $row[12]."</td>".
	  "</tr>";
	}
	print "</table>";
	print "</div>";




	$sql = "select ".
"(torderdtl.goods_cnt - torderdtl.ORD_CNCL_CNT), ".
"torder.ORD_NO, ".
"torder.MEM_NO, ".
"torder.CLM_YN, ".
"torder.ORD_CRT_TP, ".
"torder.ORD_STAT_CL, ".
"torder.PAY_TP, ".
"torder.ORD_DM as ord_dm, ".
"torder.UPD_DM, ".
"torder.EXPORT_STAT, ".
"torder.EXPORT_CNT, ".
"torder.FST_ORD_NO, ".
"torder.MC_ORD_ID, ".
"torder.EXT_ORD_NO ".
"from ".
"torder, ".
"torderdtl ".
"where ".
"torderdtl.instorecode = '".$instorecode."' and ".
"torder.ord_no = torderdtl.ord_no ".
"order by ord_dm desc";

	$res = $db->query($sql);
	if(DB::isError($res)){
	  $res->DB_Error($res->getcode(),PEAR_ERROR_DOE,NULL,NULL);
	  print $sql;
	}
	print "<div>torderdtl(直近90日分)";
	print "<table>";
	print "<tr><th>実質注文点数</th><th>注文番号</th><th>会員番号</th><th>クレーム有無</th><th>注文申込区分</th><th>注文ステータス</th><th>支払い方法</th><th>注文日時</th><th>データ更新日時</th><th>倉庫向け出力ステータス</th><th>倉庫向け出力回数</th><th>初回注文番号</th><th>モルコネ注文ID</th><th>外部システム注文番号</th></tr>";
	while($row = $res->fetchRow()) {
	  print "<tr>".
	  "<td>". $row[0]."</td>".
	  "<td>". $row[1]."</td>".
	  "<td>". $row[2]."</td>".
	  "<td>". $row[3]."</td>".
	  "<td>". $row[4]."</td>".
	  "<td>". $row[5]."</td>".
	  "<td>". $row[6]."</td>".
	  "<td>". $row[7]."</td>".
	  "<td>". $row[8]."</td>".
	  "<td>". $row[9]."</td>".
	  "<td>". $row[10]."</td>".
	  "<td>". $row[11]."</td>".
	  "<td>". $row[12]."</td>".
	  "<td>". $row[13]."</td>".
	  "</tr>";
	}
	print "</table>";
	print "</div>";

        $res_stockInfo->free();
        $res_slipInfo->free();

	$res->free();
        $db->disconnect();
      }
?>
</body>
</html>
